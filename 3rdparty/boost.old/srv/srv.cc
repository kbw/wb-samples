//
// async_tcp_echo_server.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2021 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <boost/asio.hpp>

#include <csignal>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <ctime>

#include <chrono>
#include <iostream>
#include <list>
#include <memory>
#include <utility>

using boost::asio::ip::tcp;

class session : public std::enable_shared_from_this<session>
{
public:
	session(tcp::socket socket) : socket_(std::move(socket)) {
		std::clog << "session: accepted connection" << std::endl;
	}

	~session() {
		std::clog << "~session" << std::endl;
	}

	void start() {
		strcpy(data_.get(), "Welcome to Wylbur, ");
		time_t now = std::time(nullptr);
		struct tm tm_now;
		localtime_r(&now, &tm_now);
		asctime_r(&tm_now, &data_.get()[std::strlen(data_.get())]);

		do_write(strlen(data_.get()));
//		do_read();
	}

private:
	void do_read() {
		auto self(shared_from_this());
		socket_.async_read_some(
				boost::asio::buffer(data_.get(), max_length_),
				[this, self](boost::system::error_code ec, std::size_t length) {
					if (!ec) {
						do_write_reply(length);
//						do_write(length);
					}
				});
	}

	void do_write_reply(std::size_t length) {
		{
/*
			auto tmp = std::make_unique<char[]>(max_length_);
			memcpy(tmp.get(), "reply: ", 7);
			memcpy(tmp.get() + 7, data_.get(), length);
			tmp.get()[7 + length] = 0;
			length += 7;

			std::swap(data_, tmp);
 */
 			std::memmove(data_.get() + 7, data_.get(), length);
			std::memcpy(data_.get(), "reply: ", 7);
			length += 7;
		}

		do_write(length);
	}

	void do_write(std::size_t length) {
		auto self(shared_from_this());
		boost::asio::async_write(
				socket_, boost::asio::buffer(data_.get(), length),
				[this, self](boost::system::error_code ec, std::size_t /*length*/) {
					if (!ec) {
						do_read();
					}
				});
	}

	tcp::socket socket_;
	static inline constexpr std::size_t max_length_{ 1024 };
	std::unique_ptr<char[]> data_ = std::make_unique<char[]>(max_length_);
};

class server
{
public:
	server(boost::asio::io_context& io_context, std::uint16_t port)
		: acceptor_(io_context, tcp::endpoint(tcp::v4(), port)) {
		do_accept();
	}

	~server() {
		std::clog << "~server" << std::endl;
	}

private:
	void do_accept() {
		acceptor_.async_accept(
				[this](boost::system::error_code ec, tcp::socket socket) {
					if (!ec) {
						std::make_shared<session>(std::move(socket))->start();
					}

					do_accept();
				});
	}

	tcp::acceptor acceptor_;
};

class server_manager
{
public:
	server_manager() = default;

	~server_manager() {
		stop();
	}

	void add_server(const char* port_str) {
		add_server(static_cast<uint16_t>(std::atoi(port_str)));
	}

	void add_server(std::uint16_t port) {
		ss_.emplace_back(io_context_, port);
	}

	bool stopped() const {
		return io_context_.stopped();
	}

	void start() {
		if (!runner_) {
			runner_.reset( new std::thread([this]() mutable {
					io_context_.run();
				}) );
		}
	}

	void stop() {
		if (runner_) {
			io_context_.stop();
			runner_->join();
			runner_.reset();
		}
	}

private:
	using servers = std::list<server>;

	boost::asio::io_context io_context_;
	std::unique_ptr<std::thread> runner_;
	servers ss_;
};

server_manager* g_mgr;

int main(int argc, char* argv[]) {
	std::signal(SIGINT, [](int /*sig*/) {
			if (g_mgr)
				g_mgr->stop();
		});

	try {
		if (argc == 1) {
			std::clog << "Usage: " << argv[0] << " <port> ..." << std::endl;
			return 1;
		}

		server_manager mgr;
		g_mgr = &mgr;

		for (int i = 1; i < argc; ++i)
			mgr.add_server(argv[i]);
		mgr.start();

		while (!mgr.stopped()) {
			using namespace std::chrono_literals;
			std::this_thread::sleep_for(1s);
		}
/*
		boost::asio::io_context io_context;
		server s(io_context, std::atoi(argv[1]));
		io_context.run();
 */
	} catch (std::exception& e) {
		std::cerr << "fatal: " << e.what() << std::endl;
		return 1;
	}
}
