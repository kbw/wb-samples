#pragma once

#include "runner.hpp"
#include "tcp_session.hpp"
#include "tcp_server.hpp"
#include "udp_server.hpp"

#include <iostream>
#include <list>
#include <memory>
#include <utility>

class server_manager
{
public:
	server_manager(runner& run) : run_(run) {
	}

	~server_manager() = default;

	template <class U>
	void add_tcp_server(std::uint16_t port) {
		tcp_ss_.push_back(
			std::unique_ptr<tcp_server<tcp_session>>(
				reinterpret_cast<tcp_server<tcp_session>*>(new tcp_server<U>{run_.io_context(), port}) ));
	}

	template <class U>
	void add_udp_server(std::uint16_t port) {
		udp_ss_.push_back(
			std::unique_ptr<udp_server>(
				reinterpret_cast<udp_server*>(new U{run_, port}) ));
	}

private:
	using tcp_servers = std::list<std::unique_ptr<tcp_server<tcp_session>>>;
	using udp_servers = std::list<std::unique_ptr<udp_server>>;

	runner& run_;
	tcp_servers tcp_ss_;
	udp_servers udp_ss_;
};
