#include "runner.hpp"

runner::~runner() {
	stop();
}

boost::asio::io_context& runner::io_context() {
	return io_context_;
}

bool runner::stopped() const {
	return io_context_.stopped();
}

void runner::start() {
	if (!runner_) {
		runner_.reset( new std::thread([this]() mutable {
				io_context_.run();
			}) );
	}
}

void runner::stop() {
	if (runner_) {
		io_context_.stop();
		runner_->join();
		runner_.reset();
	}
}
