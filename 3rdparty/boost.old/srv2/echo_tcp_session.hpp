#pragma once

#include "tcp_session.hpp"

#include <csignal>
#include <cstdlib>
#include <cstring>
#include <ctime>

class echo_tcp_session : public tcp_session
{
public:
	echo_tcp_session(boost::asio::ip::tcp::socket socket) : tcp_session(std::move(socket)) {
		std::clog << "echo_tcp_session: accepted connection" << std::endl;
	}

	virtual ~echo_tcp_session() {
		std::clog << "echo_tcp_session: closed" << std::endl;
	}

	virtual bool onConnect() {
		strcpy(data_.get(), "Welcome to Wylbur, ");
		time_t now = std::time(nullptr);
		struct tm tm_now;
		localtime_r(&now, &tm_now);
		asctime_r(&tm_now, &data_.get()[std::strlen(data_.get())]);

		return true;
	}

	virtual std::size_t onRead(std::size_t read_length) {
		static const std::string prefix{"reply: "};
 		std::memmove(data_.get() + prefix.size(), data_.get(), read_length);
		std::memcpy(data_.get(), prefix.c_str(), prefix.size());
		return read_length += prefix.size();
	}
};
