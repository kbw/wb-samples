#pragma once

#include "runner.hpp"
#include "udp_server.hpp"

class echo_udp_server : public udp_server
{
public:
	using udp_server::udp_server;

#ifdef USE_HIDDEN_CODE
	echo_udp_server(runner& run, std::uint16_t port) : udp_server(run, port) {
		do_receive();
	}

	void do_receive()
	{
		socket_.async_receive_from(boost::asio::buffer(data_, max_length), sender_endpoint_,
				[this](boost::system::error_code ec, std::size_t bytes_recvd) {
					if (!ec && bytes_recvd > 0) {
						do_send(bytes_recvd);
					} else {
						do_receive();
					}
				});
	}

	void do_send(std::size_t length)
	{
		socket_.async_send_to(boost::asio::buffer(data_, length), sender_endpoint_,
				[this](boost::system::error_code /*ec*/, std::size_t /*bytes_sent*/) {
					do_receive();
				});
	}

private:
	runner& run_;
	boost::asio::ip::udp::socket socket_;
	boost::asio::ip::udp::endpoint sender_endpoint_;
	enum { max_length = 1024 };
	char data_[max_length];
#endif // USE_HIDDEN_CODE
};
