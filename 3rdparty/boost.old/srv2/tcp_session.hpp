#pragma once

#include <boost/asio.hpp>

#include <iostream>
#include <memory>
#include <utility>

class tcp_session : public std::enable_shared_from_this<tcp_session>
{
public:
	tcp_session(boost::asio::ip::tcp::socket socket) : socket_(std::move(socket)) {
		std::clog << "session: accepted connection" << std::endl;
	}

	virtual ~tcp_session() {
		onDisconnect();
		std::clog << "session: closed" << std::endl;
	}

	virtual bool onConnect() {
		return false;
	}

	virtual void onDisconnect() {
	}

	virtual std::size_t onRead(std::size_t read_length) {
		return read_length;
	}

	void start() {
		if (onConnect())
			do_write(strlen(data_.get()));
		else
			do_read();
	}

private:
	void do_read() {
		auto self(shared_from_this());
		socket_.async_read_some(
				boost::asio::buffer(data_.get(), max_length_),
				[this, self](boost::system::error_code ec, std::size_t length) {
					if (!ec) {
						auto write_length = onRead(length);
						do_write(write_length);
					}
				});
	}

	void do_write(std::size_t length) {
		auto self(shared_from_this());
		boost::asio::async_write(
				socket_, boost::asio::buffer(data_.get(), length),
				[this, self](boost::system::error_code ec, std::size_t /*length*/) {
					if (!ec) {
						do_read();
					}
				});
	}

protected:
	boost::asio::ip::tcp::socket socket_;

	static inline constexpr std::size_t max_length_{ 1024 };
	std::unique_ptr<char[]> data_ = std::make_unique<char[]>(max_length_);
};
