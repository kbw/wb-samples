#pragma once

#include <boost/asio.hpp>

#include <memory>
#include <thread>

class runner
{
public:
	runner() = default;
	~runner();

	boost::asio::io_context& io_context();

	bool stopped() const;

	void start();
	void stop();

private:
	std::unique_ptr<std::thread> runner_;
	boost::asio::io_context io_context_;
};
