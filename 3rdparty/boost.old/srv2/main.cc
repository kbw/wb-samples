//
// based on: async_tcp_echo_server.cpp
// bugs:
//	- doesn't release closed sessions
//

#include "runner.hpp"
#include "server_manager.hpp"
#include "echo_tcp_session.hpp"
#include "echo_udp_server.hpp"

std::uint16_t to_port(const char* arg) {
	return static_cast<std::uint16_t>(std::atoi(arg));
}

int main(int argc, char* argv[]) {
	static runner* g_run;
	std::signal(SIGINT, [](int /*sig*/) {
			if (g_run)
				g_run->stop();
		});

	try {
		if (argc == 1) {
			std::clog << "Usage: " << argv[0] << " <port> ..." << std::endl;
			return 1;
		}

		runner run;
		server_manager mgr(run);
		g_run = &run;

		for (int i = 1; i < argc; ++i) {
			mgr.add_tcp_server<echo_tcp_session>( to_port(argv[i]) );
			mgr.add_udp_server<echo_udp_server>( to_port(argv[i]) );
		}

		run.start();
		while (!run.stopped()) {
			using namespace std::chrono_literals;
			std::this_thread::sleep_for(1s);
		}
	} catch (std::exception& e) {
		std::cerr << "fatal: " << e.what() << std::endl;
		return 1;
	}
}
