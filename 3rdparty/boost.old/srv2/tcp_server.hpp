#pragma once

#include <boost/asio.hpp>

#include <cstdint>

#include <iostream>

template <class T>
class tcp_server
{
public:
	tcp_server(boost::asio::io_context& io_context, std::uint16_t port)
		: acceptor_(io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)) {
		do_accept();
	}

	~tcp_server() {
		std::clog << "server: closed" << std::endl;
	}

private:
	void do_accept() {
		acceptor_.async_accept(
				[this](boost::system::error_code ec, boost::asio::ip::tcp::socket socket) {
					if (!ec) {
						std::make_shared<T>(std::move(socket))->start();
					}

					do_accept();
				});
	}

	boost::asio::ip::tcp::acceptor acceptor_;
};
