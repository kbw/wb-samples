#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <string>
#include <iostream>

#include <string.h>

const char* get_ext(const char* filename) {
	if (const char* p = strrchr(filename, '.'))
		return p;
	return "";
}

void read(const char* filename, boost::property_tree::ptree& tree) {
	if (strcmp(get_ext(filename), ".xml") == 0)
		boost::property_tree::read_xml(filename, tree);

	if (strcmp(get_ext(filename), ".ini") == 0)
		boost::property_tree::read_ini(filename, tree);
}

int main(int argc, char* argv[])
try {
	const char* config = (argc == 2) ? argv[1] : "config.xml";

	boost::property_tree::ptree tree;
	read(config, tree);

	std::string filename = tree.get<std::string>("debug.filename");
	std::cout << "filename: \"" << filename << "\"" << std::endl;
	int level = tree.get<int>("debug.level");
	std::cout << "level: \"" << level << "\"" << std::endl;
	std::string modules = tree.get<std::string>("debug.modules", "");
	std::cout << "modules: \"" << modules << "\"" << std::endl;

	auto node = tree.get_child("debug.modules");
	std::cout << "n modules: " << node.size() << std::endl;
	for (const boost::property_tree::ptree::value_type& v : tree.get_child("debug.modules"))
		std::cout << "\t" << v.second.data() << std::endl;;
}
//catch (const boost::property_tree::xml_parser::xml_parser_error& e) {
catch (const std::exception& e) {
	std::cerr << "fatal: " << e.what() << std::endl;
}
