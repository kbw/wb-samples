#include <boost/optional.hpp>
#include <type_traits>
#include <string>
#include <iostream>

template <typename T,
		  typename std::enable_if<std::is_integral<T>::value, T>::type* = nullptr>
void ok(T n) {
	std::cout << "integral: " << n << "\n";
}

template <typename T,
		  typename std::enable_if<boost::optional_detail::is_optional_related<T>::value, T>::type* = nullptr>
void ok(T n) {
	std::cout << "boost::optional: " << (n ? n.get() : "none") << "\n";
}

void ok(std::string n) {
	std::cout << "string: " << n << "\n";
}

int main() {
	std::string str("hello");
	boost::optional<std::string> ostr(str);
	ok(22);
	ok(str);
	ok(ostr);
}
