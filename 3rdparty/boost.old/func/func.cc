#include <boost/function.hpp>

struct int_div {
	double operator()(int x, int y) const { return ((double)x) / y; }
};

struct X {
	int func(int n) { return n; }
};

int main() {
	boost::function<int (X*, int)> mf = &X::func;
	X x;
	mf(&x, 3);

	std::function<double (int, int)> f = int_div();

	int_div obj;
	f = obj;
}
