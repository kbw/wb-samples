// https://docs.microsoft.com/en-us/cpp/parallel/concrt/task-parallelism-concurrency-runtime?view=msvc-170

// basic-continuation.cpp
// compile with: /EHsc
#include <pplx/pplxtasks.h>
#include <iostream>

int main()
{
    auto t = pplx::create_task([]() -> int
    {
        return 42;
    });

    t.then([](int result)
    {
        std::cout << result << std::endl;
    }).wait();

    // Alternatively, you can chain the tasks directly and
    // eliminate the local variable.
    /*create_task([]() -> int
    {
        return 42;
    }).then([](int result)
    {
        std::cout << result << std::endl;
    }).wait();*/
}

/* Output:
    42
*/
