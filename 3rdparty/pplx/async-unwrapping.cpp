// async-unwrapping.cpp
// compile with: /EHsc
#include <pplx/pplxtasks.h>
#include <iostream>

int main()
{
    auto t = pplx::create_task([]()
    {
        std::cout << "Task A" << std::endl;

        // Create an inner task that runs before any continuation
        // of the outer task.
        return pplx::create_task([]()
        {
            std::cout << "Task B" << std::endl;
        });
    });
  
    // Run and wait for a continuation of the outer task.
    t.then([]()
    {
        std::cout << "Task C" << std::endl;
    }).wait();
}

/* Output:
    Task A
    Task B
    Task C
*/
