// https://docs.microsoft.com/en-us/cpp/parallel/concrt/task-parallelism-concurrency-runtime?view=msvc-170
// basic-task.cpp
// compile with: /EHsc
#include <pplx/pplxtasks.h>
#include <iostream>

int main()
{
    // Create a task.
    pplx::task<int> t([]()
        {
            return 42;
        });

    // In this example, you don't necessarily need to call wait() because
    // the call to get() also waits for the result.
    t.wait();

    // Print the result.
    std::cout << t.get() << std::endl;
}

/* Output:
    42
*/
