// https://docs.microsoft.com/en-us/cpp/parallel/concrt/task-parallelism-concurrency-runtime?view=msvc-170
// create-task.cpp
// compile with: /EHsc
#include <pplx/pplxtasks.h>
#include <string>
#include <iostream>
#include <array>

int main()
{
    auto create_identity_matrix = pplx::create_task([]
        {
            std::array<std::array<int, 10>, 10> matrix;
            int row = 0;
            std::for_each(begin(matrix), end(matrix), [&row](std::array<int, 10>& matrixRow) 
            {
                std::fill(std::begin(matrixRow), std::end(matrixRow), 0);
                matrixRow[row] = 1;
                row++;
            });
            return matrix;
        });

    auto print_matrix = create_identity_matrix.then([](std::array<std::array<int, 10>, 10> matrix)
    {
        std::for_each(begin(matrix), end(matrix), [](std::array<int, 10>& matrixRow) 
        {
            std::string comma;
            std::for_each(begin(matrixRow), end(matrixRow), [&comma](int n) 
            {
                std::cout << comma << n;
                comma = ", ";
            });
            std::cout << std::endl;
        });
    });

    print_matrix.wait();
}

/* Output:
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0
    0, 1, 0, 0, 0, 0, 0, 0, 0, 0
    0, 0, 1, 0, 0, 0, 0, 0, 0, 0
    0, 0, 0, 1, 0, 0, 0, 0, 0, 0
    0, 0, 0, 0, 1, 0, 0, 0, 0, 0
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0
    0, 0, 0, 0, 0, 0, 1, 0, 0, 0
    0, 0, 0, 0, 0, 0, 0, 1, 0, 0
    0, 0, 0, 0, 0, 0, 0, 0, 1, 0
    0, 0, 0, 0, 0, 0, 0, 0, 0, 1
*/
