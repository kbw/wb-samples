// continuation-chain.cpp
// compile with: /EHsc
#include <pplx/pplxtasks.h>
#include <iostream>

int main()
{
    auto t = pplx::create_task([]() -> int
        { 
            return 0;
        });
    
    // Create a lambda that increments its input value.
    auto increment = [](int n) { return n + 1; };

    // Run a chain of continuations and print the result.
    int result = t.then(increment).then(increment).then(increment).get();
    std::cout << result << std::endl;
}

/* Output:
    3
*/
