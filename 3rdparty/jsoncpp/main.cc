#include <jsoncpp/json/reader.h>

#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <stdexcept>

Json::Value parser1(const std::string& json_str) {
	std::istringstream is(json_str);
	Json::Value json;
	is >> json;
	return json;
}

Json::Value parser2(const std::string& json_str) {
	Json::Value json;

	Json::CharReaderBuilder builder;
	std::unique_ptr<Json::CharReader> reader{builder.newCharReader()};
	std::string error_text;
	if (!reader->parse(json_str.data(), json_str.data() + json_str.size(), &json, &error_text))
		throw std::runtime_error("bad json: " + json_str);

	return json;
}

void show_results(Json::Value& json) {
	std::clog << "not_found: " << json["not_found"].type() << "\n";
	std::clog << "content-type: " << json["content-type"].type() << " stringValue: " << Json::stringValue << "\n";
	std::clog << "content-length: " << json["content-length"].type() << " intValue: " << Json::intValue << "\n";

	const std::string key{"content-type"};
	auto value = json[key.c_str()];
	std::clog << "key: " << key << " value: " << value.asString() << '\n';
//	std::clog << "key: " << key << " value: " << std::string{value2.asCString(), value2.asCStringLength()} << '\n';
}

int main() {
//	const std::string json_str{R"({"byteorder": "little", "content-type": "text/json", "content-encoding": "utf-8", "content-length": 42})"};
	const std::string json_str{R"({"byteorder": "little", "content-type: "text/json", "content-encoding": "utf-8", "content-length": 42})"};

    try {
        Json::Value json1 = parser1(json_str);
        show_results(json1);
    } catch (const std::exception& e) {
        std::clog << "fatal on json1: " << e.what() << '\n';
    }

    try {
        Json::Value json2 = parser2(json_str);
        show_results(json2);
    } catch (const std::exception& e) {
        std::clog << "fatal on json2: " << e.what() << '\n';
    }
}
