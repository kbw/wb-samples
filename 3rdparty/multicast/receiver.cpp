//
// receiver.cpp
// ~~~~~~~~~~~~
//
// Copyright (c) 2003-2021 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <scn/scan.h>
#include <spdlog/spdlog.h>

#include <boost/asio.hpp>

#include <array>
//#include <iostream>
#include <string>
#include <string_view>

constexpr short multicast_port = 30001;

class receiver {
public:
	receiver(boost::asio::io_context& io_context,
			const boost::asio::ip::address& listen_address,
			const boost::asio::ip::address& multicast_address)
		: socket_(io_context)
	{
		// Create the socket so that multiple may be bound to the same address.
		boost::asio::ip::udp::endpoint listen_endpoint(listen_address, multicast_port);
		socket_.open(listen_endpoint.protocol());
		socket_.set_option(boost::asio::ip::udp::socket::reuse_address(true));
		socket_.bind(listen_endpoint);

		// Join the multicast group.
		socket_.set_option(boost::asio::ip::multicast::join_group(multicast_address));

		do_receive();
	}

private:
	void do_receive() {
		socket_.async_receive_from(
			boost::asio::buffer(data_),
			sender_endpoint_,
			[this](boost::system::error_code ec, std::size_t length) {
				if (!ec) {
//					std::cout.write(data_.data(), length) << '\n';
					auto result = scn::scan<std::string, int>(std::string_view{data_.data(), length}, "{} {}");
					auto [token, n] = result->values();
					spdlog::info("{}", n);

					do_receive();
				}
			});
	}

	boost::asio::ip::udp::socket socket_;
	boost::asio::ip::udp::endpoint sender_endpoint_;
	std::array<char, 1024> data_;
};

int main(int argc, char* argv[]) {
	try {
		if (argc != 3) {
			spdlog::info("Usage: receiver <listen_address> <multicast_address>");
			spdlog::info("  For IPv4, try:");
			spdlog::info("	receiver 0.0.0.0 239.255.0.1");
			spdlog::info("  For IPv6, try:");
			spdlog::info("	receiver 0::0 ff31::8000:1234");
			return 1;
		}

		boost::asio::io_context io_context;
		receiver r(io_context,
			boost::asio::ip::make_address(argv[1]),
			boost::asio::ip::make_address(argv[2]));
		io_context.run();
	}
	catch (std::exception& e) {
		spdlog::error("Exception: {}", e.what());
	}
}
