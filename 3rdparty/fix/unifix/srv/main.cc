#include "Application.h"

#include <quickfix/FileStore.h>
#include <quickfix/SocketAcceptor.h>

int main() {
	auto file = "acceptor-session1.ini";
	FIX::SessionSettings settings(file);

	Application application;
	FIX::FileStoreFactory storeFactory(settings);
	FIX::ScreenLogFactory logFactory(settings);

	FIX::SocketAcceptor acceptor(application, storeFactory, settings, logFactory);
	acceptor.start();
	sleep(3600);
	acceptor.stop();
}
