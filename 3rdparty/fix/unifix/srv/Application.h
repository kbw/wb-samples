#pragma once

#include "IDGenerator.h"
#include "OrderMatcher.h"

#include <quickfix/Application.h>
#include <quickfix/MessageCracker.h>
#include <quickfix/Session.h>

//#include <quickfix/fix40/NewOrderSingle.h>
//#include <quickfix/fix41/NewOrderSingle.h>
#include <quickfix/fix42/NewOrderSingle.h>
//#include <quickfix/fix43/NewOrderSingle.h>
//#include <quickfix/fix44/NewOrderSingle.h>
//#include <quickfix/fix50/NewOrderSingle.h>

#include <quickfix/fix42/MarketDataRequest.h>
//#include <quickfix/fix43/MarketDataRequest.h>
//#include <quickfix/fix44/MarketDataRequest.h>
//#include <quickfix/fix50/MarketDataRequest.h>

#include <quickfix/fix42/OrderCancelRequest.h>

#include <spdlog/spdlog.h>

#include <sstream>

class Application : public FIX::Application, public FIX::MessageCracker {
public:
	Application() : m_orderID(0), m_execID(0) {
		spdlog::info("Application");
	}

	~Application() {
		spdlog::info("~Application");
	}

	// Application overloads
	void onCreate(const FIX::SessionID &);
	void onLogon(const FIX::SessionID &sessionID);
	void onLogout(const FIX::SessionID &sessionID);
	void toAdmin(FIX::Message &, const FIX::SessionID &);
	void toApp(FIX::Message &, const FIX::SessionID &) EXCEPT(FIX::DoNotSend);
	void fromAdmin(const FIX::Message &, const FIX::SessionID &)
	    EXCEPT(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::RejectLogon);
	void fromApp(const FIX::Message &message, const FIX::SessionID &sessionID)
	    EXCEPT(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::UnsupportedMessageType);

	// MessageCracker overloads
	//	void onMessage(const FIX40::NewOrderSingle&, const FIX::SessionID&);
	//	void onMessage(const FIX41::NewOrderSingle&, const FIX::SessionID&);
	void onMessage(const FIX42::NewOrderSingle &, const FIX::SessionID &);
	//	void onMessage(const FIX43::NewOrderSingle&, const FIX::SessionID&);
	//	void onMessage(const FIX44::NewOrderSingle&, const FIX::SessionID&);
	//	void onMessage(const FIX50::NewOrderSingle&, const FIX::SessionID&);

	//	void onMessage(const FIX40::OrderCancelRequest&, const FIX::SessionID&);
	//	void onMessage(const FIX41::OrderCancelRequest&, const FIX::SessionID&);
	void onMessage(const FIX42::OrderCancelRequest &, const FIX::SessionID &);
	//	void onMessage(const FIX43::OrderCancelRequest&, const FIX::SessionID&);
	//	void onMessage(const FIX44::OrderCancelRequest&, const FIX::SessionID&);
	//	void onMessage(const FIX50::OrderCancelRequest&, const FIX::SessionID&);

	//	void onMessage(const FIX40::MarketDataRequest&, const FIX::SessionID&);
	//	void onMessage(const FIX41::MarketDataRequest&, const FIX::SessionID&);
	void onMessage(const FIX42::MarketDataRequest &, const FIX::SessionID &);
	//	void onMessage(const FIX43::MarketDataRequest&, const FIX::SessionID&);
	//	void onMessage(const FIX44::MarketDataRequest&, const FIX::SessionID&);
	//	void onMessage(const FIX50::MarketDataRequest&, const FIX::SessionID&);

	// Order functionality
	void processOrder(const Order &);
	void processCancel(const std::string &id, const std::string &symbol, Order::Side);

	void updateOrder(const Order &, char status);
	void rejectOrder(const Order &order) {
		updateOrder(order, FIX::OrdStatus_REJECTED);
	}
	void acceptOrder(const Order &order) {
		updateOrder(order, FIX::OrdStatus_NEW);
	}
	void fillOrder(const Order &order) {
		updateOrder(order, order.isFilled() ? FIX::OrdStatus_FILLED : FIX::OrdStatus_PARTIALLY_FILLED);
	}
	void cancelOrder(const Order &order) {
		updateOrder(order, FIX::OrdStatus_CANCELED);
	}

	void rejectOrder(const FIX::SenderCompID &, const FIX::TargetCompID &, const FIX::ClOrdID &clOrdID, const FIX::Symbol &symbol,
	                 const FIX::Side &side, const std::string &message);

	// Type conversions
	Order::Side convert(const FIX::Side &);
	Order::Type convert(const FIX::OrdType &);
	FIX::Side convert(Order::Side);
	FIX::OrdType convert(Order::Type);

	std::string genOrderID() {
		std::stringstream stream;
		stream << ++m_orderID;
		spdlog::info("getOrderID: {}", m_orderID);
		return stream.str();
	}

	std::string genExecID() {
		std::stringstream stream;
		stream << ++m_execID;
		spdlog::info("getExecID: {}", m_execID);
		return stream.str();
	}

private:
	int m_orderID, m_execID;

	OrderMatcher m_orderMatcher;
	IDGenerator m_generator;
};
