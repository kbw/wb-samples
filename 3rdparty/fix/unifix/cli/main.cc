#include "Application.h"

#include <quickfix/FileStore.h>
#include <quickfix/SocketInitiator.h>

int main() {
	auto file = "initiator-session1.ini";
	FIX::SessionSettings settings(file);

	Application application;
	FIX::FileStoreFactory storeFactory(settings);
	FIX::ScreenLogFactory logFactory(settings);

	FIX::SocketInitiator initiator(application, storeFactory, settings, logFactory);
	initiator.start();
	sleep(10);
	initiator.stop();
}
