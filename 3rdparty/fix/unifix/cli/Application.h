#pragma once

#include <quickfix/Application.h>
#include <quickfix/MessageCracker.h>
#include <quickfix/Session.h>

//#include <quickfix/fix40/NewOrderSingle.h>
//#include <quickfix/fix41/NewOrderSingle.h>
#include <quickfix/fix42/NewOrderSingle.h>
//#include <quickfix/fix43/NewOrderSingle.h>
//#include <quickfix/fix44/NewOrderSingle.h>
//#include <quickfix/fix50/NewOrderSingle.h>

#include <spdlog/spdlog.h>

#include <sstream>

class Application : public FIX::Application, public FIX::MessageCracker {
public:
	Application() : m_orderID(0), m_execID(0) {
		spdlog::info("Application");
	}

	~Application() {
		spdlog::info("~Application");
	}

	// Application overloads
	void onCreate(const FIX::SessionID &);
	void onLogon(const FIX::SessionID &sessionID);
	void onLogout(const FIX::SessionID &sessionID);
	void toAdmin(FIX::Message &, const FIX::SessionID &);
	void toApp(FIX::Message &, const FIX::SessionID &) EXCEPT(FIX::DoNotSend);
	void fromAdmin(const FIX::Message &, const FIX::SessionID &)
	    EXCEPT(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::RejectLogon);
	void fromApp(const FIX::Message &message, const FIX::SessionID &sessionID)
	    EXCEPT(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::UnsupportedMessageType);

	// MessageCracker overloads
	//	void onMessage(const FIX40::NewOrderSingle&, const FIX::SessionID&);
	//	void onMessage(const FIX41::NewOrderSingle&, const FIX::SessionID&);
	void onMessage(const FIX42::NewOrderSingle &, const FIX::SessionID &);
	//	void onMessage(const FIX43::NewOrderSingle&, const FIX::SessionID&);
	//	void onMessage(const FIX44::NewOrderSingle&, const FIX::SessionID&);
	//	void onMessage(const FIX50::NewOrderSingle&, const FIX::SessionID&);

	std::string genOrderID() {
		std::stringstream stream;
		stream << ++m_orderID;
		spdlog::info("getOrderID: {}", m_orderID);
		return stream.str();
	}

	std::string genExecID() {
		std::stringstream stream;
		stream << ++m_execID;
		spdlog::info("getExecID: {}", m_execID);
		return stream.str();
	}

private:
	int m_orderID, m_execID;
};
