#include "date/date.h"

#include <exception>
#include <iostream>
	
int main() {
	using namespace date;
	using namespace std::chrono;

	constexpr auto z1 = 2015_y/March/22;
	constexpr auto z2 = March/22/2015;
	constexpr auto z3 = 22_d/March/2015;

	unsigned y = 2015;
	unsigned m = 3;
	unsigned d = 22;
	auto x1 = year{y}/m/d;
	auto x2 = month{m}/d/y;
	auto x3 = day{d}/m/y;

	auto x = m/day{d}/y;

	// last
	constexpr auto y1 = 2015_y/February/last;
	constexpr auto y2 = February/last/2015;
	constexpr auto y3 = last/February/2015;

	// index
	constexpr auto w1 = 2015_y/March/Sunday[4];
	constexpr auto w2 = March/Sunday[4]/2015;
	constexpr auto w3 = Sunday[4]/March/2015;
}
