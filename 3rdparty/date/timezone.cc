#include "date/date.h"
#include "date/tz.h"

#include <locale>
#include <exception>
#include <string_view>
#include <iostream>
	
int main() {
	using namespace date;
	using namespace std::chrono;
	
	{
		auto t = make_zoned(current_zone(), system_clock::now());
		std::cout << t << '\n';
	}
	{
		auto t = make_zoned(current_zone(), floor<milliseconds>(system_clock::now()));
		std::cout << t << '\n';  // as ms
	}
	{
		auto t = make_zoned(current_zone(), floor<seconds>(system_clock::now()));
		std::cout << t << '\n'; // as s
	}
	{
		auto t = make_zoned(current_zone(), system_clock::now());
		std::cout << format("%a, %b %d, %Y at %I:%M %p %Z", t) << '\n'; // custom
	}
	try {
		auto t = make_zoned(current_zone(), floor<seconds>(system_clock::now()));
		std::cout << format(std::locale("de_DE"), "%a, %b %d, %Y at %T %Z", t) << '\n'; // use OS locale
	}
	catch (const std::exception& e) {
		std::clog << "fatal using OS locale: " << e.what() << '\n';
	}
	try {
		std::string_view name{"Europe/Berlin"};
		auto zone = locate_zone(name);
		auto t = make_zoned(zone, floor<seconds>(system_clock::now()));
		std::cout << format(std::locale("de_DE"), "%a, %b %d, %Y at %T %Z", t) << '\n'; // named locale
	}
	catch (const std::exception& e) {
		std::clog << "fatal using named locale: " << e.what() << '\n';
	}
}
