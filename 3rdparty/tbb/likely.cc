// Use branch prediction to improve speed.
//
// Compile with optimizations on.
//
// Test on Intel Xeon 64 HP G6 with gcc:
// The hit code runs ~%20 faster than the miss or default code, which take equal
// time.
//
// Test on ARM32 with clang:
// The hit and default code ran at the same speed, while the miss code was slower
// by ~%65.
//
// How does it perform on your system?
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

enum strategy_t { hit, miss, none };
strategy_t to_value(const char* s);

template <typename T>
T f(T n) {
	return n + 1;
}

template <typename T>
bool alwaysTrue(const T*) {
	return true;
}
 
void test(long niters, const char* strategy_str);
void test2(long niters, const char* strategy_str);
void usage();

int main(int argc, char* argv[]) {
	if (argc == 2 && !strcmp(argv[1], "--help")) {
		usage(); return 1;
	}

	long niters = (argc > 1) ? atol(argv[1]) : 1000;
	const char* strategy_str = (argc > 2) ? argv[2] : "hit";

	test2(1000*niters, strategy_str);
	test(1000*niters, strategy_str);
}

void test2(const long, const char* strategy_str) {
	const strategy_t strategy = to_value(strategy_str);

	long m[] = { 0, 0 };

	switch (strategy) {
	case hit:
		if (__builtin_expect(1, alwaysTrue(m)))
			printf("hit\n");
		else
			printf("miss\n");
		break;

	case miss:
		if (__builtin_expect(0, alwaysTrue(m)))
			printf("hit\n");
		else
			printf("miss\n");
		break;

	default:
		if (alwaysTrue(m))
			printf("hit\n");
		else
			printf("miss\n");
	}
}

void test(const long niters, const char* strategy_str) {
	const strategy_t strategy = to_value(strategy_str);

	long m[] = { 0, 0 };

	for (long i = 0; i < niters; ++i)
		switch (strategy) {
		case hit:
			if (__builtin_expect(1, alwaysTrue(m)))
				m[0] = f(m[0]);
			else
				m[1] = f(m[1]);
			break;

		case miss:
			if (__builtin_expect(0, alwaysTrue(m)))
				m[0] = f(m[0]);
			else
				m[1] = f(m[1]);
			break;

		default:
			if (alwaysTrue(m))
				m[0] = f(m[0]);
			else
				m[1] = f(m[1]);
		}

	printf("%3.2f%% hit, %3.2f%% miss\n", 100.0*m[0]/(double)niters, 100.0*m[1]/(double)niters);
}

strategy_t to_value(const char* s) {
	if (!strcmp(s, "hit"))	return hit;
	if (!strcmp(s, "miss"))	return miss;
	return none;
}

void usage() {
	const char* msgs[] = {
		"likeky - demonstrates branch prediction",
		"likely [iters] [strategy]",
		"  iters - iters x 1000 iterations used",
		"  strategy - strategies are \"hit\", \"miss\".",
		"    Default is \"hit\", another other value skips branch prediction"
	};

	for (auto msg : msgs)
		printf("%s\n", msg);
}
