#include <tbb/tbb.h>
#include <numeric>
#include <algorithm>
#include <iostream>

int main(int argc, char* argv[]) {
	int sz = argc > 1 ? atoi(argv[1]) : 4;
	std::vector<double> n(sz);
	std::generate(n.begin(), n.end(), [i = 0]() mutable { return i++; });

	tbb::parallel_for(
		size_t(0), n.size(),
		[&n](size_t i) { n[i] = n[i]*n[i]; });

//	for (auto val : n) std::cout << val << ' ';
//	std::cout << ": " << std::accumulate(n.begin(), n.end(), 0.0) << std::endl;
}
