#include <stdlib.h>
#include <wykobi/wykobi.hpp>	// needs stdlib.h
#include <wykobi/wykobi_algorithm.hpp>
#include <wykobi/wykobi_instantiate.hpp>

#include <mpfr_real/real.hpp>

#include <vector>
#include <stdio.h>

typedef mpfr::real<4096> real_t;

template <typename T>
class point_list_t : public std::vector<wykobi::point2d<T>> {};

template <typename T>
point_list_t<T> create_point_list(T x, T y, T dx, T dy, std::size_t max_points)
{
	point_list_t<T> point_list;
	point_list.reserve(max_points);

	wykobi::generate_random_points<T>(
		x, y, dx, dy, max_points,
		std::back_inserter(point_list));

	return point_list;
}

int main(int argc, char* argv[])
{
	const std::size_t max_points = argc > 1 ? atoi(argv[1]) : 100000;
	real_t width = 5;
	real_t height = 10;
	point_list_t<real_t> point_list = create_point_list<real_t>(0.0, 0.0, width, height, max_points);

	wykobi::polygon<real_t, 2> convex_hull;
	wykobi::algorithm::convex_hull_graham_scan<wykobi::point2d<real_t>>(
		point_list.begin(), point_list.end(),
		std::back_inserter(convex_hull));
	printf("convex hull polygon has %zu sides\n", convex_hull.size());
}
