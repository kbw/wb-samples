#include <stdlib.h>
#include <wykobi/wykobi.hpp>
#include <mpfr_real/real.hpp>

typedef mpfr::real<4096> real_t;

template <typename T = real_t>
class point2d : public wykobi::geometric_entity {};

template <typename T = real_t>
class point3d : public wykobi::geometric_entity {};

template <typename T = real_t, std::size_t Dimension = 1>
class pointnd : public wykobi::geometric_entity {};

int main()
{
	wykobi::point2d<real_t>();
	wykobi::point3d<real_t>();
	wykobi::pointnd<real_t, 10>();
}
