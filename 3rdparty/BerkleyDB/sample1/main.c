#include <db.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define DATABASE "access.bdb"

int main()
{
	DB *dbp;
	DBT key, data;
	int ret;

	if ((ret = db_create(&dbp, NULL, 0)) != 0)
	{
		fprintf(stderr, "db_create: %s\n", db_strerror(ret));
		exit(1);
	}

	if ((ret = dbp->open(dbp, NULL, DATABASE, NULL, DB_BTREE, DB_CREATE, 0664)) != 0)
	{
		dbp->err(dbp, ret, "%s", DATABASE);
		goto err;
	} 

	memset(&key, 0, sizeof(key));
	memset(&data, 0, sizeof(data));
	key.data = (char*)"fruit";
	key.size = sizeof("fruit");
	data.data = (char*)"apple";
	data.size = sizeof("apple");

	if ((ret = dbp->put(dbp, NULL, &key, &data, 0)) == 0)
	{
		printf("db: %s: key stored.\n", (char *)key.data);
	}
	else
	{
		dbp->err(dbp, ret, "DB->put");
		goto err;
	} 

	if ((ret = dbp->put(dbp, NULL, &key, &data, DB_NOOVERWRITE)) == 0)
	{
		printf("db: %s: key stored.\n", (char *)key.data);
	}
	else
	{
		dbp->err(dbp, ret, "DB->put");
		goto err;
	}

	switch (ret = dbp->put(dbp, NULL, &key, &data, DB_NOOVERWRITE))
	{
	case 0:
		printf("db: %s: key stored.\n", (char *)key.data);
		break;
	case DB_KEYEXIST:
		printf("db: %s: key previously stored.\n", (char *)key.data);
		break;
	default:
		dbp->err(dbp, ret, "DB->put");
		goto err;
	}

err:
	dbp->close(dbp, 0);

	return 0;
}
