#include <exprtk/exprtk.hpp>

#include <cstdio>
#include <iostream>
#include <string>
#include <tuple>

template <typename T>
void evaluate(std::tuple<T, T, T> range, const std::string& expression_string)
{
	typedef exprtk::symbol_table<T> symbol_table_t;
	typedef exprtk::expression<T>	expression_t;
	typedef exprtk::parser<T>		 parser_t;

	T x;

	symbol_table_t symbol_table;
	symbol_table.add_variable("x", x);
	symbol_table.add_constants();

	expression_t expression;
	expression.register_symbol_table(symbol_table);

	parser_t parser;
	parser.compile(expression_string, expression);

	for (x = std::get<0>(range); x <= std::get<1>(range); x += std::get<2>(range))
	{
		const T y = expression.value();
		printf("%19.15f\t%19.15f\n", x, y);
	}
}

int main(int argc, char* argv[])
{
	std::string expression_string = "x*(3*x+3) - 1";
//							"clamp(-1.0, sin(2 * pi * x) + cos(x / 2 * pi), +1.0)";
	if (argc > 1)
		expression_string = argv[1];

	double min{}, max{}, step{};
	std::cout << "min: "; std::cin >> min;
	std::cout << "max: "; std::cin >> max;
	std::cout << "step: "; std::cin >> step;

	evaluate<double>({min, max, step}, expression_string);
}
