// actor maintains known-actors list
//
// -----------                ----------
// | hello   | -- create -->  | mirror |
// -----------           |    ----------
//                       |    --------------
//                       |->  | factorials |
//                       |    --------------
//                       |    ----------
//                       |->  | primes |
//                            ----------
//
#include <caf/actor_ostream.hpp>
#include <caf/actor_system.hpp>
#include <caf/caf_main.hpp>
#include <caf/event_based_actor.hpp>

#include <spdlog/spdlog.h>

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std::literals;

using actors_type = std::unordered_map<std::string, caf::actor>;
using integer_type = std::uint64_t;

caf::behavior primes(caf::event_based_actor* self) {
	auto gen_primes = [](size_t mx) -> std::vector<integer_type> {
		std::vector<integer_type> values;

		std::vector<bool> flags(mx - 2, true);
		for (size_t i = 2; i < mx; ++i) {
			if (flags[i - 2]) {
				values.push_back(i);
				for (size_t j = i; j < mx; j += i)
					flags[j - 2] = false;
			}
		}

		return values;
	};

	const std::vector<integer_type> values = gen_primes(100);
	spdlog::info("values.size={}", values.size());

	auto fetch_prime = [values](size_t idx) -> std::optional<integer_type> {
		spdlog::info("values.size={}", values.size());
		if (idx < values.size())
			return values[idx];
		return {};
	};

	return {
		[self, values, fetch_prime](size_t idx) -> integer_type {
			spdlog::info("values.size={}", values.size());
//			std::optional<integer_type> n = fetch_prime(values, idx);
			std::optional<integer_type> n = fetch_prime(idx);
//			self->println("{}", n);
			return n.value_or(0);
		},
	};
}

caf::behavior factorials(caf::event_based_actor* self) {
	std::vector<integer_type> values{1, 1};

	std::function<integer_type(size_t)> fetch_factorial = [&](size_t idx) -> integer_type {
		std::function<integer_type(size_t)> impl = [&](size_t idx) -> integer_type {
			if (idx < values.size())
				return values[idx];

			integer_type value = idx * impl(idx - 1);
			values.push_back(value);
			return values[idx];
		};
		return impl(idx);
	};

	return {
		[&](size_t idx) -> integer_type {
			integer_type value = fetch_factorial(idx);
//			self->println("{}", value);
			return value;
		},
	};
}

caf::behavior mirror(caf::event_based_actor* self) {
	return {
		[self](const std::string& what) -> std::string {
//			self->println("{}", what);
			return std::string{what.rbegin(), what.rend()};
		},
	};
}

void hello_world(caf::event_based_actor* self) {
	actors_type known_actors;
	known_actors.emplace("mirror", self->spawn(mirror));
	known_actors.emplace("factorials", self->spawn(factorials));
	known_actors.emplace("primes", self->spawn(primes));

	self->mail("Hello World!")
		.request(known_actors["mirror"], 10s)
		.then([self](const std::string& what) {
			self->println("mirror -> {}", what);
		});
	self->mail(size_t(3))
		.request(known_actors["factorials"], 10s)
		.then([self](integer_type what) {
			self->println("factorials:{} -> {}", 3, what);
		});
	self->mail(size_t(12))
		.request(known_actors["primes"], 10s)
		.then([self](integer_type what) {
			self->println("primes:{} -> {}", 1, what);
		});
}

void caf_main(caf::actor_system& sys) {
	sys.spawn(hello_world);
}

CAF_MAIN()
