// hello creates mirror before using it
//
// -----------                ----------
// | hello   | -- create -->  | mirror |
// -----------                ----------
//
#include <caf/actor_ostream.hpp>
#include <caf/actor_system.hpp>
#include <caf/caf_main.hpp>
#include <caf/event_based_actor.hpp>

#include <iostream>
#include <string>

using namespace std::literals;

caf::behavior mirror(caf::event_based_actor* self) {
	return {
		[self](const std::string& what) -> std::string {
			self->println("{}", what);
			return std::string{what.rbegin(), what.rend()};
		},
	};
}

void hello_world(caf::event_based_actor* self) {
	auto buddy = self->spawn(mirror);

	self->mail("Hello World!")
		.request(buddy, 10s)
		.then(
			[self](const std::string& what) {
				self->println("{}", what);
			});
}

void caf_main(caf::actor_system& sys) {
	sys.spawn(hello_world);
}

CAF_MAIN()
