// actor maintains known-actors list
//
// -----------                ----------
// | hello   | -- create -->  | mirror |
// -----------                ----------
//
#include <caf/actor_ostream.hpp>
#include <caf/actor_system.hpp>
#include <caf/caf_main.hpp>
#include <caf/event_based_actor.hpp>

#include <iostream>
#include <string>
#include <unordered_map>

using namespace std::literals;

using actors_type = std::unordered_map<std::string, caf::actor>;

caf::behavior mirror(caf::event_based_actor* self) {
	return {
		[self](const std::string& what) -> std::string {
			self->println("{}", what);
			return std::string{what.rbegin(), what.rend()};
		},
	};
}

void hello_world(caf::event_based_actor* self) {
	actors_type known_actors;
	known_actors.emplace("mirror", self->spawn(mirror));

	self->mail("Hello World!")
		.request(known_actors["mirror"], 10s)
		.then(
			[self](const std::string& what) {
				self->println("{}", what);
			});
}

void caf_main(caf::actor_system& sys) {
	sys.spawn(hello_world);
}

CAF_MAIN()
