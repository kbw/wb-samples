# caf samples
Sample programs using the C++ Actor Framework

## hello_world
The caf hello world sample

## hello2
actor creates another actor and uses it

## hello3
when actor creates another actor, it saves it in a dictionary for reuse

## hello4
create a bunch of actors and use them
