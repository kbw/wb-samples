#include <pipeline.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int do_main(void);

int main(int argc, char* argv[]) {
	if (argc == 1) {
		return do_main();
	}
	return 1;
}

int do_main(void) {
	pipecmd* cmd = pipecmd_new_args("ls", "-lrt", NULL);
	if (cmd) {
		char* cmdstr = pipecmd_tostring(cmd);
		if (cmdstr) {
			printf("cmd: %s\n", cmdstr);
			free(cmdstr);
		}

		pipecmd_free(cmd);
		return 0;
	}
	return 1;
}
