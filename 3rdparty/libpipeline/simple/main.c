#include <pipeline.h>
#include <stdio.h>
#include <string.h>

#define FLAG_FAIL 1

int do_main(int flags);

int main(int argc, char* argv[]) {
	int flags = 0;
	if (argc == 2  &&  strcmp(argv[1], "--fail") == 0) {
		flags = FLAG_FAIL;
	}

	if (argc == 1  ||  flags) {
		return do_main(flags);
	}
	return 1;
}

int do_main(int flags) {
	pipeline* p = pipeline_new();
	if (p) {
		pipeline_command_args(p, "ls", "-l", "-r", "-t", NULL);
//		pipeline_command_args(p, "cut", "-d", " ", "-f", "9-", NULL);
		pipeline_command_args(p, "cut", "-b", "48-", NULL);
		pipeline_command_args(p, "tail", "-n", "1", NULL);
		if (flags & FLAG_FAIL)
			pipeline_command_args(p, "unknown", NULL);

		pipeline_start(p);
		int ret = pipeline_wait(p);
		if (ret != 0)
			fprintf(stderr, "pipeline ret=%d\n", ret);

		pipeline_free(p);
		return ret;
	}
	return 1;
}
