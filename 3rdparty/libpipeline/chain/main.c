#include <unistd.h>
#include <pipeline.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSZ (16 * 1024)
static char buf[BUFSZ];

int do_parent(int nchildren);
int do_child(void);

int main(int argc, char* argv[]) {
	int nchildren = 0;
	if (argc == 2  &&  (nchildren = atoi(argv[1]))) {
		return do_parent(nchildren);
	}
	return do_child();
}

int do_parent(int nchildren) {
	int topipe[2];
	if (pipe(topipe) == -1) {
		return errno;
	}
	int frompipe[2];
	if (pipe(frompipe) == -1) {
		return errno;
	}

	pipeline* p = pipeline_new();
	int ret = errno;
	if (p) {
		pipeline_want_in(p, topipe[0]);
		pipeline_want_out(p, frompipe[1]);

		int i;
		for (i = 0; i < nchildren; ++i)
			pipeline_command_args(p, "./prog", NULL);

		pipeline_start(p); {
			int nbytes = snprintf(buf, BUFSZ, "%d", getpid());
			write(topipe[1], buf, nbytes);
		}
		ret = pipeline_wait(p); {
			int nbytes = read(frompipe[0], buf, BUFSZ);
			buf[nbytes++] = '\n';
			write(STDOUT_FILENO, buf, nbytes);
		}
		pipeline_free(p);
	}
	close(frompipe[0]);
	close(frompipe[1]);
	close(topipe[0]);
	close(topipe[1]);
	return ret;
}

int do_child(void) {
	int nbytes = read(STDIN_FILENO, buf, sizeof(buf));
	if (nbytes == -1)
		return -1;

	nbytes += snprintf(buf + nbytes, BUFSZ - nbytes, ":%d", getpid());

	int ret = write(STDOUT_FILENO, buf, nbytes) == nbytes  ? 0 : -1;
	return ret;
}
