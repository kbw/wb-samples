#pragma once

#include "filename.hpp"
#include "md5.hpp"
#include <sys/types.h>
#include <tuple>
#include <map>

struct options_t;

//---------------------------------------------------------------------------
/*
typedef std::tuple<dev_t, ino_t, nlink_t, off_t, filename_t> file_info_t;

inline file_info_t make_fileinfo(dev_t dev, ino_t ino, nlink_t nlink, off_t size, std::string name) {
	return std::make_tuple(dev, ino, nlink, size, name);
}

inline dev_t&       file_inode(file_info_t& info)	{ return std::get<0>(info); }
inline ino_t&       file_inode(file_info_t& info)	{ return std::get<1>(info); }
inline nlink_t&     file_links(file_info_t& info)	{ return std::get<2>(info); }
inline off_t&       file_size(file_info_t& info)	{ return std::get<3>(info); }
inline time_t&      file_size(file_info_t& info)	{ return std::get<4>(info); }
inline filename_t&  file_name(file_info_t& info)	{ return std::get<5>(info); }

inline dev_t        file_inode(const file_info_t& info)	{ return std::get<0>(info); }
inline ino_t        file_inode(const file_info_t& info)	{ return std::get<1>(info); }
inline nlink_t      file_links(const file_info_t& info)	{ return std::get<2>(info); }
inline off_t        file_size(const file_info_t& info)	{ return std::get<3>(info); }
inline time_t       file_size(const file_info_t& info)	{ return std::get<4>(info); }
inline filename_t   file_name(const file_info_t& info)	{ return std::get<5>(info); }
 */
//---------------------------------------------------------------------------

struct file_info_t
{
	file_info_t(dev_t dev, ino_t ino, nlink_t nlink, off_t size, time_t mtime, const std::string &name);

	dev_t	dev;
	ino_t	inode;
	nlink_t	nlink;
	off_t	size;
	time_t	mtime;
	filename_t name;
};

inline file_info_t::file_info_t(dev_t dev, ino_t ino, nlink_t nlink, off_t size, time_t mtime, const std::string &name) :
	dev(dev), inode(ino), nlink(nlink), size(size), mtime(mtime), name(name)
{
}

inline bool operator<(const file_info_t& a, const file_info_t& b) {
	if (a.dev < b.dev) return true;
	if (a.dev == b.dev && a.inode < b.inode) return true;
	if (a.dev == b.dev && a.inode == b.inode) return a.name < b.name;
	return false;
}

inline file_info_t make_fileinfo(dev_t dev, ino_t ino, nlink_t nlink, off_t size, time_t mtime, std::string name) {
	return file_info_t(dev, ino, nlink, size, mtime, name);
}

inline dev_t&		file_dev(file_info_t& info)		{ return info.dev; }
inline ino_t&		file_inode(file_info_t& info)		{ return info.inode; }
inline nlink_t&		file_links(file_info_t& info)		{ return info.nlink; }
inline off_t&		file_size(file_info_t& info)		{ return info.size; }
inline time_t&		file_mtime(file_info_t& info)		{ return info.mtime; }
inline filename_t&	file_name(file_info_t& info)		{ return info.name; }

inline dev_t		file_dev(const file_info_t& info)	{ return info.dev; }
inline ino_t		file_inode(const file_info_t& info)	{ return info.inode; }
inline nlink_t		file_links(const file_info_t& info)	{ return info.nlink; }
inline off_t		file_size(const file_info_t& info)	{ return info.size; }
inline time_t		file_mtime(const file_info_t& info)	{ return info.mtime; }
inline std::string	file_name(const file_info_t& info)	{ return info.name; }

//---------------------------------------------------------------------------

typedef std::multimap<md5_t, file_info_t> files_t;

void scan(files_t& files, options_t& opts, const std::string& name);
void show(const files_t& files, options_t& opts);
