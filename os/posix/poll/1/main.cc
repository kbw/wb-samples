#include <unistd.h>
#include <sys/stat.h>
#include <poll.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
	struct pollfd items[] = {
		{ open("in", O_RDONLY), POLLIN, 0 },
		{ creat("out", S_IRUSR | S_IWUSR), POLLOUT, 0 }
	};
	int nitems = 2;

	char buf[8*128];
	int n = 0;
	do {
		int rc = poll(items, nitems, 10*1000);
		fprintf(stderr, "poll:%d i:0 fd:%d revents:0x%02x\n", rc, items[0].fd, items[0].revents);
		fprintf(stderr, "poll:%d i:1 fd:%d revents:0x%02x\n", rc, items[1].fd, items[1].revents);

		for (int i = 0; i < nitems; ++i) {
			if (items[i].revents & POLLIN) {
				n = read(items[i].fd, buf, sizeof(buf));
				fprintf(stderr, "read %d bytes\n", n);
			}
			if (n > 0 && items[i].events & POLLOUT) {
				n = write(items[i].fd, buf, n);
				fprintf(stderr, "wrote %d bytes\n", n);
			}
		}
	}
	while (n > 0);

	for (int i = 0; i < nitems; close(items[i++].fd))
		;
}
