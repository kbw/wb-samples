#include <unistd.h>
#include <poll.h>

#include <assert.h>
#include <stdio.h>

#include <string>
#include <vector>

std::vector<struct pollfd> create();
std::string pollevent_to_txt(short event);

int main()
{
	std::vector<struct pollfd> pollset = create();
	assert(pollset.size() == 2);

	const int bufsz = 32;
	char buf[bufsz];
	int sz = 0;

	for (int loop = 0; ; ++loop) {
		bool want_read = sz == 0;
		bool want_send = sz != 0;

		pollset[0].events = want_read ? POLLIN  : 0, pollset[0].revents = 0;
		pollset[1].events = want_send ? POLLOUT : 0, pollset[1].revents = 0;

		int rc = poll(&pollset.front(), pollset.size(), -1);
		fprintf(stderr, "%d: rc=%d pollset[0].revents=%s pollset[1].revents=%s sz=%d\n",
			loop, rc, pollevent_to_txt(pollset[0].revents).c_str(), pollevent_to_txt(pollset[1].revents).c_str(), sz);
		if (rc == 0)
			break;

		if (want_read && (pollset[0].revents & POLLIN)) {
			rc = read(pollset[0].fd, buf, bufsz);
			if (rc <= 0) {
				perror("fatal: bad read");
				break;
			}
			sz = rc;
		}
		if (want_send && (pollset[1].revents & POLLOUT)) {
			rc = write(pollset[1].fd, buf, sz);
			if (rc == -1) {
				perror("fatal: bad write");
				break;
			}
			sz -= rc;
		}
	}
}

std::vector<struct pollfd> create()
{
	std::vector<struct pollfd> pollset {
		{ STDIN_FILENO, 0, 0 },
		{ STDOUT_FILENO, 0, 0 }
	};

	return pollset;
}

std::string pollevent_to_txt(short event)
{
	switch (event) {
	case 0:			return "<empty>";
	case POLLIN:	return "POLLIN ";
	case POLLOUT:	return "POLLOUT";
	case POLLERR:	return "POLLERR";
	case POLLIN|POLLOUT:	return "POLLIN|POLLOUT";
	case POLLIN|POLLERR:	return "POLLIN|POLLERR";
	case POLLOUT|POLLERR:	return "POLLOUT|POLLERR";
	default: {
			char buf[32];
			snprintf(buf, sizeof(buf), "%d", event);
			return buf;
		}
	}
}
