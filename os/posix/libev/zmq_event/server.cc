#include "zmq_event.hpp"

struct evt : public zmq_event
{
	using zmq_event::zmq_event;

	static const size_t m_bufsz = 1024;
	char m_buf[m_bufsz];
	size_t m_sz = 0;

	void write() { m_sz = socket().send(&m_buf[m_sz], m_bufsz); }
	void read()  { if (m_sz) { m_sz -= socket().recv(&m_buf[m_sz], m_sz); } }
};

int main()
{
	zmq::context_t ctx(1);
	evt ev(ctx, ZMQ_REQ, "tcp://*:6001", true);
}
