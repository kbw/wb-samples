#include <ev++.h>
#include <zmq.hpp>

#include <string>

class zmq_event
{
public:
	zmq_event(zmq::context_t& context, int type, const std::string& addr, bool dobind = false);
	virtual ~zmq_event() = default;

protected:
	// Function we are going to call to write to the ZeroMQ socket
	virtual void write() = 0;

	// Function we are going to call to read from the ZeroMQ socket
	virtual void read() = 0;

	virtual zmq::socket_t& socket() { return m_socket; }

private:
	// This gets fired before the event loop, to prepare
	void before(ev::prepare& prep, int revents);

	// This is fired after the event loop, but before any other type of events
	void after(ev::check& check, int revents);

	// We need to have a no-op function available for those events that we
	// want to add to the list, but should never fire an actual event
	template <typename T> void noop(T& w, int revents) {}

	// Some helper functions, one to start notifications
	void start_notify();

	// And one to stop notifications.
	void stop_notify();

private:
	// Our event types
	ev::io      m_watch_io;
	ev::prepare m_watch_prepare;
	ev::check   m_watch_check;
	ev::idle    m_watch_idle;

	// Our ZeroMQ socket
	zmq::socket_t m_socket;
	int           m_socket_fd = -1;
};
