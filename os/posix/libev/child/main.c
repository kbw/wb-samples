/*
 * https://gist.github.com/DamonHao/df87e727a0299c07fce3
 *
 * test_ev_child.c
 *
 *  Created on: Jun 2, 2014
 *      Author: damonhao
 */

#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h> // for puts

#include <ev.h>

ev_child cw;

static void child_cb(EV_P_ ev_child *w, int revents)
{
	ev_child_stop(EV_A_ w);
	printf("process %d exited with status %x\n", w->rpid, w->rstatus);
}

int main()
{
	pid_t pid = fork();

	if (pid < 0)
	{
		// error
		puts("fork error");
		return 0;
	}
	else if (pid == 0)
	{
		// the forked child executes here
		exit(1);
	}
	else
	{
		puts("in parent");
		struct ev_loop *loop = EV_DEFAULT;
		ev_child_init(&cw, child_cb, pid, 0);
		ev_child_start(loop, &cw);
		ev_run(loop, 0);
	}
	return 0;
}
