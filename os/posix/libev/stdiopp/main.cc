#include <ev++.h>
#include <iostream>
#include <unistd.h>

class IOWatcher
{
public:
	IOWatcher(int fd, int events)
	{
		m_io.set(fd, events);
		m_io.set<IOWatcher, &IOWatcher::CallBack>(this);
		m_io.start();
	}
	void CallBack(ev::io &w, int revents)
	{
		std::cout << "In IOWatcher::CallBack" << std::endl;
		w.stop();
	}
private:
	ev::io m_io;
};

int main()
{
	IOWatcher my_io(STDIN_FILENO, ev::READ);
	ev::default_loop().run(0);
}
