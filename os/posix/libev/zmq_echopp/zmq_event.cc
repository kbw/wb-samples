#include "zmq_event.hpp"
#include <stdexcept>

zmq_event::zmq_event(zmq::context_t& context, int type, const std::string& addr, bool dobind) :
	m_socket(context, type)
{
	dobind ? m_socket.bind(addr.c_str()) : m_socket.connect(addr.c_str());

	// Set up all of our watches
	m_watch_io.set(getfd(), ev::READ);						// IO watch check for READ on the ZeroMQ socket
	m_watch_io.set<zmq_event, &zmq_event::noop>(this);		// calls before() for edge trigger
	m_watch_prepare.set<zmq_event, &zmq_event::before>(this);// prepare watch to call the before() function
	m_watch_check.set<zmq_event, &zmq_event::after>(this);	// check watch to call the after() function
	m_watch_idle.set<zmq_event, &zmq_event::noop>(this);	// ignore on enter idle

	start_notify();
}

int zmq_event::getfd() {
	int socket_fd = -1;
	size_t fd_len = sizeof(socket_fd);
	m_socket.getsockopt(ZMQ_FD, &socket_fd, &fd_len);
	return socket_fd;
}

void zmq_event::before(ev::prepare&, int revents) {
	if (EV_ERROR & revents) { throw std::runtime_error("libev error"); }

	// Lucky for us, getting the events available doesn't invalidate the
	// events, so that calling this in `before()` and in `after()` will
	// give us the same results, so we don't need to keep zevents.
	uint32_t zevents = 0;
	size_t zevents_len = sizeof(zevents);
	m_socket.getsockopt(ZMQ_EVENTS, &zevents, &zevents_len);

	// Check what events exists, and check it against what event we want.
	// We "abuse" our watch_io.events for this information.
	// Act on edge trigger if found
	if ((zevents & ZMQ_POLLOUT) && (m_watch_io.events & ev::WRITE)) {
		m_watch_idle.start();
		return;
	}

	if ((zevents & ZMQ_POLLIN) && (m_watch_io.events & ev::READ)) {
		m_watch_idle.start();
		return;
	}

	// Spurious wakeup, or end of level trigger go back to sleep
	m_watch_io.start();
}

void zmq_event::after(ev::check&, int revents) {
	if (EV_ERROR & revents) { throw std::runtime_error("libev error"); }

	// Stop both the idle and the io watch, no point in calling the no-op callback
	// One of them will be reactived by before() on the next loop
	// If there's nothing to do, it's the end of level trigger, we go back to sleep in before()
	// If there's something to do, we'll level triggering in before()
	m_watch_idle.stop();
	m_watch_io.stop();

	// Get the events
	uint32_t zevents = 0;
	size_t zevents_len = sizeof(zevents);
	m_socket.getsockopt(ZMQ_EVENTS, &zevents, &zevents_len);

	// Check the events and call the users read/write function
	if ((zevents & ZMQ_POLLIN) && (m_watch_io.events & ev::READ))
		this->read();

	if ((zevents & ZMQ_POLLOUT) && (m_watch_io.events & ev::WRITE))
		this->write();
}

void zmq_event::start_notify() {
	m_watch_check.start();
	m_watch_prepare.start();
}

void zmq_event::stop_notify() {
	m_watch_check.stop();
	m_watch_prepare.stop();
}
