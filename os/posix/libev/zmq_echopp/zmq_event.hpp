#include <ev++.h>
#include <zmq.hpp>
#include <string>

class zmq_event
{
public:
	zmq_event(zmq::context_t& ctx, int type, const std::string& addr, bool dobind = false);
	virtual ~zmq_event() = default;

protected:
	virtual void write() = 0;	// Function we are going to call to write to the ZeroMQ socket
	virtual void read() = 0;	// Function we are going to call to read from the ZeroMQ socket
	virtual zmq::socket_t& socket() { return m_socket; }

private:
	void start_notify();	// Tell libev to start notifying us
	void stop_notify();		// Tell libev to stop notifying us

	void before(ev::prepare& prep, int revents);// This gets fired before the event loop, to prepare
	void after(ev::check& check, int revents);	// This is fired after the event loop, but before any other type of events
	template <typename T> void noop(T& w, int revents) {}	// ignore

	int getfd();

private:
	// Our event types
	ev::io      m_watch_io;
	ev::prepare m_watch_prepare;
	ev::check   m_watch_check;
	ev::idle    m_watch_idle;

	// Our ZeroMQ socket
	zmq::socket_t m_socket;
};
