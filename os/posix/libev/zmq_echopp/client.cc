#include "zmq.hpp"
#include <strings.h>
#include <string.h>
#include <stdio.h>

int main()
{
	zmq::context_t ctx(1);
	zmq::socket_t s(ctx, ZMQ_REQ);
	s.connect("tcp://127.0.0.1:6001");

	const size_t bufsz = 1024;
	char buf[bufsz] = "hello world";

	s.send(buf, strlen(buf) + 1);
	bzero(buf, bufsz);
	s.recv(buf, bufsz);
	printf("%s\n", buf);
}
