/*
 * https://gist.github.com/DamonHao/df87e727a0299c07fce3
 *
 * test_ev_once.c
 *
 *  Created on: Jun 2, 2014
 *      Author: damonhao
 */
/* receive io event or timeout. They may happen at the same time, 
you probably should give io events precedence.*/

#include <unistd.h>
#include <stdio.h> // for puts
#include <ev.h>

static void stdin_ready(int revents, void *arg)
{
	if (revents & EV_READ)
	{
		/* stdin might have data for us, joy! */
		puts("stdin io event");
	}
	else if (revents & EV_TIMER)
	{
		/* doh, nothing entered */
		puts("timeout");
	}
}

int main()
{
	puts("in main");
	struct ev_loop *loop = EV_DEFAULT;
	ev_once (loop, STDIN_FILENO, EV_READ, 5., stdin_ready, NULL);
	ev_run(loop, 0);
	return 0;
}
