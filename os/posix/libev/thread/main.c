/*
 * https://gist.github.com/DamonHao/df87e727a0299c07fce3
 *
 * test_libev_withthread.c
 *
 *  Created on: Jun 4, 2014
 *      Author: damonhao
 */

#include <ev.h>
#include <pthread.h>
#include <stdio.h>

typedef struct
{
	pthread_mutex_t lock; /* global loop lock */
	ev_async async_w;
	pthread_t tid;
	pthread_cond_t invoke_cv;
} userdata;

static void async_cb(EV_P_ ev_async *w, int revents)
{
	// just used for the side effects
}

static void l_release(EV_P)
{
	userdata *u = ev_userdata(EV_A);
	pthread_mutex_unlock(&u->lock);
}

static void l_acquire(EV_P)
{
	userdata *u = ev_userdata(EV_A);
	pthread_mutex_lock(&u->lock);
}

void *l_run(void *thr_arg)
{
	struct ev_loop *loop = (struct ev_loop *) thr_arg;

	l_acquire(EV_A);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, 0);
	ev_run(EV_A_ 0);
	l_release(EV_A);

	return 0;
}

static void l_invoke(EV_P)
{
	userdata *u = ev_userdata(EV_A);

	while (ev_pending_count(EV_A))
	{
//		wake_up_other_thread_in_some_magic_or_not_so_magic_way();
		pthread_cond_wait(&u->invoke_cv, &u->lock);
	}
}

void prepare_loop(EV_P)
{
	// for simplicity, we use a static userdata struct.
	static userdata u;

	ev_async_init(&u.async_w, async_cb);
	ev_async_start(EV_A_&u.async_w);

	pthread_mutex_init(&u.lock, 0);
	pthread_cond_init(&u.invoke_cv, 0);

	// now associate this with the loop
	ev_set_userdata(EV_A_ &u);
	ev_set_invoke_pending_cb(EV_A_ l_invoke);
	ev_set_loop_release_cb(EV_A_ l_release, l_acquire);

	// then create the thread running ev_run
	pthread_create(&u.tid, 0, l_run, EV_A);
}

static void real_invoke_pending(EV_P)
{
	userdata *u = ev_userdata(EV_A);

	pthread_mutex_lock(&u->lock);
	ev_invoke_pending(EV_A);
	pthread_cond_signal(&u->invoke_cv);
	pthread_mutex_unlock(&u->lock);
}

static void timeout_cb (EV_P_ ev_timer *w, int revents)
{
	puts ("timeout");
	// this causes the innermost ev_run to stop iterating
	ev_break (EV_A_ EVBREAK_ONE);
}

int main()
{
	struct ev_loop *loop = EV_DEFAULT;

	ev_timer timeout_watcher;
	userdata *u = ev_userdata(EV_A);
	ev_timer_init(&timeout_watcher, timeout_cb, 5.5, 0.);

	pthread_mutex_lock(&u->lock);
	ev_timer_start(EV_A_ &timeout_watcher);
	ev_async_send(EV_A_ &u->async_w);
	pthread_mutex_unlock(&u->lock);

	return 0;
}
