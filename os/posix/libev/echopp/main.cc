#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <resolv.h>

#include <ev++.h>

#include <list>
#include <memory>

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>

// data buffer class
class Buffer
{
	std::unique_ptr<char> data;
	ssize_t len, pos;

public:
	Buffer(std::unique_ptr<char>& bytes, ssize_t nbytes) : len(nbytes), pos(0) { assert(pos <= len); data.swap(bytes); }
	char*   dpos() const	{ assert(pos <= len); return data.get() + pos; }
	ssize_t nbytes() const	{ assert(pos <= len); return len - pos; }
	ssize_t pos_inc(ssize_t n) {
		assert(pos <= len);
		assert(pos + n <= len);
		return pos += n;
	}
	bool    empty() const	{ assert(pos <= len); return pos == len; }
};

// A single instance of a non-blocking Echo handler
class EchoInstance
{
public:
	EchoInstance(int s) : sfd(s) {
		printf("Got connection (%d:%d)\n", ++total_clients, sfd);
		fcntl(sfd, F_SETFL, fcntl(sfd, F_GETFL, 0) | O_NONBLOCK);

		io.set<EchoInstance, &EchoInstance::callback>(this);
		io.start(sfd, ev::READ);
	}

private:
	// effictivly a close and a destroy
	~EchoInstance() {
		io.stop(); // Stop and free watcher if client socket is closing
		close(sfd);
		printf("Drop connection (%d:%d)\n", --total_clients, sfd);
	}

	void callback(ev::io &watcher, int revents) {
		if (EV_ERROR & revents) { perror("got invalid event"); }
		if (revents & EV_READ) read_cb(watcher);
		if (revents & EV_WRITE) write_cb(watcher);
		write_queue.empty() ? io.set(ev::READ) : io.set(ev::READ | ev::WRITE);
	}

	// Receive message from client socket
	void read_cb(ev::io &watcher) {
		std::unique_ptr<char> buffer(new char[bufsz]);
		ssize_t nread = recv(watcher.fd, buffer.get(), bufsz, 0);
		if (nread < 0) { perror("read error"); return; }
		if (nread == 0) { delete this; return; }

		write_queue.emplace_back(buffer, nread);
		assert(!buffer.get());
	}

	// Socket is writable
	void write_cb(ev::io &watcher) {
		if (write_queue.empty()) { io.set(ev::READ); return; }

		Buffer& buffer = write_queue.front();
		ssize_t written = write(watcher.fd, buffer.dpos(), buffer.nbytes());
		if (written < 0) { perror("read error"); return; }
		buffer.pos_inc(written);

		if (buffer.empty()) { write_queue.pop_front(); }
	}

private:
	int	sfd;
	ev::io io;
	std::list<Buffer> write_queue;	// Buffers that are pending write

	static int total_clients;
	static const ssize_t bufsz = 4096;
};

class EchoServer {
public:
	EchoServer(int port) : s(socket(PF_INET, SOCK_STREAM, IPPROTO_TCP))
	{
		fcntl(s, F_SETFL, fcntl(s, F_GETFL, 0) | O_NONBLOCK);

		printf("Listening on port %d\n", port);
		struct sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.s_addr = INADDR_ANY;
		int rc = bind(s, (struct sockaddr *)&addr, sizeof(addr));
		if (rc != 0) { perror("bind"); }
		listen(s, 5);

		io.set<EchoServer, &EchoServer::io_accept>(this);
		io.start(s, ev::READ);

		sio.set<&EchoServer::signal_cb>();
		sio.start(SIGINT);
	}

	~EchoServer() { shutdown(s, SHUT_RD); close(s); }

	void io_accept(ev::io &watcher, int revents) {
		if (EV_ERROR & revents) { perror("got invalid event"); return; }

		struct sockaddr_in client_addr;
		socklen_t client_len = sizeof(client_addr);
		int client_sd = accept(watcher.fd, (struct sockaddr *)&client_addr, &client_len);
		if (client_sd < 0) { perror("accept error"); return; }
		new EchoInstance(client_sd);
	}

	static void signal_cb(ev::sig &signal, int) { signal.loop.break_loop(); }

private:
	int s;
	ev::io io;
	ev::sig	sio;
};

int EchoInstance::total_clients = 0;

int main(int argc, char **argv)
{
	int port = (argc > 1) ?  atoi(argv[1]) : 8192;
	EchoServer echo(port);

	ev::default_loop().run(0);
}
