#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT_NO 3033
#define BUFFER_SIZE 1024

int main()
{
	// Create client socket
	int sd = socket(PF_INET, SOCK_STREAM, 0);
	if (sd < 0)
	{
		perror("socket error");
		return -1;
	}

	struct sockaddr_in addr;
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT_NO);
	addr.sin_addr.s_addr =  htonl(INADDR_ANY);

	// Connect to server socket
	if (connect(sd, (struct sockaddr *)&addr, sizeof addr) < 0)
	{
		perror("Connect error");
		return -1;
	}

	char buffer[BUFFER_SIZE] = "";
	while (strcmp(buffer, "q") != 0)
	{
		// Read input from user and send message to the server
		fgets(buffer, BUFFER_SIZE, stdin);
		send(sd, buffer, strlen(buffer), 0);

		// Receive message from the server
		recv(sd, buffer, BUFFER_SIZE, 0);
		printf("message: %s\n", buffer);
	}

	return 0;
}
