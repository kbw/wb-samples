/*
 * https://gist.github.com/DamonHao/df87e727a0299c07fce3
 *
 * test_watcher_with_customerdata.c
 *
 *  Created on: Jun 3, 2014
 *      Author: damonhao
 */

// a single header file is required
#include <ev.h>

#include <unistd.h>
#include <stdio.h>
#include <string.h>

#define BUFFERSIZE 50

struct my_io
{
	ev_io io_watcher_;
	int data_;
};

void my_io_cb(struct ev_loop *loop, ev_io *w, int revents)
{
	struct my_io * custom_watcher = (struct my_io *) w;
	char buf[BUFFERSIZE];
	if (fgets(buf, sizeof(buf), stdin) != NULL)
	{
		if (strcmp("end", buf) != 0)
		{
			printf("read data times: %d\n", custom_watcher->data_);
			custom_watcher->data_++;
			puts(buf);
		}
		else
		{
			ev_io_stop(loop, w);
			ev_break(loop, EVBREAK_ALL);
		}
	}
	else
	{
		puts("read error");
	}
}

int main()
{
	puts("in main");
	struct ev_loop *loop = EV_DEFAULT;
	struct my_io custom_watcher;
	custom_watcher.data_ = 1;
	ev_io_init(&custom_watcher.io_watcher_, my_io_cb, STDIN_FILENO, EV_READ);
	ev_io_start(loop, &custom_watcher.io_watcher_);
	ev_run(loop, 0);
	return 0;
}
