/*
 * Main process creates two child processes.
 * Each process displays its process group
 * Main process terminates, leaving the two children running so they can be observed.
 */
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>

int main()
{
	pid_t pid, childpid[] = { 0, 0 };
	pid_t groupid;

	childpid[0] = fork();
	if (childpid[0] != 0) {
		childpid[1] = fork();
		if (childpid[1] == 0)
			childpid[0] = 0;
	}

	pid = getpid();
	groupid = getpgrp();
	printf("ProcessID=%d ProcessGroupId=%d%s\n", pid, groupid, (pid == groupid) ? " : I'm the group leader" : "");

	sleep(20);
	if (childpid[0] == 0)
		sleep(60);

	return 0;
}
