#include <sys/capsicum.h>
#include <stdio.h>
#include <string.h>
#include <memory>
#include <string>
#include <map>

struct local_string_t : public std::string
{
	typedef std::string inherited;

	using inherited::inherited;
};
typedef std::multimap<u_int, local_string_t> caps_map_t;

static caps_map_t s_caps;

static void print_caps(uint64_t rights, const char* rights_name)
{
	u_int rights_iflags = ~((u_int)rights);

	caps_map_t::const_iterator p = s_caps.find(rights_iflags);
	if (p == s_caps.end())
		s_caps.insert(std::make_pair(rights_iflags, rights_name));
}
#define PRINT_CAP(rights) print_caps(rights, #rights)

extern "C" void show_cap(u_int flags)
{
	u_int iflags = ~flags;

	std::pair<caps_map_t::const_iterator, caps_map_t::const_iterator> range = s_caps.equal_range(iflags);
	caps_map_t::const_iterator& begin = range.first;
	caps_map_t::const_iterator& end = range.second;

	if (begin != s_caps.end())
		do {
			printf(" %s\n", begin->second.c_str());
		}
		while (begin++ != end);
}

extern "C" void register_caps(void)
{
	/* General file I/O. */
	PRINT_CAP(CAP_READ);
	PRINT_CAP(CAP_WRITE);
	PRINT_CAP(CAP_SEEK);
	PRINT_CAP(CAP_PREAD);
	/* Allows for aio_write(2), openat(O_WRONLY) (without O_APPEND), pwrite(2), writev(2). */
	PRINT_CAP(CAP_PWRITE);
	PRINT_CAP(CAP_MMAP);
	PRINT_CAP(CAP_MMAP_R);
	PRINT_CAP(CAP_MMAP_W);
	PRINT_CAP(CAP_MMAP_X);
	PRINT_CAP(CAP_MMAP_RW);
	PRINT_CAP(CAP_MMAP_RX);
	PRINT_CAP(CAP_MMAP_WX);
	PRINT_CAP(CAP_MMAP_RWX);
	PRINT_CAP(CAP_CREATE);
	PRINT_CAP(CAP_FEXECVE);
	PRINT_CAP(CAP_FSYNC);
	PRINT_CAP(CAP_FTRUNCATE);
	/* Lookups - used to constrain *at() calls. */
	PRINT_CAP(CAP_LOOKUP);
	/* VFS methods. */
	PRINT_CAP(CAP_FCHDIR);
	PRINT_CAP(CAP_FCHFLAGS);
	PRINT_CAP(CAP_CHFLAGSAT);
	PRINT_CAP(CAP_FCHMOD);
	PRINT_CAP(CAP_FCHMODAT);
	PRINT_CAP(CAP_FCHOWN);
	PRINT_CAP(CAP_FCHOWNAT);
	PRINT_CAP(CAP_FCNTL);
	PRINT_CAP(CAP_FLOCK);
	PRINT_CAP(CAP_FPATHCONF);
	PRINT_CAP(CAP_FSCK);
	PRINT_CAP(CAP_FSTAT);
	PRINT_CAP(CAP_FSTATAT);
	PRINT_CAP(CAP_FSTATFS);
	PRINT_CAP(CAP_FUTIMES);
	PRINT_CAP(CAP_FUTIMESAT);
	PRINT_CAP(CAP_LINKAT_TARGET);
	PRINT_CAP(CAP_MKDIRAT);
	PRINT_CAP(CAP_MKFIFOAT);
	PRINT_CAP(CAP_MKNODAT);
	PRINT_CAP(CAP_RENAMEAT_SOURCE);
	PRINT_CAP(CAP_SYMLINKAT);
	PRINT_CAP(CAP_UNLINKAT);
	/* Socket operations. */
	PRINT_CAP(CAP_ACCEPT);
	PRINT_CAP(CAP_BIND);
	PRINT_CAP(CAP_CONNECT);
	PRINT_CAP(CAP_GETPEERNAME);
	PRINT_CAP(CAP_GETSOCKNAME);
	PRINT_CAP(CAP_GETSOCKOPT);
	PRINT_CAP(CAP_LISTEN);
	PRINT_CAP(CAP_PEELOFF);
	PRINT_CAP(CAP_RECV);
	PRINT_CAP(CAP_SEND);
	PRINT_CAP(CAP_SETSOCKOPT);
	PRINT_CAP(CAP_SHUTDOWN);
	PRINT_CAP(CAP_BINDAT);
	PRINT_CAP(CAP_CONNECTAT);
	PRINT_CAP(CAP_LINKAT_SOURCE);
	PRINT_CAP(CAP_RENAMEAT_TARGET);
	PRINT_CAP(CAP_SOCK_CLIENT);
	PRINT_CAP(CAP_SOCK_SERVER);
	PRINT_CAP(CAP_ALL0);
	PRINT_CAP(CAP_UNUSED0_44);
	PRINT_CAP(CAP_UNUSED0_57);
	PRINT_CAP(CAP_MAC_GET);
	PRINT_CAP(CAP_MAC_SET);
	PRINT_CAP(CAP_SEM_GETVALUE);
	PRINT_CAP(CAP_SEM_POST);
	PRINT_CAP(CAP_SEM_WAIT);
	PRINT_CAP(CAP_EVENT);
	PRINT_CAP(CAP_KQUEUE_EVENT);
	PRINT_CAP(CAP_IOCTL);
	PRINT_CAP(CAP_TTYHOOK);
	/* Process management via process descriptors. */
	PRINT_CAP(CAP_PDGETPID);
	PRINT_CAP(CAP_PDWAIT);
	PRINT_CAP(CAP_PDKILL);
	/* Extended attributes. */
	PRINT_CAP(CAP_EXTATTR_DELETE);
	PRINT_CAP(CAP_EXTATTR_GET);
	PRINT_CAP(CAP_EXTATTR_LIST);
	PRINT_CAP(CAP_EXTATTR_SET);
	/* Access Control Lists. */
	PRINT_CAP(CAP_ACL_CHECK);
	PRINT_CAP(CAP_ACL_DELETE);
	PRINT_CAP(CAP_ACL_GET);
	PRINT_CAP(CAP_ACL_SET);
	PRINT_CAP(CAP_KQUEUE_CHANGE);
	PRINT_CAP(CAP_KQUEUE);
	PRINT_CAP(CAP_ALL1);
	PRINT_CAP(CAP_UNUSED1_22);
	PRINT_CAP(CAP_UNUSED1_57);
}
