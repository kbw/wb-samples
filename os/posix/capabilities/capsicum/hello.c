#include <sys/capsicum.h>
#include <stdio.h>

int main()
{
	int rc;
	cap_rights_t write_access;

	rc = cap_enter();
	printf("cap_enter() : %d : capsicum %sinitialiazed.\n", rc, !rc ? "" : "not ");
	if (rc == -1)
		return 1;

/*	write_access = CAP_WRITE; */

	rc = fprintf(stdout, "hello world\n");
	if (rc < 10)
		fprintf(stderr, "rc = %d\n", rc);

	return 0;
}
