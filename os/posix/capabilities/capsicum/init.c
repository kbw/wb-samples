#include "caps.h"
#include <sys/capsicum.h>
#include <stdio.h>

int main()
{
	int rc;
	u_int flags;

	rc = cap_enter();
	printf("cap_enter() : rc=%d : capsicum %sinitialiazed\n", rc, !rc ? "" : "not ");

	rc = cap_getmode(&flags);
	printf("cap_getmode() : rc=%d : capsicum %s\n", rc, flags ? "enabled" : "disabled");
//	register_caps();
//	show_cap(flags);

	return 0;
}
