#include <unistd.h>
#include <aio.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>

#define TRUE 1
#define FALSE 0

int create_server_socket(const char* url);
void test_parse_url();

int main(int argc, char * argv[])
{
	if (argc > 1 && strcmp(argv[1], "--selftest") == 0) {
		test_parse_url();
		return 0;
	}

	printf("fd=%d\n", create_server_socket("tcp://*:5500"));
	printf("fd=%d\n", create_server_socket("http://*:8080"));
	return 0;

	if (argc != 2) {
		printf("Usage: %s {filename}\n", argv[0]);
		return -1;
	}

	//Open the file specified on the command line
	FILE* file = fopen(argv[1], "r");
	if (file) {
		ssize_t ret;
		struct stat finfo;
		struct aiocb cb;
	
		//Set up the control block
		fstat(fileno(file), &finfo);
		bzero(&cb, sizeof(cb));
		cb.aio_buf = calloc(1, (size_t)finfo.st_size + 1);
		cb.aio_fildes = fileno(file);
		cb.aio_nbytes = (size_t)finfo.st_size;
		cb.aio_offset = 0;

		//Perform the read
		ret = aio_read(&cb);
		if (ret == -1) {
			printf("aio_read error=%s, falling back to synchronous read\n", strerror(errno));
			ret = pread(cb.aio_fildes, (char*)cb.aio_buf, cb.aio_nbytes, cb.aio_offset);
		}
		else {
			//Wait for it to complete
			const struct aiocb* const cbs[] = { &cb };
			ret = aio_suspend(cbs, sizeof(cbs)/sizeof(cbs[0]), NULL);
			printf("AIO operation returned %ld\n", aio_return(&cb));
		}
		printf("%s\nlen=%zu\n", (char*)cb.aio_buf, cb.aio_nbytes);

		free((void*)cb.aio_buf), cb.aio_buf = NULL;
		fclose(file), file = NULL;
	}

	return 0;
}

//---------------------------------------------------------------------------
//
static char* parse_url(const char* url, char** protocol, char** addr, char** port, char** path);

// -1: bad url
// -2: bad protocol
// -3: socket error
int create_server_socket(const char* url)
{
	char *url_parts, *protocol, *addr, *port, *path;
	url_parts = parse_url(url, &protocol, &addr, &port, &path);
	int ret = -1;
	if (!url_parts)
		goto create_server_socket_fail;

	ret = -2;
	unsigned short default_port = USHRT_MAX;
	{
		struct servent* ent;
		ent = getservbyname(protocol, "tcp");
		if (ent) {
			default_port = (u_short)ent->s_port;
			endservent();
		}
		else {
			ent = getservbyname(protocol, "udp");
			if (ent) {
				default_port = (u_short)ent->s_port;
				endservent();
			}
		}
	}

	if (default_port == USHRT_MAX) {
		if (!strcmp(protocol, "tcp") && !strcmp(protocol, "udp"))
			goto create_server_socket_fail;
	}

	// port
	u_short portno = (port) ? (u_short)atoi(port) : 80;

	// set address info
	struct sockaddr_in serveraddr;
	bzero(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(portno);

	if (strcmp(addr, "*") == 0) {
		serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	}
	else {
		ret = -3;
		struct hostent *server = gethostbyname(addr);
		if (!server)
			goto create_server_socket_fail;

		bcopy(server->h_addr, &serveraddr.sin_addr.s_addr, (size_t)server->h_length);
	}

	int fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

	ret = -4;
	if (bind(fd, (struct sockaddr*)&serveraddr, sizeof(serveraddr)) == -1)
		goto create_server_socket_fail;

	ret = fd;

create_server_socket_fail:
	free(url_parts);
	return ret;
}

//---------------------------------------------------------------------------
//
void test_parse_url()
{
	struct results_rec {
		int parsed_url;
		const char* protocol;
		const char* addr;
		const char* port;
		const char* path;
	};
	struct test_rec {
		const char* url;
		struct results_rec results;
	};

	const struct test_rec tests[] = {
		{ "tcp://*:8080/index.html", { TRUE, "tcp", "*", "8080", "index.html" } },
		{ "udp://127.0.0.1/humans", { TRUE, "udp", "127.0.0.1", NULL, "humans" } },
		{ "smtp://webbusy.biz:812", { TRUE, "smtp", "webbusy.biz", "812", NULL} },
		{ "https", { FALSE, NULL, NULL, NULL, NULL } },
		{ "http://", { FALSE, NULL, NULL, NULL, NULL } },
		{ "icmp://:12", { FALSE, "icmp", "", "12", NULL} },
		{ "pcap:///logs?type=pcap", { FALSE, "pcap", "", NULL, "logs?type=pcap" } },
		{ "log://:12/var/log", { FALSE, "log", "", "12", "var/log" } },
		{ "ncc://siteconfidence.co.uk", { TRUE, "ncc", "siteconfidence.co.uk", NULL, NULL } },
		{ NULL, { TRUE, NULL, NULL, NULL, NULL } }
	};

	int i;
	for (i = 0; tests[i].url; ++i) {
		const struct test_rec* test = tests + i;

		char *url_parts, *protocol, *addr, *port, *path;
		url_parts = parse_url(test->url, &protocol, &addr, &port, &path);

		int ok;
		ok = (test->results.parsed_url == (url_parts != NULL));
		if (!ok) {
			printf("FAILED: url=%s reason=%s\n", test->url, "parse failure");
			free(url_parts);
			continue;
		}

		ok = (test->results.parsed_url ? (protocol ? strcmp(test->results.protocol, protocol) == 0 : FALSE) : (protocol == NULL));
		if (!ok) {
			printf("FAILED: url=%s reason=\"%s\"\n", test->url, "protocol failure");
			free(url_parts);
			continue;
		}

		ok = (test->results.parsed_url ? (addr ? strcmp(test->results.addr, addr) == 0 : FALSE) : (addr == NULL));
		if (!ok) {
			printf("FAILED: url=%s reason=\"%s\"\n", test->url, "addr failure");
			free(url_parts);
			continue;
		}

		ok = (test->results.parsed_url ? (port ? strcmp(test->results.port, port) == 0 : (port == test->results.port)) : (port == NULL));
		if (!ok) {
			printf("FAILED: url=%s reason=\"%s\"\n", test->url, "port failure");
			free(url_parts);
			continue;
		}

		ok = (test->results.parsed_url ? (path ? strcmp(test->results.path, path) == 0 : (path == test->results.path)) : (path == NULL));
		if (!ok) {
			printf("FAILED: url=%s reason=\"%s\"\n", test->url, "path failure");
			free(url_parts);
			continue;
		}

		printf("PASSED: url=%s\n", test->url);
		free(url_parts);
	}
}

static char* parse_url(const char* in, char** protocol, char** addr, char** port, char** path)
{
	char* url = strdup(in);
	char *p, *p2;
	char* q = url;

	*protocol = *addr = *port = *path = NULL;

	p = strstr(q, "://");
	if (!p) {
		goto term;
	}

	*protocol = url;
	*p = 0;
	q = p + 3; // strlen("://");

	if (!*q) {
		// no addr, no port, no path
		goto term;
	}

	p = strstr(q, ":");
	p2 = strstr(q, "/");
	if (!p && p2) {
		// no port, have path
		*addr = q;
		*p2 = 0;
		q = p2 + 1; // strlen("/")
		*port = NULL;
		*path = q;
	}
	else if (p && !p2) {
		// have port, no path
		*addr = q;
		*p = 0;
		q = p + 1; // strlen(":")
		*port = q;
		*path = NULL;
	}
	else if (p && p2) {
		// have port, have path
		*addr = q;
		*p = 0;
		q = p + 1; // strlen(":")
		*port = q;
		*p2 = 0;
		q = p2 + 1; // strlen("/")
		*path = q;
	}
	else if (!p && !p2) {
		// no port, no path
		*addr = q;
	}

term:
	if (!*addr || !**addr) {
		*protocol = *addr = *port = *path = NULL;
		free(url), url = NULL;
	}
	return url;
}
