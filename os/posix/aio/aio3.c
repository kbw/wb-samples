#include <aio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define	COUNT	4
#define	NBYTES	10
struct aiocb* cb[COUNT];

int main(int argc, char* argv[])
{
	if (argc != 2) {
		printf("Usage: %s {filename}\n", argv[0]);
		return -1;
	}

	FILE* file = fopen(argv[1], "r");
	if (file) {
		int ret;

		//Create a buffer to store the read data
		char* buf = calloc(1, 2*NBYTES);

		int i;
		for (i = 0; i < COUNT; ++i) {
			//Allocate space for the aio control blocks
			cb[i] = calloc(1, sizeof(struct aiocb));

			//Somewhere to store the result
			cb[i]->aio_buf = buf + i*NBYTES;

			//The file to read from
			cb[i]->aio_fildes = fileno(file);

			//The number of bytes to read, and the offset
			cb[i]->aio_nbytes = NBYTES;
			cb[i]->aio_offset = i*NBYTES;

			//Specify that these are read operations
			cb[i]->aio_lio_opcode = LIO_READ;
		}

		ret = lio_listio(LIO_WAIT, cb, 2, NULL);
		printf("AIO %d operations returned %d\n", COUNT, ret);

		for (i = COUNT; i > 0; --i)
			free(cb[i - 1]), cb[i - 1] = NULL;
		free(buf), buf = NULL;
	}

	return 0;
}
