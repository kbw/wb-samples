#include <unistd.h>
#include <aio.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int argc, char * argv[])
{
	if (argc != 2) {
		printf("Usage: %s {filename}\n", argv[0]);
		return -1;
	}

	//Open the file specified on the command line
	FILE* file = fopen(argv[1], "r");
	if (file) {
		ssize_t ret;
		struct stat finfo;
		struct aiocb cb;
	
		//Set up the control block
		fstat(fileno(file), &finfo);
		bzero(&cb, sizeof(cb));
		cb.aio_buf = calloc(1, (size_t)finfo.st_size + 1);
		cb.aio_fildes = fileno(file);
		cb.aio_nbytes = (size_t)finfo.st_size;
		cb.aio_offset = 0;

		//Perform the read
		ret = aio_read(&cb);
		if (ret == -1) {
			printf("aio_read error=%s, falling back to synchronous read\n", strerror(errno));
			ret = pread(cb.aio_fildes, (char*)cb.aio_buf, cb.aio_nbytes, cb.aio_offset);
		}
		else {
			//Wait for it to complete
			const struct aiocb* const cbs[] = { &cb };
			ret = aio_suspend(cbs, sizeof(cbs)/sizeof(cbs[0]), NULL);
			printf("AIO operation returned %ld\n", aio_return(&cb));
		}
		printf("%s\nlen=%zu\n", (char*)cb.aio_buf, cb.aio_nbytes);

		free((void*)cb.aio_buf), cb.aio_buf = NULL;
		fclose(file), file = NULL;
	}

	return 0;
}
