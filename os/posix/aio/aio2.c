#include <aio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#define	COUNT	4
#define	NBYTES	10
struct aiocb* cb[COUNT];

//The signal number to use.
#define SIG_AIO (SIGRTMIN + 5)
int count;

//Signal handler called when an AIO operation finishes
void aio_handler(int signal, siginfo_t *info, void *uap)
{
	int cbNumber = info->si_value.sival_int;
	printf("AIO operation %d completed returning %ld\n", 
		cbNumber,
		aio_return(cb[cbNumber]));
	++count;
}

int main(int argc, char* argv[])
{
	if (argc != 2) {
		printf("Usage: %s {filename}\n", argv[0]);
		return -1;
	}

	//Set up the signal handler
	struct sigaction action;
	action.sa_sigaction = aio_handler;
	action.sa_flags = SA_SIGINFO;
	sigemptyset(&action.sa_mask);
	sigaction(SIG_AIO, &action, NULL);

	FILE* file = fopen(argv[1], "r");
	if (file) {
		//Create a buffer to store the read data
		char* buf = calloc(1, COUNT*NBYTES);

		int i;
		for (i = 0; i < COUNT; ++i) {
			//Allocate space for the aio control blocks
			cb[i] = calloc(1, sizeof(struct aiocb));

			//Somewhere to store the result
			cb[i]->aio_buf = buf + i*NBYTES;

			//The file to read from
			cb[i]->aio_fildes = fileno(file);

			//The number of bytes to read, and the offset
			cb[i]->aio_offset = i*NBYTES;

			//The signal to send, and the value of the signal
			cb[i]->aio_sigevent.sigev_notify = SIGEV_SIGNAL;
			cb[i]->aio_sigevent.sigev_signo = SIG_AIO;
			cb[i]->aio_sigevent.sigev_value.sival_int = i;

			aio_read(cb[i]);
		}
		while (count < 2)
			sleep(1);

		for (i = COUNT; i > 0; --i)
			free(cb[i - 1]), cb[i - 1] = NULL;
		free(buf), buf = NULL;
	}

	return 0;
}
