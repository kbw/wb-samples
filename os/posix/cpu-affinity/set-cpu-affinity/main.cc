#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <sched.h>

#include <iostream>
#include <stdlib.h>

using namespace std;

cpu_set_t get_affinity()
{
	cpu_set_t cpus;
	CPU_ZERO(&cpus);

	int err = sched_getaffinity(0, sizeof(cpus), &cpus);
	if (err)
	{
		cout << "err=" << err << ": " << strerror(err) << endl;
		exit(1);
	}

	return cpus;
}

void set_affinity(cpu_set_t cpus)
{
	int err = sched_setaffinity(0, sizeof(cpus), &cpus);
	if (err)
	{
		cout << "err=" << err << ": " << strerror(err) << endl;
		exit(1);
	}
}

int main(int argc, char* argv[])
{
	if (argc > 2)
	{
		cerr << "usage: set-cpu-affinity cpu-id" << endl;
		return 1;
	}

	cpu_set_t cpus = get_affinity();

	struct timeval tv;
	gettimeofday(&tv, NULL);
	srand((unsigned)tv.tv_usec);
	int cpu_id = (argc == 2) ? atoi(argv[1]) : rand();
	cpu_id %= CPU_COUNT(&cpus);

	CPU_ZERO(&cpus);
	CPU_SET(cpu_id, &cpus);

	set_affinity(cpus);
	cout << "cpu_id = " << cpu_id << endl;
}
