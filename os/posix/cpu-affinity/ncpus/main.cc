#include <unistd.h>
#include <string.h>
#include <sched.h>
#include <iostream>

using namespace std;

int main()
{
	int err;
	cpu_set_t cpus;
	CPU_ZERO(&cpus);

	err = sched_getaffinity(0, sizeof(cpus), &cpus);

	if (err)
	{
		cout << "err=" << err << ": " << strerror(err) << endl;
		return 1;
	}
	
	cout << "n cpus = " << CPU_COUNT(&cpus) << endl;
}
