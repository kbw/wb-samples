#include <unistd.h>
#include <sys/time.h>
#include <sched.h>
#include <pthread.h>

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string.h>

using namespace std;

//---------------------------------------------------------------------------

cpu_set_t get_affinity()
{
	cpu_set_t cpus;
	CPU_ZERO(&cpus);

	int err = sched_getaffinity(0, sizeof(cpus), &cpus);
	if (err)
	{
		cout << "err=" << err << ": " << strerror(err) << endl;
		exit(1);
	}

	return cpus;
}

void set_affinity(const cpu_set_t& cpus)
{
	int err = sched_setaffinity(0, sizeof(cpus), &cpus);
	if (err)
	{
		cout << "err=" << err << ": " << strerror(err) << endl;
		exit(1);
	}
}

void set_thread_affinity(pthread_t* id, const cpu_set_t& cpus)
{
	int err = pthread_setaffinity_np(*id, sizeof(cpus), &cpus);
	if (err)
	{
		cout << "err=" << err << ": " << strerror(err) << endl;
		exit(1);
	}
}

//---------------------------------------------------------------------------

typedef void* thread_func_t(void*);

void create_thread(pthread_t* id, thread_func_t* func, void* param)
{
	int err = pthread_create(id, NULL, func, param);
	if (err)
	{
		cout << "err=" << err << ": " << strerror(err) << endl;
		exit(1);
	}
}

struct func_args_t
{
	func_args_t(pthread_t* id, int cpu_id) :
		id(id), cpu_id(cpu_id)
	{
		CPU_ZERO(&cpus);
		CPU_SET(cpu_id, &cpus);
	}

	pthread_t* id;
	int cpu_id;
	cpu_set_t cpus;
};

void* func(void* param)
{
	if (func_args_t* arg = reinterpret_cast<func_args_t*>(param))
	{
		set_thread_affinity(arg->id, arg->cpus);
		sleep(1);
		delete arg;
	}

	return NULL;
}

//---------------------------------------------------------------------------

int main(int argc, char* argv[])
{
	if (argc == 1)
	{
		cerr << "usage: set-cpu-thread-affinity cpu-id [n-threads=1]" << endl;
		return 1;
	}

	struct timeval tv;
	gettimeofday(&tv, NULL);
	srand((unsigned)tv.tv_usec);

	cpu_set_t cpus = get_affinity();
	int cpu_id = (argc > 1) ? atoi(argv[1]) : rand();
	cpu_id %= CPU_COUNT(&cpus);

	CPU_ZERO(&cpus);
	CPU_SET(cpu_id, &cpus);

	const int nthreads = (argc > 2) ? abs(atoi(argv[2])) : 1;
	vector<pthread_t> ids(nthreads);
	for (int i = 0; i < nthreads; ++i)
		create_thread(&ids[i], func, new func_args_t(&ids[i], (cpu_id + i) % CPU_COUNT(&cpus)));

	for (int i = 0; i < nthreads; ++i)
	{
		void* ret = NULL;
		pthread_join(ids[i], &ret);
	}
	ids.clear();

	cout << "cpu_id = " << cpu_id << endl;
}
