#!/bin/sh

export OS=`uname -s`
export REL=`uname -r`
export ARCH=`uname -m`
export KNAME=`uname -i`

dtrace -l > dtrace-probes-${OS}-${REL}-${ARCH}=${KNAME}
