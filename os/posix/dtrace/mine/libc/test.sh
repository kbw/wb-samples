#!/bin/sh

make prog || exit $?

sudo dtrace -n ":::entry { @[probefunc] = count(); }" -c ./prog
for func in bzero bcopy strlen strcat strcpy malloc calloc realloc free; do
	sudo dtrace -n "::${func}:entry { @[pid,probefunc] = count(); }" -c ./prog
done
