#include "kqutils.hpp"

#include <sys/errno.h>

#include <signal.h>

/*
#include "defs.hpp"
#include "finfo.hpp"

#include <unistd.h>
#include <fcntl.h>
#include <sys/errno.h>
#include <sys/event.h>
#include <sys/types.h>
#include <sys/uio.h>

#include <map>
#include <numeric>
#include <vector>
#include <string>

#include <stdarg.h>
#include <signal.h>

//---------------------------------------------------------------------------
//
typedef std::map<int, finfo_t>		fileinfo_t;	// files by fd
typedef std::vector<struct kevent>	kevents_t;

ssize_t trace(const char* fmt, ...) __attribute__ ((format (printf, 1, 2)));

inline fileinfo_t& append(fileinfo_t& target, fileinfo_t source)
{
	for (auto& entry : source)
		target.emplace(entry.first, std::move(entry.second));

	return target;
}

//---------------------------------------------------------------------------
//
fileinfo_t make_default_fileinfo()
{
	fileinfo_t files;

//	files.emplace(STDIN_FILENO, finfo_t(STDIN_FILENO, O_RDONLY, "stdin"));
	files.emplace(STDOUT_FILENO, finfo_t(STDOUT_FILENO, O_WRONLY, "stdout"));
	files.emplace(STDERR_FILENO, finfo_t(STDERR_FILENO, O_WRONLY, "stderr"));

	return files;
}

fileinfo_t make_fileinfo(int argc, char* argv[])
{
	fileinfo_t files;

	for (int i = 1; i < argc; ++i) {
		int fd = open(argv[i], O_RDONLY);
		if (fd == -1)
			err(EXIT_FAILURE, "cannot open: %s", argv[i]);

		files.emplace(fd, finfo_t(fd, O_WRONLY, argv[i]));
	}

	return files;
}

kevents_t make_events(fileinfo_t& files)
{
	std::vector<struct kevent> events{files.size()};

	size_t i{};
	fileinfo_t::iterator p{ files.begin() };
//	fd_t fd = p->first;
//	EV_SET(&events[i], fd, EVFILT_VNODE, EV_ADD | EV_CLEAR, NOTE_READ, 0, NULL);
//	++p, ++i;

	for (; p != files.end(); ++p, ++i) {
		fd_t fd = p->first;
		EV_SET(&events[i], fd, EVFILT_VNODE, EV_ADD | EV_CLEAR, NOTE_WRITE, 0, NULL);
	}

	return events;
}

ssize_t write(int fd, std::vector<struct iovec> iov)
{
	size_t offset{};
	size_t size{ iov.size() };

	size_t sent{};
	size_t nbytes{ std::accumulate(
						std::cbegin(iov), std::cend(iov), static_cast<size_t>(0),
						[](size_t a, const struct iovec& b) -> size_t { return a + b.iov_len; }) };

	while (true) {
		ssize_t ret = writev(fd, &iov[offset], static_cast<int>(size));
		if (ret == -1)
			return sent ? static_cast<ssize_t>(sent) : -1;

		if (ret == 0)
			return static_cast<ssize_t>(sent);

		sent += static_cast<size_t>(ret);
		if (sent == nbytes)
			return static_cast<ssize_t>(nbytes);

		// fixup iov for retry
		size_t remaining{ sent };
		for (auto i = offset; i != iov.size(); ++i) {
			remaining += iov[i].iov_len;

			if (remaining > sent) {
				auto delta = remaining - sent;

				iov[i].iov_base = static_cast<char*>(iov[i].iov_base) + delta;
				iov[i].iov_len -= delta;

				offset = i;
				break;
			}
		}
		assert(false); // cannot get here!
	}
}

void write(fd_t fd, finfo_t& file)
{
	static fd_t lastfd{-1};

	ssize_t sent{};
	ssize_t nbytes{ file.nbytes };

	if (nbytes > 0) {
		std::vector<struct iovec> iov;

		if (lastfd != -1 && file.fd != lastfd) {
			iov.push_back({ file.comment.get(), static_cast<size_t>(file.commentsz) });
		}
		iov.push_back({ file.buf.get(), static_cast<size_t>(nbytes - sent) });

		write(fd, std::move(iov));
	}

	lastfd = file.fd;
}

//---------------------------------------------------------------------------
//
ssize_t trace(const char* fmt, ...)
{
	if (getenv("NOTRACE"))
		return 0;

	va_list args;
	va_start(args, fmt);
	char* buf = nullptr;
	ssize_t nbytes = vasprintf(&buf, fmt, args);
	va_end(args);
	if (unlikely(nbytes == -1))
		return -1;

	std::unique_ptr<char, decltype(&free)> tmp(buf, free);
	std::vector<struct iovec> iov{
		{ (void*)("trace: "), 7 },
		{ buf, static_cast<size_t>(nbytes) }
	};
	return write(STDERR_FILENO, std::move(iov));

//	ssize_t nbytes1 = write(STDERR_FILENO, "trace: ", 7);
//	ssize_t nbytes2 = write(STDERR_FILENO, buf, static_cast<size_t>(nbytes));
//	return nbytes1 + nbytes2;
}

std::string flags_str(u_short flags)
{
	std::string str;

	for (decltype(flags) i = 1; i != 0; i <<= 1) {
		if ((flags & i) == EV_ADD)		str += " | EV_ADD";
		if ((flags & i) == EV_DELETE)	str += " | EV_DELETE";
		if ((flags & i) == EV_ENABLE)	str += " | EV_ENABLE";
		if ((flags & i) == EV_DISABLE)	str += " | EV_DISABLE";

		if ((flags & i) == EV_ONESHOT)	str += " | EV_ONESHOT";
		if ((flags & i) == EV_CLEAR)	str += " | EV_CLEAR";
		if ((flags & i) == EV_RECEIPT)	str += " | EV_RECEIPT";
		if ((flags & i) == EV_DISPATCH)	str += " | EV_DISPATCH";

		if ((flags & i) == EV_DROP)		str += " | EV_DROP";
		if ((flags & i) == EV_FLAG1)	str += " | EV_FLAG1";
		if ((flags & i) == EV_ERROR)	str += " | EV_ERROR";
		if ((flags & i) == EV_EOF)		str += " | EV_EOF";

		if ((flags & i) == EV_SYSFLAGS)	str += " | EV_SYSFLAGS";
	}

	return (str.size() > 3) ? str.substr(3) : std::string("EV_NOP");
}

std::string fflags_str(u_int fflags)
{
	std::string str;

	for (decltype(fflags) i = 1; i != 0; i <<= 1) {
		if ((fflags & i) == NOTE_DELETE)		str += " | NOTE_DELETE";
		if ((fflags & i) == NOTE_WRITE)			str += " | NOTE_WRITE";
		if ((fflags & i) == NOTE_EXTEND)		str += " | NOTE_EXTEND";
		if ((fflags & i) == NOTE_ATTRIB)		str += " | NOTE_ATTRIB";

		if ((fflags & i) == NOTE_LINK)			str += " | NOTE_LINK";
		if ((fflags & i) == NOTE_RENAME)		str += " | NOTE_RENAME";
		if ((fflags & i) == NOTE_REVOKE)		str += " | NOTE_REVOKE";
		if ((fflags & i) == NOTE_OPEN)			str += " | NOTE_OPEN";

		if ((fflags & i) == NOTE_CLOSE)			str += " | NOTE_CLOSE";
		if ((fflags & i) == NOTE_CLOSE_WRITE)	str += " | NOTE_CLOSE_WRITE";
		if ((fflags & i) == NOTE_READ)			str += " | NOTE_READ";
	}

	return (str.size() > 3) ? str.substr(3) : std::string("NOTE_FFNOP");
}
 */

//---------------------------------------------------------------------------
//
void decode_events(fileinfo_t& files, int kq, const struct kevent& tevent)
{
	// complete asynchronous read
	if (tevent.udata) {
		fd_t fd{ static_cast<int>( reinterpret_cast<long>(tevent.udata) ) };
		finfo_t& file = files.find(fd)->second;
		struct aiocb* cb = reinterpret_cast<struct aiocb*>(tevent.ident);

		if (!file.pending)
			trace("unexpected aio complete\n");
		file.pending = false;

		file.nbytes = aio_return(cb);
		if (file.nbytes == -1) {
			trace("async read failed code=%d error=\"%s\"\n", errno, strerror(errno));
			return;
		}

		assert(file.buf.get() == file.cb.get()->aio_buf);
		trace("aio_return: offset=%ld nbytes=%zd\n", file.offset, file.nbytes);
		write(STDOUT_FILENO, file);
		return;
	}

	// initiate asynchronous read
	fd_t fd{ static_cast<int>(tevent.ident) };
	fileinfo_t::iterator pfile = files.find(fd);
	if (pfile == files.end()) {
		trace("ERROR: fd lookup\n");
		err(EXIT_FAILURE, "ERROR: fd=%d lookup", fd);
	}
	finfo_t& file = pfile->second;

	if (file.pending) {
		trace("aio_read pending: dropping aio_read request\n");
		return;
	}
	file.pending = true;

	// add aio_read
	int ret = aio_read(file.fill_cb(kq));
	trace("aio_read(fd=%d offset=%ld nbytes=%d)=%d code=%d error=\"%s\"\n",
		fd, file.offset, file.bufsz, ret, errno, strerror(errno));
	if (ret == 0)
		return;

	ret = aio_cancel(fd, file.cb.get());
	trace("aio_cancel() code=%d text=%s\n", ret,
		(ret == AIO_CANCELED    ? "AIO_CANCELED" :
		 ret == AIO_NOTCANCELED ? "AIO_NOTCANCELED" :
		 ret == AIO_ALLDONE     ? "AIO_ALLDONE" : strerror(errno)));
	file.pending = false;
	if (ret == AIO_CANCELED) {
		trace("falling back to synchronous read\n");
		return;
	}

	// fall back to synchronous read
	char* buf{ file.buf.get() };
	ssize_t bufsz{ file.bufsz };

	assert(file.offset == lseek(fd, 0, SEEK_CUR));
	file.nbytes = read(fd, buf, static_cast<size_t>(bufsz));
	if (file.nbytes == -1)
		err(EXIT_FAILURE, "read failed code=%d error=\"%s\"", errno, strerror(errno));
	if (file.nbytes == 0)
		return;
	trace("read: nbytes=%zd\n", file.nbytes);
	write(STDOUT_FILENO, file.buf.get(), static_cast<size_t>(file.nbytes));
}

namespace
{
	bool s_stop = false;

	void handle_signal(int)
	{
		s_stop = true;
	}
}

int main(int argc, char* argv[])
{
	struct sigaction sa;
	sa.sa_handler = &handle_signal;
	sa.sa_flags = SA_RESTART;
	sigfillset(&sa.sa_mask);
	sigaction(SIGHUP, &sa, NULL);

	fileinfo_t files = make_fileinfo(argc, argv);
//	append(files, make_default_fileinfo());
//	append(files, make_fileinfo(argc, argv));
	trace("files.size=%zd\n", files.size());
	if (files.empty())
		return 0;

	kevents_t events{ make_events(files) };
	kevents_t tevents{ 2 * files.size()} ; // allow error and fired on each

	int kq = kqueue();
	if (kq == -1)
		err(EXIT_FAILURE, "kqueue(): code=%d error%s", errno, strerror(errno));

	for (; !s_stop; ) {
		// wait for an event
		int n_tevents{ kevent(
				kq,
				!events.empty() ? &events.front() : NULL, static_cast<int>(events.size()),
				&tevents.front(), static_cast<int>(tevents.size()), NULL) };
		trace("kevent(kq, events=%lu, tevents=%lu, NULL)=%d\n", events.size(), tevents.size(), n_tevents);
		if (n_tevents == -1)
			err(EXIT_FAILURE, "kevent failed code=%d error=\"%s\"", errno, strerror(errno));

		for (size_t i = 0; i != static_cast<size_t>(n_tevents); ++i) {
			const struct kevent& tevent = tevents[i];

			trace("event[%zu]: ident:0x%lx flags:0x%hx (%s) fflags:0x%x (%s) data:0x%lx udata:%p\n",
				i, tevent.ident,
				tevent.flags, flags_str(tevent.flags).c_str(),
				tevent.fflags, fflags_str(tevent.fflags).c_str(),
				tevent.data, tevent.udata);
			if (tevent.flags & EV_ERROR) {
//				trace("FATAL\n");
//				err(EXIT_FAILURE, "ERROR: code=%d error=\"%s\"", errno, strerror(errno));
				trace("ERROR: code=%d error=\"%s\"\n", errno, strerror(errno));
				continue;
			}

			decode_events(files, kq, tevent);
		}
	}
}
