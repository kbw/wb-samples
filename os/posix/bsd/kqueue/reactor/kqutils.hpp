#include "defs.hpp"
#include "finfo.hpp"

//	#include <unistd.h>
//	#include <fcntl.h>
//	#include <sys/errno.h>
#include <sys/event.h>
//	#include <sys/types.h>
//	#include <sys/uio.h>

#include <map>
//	#include <numeric>
#include <vector>
#include <string>

//	#include <stdarg.h>
//	#include <signal.h>

//---------------------------------------------------------------------------
//
typedef std::map<int, finfo_t>		fileinfo_t;	// files by fd
typedef std::vector<struct kevent>	kevents_t;

ssize_t trace(const char* fmt, ...) __attribute__ ((format (printf, 1, 2)));

inline fileinfo_t& append(fileinfo_t& target, fileinfo_t source)
{
	for (auto& entry : source)
		target.emplace(entry.first, std::move(entry.second));

	return target;
}

//---------------------------------------------------------------------------
//
fileinfo_t make_default_fileinfo();
fileinfo_t make_fileinfo(int argc, char* argv[]);

kevents_t make_events(fileinfo_t& files);

ssize_t write(int fd, std::vector<struct iovec> iov);
void write(fd_t fd, finfo_t& file);

//---------------------------------------------------------------------------
//
std::string flags_str(u_short flags);
std::string fflags_str(u_int fflags);
