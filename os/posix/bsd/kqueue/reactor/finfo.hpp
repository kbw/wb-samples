#pragma once

#include <unistd.h>
#include <sys/types.h>
#include <sys/aio.h>
#include <err.h>

#include <memory>

#include <assert.h>
#include <stdio.h>

//---------------------------------------------------------------------------
//
struct finfo_t
{
	finfo_t(int fd, int flags, const char* name);

	finfo_t() = delete;
	~finfo_t() = default;

	finfo_t(const finfo_t& n) = delete;
	finfo_t& operator=(const finfo_t& n) = delete;

	finfo_t(finfo_t&& n);
	finfo_t& operator=(finfo_t&& n) = delete;

	struct aiocb* fill_cb(int kq);

	void invariant() const;

	// file info
	fd_t fd{-1};
	int flags{0};
	off_t offset{-1};
	size_t namelen{std::numeric_limits<size_t>::max()};
	const char* name{nullptr};

	// prebuilt comment
	int commentsz{-1};
	std::unique_ptr<char, decltype(&free)> comment{nullptr, free};
	ssize_t nbytes{0};

	// asynchronous i/o
	bool pending{false};
	std::unique_ptr<struct aiocb> cb;
	inline static constexpr int bufsz{FDBUF_SZ};
	std::unique_ptr<char[]> buf;
};

inline finfo_t::finfo_t(fd_t fd, int flags, const char* name) :
	// file info
	fd(fd), flags(flags), offset(lseek(fd, 0, SEEK_END)), namelen(strlen(name)), name(name),

	// asynchronous i/o
	pending(false),
	cb(new struct aiocb),
	buf(new char[bufsz])
{
	if (!buf.get())
		err(EXIT_FAILURE, "cannot create finfo_t::buf");

	char* p = nullptr;
	commentsz = asprintf(&p, "\n==== %s ====\n\n", name);
	if (commentsz == -1 || !p)
		err(EXIT_FAILURE, "cannot create finfo_t(%s)", name);
	comment.reset(p);

	invariant();
}

inline finfo_t::finfo_t(finfo_t&& n) :
	// file info
	fd(n.fd), flags(n.flags), offset(n.offset), namelen(n.namelen), name(n.name),

	// prebuilt comment
	commentsz(n.commentsz), comment(std::move(n.comment)),
	nbytes(n.nbytes),

	// asynchronous i/o
	pending(n.pending),
	cb(new struct aiocb),
	buf(new char[bufsz])
{
	n.fd		= -1;
	n.name		= nullptr;
	n.offset	= -1;
	n.nbytes	= -1;
	n.pending	= false;
	std::swap(cb, n.cb);
	std::swap(buf, n.buf);

	invariant();
}

inline void finfo_t::invariant() const
{
#ifndef NDEBUG
	assert(fd == -1 || fd >= 0);
	assert((name == nullptr && namelen == std::numeric_limits<size_t>::max()) || strlen(name) == namelen);
	assert((comment.get() == nullptr && commentsz == -1) || strlen(comment.get()) == static_cast<size_t>(commentsz));
	assert(buf.get() && bufsz == FDBUF_SZ);
#endif
}

inline struct aiocb* finfo_t::fill_cb(int kq)
{
	invariant();

	bzero(cb.get(), sizeof(cb));
	cb.get()->aio_fildes = fd;
	cb.get()->aio_offset = offset;
	cb.get()->aio_buf    = buf.get();
	cb.get()->aio_nbytes = bufsz;
	cb.get()->aio_sigevent.sigev_notify_kqueue = kq;
	cb.get()->aio_sigevent.sigev_notify = SIGEV_KEVENT;
	cb.get()->aio_sigevent.sigev_value.sigval_int = fd;

	nbytes = 0;

	return cb.get();
}
