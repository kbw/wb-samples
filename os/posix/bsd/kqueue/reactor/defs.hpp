#pragma once

#define likely(x)	__builtin_expect(!!(x), 1) 
#define unlikely(x)	__builtin_expect(!!(x), 0) 

typedef int		fd_t;

constexpr int	FDBUF_SZ = 512 * 1024;
