#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/syscall.h>
#include <sys/sysproto.h>	/* struct mkdir_args */

/* mkdir system call hook. */
static int mkdir_hook(struct thread *td, void *syscall_args)
{
	struct mkdir_args /*{
		char    *path;
		int     mode;
	}*/ *uap = (struct mkdir_args *)syscall_args;

	char path[PATH_MAX];
	size_t done;
	int error;

	if ((error = copyinstr(uap->path, path, sizeof(path), &done)) != 0)
		return error;

	/* Print a debug message. */
	uprintf("The directory \"%s\" will be created with the following"
	    " permissions: %o\n", path, uap->mode);

	return sys_mkdir(td, syscall_args);
}

/* The function called at load/unload. */
static int load(struct module *module, int cmd, void *arg)
{
	int error = 0;

	switch (cmd) {
	case MOD_LOAD:
		/* Replace mkdir with mkdir_hook. */
		sysent[SYS_mkdir].sy_call = (sy_call_t *)mkdir_hook;
		break;

	case MOD_UNLOAD:
		/* Change everything back to normal. */
		sysent[SYS_mkdir].sy_call = (sy_call_t *)sys_mkdir;
		break;

	default:
		error = EOPNOTSUPP;
	}

	return error;
}

static moduledata_t mkdir_hook_mod = {
	"mkdir_hook",           /* module name */
	load,                   /* event handler */
	NULL                    /* extra data */
};

DECLARE_MODULE(mkdir_hook, mkdir_hook_mod, SI_SUB_DRIVERS, SI_ORDER_MIDDLE);
