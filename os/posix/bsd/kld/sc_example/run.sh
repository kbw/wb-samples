#!/bin/sh

cmd=$1
if [ -z $1 ]; then
	cmd=hello
fi

sudo kldload kld/sc_example.ko
# sudo kldstat -v
LD_LIBRARY_PATH=~/Code/git/wb-samples/samples/BSD/KLD/sc_example/sc_example prog/sc_example ${cmd}
sudo kldunload kld/sc_example.ko
