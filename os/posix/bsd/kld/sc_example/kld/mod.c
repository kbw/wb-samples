#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <bsm/audit_kevents.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>

/* The system call function. */
static int sc_example(struct thread* td, void* syscall_args)
{
	/* The system call's arguments. */
	struct args {
		const char* str;
	};
	struct args* const uap = (struct args*)syscall_args;

	char buf[255];
	size_t nbytes = 0;
	int ret = copyinstr(uap->str, buf, sizeof(buf), &nbytes);
	if (ret != 0)
		return ret;

	ret = uprintf("kld: sc_example: %s\n", uap->str);
	return ret == nbytes+17 ? 0 : ENOBUFS;
}

/* The sysent for the new system call. */
static struct sysent sc_example_sysent = {
	1,					/* number of arguments */
	sc_example			/* implementing function */
};

/* The offset in sysent[] where the system call is to be allocated. */
static int offset = NO_SYSCALL;

/* The function called at load/unload. */
static int load(struct module* module, int cmd, void* arg)
{
	int error = 0;

	switch (cmd) {
	case MOD_LOAD:
		uprintf("MOD_LOAD: offset=%d\n", offset);
		break;

	case MOD_UNLOAD:
		uprintf("MOD_UNLOAD: offset=%d\n", offset);
		break;

	case MOD_SHUTDOWN:
		uprintf("MOD_SHUTDOWN: offset=%d\n", offset);
		break;

	case MOD_QUIESCE:
		uprintf("MOD_QUIESCE: offset=%d\n", offset);
		break;

	default:
		error = EOPNOTSUPP;
	}

	return error;
}

SYSCALL_MODULE(sc_example, &offset, &sc_example_sysent, load, NULL);
