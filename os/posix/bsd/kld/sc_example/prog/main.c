#include <sc_example/sc_example.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	const char* msg = "hello world";
	int ret;

	if (argc > 1)
		msg = argv[1];

	ret = sc_example(msg);
	if (ret == -1) {
		fprintf(stderr, "call to sc_example failed: %d: %s: %s\n", errno, sc_example_errtext, strerror(errno));
		return 1;
	}

	printf("%s: sc_example: ret=%d\n", __FUNCTION__, ret);
	return 0;
}
