#include <unistd.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/syscall.h>
#include <sys/module.h>

const char* sc_example_errtext = "";

int sc_example(const char* str)
{
	int id;
	int syscall_num;
	struct module_stat stat;

	/* Determine sc_ecample's offset value. */
	if ((id = modfind("sys/sc_example")) == -1) {
		sc_example_errtext = "modfind failed";
		return -1;
	}

	stat.version = sizeof(stat);
	if (modstat(id, &stat) == -1) {
		sc_example_errtext = "modstat failed";
		return -1;
	}
	syscall_num = stat.data.intval;

	/* Make the call. */
	if (syscall(syscall_num, str) == -1) {
		sc_example_errtext = "syscall failed";
		return -1;
	}

	sc_example_errtext = "";
	errno = 0;
	return 0;
}
