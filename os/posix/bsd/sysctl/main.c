#include <unistd.h>
#include <stdio.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/user.h>

int MaxProcess()
{
	int mib[2], value, ret;
	size_t len;

	mib[0] = CTL_KERN;
	mib[1] = KERN_MAXPROC;
	len = sizeof(value);
	ret = sysctl(mib, 2, &value, &len, NULL, 0);
	printf("%d = sysctl(CTL_KERN, KERNL_MAXPROC, %d)\n", ret, value);

	return 0;
}

int KernQuantum()
{
	int mib[2], value, ret;
	size_t len;

	mib[0] = CTL_KERN;
//	mib[1] = KERN_QUANTUM;
	len = sizeof(value);
	ret = sysctl(mib, 2, &value, &len, NULL, 0);
	printf("%d = sysctl(CTL_KERN, KERN_QUANTUM, %d)\n", ret, value);

	return 0;
}

int GetProcessInfo(int pid)
{
	int mib[4], ret;
	size_t len;
	struct kinfo_proc value;

	mib[0] = CTL_KERN;
	mib[1] = KERN_PROC;
	mib[2] = KERN_PROC_PID;
	mib[3] = pid;
	len = sizeof(value);
	ret = sysctl(mib, 4, &value, &len, NULL, 0);
	printf("%d = sysctl(CTL_KERN, KERN_PROC)\n", ret);

	return 0;
}

int main()
{
	MaxProcess();
	KernQuantum();
	GetProcessInfo(getpid());	// current

	return 0;
}
