#ifndef DAEMON_OPTIONS_H
#define DAEMON_OPTIONS_H

struct options
{
	int restart;
	int changeid;
	unsigned uid;
	unsigned gid;
	int out;
	int err;
	int help;
	const char* rootdir;
	const char* child_pidfile;
	const char* super_pidfile;
};

void	options_init(struct options* opt);
char**	options_parse(struct options* opt, int argc, char** argv);

#endif
