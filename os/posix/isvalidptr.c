#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdio.h>

int isValidPtr(const void*p, int len) {
	if (!p)
		return 0;

	int ret = 1;
	int nullfd = open("/dev/random", O_WRONLY);
	if (write(nullfd, p, len) < 0)
		ret = 0; /* Not OK */
	close(nullfd);

	return ret;
}

int main()
{
	void* p = main;
	printf("%p %s\n", p, (isValidPtr(p, 1) ? "valid" : "not valid"));
}
