; https://en.wikipedia.org/wiki/X86_calling_conventions#cdecl
;
%include	'../../system.inc'

section	.data
	hello	db	'Hello, World!', 0Ah
	hbytes	equ	$-hello

section	.text
	global	_start

_start:
	push	dword hbytes
	push	dword hello
	push	dword stdout
	sys.write

	push	dword 0
	sys.exit
