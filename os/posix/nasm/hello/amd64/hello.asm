; https://en.wikipedia.org/wiki/X86_calling_conventions#System_V_AMD64_ABI
;
%include '../../system.inc'

section .data
	hello	db  'Hello, World!', 0Ah
	hbytes	equ  $-hello

section .text
	global	_start

_start:
	mov		rdi, stdout
	lea		rsi, [rel hello]
	mov		rdx, hbytes
	sys.write

	xor		rdi, rdi
	sys.exit
