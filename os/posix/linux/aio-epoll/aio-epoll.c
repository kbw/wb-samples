// https://blog.cloudflare.com/io_submit-the-epoll-alternative-youve-never-heard-about/

#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/syscall.h>
#include <linux/aio_abi.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define BUF_SZ (1 * 1024 * 1024 * 1024)
#define FILENAME "/dev/zero"

#define PFATAL(x...)                                     \
	do {                                                 \
		fprintf(stderr, "[-] SYSTEM ERROR : " x);        \
		fprintf(stderr, "\n\tLocation : %s(), %s:%u\n",  \
			__FUNCTION__, __FILE__, __LINE__);           \
		perror("      OS message ");                     \
		fprintf(stderr, "\n");                           \
		exit(EXIT_FAILURE);                              \
	} while (0)

inline static int io_setup(unsigned nr, aio_context_t *ctxp)
{
	return syscall(__NR_io_setup, nr, ctxp);
}

inline static int io_destroy(aio_context_t ctx)
{
	return syscall(__NR_io_destroy, ctx);
}

inline static int io_submit(aio_context_t ctx, long nr, struct iocb **iocbpp)
{
	return syscall(__NR_io_submit, ctx, nr, iocbpp);
}

/*
inline static int io_getevents(aio_context_t ctx, long min_nr, long max_nr,
					struct io_event *events, struct timespec *timeout)
{
	// This might be improved.
	return syscall(__NR_io_getevents, ctx, min_nr, max_nr, events, timeout);
}
 */

#define AIO_RING_MAGIC 0xa10a10a1
struct aio_ring {
	unsigned id; /** kernel internal index number */
	unsigned nr; /** number of io_events */
	unsigned head;
	unsigned tail;

	unsigned magic;
	unsigned compat_features;
	unsigned incompat_features;
	unsigned header_length; /** size of aio_ring */

	struct io_event events[0];
};

/* Stolen from kernel arch/x86_64.h */
#ifdef __x86_64__
 #define read_barrier() __asm__ __volatile__("lfence" ::: "memory")
#else
 #ifdef __i386__
  #define read_barrier() __asm__ __volatile__("" : : : "memory")
 #else
  #define read_barrier() __sync_synchronize()
 #endif
#endif

/* Code based on axboe/fio:
 * https://github.com/axboe/fio/blob/702906e9e3e03e9836421d5e5b5eaae3cd99d398/engines/libaio.c#L149-L172
 */
inline static int io_getevents(aio_context_t ctx, long min_nr, long max_nr,
					struct io_event *events, struct timespec *timeout)
{
	long i = 0;

	struct aio_ring *ring = (struct aio_ring *)ctx;
	if (ring == NULL || ring->magic != AIO_RING_MAGIC) {
		goto do_syscall;
	}

	for (; i < max_nr; ++i) {
		unsigned head = ring->head;
		if (head == ring->tail) {
			/* There are no more completions */
			break;
		}

		/* There is another completion to reap */
		events[i] = ring->events[head];
		read_barrier();
		ring->head = (head + 1) % ring->nr;
	}

	/*
     * Fall thru to syscall if i == 0 and there's no timeout, or a timeout
     * value.
	 */
	if (i == 0 && timeout != NULL &&
        timeout->tv_sec == 0 && timeout->tv_nsec == 0) {
		/* Requested non blocking operation. */
		return 0;
	}

	if (i && i >= min_nr) {
		return i;
	}

do_syscall:
	return syscall(__NR_io_getevents, ctx, min_nr - i, max_nr - i,
				&events[i], timeout);
}

int main()
{
	int fd = open(FILENAME, O_RDONLY);
	if (fd < 0)
		PFATAL("open(" FILENAME ")");

	char *buf = calloc(BUF_SZ, sizeof(char));

	aio_context_t ctx = 0;
	int r = io_setup(128, &ctx);
	if (r < 0)
		PFATAL("io_setup()");

	struct iocb cb = {	.aio_fildes = fd,
						.aio_lio_opcode = IOCB_CMD_PREAD,
						.aio_buf = (uint64_t)buf,
						.aio_nbytes = BUF_SZ };
	struct iocb *list_of_iocb[1] = {&cb};

	r = io_submit(ctx, 1, list_of_iocb);
	if (r != 1)
		PFATAL("io_submit()");
	printf("\n");

	struct io_event events[1] = {};
	r = io_getevents(ctx, 1, 1, events, NULL);
	if (r != 1)
		PFATAL("io_getevents()");
	printf("read %lld bytes from " FILENAME "\n", events[0].res);

    free(buf), buf = NULL;
	io_destroy(ctx);
	close(fd);

	return 0;
}
