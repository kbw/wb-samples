#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "tlpi_hdr.h"

/* Display information from inotify_event structure */
static void
displayInotifyEvent(int argc, char** argv, struct inotify_event *event)
{
//  printf("    wd =%2d; ", event->wd);
    if (event->cookie > 0)
        printf("cookie =%4d; ", event->cookie);

//  printf("mask = ");
    if (event->mask & IN_ACCESS)        printf("IN_ACCESS ");
    if (event->mask & IN_ATTRIB)        printf("IN_ATTRIB ");
    if (event->mask & IN_CLOSE_NOWRITE) printf("IN_CLOSE_NOWRITE ");
    if (event->mask & IN_CLOSE_WRITE)   printf("IN_CLOSE_WRITE ");
    if (event->mask & IN_CREATE)        printf("IN_CREATE ");
    if (event->mask & IN_DELETE)        printf("IN_DELETE ");
    if (event->mask & IN_DELETE_SELF)   printf("IN_DELETE_SELF ");
    if (event->mask & IN_IGNORED)       printf("IN_IGNORED ");
    if (event->mask & IN_ISDIR)         printf("IN_ISDIR ");
    if (event->mask & IN_MODIFY)        printf("IN_MODIFY ");
    if (event->mask & IN_MOVE_SELF)     printf("IN_MOVE_SELF ");
    if (event->mask & IN_MOVED_FROM)    printf("IN_MOVED_FROM ");
    if (event->mask & IN_MOVED_TO)      printf("IN_MOVED_TO ");
    if (event->mask & IN_OPEN)          printf("IN_OPEN ");
    if (event->mask & IN_Q_OVERFLOW)    printf("IN_Q_OVERFLOW ");
    if (event->mask & IN_UNMOUNT)       printf("IN_UNMOUNT ");
    printf(" on %s\n", argv[event->wd]);

    if (event->len > 0)
        printf("        name = %s\n", event->name);
}

#define PATH_MAX (4 * 1024)
#define BUF_LEN (10 * (sizeof(struct inotify_event) + PATH_MAX + 1))

int
main(int argc, char *argv[])
{
    int inotifyFd, wd, j;
    char buf[BUF_LEN] __attribute__ ((aligned(8)));
    ssize_t numRead;

    if (argc < 2 || strcmp(argv[1], "--help") == 0) {
        printf("%s pathname...\n", argv[0]);
        return 1;
    }

    inotifyFd = inotify_init1(IN_NONBLOCK | IN_CLOEXEC);
    if (inotifyFd == -1) {
        printf("inotify_init");
        return 1;
    }

    for (j = 1; j < argc; j++) {
        wd = inotify_add_watch(inotifyFd, argv[j], IN_ALL_EVENTS);
        if (wd == -1) {
            printf("inotify_add_watch");
            return 1;
        }

        printf("Watching %s using wd %d\n", argv[j], wd);
    }

    for (;;) {
        fd_set rfds;
        FD_ZERO(&rfds);
        FD_SET(inotifyFd, &rfds);

        printf("waiting\n");
        int nSelects = select(inotifyFd + 1, &rfds, NULL, NULL, NULL);
        if (nSelects == -1) {
            printf("select");
            break;
        }

        if (FD_ISSET(inotifyFd, &rfds)) {
            numRead = read(inotifyFd, buf, BUF_LEN);
            if (numRead == 0) {
                printf("read() from inotify fd returned 0!\n");
                return 1;
            }

            if (numRead == -1) {
                printf("read\n");
                break;
            }

            //printf("Read %zd bytes from inotify fd\n", numRead);

            /* Process all of the events in buffer returned by read() */
            struct inotify_event *event;
            char* p;
            for (p = buf; p < buf + numRead; p += sizeof(*event) + event->len) {
                event = (struct inotify_event *) p;

                if (event->mask & IN_MODIFY)
                    displayInotifyEvent(argc, argv, event);
//                  printf("IN_MODIFY on %s\n", argv[event->wd]);
            }
        } /* FD_ISSET */
    }

    return 0;
}