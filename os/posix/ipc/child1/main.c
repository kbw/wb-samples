/*
 * Taken from Embedded FreeBSD Cookbook
 * Examples 2-1, 2-2
 */

#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>

int main(int argc, char* argv[], char* env[])
{
	pid_t	pid;
	int	status;

	if ((pid = fork()) > 0)
	{
		printf("%d: waiting for child\n", pid);
		wait(&status);
		printf("%d: child status = %d\n", pid, status);
	}
	else if (pid == 0)
	{
		execve("/bin/date", argv, env);
	}

	return 0;
}
