#include "utils.hpp"

#include <unistd.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/wait.h>

#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <utility>
#include <memory>

namespace
{
	typedef int fd_t;
	typedef fd_t pipe_t[2];

	constexpr int RD = 0;
	constexpr int RW = 1;
}

int main(int argc, char* argv[])
{
	const size_t nchildren{ argc == 1 ? 1 : (size_t)std::max(atoi(argv[1]), 1) };

	// create i/o buffer
	const size_t BUFSZ = 20 + 10*nchildren;
	std::unique_ptr<char[]> wrapped_buf{new char[BUFSZ]};
	char* buf{ wrapped_buf.get() };
	ssize_t nbytes{};

	// parent pipes
	pipe_t out, in;
	if (pipe(out) == -1 || pipe(in) == -1) exit(2);

	// child interconnect pipes
	pipe_t* interconnect = (pipe_t*)calloc(nchildren-1, sizeof(pipe_t));
	for (size_t i = 0; i < nchildren-1; ++i)
		if (pipe(interconnect[i]) == -1) exit(2);

	// create children
	std::vector<pid_t> pids(nchildren, -1);
	for (size_t i = 0; i < nchildren; ++i) {
		if ((pids[i] = fork()) == 0) {
			// select pipes
			pipe_t& childin { i == 0           ? in  : interconnect[i-1] };
			pipe_t& childout{ i == nchildren-1 ? out : interconnect[i] };

			// map pipes stdin, stdout
			while (dup2(childout[RW], STDOUT_FILENO) == -1 && errno == EINTR);
			while (dup2(childin[RD], STDIN_FILENO) == -1 && errno == EINTR);
			dup2(childout[RW], STDOUT_FILENO);
			dup2(childin[RD], STDIN_FILENO);

			// close all pipes, no longer need them
			close(out[RD]); close(in[RW]); out[RD] = in[RW] = -1;
			close(out[RW]); close(in[RD]); out[RW] = in[RD] = -1;
			for (size_t i = 0; i < nchildren-1; ++i)
				close(interconnect[i][RD]), close(interconnect[i][RW]);
			free(interconnect), interconnect = nullptr;

			// run program
			nbytes = read(STDIN_FILENO, buf, BUFSZ);
			if (nbytes == -1) {
				pwrite(STDOUT_FILENO, "ERROR:%d ", getpid());
				exit(4);
			}

			nbytes += snprintf(buf + nbytes, BUFSZ - (size_t)nbytes, "%d ", getpid());
			write(STDOUT_FILENO, buf, (size_t)nbytes);
			exit(0);
		} // end child
		else if (pids[i] == -1)
			abort();
	} // for each pid

	// close all pipes except ends of the chain
	close(out[RW]); close(in[RD]); out[RW] = in[RD] = -1;
	for (size_t i = 0; i < nchildren-1; ++i)
		close(interconnect[i][RD]), close(interconnect[i][RW]);
	free(interconnect), interconnect = nullptr;

	// from parent side, pipes have the wrong names
	std::swap(in, out);

	// run program
	pwrite(out[RW], "child pids: ");
	nbytes = read(in[RD], buf, BUFSZ);
	struct iovec iov[] = {
		{ (void*)buf, (size_t)nbytes },
		{ (void*)"\n", 1 }
	};
	writev(STDOUT_FILENO, iov, 2);

	// done
	int status{};
	for (auto pid : pids) waitpid(pid, &status, 0);
}
