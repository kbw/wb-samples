#include "utils.hpp"

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <memory>

ssize_t pwrite(int fd, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	char* buf{ nullptr };
	int len{ asprintf(&buf, fmt, args) };
	va_end(args);

	if (!buf || len == -1) return -1;
	std::unique_ptr<char, decltype(&free)> ptr(buf, &free);
	return write(fd, buf, (size_t)len);
}
