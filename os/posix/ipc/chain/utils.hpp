#pragma once

#include <sys/types.h>

ssize_t pwrite(int fd, const char* fmt, ...) __attribute__((format(printf, 2, 3)));
