#include <unistd.h>
#include <stdlib.h>
#include <poll.h>
#include <math.h>
#include <stdio.h>

struct ctx_t {
		pid_t pid;
		int s;
		double value;
};

double calc_e() {
	return expf(1.0);
}

double calc_pi() {
	return acos(-1);
}

void start_children(struct ctx_t* e, struct ctx_t* pi) {
	e->value = pi->value = 0.0;
	e->s = pi->s = -1;

	e->pid = fork();
	if (e->pid == 0) {
		/* running proc_e child code */
		sleep(1);
		char buf[16];
		snprintf(buf, sizeof(buf), "%f", calc_e());
		exit(0);
	}

	pi->pid = fork();
	if (pi->pid == 0) {
		/* running proc_e child code */
		sleep(1);
		char buf[16];
		snprintf(buf, sizeof(buf), "%f", calc_pi());
		exit(0);
	}
}

void get_with_poll(struct ctx_t* e, struct ctx_t* pi) {
	struct pollfd items[2] = {
		{ -1, POLLIN, 0 },
		{ -1, POLLIN, 0 }
	};
}

int main() {
	struct ctx_t e, pi;
	start_children(&e, &pi);
	get_with_poll(&e, &pi);

	printf("e=%f\npi=%f\n", e.value, pi.value);
}
