/*
 * Taken from Embedded FreeBSD Cookbook
 * Examples 2-1, 2-2
 */

#include "daemon.h"
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <strings.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[], char* env[])
{
	int listenfd;
	int s;
	struct sockaddr_in addr;
	socklen_t addr_len = sizeof(addr);
	char buf[64];

	init_daemon();

	listenfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_port = atoi(argv[1]);
	bind(listenfd, (struct sockaddr*)&addr, sizeof(addr));

	listen(listenfd, 5);

	while ((s = accept(listenfd, (struct sockaddr*)&addr, &addr_len)) >= 0)
	{
		int pid = fork();
		int n;

		if (pid > 0)
		{
			close(s);
			addr_len = sizeof(addr_len);
			continue;
		}

		n = read(s, buf, sizeof(buf));
		if (0 <= n && n < sizeof(buf))
		{
			write(s, "accepted ...", 13);
		}
		else
		{
			write(s, "rejected ...", 13);
		}
		close(s);
		s = -1;
	}

	return 0;
}
