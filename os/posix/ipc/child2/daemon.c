/*
 * Taken from Embedded FreeBSD Cookbook
 * Examples 2-1, 2-2
 */

#include "daemon.h"

#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <signal.h>
#include <strings.h>
#include <stdlib.h>

static void handle_sigchld()
{
	int pid;
	int status;

	while ((pid = wait3(&status, WNOHANG, NULL)) > 0)
		;
}

void init_daemon()
{
	int childpid = 0;
	int fd = 0;
	struct rlimit limits;

	/*
	 *	create a child process
	 */
	if ((childpid = fork()) < 0)
	{
		/* handle the error condition */
		exit(-1);
	}
	else if (childpid > 0)
	{
		/* this is the parent, we may successfully exeit */
		exit(0);
	}

	/* now executing as the child process */

	/* become the session leader */
	setsid();

	/*
	 *	close all fds, need to get the maximum number of open files
	 */
	bzero(&limits, sizeof(limits));
	getrlimit(RLIMIT_NOFILE, &limits);
	for (fd = 0; fd < limits.rlim_max; ++fd)
	{
		close(fd);
	}

	/*
	 *	release cwd
	 */
	chdir("/");
	
	/*
	 *	reset umask
	 */
	umask(0);

	/*
	 *	handle child termiations
	 */
	signal(SIGCHLD, handle_sigchld);
}
