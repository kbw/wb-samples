/* equivalent to "sort < file1 | uniq" */
/* http://www.cs.utoronto.ca/~maclean/csc209/Fall98/Slides/Week8/tsld007.htm */
int main()
{
	int fd[2];
	FILE* fp = fopen( "file1", "r" );
	dup2( fileno(fp), fileno(stdin) );
	fclose( fp );
	pipe( fp );
	if ( fork() == 0 ) {
		dup2( fd[1], fileno(stdout) );
		close( fd[0] ); close( fd[1] );
		execl( "/usr/bin/sort", "sort", (char*)0 ); exit( 2 );
	}
	else {
		dup2( fd[0], fileno(stdin) );
		close( fd[0] ); close( fd[1] );
		execl( "/usr/bin/uniq", "uniq", (char*)0 ); exit( 3 );
	}
}
