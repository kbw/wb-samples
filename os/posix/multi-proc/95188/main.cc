// http://www.cplusplus.com/forum/unices/95188/

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

using std::cout;
using std::endl;
using std::ifstream;


int main(int argc, char* argv[]) {
	// check proper usage
	if (argc != 2) {
		cout << "Usage: " << argv[0] << " input_filename" << endl;
		exit(1);
	}

	// variables global to main()
	char fileName[30];
	ifstream fin;
	char buffer[30];
	pid_t pid;

	// open input file
	strncpy(fileName, argv[1], sizeof fileName);
	fin.open(fileName);
	if (fin.fail()) {
		cout << "Could not open " << fileName << endl;
		exit(2);
	}

	// for each line create process and replace core image
	cout << "********************* START *********************" << endl;
	fin.getline(buffer, sizeof buffer);
	while (!fin.eof()) {

		cout << "size[" << strlen(buffer) << "], command: " 
                    << buffer << endl;

		if ((pid = fork()) < 0) {
			cout << "Fork failed." << endl;
			exit(3);
		}
		else if (pid > 0) {
			waitpid(pid, NULL, 0);
			cout << endl;
		}
		else {
			char* const args[] = {buffer, (char*) 0};
			if (execv(buffer, args) == -1) {
				cout << "Error in execv(): " << errno << ": " << strerror(errno)  << endl;
				exit(4);
			}
		}

		fin.getline(buffer, sizeof buffer);
	}

	cout << "*********************  END  *********************" << endl;
	fin.close();
	return 0;
}
