// usage: run "prog arg args ..." "prog2 arg arg ..."
#include <unistd.h>
#include <sys/wait.h>
#include <sys/errno.h>

#include <string>
#include <vector>
#include <list>
#include <algorithm>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef std::vector<std::string> prog_t;
typedef std::list<prog_t> progs_t;
typedef std::vector<pid_t> pids_t;

int wait(const pids_t& pids);
void run(pids_t& pids, const prog_t& prog);
prog_t split(const char* in);

int main(int argc, char* argv[])
{
	progs_t progs;
	std::for_each(argv + 1, argv + argc, [&](char *arg) { progs.push_back(split(arg)); });

	pids_t pids;
	for (auto& prog : progs) run(pids, prog);

	return wait(pids);
}

int wait(const pids_t& pids)
{
	int ret = 0;

	int stat;
	for (pid_t pid : pids) {
		waitpid(pid, &stat, 0);
		ret = ret || WEXITSTATUS(stat);
	}

	return ret;
}

void run(pids_t& pids, const prog_t& prog)
{
	int pid = fork();
	if (pid == 0) {
		// build arg vector
		std::vector<char*> args;
		for (const std::string& arg : prog) args.push_back(strdup(arg.c_str()));
		args.push_back(nullptr);

		execv(args[0], args.data()); // if exec works, this program instance ends here
		for (char* arg : args) free(arg);
		fprintf(stderr, "%s: execv failed: %s\n", args[0], strerror(errno));
	}
	else if (pid > 0)
		pids.push_back(pid);
}

prog_t split(const char* in)
{
	prog_t out;

	for (const char *begin = in, *end; ; begin = end + 1) {
		while (isspace(*begin)) ++begin; // skip spaces

		end = strchr(begin, ' ');
		if (!end) {
			if (*begin) out.emplace_back(begin); // append final string
			break;
		}

		out.emplace_back(begin, end);
	}

	return out;
}
