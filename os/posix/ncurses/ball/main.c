#include <ncurses.h>
#include <unistd.h>

#define DELAY (30*1000)

int main()
{
	int x = 0, y = 0;
	int dx = 0, dy = 0;

	initscr();
	noecho();
	curs_set(FALSE);


	while (1)
	{
		clear();

		int max_x = 0, max_y = 0;
		getmaxyx(stdscr, max_y, max_x);

		if (x <= 0)
			dx = 1;
		if (y <= 0)
			dy = 1;
		mvprintw(y += dy, x += dx, "+");
		if (x >= max_x) {
			x = max_x;
			dx = -1;
		}
		if (y >= max_y) {
			y = max_y;
			dy = -1;
		}

		refresh();

		usleep(DELAY);
	}

	endwin();
}
