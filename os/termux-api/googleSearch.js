var querystring = require('querystring')
var http = require('http')

var postData = querystring.stringify({ 'msg' : 'pompeii' });

var options = {
	hostname: 'www.google.com',
	port: 80,
	path: '/upload',
	method: 'POST',
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Content-Length': Buffer.byteLength(postData)
	}
};

var req = http.request(options, function(res) {
	console.log(`STATUS: ${res.statusCode}`);
	console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
	res.setEncoding('utf8');
	res.on('data', function(chunk) {
		console.log(`${chunk}`);
	});
	res.on('end', function() {
		console.log('No more data in response.');
	});
});

req.on('error', function(e) {
	console.log(`problem with request: ${e.message}`);
});

req.write(postData);	// write data to request body
req.end();
