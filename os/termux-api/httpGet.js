var http = require('http');

var page = http.request({ hostname: 'www.guardian-unlimited.com' }, function(res) {
	console.log(`STATUS: ${res.statusCode}`);
	console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
	res.setEncoding('utf8');
	res.on('data', function(chunk) {
		console.log(chunk);
	});
	res.on('end', function() {
		console.log('done');
	});
}).end();

//console.log(page.headers)
