var api = require('termux-api').default;

let result = api.createCommand()
	.contactList()
	.build()
	.run();

result.getOutputObject()
    .then(function(contactList) {
		console.log('Last known location: ', contactList);
	});
