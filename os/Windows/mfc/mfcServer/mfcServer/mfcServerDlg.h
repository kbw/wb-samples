
// mfcServerDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CmfcServerDlg dialog
class CmfcServerDlg : public CDialog
{
// Construction
public:
	CmfcServerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MFCSERVER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	void UpdateClientListBox();

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CListBox m_ClientIdList;
	CListBox m_ClientAddrList;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedManualUpdate();
};
