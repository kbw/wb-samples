// mfcServerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "mfcServer.h"
#include "mfcServerDlg.h"

#include <process.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const UINT UODATE_LISTBOX = 2;


// CmfcServerDlg dialog
CmfcServerDlg::CmfcServerDlg(CWnd* pParent /*=NULL*/) :
	CDialog(CmfcServerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CmfcServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_CLENTIP, m_ClientIdList);
	DDX_Control(pDX, IDC_LIST_CLIENTADDR, m_ClientAddrList);
}

BEGIN_MESSAGE_MAP(CmfcServerDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
	ON_BN_CLICKED(ID_MANUALUPDATE, &CmfcServerDlg::OnBnClickedManualUpdate)
END_MESSAGE_MAP()


void CmfcServerDlg::UpdateClientListBox()
{
	if (m_ClientAddrList.GetCount() != clients.size())
	{
		m_ClientAddrList.ResetContent();
		for (Clients::const_iterator p = clients.begin(); p != clients.end(); ++p)
			m_ClientAddrList.AddString(p->addr.c_str());
	}
}

// CmfcServerDlg message handlers

BOOL CmfcServerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	unsigned short port = 7171;
	unsigned tid;
	_beginthreadex(NULL, 0, tcp_server, reinterpret_cast<void*>(port), 0, &tid);

	DWORD dwTimer = 5;
	SetTimer(UODATE_LISTBOX, 1000*dwTimer, 0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CmfcServerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CmfcServerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CmfcServerDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == UODATE_LISTBOX)
		UpdateClientListBox();

	CDialog::OnTimer(nIDEvent);
}

void CmfcServerDlg::OnBnClickedManualUpdate()
{
	UpdateClientListBox();
}
