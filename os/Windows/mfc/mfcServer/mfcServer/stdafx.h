// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#undef _UNICODE
#undef UNICODE

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

//#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif

#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

//#include <afxcontrolbars.h>     // MFC support for ribbons and control bars

#undef min
#undef max


// Shared code begins here
#include <winsock2.h>
#include <string>
#include <list>

class CriticalSection
{
	CRITICAL_SECTION m_cs;

public:
	CriticalSection()	{ InitializeCriticalSection(&m_cs); }
	~CriticalSection()	{ DeleteCriticalSection(&m_cs); }

	void Lock()			{ EnterCriticalSection(&m_cs); }
	void Unlock()		{ LeaveCriticalSection(&m_cs); }
};

template <typename T>
class Mutex
{
	T m_mtx;
public:
	friend class Lock;
	class Lock
	{
		Mutex&	m_mtx;
	public:
		explicit Lock(Mutex& mtx) : m_mtx(mtx)	{ m_mtx.m_mtx.Lock(); }
		~Lock()	{ m_mtx.m_mtx.Unlock(); }
	};

	Mutex()		{}
	~Mutex()	{}
};

struct ClientInfo
{
	SOCKET sock;
	std::string addr;
};

typedef std::list< ClientInfo > Clients;
extern Clients clients;
extern Mutex<CriticalSection> client_cs;

unsigned __stdcall tcp_server(void *param);


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif