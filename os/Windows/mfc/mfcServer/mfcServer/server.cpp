#include "stdafx.h"
#include <process.h>
#include <string.h>
#include <algorithm>

#pragma comment(lib, "ws2_32.lib")


// Shared data: collection of clients and lock
Clients clients;
Mutex<CriticalSection> client_cs;


// handle client connection in sperate thread
unsigned __stdcall tcp_servers_client(void *param)
{
	SOCKET s = reinterpret_cast<SOCKET>(param);

	char buf[128];
	memset(buf, 0, sizeof(buf));

	// Just echo with 15 sec delay
	recv(s, buf, sizeof(buf), 0);
	Sleep(15 * 1000);
	send(s, buf, static_cast<int>(std::min(sizeof(buf), strlen(buf) + 1)), 0);

	// close connection
	Mutex<CriticalSection>::Lock lock(client_cs);
	for (Clients::iterator p = clients.begin(); p != clients.end(); ++p)
	{
		if (p->sock == s)
		{
			closesocket(s);
			clients.erase(p);
			break;
		}
	}

	return 0;
}


// tcp server main thread, spawned by GUI
unsigned __stdcall tcp_server(void *param)
{
	int port = 7171;
	if (param)
		port = reinterpret_cast<short>(param);

	SOCKET s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s == -1)
	{
		closesocket(s);
		return 1;
	}

	sockaddr_in addr_in;
	addr_in.sin_family = AF_INET;
	addr_in.sin_addr.s_addr = INADDR_ANY;
	addr_in.sin_port = htons(port);
	int bind_ret = bind(s, (sockaddr*)&addr_in, sizeof(addr_in));           
	if (bind_ret == -1)
	{
		closesocket(s);
		return 1;
	}

	int listen_ret = listen(s, 5); 
	if (listen_ret == -1)
	{
		closesocket(s);
		return 1;
	}

	while (true)
	{
		sockaddr_in client_addr;
		int len = sizeof(client_addr);
		SOCKET client_sock = accept(s, (sockaddr*)&client_addr, &len);

		ClientInfo info;
		info.sock = client_sock;
		info.addr = inet_ntoa(client_addr.sin_addr);
		{
			Mutex<CriticalSection>::Lock lock(client_cs);
			clients.push_back(info);
		}

		unsigned tid;
		_beginthreadex(NULL, 0, tcp_servers_client, reinterpret_cast<void*>(client_sock), 0, &tid);
	}

	closesocket(s);
	return 0;
}
