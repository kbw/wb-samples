// Windows/POSIX support
// C++14 for VS compatibility
// older includes for VS compatibility
#include <iomanip>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

// socket stuff
#if defined(_WIN32) || defined(_WIN64)
#define WIN32_LEAN_AND_MEAN 1
#include <WinSock2.h>
#include <ws2tcpip.h>
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "AdvApi32.lib")
class WsaWrapper {
public:
	WsaWrapper() {
		auto rc = WSAStartup(MAKEWORD(2, 2), &data_);
		if (rc != 0)
			throw std::runtime_error("Cannot initialize WinSock");
	}
	~WsaWrapper() { WSACleanup(); };
	WsaWrapper(const WsaWrapper& n) = delete;
	WsaWrapper& operator=(const WsaWrapper& n) = delete;
private:
	WSADATA data_;
};
using socket_t = SOCKET;
using ssize_t = int;
#else
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
using socket_t = int;
#endif

namespace simple_socket {
	socket_t connectTo(const char* host, short port);
	void closeSocket(socket_t s);

	class ClientSocket {
	public:
		ClientSocket(int argc, char* argv[]) : s_(static_cast<socket_t>(-1)) {
			const char* host = "127.0.0.1";
			short port = 27015;
			if (argc > 1)
				host = argv[1];
			if (argc > 2)
				port = static_cast<short>(atoi(argv[2]));
			s_ = connectTo(host, port);
		}
		~ClientSocket() {
			closeSocket(s_);
		}
		ClientSocket() = delete;
		ClientSocket(const ClientSocket& n) = delete;
		ClientSocket& operator=(const ClientSocket& n) = delete;

		socket_t socket() { return s_; }
	private:
		socket_t s_;
	};

	socket_t connectTo(const char* host, short port) {
		socket_t s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (s < 0)
			throw std::runtime_error("Cannot create socket");

		sockaddr_in addr;
		memset(&addr, 0, sizeof(addr));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		int rc = inet_pton(AF_INET, host, &addr.sin_addr);
		if (rc != 1)
			throw std::runtime_error(std::string("Cannot accept address: ") + host);

		rc = connect(s, reinterpret_cast<sockaddr*>(&addr), sizeof(addr));
		if (rc != 0)
			throw std::runtime_error(std::string("Cannot connect to: ") + host + std::string(":") + std::to_string(port));

		return s;
	}

	void closeSocket(socket_t s) {
#if defined(_WIN32) || defined(_WIN64)
		closesocket(s);
#else
		close(s);
#endif
	}
} // simple_socket

// RCON stuff
namespace rcon {
	using id_t = int32_t;

	enum class PacketType : int32_t {
		SERVERDATA_AUTH = 3,
		SERVERDATA_AUTH_RESPONSE = 2,
		SERVERDATA_EXECCOMMAND = 2,
		SERVERDATA_RESPONSE_VALUE = 0
	};

#pragma pack(push) // save old alignment
#pragma pack(1) // need byte alignment
	struct Packet {
		id_t id;
		PacketType type;
		char body[];
	};
#pragma pack(pop)  // restore alignment

	inline std::pair<std::unique_ptr<char[]>, size_t> createPacket(id_t id, PacketType type, const std::string& body) {
		size_t size = sizeof(id) + sizeof(type) + body.size() + 2; // two trailing nulls
		auto raw_buffer = std::make_unique<char[]>(size);
		Packet* packet = reinterpret_cast<Packet*>(raw_buffer.get());
		memset(packet, 0, size);
		packet->id = id;
		packet->type = type;
		memcpy(packet->body, body.data(), body.size());
		return { std::move(raw_buffer), size };
	}

	std::pair<std::unique_ptr<char[]>, size_t> createServerDataAuth(id_t id, const std::string& password) {
		return createPacket(id, PacketType::SERVERDATA_AUTH, password);
	}

	std::pair<std::unique_ptr<char[]>, size_t> createServerExecCmd(id_t id, const std::string& cmd) {
		return createPacket(id, PacketType::SERVERDATA_EXECCOMMAND, cmd);
	}
} // namespace rcon

std::pair<bool, std::string> serverDataAuth(simple_socket::ClientSocket& s, rcon::id_t id, const std::string& password) {
	auto msg = rcon::createServerDataAuth(id, password);
	ssize_t nbytes = ::send(s.socket(), msg.first.get(), msg.second, 0);
	if (rcon::id_t(nbytes) != msg.second)
		return { false, "Failed to send ServerDataAuth: id=" + std::to_string(id) + " password=" + password };

	const size_t bufsz = 64;
	char buf[bufsz];
	memset(buf, 0, bufsz);
	nbytes = ::recv(s.socket(), buf, bufsz, 0);
	if (nbytes == -1)
		return { false, "Failed to receive ServerDataAuth response" };

	const rcon::Packet* packet = reinterpret_cast<const rcon::Packet*>(buf);
	if (packet->id == -1)
		return { false, "id is -1" };
	if (packet->type != rcon::PacketType::SERVERDATA_AUTH_RESPONSE)
		return { false, "Unexpected reply message type: " + std::to_string(static_cast<int>(packet->type)) };
	return { true, "ok" };
}

std::pair<bool, std::string> serverExecCmd(simple_socket::ClientSocket& s, rcon::id_t id, const std::string& cmd, std::string& reply) {
	auto msg = rcon::createServerExecCmd(id, cmd);
	ssize_t nbytes = ::send(s.socket(), msg.first.get(), msg.second, 0);
	if (rcon::id_t(nbytes) != msg.second)
		return { false, "Failed to send ServerExecCmd: id=" + std::to_string(id) + " command=" + cmd };

	const size_t bufsz = 1024;
	char buf[bufsz];
	memset(buf, 0, bufsz);
	nbytes = ::recv(s.socket(), buf, bufsz, 0);
	if (nbytes == -1)
		return { false, "Failed to receive ServerExecCmd response" };

	const rcon::Packet* packet = reinterpret_cast<const rcon::Packet*>(buf);
	if (packet->id == -1)
		return { false, "id is -1" };
	if (packet->type != rcon::PacketType::SERVERDATA_RESPONSE_VALUE)
		return { false, "Unexpected reply message type: " + std::to_string(static_cast<int>(packet->type)) };
	reply = packet->body;
	return { true, "ok" };
}

int main(int argc, char* argv[])
try {
#if defined(_WIN32) || defined(_WIN64)
	WsaWrapper wsa;
#endif
	simple_socket::ClientSocket s(argc, argv);

	rcon::id_t id{ 7 }; // arbitrary number
	auto rc = serverDataAuth(s, id, "password");
	std::cout << "serverDataAuth returned state="
		<< std::boolalpha << rc.first
		<< " message=" << rc.second
		<< std::endl;

	if (rc.first) {
		// send random command, now that we're connected
		std::string reply;
		rc = serverExecCmd(s, id, "sleep", reply);
		std::cout << "serverExecCmd returned state="
			<< std::boolalpha << rc.first
			<< " message=" << rc.second
			<< " reply=" << reply
			<< std::endl;
	}
}
catch (const std::exception& e) {
	std::clog << "fatal: " << e.what() << std::endl;
}