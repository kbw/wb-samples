/*
 *      Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 *
 * This software is provided by Sun ``AS IS'' and any express or implied
 * warranties, including, but not limited to, the implied warranties of
 * merchantability and fitness for a particular purpose are disclaimed.
 * In no event shall Sun Microsystems be liable for any direct, indirect,
 * incidental, special, exemplary, or consequential damages.
 *
 * This software is not a product, and is provided for evaluation purposes
 * only.
 *
 * This software may not be resold without the express permission of
 * Sun Microsystems.
 */

#pragma ident	"@(#)sctp_client.c	1.1	04/01/27 SMI"

/*
 * Simple networking client which uses SCTP sockets.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/filio.h>
#include <netinet/sctp.h>

int
main(int argc, char **argv)
{
	int s, af, tmp, i;
	struct in6_addr a6;
	struct in_addr a;
	struct hostent *hp;
	int port;
	char data[8192];
	struct sockaddr_in6 des6;
	struct sockaddr_in dest;
	struct pollfd pfds[2];

	if (argc < 3) {
		printf("Usage %s desthost destport\n", argv[0]);
		exit(1);
	}

	if (inet_pton(AF_INET6, argv[1], &a6) == 1) {
		af = AF_INET6;
	} else if (inet_pton(AF_INET, argv[1], &a) == 1) {
		af = AF_INET;
	} else if ((hp = getipnodebyname(argv[1], AF_INET6, 0,
			&tmp)) != NULL) {
		af = AF_INET6;
		bcopy(hp->h_addr_list[0], &a6, sizeof (a6));
	} else if ((hp = gethostbyname(argv[1])) != NULL) {
		af = AF_INET;
		bcopy(hp->h_addr_list[0], &a, sizeof (a));
	} else {
		printf("Couldn't resolve hostname: %s\n", argv[1]);
		exit(1);
	}

	port = atoi(argv[2]);
	if (!port) {
		printf("Couldn't parse destination port: %s\n", argv[2]);
		exit(1);
	}

	s = socket(af, SOCK_STREAM, IPPROTO_SCTP);
	if (s < 0) {
		perror("socket()");
		exit(1);
	}
	if (af == AF_INET6) {
		bzero(&des6, sizeof (des6));

		des6.sin6_family = AF_INET6;
		des6.sin6_addr = a6;
		des6.sin6_port = htons(port);

		inet_ntop(AF_INET6, &a6, data, sizeof (data));
		printf("Connect v6 to %s/%d\n", data, port);

		if (connect(s, (struct sockaddr *)&des6, sizeof (des6)) < 0) {
			perror("connect");
			exit(1);
		}
	} else {
		bzero(&dest, sizeof (dest));

		dest.sin_family = AF_INET;
		dest.sin_addr = a;
		dest.sin_port = htons(port);

		inet_ntop(AF_INET, &a, data, sizeof (data));
		printf("Connect v4 to %s/%d\n", data, port);

		if (connect(s, (struct sockaddr *)&dest, sizeof (dest)) < 0) {
			perror("connect");
			exit(1);
		}
	}

	bzero(pfds, sizeof (pfds));
	pfds[0].fd = 0;
	pfds[0].events = POLLIN;
	pfds[1].fd = s;
	pfds[1].events = POLLIN;

	while (1) {
		tmp = poll(pfds, 2, -1);
		if (tmp < 0) {
			perror("poll()");
			exit(1);
		}
		for (i = 0; i < 2; i++) {
			if (tmp == 0) {
				continue;
			}
			if (pfds[i].revents) {
				tmp = read(pfds[i].fd, data, sizeof (data));
				if (tmp < 0) {
					perror("read()");
					exit(1);
				} else if (tmp == 0) {
					exit(0);
				} else {
					if (write(pfds[(i + 1) % 2].fd, data,
						tmp) < 0) {
						perror("write()");
						exit(1);
					}
				}
			}
		}
	}
	return (0);
}
