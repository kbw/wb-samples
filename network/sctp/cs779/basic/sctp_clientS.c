/*
 * Simple networking client which uses SCTP sockets.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/filio.h>
#include <netinet/sctp.h>

#define MAXLINE 1024
#define INFTIM -1

int
main(int argc, char **argv)
{
	int s,  tmp, i;
	struct hostent *hp;
        int error;
	int port;
	char data[8192];
	struct sockaddr_in dest;
	struct pollfd pfds[2];


	if (argc < 3) {
		printf("Usage %s desthost destport\n", argv[0]);
		exit(1);
	}


        bzero(&dest, sizeof (dest));
        hp = getipnodebyname(argv[1], AF_INET, AI_DEFAULT, &error);
        if (hp == NULL) {
                fprintf(stderr, "host not found\n");
                exit(1);
        }
	port = atoi(argv[2]);
        dest.sin_family = AF_INET;
        dest.sin_addr.s_addr = *(ipaddr_t *)hp->h_addr_list[0];
        dest.sin_port = htons(port);



	s = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP);

	if (connect(s, (struct sockaddr *)&dest, sizeof (dest)) < 0) {
			perror("connect");
			exit(1);
	}


        bzero(pfds, sizeof (pfds));
        pfds[0].fd = 0;
        pfds[0].events = POLLIN;
        pfds[1].fd = s;
        pfds[1].events = POLLIN;

        while (1) {
printf("\n");
                tmp = poll(pfds, 2, INFTIM);
                if (tmp < 0) {
                        perror("poll()");
                        exit(1);
                }
                for (i = 0; i < 2; i++) {
                        if (tmp == 0) {
                                continue;
                        }
                        if (pfds[i].revents) {
                                tmp = read(pfds[i].fd, data, sizeof (data));
                                printf("Read from %d \n", pfds[i].fd); 
                             //   printf("%s\n", data);
                                if (tmp < 0) {
                                        perror("read()");
                                        exit(1);
                                } else if (tmp == 0) {
                                        exit(0);
                                } else {
                                        if (write(pfds[(i + 1) % 2].fd, data, tmp) < 0) {
                                                perror("write()");
                                                exit(1);
                                        }
                                printf("Wrote to %d:\n", pfds[(i + 1) % 2].fd); 
                              //  printf("%s\n", data);
                                }
                        }
                }
        }
        return (0);
}

