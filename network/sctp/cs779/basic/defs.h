#ifndef DEFS_H
#define DEFS_H

#include <unistd.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>

#include <arpa/inet.h>
#include <netinet/sctp.h>
#include <netdb.h>

#ifndef h_addr
#define h_addr h_addr_list[0] /* for backward compatibility */
#endif

#ifndef AI_DEFAULT
#define AI_DEFAULT (AI_ADDRCONFIG | AI_V4MAPPED)

struct hostent *
getipnodebyname(const char	*name, int af, int flags, int *error_num);

struct hostent *
getipnodebyaddr(const void	*src, size_t len, int af, int *error_num);

void
freehostent(struct	hostent	*ptr);
#endif

#include <stdio.h>
#include <stdlib.h>

#endif
