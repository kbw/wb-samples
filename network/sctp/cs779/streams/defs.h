#ifndef DEFS_H
#define DEFS_H

#include <unistd.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/sctp.h>
#include <netdb.h>

#ifndef h_addr
#define h_addr h_addr_list[0] /* for backward compatibility */
#endif

#include <stdio.h>
#include <stdlib.h>

#endif
