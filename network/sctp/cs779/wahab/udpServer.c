/*
NAME:        udpServer0 
SYNOPSIS:    udpServer0 
DESCRIPTION:    The program creates a datagram socket in the inet 
                domain, binds it to port 10111 and receives any message
                arrived to the socket and prints it out
*/

#include "defs.h"
#include "util.h"

int main(int argc, char* argv[])
{
	unsigned short port = 10111;

	if (argc >= 2) {
		port = atoi(argv[1]);
	}
	if (port == 0) {
		fprintf(stderr, "usage: udpServer <port>\n");
		exit(1);
	}

	struct sockaddr_in server = create_any_addr_in(port);

	int sd = socket (PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (bind(sd, (struct sockaddr *) &server, sizeof(server)) < 0) {
		printf("error binding\n");
		exit(1);
	}

	for (;;) {
/* use read, recv or recvfrom
		rc = read(sd, buf, sizeof(buf));
		rc = recv(sd, buf, sizeof(buf), 0);
 */
		struct sockaddr_in cliaddr;
		socklen_t len = sizeof(struct sockaddr_in);
		char buf[512];
		int rc = recvfrom(sd, buf, sizeof(buf), 0, (struct sockaddr *)&cliaddr, &len);
		if (rc <= 0) {
			exit(1);
		}
		buf[rc] = '\0';
		printf("Received: %s\n", buf);
	}
}
