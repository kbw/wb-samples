#pragma once

#include "defs.h"

#include <netinet/in.h>

#include <stdint.h>

struct sockaddr_in create_addr_in(const char* host, uint16_t port);
struct sockaddr_in create_any_addr_in(uint16_t port);
