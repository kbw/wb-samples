#include "util.h"

#include <stdlib.h>

struct sockaddr_in create_addr_in(const char* host, uint16_t port) {
	struct hostent* hp = gethostbyname(host);
	if (!hp) {
		fprintf(stderr, "gethostbyname(%s) failed\n", host);
		exit(1);
	}

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	bcopy(hp->h_addr, &(addr.sin_addr.s_addr), hp->h_length);
	addr.sin_port = htons(port);

	return addr;
}

struct sockaddr_in create_any_addr_in(uint16_t port) {
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY); 
	addr.sin_port = htons(port);

	return addr;
}
