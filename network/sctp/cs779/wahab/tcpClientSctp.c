/*
NAME:        tcpClient0 
SYNOPSIS:    tcpClient0 <hostname>
DESCRIPTION:    The program creates a tcp socket in the inet 
                domain, connects to tcpServer0 running at <hostname> 
                and waiting at port 10111, sends the message "HI" and exits
*/

#include "defs.h"
#include "util.h"

int main(int argc, char* argv[])
{
	unsigned short port = 10111;
	const char* host = "localhost";

	if (argc >= 2) {
		port = atoi(argv[1]);
	}
	if (argc >= 3) {
		port = atoi(argv[2]);
	}
	if (port == 0) {
		fprintf(stderr, "usage: tcpClientSctp <host> <port>\n");
		exit(1);
	}

	int sd = socket(PF_INET, SOCK_STREAM, IPPROTO_SCTP);

	struct sockaddr_in server = create_addr_in(host, port);

	connect(sd, (struct sockaddr *)&server, sizeof(server));
	for (;;) {
		send(sd, "HI", 2, 0 );
		printf("sent HI\n");
		sleep(5);
	}
}
