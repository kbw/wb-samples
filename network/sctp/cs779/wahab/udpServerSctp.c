/*
NAME:        udpServerSctp 
SYNOPSIS:    udpServerSctp 
DESCRIPTION:    The program creates a datagram socket in the inet 
	             domain and using SEQPACKET of SCTP protocol, 
	             binds it to port <port> and receives any message
	             arrived to the socket and prints it out
*/

#include "defs.h"
#include "util.h"

void reusePort(int s);

int main(int argc, char* argv[])
{
	unsigned short port = 10111;

	if (argc >= 2) {
		port = atoi(argv[1]);
	}
	if (port == 0) {
		fprintf(stderr, "usage: udpServerSctp <port>\n");
		exit(1);
	}

	struct sockaddr_in server = create_any_addr_in(port);

	int sd = socket(PF_INET, SOCK_SEQPACKET, IPPROTO_SCTP);
	reusePort(sd);

	if (bind(sd, (struct sockaddr *)&server, sizeof(server)) < 0) {
		printf("error binding\n");
		exit(1);
	}

	listen(sd, 1);

	for (;;) {
		struct sockaddr_in cliaddr;
		struct sctp_sndrcvinfo sri;
		char buf[512];
		int msg_flags;

		socklen_t len = sizeof(struct sockaddr_in);
		int rc = sctp_recvmsg(sd, buf, sizeof(buf), (struct sockaddr *)&cliaddr, &len, &sri, &msg_flags);
		if (rc <= 0)
			exit(1);
		buf[rc] = '\0';
		printf("sctp_recvmsg: %s\n", buf);

		rc = recvfrom(sd, buf, sizeof(buf), 0, (struct sockaddr *) &cliaddr, &len);
		if (rc <= 0)
			exit(1);
		buf[rc] = '\0';
		printf("recvfrom: %s\n", buf);

		len = sizeof(cliaddr);
		getpeername(sd, (struct sockaddr *) &cliaddr, &len);
		printf("remote addr: %s,  port %d\n", inet_ntop(AF_INET, &cliaddr.sin_addr, buf, sizeof(buf)), ntohs(cliaddr.sin_port));
	}
}

void reusePort(int s)
{
	int one = 1;
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(one)) == -1) {
		printf("error in setsockopt,SO_REUSEPORT \n");
		exit(1);
	}
}

