/*
NAME:        udpClient0 
SYNOPSIS:    udpClient0 <hostname>
DESCRIPTION:    The program creates a datagram socket in the inet 
                domain, send the message "HI" every 2 seconds 
                to udpServer0 running at 
                <hostanme> at port 10111 and exits
*/

#include "defs.h"
#include "util.h"

int main(int argc, char* argv[])
{
	unsigned short port = 10111;
	const char* host = "localhost";

	if (argc >= 2) {
		port = atoi(argv[1]);
	}
	if (argc >= 3) {
		port = atoi(argv[2]);
	}
	if (port == 0) {
		fprintf(stderr, "usage: udpClient <host> <port>\n");
		exit(1);
	}

	int sd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

	struct sockaddr_in server = create_addr_in(host, port);

	for (;;) {
		sendto(sd, "HI",2, 0, (struct sockaddr *)&server, sizeof(server));
		printf("sent HI\n");
		sleep(2);
	}
}
