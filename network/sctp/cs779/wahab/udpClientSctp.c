/*
NAME:        udpClientSctp 
SYNOPSIS:    udpClientSctp <hostname>
DESCRIPTION:    The program creates a datagram socket in the inet 
                domain using SEQPACKET of SCTP, 
                send the message "HI" every 2 seconds 
                to udpServerSctp running at <hostanme> at port <port> and exits
*/

#include "defs.h"
#include "util.h"

int main(int argc, char* argv[])
{
	unsigned short port = 10111;
	const char* host = "localhost";

	if (argc >= 2) {
		port = atoi(argv[1]);
	}
	if (argc >= 3) {
		port = atoi(argv[2]);
	}
	if (port == 0) {
		fprintf(stderr, "usage: udpClientSctp <host> <port>\n");
		exit(1);
	}

	int sd = socket(PF_INET, SOCK_SEQPACKET, IPPROTO_SCTP);

	struct sockaddr_in server = create_addr_in(host, port);

	for (;;) {
		sctp_sendmsg(sd, "HI1", 3, (struct sockaddr *)&server, sizeof(server), 0, 0, 0, 0, 0);
		printf("sctp_sendmsg HI1\n");
		sendto(sd, "HI2",3, 0, (struct sockaddr *)&server, sizeof(server));
		printf("sendto HI2\n");
		sleep(5);
	}
}
