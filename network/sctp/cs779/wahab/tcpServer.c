/*
NAME:        tcpServer0 
SYNOPSIS:    tcpServer0 
DESCRIPTION:    The program creates a tcp socket in the inet 
                domain, binds it to port 10111, listen and accept 
                a connection from tcpClient0, and receives any message
                arrived to the socket and prints it out
*/

#include "defs.h"
#include "util.h"

int main(int argc, char* argv[])
{ 
	unsigned short port = 10111;

	if (argc >= 2) {
		port = atoi(argv[1]);
	}
	if (port == 0) {
		fprintf(stderr, "usage: tcpServer <port>\n");
		exit(1);
	}

	int sd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); 

	struct sockaddr_in name = create_any_addr_in(port);

	if (bind(sd, (struct sockaddr *)&name, sizeof(name)) < 0) { 
		fprintf(stderr, "error binding\n"); 
		exit(-1) ; 
	}

	listen(sd, 1); 
	int psd = accept(sd, 0, 0); 
	for (;;) { 
		char buf[1024];
		int cc = recv(psd, buf, sizeof(buf), 0); 
		if (cc <= 0) exit (0);
 
		buf[cc] = '\0'; 
		printf("message received: %s\n", buf); 
	} 
	close(sd); 
}
