https://www.cs.odu.edu/~cs779/spring13/lectures/sctp.html

Summary:
Matching apps:
tcpServer <port>
tcpClient <host> <port>

udpServer <port>
udpClient <host> <port>

tcpServerSctp <port>
tcpClientSctp <host> <port>
udpClientSctp <host> <port>

udppServerSctp <port>
udpClientSctp <host> <port>
tcpClientSctp <host> <port>

