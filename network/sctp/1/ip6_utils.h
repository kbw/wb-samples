#pragma once

#include <netinet/in.h>
//#include <arpa/inet.h>

#ifdef __cplusplus
extern "C" {
#endif

const char* inet6_ntoa(struct in6_addr addr);

#ifdef __cplusplus
}
#endif
