#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <string.h>

const char* inet6_ntoa(struct in6_addr addr)
{
	static char s_addr6[16*4 + 1];
	memset(s_addr6, 0, sizeof(s_addr6));

	int offset = 0;
	for (int i = 0; i < 16; ++i) {
		if (i && (i % 2) == 0 && s_addr6[offset - 2] != ':')
			offset += snprintf(s_addr6 + offset, sizeof(s_addr6) - offset, ":");

		if ((i % 2) == 0 && !addr.s6_addr[i] && !addr.s6_addr[i + 1]) {
			i += 1;
			continue;
		}

		if ((i % 2) == 0) {
			if (addr.s6_addr[i])
				offset += snprintf(s_addr6 + offset, sizeof(s_addr6) - offset, "%x", addr.s6_addr[i]);
		}
		else {
			if (addr.s6_addr[i - 1])
				offset += snprintf(s_addr6 + offset, sizeof(s_addr6) - offset, "%02x", addr.s6_addr[i]);
			else
				offset += snprintf(s_addr6 + offset, sizeof(s_addr6) - offset, "%x", addr.s6_addr[i]);
		}
	}

	return s_addr6;
}
