#include "ip6_utils.h"
#include "utils.hpp"

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

#include <stdio.h>
#include <string.h>
#include <sys/errno.h>

#include <vector>
#include <utility>

typedef struct in_addr ip4_address_t;
typedef struct in6_addr ip6_address_t;
typedef std::vector<ip4_address_t> ip4_addresses_t;
typedef std::vector<ip6_address_t> ip6_addresses_t;
typedef std::pair<ip4_addresses_t, ip6_addresses_t> addresses_t;

addresses_t load_addresses() {
	ip4_addresses_t ip4s;
	ip6_addresses_t ip6s;

	struct ifaddrs* ifa = NULL;
	if (getifaddrs(&ifa) == -1)
		throw std::runtime_error("getifaddrs failed");

	for (struct ifaddrs* p = ifa; p; p = p->ifa_next) {
		if (p->ifa_addr) {
			switch (p->ifa_addr->sa_family) {
			case AF_INET: {
				struct in_addr addr = ((struct sockaddr_in*)p->ifa_addr)->sin_addr;
				ip4s.push_back(addr);
//				printf("%s inet %s\n", p->ifa_name, inet_ntoa(addr));
				}
				break;
			case AF_INET6: {
				struct in6_addr addr = ((struct sockaddr_in6*)p->ifa_addr)->sin6_addr;
				ip6s.push_back(addr);
//				printf("%s inet6 %s\n", p->ifa_name, inet6_ntoa(addr));
				}
				break;
			}
		}
	}

	freeifaddrs(ifa), ifa = NULL;
	return addresses_t(ip4s, ip6s);
}

std::vector<struct sockaddr_in> create_addr4(const ip4_addresses_t& addrs) {
	std::vector<struct sockaddr_in> addr4(addrs.size());

	for (size_t i = 0; i != addr4.size(); ++i) {
		memset(&addr4[i], 0, sizeof(addr4[i]));
		addr4[i].sin_len = sizeof(addr4[i]);
		addr4[i].sin_family = AF_INET;
		addr4[i].sin_port = htons(3456);
		addr4[i].sin_addr = addrs[i];
	}

	return addr4;
}

int main(int argc, char* argv[])
try {
	addresses_t addrs = load_addresses();
//	for (auto& addr : addrs.first)
//		printf("%s\n", inet_ntoa(addr));
//	for (auto& addr : addrs.second)
//		printf("%s\n", inet6_ntoa(addr));

	wrapper<int, decltype(&close)> s4(socket(PF_INET, SOCK_SEQPACKET, IPPROTO_SCTP), close);
	if (s4 == -1)
		throw std::runtime_error("socket(PF_INET, SOCK_SEQPACKET, IPPROTO_SCTP)");

	std::vector<struct sockaddr_in> addr4 = create_addr4(addrs.first);
	int rc = sctp_bindx(s4.get(), (struct sockaddr*)&addr4.front(), addr4.size(), SCTP_BINDX_ADD_ADDR);
	if (rc == -1)
		throw std::runtime_error("sctp_bindx");

	rc = listen(s4.get(), 64);
	if (rc == -1)
		throw std::runtime_error("sctp_listen");

	do {
		char buf[32];
		struct sockaddr_in client_addr;
		socklen_t client_addrlen = sizeof(client_addr);
		struct sctp_sndrcvinfo info;
		int flags = 0;
		ssize_t len = sctp_recvmsg(s4.get(), buf, sizeof(buf), (struct sockaddr*)&client_addr, &client_addrlen, &info, &flags);
		printf("sctp_recvmsg: return=%zd flags=0x%04x stream=%d ssn=%d flags=%04x ppid=%d ctx=%d\n",
			len, flags, info.sinfo_stream, info.sinfo_ssn, info.sinfo_flags, info.sinfo_ppid, info.sinfo_context);
	}
	while (true);
}
catch (const std::exception& e) {
	fprintf(stderr, "fatal: errno=%d %s\n", errno, e.what());
}
