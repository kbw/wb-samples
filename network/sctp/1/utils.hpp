#pragma once

template <class T, class Deleter>
class wrapper {
	T handle;
	Deleter deleter;
	bool owner;

public:
	wrapper(T obj, Deleter d) : handle(obj), deleter(d), owner(true) {}
	~wrapper() { if (owner) deleter(handle); }

	T release() { owner = false; return handle; }

	void reset(T obj) { if (owner) deleter(handle); handle = obj; owner = true; }
	void reset() { if (owner) deleter(handle); owner = false; }

	T get() { return handle; }
	Deleter* get_deleter() { return deleter; }

	operator bool() const { return owner; }
	operator T() { return handle; }
	operator T() const { return handle; }
};
