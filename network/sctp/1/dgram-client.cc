#include "ip6_utils.h"
#include "utils.hpp"

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/errno.h>

#include <vector>
#include <utility>
#include <algorithm>

typedef struct in_addr ip4_address_t;
typedef struct in6_addr ip6_address_t;
typedef std::vector<ip4_address_t> ip4_addresses_t;
typedef std::vector<ip6_address_t> ip6_addresses_t;
typedef std::pair<ip4_addresses_t, ip6_addresses_t> addresses_t;

addresses_t load_addresses() {
	ip4_addresses_t ip4s;
	ip6_addresses_t ip6s;

	struct ifaddrs* ifa = NULL;
	if (getifaddrs(&ifa) == -1)
		throw std::runtime_error("getifaddrs failed");

	for (struct ifaddrs* p = ifa; p; p = p->ifa_next) {
		if (p->ifa_addr) {
			switch (p->ifa_addr->sa_family) {
			case AF_INET: {
				struct in_addr addr = ((struct sockaddr_in*)p->ifa_addr)->sin_addr;
				ip4s.push_back(addr);
//				printf("%s inet %s\n", p->ifa_name, inet_ntoa(addr));
				}
				break;
			case AF_INET6: {
				struct in6_addr addr = ((struct sockaddr_in6*)p->ifa_addr)->sin6_addr;
				ip6s.push_back(addr);
//				printf("%s inet6 %s\n", p->ifa_name, inet6_ntoa(addr));
				}
				break;
			}
		}
	}

	freeifaddrs(ifa), ifa = NULL;
	return addresses_t(ip4s, ip6s);
}

std::vector<struct sockaddr_in> create_addr4(const ip4_addresses_t& addrs) {
	std::vector<struct sockaddr_in> addr4(addrs.size());

	for (size_t i = 0; i != addr4.size(); ++i) {
		memset(&addr4[i], 0, sizeof(addr4[i]));
		addr4[i].sin_len = sizeof(addr4[i]);
		addr4[i].sin_family = AF_INET;
		addr4[i].sin_port = htons(3456);
		addr4[i].sin_addr = addrs[i];
	}

	return addr4;
}

int main(int argc, char* argv[])
try {
	enum { enone, eerror, eppid, estreamno, ettl, ectx } state = enone;
	uint32_t ppid = 0;
	uint16_t streamno = 0;
	uint32_t ttl = 16;
	uint32_t ctx = 0;
	std::for_each(argv + 1, argv + argc, [&](const char* arg) {
		switch (state) {
		case enone:
			state =	(!strcmp(arg, "--ppid"))   ? eppid :
					(!strcmp(arg, "--stream")) ? estreamno :
					(!strcmp(arg, "--ttl"))    ? ettl :
					(!strcmp(arg, "--ctx"))    ? ectx : eerror;
			break;
		case eppid:
			ppid = atoll(arg);
			break;
		case estreamno:
			streamno = atoll(arg);
			break;
		case ettl:
			ttl = atoll(arg);
			break;
		case ectx:
			ctx = atoll(arg);
			break;
		default:
			;
		}

		if (state == eerror) {
			fprintf(stderr, "errno: bad arg: %s\n", arg);
			exit(1);
		}
	});

	addresses_t addrs = load_addresses();
//	for (auto& addr : addrs.first)
//		printf("%s\n", inet_ntoa(addr));
//	for (auto& addr : addrs.second)
//		printf("%s\n", inet6_ntoa(addr));

	wrapper<int, decltype(&close)> s4(socket(PF_INET, SOCK_SEQPACKET, IPPROTO_SCTP), close);
	if (s4 == -1)
		throw std::runtime_error("socket(PF_INET, SOCK_SEQPACKET, IPPROTO_SCTP)");

	std::vector<struct sockaddr_in> addr4 = create_addr4(addrs.first);
//	int rc = sctp_bindx(s4.get(), (struct sockaddr*)&addr4.front(), addr4.size(), SCTP_BINDX_ADD_ADDR);
//	if (rc == -1)
//		throw std::runtime_error("sctp_bindx");
//
//	rc = listen(s4.get(), 64);
//	if (rc == -1)
//		throw std::runtime_error("sctp_listen");
//
	char buf[32];
	snprintf(buf, sizeof(buf), "hello world");
//	struct sockaddr_in client_addr;
//	socklen_t client_addrlen = sizeof(client_addr);
//	struct sctp_sndrcvinfo info;
	uint32_t flags = SCTP_UNORDERED;
	ssize_t len = sctp_sendmsg(s4.get(), buf, strlen(buf) + 1, (struct sockaddr*)&addr4[0], sizeof(addr4[0]), ppid, flags, streamno, ttl, ctx);
	printf("sctp_sendmsg: return=%zd\n", len);
}
catch (const std::exception& e) {
	fprintf(stderr, "fatal: errno=%d %s\n", errno, e.what());
}
