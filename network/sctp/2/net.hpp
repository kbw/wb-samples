#pragma once

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>

#include <stdexcept>

#include <errno.h>
#include <string.h>

#include <algorithm>
#include <iterator>
#include <ostream>
#include <vector>

#ifndef likely
	#define likely(x)	__builtin_expect((x),1)
	#define unlikely(x)	__builtin_expect((x),0)
#endif

namespace kw::net {
	class error : public std::runtime_error {
	public:
		error(int syserr) : std::runtime_error(strerror(syserr)) {}
		error(const std::string& msg) : std::runtime_error(msg) {}
	};

	class socket {
		int s_{-1};

	public:
		socket() = default;
		~socket() {
			if (s_ != -1)
				close(true);
		}

		socket(const socket& n) = delete;
		socket& operator=(const socket& n) = delete;

		socket(socket&& n) : s_(n.s_) { n.s_ = -1; }
		socket& operator=(socket&& n) {
			if (this != &n) {
				close(s_);
				s_ = n.s_;
				n.s_ = -1;
			}
			return *this;
		}

	protected:
		int handle() { return s_; }
		int handle(int s) {
			if (s_ != -1)
				throw error("fatal: reassign socket without close");

			s_ = s;
			return s;
		}

		void close(bool quiet = false) {
			int rc = ::close(s_);
			s_ = -1;

			if (!quiet && rc == -1)
				throw error(errno);
		}
	};
} // kw::net

namespace kw::net1 {
	struct address {};
	struct buffer {};
	using address_t = address;
	using addresses_t = std::vector<address>;
	using buffer_t = buffer;

	template <typename T>
	struct socket {
		int bind(address_t&) { return -1; }
	};

	template <typename T>
	struct reachable {
		int bind(address_t&) { return -1; }
	};

	template <typename T>
	struct single_peer {
		ssize_t send_msg(const buffer_t&) { return -1; }
		ssize_t recv_msg(buffer_t&) { return -1; }
	};

	template <typename T>
	struct multi_peer {
		ssize_t send_msg(const buffer_t&) { return -1; }
		ssize_t recv_msg(buffer_t&) { return -1; }
	};

	template <typename T>
	struct single_client {
		int connect(const address_t&) { return -1; }
		ssize_t send(const buffer_t&) { return -1; }
		ssize_t recv(buffer_t&) { return -1; }
	};

	template <typename T>
	struct multi_client {
		int connect(const addresses_t&) { return -1; }
		ssize_t send(const buffer_t&, const addresses_t&) { return -1; }
		ssize_t recv(buffer_t&, addresses_t&) { return -1; }
	};

	template <typename T>
	struct single_server {
		ssize_t accept(address_t&);
		ssize_t send(const buffer_t&) { return -1; }
		ssize_t recv(buffer_t&) { return -1; }
	};

	struct udp : reachable<udp> {
	};
} // kw::net1

namespace kw::net2 {
	struct errno_error : public std::runtime_error {
		errno_error(const char* op) : std::runtime_error(make_msg(op)) {}

	private:
		static std::string make_msg(const char* op) {
			char buffer[96];
#if (defined(__USE_XOPEN2K) && !defined(__USE_GNU)) || \
	(defined(__ANDROID_API__) && defined(__USE_GNU) && __ANDROID_API__ < 23) || \
	(defined(__POSIX_VISIBLE) && __POSIX_VISIBLE >= 200112)
			int len = snprintf(buffer, sizeof(buffer), "%s:%d ", op, errno);
			strerror_r(errno, buffer + len, sizeof(buffer) - len);
			return buffer;
#else
			char* str = strerror_r(errno, buffer, sizeof(buffer));
			int len = snprintf(buffer, sizeof(buffer), "%s: %d %s", op, errno, str);
			if (unlikely(len == -1))
				return std::to_string(errno);
			return { buffer, static_cast<size_t>(len) };
#endif
		}
	};

	struct ip4_address : public sockaddr_in {
		static constexpr int protocol = PF_INET;
		static constexpr int family = AF_INET;

		ip4_address() {
			sin_family = family;
			sin_addr.s_addr = INADDR_ANY;
			sin_port = 0;
		}
		ip4_address(uint16_t port) {
			sin_family = family;
			sin_addr.s_addr = INADDR_ANY;
			sin_port = htons(port);
		}
		ip4_address(const std::string& ipaddr, uint16_t port) {
			sin_family = family;
			sin_addr.s_addr = ::inet_addr(ipaddr.c_str());
			sin_port = htons(port);
		}

		uint16_t port() const {
			return ntohs(sin_port);
		}

		void port(uint16_t value) {
			sin_port = htons(value);
		}
	};

	struct ip6_address : public sockaddr_in6 {
		static constexpr int protocol = PF_INET6;
		static constexpr int family = AF_INET6;

		ip6_address() {
			sin6_family = family;
			sin6_addr = in6addr_any;
			sin6_port = 0;
		}
		ip6_address(uint16_t port) {
			sin6_family = family;
			sin6_addr = in6addr_any;
			sin6_port = htons(port);
		}
		ip6_address(const std::string& ipaddr, uint16_t port) {
			sin6_family = family;
			inet_pton(family, ipaddr.c_str(), &sin6_addr);
			sin6_port = htons(port);
		}
	};

	using address_t = ip4_address;
	using addresses_t = std::vector<address_t>;

	using sock_t = int;
	using socklen_t = ::socklen_t;
	constexpr sock_t bad_socket = -1;

	struct buffer_t : std::vector<char> {
		buffer_t(const char* str) {
			auto len = strlen(str);
			std::copy(str, str + len, std::back_inserter(*this));
		}
		buffer_t(size_t sz) {
			resize(sz);
		}
		buffer_t() = default;
	};

	inline buffer_t operator+(const char* str, const buffer_t& buf) {
		buffer_t ret;
		auto len = strlen(str);
		ret.reserve(len + buf.size());

		std::copy(str, str + len, std::back_inserter(ret));
		std::copy(buf.begin(), buf.end(), std::back_inserter(ret));
		return ret;
	}

	inline std::ostream& operator<<(std::ostream& os, const buffer_t& buf) {
		std::copy(buf.begin(), buf.end(), std::ostream_iterator<buffer_t::value_type>(os, ""));
		return os;
	}

	struct socket {
		~socket() { close(); }

		sock_t sock() const {
			return s_;
		}

		void close() {
			if (s_ != bad_socket) {
				::close(s_);
				s_ = bad_socket;
			}
		}

	protected:
		socket() : s_(bad_socket) {}
		socket(sock_t s) : s_(s) {}

	private:
		int s_{bad_socket};
	};

	struct udp : socket {
		udp() : socket(::socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) {}

		void sendto(const buffer_t& buf, const address_t& addr) {
			ssize_t nbytes = ::sendto(sock(), buf.data(), buf.size(), 0, (const sockaddr*)&addr, sizeof(addr));
			if (unlikely(nbytes == -1))
				throw errno_error("sendto failed");
		}

		void recvfrom(buffer_t& buf, address_t& addr) {
			socklen_t len;
			ssize_t nbytes = ::recvfrom(sock(), buf.data(), buf.size(), 0, (sockaddr*)&addr, &len);
			if (unlikely(nbytes == -1))
				throw errno_error("recvfrom failed");
			if (unlikely(len != sizeof(addr)))
				throw std::runtime_error("recvfrom wrong address type");

			buf.resize(nbytes);
		}
	};

	struct udp_server : udp {
		udp_server() = default;

		void bind(const address_t& addr) {
			int rc = ::bind(sock(), (const sockaddr*)&addr, sizeof(addr));
			if (unlikely(rc == -1))
				throw errno_error("bind failed");
		}
	};

	struct tcp_base : socket {
		tcp_base(sock_t s) : socket(s) {}
		tcp_base() : socket(::socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) {}

		void send(const buffer_t& buf) {
			ssize_t nbytes = ::send(sock(), buf.data(), buf.size(), 0);
			if (unlikely(nbytes == -1))
				throw errno_error("send failed");
		}

		void recv(buffer_t& buf) {
			ssize_t nbytes = ::recv(sock(), buf.data(), buf.size(), 0);
			if (unlikely(nbytes == -1))
				throw errno_error("recv failed");

			buf.resize(nbytes);
		}
	};

	struct tcp_server : tcp_base {
		tcp_server() = default;

		void bind(const address_t& addr) {
			int rc = ::bind(sock(), (const sockaddr*)&addr, sizeof(addr));
			if (unlikely(rc == -1))
				throw errno_error("bind failed");
		}

		void listen(int buffer_size = 4) {
			int rc = ::listen(sock(), buffer_size);
			if (unlikely(rc == -1))
				throw errno_error("listen failed");
		}

		tcp_base accept(address_t& addr) {
			socklen_t len;
			tcp_base client(::accept(sock(), (sockaddr*)&addr, &len));
			if (unlikely(len != sizeof(addr)))
				throw std::runtime_error("recvfrom wrong address type");
			return client;
		}
	};

	struct tcp_client : tcp_base {
		tcp_client() = default;
		~tcp_client() {
			shutdown(SHUT_WR);
		}

		void connect(const address_t& addr) {
			int rc = ::connect(sock(), (const sockaddr*)&addr, sizeof(addr));
			if (unlikely(rc == -1))
				throw errno_error("connect failed");
		}

		void shutdown(int how) {
			if (unlikely(sock() != bad_socket)) {
				int rc = ::shutdown(sock(), how);
				if (unlikely(rc == -1))
					throw errno_error("shutdown failed");
			}
		}
	};

	struct sctp_stream : socket {
		sctp_stream() : socket(::socket(PF_INET, SOCK_STREAM, IPPROTO_SCTP)) {}

		void bind(addresses_t) {}
		void connect(addresses_t) {}
		void accept(address_t&) {}

		void send(buffer_t) {}
		void recv(buffer_t&) {}
	};

	struct sctp_msg : socket {
		sctp_msg() : socket(::socket(PF_INET, SOCK_DGRAM, IPPROTO_SCTP)) {}

		void bind(addresses_t) {}

		void sendto(buffer_t, address_t) {}
		void recvfrom(buffer_t&, address_t&) {}
	};

	struct sctp : socket {
		sctp() : socket(::socket(PF_INET, SOCK_STREAM, IPPROTO_SCTP)) {}

		void bind(addresses_t) {}
		void connect(addresses_t) {}
		void accept(address_t&) {}

		void sendto(buffer_t, address_t) {}
		void recvfrom(buffer_t&, address_t&) {}

		void send(buffer_t) {}
		void recv(buffer_t&) {}
	};

	inline udp create_udp() {
		udp s;
		return s;
	}

	inline udp_server create_udp_server(const address_t& addr) {
		udp_server s;
		s.bind(addr);
		return s;
	}

	inline tcp_client create_tcp(const address_t& addr) {
		tcp_client s;
		s.connect(addr);
		return s;
	}

	inline tcp_server create_tcp_server(const address_t& addr) {
		tcp_server s;
		s.bind(address_t{addr.port()});
		s.listen();
		return s;
	}
} // kw::net2
