#include "net.hpp"

#include <algorithm>
#include <exception>
#include <iostream>
#include <stdexcept>

#include <assert.h>
#include <string.h>

constexpr uint16_t port_a{2311};
constexpr size_t small_buffer{10};
constexpr size_t big_buffer{10};

void udp();
void tcp();

int main(int argc, char* argv[])
try {
	auto handler = [](char* arg) {
		if (strcmp("udp", arg) == 0) {
			udp();
		}
		else if (strcmp("tcp", arg) == 0) {
			tcp();
		}
		else if (strcmp("all", arg) == 0) {
			udp();
			tcp();
		}
	};
	std::for_each(argv + 1, argv + argc, handler);
}
catch (const std::exception& e) {
	std::cerr << "fatal: " << e.what() << std::endl;
	return 1;
}

void udp() {
	kw::net2::ip4_address addr{"127.0.0.1", port_a};
	auto udp_srv = kw::net2::create_udp_server(addr);
	auto udp_cli = kw::net2::create_udp();

	kw::net2::buffer_t buf{"hello"};
	udp_cli.sendto(buf, addr);

	kw::net2::buffer_t buf2{small_buffer};
	udp_srv.recvfrom(buf2, addr);
	std::cout << buf2 << std::endl;
}

void tcp() {
	kw::net2::ip4_address addr{"127.0.0.1", port_a};
	auto tcp_srv = kw::net2::create_tcp_server(addr);
	auto tcp_cli = kw::net2::create_tcp(addr);

	kw::net2::ip4_address addr2;
	auto connection{tcp_srv.accept(addr2)};

	tcp_cli.send({"hello tcp"});

	kw::net2::buffer_t buf{small_buffer};
	connection.recv(buf);
	buf = "reply: " + buf;
	connection.send(buf);

	kw::net2::buffer_t buf2{big_buffer};
	tcp_cli.recv(buf2);
	tcp_cli.~tcp_client(); // shutdown client before server: tcp_srv and connection

	std::cout << buf2 << std::endl;
	if (buf != buf2) {
		throw std::runtime_error("tcp server returned unexpected value");
	}
}
