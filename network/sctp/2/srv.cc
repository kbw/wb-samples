#include "common.h"

#include <unistd.h>
#include <strings.h>

#include <sys/types.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <netinet/sctp.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <exception>

int main(int argc, char* argv[])
try {
	int rc;
	if (argc == 1) {
		fprintf(stderr, "usage: srv [ip address] ...\n");
		exit(1);
	}

	// SOCK_STREAM: connection oriented protocol
	int s = socket(PF_INET, SOCK_STREAM, IPPROTO_SCTP);

	// bind to all addresses
	std::vector<struct sockaddr_in> addrs{ create_addr_ins((const char**)&argv[1], argc - 1) };

	rc = sctp_bindx(s, reinterpret_cast<struct sockaddr*>(&addrs.front()), static_cast<int>(addrs.size()), SCTP_BINDX_ADD_ADDR);
	if (rc != 0) {
		fprintf(stderr, "bind: rc=%d errno=%s\n", rc, static_cast<const char*>(strerror(errno)));
		close(s);
		exit(1);
	}

	rc = listen(s, 5);
	if (rc != 0) {
		fprintf(stderr, "listen: rc=%d errno=%s\n", rc, static_cast<const char*>(strerror(errno)));
		close(s);
		exit(1);
	}

	while (1) {
		struct sockaddr_in client_addr;
		socklen_t client_addr_len = sizeof(client_addr);
		int client_s = accept(s, reinterpret_cast<struct sockaddr*>(&client_addr), &client_addr_len);

		char buf[128];
		printf("remote: %s:%d\n", inet_ntop(AF_INET, &client_addr.sin_addr, buf, sizeof(buf)), ntohs(client_addr.sin_port));
		close(client_s);
	}

	close(s);
}
catch (const std::exception &e) {
	fprintf(stderr, "fatal: %s\n", e.what());
}
