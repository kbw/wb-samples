#include "common.h"

#include <unistd.h>
#include <strings.h>

#include <sys/types.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <netinet/sctp.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <exception>

int main(int argc, char* argv[])
try {
	int rc;
	if (argc == 1) {
		fprintf(stderr, "usage: cli [ip address] ....\n");
		exit(1);
	}

	// SOCK_STREAM: connection oriented protocol
	int s = socket(PF_INET, SOCK_STREAM, IPPROTO_SCTP);

	// connect to all addresses
	std::vector<struct sockaddr_in> addrs{ create_addr_ins((const char**)&argv[1], argc - 1) };

	sctp_assoc_t id;
	rc = sctp_connectx(s, reinterpret_cast<struct sockaddr*>(&addrs.front()), static_cast<int>(addrs.size()), &id);
	if (rc != 0) {
		fprintf(stderr, "connect: rc=%d errno=%s\n", rc, static_cast<const char*>(strerror(errno)));
		close(s);
		exit(1);
	}
	fprintf(stdout, "association id: %d\n", id);

	shutdown(s, SHUT_WR);
	close(s);
	fprintf(stdout, "done\n");
}
catch (const std::exception& e) {
	fprintf(stderr, "fatai: %s\n", e.what());
}
