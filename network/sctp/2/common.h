#pragma once

#include "net.hpp"

#include <unistd.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <netdb.h>

#include <vector>

struct sockaddr_in create_addr_in(const char* addr_port);
std::vector<struct sockaddr_in> create_addr_ins(const char** addr_ports, size_t nitems);
