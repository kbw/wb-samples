#include "common.h"

#include <unistd.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <netdb.h>

#include <string.h>

#include <stdexcept>

struct sockaddr_in create_addr_in(const char* addr_port) {
	const char* p = strchr(addr_port, ':');
	if (!p)
		throw std::invalid_argument(addr_port);

	char addr[128];
	bzero(addr, sizeof(addr));
	strncpy(addr, addr_port, p - addr_port);
	uint16_t port = atoi(p + 1);

	struct hostent* hent = gethostbyname(addr);
	if (!hent)
		throw std::invalid_argument(strerror(errno));

	struct sockaddr_in v4addr;
	bzero(&v4addr, sizeof(v4addr));
	v4addr.sin_family = AF_INET;
	bcopy(hent->h_addr, &(v4addr.sin_addr.s_addr), hent->h_length);
	v4addr.sin_port = htons(port);

	return v4addr;
}

std::vector<struct sockaddr_in> create_addr_ins(const char** addr_ports, size_t nitems) {
	std::vector<struct sockaddr_in> v4addrs;

	for (size_t i = 0; i != nitems; ++i)
		v4addrs.emplace_back(create_addr_in(addr_ports[i]));

	return v4addrs;
}
