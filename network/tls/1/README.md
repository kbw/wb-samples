https://aticleworld.com/ssl-server-client-using-openssl-in-c/

Example of secure server-client program using OpenSSL in C
==========================================================

In this example code, we will create a secure connection between client and server using the TLS1.2 protocol. In this communication, the client sends an XML request to the server which contains the username and password.

The server verifies the XML request, if it is valid then it sends a proper XML response to the client either give a message of Invalid Request.

Install the OpenSSL library, for the ubuntu use the below command.
------------------------------------------------------------------
sudo apt-get install libssl–dev

Before compiling the client and server program you will need a Certificate. You can generate your own certificate using the below command.

openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout mycert.pem -out mycert.pem

Note: Here certificate name is mycert.pem.

Example Client code for TLS1.2 communication
--------------------------------------------
Compile the Client : gcc -Wall -o client  Client.c -L/usr/lib -lssl -lcrypto
Run :   ./client <host_name> <port_number>
