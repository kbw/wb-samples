#include <libm8/app.hpp>
#include <libm8/msg_queue.hpp>
#include <libm8/utils.hpp>
#include <libm8/log.hpp>

//---------------------------------------------------------------------------
// server_t
class server_t : public m8::app_t {
public:
	typedef app_t inherited;

	server_t(const m8::basic_string& name, const m8::basic_string& config_filename);

	static void echo_rep_callback(void* ctx, const m8::buffer_t* buf);
	static void echo_req_callback(void* ctx, const m8::buffer_t& buf);

private:
	m8::msg_queue_t* echo_rep_queue;
	m8::msg_queue_t* echo_req_queue;
};
