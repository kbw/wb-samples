#include <zmq.hpp>
#include <string>
#include <memory>
#include <iostream>
#include <stdlib.h>

int main(int argc, char* argv[])
try {
	std::string cnt  = (argc > 1) ? argv[1] : "-1";
	std::string addr = (argc > 2) ? argv[2] : "tcp://*:2000";

	zmq::context_t ctx(1);
	zmq::socket_t s(ctx, zmq::socket_type::rep);
	s.bind(addr);

	for (unsigned i = 0, mx = atoi(cnt.c_str()); i < mx; ++i) {
		const int bufsz = 2048;
		std::unique_ptr<char[]> buf( new char[bufsz] );

		int rc = s.recv(buf.get(), bufsz);
		s.send(buf.get(), rc);
		std::cout << std::string(buf.get(), rc) << std::endl;
	}
}
catch (const std::exception& e) {
	std::cerr << "fatal: " << e.what() << std::endl;
}
