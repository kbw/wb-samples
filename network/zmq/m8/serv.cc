#include <rapidjson/document.h>

#include <zmq.hpp>

#include <sys/types.h>
#include <sys/stat.h>

#include <string>
#include <stdexcept>
#include <utility>
#include <list>
#include <map>
#include <vector>
#include <queue>
#include <fstream>
#include <iostream>

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

//---------------------------------------------------------------------------
// utility prototypes
std::string format(const char* fmt, ...);

//---------------------------------------------------------------------------
// macros
#ifndef NDEBUG
	class consistency_error : public std::runtime_error {
	public:
		consistency_error(
				const char* file, int line,
				const std::string& text) :
			std::runtime_error(format("%s:%d ", file, line) + text)
		{}
	};

	class precondition_error : public consistency_error {
	public:
		precondition_error(
				const char* file, int line,
				const std::string& text) :
			consistency_error(file, line, text)
		{}
	};
	#define	precondition(expr, text) if (!(expr)) throw precondition_error(__FILE__, __LINE__, (text));

	class postcondition_error : public consistency_error {
	public:
		postcondition_error(
				const char* file, int line,
				const std::string& text) :
			consistency_error(file, line, text)
		{}
	};
	#define	postcondition(expr, text) if (!(expr)) throw postcondition_error(__FILE__, __LINE__, (text));

	class invariant_error : public consistency_error {
	public:
		invariant_error(
				const char* file, int line,
				const std::string& text) :
			consistency_error(file, line, text)
		{}
	};
	#define	invariant(expr, text) if (!(expr)) throw postcondition_error(__FILE__, __LINE__, (text));
#else
	#define	precondition(expr, obj)
	#define	postcondition(expr, obj)
	#define	invariant(expr, text)
#endif

//---------------------------------------------------------------------------
// types
struct stream_config_entry_t {
	stream_config_entry_t() : bind(false), type(zmq::socket_type::req) {}

	std::string addr;
	bool bind;
	zmq::socket_type type;
};
typedef std::map<std::string, stream_config_entry_t> stream_config_t;

typedef std::string buffer_t;

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef void recv_f(void* ctx, buffer_t* buf);
typedef void send_f(void* ctx, const buffer_t& buf);

class msg_queue_t {
public:
	msg_queue_t(const stream_config_entry_t& config);
	virtual	~msg_queue_t();

	virtual void register_on_recv(void* ctx, recv_f* func);
	virtual void register_on_send(void* ctx, send_f* func);

	virtual void recv(buffer_t* buf);
	virtual void send(const buffer_t& buf);

	static void multiplex(long timeout = -1);

private:
	struct recv_info_t : std::pair<void*, recv_f*> {
		recv_info_t(void* ctx = nullptr, recv_f* func = nullptr) : std::pair<void*, recv_f*>(ctx, func) {}

		void* ctx() const { return first; }
		recv_f* func() const { return second; }

		void ctx(void* n) { first = n; }
		void func(recv_f* n) { second = n; }
	};
	typedef std::list<recv_info_t> recv_infos_t;

	struct send_info_t : std::pair<void*, send_f*> {
		send_info_t(void* ctx = nullptr, send_f* func = nullptr) : std::pair<void*, send_f*>(ctx, func) {}

		void* ctx() const { return first; }
		send_f* func() const { return second; }

		void ctx(void* n) { first = n; }
		void func(send_f* n) { second = n; }
	};
	typedef std::list<send_info_t> send_infos_t;

	typedef std::queue<buffer_t, std::list<buffer_t>> send_queue_t;

	void do_recv(buffer_t* buf);
	void do_send(const buffer_t& buf);

	const stream_config_entry_t m_config;
	zmq::socket_t m_socket;

	recv_infos_t m_recv;
	send_infos_t m_send;
	send_queue_t m_send_queue;

private:
	typedef std::vector<msg_queue_t*> pqueues_t;

	static pqueues_t sm_queues;
};
typedef std::list<msg_queue_t*> msg_queues_t;

//---------------------------------------------------------------------------
// prototypes
stream_config_t	config(const std::string& filename);
msg_queues_t	create(const stream_config_t& config);

//---------------------------------------------------------------------------
// globals
const std::string g_config_name = getenv("SERV_CONFIG") ? getenv("SERV_CONFIG") : "serv.json";
zmq::context_t g_ctx(1);

msg_queue_t::pqueues_t msg_queue_t::sm_queues;

//---------------------------------------------------------------------------
//
int main(int argc, char* argv[])
try {
	std::string cnt      = (argc > 1) ? argv[1] : "-1";
	std::string repaddr  = (argc > 2) ? argv[2] : "tcp://*:2000";
	std::string pulladdr = (argc > 2) ? argv[2] : "tcp://*:2001";

	stream_config_t config_entries = config(g_config_name);

	msg_queues_t queues = create(config_entries);
	msg_queue_t::multiplex();
}
catch (const std::exception& e) {
	std::cerr << "fatal: " << e.what() << std::endl;
}

//---------------------------------------------------------------------------
// json config
stream_config_t		read_config(const rapidjson::Document& doc);
rapidjson::Document	read_config(const std::string& filename);
zmq::socket_type	to_zmq_socket_type(const char* text);

stream_config_t config(const std::string& filename) {
	return read_config(read_config(filename));
}

stream_config_t read_config(const rapidjson::Document& doc) {
	stream_config_t entries;

	for (rapidjson::Value::ConstMemberIterator p = doc.MemberBegin(); p != doc.MemberEnd(); ++p) {
		if (p->value.IsObject()) {
			stream_config_entry_t entry;

			for (rapidjson::Value::ConstMemberIterator q = p->value.MemberBegin(); q != p->value.MemberEnd(); ++q) {
				if (q->value.IsString() && strcmp(q->name.GetString(), "addr") == 0) {
					entry.addr = q->value.GetString();
				}
				else if (q->value.IsBool()   && strcmp(q->name.GetString(), "bind") == 0) {
					entry.bind = q->value.GetBool();
				}
				else if (q->value.IsString() && strcmp(q->name.GetString(), "bind") == 0) {
					entry.bind = !strcmp(q->value.GetString(), "true") ? true : false;
				}
				else if (q->value.IsString() && strcmp(q->name.GetString(), "zmq_socket_type") == 0) {
					entry.type = to_zmq_socket_type(q->value.GetString());
				}
				else
					throw std::runtime_error("unrecognised config ernty \"" + std::string(q->name.GetString()) + "\"");
			}

			entries.emplace(p->name.GetString(), entry);
		}
	}

	return entries;
}

rapidjson::Document read_config(const std::string& filename) {
	struct stat info;
	int rc = stat(filename.c_str(), &info);
	if (rc == -1)
		throw std::runtime_error("\"" + g_config_name + "\" does not exist");

	std::unique_ptr<char[]> txt( new char[info.st_size + 1] );
	txt[info.st_size] = 0;
	std::ifstream is(filename, std::ios::binary);
	is.read(txt.get(), info.st_size);

	rapidjson::Document doc;
	doc.Parse(txt.get());
	if (!doc.IsObject())
		throw std::runtime_error("\"" + g_config_name + "\" not a valid JSON document");

	return doc;
}

zmq::socket_type to_zmq_socket_type(const char* text) {
	if (strcmp(text, "req") == 0) {
		return zmq::socket_type::req;
	}
	if (strcmp(text, "rep") == 0) {
		return zmq::socket_type::rep;
	}
	if (strcmp(text, "push") == 0) {
		return zmq::socket_type::push;
	}
	if (strcmp(text, "pull") == 0) {
		return zmq::socket_type::pull;
	}
	if (strcmp(text, "pub") == 0) {
		return zmq::socket_type::pub;
	}
	if (strcmp(text, "sub") == 0) {
		return zmq::socket_type::sub;
	}

	throw std::runtime_error("unknown zmq socket type: " + std::string(text));
}

//---------------------------------------------------------------------------
// msg_queue_t

msg_queues_t create(const stream_config_t& config) {
	msg_queues_t queues;

/*
	for (stream_config_t::const_iterator p = config.begin(); p != config.end(); ++p) {
		if (p->first == "heartbeat") {
			queues.push_back(new heartbeat_queue_t(p->second));
		} else if (p->first == "control") {
			queues.push_back(new control_queue_t(p->second));
		} else if (p->first == "echo") {
			queues.push_back(new echo_queue_t(p->second));
		} else {
			throw std::runtime_error("unknown queue type: " + p->first);
		}
	}
 */

	return queues;
}

msg_queue_t::msg_queue_t(const stream_config_entry_t& config) :
	m_config(config),
	m_socket(g_ctx, config.type)
{
	if (config.bind) {
		m_socket.bind(config.addr);
	}
	else {
		m_socket.connect(config.addr);
	}

	sm_queues.push_back(this);
}

msg_queue_t::~msg_queue_t() {
	auto p = std::find(sm_queues.begin(), sm_queues.end(), this);
	if (p != sm_queues.end()) {
		sm_queues.erase(p);
	}
}

void msg_queue_t::register_on_recv(void* ctx, recv_f* func) {
	m_recv.emplace_back(ctx, func);
}

void msg_queue_t::register_on_send(void* ctx, send_f* func) {
	m_send.emplace_back(ctx, func);
}

void msg_queue_t::do_recv(buffer_t* buf) {
	precondition(buf, "null buffer in recv");

	zmq::message_t msg;
	m_socket.recv(&msg);
	buf->assign(static_cast<const char*>(msg.data()), msg.size());

	for (recv_infos_t::iterator p = m_recv.begin(); p != m_recv.end(); ++p) {
		if (p->func()) {
			p->func()(p->ctx(), buf);
		}
	}
}

void msg_queue_t::do_send(const buffer_t& buf) {
	zmq::message_t msg(buf.c_str(), buf.size());
	m_socket.send(msg);
}

void msg_queue_t::recv(buffer_t* buf) {
}

void msg_queue_t::send(const buffer_t& buf) {
	for (send_infos_t::iterator p = m_send.begin(); p != m_send.end(); ++p) {
		if (p->func()) {
			p->func()(p->ctx(), buf);
		}
	}
}

void msg_queue_t::multiplex(long timeout) {
	typedef std::vector<zmq::pollitem_t> pollitems_t;

	precondition(!sm_queues.empty(), "nothing to multiplex");

	// build poll items array
	pollitems_t items;
	items.reserve(sm_queues.size());
	for (pqueues_t::iterator p = sm_queues.begin(); p != sm_queues.end(); ++p) {
		items.push_back({ (*p)->m_socket, 0, 0, 0 });
	}

	while (true) {
		// reset event flags
		for (size_t i = 0; i != items.size(); ++i) {
			items[i].events = ZMQ_POLLIN;
			if (!sm_queues[i]->m_send_queue.empty()) {
				items[i].events |= ZMQ_POLLOUT;
			}
		}

		// do it
		int rc = zmq::poll(&items.front(), (int)items.size(), timeout);
		invariant(-1 != rc, "poll failed");
		invariant(0 <= rc && rc <= (int)items.size(), "poll out of range");

		// who did what? handle it
		for (size_t i = 0, cnt = 0; (int)cnt != rc  &&  i != items.size(); ++i) {
			if (items[i].revents) {
				++cnt;

				// let owner pull from queue
				buffer_t buf;
				if (items[i].revents & ZMQ_POLLIN) {
					sm_queues[i]->do_recv(&buf);
				}

				// let owner push onto queue
				if (!sm_queues[i]->m_send_queue.empty() && items[i].revents & ZMQ_POLLOUT) {
					do {
						buffer_t& buf = sm_queues[i]->m_send_queue.front();
						sm_queues[i]->do_send(buf);
						sm_queues[i]->m_send_queue.pop();
					}
					while (!sm_queues[i]->m_send_queue.empty());
				}
			}
		}
	}
}

//---------------------------------------------------------------------------
// utilities

std::string format(const char* fmt, ...) {
	va_list params;
	va_start(params, fmt);
	char* buf = nullptr;
	int len = vasprintf(&buf, fmt, params);
	va_end(params);

	std::string ret;
	if (buf && len >= 0) {
		ret.assign(buf, (size_t)len);
	}

	free(buf);
	return ret;
}
