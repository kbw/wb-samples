#include <zmq.hpp>
#include <string>
#include <iostream>

int main(int argc, char* argv[])
try {
	std::string msg =  (argc > 1) ? argv[1] : "hello world";
	std::string addr = (argc > 2) ? argv[2] : "tcp://95.211.160.212:2001";

	zmq::context_t ctx(1);
	zmq::socket_t s(ctx, zmq::socket_type::push);
	s.connect(addr);

	s.send(msg.c_str(), msg.size());
	std::cout << msg << std::endl;
}
catch (const std::exception &e) {
	std::cerr << "fatal: " << e.what() << std::endl;
}
