#include <stdlib.h>

template <class T>
struct Mallocator {
	typedef T value_type;

	Mallocator() = default;

	template <class U>
	Mallocator(const Mallocator<U>&)
	{
	}

	T* allocate(std::size_t n)
	{
		return static_cast<T*>(malloc(n * sizeof(T)));
	}

	void deallocate(T* p, std::size_t)
	{
		free(p);
	}
};

template <class T, class U>
bool operator==(const Mallocator<T>&, const Mallocator<U>&)
{
	return true;
}

template <class T, class U>
bool operator!=(const Mallocator<T>&, const Mallocator<U>&)
{
	return false;
}
