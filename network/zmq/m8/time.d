syscall::accept:entry,
syscall::close:entry,
syscall::getpeername:entry,
syscall::write:entry,
syscall::setsockopt:entry,
syscall::fcntl:entry,
syscall::sendto:entry,
syscall::recvfrom:entry,
syscall::poll:entry,
syscall::kevent:entry,
syscall::getpid:entry
/execname == "rep"/
{
	ts[probefunc] = timestamp;
}

syscall::accept:return,
syscall::close:return,
syscall::getpeername:return,
syscall::write:return,
syscall::setsockopt:return,
syscall::fcntl:return,
syscall::sendto:return,
syscall::recvfrom:return,
syscall::poll:return,
syscall::kevent:return,
syscall::getpid:return
/execname == "rep" && ts[probefunc] != 0/
{
	printf("%d nsecs", timestamp - ts[probefunc]);
}

fbt:::
{
	printf("%s", probefunc);
}
