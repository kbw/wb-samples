#include "server.hpp"

#include <libm8/service.hpp>
#include <libm8/log.hpp>

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// server_t
server_t::server_t(const m8::basic_string& name, const m8::basic_string& config_filename) :
	m8::app_t(name, config_filename),
	echo_rep_queue(nullptr),
	echo_req_queue(nullptr)
{
	for (services_t::iterator p = get_services().begin(); p != get_services().end(); ++p) {
		m8::clog.format("1st=%s\n", p->first.c_str());
		m8::clog.format("2nd=%s\n", p->second->get_name().c_str());

		if (p->second->get_name() == "server") {
			for (m8::service_t::msg_queues_t::iterator q = p->second->get_queues().begin(); q != p->second->get_queues().end(); ++q) {
				m8::clog.format("1st=%s\n", q->first.c_str());
				m8::clog.format("2nd=%s\n", q->second->get_name().c_str());

				if (q->second->get_name() == "echo-rep") {
					echo_rep_queue = q->second.get();
				}
				if (q->second->get_name() == "echo-req") {
					echo_req_queue = q->second.get();
				}
			}
		}
	}

	if (echo_rep_queue) {
		echo_rep_queue->register_on_recv(this, echo_rep_callback);
	}
	if (echo_req_queue) {
		echo_req_queue->register_on_send(this, echo_req_callback);
	}
}

void server_t::echo_rep_callback(void* ctx, const m8::buffer_t* buf) {
	ensure(buf, "receive buffer is empty");

	if (server_t* self = static_cast<server_t*>(ctx)) {
		m8::clog << PREAMBLE << self->get_name() << ": recv: " << buf->size() << ": " << *buf << "\n";

		ensure(self->echo_rep_queue, "no echo_rep queue to reply on");
		self->echo_rep_queue->send(*buf);
	}
	else {
		m8::clog << PREAMBLE << "(null): recv: " << buf->size() << ": " << *buf << "\n";
	}
}

void server_t::echo_req_callback(void* ctx, const m8::buffer_t& buf) {
/*
	ensure(buf, "receive buffer is empty");

	if (server_t* self = static_cast<server_t*>(ctx)) {
		m8::clog << PREAMBLE << self->get_name() << ": recv: " << buf->size() << ": " << *buf << "\n";

		ensure(self->echo_req_queue, "no echo_req queue to reply on");
		self->echo_req_queue->send(*buf);
	}
	else {
		m8::clog << PREAMBLE << "(null): recv: " << buf->size() << ": " << *buf << "\n";
	}
 */
}
