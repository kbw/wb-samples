#include <zmq.hpp>
#include <string>
#include <iostream>

int main(int argc, char* argv[])
try {
	std::string addr = (argc > 1) ? argv[1] : "tcp://95.211.160.212:2001";

	zmq::context_t ctx(1);
	zmq::socket_t s(ctx, zmq::socket_type::pull);
	s.connect(addr);

	zmq::message_t msg;
	s.recv(&msg);

	std::string txt(static_cast<const char*>(msg.data()), msg.size());
	std::cout << txt << std::endl;
}
catch (const std::exception &e) {
	std::cerr << "fatal: " << e.what() << std::endl;
}
