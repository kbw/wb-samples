#!/bin/sh

PROG="./req"

# validate
if [ ! -x ${PROG} ]; then
	echo "missing program: ${PROG}"
	exit 1
fi

# extract n
if [ -z $1 ]; then
	# default to 1
	n="1";
else
	if [ $1 -le "0" ]; then
		echo "bad n: $n"
		exit 2
	fi
	n=$1
fi

# extract msg
if [ -z $2 ]; then
	msg=$2
else
	msg="hello world"
fi

# extract url
if [ -z $3 ]; then
	url=$3
else
	url="tcp://127.0.0.1:2000"
fi

for i in `seq 1 $n`; do
	${PROG} "${i}: ${msg}" ${url} &
done
