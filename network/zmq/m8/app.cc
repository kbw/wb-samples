#include "server.hpp"

#include <libm8/utils.hpp>
#include <libm8/app.hpp>
#include <libm8/log.hpp>

#include <memory>

#include <stdlib.h>

//---------------------------------------------------------------------------
//
int main(int, char*[])
try {
	const m8::basic_string config_name = getenv("SERV_CONFIG") ? getenv("SERV_CONFIG") : "server.json";

	m8::clog.enable(false);	// trace off
	std::unique_ptr<m8::app_t> app(new server_t("server", config_name));
	app->run();
}
catch (const std::exception& e) {
	m8::cerr << "fatal: " << e.what() << "\n";
}
