#pragma once
#include "Stream.hpp"
#include <vector>

//=============================================================================
//
class Bundle
{
public:
	typedef Stream*					stream_t;
	typedef std::vector<stream_t>	streams_t;

public:
	Bundle(const std::string& name);
	~Bundle();

	const std::string& name() const;
	void add(Stream* e);
	void remove(Stream* e);
	streams_t getStreams(Stream::mode_t m) const;

private:
	const std::string& m_name;
	streams_t m_streams;	// streams within the bundle
};
