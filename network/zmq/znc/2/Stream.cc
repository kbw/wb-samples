#include "Stream.hpp"
#include <stdexcept>

//=============================================================================
//
Stream::handle_t::handle_t(zmq::socket_t* zsock) :
	zsock(zsock),
	fd(-1)
{
}

Stream::handle_t::handle_t(int fd) :
	zsock(nullptr),
	fd(fd)
{
}

Stream::Stream(zmq::socket_t* zsock, const std::string& name, Stream::mode_t d, Stream::mode_t m) :
	m_name(name),
	m_direction(d),
	m_mode(m),
	m_handle(zsock),
	m_parent(nullptr)
{
}

Stream::Stream(int fd, const std::string& name, Stream::mode_t d, Stream::mode_t m) :
	m_name(name),
	m_direction(d),
	m_mode(m),
	m_handle(fd),
	m_parent(nullptr)
{
}

Stream::~Stream()
{
}

const std::string& Stream::name() const
{
	return m_name;
}

Stream::mode_t Stream::mode() const
{
	return m_mode;
}

void Stream::mode(Stream::mode_t m)
{
	if ((m_direction & Stream::eInput) && (m_mode & Stream::eInput)) {
		m_mode = m;
		return;
	}

	if ((m_direction & eOutput) && (m_mode & eOutput)) {
		m_mode = m;
		return;
	}

	throw std::runtime_error("bad mode in Stream::mode(mode_t)");
}

int Stream::getHandle() const
{
	return m_handle.fd;
}

zmq::socket_t* Stream::getSocket() const
{
	return m_handle.zsock;
}

Bundle* Stream::getBundle() const
{
	return m_parent;
}

void Stream::setParent(Bundle* parent)
{
	m_parent = parent;
}
