#include "Pool.hpp"
#include "Bundle.hpp"
#include "Stream.hpp"

//-----------------------------------------------------------------------------
//
Pool::Pool()
{
}

Pool::~Pool()
{
	for (auto p = m_bundles.rbegin(); p != m_bundles.rend(); ++p)
		delete *p;
}

void Pool::add(Bundle* bundle)
{
	m_bundles.push_back(bundle);
}

std::tuple<Pool::zmq_pollitems_t, Pool::streams_t> Pool::getItems(Stream::mode_t mode) const
{
	zmq_pollitems_t items;
	streams_t streams;

	for (Bundle* pbundle : m_bundles)
		for (Stream* pstream : pbundle->getStreams(mode)) {
			streams.push_back(pstream);

			short zmq_mode = 0;
			if (pstream->mode() & Stream::eInput)
				zmq_mode |= ZMQ_POLLIN;
			if (pstream->mode() & Stream::eOutput)
				zmq_mode |= ZMQ_POLLOUT;

			if (zmq_mode == 0) {
				trace << "  ignoring streeam: " << pbundle->name() << ":" << pstream->name() << std::endl;
				continue;
			}

			if (zmq::socket_t* zsock = pstream->getSocket()) {
				zmq_pollitem_t rec = { static_cast<void*>(zsock), -1, zmq_mode, 0 };
				items.push_back(rec);
				trace
					<< "  add stream: " << pbundle->name() << ":" << pstream->name() << " for "
					<< ((zmq_mode & ZMQ_POLLIN)               ? "input" :
						(zmq_mode & ZMQ_POLLOUT)              ? "output" :
						(zmq_mode & (ZMQ_POLLIN|ZMQ_POLLOUT)) ? "io" : "unknown")
					<< std::endl;
			}
			else {
				zmq_pollitem_t rec = { nullptr, pstream->getHandle(), zmq_mode, 0 };
				items.push_back(rec);
				trace
					<< "  add stream: " << pbundle->name() << ":" << pstream->name() << " for "
					<< ((zmq_mode & ZMQ_POLLIN)               ? "input" :
						(zmq_mode & ZMQ_POLLOUT)              ? "output" :
						(zmq_mode & (ZMQ_POLLIN|ZMQ_POLLOUT)) ? "io" : "unknown")
					<< std::endl;
			}
		}

	return std::make_tuple(items, streams);
}
