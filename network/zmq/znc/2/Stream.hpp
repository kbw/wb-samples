#pragma once
#include "defs.hpp"
#include <zmq.hpp>
#include <string>
#include <deque>

//=============================================================================
//
class Stream
{
public:
	struct handle_t {
		handle_t(zmq::socket_t* zsock);
		handle_t(int fd);

		zmq::socket_t* zsock;
		int fd;
	};

	typedef uint16_t mode_t;
	static const mode_t eNone = 0;
	static const mode_t eInput = 1;
	static const mode_t eOutput = 2;

public:
	Stream(zmq::socket_t* zsock, const std::string& name, mode_t d, mode_t m);
	Stream(int fd, const std::string& name, mode_t d, mode_t m);
	~Stream();

	const std::string& name() const;

	mode_t mode() const;
	void mode(mode_t m);

	int getHandle() const;
	zmq::socket_t* getSocket() const;

	Bundle* getBundle() const;
	void setParent(Bundle* parent);

private:
	std::string m_name;		// application name for stream
	mode_t m_direction;		// allowed directions
	mode_t m_mode;			// enabled directions
	handle_t m_handle;		// object handle
	Bundle* m_parent;		// owning bundle
};
