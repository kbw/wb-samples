#pragma once
#include <unistd.h>
#include <deque>
#include <fstream>

//=============================================================================
//
extern std::ofstream	trace;

//=============================================================================
//
class Pool;
class Bundle;
class Stream;
typedef std::deque<std::string> strings_t;
