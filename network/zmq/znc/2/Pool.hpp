#pragma once
#include "Stream.hpp"
#include <tuple>
#include <vector>

//=============================================================================
//
class Pool
{
public:
	typedef std::vector<zmq_pollitem_t>	zmq_pollitems_t;
	typedef ::zmq_pollitem_t			zmq_pollitem_t;
	typedef std::vector<Bundle*>		bundles_t;
	typedef std::vector<Stream*>		streams_t;
	typedef Stream*						stream_t;

public:
	Pool();
	~Pool();

	void add(Bundle* bundle);
	std::tuple<zmq_pollitems_t, streams_t>	getItems(Stream::mode_t mode) const;

private:
	bundles_t m_bundles;	// bundles
};
