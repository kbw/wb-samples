#include "Bundle.hpp"

//-----------------------------------------------------------------------------
//
Bundle::Bundle(const std::string& name) :
	m_name(name)
{
}

Bundle::~Bundle()
{
	for (auto p = m_streams.rbegin(); p != m_streams.rend(); ++p)
		delete *p;
}

const std::string& Bundle::name() const
{
	return m_name;
}

void Bundle::add(Stream* e)
{
	m_streams.push_back(e);
	e->setParent(this);
}

void Bundle::remove(Stream* e)
{
	for (auto p = m_streams.begin(); p != m_streams.end(); ++p)
		if (*p == e) {
			m_streams.erase(std::remove(m_streams.begin(), m_streams.end(), e), m_streams.end());
			delete e;
			break;
		}
}

Bundle::streams_t Bundle::getStreams(Stream::mode_t m) const
{
	streams_t streams;

	for (const stream_t& s : m_streams)
		if (s) {
			if (static_cast<int>(s->mode()) & static_cast<int>(m))
				streams.push_back(s);
		}

	return streams;
}
