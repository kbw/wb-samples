#include "Pool.hpp"
#include "Bundle.hpp"
#include "Stream.hpp"
#include <iostream>

namespace
{
	bool reset(const strings_t& msgs, Stream::mode_t& mode)
	{
		// we always want read
		Stream::mode_t working_mode = Stream::eInput;
		if (msgs.empty())
			working_mode &= ~Stream::eOutput;
		else
			working_mode |= Stream::eOutput;

		// calculate mode change
		bool changed = working_mode != mode;
		mode = working_mode;
		trace << " mode: eInput=" << ((mode & Stream::eInput) ? "on" : "off") << " eOutput=" << ((mode & Stream::eOutput) ? "on" : "off") << std::endl;
		return changed;
	}

	void parse_stream_buffer(
		strings_t& msgs,
		bool more,
		std::unique_ptr<char[]>& buf,
		std::unique_ptr<char[]>& spare_buf, char*& spare_buf_ptr)
	{
		char* p = buf.get();
		for (char* q; (q = strchr(p, '\n')); p = q + 1) {
			if (p != q) {
				msgs.emplace_back(p, q);
				trace << "   msg=" << msgs.back() << std::endl;
			}
		}

		if (more) {
			spare_buf.swap(buf);
			spare_buf_ptr = p;
		}
		else {
			if (*p) {
				msgs.emplace_back(p);
				trace << "   msg=" << msgs.back() << std::endl;
			}
		}

		trace << "   msgs.size=" << msgs.size() << std::endl;
	}

	bool read_fd(
			strings_t& msgs,
			Stream::mode_t mode, Pool* pool, Pool::zmq_pollitems_t& items, Pool::streams_t& streams, size_t i)
	{
		const size_t bufsz = 4*1024;		// one page
		std::unique_ptr<char[]> buf;		// main buffer
		std::unique_ptr<char[]> spare_buf;	// overflow buffer
		char* spare_buf_ptr = nullptr;		// overflow buffer offset

		bool more;
		do {
			more = false;

			// allocate the buffer
			if (!buf.get()) {
				buf.reset(new char[bufsz]);
				trace << "  allocate buf: bufsz=" << bufsz << std::endl;
			}
			bzero(buf.get(), bufsz);

			// read the device
			int n = read(items[i].fd, buf.get(), bufsz);
			trace << "  read=" << n << std::endl;

			if (n == 0) {
				// end of stream
				trace << "  closing fd=" << items[i].fd << std::endl;
				close(items[i].fd);
				streams[i]->getBundle()->remove(streams[i]);

				if (more) {
					trace << "  flushing buffer" << std::endl;
					spare_buf.swap(buf);
					parse_stream_buffer(msgs, more, buf, spare_buf, spare_buf_ptr);
				}

				std::tie(items, streams) = pool->getItems(mode | Stream::eInput);	// reinitialize pool items
				trace << " " << items.size() << " items" << std::endl;
				trace << " " << streams.size() << " streams" << std::endl;
				return false;
			}

			trace << "  processing buffer" << std::endl;
			more = n == bufsz;	// do we think there's more to read?
			parse_stream_buffer(msgs, more, buf, spare_buf, spare_buf_ptr);
		}
		while (more);

		return true;
	}
}

int poll(zmq_pollitem_t* data, size_t size, int ms_sleep)
{
	int rc = zmq_poll(data, size, ms_sleep);
	trace << " zmq_poll=" << rc << std::endl;

	return rc;
}

Pool* create_pool(zmq::context_t& ctx)
{
	Pool* pool = new Pool;

	if (Bundle* reader_bundle = new Bundle("reader")) {
		zmq::socket_t* pub = new zmq::socket_t(ctx, zmq::socket_type::pub);
		pub->bind("inproc://news.znc");
		reader_bundle->add(new Stream(pub, "repeater", Stream::eOutput, Stream::eOutput));

		reader_bundle->add(new Stream(STDIN_FILENO, "stdin", Stream::eInput, Stream::eInput));

		pool->add(reader_bundle);
	}

	if (Bundle* writer_bundle = new Bundle("writer")) {
		zmq::socket_t* sub = new zmq::socket_t(ctx, zmq::socket_type::sub);
		sub->connect("inproc://news.znc");
		sub->setsockopt(ZMQ_SUBSCRIBE, "", 0);					// subscribe to everything
		writer_bundle->add(new Stream(sub, "receiver", Stream::eInput, Stream::eInput));

		writer_bundle->add(new Stream(STDOUT_FILENO, "stdout", Stream::eOutput, Stream::eOutput));

		pool->add(writer_bundle);
	}

	return pool;
}

int main()
try
{
	trace << "begin" << std::endl;

	zmq::context_t	ctx(1);
	zmq_ctx_set(static_cast<void*>(ctx), ZMQ_IO_THREADS, 4);

	std::unique_ptr<Pool> pool( create_pool(ctx) );

	strings_t msgs;				// messages read to be written
	strings_t msgs2;
	Stream::mode_t mode = 0;	// stream i/o mode
	Pool::zmq_pollitems_t items;// parallel array: poll items
	Pool::streams_t streams;	// parallel array: streams

	bool stop = false;
	while (!stop) {
		// reset poolitems to wait for
		if (reset(msgs, mode)) {
			std::tie(items, streams) = pool->getItems(mode);	// initialize pool items
			trace << " " << items.size() << " items" << std::endl;
			trace << " " << streams.size() << " streams" << std::endl;
		}

		// wait for poolitems
		int rc = poll(items.data(), items.size(), 1 * 1000);

		// handle what fired
		bool check_items = rc > 0;
		for (size_t i = 0; check_items && i != items.size(); ++i) {
			// handle read
			if (items[i].revents & ZMQ_POLLIN) {
				trace << "  stream(" << i << ") " << streams[i]->name() << " fired for input" << std::endl;

				if (streams[i]->getSocket()) {
					// handle read socket
					zmq::message_t msg;
					streams[i]->getSocket()->recv(&msg);
					msgs2.push_back(std::string(static_cast<const char*>(msg.data()), msg.size()));
				}
				else if (items[i].fd != -1) {
					// handle read fd
					if (!read_fd(msgs, mode, pool.get(), items, streams, i))
						break;	// refresh items fd is closed
				}
			}

			// handle write
			if (items[i].revents & ZMQ_POLLOUT) {
				trace << "  stream(" << i << ") " << streams[i]->name() << " fired for output" << std::endl;

				if (streams[i]->getSocket()) {
					// handle write socket
					zmq::message_t msg(msgs.front().c_str(), msgs.front().size());
					streams[i]->getSocket()->send(msg);
					trace << "  msg=" <<  msgs.front() << std::endl;
					msgs.pop_front();
				}
/* 9 27 19 67 50 26
				else if (items[i].fd != -1) {
					// handle write fd
					if (streams[i]->name() == "stdout") {
						for (int n, offset = 0; offset < (int)msgs2.front().size(); offset += n) {
							n = write(items[i].fd, msgs2.front().c_str() + offset, msgs2.front().size() - offset);
							if (n <= 0)
								break;
						}
						msgs2.pop_front();

						if (msgs2.empty())
							std::tie(items, streams) = pool->getItems(mode | Stream::eInput);	// reinitialize pool items
					}
				}
 */
			}
		}

		stop = rc == 0;
	}

	trace << "done\n" << std::endl;
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << "\n" << std::endl;
}
