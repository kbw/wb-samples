#include <unistd.h>
#include <strings.h>
#include <zmq.hpp>
#include <stdexcept>
#include <thread>
#include <fstream>
#include <iostream>
#include <deque>
#include <vector>

std::ofstream trace("/tmp/out.log", std::ios::app);
zmq::context_t ctx(1);

int main()
try
{
	zmq::socket_t pub(ctx, zmq::socket_type::pub);			// publisher
	pub.bind("inproc://news.znc");

	zmq::socket_t sub(ctx, zmq::socket_type::sub);			// subscriber
	sub.connect("inproc://news.znc");
	sub.setsockopt(ZMQ_SUBSCRIBE, "", 0);					// subscribe to everything

	std::vector<zmq_pollitem_t> items = {
		{ nullptr, STDIN_FILENO, ZMQ_POLLIN, 0 },			// stdin
		{ static_cast<void*>(sub), 0, ZMQ_POLLIN, 0 },		// subscriber
		{ static_cast<void*>(pub), 0, ZMQ_POLLOUT, 0 }		// publisher
	};

	std::deque<std::string> outbuf;							// shared output buffer
	bool want_send = false;
	bool stop = false;

	while (!stop) {
		items[0].events = ZMQ_POLLIN;
		items[1].events = ZMQ_POLLIN;
		items[2].events = ZMQ_POLLOUT;
		items[0].revents = items[1].revents = items[2].revents = 0;

		// wait for inputs
		int rc = zmq_poll(items.data(), want_send ? items.size() : (items.size() - 1), 60*1000);
		trace << "zmq_poll=" << rc << std::endl;

		// consume buffer before overwriting it
		if (items[2].revents & ZMQ_POLLOUT) {
			trace << "  publish" << std::endl;

		  	if (!outbuf.empty()) {
				std::string out = outbuf.front();
				outbuf.pop_front();

				zmq::message_t msg(out.data(), out.size());
				pub.send(msg);
				trace << "    buf=\"" << out << "\"" << std::endl;

				want_send = !outbuf.empty();
			}
		}

		// process inputs
		if (items[0].revents & ZMQ_POLLIN) {
			const size_t bufsz = 4*1024;
			char buf[bufsz];
			bzero(buf, bufsz);

			int n = read(items[0].fd, buf, bufsz);
			if (n > 0 && buf[n - 1] == '\n') {
				buf[--n] = 0;
			}
			for (int i = 0; i < 1; ++i)
				outbuf.emplace_back(buf, n);
			trace << "  read=" << n << "\n    buf=\"" << outbuf.back() << "\"" << std::endl;

			if (n == 0)
				stop = true;
			else
				want_send = true;
		}
		if (items[1].revents & ZMQ_POLLIN) {
			zmq::message_t msg;
			sub.recv(&msg);
			std::string out(static_cast<const char*>(msg.data()), msg.size());

			trace << "  subscribe" << "\n    buf=\"" << out << "\"" << std::endl;
		}
	}
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << "\n";
}
