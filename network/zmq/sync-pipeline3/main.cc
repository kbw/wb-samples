#include "datafeed.hpp"
#include <sync_pipeline.hpp>

#include <stdexcept>
#include <iostream>

const std::string feed_bind		= "inproc://feed";
const std::string feed_conn		= "inproc://feed";
const std::string pipein_bind	= "inproc://pipein";
const std::string pipein_conn	= "inproc://pipein";
const std::string pipeout_bind	= "inproc://pipeout";
const std::string pipeout_conn	= "inproc://pipeout";

const timeval zmq_timeout = { 0, 500*1000 };
const size_t  zmq_workers = 8;

//---------------------------------------------------------------------------

sync_pipeline_t* create(zmq::context_t& ctx, int argc, char* argv[]);

//---------------------------------------------------------------------------

int main(int argc, char* argv[])
try
{
	zmq::context_t	ctx(1);
	std::unique_ptr<std::thread> feed(datafeed(ctx, feed_bind));	// datafeed
	std::unique_ptr<sync_pipeline_t> pipeline(create(ctx, argc, argv));

	pipeline->run();
	feed->join();
}
catch (const std::exception& e)
{
	std::clog << "error: " << e.what() << std::endl;
}
catch (...)
{
	std::clog << "error: " << "unknown" << std::endl;
}

//---------------------------------------------------------------------------

sync_pipeline_t* create(zmq::context_t& ctx, int argc, char* argv[])
{
	switch (argc)
	{
	case 1:
		return new sync_pipeline_t(
					"sync3",
					ctx, feed_conn,
					pipein_bind, pipein_conn,
					pipeout_bind, pipeout_conn,
					zmq_workers, zmq_timeout);

	default:
		wbassert(argc > 1, std::runtime_error("bad argc"));
		{
			pchars args(argv + 2, argv + argc);
			return new sync_pipeline_t(
						"sync3",
						ctx, feed_conn,
						pipein_bind, pipein_conn,
						pipeout_bind, pipeout_conn,
						args, zmq_timeout);
		}
	}
}
