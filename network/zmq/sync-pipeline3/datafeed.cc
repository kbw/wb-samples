#include "datafeed.hpp"
#include <iostream>

//---------------------------------------------------------------------------

std::thread* datafeed(zmq::context_t& ctx, const std::string& addr, size_t max_prime)
{
	auto feed = [](zmq::context_t& ctx, std::string addr, size_t max_prime)
	{
		zmq::socket_t s(ctx, zmq::socket_type::pub);
		s.bind(addr.c_str());

		for (size_t i = 0; i != max_prime; ++i)
		{
			std::string msg = "prime" + std::to_string(i) + ":{\"source\":\"test feed\"}";
			zmq_send_const(s, msg.c_str(), msg.size(), 0);

			usleep(100);
		}

		for (int i = 0; i != 4; ++i)
		{
			std::string msg = "msg" + std::to_string(i) + ":{\"channelname\":\"test feed\"}";
			zmq_send_const(s, msg.c_str(), msg.size(), 0);

			std::string txt = "txt" + std::to_string(i) + ":{\"channelname\":\"test feed\"}";
			zmq_send_const(s, txt.c_str(), txt.size(), 0);

			usleep(100);
		}
	};

	return new std::thread(feed, std::ref(ctx), addr, max_prime);
}
