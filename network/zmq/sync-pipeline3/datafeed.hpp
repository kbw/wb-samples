#pragma once
#include <zmq.hpp>
#include <string>
#include <thread>

std::thread* datafeed(zmq::context_t& ctx, const std::string& addr, size_t max_prime = 20);
