#include <zmq_helpers.hpp>
#include <string.h>

std::string work(const std::string& str);

int main(int argc, char* argv[])
{
	// argv[1] - zmq subscriber address
	// argv[2] - zmq push address
	// We subscribe for input and push our result

	if (argc != 3)
		return 1;

	zmq::context_t ctx(1);
	zmq::socket_t sub(ctx, zmq::socket_type::sub);
	zmq::socket_t push(ctx, zmq::socket_type::push);

	sub.connect(argv[1]);
	push.connect(argv[2]);

	sub.setsockopt(ZMQ_SUBSCRIBE, "", 0);

	std::string str;
	while (recv(sub, &str))
	{
		if (str.size() > 6 && strncmp(str.c_str(), "prime:", 6) == 0)
			continue;

		str = work(str);

		send(push, str);
	}
}

std::string work(const std::string& str)
{
	return str;
}
