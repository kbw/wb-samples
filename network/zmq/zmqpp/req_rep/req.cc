//  Hello World client
#include <zmqpp/zmqpp.hpp>

#include <iostream>
#include <string>
#include <string_view>

int main(int argc, char *argv[]) {
  const std::string sendtext{ (argc > 1) ? argv[1] : "Hello" };
  const std::string endpoint = "tcp://localhost:5555";

  // initialize the 0MQ context
  zmqpp::context context;

  // generate a push socket
  zmqpp::socket_type type = zmqpp::socket_type::req;
  zmqpp::socket socket (context, type);

  // open the connection
  std::cout << "Connecting to hello world server…" << std::endl;
  socket.connect(endpoint);
  for (int request_nbr = 0; request_nbr != 10; ++request_nbr) {
    // send a message
    std::cout << "Sending " << sendtext <<  " " << request_nbr << std::endl;
    // compose a message from a string and a number
    zmqpp::message message;
    message << sendtext;
    socket.send(message);

    std::string buffer;
    socket.receive(buffer);
    
//  std::cout << "Received World " << request_nbr << std::endl;
  }
}
