//  Hello World server
#include <zmqpp/zmqpp.hpp>

#include <chrono>
#include <iostream>
#include <thread>
#include <string>
#include <string_view>

int main(int argc, char *argv[]) {
  const std::string endpoint = "tcp://*:5555";

  // initialize the 0MQ context
  zmqpp::context context;

  // generate a pull socket
  zmqpp::socket_type type = zmqpp::socket_type::reply;
  zmqpp::socket socket (context, type);

  // bind to the socket
  socket.bind(endpoint);
  while (1) {
    // receive the message
    // decompose the message 
    zmqpp::message message;
    socket.receive(message);
    std::string text;
    message >> text;

    //Do some 'work'
    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Received " << text << std::endl;
    socket.send(text);
  }
}
