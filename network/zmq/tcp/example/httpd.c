/*
 *	https://libzmq.readthedocs.io/en/zeromq4-1/zmq_socket.html
 *
 *	httpd, a trivial HTTP listener to demonstrade zmq <--> socket i/o
 *
 *	The key to comms between a ZMQ_STREAM and a SOCK_STREAM is:
 *	--	you always recv the id, then recv your data
 *	--	you always send the id, then send your data
 *	--	finally, you always send with ZMQ_SNDMORE, you just have to.
 *
 *	The packets are routed, and the id is used to determine where packets go.
 *	It's a zeromq thing.
 */
#include <zmq.h>

#include <assert.h>
#include <string.h>

int main() {
	void *ctx = zmq_ctx_new ();
	assert (ctx);

	/* Create ZMQ_STREAM socket */
	void *socket = zmq_socket (ctx, ZMQ_STREAM);
	assert (socket);
	int rc = zmq_bind (socket, "tcp://*:8080");
	assert (rc == 0);

	/* Data structure to hold the ZMQ_STREAM ID */
	uint8_t id [256];
	size_t id_size = 256;

	/* Data structure to hold the ZMQ_STREAM received data */
	uint8_t raw [256];
	size_t raw_size = 256;

	while (1) {
		/*  Get HTTP request; ID frame and then request */
		id_size = zmq_recv (socket, id, 256, 0);
		printf("id_size=%lu\n", id_size);
		assert (id_size > 0);

		do {
			raw_size = zmq_recv (socket, raw, 256, 0);
			assert (raw_size >= 0);
		} while (raw_size == 256);
		printf("raw_size=%lu\n", raw_size);
		raw[raw_size] = 0;
		printf("raw=%s\n", raw);

		id_size = zmq_recv (socket, id, 256, 0);
		printf("id_size=%lu\n", id_size);
		assert (id_size > 0);

		do {
			raw_size = zmq_recv (socket, raw, 256, 0);
			assert (raw_size >= 0);
		} while (raw_size == 256);
		printf("raw_size=%lu\n", raw_size);
		raw[raw_size] = 0;
		printf("raw=%s\n", raw);

		/* Prepares the response */
		char http_response [] =
			"HTTP/1.0 200 OK\r\n"
			"Content-Type: text/plain\r\n"
			"\r\n"
			"Hello, World!";
		/* Sends the ID frame followed by the response */
		zmq_send (socket, id, id_size, ZMQ_SNDMORE);
		zmq_send (socket, http_response, strlen (http_response), ZMQ_SNDMORE);
		/* Closes the connection by sending the ID frame followed by a zero response */
		zmq_send (socket, id, id_size, ZMQ_SNDMORE);
		zmq_send (socket, 0, 0, ZMQ_SNDMORE);
		/* NOTE: If we don't use ZMQ_SNDMORE, then we won't be able to send more */
		/* message to any client */
	}
	zmq_close (socket);
	zmq_ctx_destroy (ctx);
}
