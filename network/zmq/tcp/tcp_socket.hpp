#pragma once

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <spdlog/spdlog.h>

#include <atomic>
#include <chrono>
#include <iostream>
#include <stdexcept>
#include <functional>
#include <string>
#include <string_view>
#include <thread>
#include <unordered_map>

#include <stdlib.h>
#include <string.h>

namespace ka::net::experimental {
using port_t = uint16_t;
constexpr port_t default_port = 27367;

port_t to_port(const char* str);

class tcp_socket_t {
public:
	using socket_t = int;
	using recv_callback_t = std::function<std::string(socket_t, std::string_view)>;

	// client
	tcp_socket_t(const std::string& host, port_t port) {
		sock_ = ::socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sock_ == bad_socket_)
			throw std::runtime_error("socket create failed");
		set_linger(sock_, true, 0);
		spdlog::info("{}: socket created: port:{}", "tcp_socket_t(client)", port);

		memset(&addr_, 0, sizeof(addr_));
		addr_.sin_family = AF_INET;
		addr_.sin_addr.s_addr = INADDR_ANY;
		//inet_aton(host.c_str(), &addr_.sin_addr.s_addr);
		::inet_aton(host.c_str(), &addr_.sin_addr);
		addr_.sin_port = htons(port);
		int rc = ::connect(sock_, reinterpret_cast<sockaddr*>(&addr_), sizeof(addr_));
		if (rc == posix_fail_)
			throw std::runtime_error("connect failed");

		std::this_thread::sleep_for(std::chrono::seconds(0));
	}

	// server
	tcp_socket_t(port_t port) {
		sock_ = ::socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sock_ == bad_socket_)
			throw std::runtime_error("socket create failed");
		set_linger(sock_, true, 0);
		spdlog::info("{}: socket created: port:{}", "tcp_socket_t(server)", port);

		memset(&addr_, 0, sizeof(addr_));
		addr_.sin_family = AF_INET;
		addr_.sin_addr.s_addr = INADDR_ANY;
		addr_.sin_port = htons(port);
		int rc = ::bind(sock_, reinterpret_cast<sockaddr*>(&addr_), sizeof(addr_));
		if (rc == posix_fail_)
			throw std::runtime_error("bind failed");
		spdlog::info("{}: bind ok", "tcp_socket_t(server)");

		rc = ::listen(sock_, 2 << 3);
		if (rc == posix_fail_)
			throw std::runtime_error("listen failed");
		spdlog::info("{}: listen ok", "tcp_socket_t(server)");

		acceptor_ = std::thread([&]() {
				set_blocking(sock_, false);
				while (!stop_) {
					struct sockaddr_in cli_addr;
					socklen_t len = sizeof(cli_addr);
					socket_t client_sock = ::accept(sock_, reinterpret_cast<sockaddr*>(&cli_addr), &len);
					if (client_sock == bad_socket_) {
						if (errno == EWOULDBLOCK) {
							std::this_thread::sleep_for(std::chrono::nanoseconds(100));
							continue;
						}

						stop_ = true;
						std::cerr << "accept failed: " << client_sock << std::endl;
					}
					spdlog::info("{}: acceptor thread: accepted connection from: {}:{}", "tcp_socket_t(server)",
						::inet_ntoa(cli_addr.sin_addr),
						ntohs(cli_addr.sin_port));
					handle_client(client_sock);
				}
				set_blocking(sock_, true);
			});
		std::this_thread::sleep_for(std::chrono::seconds(0));
		spdlog::info("{}: acceptor thread running", "tcp_socket_t(server)");
	}

	~tcp_socket_t() {
		spdlog::info("{}: stopping", "~tcp_socket_t");
		stop_ = true;
		stop_thread(acceptor_);
		spdlog::info("{}: raw_socket: acceptor stopped", "~tcp_socket_t");
		for (auto& client : clients_) {
			client.second.stop = true;
			stop_thread(client.second.worker);
		}
		spdlog::info("{}: raw_socket: clients stopped", "~tcp_socket_t");

		if (sock_ != bad_socket_) {
			::close(sock_);
			sock_ = bad_socket_;
		}
		spdlog::info("{}: raw_socket: server sock closed", "~tcp_socket_t");
	}

	tcp_socket_t(tcp_socket_t &&) = delete;
	tcp_socket_t& operator=(tcp_socket_t &&) = delete;
	tcp_socket_t(const tcp_socket_t &) = delete;
	tcp_socket_t& operator=(const tcp_socket_t &) = delete;

	ssize_t send(std::string_view str) {
		return ::send(sock_, str.data(), str.size(), 0);
	}

	void on_recv(recv_callback_t recv_callback) {
		spdlog::info("{}: on_recv", "tcp_socket_t::on_recv");
		recv_callback_ = &recv_callback;
	}

private:
	bool set_blocking(socket_t fd, bool blocking) {
		int flags = fcntl(fd, F_GETFL, 0);
		flags = blocking ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
		return ::fcntl(fd, F_SETFL, flags) == 0;
	}

	bool set_linger(socket_t s, bool on, int timeout) {
		struct linger so_linger;
		so_linger.l_onoff = on;
		so_linger.l_linger = timeout;
	 	return ::setsockopt(s, SOL_SOCKET, SO_LINGER, &so_linger, sizeof(so_linger)) == 0;
	}

	void stop_thread(std::thread& thread) {
		spdlog::info("{}: stop_thread", "tcp_socket_t::stop_thread");
		if (thread.joinable())
			thread.join();
	}

	void handle_client(socket_t client_sock) {
		spdlog::info("{}: handle_client", "tcp_socket_t::handle_client");
		clients_t::iterator iter;
		{
			std::lock_guard lock(clients_mtx_);
			iter = clients_.emplace(std::piecewise_construct,
					std::forward_as_tuple(client_sock),
					std::forward_as_tuple(false, std::thread{})).first;
		}
		worker_info_t& worker_info = iter->second;
		spdlog::info("{}: inserted client", "tcp_socket_t::handle_client");
		worker_info.worker = std::thread([&]() {
				while (!stop_ && !worker_info.stop) {
					fd_set rdset;
					FD_ZERO(&rdset);
					FD_SET(sock_, &rdset);
					struct timeval timeout{0, 500*1000};
					posix_ret_t rc = ::select(sock_, &rdset, nullptr, nullptr, &timeout);
					spdlog::info("{}: select: {}", "tcp_socket_t::handle_client", rc);
					if (rc == posix_fail_) {
						worker_info.stop = true;
						std::cerr << "select failed" << std::endl;
					}
					if (FD_ISSET(sock_, &rdset)) {
						char buffer[2 << 9];
						ssize_t nbytes = ::recv(sock_, buffer, sizeof(buffer), 0);
						if (nbytes <= 0) {
							worker_info.stop = true;
							continue;
						}

						if (recv_callback_)
							(*recv_callback_)(client_sock, std::string_view{buffer, static_cast<size_t>(nbytes)});
					}
				}
				remove_client(client_sock);
			});
	}

	void remove_client(socket_t client_sock) {
		spdlog::info("{}: socket: {}", "tcp_socket_t::remove_client", client_sock);
		std::lock_guard lock(clients_mtx_);
		auto iter = clients_.find(client_sock);
		if (iter != clients_.end()) {
			iter->second.stop = true;
			stop_thread(iter->second.worker);
			::close(client_sock);
			clients_.erase(iter);
		}
	}

private:
	using posix_ret_t = int;

	// client connection handlers for server
	struct worker_info_t {
		std::atomic<bool> stop;
		std::thread worker;
	};
	using clients_t = std::unordered_map<socket_t, worker_info_t>;

	socket_t sock_{bad_socket_};
	struct sockaddr_in addr_;
	std::atomic<bool> stop_;
	std::thread acceptor_;
	std::mutex clients_mtx_;
	clients_t clients_;
	recv_callback_t* recv_callback_{};

	static constexpr socket_t bad_socket_{-1};
	static constexpr posix_ret_t posix_fail_{-1};
};

inline port_t to_port(const char* str) {
	if (str)
		if (port_t port = static_cast<port_t>(atoi(str)))
			return port;
	return default_port;
}
}  // namespace ka::net::experimental
