#include "../raw_socket.hpp"
#include <zmq.hpp>

int main(int argc, char* argv[])
try {
	const port_t server_port = to_port(argv[1]);
	const std::string host = "192.168.68.63";
	const std::string url = "tcp://" + host + ":" + std::to_string(server_port);
	spdlog::info("url: {}", url);

	raw_socket_t server(server_port);
	server.on_recv([](std::string_view in) -> std::string {
			return "recv: " + std::string(in.data(), in.size());
		});
	//std::this_thread::sleep_for(std::chrono::seconds(1));

	zmq::context_t ctx(4);
	zmq::socket_t zclient(ctx, zmq::socket_type::stream);
	zclient.connect(url);

	zmq::message_t id_msg;
	zclient.recv(id_msg);
	uint32_t id = id_msg.routing_id();
	//std::string id = zclient.get(zmq::sockopt::routing_id_t);

	auto rc = zclient.send(id_msg, zmq::send_flags::sndmore);
	zclient.send(zmq::message_t{}, zmq::send_flags::sndmore);
	spdlog::info("send id returned: {}", (rc.has_value() ? std::to_string(rc.value()) : std::string("no value")));

	zclient.send(id_msg, zmq::send_flags::sndmore);
	zmq::message_t msg{"hello world", 11};
	rc = zclient.send(msg, zmq::send_flags::sndmore);
	spdlog::info("send msg returned: {}", (rc.has_value() ? std::to_string(rc.value()) : std::string("no value")));

	//system("netstat -4ltp --numeric-ports | egrep \"Proto|27367\"");
	//system("netstat -4tp --numeric-ports | egrep \"Proto|27367\"");
	//std::this_thread::sleep_for(std::chrono::seconds(5));
}
catch (const std::exception& e) {
	spdlog::critical("{}", e.what());
}
