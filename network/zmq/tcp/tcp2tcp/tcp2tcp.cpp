#include "../tcp_socket.hpp"

#include <functional>
#include <string>
#include <string_view>

using namespace std::literals;

using socket_t = ka::net::experimental::tcp_socket_t::socket_t;
using tcp_socket_t = ka::net::experimental::tcp_socket_t;
using port_t = ka::net::experimental::port_t;

const std::string protocol = "tcp://";
const std::string host = "192.168.68.63";

int main(int argc, char* argv[])
try {
	spdlog::set_pattern("[%Y-%m-%d %H:%M:%S.%f] [%^%l%$] %v");

	const port_t server_port = ka::net::experimental::to_port(argv[1]);
	tcp_socket_t server(server_port);
	server.on_recv([](socket_t sock, std::string_view in) -> std::string {
			std::string out = "recv(" + std::to_string(sock) + "): " + std::string(in.data(), in.size());
			spdlog::info("{}: {}", "main", out);
			return out;
		});
	//std::this_thread::sleep_for(std::chrono::seconds(1));

	const port_t client_port = ka::net::experimental::to_port(argv[1]);
	//const std::string client_url = protocol + host + ":" + std::to_string(client_port);
	tcp_socket_t client(host, client_port);
	ssize_t nbytes = client.send("hello world"sv);
	spdlog::info("{}: tcp send returned: {}", "main", nbytes);

	std::this_thread::sleep_for(std::chrono::seconds(2));
}
catch (const std::exception& e) {
	spdlog::critical("{}", e.what());
	return 1;
}
