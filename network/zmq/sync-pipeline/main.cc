#include "zmq_helpers.hpp"
#include "time_helpers.hpp"

#include <string>		// helpers
#include <thread>
#include <memory>
#include <list>
#include <vector>

#include <exception>	// error handling
#include <iostream>

const std::string feed_bind		= "inproc://feed";
const std::string feed_conn		= "inproc://feed";
const std::string pipein_bind	= "inproc://pipein";
const std::string pipein_conn	= "inproc://pipein";
const std::string pipeout_bind	= "inproc://pipeout";
const std::string pipeout_conn	= "inproc://pipeout";

const timeval zmq_timeout = { 0, 500*1000 };
const size_t  zmq_workers = 8;

//---------------------------------------------------------------------------

typedef std::list<std::pair<zmq::socket_t, zmq::socket_t>> sockets_t;

zmq::socket_t create_sub(zmq::context_t& ctx, const std::string& addr);
zmq::socket_t create_push(zmq::context_t& ctx, const std::string& addr);
zmq::socket_t create_pull(zmq::context_t& ctx, const std::string& addr);
sockets_t     create_workers(
				zmq::context_t& ctx,
				const std::string& addr_in, const std::string& addr_out,
				size_t n);

void          process(
				zmq::context_t& ctx,
				zmq::socket_t& in, zmq::socket_t& push, zmq::socket_t& pull,
				sockets_t& workers);

std::thread*  datafeed(zmq::context_t& ctx, const std::string& feed);

//---------------------------------------------------------------------------

int main()
try
{
	zmq::context_t ctx(1);

	std::unique_ptr<std::thread> feed(datafeed(ctx, feed_bind));	// datafeed

	zmq::socket_t in = create_sub(ctx, feed_conn);					// subscribes to datafeed
	zmq::socket_t push = create_push(ctx, pipein_bind);				// pipeline begin
	zmq::socket_t pull = create_pull(ctx, pipeout_bind);			// pipeline end
	sockets_t workers  = create_workers(ctx, pipein_conn, pipeout_conn,zmq_workers);	// workers

	process(ctx, in, push, pull, workers);
	feed->join();
}
catch (const std::exception& e)
{
	std::clog << "error: " << e.what() << std::endl;
}
catch (...)
{
	std::clog << "error: " << "unknown" << std::endl;
}

//---------------------------------------------------------------------------

zmq::socket_t create_sub(zmq::context_t& ctx, const std::string& addr)
{
	zmq::socket_t s(ctx, zmq::socket_type::sub);
	s.connect(addr.c_str());

	int val = to_msec(zmq_timeout);
	s.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(val));

	s.setsockopt(ZMQ_SUBSCRIBE, "msg", 3);
	return s;
}

zmq::socket_t create_push(zmq::context_t& ctx, const std::string& addr)
{
	zmq::socket_t s(ctx, zmq::socket_type::push);
	s.bind(addr.c_str());

	int val = to_msec(zmq_timeout);
	s.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(val));
	return s;
}

zmq::socket_t create_pull(zmq::context_t& ctx, const std::string& addr)
{
	zmq::socket_t s(ctx, zmq::socket_type::pull);
	s.bind(addr.c_str());

	int val = to_msec(zmq_timeout);
	s.setsockopt(ZMQ_RCVTIMEO, &val, sizeof(val));
	return s;
}

sockets_t create_workers(zmq::context_t& ctx, const std::string& addr_in, const std::string& addr_out, size_t n)
{
	sockets_t s;
	for (size_t i = 0; i != n; ++i)
	{
		s.emplace_back(
			std::make_pair(
				zmq::socket_t(ctx, zmq::socket_type::pull),
				zmq::socket_t(ctx, zmq::socket_type::push)));

		s.front().first.connect(addr_in.c_str());
		s.front().second.connect(addr_out.c_str());

		int val = to_msec(zmq_timeout);
		s.front().first.setsockopt(ZMQ_RCVTIMEO, &val, sizeof(val));
		val = to_msec(zmq_timeout);
		s.front().second.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(val));
	}

	return s;
}

void process(zmq::context_t& ctx, zmq::socket_t& sub, zmq::socket_t& push, zmq::socket_t& pull, sockets_t& workers)
{
	auto f_push = [](zmq::socket_t& sub, zmq::socket_t& push, zmq::socket_t& sync, size_t nworkers)
	{
		// copy from sub to push
		std::string str;;
		while (true)
		{
			if (!recv(sub, &str, to_msec(zmq_timeout)))
			{
				std::clog << "push: done" << std::endl;
				break;
			}

			for (size_t i = 0; i != nworkers; ++i)
			{
				send(push, str, to_msec(zmq_timeout));
				std::clog << "push:\"" << str << "\"" << std::endl;
			}

			std::clog << "push: listening" << std::endl;
			if (recv(sync, &str, to_msec(zmq_timeout)))
			{
				std::clog << "push: recieve sync" << std::endl;
//				send(sync, str, to_msec(zmq_timeout));
			}
			else
				std::clog << "push: missed sync" << std::endl;
			std::clog << std::endl;
		}
	};

	auto f_worker = [](zmq::socket_t& pull, zmq::socket_t& push)
	{
		// copy from push to workers
		auto work = [](std::string s)
		{
			std::cout << "work:\"" + s << "\"" << std::endl;
			return s;
		};

		while (true)
		{
			std::string str;
			if (!recv(pull, &str, to_msec(zmq_timeout)))
			{
				std::clog << "work: timed out" << std::endl;
				break;
			}

			str = work(str);

			send(push, str, to_msec(zmq_timeout));
			std::clog << "work: done" << std::endl;
		}
	};

	auto f_pull = [](zmq::socket_t& pull, zmq::socket_t& sync, size_t nworkers)
	{
		// copy from workers to pull
		std::string str;
		bool timed_out = false;
		while (!timed_out)
		{
			for (size_t i = 0; i != nworkers; ++i)
			{
				if (!recv(pull, &str, to_msec(zmq_timeout)))
				{
					std::clog << "pull: timed out" << std::endl;
					timed_out = true;
					break;
				}

				std::clog << "pull:\"" << str << "\"" << std::endl;
			}

			if (send(sync, "done", to_msec(zmq_timeout)))
			{
				std::clog << "pull: send sync" << std::endl;
//				recv(sync, &str, to_msec(zmq_timeout));;
			}
			else
				std::clog << "pull: sync failed" << std::endl;
		}
	};

	std::pair<zmq::socket_t, zmq::socket_t> sync = {
		zmq::socket_t(ctx, zmq::socket_type::pair),
		zmq::socket_t(ctx, zmq::socket_type::pair)
	};

	sync.first.bind("inproc://sync-pipeline");
	sync.second.connect("inproc://sync-pipeline");
	
	int val = to_msec(zmq_timeout);
	sync.first.setsockopt(ZMQ_RCVTIMEO, &val, sizeof(int));
	sync.first.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(int));
	sync.second.setsockopt(ZMQ_RCVTIMEO, &val, sizeof(int));
	sync.second.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(int));

	typedef std::vector<std::unique_ptr<std::thread>> threads_t;

	const size_t nworkers = workers.size();

	threads_t threads;
	threads.push_back(
		threads_t::value_type(
			new std::thread(f_push, std::ref(sub), std::ref(push), std::ref(sync.first), nworkers)));
	threads.push_back(
		threads_t::value_type(
			new std::thread(f_pull, std::ref(pull), std::ref(sync.second), nworkers)));
	for (sockets_t::iterator p = workers.begin(); p != workers.end(); ++p)
		threads.push_back(
			threads_t::value_type(
				new std::thread(f_worker, std::ref(p->first), std::ref(p->second))));

	for (threads_t::iterator p = threads.begin(); p != threads.end(); ++p)
		(*p)->join();
}

std::thread* datafeed(zmq::context_t& ctx, const std::string& addr)
{
	auto feed = [](zmq::context_t& ctx, std::string addr)
	{
		zmq::socket_t s(ctx, zmq::socket_type::pub);
		s.bind(addr.c_str());

		for (int i = 0; i != 4; ++i)
		{
			std::string msg = "msg" + std::to_string(i) + ":{\"channelname\":\"test feed\"}";
			zmq_send_const(s, msg.c_str(), msg.size(), 0);

			std::string txt = "txt" + std::to_string(i) + ":{\"channelname\":\"test feed\"}";
			zmq_send_const(s, txt.c_str(), txt.size(), 0);

			usleep(100);
		}
	};

	return new std::thread(feed, std::ref(ctx), addr);
}
