#include <string>
#include <unistd.h>
#include <sys/time.h>
#include <sys/timespec.h>
#include <stdlib.h>

inline int atoi(const std::string& s)
{
	return atoi(s.c_str());
}

inline long atol(const std::string& s)
{
	return atol(s.c_str());
}

inline bool operator<(const timespec& a, const timespec& b)
{
	if (a.tv_sec < b.tv_sec)
		return true;

	return (a.tv_sec == b.tv_sec) && (a.tv_nsec < b.tv_nsec);
}

inline timespec operator+(const timespec& a, const timespec& b)
{
	const long SEC = 1000 * 1000 * 1000;

	timespec c = { a.tv_sec + b.tv_sec, a.tv_nsec + b.tv_nsec };
	if (c.tv_nsec > SEC)
	{
		++c.tv_sec;
		c.tv_nsec -= SEC;
	}

	return c;
}

inline timespec operator-(const timespec& a, const timespec& b)
{
	const long SEC = 1000 * 1000 * 1000;

	timespec c = { a.tv_sec - b.tv_sec, a.tv_nsec - b.tv_nsec };
	if (c.tv_nsec < 0)
	{
		--c.tv_sec;
		c.tv_nsec += SEC;
	}

	return c;
}

inline bool operator<(const timeval& a, const timeval& b)
{
	if (a.tv_sec < b.tv_sec)
		return true;

	return (a.tv_sec == b.tv_sec) && (a.tv_usec < b.tv_usec);
}

inline timeval operator+(const timeval& a, const timeval& b)
{
	const long SEC = 1000 * 1000;

	timeval c = { a.tv_sec + b.tv_sec, a.tv_usec + b.tv_usec };
	if (c.tv_usec > SEC)
	{
		++c.tv_sec;
		c.tv_usec -= SEC;
	}

	return c;
}

inline timeval operator-(const timeval& a, const timeval& b)
{
	const long SEC = 1000 * 1000;

	timeval c = { a.tv_sec - b.tv_sec, a.tv_usec - b.tv_usec };
	if (c.tv_usec < 0)
	{
		--c.tv_sec;
		c.tv_usec += SEC;
	}

	return c;
}

inline long to_nsec(const timespec& t)
{
	const long SEC = 1000 * 1000 * 1000;

	return SEC*t.tv_sec + t.tv_nsec;
}

inline long to_usec(const timespec& t)
{
	return to_nsec(t) / 1000;
}

inline long to_msec(const timespec& t)
{
	return to_usec(t) / 1000;
}

inline long to_sec(const timespec& t)
{
	return to_msec(t) / 1000;
}

inline long to_usec(const timeval& t)
{
	const long SEC = 1000 * 1000;

	return SEC*t.tv_sec + t.tv_usec;
}

inline long to_msec(const timeval& t)
{
	return to_usec(t) / 1000;
}

inline long to_sec(const timeval& t)
{
	return to_msec(t) / 1000;
}

inline timeval now()
{
	struct timeval t;
	gettimeofday(&t, nullptr);
	return t;
}
