#pragma once
#include "defs.hpp"

int create_peer(pchars args);
int create_child(pchars args);
int spawn(pchars args);
