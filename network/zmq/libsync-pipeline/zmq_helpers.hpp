#pragma once
#include <zmq.hpp>
#include <iostream>

inline bool recv(zmq::socket_t& s, zmq::message_t* msg, long timeout = -1)
{
	zmq_pollitem_t item = { s, 0, ZMQ_POLLIN, 0 };

	zmq_poll(&item, 1, timeout);
	if (item.revents & ZMQ_POLLIN)
	{
		s.recv(msg);
		return true;
	}

	return false;
}

inline bool recv(zmq::socket_t& s, std::string* str, long timeout = -1)
{
	zmq::message_t msg;
	if (recv(s, &msg, timeout))
	{
		if (str)
			*str = std::string(static_cast<const char*>(msg.data()), msg.size());
		return true;
	}

	return false;
}

inline bool send(zmq::socket_t& s, const std::string& str, long timeout = -1)
{
	zmq_pollitem_t item = { s, 0, ZMQ_POLLOUT, 0 };

	zmq_poll(&item, 1, timeout);
	if (item.revents & ZMQ_POLLOUT)
	{
		int rc = zmq_send_const(s, str.c_str(), str.size(), 0);
		if (rc < 0)
			std::clog << "send failed: " << rc << std::endl;
		return true;
	}

	return false;
}

inline bool send(zmq::socket_t& s, const zmq::message_t& msg, long timeout = -1)
{
	std::string str = { static_cast<const char*>(msg.data()), msg.size() };
	return send(s, str, timeout);
}

template <typename CONTAINER>
inline bool send(CONTAINER& c, const zmq::message_t& msg, long timeout = -1)
{
	std::vector<zmq_pollitem_t> items(c.size(), { NULL, 0, ZMQ_POLLOUT, 0 });
	{
		size_t idx = 0;
		for (typename CONTAINER::iterator p = c.begin(); p != c.end(); ++p)
			items[idx++].socket = *c;
	}

	size_t cnt = 0;
	zmq_poll(&items.front(), items.size(), timeout);
	for (size_t idx = 0; idx != items.size(); ++idx)
		if (items[idx].revents & ZMQ_POLLOUT)
		{
			int rc = zmq_send_const(items[idx].socket, msg.data(), msg.size(), 0);
			if (rc < 0)
				std::clog << "send failed: " << rc << std::endl;
			++cnt;
		}

	return cnt == items.size();
}
