#include <zmq.hpp>
#include <stdexcept>
#include <iostream>

zmq::socket_type to_zmq_type(const std::string& str);
zmq::socket_t create(int argc, char* argv[]);
void usage();

zmq::context_t ctx(1);

int main(int argc, char* argv[])
try
{
	zmq::socket_t s = create(argc, argv);
}
catch (const std::exception& e)
{
	std::cerr << "fatal: " << e.what() << std::endl;
	usage();
}

zmq::socket_t create(int argc, char* argv[])
{
	if (argc > 1) {
		int i;
		for (i = 1; i < argc; ++i) {
			bool parsed = false;

			char* p = strchr(argv[i], ':');
			if (!p)
				break;	// no zmq type

			char* q = strchr(p + 1, ':');
			if (!q)
				break;	// no protocol

			if (strncasecmp(q, "://", 3))
				break;	// not a url

			char* r = strchr(q + 1, ':');
			if (!r)
				break;	// no port

			parsed = true;
			std::string zmqtype(argv[i], p);
			std::string proto(p + 1, q);
			std::string host(q + 1, r);
			std::string port(r + 1);
			std::string addr(p + 1);

			zmq::socket_t s(ctx, to_zmq_type(zmqtype));
			s.connect(addr);
			return s;
		}

		throw std::runtime_error("error: cannot parse " + std::string(argv[i]));
	}

	throw std::runtime_error("no args");
}

void usage()
{
	std::cout << "test_zmq type:addr" << std::endl;
	std::cout << "e.g. sub:tcp://127.0.0.1:6789" << std::endl;
}

zmq::socket_type to_zmq_type(const std::string& str)
{
	return
		str == "pub" ? zmq::socket_type::pub :
		str == "sub" ? zmq::socket_type::sub :
		str == "req" ? zmq::socket_type::req :
		str == "rep" ? zmq::socket_type::rep :
		str == "push" ? zmq::socket_type::push :
		str == "pull" ? zmq::socket_type::pull : zmq::socket_type::sub;
}
