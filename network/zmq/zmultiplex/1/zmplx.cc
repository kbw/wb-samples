#include <zmplx/zmplx.hpp>
#include <exception>
#include <stdio.h>

//---------------------------------------------------------------------------

int main(int argc, char* argv[])
try {
	zmplx::options_t opts = std::for_each(argv + 1, argv + argc, zmplx::options_t());
	zmplx::session_t grp(opts);
}
catch (const std::exception& err) {
	fprintf(stderr, "fatal: %s\n", err.what());
	return 1;
}
catch (...) {
	fprintf(stderr, "unknown fatal\n");
	return 1;
}
