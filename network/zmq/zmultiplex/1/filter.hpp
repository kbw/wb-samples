#include "defs.hpp"
#include <utility>
#include <sstream>

//----------------------------------------------------------------------

struct filter_t {
	virtual ~filter_t() {
	}

	virtual string_t transform(const string_t& in) {
		return in;
	}
};

struct partition_filter_t : public filter_t {
	partition_filter_t(string_t& buffer) :
		buffer(std::move(buffer)) {
	}

	virtual operator bool const () {
		return false;
	}

	virtual string_t get() {
		return buffer;
	}

protected:
	virtual string_t& get_internal() {
		return buffer;
	}

	string_t buffer;
};

struct line_filter_t : partition_filter_t {
	line_filter_t(string_t& buffer) :
		partition_filter_t(buffer) {

		append(get_internal());
	}

	virtual operator bool const () {
		return static_cast<bool>(ss);
	}

	virtual string_t get() {
		string_t line;
		std::getline(ss, line);
		return line;
	}

	virtual void append(const string_t& str) {
		ss.write(str.c_str(), str.size());
	}

	std::stringstream ss;
};
