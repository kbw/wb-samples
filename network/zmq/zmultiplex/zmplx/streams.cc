#include "streams.hpp"
#include "options.hpp"

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <zmq.hpp>

#include <stdexcept>
#include <stdlib.h>

//----------------------------------------------------------------------

namespace {
	using namespace zmplx;

	void validate_input(data_streams_t& streams)
	{
		if (streams.empty())
			throw std::runtime_error("no input streams");
	}

	void validate_output(data_streams_t& streams)
	{
		if (streams.empty())
			throw std::runtime_error("no output streams");
	}
}

namespace zmplx {
	data_stream_t::~data_stream_t()
	{
		if (close_) {
			switch (handle_.tag_) {
			case type_t::tag_t::fd:		close(handle_.value_.fd);		break;
			case type_t::tag_t::zsock:	delete handle_.value_.zsock;	break;
			case type_t::tag_t::unknown:
			default:
				;
			}
		}
	}

	data_stream_t create_stream(zmq::context_t& ctx, const filename_t& name, const zopts_t& opts, iotype_t io)
	{
		// name format: [pub|sub|pull|push|req|rep]:[zmq protocol]://[address]:port
		if (const char* p = strchr(name.c_str(), ':')) {
			string_t proto(name.c_str(), p);
			string_t addr(p + 1);

			if (zmq::socket_t* zsock =
					(proto == "pub") ? new zmq::socket_t(ctx, zmq::socket_type::pub) :
					(proto == "pull")? new zmq::socket_t(ctx, zmq::socket_type::pull):
					(proto == "req") ? new zmq::socket_t(ctx, zmq::socket_type::req) :
					(proto == "sub") ? new zmq::socket_t(ctx, zmq::socket_type::sub) :
					(proto == "push")? new zmq::socket_t(ctx, zmq::socket_type::push):
					(proto == "rep") ? new zmq::socket_t(ctx, zmq::socket_type::rep) : nullptr) {

				for (const zopts_t::value_type& opt : opts) {
					int val = atoi(opt.second.c_str());

					switch (opt.first) {
					case option_t::affinity:
						zsock->setsockopt(ZMQ_AFFINITY, &val);
						break;

					case option_t::hwm:
						zsock->setsockopt(ZMQ_SNDHWM, &val);
						zsock->setsockopt(ZMQ_RCVHWM, &val);
						break;

					case option_t::subscribe:
						zsock->setsockopt(ZMQ_SUBSCRIBE, (int*)opt.second.c_str());
						break;

					case option_t::unknown:
					default:
						;
					}
				}

				switch (io) {
				case iotype_t::input:  zsock->connect(addr); break;
				case iotype_t::output: zsock->bind(addr);	break;
				case iotype_t::unknown:
				default:
					delete zsock;
					throw std::runtime_error("dunno whether \"" + string_t(name) + "\" is input or output");
				}

				return data_stream_t(name, zsock);
			}

			throw std::runtime_error("cannot create zmq stream of type=" + proto);
		}
		else if (io == iotype_t::input && (name == "-" || name == "stdin")) {
			return data_stream_t(name, STDIN_FILENO, false);
		}
		else if (io == iotype_t::output && (name == "-" || name == "stdout")) {
			return data_stream_t(name, STDOUT_FILENO, false);
		}
		else if (io == iotype_t::output && name == "stderr") {
			return data_stream_t(name, STDERR_FILENO, false);
		}
		else {
			int mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
			int fd = io == iotype_t::input  ? open(name.c_str(), O_RDONLY) :
			         io == iotype_t::output ? creat(name.c_str(), mode)	: -1;
			if (fd == -1)
				throw std::runtime_error("cannot open: \"" + name + "\" for " + (io == iotype_t::input ? "read" : "write"));
			return data_stream_t(name, fd);
		}
	}

	data_streams_t create_streams(zmq::context_t& ctx, const filenames_t& files, iotype_t io)
	{
		data_streams_t streams;

		for (const std::pair<filename_t, zopts_t>& file : files) {
			// name format: [pub|sub|pull|push|req|rep]:[zmq protoco]l://[addres]s:port
			const filename_t& name = file.first;
			const zopts_t& opts = file.second;

			streams.emplace_back(create_stream(ctx, name, opts, io));
		}

		return streams;
	}

	stream_pair_t create_streams(const options_t& opts)
	{
		stream_pair_t set {
			create_streams(*opts.ctx, opts.ifiles, iotype_t::input),
			create_streams(*opts.ctx, opts.ofiles, iotype_t::output)
		};

		validate_input(set.first);
		validate_output(set.second);

		return set;
	}
}
