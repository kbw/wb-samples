#pragma once

#include <stdexcept>
#include <istream>
#include <ostream>
#include <sstream>

namespace
{
	using namespace zmplx;

	//------------------------------------------------------------------

	inline msg_t::~msg_t()
	{
	}

	inline int msg_t::get_type() const
	{
		throw std::runtime_error("called pure virtual function: msg_t::get_type");
	}

	inline void msg_t::save(std::ostream&)
	{
		throw std::runtime_error("called pure virtual function: msg_t::save");
	}

	//------------------------------------------------------------------

	inline add_istream_msg_t::add_istream_msg_t(const filename_t& name) : name(name)
	{
	}

	inline add_istream_msg_t::add_istream_msg_t(std::istream& is)
	{
		std::getline(is, name);		
	}

	inline add_istream_msg_t::~add_istream_msg_t()
	{
	}

	inline int add_istream_msg_t::get_type() const
	{
		return id;
	}

	inline filename_t add_istream_msg_t::get_name() const
	{
		return name;
	}

	inline void add_istream_msg_t::save(std::ostream& os)
	{
		os	<< id << '\n'
			<< name;
	}

	//------------------------------------------------------------------

	inline add_ostream_msg_t::add_ostream_msg_t(const filename_t& name) : name(name)
	{
	}

	inline add_ostream_msg_t::add_ostream_msg_t(std::istream& is)
	{
		std::getline(is, name);		
	}

	inline add_ostream_msg_t::~add_ostream_msg_t()
	{
	}

	inline int add_ostream_msg_t::get_type() const
	{
		return id;
	}

	inline filename_t add_ostream_msg_t::get_name() const
	{
		return name;
	}

	inline void add_ostream_msg_t::save(std::ostream& os)
	{
		os	<< id << '\n'
			<< name;
	}

	//------------------------------------------------------------------

	inline del_istream_msg_t::del_istream_msg_t(const filename_t& name) : name(name)
	{
	}

	inline del_istream_msg_t::del_istream_msg_t(std::istream& is)
	{
		std::getline(is, name);		
	}

	inline del_istream_msg_t::~del_istream_msg_t()
	{
	}

	inline int del_istream_msg_t::get_type() const
	{
		return id;
	}

	inline filename_t del_istream_msg_t::get_name() const
	{
		return name;
	}

	inline void del_istream_msg_t::save(std::ostream& os)
	{
		os	<< id << '\n'
			<< name;
	}

	//------------------------------------------------------------------

	inline del_ostream_msg_t::del_ostream_msg_t(const filename_t& name) : name(name)
	{
	}

	inline del_ostream_msg_t::del_ostream_msg_t(std::istream& is)
	{
		std::getline(is, name);		
	}

	inline del_ostream_msg_t::~del_ostream_msg_t()
	{
	}

	inline int del_ostream_msg_t::get_type() const
	{
		return id;
	}

	inline filename_t del_ostream_msg_t::get_name() const
	{
		return name;
	}

	inline void del_ostream_msg_t::save(std::ostream& os)
	{
		os	<< id << '\n'
			<< name;
	}

	//------------------------------------------------------------------

	inline pause_msg_t::pause_msg_t()
	{
	}

	inline pause_msg_t::pause_msg_t(std::istream&)
	{
	}

	inline pause_msg_t::~pause_msg_t()
	{
	}

	inline int pause_msg_t::get_type() const
	{
		return id;
	}

	inline void pause_msg_t::save(std::ostream& os)
	{
		os << id;
	}

	//------------------------------------------------------------------

	inline reset_msg_t::reset_msg_t()
	{
	}

	inline reset_msg_t::reset_msg_t(std::istream&)
	{
	}

	inline reset_msg_t::~reset_msg_t()
	{
	}

	inline int reset_msg_t::get_type() const
	{
		return id;
	}

	inline void reset_msg_t::save(std::ostream& os)
	{
		os << id;
	}

	//------------------------------------------------------------------

	msg_t* msg_t::create(const buffer_t& buf)
	{
		std::istringstream is({ buf.c_str(), buf.size() });
		return create(is);
	}

	msg_t* msg_t::create(std::istream& is)
	{
		string_t line;
		if (std::getline(is, line)) {
			int id = atoi(line.c_str());

			switch (id) {
			case add_istream_msg_t::id: return new add_istream_msg_t(is);
			case add_ostream_msg_t::id: return new add_ostream_msg_t(is);
			case del_istream_msg_t::id: return new del_istream_msg_t(is);
			case del_ostream_msg_t::id: return new del_ostream_msg_t(is);
			case pause_msg_t::id:       return new pause_msg_t(is);
			case reset_msg_t::id:       return new reset_msg_t(is);
			default:
				;	// drop thru
			}
		}

		return nullptr;
	}
}
