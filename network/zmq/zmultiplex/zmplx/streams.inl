#pragma once

namespace zmplx
{
	inline data_stream_t::data_stream_t() :
		close_(false)
	{
		handle_.tag_ = type_t::tag_t::unknown;
	}

	inline data_stream_t::data_stream_t(const filename_t& name, int fd, bool do_close) :
		close_(do_close),
		name_(name)
	{
		handle_.tag_ = type_t::tag_t::fd;
		handle_.value_.fd = fd;
	}

	inline data_stream_t::data_stream_t(const filename_t& name, zmq::socket_t* zsock, bool do_close) :
		close_(do_close),
		name_(name)
	{
		handle_.tag_ = type_t::tag_t::zsock;
		handle_.value_.zsock = zsock;
	}

	inline data_stream_t::data_stream_t(data_stream_t&& n) :
		handle_(n.handle_),
		close_ (n.close_),
		name_  (std::move(n.name_))
	{
		n.handle_.tag_ = type_t::tag_t::unknown;
		n.close_ = false;
	}

	inline data_stream_t& data_stream_t::operator=(data_stream_t&& n) {
		if (this != &n) {
			handle_ = n.handle_;
			close_  = n.close_;
			name_   = std::move(n.name_);

			n.handle_.tag_ = type_t::tag_t::unknown;
			n.close_ = false;
		}

		return *this;
	}

	inline stream_pair_t::stream_pair_t(data_streams_t&& in, data_streams_t&& out) :
		inherited(std::move(in), std::move(out))
	{
	}
}
