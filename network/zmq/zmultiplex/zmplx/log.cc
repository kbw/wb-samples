#include "log.hpp"
#include <unistd.h>
#include <memory>
#include <stdexcept>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

namespace
{
	int level_ = 0;
}

void set_verbosity(int level)
{
	level_ = level;
}

int verbose(int level, const char* fmt, ...)
{
	if (level <= level_ && isatty(STDOUT_FILENO)) {
		va_list params;
		va_start(params, fmt);

		char* buf = nullptr;
		int len = vasprintf(&buf, fmt, params);

		std::unique_ptr<char, decltype(free)*> buf_mgr { buf, free };
		va_end(params);

		if (!buf)
			throw std::bad_alloc();
		if (len < 0)
			throw std::runtime_error("vasprintf failed");

		int ret = write(STDOUT_FILENO, buf, len);
		ret += write(STDOUT_FILENO, "\n", 1);
		return ret;
	}

	return 0;
}
