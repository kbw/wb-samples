#pragma once

#include "defs.hpp"
#include <memory>

namespace zmq
{
	class context_t;
}

namespace zmplx
{
	struct options_t
	{
		static std::unique_ptr<zmq::context_t> ctx;

		iotype_t io;
		zopts_t opts;
		int verbose;
		filenames_t ifiles;
		filenames_t ofiles;

		options_t();
		void operator()(char* arg);
	};
}
