#pragma once

#include <new>
#include <stdlib.h>

namespace zmplx
{
	//-----------------------------------------------------------------------
	//
	template <typename T>
	inline T* allocator<T>::allocate(size_t n)
	{
		return reinterpret_cast<T*>(malloc(sizeof(T) * n));
	}

	template <typename T>
	inline T* allocator<T>::reallocate(T* p, size_t n)
	{
		return reinterpret_cast<T*>(realloc(p, sizeof(T) * n));
	}

	template <typename T>
	inline void allocator<T>::deallocate(T* p, size_t)
	{
		free(p);
	}

	// memory pool is the same, irrespective of type, for this allocator
	template <typename T, typename U>
	inline bool operator==(const allocator<T>&, const allocator<U>&)
	{
		return true;
	}

	template <typename T, typename U>
	inline bool operator!=(const allocator<T>&, const allocator<U>&)
	{
		return false;
	}
}
