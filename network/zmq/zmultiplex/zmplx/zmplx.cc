#include "zmplx.hpp"
#include "zmplx-msg.hpp"
#include "log.hpp"

#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>

#include <zmq.h>
#include <zmq.hpp>

#include <list>
#include <memory>

namespace
{
	using namespace zmplx;

	typedef	std::vector<zmq_pollitem_t> zmq_pollitems_t;
	typedef	std::list<buffer_t> buffers_t;

	//------------------------------------------------------------------

	string_t errtxt(int n = -1)
	{
		if (n < 0) {
			size_t bufsz = 128;
			buffer_t buf(bufsz);

			int len = snprintf(static_cast<char*>(buf.data()), buf.size(), "errno=%d:\"%s\"", errno, strerror(errno));
			errno = 0;
			return string_t(buf.c_str(), len);
		}

		errno = 0;
		return string_t();
	}

	//------------------------------------------------------------------

	zmq_pollitems_t init(stream_pair_t& streams)
	{
		zmq_pollitems_t items;

		for (data_streams_t::iterator p = streams.first.begin(); p != streams.first.end(); ++p) {
			switch (p->handle_.tag_) {
			case data_stream_t::type_t::tag_t::zsock: {
				zmq::socket_t* zsock = p->handle_.value_.zsock;
				items.push_back({ *zsock, -1, ZMQ_POLLIN, 0 });
				verbose(3, "%s: add zmq socket=%p", __FUNCTION__, static_cast<void*>(*zsock));
				break;
			}

			case data_stream_t::type_t::tag_t::fd:
				items.push_back({ nullptr, p->handle_.value_.fd, ZMQ_POLLIN, 0 });
				verbose(3, "%s: add fd=%d", __FUNCTION__, p->handle_.value_.fd);
				break;

			case data_stream_t::type_t::tag_t::unknown:
			default:
				std::runtime_error("bad input stream in: " + string_t(__FUNCTION__));
			}
		}

		return items;
	}

	bool read(buffers_t& buffers, zmq_pollitem_t& item)
	{
		// helper local function
		auto rd_size = [](int fd) -> size_t {
			const size_t maxsize = 4*1024*1024;
			const size_t minsize = 2;

			int size;
			if (ioctl(fd, FIONREAD, &size) != -1) {
				size_t bufsz = std::max(minsize, std::min(maxsize, (size_t)size + 1));
				verbose(1, "%s: rd_size=%zd buffer_size=%zu", __FUNCTION__, size, bufsz);
				return true;
			}

			return false;
		};

		// handle read
		verbose(2, ":%s: items.revents=0x%02x", __FUNCTION__, item.revents);
//		if (item.revents & ZMQ_POLLIN) {
			if (item.socket) {
				buffer_t buf(4*1024);

				verbose(2, ":%s: zmq_read", __FUNCTION__);
				int n = ::zmq_recv(item.socket, buf.data(), buf.size(), 0);
				verbose(1, ":%s: zmq_recv(%d bytes)=%s", __FUNCTION__, n, errtxt(n).c_str());
				if (n <= 0)
					return false;

				buf.resize(n);
				buffers.emplace_back(std::move(buf));
			}
			else {
				//buffer_t buf(rd_size(item.fd));
				buffer_t buf(4*1024);

				verbose(2, ":%s: read into bufsz=%zu", __FUNCTION__, buf.size());
				int n = ::read(item.fd, buf.data(), buf.size());
				verbose(1, "%s: read(%d bytes)=%s", __FUNCTION__, n, errtxt(n).c_str());
				if (n <= 0)
					return false;

				buf.resize(n);
				buffers.emplace_back(std::move(buf));
			}
//		}

		verbose(2, "%s: return true", __FUNCTION__);
		return true;
	}

	void write(data_streams_t::iterator p, const buffer_t& buf)
	{
		size_t nbytes = 0;
		int n = 0;

		switch (p->handle_.tag_) {
		case data_stream_t::type_t::tag_t::zsock:
			do {
				zmq::socket_t* zsock = p->handle_.value_.zsock;
				n = ::zmq_send(*zsock, buf.c_str() + n, buf.size() - n, 0);
				verbose(1, "%s: zmq_send(%zu bytes)=%d %s", __FUNCTION__, buf.size(), n, errtxt(n).c_str());
				if (n > 0)
					nbytes += n;
			}
			while (n > 0  &&  (nbytes-n < buf.size()));
			break;

		case data_stream_t::type_t::tag_t::fd:
			do {
				n = ::write(p->handle_.value_.fd, buf.c_str() + n, buf.size() - n);
				verbose(1, "%s: write(%zu bytes)=%d %s", __FUNCTION__, buf.size(), n, errtxt(n).c_str());
				if (n > 0)
					nbytes += n;
			}
			while (n > 0  &&  buf.size() != nbytes);
			break;

		case data_stream_t::type_t::tag_t::unknown:
		default:
			throw std::runtime_error("unknown write state");
		}
	}

	bool poll(stream_pair_t& streams, zmq_pollitems_t& items)
	{
		// helper local functions
		auto erase = [&streams, &items](size_t& i) {
			{
				data_streams_t::iterator p = streams.first.begin();
				std::advance(p, i);
				verbose(2, "%s: streams.first.erase(%d)", __FUNCTION__, i);
				streams.first.erase(p);
				verbose(2, "%s: streams.first.size=(%zu)", __FUNCTION__, streams.first.size());
			}
			{
				zmq_pollitems_t::iterator p = items.begin();
				std::advance(p, i);
				verbose(2, "%s: items.erase(%d)", __FUNCTION__, i);
				items.erase(p);
				verbose(2, "%s: items.size=(%zu)", __FUNCTION__, items.size());
			}

			--i;
		};

		// poll
		verbose(2, "%s: %zu input%s", __FUNCTION__, items.size(), items.size() == 1 ? "" : "s");
		for (size_t i = 0; i != items.size(); ++i)
			if (items[i].socket)
				verbose(2, "%s: i=%d zmq=%p events=0x%02x", __FUNCTION__, i, items[i].socket, items[i].events);
			else
				verbose(2, "%s: i=%d fd=%d events=0x%02x", __FUNCTION__, i, items[i].fd, items[i].events);
		int rc = zmq_poll(items.data(), items.size(), -1);
		//int rc = 1;
		verbose(2, "%s: zmq_poll=%d nitems=%zu", __FUNCTION__, rc, items.size());

		// process driggered inputs
		bool processed_stream = false;
		std::list<buffer_t> buffers;
		for (size_t i = 0; rc > 0 && i != streams.first.size(); ++i) {
			verbose(2, "%s: rc=%d i=%zu fd=%d revents=0x%02x", __FUNCTION__, rc, i, items[i].fd, items[i].revents);
			/*
			if (!items[i].revents) {
				verbose(2, "%s: skip(%d)", __FUNCTION__, i);
				continue;
			}
			 */
			if (!read(buffers, items[i])) {
				verbose(2, "%s: erase(%d)", __FUNCTION__, i);
				erase(i);
				processed_stream = true;
				continue;
			}
			if (items[i].revents & ZMQ_POLLERR) {
				verbose(0, "%s: error: items[%zu].{fd=%d revents=0x%02x} %s", __FUNCTION__, i, items[i].fd, items[i].revents, errtxt().c_str());
				erase(i);
				processed_stream = true;
				continue;
			}

			processed_stream = true;
			--rc;
		}

		// write output
		verbose(2, "%s: write buffers.size=%zu streams.size=%zu", __FUNCTION__, buffers.size(), streams.second.size());
		for (data_streams_t::iterator p = streams.second.begin(); !buffers.empty() && p != streams.second.end(); ++p) {
			for (const buffer_t& buf : buffers) {
				verbose(2, "%s: buf.size=%uz", __FUNCTION__, buf.size());
				write(p, buf);
			}
		}

		verbose(2, "%s: return=%s", __FUNCTION__, processed_stream ? "true" : "false");
		return processed_stream;
//
//		verbose(2, "%s: return=%s", __FUNCTION__, (!buffers.empty() && !items.empty()) ? "true" : "false");
//		return !buffers.empty() && !items.empty();
	}

	//------------------------------------------------------------------

	string_t to_string(msg_t* msg)
	{
		std::ostringstream os;
		msg->save(os);
		return string_t(os.str().c_str(), os.str().size());
	}
}

namespace zmplx
{
	session_t::session_t(const options_t& opts) :
		opts_(opts),
		streams_(create_streams(opts)),
		pair_name_(make_pair_name()),
		channel_(new zmq::socket_t(*options_t::ctx, zmq::socket_type::pair)),
		paused_(false),
		reset_(false)
	{
		verbose(2, "%s: binding to internal channel", __FUNCTION__);
		channel_->bind(get_pair_name());

		verbose(2, "%s: starting worker", __FUNCTION__);
		thread_.reset(new std::thread(worker, std::ref(*this)));
	}

	session_t::~session_t()
	{
		thread_->join();
		verbose(2, "%s: session_t terminating", __FUNCTION__);
	}

	string_t session_t::make_pair_name()
	{
		static int id = 0;

		char buf[16];
		int len = snprintf(buf, sizeof(buf), "inproc://#%d", ++id);
		return string_t(buf, len);
	}

	//------------------------------------------------------------------
	//	operation

	bool session_t::move(session_t& dst, session_t& src, const filename_t& name, iotype_t io)
	{
		switch (io) {
		case iotype_t::input:
			for (auto p = src.get_streams().first.begin(); p != src.get_streams().first.end(); ++p) {
				if (p->name_ == name) {
					dst.pause();
					src.pause();

					dst.get_streams().first.emplace_back(std::move(*p));
					src.get_streams().first.erase(p);

					dst.reset();
					src.reset();
					return true;
				}
			}
			break;

		case iotype_t::output:
			for (auto p = src.get_streams().second.begin(); p != src.get_streams().second.end(); ++p) {
				if (p->name_ == name) {
					dst.pause();
					src.pause();

					dst.get_streams().second.emplace_back(std::move(*p));
					src.get_streams().second.erase(p);

					dst.reset();
					src.reset();
					return true;
				}
			}
			break;

		case iotype_t::unknown:
		default:
			throw std::runtime_error("No direction to move stream name=" + name);
		}

		return false;
	}

	//------------------------------------------------------------------
	//	messaging helpers -- runs in handle

	void session_t::add_input(const filename_t& name)
	{
		std::unique_ptr<msg_t> msg(new add_istream_msg_t(name));
		string_t str = to_string(msg.get());
		zmq_send(static_cast<void*>(*channel_), str.c_str(), str.size(), 0);
	}

	void session_t::add_output(const filename_t& name)
	{
		std::unique_ptr<msg_t> msg(new add_ostream_msg_t(name));
		string_t str = to_string(msg.get());
		zmq_send(static_cast<void*>(*channel_), str.c_str(), str.size(), 0);
	}

	void session_t::del_input(const filename_t& name)
	{
		std::unique_ptr<msg_t> msg(new del_istream_msg_t(name));
		string_t str = to_string(msg.get());
		zmq_send(static_cast<void*>(*channel_), str.c_str(), str.size(), 0);
	}

	void session_t::del_output(const filename_t& name)
	{
		std::unique_ptr<msg_t> msg(new del_ostream_msg_t(name));
		string_t str = to_string(msg.get());
		zmq_send(static_cast<void*>(*channel_), str.c_str(), str.size(), 0);
	}

	void session_t::pause()
	{
		std::unique_ptr<msg_t> msg(new pause_msg_t);
		string_t str = to_string(msg.get());
		zmq_send(static_cast<void*>(*channel_), str.c_str(), str.size(), 0);
	}

	void session_t::reset()
	{
		std::unique_ptr<msg_t> msg(new reset_msg_t);
		string_t str = to_string(msg.get());
		zmq_send(static_cast<void*>(*channel_), str.c_str(), str.size(), 0);
	}

	void session_t::do_pause()
	{
		paused_ = true;
	}

	void session_t::do_reset()
	{
		reset_ = true;
	}

	bool session_t::is_paused() const
	{
		return paused_;
	}

	bool session_t::is_reset() const
	{
		return reset_;
	}

	void session_t::clear_reset()
	{
		paused_ = reset_ = false;
	}

	//------------------------------------------------------------------
	//	thread func helper -- runs in worker

	void session_t::handle_thread_msg(session_t& obj, zmq::socket_t& channel)
	{
		zmq_pollitem_t item = { static_cast<void*>(channel), 0, ZMQ_POLLIN, 0 };
		int rc = zmq_poll(&item, 1, 0);	// zero timeout
		if (rc != 1)
	   		return;

		buffer_t buf(1024);
		int n = zmq_recv(item.socket, buf.data(), buf.size(), 0); /// zero timeout
		if (n == -1)
			return; /// we timedout

		buf.resize(n);
		if (msg_t* msg = msg_t::create(buf)) {
			switch (msg->get_type()) {
			case add_istream_msg_t::id: {
				add_istream_msg_t* imsg = static_cast<add_istream_msg_t*>(msg);
				data_stream_t stream = create_stream(*obj.get_opts().ctx, imsg->get_name(), ::zopts_t(), iotype_t::input);
				obj.get_streams().first.emplace_back(std::move(stream));
				break;
			}

			case add_ostream_msg_t::id: {
				add_ostream_msg_t* omsg = static_cast<add_ostream_msg_t*>(msg);
				data_stream_t stream = create_stream(*obj.get_opts().ctx, omsg->get_name(), ::zopts_t(), iotype_t::output);
				obj.get_streams().second.emplace_back(std::move(stream));
				break;
			}

			case del_istream_msg_t::id: {
				del_istream_msg_t* imsg = static_cast<del_istream_msg_t*>(msg);
				for (auto p = obj.get_streams().first.begin(); p != obj.get_streams().first.end(); ++p)
					if (p->name_ == imsg->get_name())
						obj.get_streams().first.erase(p);
				break;
			}

			case del_ostream_msg_t::id: {
				del_ostream_msg_t* omsg = static_cast<del_ostream_msg_t*>(msg);
				for (auto p = obj.get_streams().second.begin(); p != obj.get_streams().second.end(); ++p)
					if (p->name_ == omsg->get_name())
						obj.get_streams().second.erase(p);
				break;
			}

			case pause_msg_t::id:
				obj.do_pause();
				break;

			case reset_msg_t::id:
				obj.do_reset();
				break;

			default:
				;
			}
		}
	}

	//------------------------------------------------------------------
	//	thread func

	void session_t::worker(session_t& obj)
	{
		const int def_timeout = 1; /// 1usec
		const int max_timeout = 1*1000*1000; /// 1 sec

		set_verbosity(obj.get_opts().verbose);
		zmq::socket_t channel(*obj.get_opts().ctx, zmq::socket_type::pair);
		channel.connect(obj.get_pair_name());

		std::vector<zmq_pollitem_t> items = init(obj.get_streams());

		for (int timeout = def_timeout; poll(obj.get_streams(), items); handle_thread_msg(obj, channel)) {
			if (obj.is_paused()) {
				usleep(std::min(timeout, max_timeout));
				timeout *= 10;
				continue;
			}

			if (obj.is_reset()) {
				items = init(obj.get_streams());
				obj.clear_reset();
			}

			timeout = def_timeout;
		}

		verbose(2, "%s: worker terminating", __FUNCTION__);
	}
}
