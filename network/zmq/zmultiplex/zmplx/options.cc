#include "options.hpp"
#include <zmq.hpp>

#include <stdexcept>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

namespace
{
	void usage(FILE* stream = stdout)
	{
		const char* text[] = {
			"zmplx: a ZeroMQ multiplexor",
			"zmplx [options]",
			" -h, --help",
			"    Show this message",
			" -i, --input",
			"    specify an input stream",
			" -o, --output",
			"    specify an output stream",
			" -v, --verbose",
			"    increase trace information",
			" --affinity=<n>",
			"    cpu affinity",
			" --hwm=<n>",
			"    ZermoMQ high water mark",
			" --subscrib=<msg>",
			"    set ZeroMQ subscriber filter",
			nullptr
		};

		for (size_t i = 0; text[i]; ++i)
			fprintf(stream, "%s\n", text[i]);
		fflush(stream);
	}
}

namespace zmplx
{
	std::unique_ptr<zmq::context_t> options_t::ctx(new zmq::context_t(1));

	options_t::options_t() :
		io(iotype_t::unknown),
		verbose(0)
	{
		errno = 0;
	}

	void options_t::operator()(char* arg)
	{
		if (*arg == '-' && arg[1]) {
			if (!strcmp(arg, "-h") || !strcmp(arg, "--help")) {
				usage();
				exit(0);
			}
			if (!strcmp(arg, "-i") || !strcmp(arg, "--input")) {
				io = iotype_t::input;
			}
			else if (!strcmp(arg, "-o") || !strcmp(arg, "--output")) {
				io = iotype_t::output;
			}
			else if (!strncmp(arg, "-v=", 3)) {
				verbose = atoi(arg += 3);
			}
			else if (!strncmp(arg, "--verbose=", 10)) {
				verbose = atoi(arg += 10);
			}
			else if (!strcmp(arg, "-v") || !strcmp(arg, "--verbose")) {
				++verbose;
			}
			else if (!strncmp(arg, "--affinity=", 11)) {
				option_t opt = option_t::affinity;
				string_t val = arg + 11;
				opts.emplace_back(opt, std::move(val));
			}
			else if (!strncmp(arg, "--hwm=", 6)) {
				option_t opt = option_t::hwm;
				string_t val = arg + 6;
				opts.emplace_back(opt, std::move(val));
			}
			else if (!strncmp(arg, "--subscribe=", 12)) {
				option_t opt = option_t::subscribe;
				string_t val = arg + 12;
				opts.emplace_back(opt, std::move(val));
			}
			else
				throw std::runtime_error("unrecognized option: " + string_t(arg));
		}
		else {
			switch (io) {
			case iotype_t::input:  ifiles.emplace_back(filename_t(arg), std::move(opts)); break;
			case iotype_t::output: ofiles.emplace_back(filename_t(arg), std::move(opts)); break;
			case iotype_t::unknown:
			default:
				throw std::runtime_error("don't know if \"" + string_t(arg) + "\" is for input or output");
			}
		}
	}
}
