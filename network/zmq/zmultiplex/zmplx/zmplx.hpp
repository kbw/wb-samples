#pragma once

#include "defs.hpp"
#include "options.hpp"
#include "streams.hpp"

#include <thread>
#include <memory>

namespace zmq
{
	class socket_t;
}

namespace zmplx
{
	class session_t
	{
	public:
		session_t(const options_t& opts);
		~session_t();

		session_t(session_t&& n) = default;
		session_t& operator=(session_t&& n) = default;

		session_t(const session_t&& n) = delete;
		session_t& operator=(const session_t&& n) = delete;

		/// Accessors
		const options_t& get_opts() const       { return opts_; }
		const string_t&  get_pair_name() const  { return pair_name_; }
		stream_pair_t&   get_streams()          { return streams_; }

		/// IPC methods
		void add_input(const filename_t& spec);
		void add_output(const filename_t& spec);
		void del_input(const filename_t& spec);
		void del_output(const filename_t& spec);

		void pause();
		void reset();
		bool is_paused() const;
		bool is_reset() const;
		void clear_reset();

		/// Operations
		static bool move(
						session_t& dst, session_t& src, const filename_t& name,
						iotype_t io = iotype_t::input);

	private:
		void do_pause();
		void do_reset();

		static string_t make_pair_name();
		static void worker(session_t& obj); /// thread func
		static void handle_thread_msg(session_t& obj, zmq::socket_t& channel); /// thread func helper

	private:
		options_t opts_;
		stream_pair_t streams_;

		/// IPC mechanism
		const string_t pair_name_;
		std::unique_ptr<zmq::socket_t> channel_;
		std::unique_ptr<std::thread> thread_;

		/// Event machine control
		bool paused_;
		bool reset_;
	};
}
