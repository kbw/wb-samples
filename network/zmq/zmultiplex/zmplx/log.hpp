#pragma once

void set_verbosity(int level);
int  verbose(int level, const char* fmt, ...);
