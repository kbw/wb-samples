#pragma once

#include "buffer.hpp"
#include <string>
#include <vector>
#include <utility>

namespace zmplx
{
	enum class iotype_t { unknown, input, output };
	enum class option_t { unknown, affinity, hwm, subscribe };

	typedef buffer<allocator<unsigned char>> buffer_t;

	class string_t : public std::string
	{
		typedef std::string inherited;

		using inherited::inherited;
	};
	typedef std::vector<string_t> strings_t;

	class zopts_t : public std::vector<std::pair<option_t, string_t>>
	{
	public:
		typedef std::vector<std::pair<option_t, string_t>> inherited;

		using inherited::inherited;
	};

	class filename_t : public string_t
	{
	public:
		typedef string_t inherited;

		filename_t() {}
		filename_t(const string_t& n) : string_t(n) {}
		filename_t(const strings_t& strs) {
			for (const string_t& str : strs) {
				if (!empty())
					append(1, '/');
				append(str);
			}
		}
	};

	class filenames_t : public std::vector<std::pair<filename_t, zopts_t>>
	{
	public:
		typedef std::vector<std::pair<filename_t, zopts_t>> inherited;

		using inherited::inherited;
	};
}
