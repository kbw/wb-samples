#pragma once

#include <new>

namespace zmplx
{
	//-----------------------------------------------------------------------
	//
	template <typename Allocator>
	inline buffer<Allocator>::buffer(size_t len, const Allocator& alloc) :
		alloc_(alloc),
		len_(len),
		data_(len ? reinterpret_cast<void*>(alloc_.allocate(len + 1)) : nullptr)
	{
		if (len_ && !data_)
			throw std::bad_alloc();
	}

	template <typename Allocator>
	inline buffer<Allocator>::buffer(buffer&& n):
		alloc_(n.alloc_),
		len_(n.len_),
		data_(n.data_)
	{
		n.len_ = 0;
		n.data_ = nullptr;
	}

	template <typename Allocator>
	inline buffer<Allocator>::~buffer()
	{
		alloc_.deallocate(reinterpret_cast<typename Allocator::pointer>(data_), len_);
	}

	template <typename Allocator>
	inline buffer<Allocator>& buffer<Allocator>::operator=(buffer&& n)
	{
		if (this != &n) {
			alloc_.deallocate(data_);

			alloc_ = n.alloc_;
			len_ = n.len_;
			data_ = n.data_;

			n.len_ = 0;
			n.data_ = nullptr;
		}

		return *this;
	}

	template <typename Allocator>
	inline bool buffer<Allocator>::resize(size_t n)
	{
		if (n == len_)
			return true;

		if (n == 0) {
			alloc_.deallocate(reinterpret_cast<typename Allocator::pointer>(data_), len_);
			data_ = nullptr, len_ = 0;
			return true;
		}

		if (void* p = alloc_.reallocate(reinterpret_cast<typename Allocator::pointer>(data_), n + 1)) {
			len_ = n;
			data_ = p;
			return true;
		}

		return false;
	}

	template <typename Allocator>
	inline void* buffer<Allocator>::data()
	{
		return data_;
	}

	template <typename Allocator>
	inline const void* buffer<Allocator>::data() const
	{
		return data_;
	}

	template <typename Allocator>
	inline const char* buffer<Allocator>::c_str() const
	{
		place_null();
		return static_cast<const char*>(data_);
	}

	template <typename Allocator>
	inline size_t buffer<Allocator>::size() const
	{
		return len_;
	}

	template <typename Allocator>
	inline void buffer<Allocator>::place_null() const
	{
		if (data_ && len_) {
			unsigned char* p = static_cast<unsigned char*>(data_);
			if (p[len_])
				p[len_] = 0;	// buffer size is len_ + 1
		}
	}
}
