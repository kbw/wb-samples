#pragma once

#include "memory.hpp"
#include <stddef.h>

namespace zmplx
{
	// buffer class uses specified allocator
	// the allocator must provide reallocate()
	template <typename Allocator = allocator<unsigned char>>
	class buffer
	{
	public:
		buffer(size_t len = 0, const Allocator& alloc = Allocator());
		buffer(buffer&& n);
		~buffer();

		buffer& operator=(buffer&& n);

		bool resize(size_t n);

		void* data();
		const void* data() const;
		const char* c_str() const;

		size_t size() const;

	private:
		// can't copy buffer content
		buffer(const buffer& n) = delete;
		buffer& operator=(const buffer& n) = delete;

		void place_null() const;
	
	private:
		Allocator alloc_;
		size_t len_;
		mutable void* data_;
	};
}

#include "buffer.inl"
