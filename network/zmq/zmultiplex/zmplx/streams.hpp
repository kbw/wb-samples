#pragma once

#include "defs.hpp"
#include "options.hpp"
#include <utility>
#include <list>

namespace zmq
{
	class context_t;
	class socket_t;
}

namespace zmplx
{
	struct options_t;

	//------------------------------------------------------------------
	//	We must disable copies on all of these data structures

	struct data_stream_t
	{
		struct type_t	// can be a file descriptor or a socket pointer
		{
			enum class tag_t { unknown, fd, zsock };

			union value_t
			{
				int fd;
				zmq::socket_t* zsock;
			};

			tag_t tag_;
			value_t value_;
		};

		type_t handle_;
		bool close_;
		filename_t name_;

		data_stream_t();
		~data_stream_t();

		data_stream_t(const filename_t& name, int fd, bool do_close = true);
		data_stream_t(const filename_t& name, zmq::socket_t* zsock, bool do_close = true);

		data_stream_t(data_stream_t&& n);
		data_stream_t& operator=(data_stream_t&& n);

		data_stream_t(const data_stream_t& n) = delete;
		data_stream_t& operator=(const data_stream_t& n) = delete;
	};

	class data_streams_t : public std::list<data_stream_t>
	{
	public:
		typedef std::list<data_stream_t> inherited;

		data_streams_t() = default;
		~data_streams_t() = default;

		data_streams_t(data_streams_t&& n) = default;
		data_streams_t& operator=(data_streams_t&& n) = default;

		data_streams_t(const data_streams_t& n) = delete;
		data_streams_t& operator=(const data_streams_t& n) = delete;
	};

	class stream_pair_t : public std::pair<data_streams_t, data_streams_t>
	{
	public:
		typedef std::pair<data_streams_t, data_streams_t> inherited;

		stream_pair_t(data_streams_t&& in, data_streams_t&& out);
		~stream_pair_t() = default;

		stream_pair_t(stream_pair_t&& n) = default;
		stream_pair_t& operator=(stream_pair_t&& n) = default;

		stream_pair_t(const stream_pair_t& n) = delete;
		stream_pair_t& operator=(const stream_pair_t& n) = delete;
	};

	//------------------------------------------------------------------

	data_stream_t  create_stream(zmq::context_t& ctx, const filename_t& name, const zopts_t& opts, iotype_t io);
	data_streams_t create_streams(zmq::context_t& ctx, const filenames_t& spec, iotype_t io);
	stream_pair_t  create_streams(const options_t& opts);
}

#include "streams.inl"
