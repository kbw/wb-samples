#pragma once

#include <memory>
#include <stddef.h>

namespace zmplx
{
	// our allocator has a reallocate() method
	template <typename T>
	class allocator
	{
	public:
		typedef T	value_type;
		typedef T*	pointer;
		typedef T&	reference;

		typedef const T* const_pointer;
		typedef const T& const_reference;

		typedef size_t		size_type;
		typedef ptrdiff_t	difference_type;

		T*   allocate(size_t n);
		T*   reallocate(T* p, size_t n);
		void deallocate(T* p, size_t n);
	};

	template <typename T, typename U>
	inline bool operator==(const allocator<T>&, const allocator<U>&);
	template <typename T, typename U>
	inline bool operator!=(const allocator<T>&, const allocator<U>&);
}

#include "memory.inl"
