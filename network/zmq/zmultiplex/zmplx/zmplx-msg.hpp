#pragma once

#include "defs.hpp"
#include "buffer.hpp"
#include <iosfwd>

namespace
{
	using namespace zmplx;

	//------------------------------------------------------------------

	class msg_t
	{
	public:
		enum { id = -1 };

		virtual ~msg_t() = 0;
		virtual int get_type() const = 0;
		virtual void save(std::ostream& os) = 0;

		static msg_t* create(const buffer_t& buf);
		static msg_t* create(std::istream& buf);
	};

	class add_istream_msg_t : public msg_t
	{
	public:
		enum { id = 1 };

		add_istream_msg_t(const filename_t& name);
		add_istream_msg_t(std::istream& is);
		virtual ~add_istream_msg_t();

		virtual int get_type() const;
		virtual filename_t get_name() const;
		virtual void save(std::ostream& os);

	private:
		filename_t name;
	};

	class add_ostream_msg_t : public msg_t
	{
	public:
		enum { id = 2 };

		add_ostream_msg_t(const filename_t& name);
		add_ostream_msg_t(std::istream& is);
		virtual ~add_ostream_msg_t();

		virtual int get_type() const;
		virtual filename_t get_name() const;
		virtual void save(std::ostream& os);

	private:
		filename_t name;
	};

	class del_istream_msg_t : public msg_t
	{
	public:
		enum { id = 3 };

		del_istream_msg_t(const filename_t& name);
		del_istream_msg_t(std::istream& is);
		virtual ~del_istream_msg_t();

		virtual int get_type() const;
		virtual filename_t get_name() const;
		virtual void save(std::ostream& os);

	private:
		filename_t name;
	};

	class del_ostream_msg_t : public msg_t
	{
	public:
		enum { id = 4 };

		del_ostream_msg_t(const filename_t& name);
		del_ostream_msg_t(std::istream& is);
		virtual ~del_ostream_msg_t();

		virtual int get_type() const;
		virtual filename_t get_name() const;
		virtual void save(std::ostream& os);

	private:
		filename_t name;
	};

	class pause_msg_t : public msg_t
	{
	public:
		enum { id = 5 };

		pause_msg_t();
		pause_msg_t(std::istream&);
		virtual ~pause_msg_t();

		virtual int get_type() const;
		virtual void save(std::ostream& os);
	};

	class reset_msg_t : public msg_t
	{
	public:
		enum { id = 6 };

		reset_msg_t();
		reset_msg_t(std::istream&);
		virtual ~reset_msg_t();

		virtual int get_type() const;
		virtual void save(std::ostream& os);
	};
}

#include "zmplx-msg.inl"
