#include <zmplx/options.hpp>
#include <zmplx/streams.hpp>
#include <zmplx/zmplx.hpp>
#include <algorithm>
#include <exception>
#include <iostream>

int main(int argc, char* argv[])
try {
	zmplx::options_t opts = std::for_each(argv + 1, argv + argc, zmplx::options_t());
	zmplx::session_t session(opts);
}
catch (const std::exception& e) {
	std::clog << "fatal: " << e.what() << "\n";
}
