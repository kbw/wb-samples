#include <unistd.h>
#include <sys/stat.h>
#include <zmq.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
	if (argc != 3)
		return 1;

	int mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	struct zmq_pollitem_t items[] = {
		{ nullptr, open(argv[1], O_RDONLY), ZMQ_POLLIN, 0 },
		{ nullptr, creat(argv[2], mode), ZMQ_POLLOUT, 0 }
	};
	int nitems = 1;

	char buf[4*1024*1024];
	int n = 0;
	do {
		int rc = zmq_poll(items, nitems, 10*1000);
		fprintf(stdout, "poll:%d i:0 fd:%d revents:0x%02x\n", rc, items[0].fd, items[0].revents);

		for (int i = 0; i < nitems; ++i) {
			if (items[i].revents & ZMQ_POLLIN) {
				n = read(items[i].fd, buf, sizeof(buf));
				fprintf(stdout, "read %d bytes\n", n);
			}
			if (n > 0) {
				n = write(items[1].fd, buf, n);
				fprintf(stdout, "wrote %d bytes\n", n);
			}
		}
	}
	while (n > 0);

	for (int i = 0; i < nitems; close(items[i++].fd))
		;
}
