#pragma once
#include <zmq.hpp>
#include <string>
#include <memory>

class sync_pipeline_impl_t;

class sync_pipeline_t
{
public:
	sync_pipeline_t(
		const std::string& name,
		zmq::context_t& ctx,
		const std::string& feed_sub_addr,
		const std::string& pipein_bind_addr, const std::string& pipein_conn_addr,
		const std::string& pipeout_bind_addr, const std::string& pipeout_conn_addr,
		size_t nworkers, struct timeval timeout);
	~sync_pipeline_t();

	void run();

private:
	sync_pipeline_t(const sync_pipeline_t& n) = delete;
	sync_pipeline_t& operator=(const sync_pipeline_t& n) = delete;

	std::unique_ptr<sync_pipeline_impl_t> impl;
};
