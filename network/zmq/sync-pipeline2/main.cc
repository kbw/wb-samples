#include "sync_pipeline.hpp"
#include "datafeed.hpp"

#include <exception>	// error handling
#include <iostream>

const std::string feed_bind		= "inproc://feed";
const std::string feed_conn		= "inproc://feed";
const std::string pipein_bind	= "inproc://pipein";
const std::string pipein_conn	= "inproc://pipein";
const std::string pipeout_bind	= "inproc://pipeout";
const std::string pipeout_conn	= "inproc://pipeout";

const timeval zmq_timeout = { 0, 500*1000 };
const size_t  zmq_workers = 8;

//---------------------------------------------------------------------------

int main()
try
{
	zmq::context_t	ctx(1);
	std::unique_ptr<std::thread> feed(datafeed(ctx, feed_bind));	// datafeed
	sync_pipeline_t	pipeline(
						"sample",
						ctx, feed_conn,
						pipein_bind, pipein_conn,
						pipeout_bind, pipeout_conn,
						zmq_workers, zmq_timeout);

	pipeline.run();
	feed->join();
}
catch (const std::exception& e)
{
	std::clog << "error: " << e.what() << std::endl;
}
catch (...)
{
	std::clog << "error: " << "unknown" << std::endl;
}
