#include "sync_pipeline.hpp"
#include "zmq_helpers.hpp"
#include "time_helpers.hpp"

#include <zmq.hpp>

#include <string>		// helpers
#include <thread>
#include <memory>
#include <list>
#include <vector>
#include <iostream>

#include <string.h>

//---------------------------------------------------------------------------

class sync_pipeline_impl_t
{
public:
	typedef std::list<std::pair<zmq::socket_t, zmq::socket_t>> sockets_t;

	sync_pipeline_impl_t(
		const std::string& name,			
		zmq::context_t& ctx,
		const std::string& feed_sub_addr,
		const std::string& pipein_bind_addr, const std::string& pipein_conn_addr,
		const std::string& pipeout_bind_addr, const std::string& pipeout_conn_addr,
		size_t nworkers, struct timeval timeout);

	void run();

private:
	zmq::socket_t create_sub(zmq::context_t& ctx, const std::string& addr);
	zmq::socket_t create_push(zmq::context_t& ctx, const std::string& addr);
	zmq::socket_t create_pull(zmq::context_t& ctx, const std::string& addr);
	sockets_t     create_workers(
					zmq::context_t& ctx,
					const std::string& addr_in, const std::string& addr_out,
					size_t n);
	void          process(
					zmq::context_t& ctx,
					zmq::socket_t& in, zmq::socket_t& push, zmq::socket_t& pull,
					sockets_t& workers);

private:
	const std::string name;
	zmq::context_t& ctx;
	zmq::socket_t in, push, pull;
	sockets_t workers;
	timeval zmq_timeout;

};

//---------------------------------------------------------------------------

sync_pipeline_t::sync_pipeline_t(
		const std::string& name,
		zmq::context_t& ctx,
		const std::string& feed_sub_addr,
		const std::string& pipein_bind_addr, const std::string& pipein_conn_addr,
		const std::string& pipeout_bind_addr, const std::string& pipeout_conn_addr,
		size_t nworkers,
		timeval timeout) :
	impl(
		std::unique_ptr<sync_pipeline_impl_t>(
			new sync_pipeline_impl_t(
					name,
					ctx,
					feed_sub_addr,
					pipein_bind_addr, pipein_conn_addr,
					pipeout_bind_addr, pipeout_conn_addr,
					nworkers,
					timeout)))
{
}

sync_pipeline_t::~sync_pipeline_t()
{
}

void sync_pipeline_t::run()
{
	impl->run();
}

//---------------------------------------------------------------------------

sync_pipeline_impl_t::sync_pipeline_impl_t(
		const std::string& name,
		zmq::context_t& ctx,
		const std::string& feed_sub_addr,
		const std::string& pipein_bind_addr, const std::string& pipein_conn_addr,
		const std::string& pipeout_bind_addr, const std::string& pipeout_conn_addr,
		size_t nworkers, struct timeval timeout) :
	name(name),
	ctx(ctx),
	in(create_sub(ctx, feed_sub_addr)),
	push(create_push(ctx, pipein_bind_addr)),
	pull(create_pull(ctx, pipeout_bind_addr)),
	workers(create_workers(ctx, pipein_conn_addr, pipeout_conn_addr, nworkers)),
	zmq_timeout(timeout)
{
}

void sync_pipeline_impl_t::run()
{
	process(ctx, in, push, pull, workers);
}

//---------------------------------------------------------------------------

zmq::socket_t sync_pipeline_impl_t::create_sub(zmq::context_t& ctx, const std::string& addr)
{
	zmq::socket_t s(ctx, zmq::socket_type::sub);
	s.connect(addr.c_str());

	int val = to_msec(zmq_timeout);
	s.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(val));

	s.setsockopt(ZMQ_SUBSCRIBE, "msg", 0);
	std::clog << "create_sub: " << addr << std::endl;
	return s;
}

zmq::socket_t sync_pipeline_impl_t::create_push(zmq::context_t& ctx, const std::string& addr)
{
	zmq::socket_t s(ctx, zmq::socket_type::pub);
	s.bind(addr.c_str());

	int val = to_msec(zmq_timeout);
	s.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(val));
	std::clog << "create_push: " << addr << std::endl;
	return s;
}

zmq::socket_t sync_pipeline_impl_t::create_pull(zmq::context_t& ctx, const std::string& addr)
{
	zmq::socket_t s(ctx, zmq::socket_type::pull);
	s.bind(addr.c_str());

	int val = to_msec(zmq_timeout);
	s.setsockopt(ZMQ_RCVTIMEO, &val, sizeof(val));
	std::clog << "create_pull: " << addr << std::endl;
	return s;
}

sync_pipeline_impl_t::sockets_t sync_pipeline_impl_t::create_workers(zmq::context_t& ctx, const std::string& addr_in, const std::string& addr_out, size_t n)
{
	sockets_t s;
	for (size_t i = 0; i != n; ++i)
	{
		s.emplace_back(
			std::make_pair(
				zmq::socket_t(ctx, zmq::socket_type::sub),
				zmq::socket_t(ctx, zmq::socket_type::push)));
		std::clog << "create_worker: " << addr_in << ":" << addr_out << std::endl;

		s.front().first.connect(addr_in.c_str());
		s.front().second.connect(addr_out.c_str());

		s.front().first.setsockopt(ZMQ_SUBSCRIBE, "", 0);

		int val = to_msec(zmq_timeout);
		s.front().first.setsockopt(ZMQ_RCVTIMEO, &val, sizeof(val));
		val = to_msec(zmq_timeout);
		s.front().second.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(val));
	}

	return s;
}

void sync_pipeline_impl_t::process(zmq::context_t& ctx, zmq::socket_t& sub, zmq::socket_t& push, zmq::socket_t& pull, sockets_t& workers)
{
	auto f_push = [this](zmq::socket_t& sub, zmq::socket_t& push, zmq::socket_t& sync, size_t nworkers)
	{
		// copy from sub to push
		std::string str;;
		while (true)
		{
			if (!recv(sub, &str, to_msec(zmq_timeout)))
			{
				std::clog << "push: done" << std::endl;
				break;
			}

			for (size_t i = 0; i != nworkers; ++i)
			{
				send(push, str, to_msec(zmq_timeout));
				std::clog << "push:\"" << str << "\"" << std::endl;
			}

			std::clog << "push: listening" << std::endl;
			if (recv(sync, &str, to_msec(zmq_timeout)))
				std::clog << "push: recieve sync" << std::endl;
			else
				std::clog << "push: missed sync" << std::endl;
			std::clog << std::endl;
		}
	};

	auto f_worker = [this](zmq::socket_t& pull, zmq::socket_t& push)
	{
		// copy from push to workers
		auto work = [](std::string s)
		{
			std::cout << "work:\"" + s << "\"" << std::endl;
			return s;
		};

		while (true)
		{
			std::string str;
			if (!recv(pull, &str, to_msec(zmq_timeout)))
			{
				std::clog << "work: timed out" << std::endl;
				break;
			}

			if (str.size() >= 6 && strncmp(str.c_str(), "prime:", 6) == 0)	// skip publish primers
				continue;
			str = work(str);

			send(push, str, to_msec(zmq_timeout));
			std::clog << "work: done" << std::endl;
		}
	};

	auto f_pull = [this](zmq::socket_t& pull, zmq::socket_t& sync, size_t nworkers)
	{
		// copy from workers to pull
		std::string str;
		bool timed_out = false;
		while (!timed_out)
		{
			for (size_t i = 0; i != nworkers; ++i)
			{
				if (!recv(pull, &str, to_msec(zmq_timeout)))
				{
					std::clog << "pull: timed out" << std::endl;
					timed_out = true;
					break;
				}

				std::clog << "pull:\"" << str << "\"" << std::endl;
			}

			if (send(sync, "done", to_msec(zmq_timeout)))
				std::clog << "pull: send sync" << std::endl;
			else
				std::clog << "pull: sync failed" << std::endl;
		}
	};

	std::pair<zmq::socket_t, zmq::socket_t> sync = {
		zmq::socket_t(ctx, zmq::socket_type::pair),
		zmq::socket_t(ctx, zmq::socket_type::pair)
	};

	sync.first.bind("inproc://sync-pipeline" + name);
	sync.second.connect("inproc://sync-pipeline" + name);
	
	int val = to_msec(zmq_timeout);
	sync.first.setsockopt(ZMQ_RCVTIMEO, &val, sizeof(int));
	sync.first.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(int));
	sync.second.setsockopt(ZMQ_RCVTIMEO, &val, sizeof(int));
	sync.second.setsockopt(ZMQ_SNDTIMEO, &val, sizeof(int));

	typedef std::vector<std::unique_ptr<std::thread>> threads_t;

	const size_t nworkers = workers.size();

	threads_t threads;
	threads.push_back(
		threads_t::value_type(
			new std::thread(f_push, std::ref(sub), std::ref(push), std::ref(sync.first), nworkers)));
	threads.push_back(
		threads_t::value_type(
			new std::thread(f_pull, std::ref(pull), std::ref(sync.second), nworkers)));
	for (sockets_t::iterator p = workers.begin(); p != workers.end(); ++p)
		threads.push_back(
			threads_t::value_type(
				new std::thread(f_worker, std::ref(p->first), std::ref(p->second))));

	for (threads_t::iterator p = threads.begin(); p != threads.end(); ++p)
		(*p)->join();
}
