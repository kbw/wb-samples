#include "SimplifiedActor.hpp"


SimplifiedActor::SimplifiedActor(const std::string& id) :
	Actor(id)
{
}

void SimplifiedActor::Terminate()
{
	Send("$TERM");
}

//---------------------------------------------------------------------------
//	Remote command management

Actor::strings_t SimplifiedActor::Exec(const std::string& cmd, const strings_t& params)
{
	Send(cmd, params);
	return Recv();
}

Actor::strings_t SimplifiedActor::OnCommand(const std::string& cmd, const strings_t& params)
{
	handlers_t::const_iterator p = m_handlers.find(cmd);
	if (p != m_handlers.end())
	{
		return (p->second)(this, params);
	}

	Actor::strings_t out;
	out.push_back("unhandled command");
	out.push_back(cmd);
	std::copy(params.begin(), params.end(), std::back_inserter(out));
	return out;
}

void SimplifiedActor::Register(const std::string& cmd, simplified_callback_t* f)
{
	m_handlers.insert(std::make_pair(cmd, f));
}

void SimplifiedActor::Deregister(const std::string& cmd)
{
	handlers_t::iterator p = m_handlers.find(cmd);
	if (p != m_handlers.end())
		m_handlers.erase(p);
}

void SimplifiedActor::Deregister(simplified_callback_t* f)
{
	for (handlers_t::iterator p = m_handlers.begin(); p != m_handlers.end(); )
		if (p->second == f)
			m_handlers.erase(p++);	// inc p before it's invalidated
		else
			++p;
}
