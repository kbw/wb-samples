#pragma once

#include "Actor.hpp"
#include <map>

class SimplifiedActor;
typedef Actor::strings_t simplified_callback_t(void* self, const Actor::strings_t& params);

//!	SimplifiedActor
//!
//!	This is the base of one style of Actor usage.  In this style, all arguments are passed
//!	in and returned as Actor::strings_t, an ordered set of strings.
//!
//!	Members are added to the actor, with matching static functions that call these.  These
//! static functions can be registered and deregistered at runtime.  Only registered calls
//! are executed within remote side of the actor, members are executed in the local side.
//!	The OnCommand supports handling messages within the remote side for an implementation.
class SimplifiedActor : public Actor
{
public:
	typedef Actor inherited;

	SimplifiedActor(const std::string& id);

	void		Terminate();

	//  Remote command management
	void		Register(const std::string& cmd, simplified_callback_t* f);
	void		Deregister(const std::string& cmd);
	void		Deregister(simplified_callback_t* f);
	strings_t	Exec(const std::string& cmd, const strings_t& params);

protected:
	strings_t	OnCommand(const std::string& cmd, const strings_t& params);

private:
	typedef std::map<std::string, simplified_callback_t*> handlers_t;

	handlers_t	m_handlers;
};
