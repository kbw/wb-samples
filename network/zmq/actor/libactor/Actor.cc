#include "Actor.hpp"

extern "C"
{
	#include <czmq.h>
	#include <zactor.h>
}
#include <zmq.hpp>


namespace
{
	zmsg_t* create_msg(const std::string& str)
	{
		if (zmsg_t* msg = zmsg_new())
		{
			if (zframe_t* frame = zframe_from(str.c_str()))
				zmsg_append(msg, &frame);

			return msg;
		}

		return NULL;
	}

	zmsg_t* create_msg(const Actor::strings_t& params)
	{
		if (zmsg_t* msg = zmsg_new())
		{
			for (Actor::strings_t::const_iterator p = params.begin(); p != params.end(); ++p)
				if (zframe_t* frame = zframe_from(p->c_str()))
					zmsg_append(msg, &frame);

			return msg;
		}

		return NULL;
	}

	zmsg_t* append_msg(zmsg_t* msg, const Actor::strings_t& params)
	{
		for (Actor::strings_t::const_iterator p = params.begin(); p != params.end(); ++p)
			if (zframe_t* frame = zframe_from(p->c_str()))
				zmsg_append(msg, &frame);

		return msg;
	}

	zmsg_t* load_frames(zmsg_t* msg, Actor::strings_t& params)
	{
		while (msg)
		{
			if (char* param = zmsg_popstr(msg))
			{
				params.push_back(param);
				free(param);
				continue;
			}

			// no more frames
			break;
		}

		return msg;
	}
} // namespace


// ---------------------------------------------------------------------------

class ActorImpl
{
public:
	ActorImpl(Actor* self, const std::string& addr);
	~ActorImpl();

	void	Send(const std::string& cmd, const Actor::strings_t& params);
	Actor::strings_t	Recv();

private:
	ActorImpl(const ActorImpl& );
	ActorImpl& operator=(const ActorImpl& );

	static	void	handler(zsock_t* pipe, void* args);

	Actor*		m_parent;
	std::string	m_addr;
	zactor_t*	m_actor;
};

ActorImpl::ActorImpl(Actor* parent, const std::string& addr) :
	m_parent(parent),
	m_addr(addr),
	m_actor(zactor_new(handler, this))
{
}

ActorImpl::~ActorImpl()
{
	zactor_destroy(&m_actor);
}

void ActorImpl::Send(const std::string& cmd, const Actor::strings_t& params)
{
	if (params.empty())
	{
		zstr_send(m_actor, cmd.c_str());		// send final
		return;
	}

	size_t n = params.size();
	zstr_sendm(m_actor, cmd.c_str());			// send with MORE flag
	for (Actor::strings_t::const_iterator p = params.begin(); p != params.end(); ++p)
		switch (--n)
		{
		case 0:
			zstr_send(m_actor, p->c_str());		// send final
			break;
		default:
			zstr_sendm(m_actor, p->c_str());	// send with MORE flag
		}
}

Actor::strings_t ActorImpl::Recv()
{
	Actor::strings_t params;

	if (zmsg_t* msg = load_frames(zmsg_recv(m_actor), params))
		zmsg_destroy(&msg);

	return params;
}

void ActorImpl::handler(zsock_t* pipe, void* args)
{
	ActorImpl* self = static_cast<ActorImpl*>(args);

	zsock_signal(pipe, 0);
	
	bool stop = false;
	while (!stop)
	{
		if (zmsg_t* msg = zmsg_recv(pipe))
		{
			char* cmd = zmsg_popstr(msg);
			if (!cmd)
			{
				// Error: We received nothing
			}
			else if (streq(cmd, "$TERM"))
			{
				// Quit message
				stop = true;
			}
			else
			{
				std::string command(cmd);

				// load parameters from extra frames in the message
				Actor::strings_t params;
				load_frames(msg, params);

				// Dispatch
				const Actor::strings_t reply = self->m_parent->OnCommand(command, params);

				// Send reply
				if (zmsg_t* rmsg = create_msg(reply))
					zmsg_send(&rmsg, pipe);
			}

			free(cmd), cmd = NULL;
			zmsg_destroy(&msg), msg = NULL;
			continue;
		}

		// no more messages
		stop = true;
	}
}


// ---------------------------------------------------------------------------

Actor::Actor(const std::string& addr) :
	pImpl(new ActorImpl(this, addr))
{
}

Actor::~Actor()
{
	delete pImpl, pImpl = 0;
}

void Actor::Send(const std::string& cmd, const strings_t& params)
{
	pImpl->Send(cmd, params);
}

Actor::strings_t Actor::Recv()
{
	return pImpl->Recv();
}

Actor::strings_t Actor::OnCommand(const std::string& cmd, const strings_t& params)
{
	return strings_t();
}
