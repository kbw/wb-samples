#pragma once

#include <string>
#include <vector>

class ActorImpl;

//!	Actor implements the axioms in a primitive way.  All parameters are passed
//! in and returned as Actor::strings_t, an ordered set of strings.
//!
//!	An address is a URL without an path.  The protocols are those supported by
//!	ZeroMQ and include TCP/IP protocols and shared memory.  Hostnames are DNS
//!	names.  There is no default port, it must be explicitly specified.
class Actor
{
friend class ActorImpl;
public:
	typedef std::vector<std::string> strings_t;

	Actor(const std::string& addr);
	virtual	~Actor();

	virtual	void		Send(const std::string& cmd, const strings_t& params = strings_t());
	virtual	strings_t	Recv();

protected:
	virtual	strings_t	OnCommand(const std::string& cmd, const strings_t& params);

private:
	Actor(const Actor& );
	Actor& operator=(const Actor& );

	ActorImpl*	pImpl;
};
