#pragma once

#include "SimplifiedActor.hpp"
#include <map>
#include <set>

extern "C"
{
	typedef SimplifiedActor* CreatorFunc(const char* addr);
}

class Creator : public SimplifiedActor
{
public:
	Creator(const std::string& id);
	~Creator();

	Actor::strings_t Exist(const Actor::strings_t& params);
	Actor::strings_t Create(const Actor::strings_t& params);
	SimplifiedActor* Create(const std::string& name, const std::string& addr);
	Actor::strings_t actor(const Actor::strings_t& params);

private:
	typedef void* handle_t;

	struct ActorInfo
	{
		handle_t handle;
		CreatorFunc* create;

		ActorInfo(handle_t handle = NULL, CreatorFunc* create = NULL) :
			handle(handle), create(create)
		{}
	};

	typedef std::map<std::string, ActorInfo> ActorInfoList;
	typedef	std::set<std::pair<SimplifiedActor*, std::string> > InstanceList;

	std::string	lib_extension() const;
	bool		load_library(const std::string& actor_name);

	ActorInfoList m_actors;
	InstanceList m_instances;
};
