#include "Creator.hpp"
#include <dlfcn.h>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>

#ifdef PLATFORM
#include <string.h>
#endif

#include <string.h>

namespace
{
/*
	Actor::strings_t Fetch(void* p, const Actor::strings_t& params)
	{
		return Actor::strings_t();
	}

	Actor::strings_t Build(void* p, const Actor::strings_t& params)
	{
		return Actor::strings_t();
	}

	Actor::strings_t Install(void* p, const Actor::strings_t& params)
	{
		return Actor::strings_t();
	}
 */

	Actor::strings_t Exist(void* p, const Actor::strings_t& params)
	{
		Creator* self = reinterpret_cast<Creator*>(p);
		std::cout << "::Exist\n";

		return self->Exist(params);
	}

	Actor::strings_t Create(void* p, const Actor::strings_t& params)
	{
		Creator* self = reinterpret_cast<Creator*>(p);
		std::cout << "::Create\n";

		return self->Create(params);
	}

	Actor::strings_t actor(void* p, const Actor::strings_t& params)
	{
		Creator* self = reinterpret_cast<Creator*>(p);
		std::cout << "::actor\n";

		return self->actor(params);
	}
}

Creator::Creator(const std::string& id) :
	SimplifiedActor(id)
{
	Register("Exist", ::Exist);
	Register("Create", ::Create);
	Register("actor", ::actor);
}

Creator::~Creator()
{
}

// Does Actor Exist?
//	In:	ordered sequence of names
//	Out: ordered sequence of { "yes", "no" }
Actor::strings_t Creator::Exist(const Actor::strings_t& params)
{
	std::cout << "Creator::Exist\n";
	Actor::strings_t ret;

	for (Actor::strings_t::const_iterator p = params.begin(); p != params.end(); ++p)
	{
		const std::string& name = *p;

		ActorInfoList::const_iterator p_actor = m_actors.find(name);
		bool ok = (p_actor == m_actors.end()) ? load_library(name) : true;

		ret.push_back(ok ? "yes" : "no");
	}

	return ret;
}

// Instantiate Actor
//	In: name addr1 ... addrn
//	Out: instance name
Actor::strings_t Creator::Create(const Actor::strings_t& params)
{
	std::cout << "Creator::Create\n";
	Actor::strings_t ret;

	const std::string& name = params[0];	// module name
	const std::string& addr = params[1];	// we only support 1 addr currently
	if (SimplifiedActor* p = Create(name, addr))
	{
		m_instances.insert(std::make_pair(p, name));
		std::stringstream ss;
		ss << "Ox" << std::setw(8) << std::setfill('0') << std::hex << p;
		ret.push_back(ss.str());
	}

	return ret;
}

// Execute a command in a named actor
//	In: name, action
//	Out: reply from that action
Actor::strings_t Creator::actor(const Actor::strings_t& params)
{
	std::cout << "Creator::Actor\n";
	Actor::strings_t ret;

	const std::string& name = params[0];
	const std::string& action = params[1];

	SimplifiedActor* actor = NULL;
	InstanceList::iterator idx;
	if (strncmp(name.c_str(), "0x", 2) == 0)
	{
		size_t nchars = 0;
		int addr = std::stoi(name.c_str() + 2, &nchars, 16);
		if (SimplifiedActor* p = reinterpret_cast<SimplifiedActor*>(addr))
		{
			struct CompareFirst
			{
				SimplifiedActor* ptr;

				CompareFirst(SimplifiedActor* p) : ptr(p) {}
				bool operator()(const InstanceList::value_type& val) const { return ptr == val.first; }
			};

			idx = std::find_if(m_instances.begin(), m_instances.end(), CompareFirst(p));
			if (idx != m_instances.end())
				actor = p;
		}
	}

	if (actor)
	{
		if (action == "Terminate")
		{
			
			actor->Terminate();
			m_instances.erase(idx);
		}
	}

	return ret;
}

std::string Creator::lib_extension() const
{
#ifdef PLATFORM
	if (strcmp("##PLATFORM##", "Darwin") == 0)
		return ".dylib";
#endif
	return ".so";
}

typedef void local_func_t(void);
static local_func_t* local_dlfunc(void* h, const char* name)
{
	union
	{
		void* sym;
		void (*func)(void);
	} ret;

	ret.sym = dlsym(h, name);
	return ret.func;
}

bool Creator::load_library(const std::string& name)
{
	bool ok = false;

	// try to load the library
	std::string libname = "libactor-" + name + lib_extension();
	if (handle_t h = dlopen(libname.c_str(), RTLD_NOW))
	{
		if (CreatorFunc* create = reinterpret_cast<CreatorFunc*>(local_dlfunc(h, "create")))
		{
			m_actors.insert(std::make_pair(name, ActorInfo(h, create)));
			ok = true;
		}
	}

	return ok;
}

SimplifiedActor* Creator::Create(const std::string& name, const std::string& addr)
{
	std::cout << "Creator::Create name=" << name << ", addr=" << addr << "\n";
	ActorInfoList::const_iterator p_actor = m_actors.find(name);
	if (p_actor != m_actors.end())
	{
		return p_actor->second.create(addr.c_str());
	}

	return NULL;
}
