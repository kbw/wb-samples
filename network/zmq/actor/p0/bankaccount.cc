#include "bankaccount.hpp"
#include <unistd.h>
#include <iostream>

//---------------------------------------------------------------------------

namespace
{
	const string cmdBalance  = "$bal";
	const string cmdDeposit  = "$deposit";
	const string cmdWithdraw = "$withdraw";

	inline struct timespec operator-(struct timespec a, struct timespec b)
	{
		struct timespec c = { a.tv_sec - b.tv_sec, a.tv_nsec - b.tv_nsec };
		if (c.tv_nsec < 0)
		{
			c.tv_nsec += 1000 * 1000 * 1000;
			--c.tv_nsec;
		}

		return c;
	}

	inline struct timespec& operator-=(struct timespec& a, struct timespec b)
	{
		a.tv_sec -= b.tv_sec;
		a.tv_nsec -= b.tv_nsec;
		if (a.tv_nsec < 0)
		{
			a.tv_nsec += 1000 * 1000 * 1000;
			--a.tv_sec;
		}

		return a;
	}
}

using namespace std;

bankaccount::bankaccount(const string& accno) :
	SimplifiedActor("inproc://bankaccount"),

	accno(accno),
	bal(0.0)
{
	clock_gettime(CLOCK_MONOTONIC, &start);
	clog << "started actor: ok\n";

	Register(cmdBalance, handleBalance);
	Register(cmdDeposit, handleDeposit);
	Register(cmdWithdraw, handleWithdraw);
}

bankaccount::~bankaccount()
{
}

string bankaccount::time() const
{
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	now -= start;

	char buf[16];
	snprintf(buf, sizeof(buf), "%ld.%09ld", now.tv_sec, now.tv_nsec);
	return buf;
}

//---------------------------------------------------------------------------

double bankaccount::balance()
{
	Actor::strings_t ret = Exec(cmdBalance, Actor::strings_t());
	clog << time() << " : " << __FUNCTION__ << ": ret.size()=" << ret.size() << "\n";

	double value = -1.23;
	if (!ret.empty())
	{
		value = atof( ret.front().c_str() );
	}

	return value;
}

Actor::strings_t bankaccount::handleBalance(void* p, const Actor::strings_t& args)
{
	Actor::strings_t ret;
	if (const bankaccount* self = static_cast<const bankaccount*>(p))
	{
		clog << self->time() << " : " << __FUNCTION__ << ": args.size()=" << args.size() << "\n";
		if (!args.empty())
		{
			clog << self->time() << " : " << __FUNCTION__ << ": received " << args.size() << " items\n";
			for (const string& arg : args)
				clog << self->time() << " : " << __FUNCTION__ << "arg=" << arg << "\n";
		}

		ret.emplace_back( std::to_string(self->bal) );
	}

	return ret;
}

//---------------------------------------------------------------------------

double bankaccount::deposit(double amount)
{
	Actor::strings_t ret = Exec(cmdDeposit, { std::to_string(amount) });
	clog << time() << " : " << __FUNCTION__ << ": ret.size()=" << ret.size() << "\n";

	double value = 0.0;
	if (!ret.empty())
	{
		value = atof( ret.front().c_str() );
	}

	return value;
}

Actor::strings_t bankaccount::handleDeposit(void* p, const Actor::strings_t& args)
{
	Actor::strings_t ret;
	if (bankaccount* self = static_cast<bankaccount*>(p))
	{
		clog << self->time() << " : " << __FUNCTION__ << ": args.size()=" << args.size() << "\n";
		if (args.size() == 1)
		{
			clog << self->time() << " : " << __FUNCTION__ << ": received: " << args.front() << "\n";

			double amount = atof( args.front().c_str() );
			self->bal += amount;

			ret.emplace_back( std::to_string(amount) );
		}
		else
		{
			clog << self->time() << " : " << __FUNCTION__ << ": received " << args.size() << " items\n";
			for (const string& arg : args)
				clog << self->time() << " : " << __FUNCTION__ << "arg=" << arg << "\n";
		}
	}

	return ret;
}

//---------------------------------------------------------------------------

double bankaccount::withdraw(double amount)
{
	Actor::strings_t ret = Exec(cmdWithdraw, { std::to_string(amount) });
	clog << time() << " : " << __FUNCTION__ << ": ret.size()=" << ret.size() << "\n";
	if (ret.size() != 1)
	{
		string err = ret.front();
		for (size_t i = 1; i < ret.size(); ++i)
			err += " " + ret[i];
		throw std::runtime_error(err);
	}

	return atof( ret.front().c_str() );
}

Actor::strings_t bankaccount::handleWithdraw(void* p, const Actor::strings_t& args)
{
	Actor::strings_t ret;
	if (bankaccount* self = static_cast<bankaccount*>(p))
	{
		clog << self->time() << " : " << __FUNCTION__ << ": args.size()=" << args.size() << "\n";
		if (args.size() == 1)
		{
			clog << self->time() << " : " << __FUNCTION__ << ": received: " << args.front() << "\n";

			double amount = atof( args.front().c_str() );
			self->bal -= amount;

			ret.emplace_back( std::to_string(amount) );
		}
		else
		{
			clog << self->time() << " : " << __FUNCTION__ << ": received " << args.size() << " items\n";
			for (const string& arg : args)
				clog << self->time() << " : " << __FUNCTION__ << "arg=" << arg << "\n";
		}
	}

	return ret;
}
