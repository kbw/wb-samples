#include "bankaccount.hpp"
#include <exception>
#include <iostream>
#include <iomanip>

using namespace std;

//---------------------------------------------------------------------------

int main()
try
{
	bankaccount a("0001");

	cout << a.time() << " : main: test timer\n";
	cout << a.time() << " : main: test timer\n";

	cout << a.time() << " : main: " << "deposit: " << setprecision(4) << a.deposit(34.56) << "\n\n";
	cout << a.time() << " : main: " << "balance: " << setprecision(4) << a.balance() << "\n\n";

	cout << a.time() << " : main: " << "withdraw: " << setprecision(4) << a.withdraw(89.01) << "\n\n";
	cout << a.time() << " : main: " << "balance: " << setprecision(4) << a.balance() << "\n\n";
}
catch (const std::exception& e)
{
	cerr << "err: " << e.what() << "\n";
}
