#pragma once

#include "defs.hpp"
#include <libactor/SimplifiedActor.hpp>
#include "sys/time.h"

//---------------------------------------------------------------------------

class bankaccount : private SimplifiedActor
{
public:
	bankaccount(const string& accno);
	virtual	~bankaccount();

	virtual	double	balance();
	virtual	double	deposit(double amount);
	virtual	double	withdraw(double amount);

			string	time() const;

private:
	static	strings_t	handleBalance(void* self, const strings_t& args);
	static	strings_t	handleDeposit(void* self, const strings_t& args);
	static	strings_t	handleWithdraw(void* self, const strings_t& args);

private:
	string accno;
	double bal;

	struct timespec start;
};
