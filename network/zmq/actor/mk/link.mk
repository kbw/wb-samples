include ../mk/platform.mk

LDFLAGS  += -L/usr/local/lib -lczmq -lzmq

ifeq ($(strip $(PLATFORM)), Linux)
  LDFLAGS += -ldl
endif
