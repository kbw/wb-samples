#include "common.hpp"
#include "options.hpp"
#include <unistd.h>		// exec
#include <vector>
#include <stdlib.h>		// malloc
#include <stdarg.h>		// va_ ...
#include <stdio.h>		// asprintf
#include <string.h>		// strdup

int spawn(const char* prog, ...)
{
	if (int pid = fork())
		return pid;

	if (opts.verbose == 2)
		 printf("spawn:%d: child exec=%s\n", getpid(), prog);
	int argc = 1;
	char** argv = nullptr;
	{
		std::vector<char*> args;
		args.reserve(8);
		args.push_back(strdup(prog));
		if (opts.verbose == 2)
			printf("\tspawn:%d: arg[0]=%s\n", getpid(), prog);

		va_list va;
		va_start(va, prog);
		const char* p;
		while ((p = va_arg(va, const char*)))
		{
			++argc;
			args.push_back(strdup(p));
			if (opts.verbose == 2)
				printf("\tspawn:%d: arg[%lu]=%s\n", getpid(), args.size() - 1, p);
		}
		va_end(va);

		args.push_back(nullptr);
		if (opts.verbose == 2)
			printf("\tspawn:%d: arg[%lu]=%s\n", getpid(), args.size() - 1, "null");

		argv = (char**)malloc(args.size() * sizeof(char*));
		for (size_t i = 0; i != args.size(); ++i)
			argv[i] = args[i];
	}

	string o = fmt("spawn:%d: execv=%s", getpid(), *argv);
	for (int i = 1; i < argc; ++i)
		o += fmt(" %s", argv[i]);
	if (opts.verbose == 1)
		printf("%s\n", o.c_str());
	return execv(*argv, argv);
}

std::string fmt(const char* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	char* buf = nullptr;
	int bufsz = vasprintf(&buf, fmt, va);
	va_end(va);

	std::string s;
	if (buf && bufsz > 0)
		s.assign(buf, bufsz);
	free(buf);
	return s;
}
