#include "msg_queue.hpp"
#include "config.hpp"
#include "utils.hpp"
#include "log.hpp"

#include <sys/types.h>
#include <sys/stat.h>

#include <fstream>
#include <iostream>

#include <stdlib.h>

//---------------------------------------------------------------------------
// m8::msg_queue_t
m8::msg_queue_t::pqueues_t m8::msg_queue_t::sm_queues;

m8::msg_queue_t::msg_queue_t(const m8::basic_string& name, zmq::context_t& ctx, const m8::stream_info_t& stream_info) :
	m_name(name),
	m_socket(ctx, stream_info.type)
{
	setsockopt(stream_info);

	sm_queues.push_back(this);
}

m8::msg_queue_t::~msg_queue_t() {
	auto p = std::find(sm_queues.begin(), sm_queues.end(), this);
	if (p != sm_queues.end()) {
		sm_queues.erase(p);
	}
}

const m8::basic_string& m8::msg_queue_t::get_name() const {
	return m_name;
}

void m8::msg_queue_t::setsockopt(const m8::stream_info_t& stream_info) {
	if (stream_info.bind) {
		m_socket.bind(stream_info.addr);
	}
	else {
		m_socket.connect(stream_info.addr);
	}

	if (stream_info.backlog) {
		int n = *stream_info.backlog;
		m_socket.setsockopt(ZMQ_BACKLOG, n);
	}

	if (stream_info.conflate) {
		int n = *stream_info.conflate;
		m_socket.setsockopt(ZMQ_CONFLATE, n);
	}

	if (stream_info.linger) {
		int n = *stream_info.linger;
		m_socket.setsockopt(ZMQ_LINGER, n);
	}

	if (stream_info.rcvhwm) {
		int n = *stream_info.rcvhwm;
		m_socket.setsockopt(ZMQ_RCVHWM, n);
	}

	if (stream_info.sndhwm) {
		int n = *stream_info.sndhwm;
		m_socket.setsockopt(ZMQ_SNDHWM, n);
	}
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// m8::msg_queue_t io
void m8::msg_queue_t::register_on_recv(void* ctx, recv_f* func) {
	m_recv.emplace_back(ctx, func);
}

void m8::msg_queue_t::recv(const m8::buffer_t* buf) {
	ensure(buf, "null buffer passed to recv");

	m8::clog << PREAMBLE << "recv(buf=\"" << *buf << "\")" << "\n";
	for (recv_infos_t::iterator p = m_recv.begin(); p != m_recv.end(); ++p) {
		if (p->func()) {
			m8::clog << PREAMBLE << "calling recv callback with buf=\"" << *buf << "\"" << "\n";
			p->func()(p->ctx(), buf);
		}
	}
}

void m8::msg_queue_t::register_on_send(void* ctx, send_f* func) {
	m_send.emplace_back(ctx, func);
}

void m8::msg_queue_t::send(const m8::buffer_t& buf) {
	m8::clog << PREAMBLE << "send(buf=\"" << buf << "\")" << "\n";
	m_send_queue.push(buf);

	for (send_infos_t::iterator p = m_send.begin(); p != m_send.end(); ++p) {
		if (p->func()) {
			m8::clog << PREAMBLE << "calling send callback with buf=\"" << buf << "\"" << "\n";
			p->func()(p->ctx(), buf);
		}
	}
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// m8::msg_queue_t multiplexer
void m8::msg_queue_t::do_recv(m8::buffer_t* buf) {
	precondition(buf, "null buffer in do_recv");

	zmq::message_t msg;
	m_socket.recv(&msg);
	buf->assign(static_cast<const char*>(msg.data()), msg.size());
	m8::clog << PREAMBLE << "do_recv(buf=\"" << *buf << "\")" << "\n";

	recv(buf);
}

void m8::msg_queue_t::do_send(const m8::buffer_t& buf) {
	m8::clog << PREAMBLE << "do_send(buf=\"" << buf << "\")" << "\n";
	zmq::message_t msg(buf.c_str(), buf.size());
	m_socket.send(msg);
}

void m8::msg_queue_t::multiplex(long timeout) {
	precondition(!sm_queues.empty(), "nothing to multiplex");

	// build pollitems array
	// pollitems and sm_queues have matching position so we index from one to the other directly
	pollitems_t pollitems;
	pollitems.reserve(sm_queues.size());
	for (pqueues_t::iterator p = sm_queues.begin(); p != sm_queues.end(); ++p) {
		pollitems.push_back({ (*p)->m_socket, 0, 0, 0 });
	}
	ensure(pollitems.size() == sm_queues.size(), format("pollitems and sm_queues have different sizes pollitems=%zu sm_queues=%zu", pollitems.size(), sm_queues.size()));

	while (true) {
		// reset event flags prior to poll
		for (size_t i = 0; i != pollitems.size(); ++i) {
			pollitems[i].revents = 0;
			pollitems[i].events = ZMQ_POLLIN;
			if (!sm_queues[i]->m_send_queue.empty()) {
				pollitems[i].events |= ZMQ_POLLOUT;
			}
		}

		// do it, poll
		if (m8::clog.enabled()) {
			m8::clog << PREAMBLE << pollitems.size() << " pollitems, timeout=" << timeout << " waiting..." << "\n";
			for (size_t i = 0; i != sm_queues.size(); ++i) {
				m8::clog << PREAMBLE << "sm_queues[" << i << "]->m_send_queue.size=" << sm_queues[i]->m_send_queue.size() << "\n";
			}
		}
		int rc = zmq::poll(&pollitems.front(), (int)pollitems.size(), timeout);
		ensure(-1 != rc, "poll failed");
		ensure(0 <= rc && rc <= (int)pollitems.size(), "poll out of range");
		multiplex_handler(rc, pollitems);
	}
}

// who did what? handle it
void m8::msg_queue_t::multiplex_handler(int rc, pollitems_t& pollitems) {
	int cnt = 0;
	for (size_t i = 0; i != pollitems.size(); ++i) {
		if (pollitems[i].revents) {
			++cnt;

			// let owner pull from queue
			if (pollitems[i].revents & ZMQ_POLLIN) {
				buffer_t buf;
				sm_queues[i]->do_recv(&buf);
			}

			// let owner push onto its queue (match by index)
			if (pollitems[i].revents & ZMQ_POLLOUT && !sm_queues[i]->m_send_queue.empty()) {
				do {
					m8::buffer_t buf = std::move(sm_queues[i]->m_send_queue.front());
					ensure(sm_queues[i]->m_send_queue.front().empty(), "buffer content not moved");

					sm_queues[i]->do_send(buf);
					sm_queues[i]->m_send_queue.pop();
				}
				while (!sm_queues[i]->m_send_queue.empty());
			}
		}
	}
	ensure(cnt == rc, format("didn't process all events cnt=%d rc=%d", cnt, rc));
}
