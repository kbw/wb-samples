#ifdef LOG_INLINE
#include <iostream>

namespace m8 {
	//-----------------------------------------------------------------------
	// logstream
	inline bool logstream::enable(bool value) {
		return m_enabled = value;
	}

	inline bool logstream::enabled() const {
		return m_enabled;
	}

	inline m8::logstream& m8::logstream::format(const char* fmt, ...) {
		return *this;
	}

	inline logstream& logstream::operator<<(short value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(unsigned short value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(int value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(unsigned int value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(long value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(unsigned long value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(long long value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(unsigned long long value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(float value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(double value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(long double value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

/*
	inline logstream& logstream::operator<<(bool value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}
 */

	inline logstream& logstream::operator<<(const char* value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(const void* value) {
		if (m_enabled) {
			m_os << value;
		}
		return *this;
	}

/*
	inline logstream& logstream::operator<<(std::nullptr_t) {
		if (m_enabled)
			m_os << "(null)";
		return *this;
	}

	inline logstream& logstream::operator<<(std::basic_streambuf<char, std::char_traits<char>>* sb) {
		if (m_enabled) {
			m_os << sb;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(std::ios_base& (*func)(std::ios_base&)) {
		if (m_enabled) {
			m_os << func;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(std::basic_ios<char, std::char_traits<char>>& (*func)(std::basic_ios<char, std::char_traits<char>>&)) {
		if (m_enabled) {
			m_os << func;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(std::basic_ostream<char, std::char_traits<char>>& (*func)(std::basic_ostream<char, std::char_traits<char>>&)) {
		if (m_enabled) {
			m_os << func;
		}
		return *this;
	}

	inline logstream& logstream::operator<<(logstream& (*func)(logstream& os)) {
		if (m_enabled) {
			m_os << func;
		}
		return *this;
	}

	inline logstream& endl(logstream& os) {
		os.put(os.widen('\n'));
		os.flush();
		return os;
	}
 */
}
#endif	// LOG_INLINE
