#include "service.hpp"
#include "utils.hpp"
#include "log.hpp"

//---------------------------------------------------------------------------
// helpers
namespace {
	m8::service_t::msg_queues_t create_queues(zmq::context_t& ctx, const m8::stream_config_t& config) {
		m8::service_t::msg_queues_t msg_queues;

		for (m8::stream_config_t::const_iterator p = config.begin(); p != config.end(); ++p) {
			const m8::basic_string& name = p->first;
			const m8::stream_info_t& info = p->second;

			msg_queues.emplace(name, std::unique_ptr<m8::msg_queue_t>(new m8::msg_queue_t(name, ctx, info)));
			m8::clog << "\tstream: " << name << "\n";
		}

		return msg_queues;
	}
}

//---------------------------------------------------------------------------
// service_t
m8::service_t::service_t(zmq::context_t& ctx, const m8::basic_string& name, const m8::stream_config_t& stream_info) :
	m_name(name),
	m_queues(create_queues(ctx, stream_info))
{
	m8::clog << "service: " << name << "\n";
}

const m8::basic_string& m8::service_t::get_name() const {
	return m_name;
}

m8::service_t::msg_queues_t& m8::service_t::get_queues() {
	return m_queues;
}
