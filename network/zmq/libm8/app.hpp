#pragma once
#include "utils.hpp"

#include <zmq.hpp>

#include <memory>
#include <map>

namespace m8 {
	class service_t;

	//-----------------------------------------------------------------------
	// app_t
	class app_t {
	public:
		typedef std::map<basic_string, std::unique_ptr<service_t>> services_t;

		app_t(const basic_string& name, const basic_string& config_filename);
		virtual ~app_t();
		virtual void run();
		virtual const basic_string& get_name() const;
		virtual services_t& get_services();

	private:
		const basic_string m_name;
		zmq::context_t m_ctx;
		services_t m_services;
	};
}
