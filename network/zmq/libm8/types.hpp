#pragma once
#include <string>

namespace m8 {
	//-----------------------------------------------------------------------
	// types
	struct basic_string : public std::string {
		typedef std::string inherited;

		using inherited::inherited;
	};
}
