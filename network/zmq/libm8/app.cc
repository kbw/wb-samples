#include "app.hpp"
#include "service.hpp"

//---------------------------------------------------------------------------
// m8::app_t
m8::app_t::app_t(const basic_string& name, const basic_string& config_filename) :
	m_name(name)
{
	m8::service_config_t config(read_config(config_filename, config_type::json));

	for (m8::service_config_t::const_iterator p = config.begin(); p != config.end(); ++p) {
		const basic_string& name = p->first;
		const stream_config_t& info = p->second;

		m_services.emplace(name, std::unique_ptr<service_t>(new service_t(m_ctx, name, info)));
	}
}

m8::app_t::~app_t() {
}

void m8::app_t::run() {
	msg_queue_t::multiplex(15000);
}

const m8::basic_string& m8::app_t::get_name() const {
	return m_name;
}

m8::app_t::services_t& m8::app_t::get_services() {
	return m_services;
}
