#pragma once
#include "types.hpp"

#include <stdexcept>

namespace m8 {
	//-----------------------------------------------------------------------
	// utility prototypes
	basic_string format(const char* fmt, ...);

	//-----------------------------------------------------------------------
	// macros
#ifndef NDEBUG
		class consistency_error : public std::runtime_error {
		public:
			consistency_error(
					const char* file, int line,
					const basic_string& text) :
				std::runtime_error(format("%s:%d ", file, line) + text)
			{}
		};

		class precondition_error : public consistency_error {
		public:
			precondition_error(
					const char* file, int line,
					const basic_string& text) :
				consistency_error(file, line, text)
			{}
		};
		#define	precondition(expr, text) if (!(expr)) throw m8::precondition_error(__FILE__, __LINE__, (text));

		class postcondition_error : public consistency_error {
		public:
			postcondition_error(
					const char* file, int line,
					const basic_string& text) :
				consistency_error(file, line, text)
			{}
		};
		#define	postcondition(expr, text) if (!(expr)) throw m8::postcondition_error(__FILE__, __LINE__, (text));

		class invariant_error : public consistency_error {
		public:
			invariant_error(
					const char* file, int line,
					const basic_string& text) :
				consistency_error(file, line, text)
			{}
		};
		#define	invariant(expr, text) if (!(expr)) throw m8::postcondition_error(__FILE__, __LINE__, (text));

		class assert_error : public consistency_error {
		public:
			assert_error(
					const char* file, int line,
					const basic_string& text) :
				consistency_error(file, line, text)
			{}
		};
		#define	ensure(expr, text) if (!(expr)) throw m8::postcondition_error(__FILE__, __LINE__, (text));
#else
		#define	precondition(expr, obj)
		#define	postcondition(expr, obj)
		#define	invariant(expr, text)
		#define	ensure(expr, text)
#endif
}
