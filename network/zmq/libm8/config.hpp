#pragma once
#include "types.hpp"

#include <rapidjson/document.h>
#include <zmq.hpp>

#include <memory>
#include <map>

namespace m8 {
	//-----------------------------------------------------------------------
	// config
	struct stream_info_t {
		stream_info_t() :
			bind(false), type(zmq::socket_type::req)
		{}

		basic_string addr;
		bool bind;
		zmq::socket_type type;

		std::shared_ptr<int> affinity;
		std::shared_ptr<int> backlog;
		std::shared_ptr<bool> conflate;
		std::shared_ptr<int> linger;
		std::shared_ptr<int> rcvhwm;
		std::shared_ptr<int> sndhwm;
	};
	typedef std::map<basic_string, stream_info_t> stream_config_t;
	typedef std::map<basic_string, stream_config_t> service_config_t;

	enum struct config_type { unknown, json, yaml, ini };

	basic_string to_basic_string(config_type val);

	zmq::socket_type to_zmq_socket_type(const basic_string& text);
	zmq::socket_type to_zmq_socket_type(const char* text);
	basic_string to_basic_string(config_type type);

	service_config_t read_config(const basic_string& fielname, config_type type);
	service_config_t read_config_json(const rapidjson::Document& doc);
	rapidjson::Document read_config_json(const basic_string& fileame);
}
