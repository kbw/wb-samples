#include "utils.hpp"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

//---------------------------------------------------------------------------
// utilities

m8::basic_string m8::format(const char* fmt, ...) {
	va_list params;
	va_start(params, fmt);

	char* buf = nullptr;
	int len = vasprintf(&buf, fmt, params);
	va_end(params);

	basic_string ret;
	if (buf && len >= 0) {
		ret.assign(buf, (size_t)len);
	}

	free(buf);
	return ret;
}
