#pragma once
#include "fwd-config.hpp"
#include "types.hpp"
#include <zmq.hpp>

#include <utility>
#include <memory>
#include <map>
#include <vector>
#include <list>
#include <queue>

namespace m8 {
	//-----------------------------------------------------------------------
	// types
	class buffer_t : public basic_string {
		typedef basic_string inherited;

		using inherited::inherited;
	};

	//-----------------------------------------------------------------------
	// meg_queue_t
	typedef void recv_f(void* ctx, const buffer_t* buf);
	typedef void send_f(void* ctx, const buffer_t& buf);

	class msg_queue_t {
	public:
		msg_queue_t(const basic_string& name, zmq::context_t& ctx, const stream_info_t& stream_info);
		~msg_queue_t();

		const basic_string& get_name() const;

		void register_on_send(void* ctx, send_f* func);
		void send(const buffer_t& buf);

		void register_on_recv(void* ctx, recv_f* func);
		void recv(const buffer_t* buf);

		static void multiplex(long timeout = -1);

	private:
		// bind recv callback to queue
		struct recv_info_t : std::pair<void*, recv_f*> {
			recv_info_t(void* ctx = nullptr, recv_f* func = nullptr) : std::pair<void*, recv_f*>(ctx, func) {}

			void* ctx() const { return first; }
			recv_f* func() const { return second; }

			void ctx(void* n) { first = n; }
			void func(recv_f* n) { second = n; }
		};

		// bind send callback to queue
		struct send_info_t : std::pair<void*, send_f*> {
			send_info_t(void* ctx = nullptr, send_f* func = nullptr) : std::pair<void*, send_f*>(ctx, func) {}

			void* ctx() const { return first; }
			send_f* func() const { return second; }

			void ctx(void* n) { first = n; }
			void func(send_f* n) { second = n; }
		};

		typedef std::list<recv_info_t> recv_infos_t;
		typedef std::list<send_info_t> send_infos_t;

		typedef std::queue<buffer_t, std::list<buffer_t>> send_queue_t;
		typedef std::vector<msg_queue_t*> pqueues_t;

		typedef std::vector<zmq::pollitem_t> pollitems_t;

	private:
		void setsockopt(const stream_info_t& stream_info);

		void do_recv(buffer_t* buf);
		void do_send(const buffer_t& buf);

		static void multiplex_handler(int rc, pollitems_t& pollitems);

	private:
		const basic_string m_name;
		zmq::socket_t m_socket;
		recv_infos_t m_recv;
		send_infos_t m_send;
		send_queue_t m_send_queue;

		static pqueues_t sm_queues;
	};
}
