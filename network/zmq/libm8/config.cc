#include "config.hpp"
#include "msg_queue.hpp"
#include "utils.hpp"
#include "log.hpp"

#include <sys/types.h>
#include <sys/stat.h>

#include <fstream>

/*
//---------------------------------------------------------------------------
// helpers
msg_queues_t m8::create_queues(zmq::context_t& ctx, const stream_config_t& config) {
	msg_queues_t msg_queues;

	for (stream_config_t::const_iterator p = config.begin(); p != config.end(); ++p) {
		const basic_string& name = p->first;
		const stream_info_t& info = p->second;

		msg_queues.emplace(name, std::unique_ptr<msg_queue_t>(new msg_queue_t(name, ctx, info)));
		m8::clog << "\tstream: " << name << "\n";
	}

	return msg_queues;
}
 */

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// json config
zmq::socket_type m8::to_zmq_socket_type(const basic_string& text) {
	return to_zmq_socket_type(text.c_str());
}

zmq::socket_type m8::to_zmq_socket_type(const char* text) {
	if (!strcmp(text, "req")) {
		return zmq::socket_type::req;
	}
	if (!strcmp(text, "rep")) {
		return zmq::socket_type::rep;
	}
	if (!strcmp(text, "push")) {
		return zmq::socket_type::push;
	}
	if (!strcmp(text, "pull")) {
		return zmq::socket_type::pull;
	}
	if (!strcmp(text, "pub")) {
		return zmq::socket_type::pub;
	}
	if (!strcmp(text, "sub")) {
		return zmq::socket_type::sub;
	}

	throw std::runtime_error("unknown zmq socket type: " + basic_string(text));
}

m8::basic_string m8::to_basic_string(config_type type) {
	switch (type) {
	case config_type::unknown:
		return "unknown";
	case config_type::json:
		return "json";
	case config_type::yaml:
		return "yaml";
	case config_type::ini:
		return "ini";
	default:
		return "bad config type";
	}
}

m8::service_config_t m8::read_config(const basic_string& filename, config_type type) {
	switch (type) {
	case config_type::json:
		return read_config_json(read_config_json(filename));
	default:
		throw std::runtime_error("unsupported config file type: " + to_basic_string(type));
	}
}

m8::service_config_t m8::read_config_json(const rapidjson::Document& doc) {
	service_config_t services;

	for (rapidjson::Value::ConstMemberIterator p = doc.MemberBegin(); p != doc.MemberEnd(); ++p) {
		// service name
		basic_string service_name;
		if (p->name.IsString()) {
			service_name.assign(p->name.GetString());
		}
		else if (p->name.IsNumber()) {
			service_name.assign(std::to_string(p->name.GetInt()));
		}
		else {
			throw std::runtime_error("unrecognised service name type");
		}
		m8::clog << "service name: " << service_name << "\n";

		// service body
		if (p->value.IsObject()) {
			stream_config_t streams;
			for (rapidjson::Value::ConstMemberIterator q = p->value.MemberBegin(); q != p->value.MemberEnd(); ++q) {
				// stream name
				basic_string stream_name;
				if (q->name.IsString()) {
					stream_name.assign(q->name.GetString());
				}
				else if (q->name.IsNumber()) {
					stream_name.assign(std::to_string(q->name.GetInt()));
				}
				else {
					throw std::runtime_error("unrecognised stream name type");
				}
				m8::clog << "\tstream name: " << stream_name << "\n";

				// stream body
				if (q->value.IsObject()) {
					// stream entries
					stream_info_t stream;
					for (rapidjson::Value::ConstMemberIterator r = q->value.MemberBegin(); r != q->value.MemberEnd(); ++r) {
						// addr
						if (!strcmp(r->name.GetString(), "addr") && r->value.IsString()) {
							stream.addr = r->value.GetString();
						}
						// bind
						else if (!strcmp(r->name.GetString(), "bind") && r->value.IsBool()) {
							stream.bind = r->value.GetBool();
						}
						else if (!strcmp(r->name.GetString(), "bind") && r->value.IsString()) {
							stream.bind = !strcasecmp(r->value.GetString(), "true") ? true : false;
						}
						// zmq_socket_type
						else if (!strcmp(r->name.GetString(), "zmq_socket_type") && r->value.IsString()) {
							stream.type = to_zmq_socket_type(r->value.GetString());
						}
						// backlog
						else if (!strcmp(r->name.GetString(), "backlog") && r->value.IsString()) {
							stream.backlog.reset(new int(atoi(r->value.GetString())));
						}
						else if (!strcmp(r->name.GetString(), "backlog") && r->value.IsInt()) {
							stream.backlog.reset(new int(r->value.GetInt()));
						}
						// conflate
						else if (!strcmp(r->name.GetString(), "conflate") && r->value.IsString()) {
							stream.conflate.reset(new bool(atoi(r->value.GetString())));
						}
						else if (!strcmp(r->name.GetString(), "conflate") && r->value.IsInt()) {
							stream.conflate.reset(new bool(r->value.GetInt()));
						}
						else if (!strcmp(r->name.GetString(), "conflate") && r->value.IsBool()) {
							stream.conflate.reset(new bool(r->value.GetBool()));
						}
						// linger
						else if (!strcmp(r->name.GetString(), "linger") && r->value.IsString()) {
							stream.linger.reset(new int(atoi(r->value.GetString())));
						}
						else if (!strcmp(r->name.GetString(), "linger") && r->value.IsInt()) {
							stream.linger.reset(new int(r->value.GetInt()));
						}
						// rcvhwm
						else if (!strcmp(r->name.GetString(), "rcvhwm") && r->value.IsString()) {
							stream.rcvhwm.reset(new int(atoi(r->value.GetString())));
						}
						else if (!strcmp(r->name.GetString(), "rcvhwm") && r->value.IsInt()) {
							stream.rcvhwm.reset(new int(r->value.GetInt()));
						}
						// sndhwm
						else if (!strcmp(r->name.GetString(), "sndhwm") && r->value.IsString()) {
							stream.sndhwm.reset(new int(atoi(r->value.GetString())));
						}
						else if (!strcmp(r->name.GetString(), "sndhwm") && r->value.IsInt()) {
							stream.sndhwm.reset(new int(r->value.GetInt()));
						}
						// error
						else {
							throw std::runtime_error("unrecognised config entry: \"" + basic_string(r->name.GetString()) + "\"");
						}
					}
					m8::clog << "\t\tentry address: " << stream.addr << "\n";

					streams.emplace(stream_name, std::move(stream));
				}
			}

			services.emplace(service_name, std::move(streams));
		}
	}

	return services;
}

rapidjson::Document m8::read_config_json(const basic_string& filename) {
	struct stat info;
	int rc = stat(filename.c_str(), &info);
	if (rc == -1)
		throw std::runtime_error("\"" + filename + "\" does not exist");

	std::unique_ptr<char[]> txt(new char[info.st_size + 1]);
	txt[info.st_size] = 0;
	{
		std::ifstream is(filename, std::ios::binary);
		is.read(txt.get(), info.st_size);
	}

	rapidjson::Document doc;
	doc.Parse(txt.get());
	if (!doc.IsObject())
		throw std::runtime_error("\"" + filename + "\" not a valid JSON document");

	return doc;
}
