#include "log.hpp"
#include <iostream>

m8::logstream m8::cout(std::cout);
m8::logstream m8::cerr(std::cerr);
m8::logstream m8::clog(std::clog);

//---------------------------------------------------------------------------
// m8::logstream
m8::logstream::logstream(std::ostream& os, bool enabled) :
	m_os(os),
	m_enabled(enabled)
{
}

#ifndef LOG_INLINE
bool m8::logstream::enable(bool value) {
	return m_enabled = value;
}

bool m8::logstream::enabled() const {
	return m_enabled;
}

m8::logstream& m8::logstream::format(const char* fmt, ...) {
	return *this;
}

m8::logstream& m8::logstream::operator<<(short value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(unsigned short value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(int value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(unsigned int value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(long value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(unsigned long value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(long long value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(unsigned long long value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(float value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(double value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(long double value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

/*
m8::logstream& m8::logstream::operator<<(bool value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}
 */

m8::logstream& m8::logstream::operator<<(const char* value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(const void* value) {
	if (m_enabled) {
		m_os << value;
	}
	return *this;
}

/*
m8::logstream& m8::logstream::operator<<(std::nullptr_t) {
	if (m_enabled)
		m_os << "(null)";
	return *this;
}

m8::logstream& m8::logstream::operator<<(std::basic_streambuf<char, std::char_traits<char>>* sb) {
	if (m_enabled) {
		m_os << sb;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(std::ios_base& (*func)(std::ios_base&)) {
	if (m_enabled) {
		m_os << func;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(std::basic_ios<char, std::char_traits<char>>& (*func)(std::basic_ios<char, std::char_traits<char>>&)) {
	if (m_enabled) {
		m_os << func;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(std::basic_ostream<char, std::char_traits<char>>& (*func)(std::basic_ostream<char, std::char_traits<char>>&)) {
	if (m_enabled) {
		m_os << func;
	}
	return *this;
}

m8::logstream& m8::logstream::operator<<(m8::logstream& (*func)(m8::logstream& os)) {
	if (m_enabled) {
		m_os << func;
	}
	return *this;
}

m8::logstream& endl(m8::logstream& os) {
	os.put(os.widen('\n'));
	os.flush();
	return os;
}
 */
#endif	// LOG_INLINE
