#pragma once
#include "msg_queue.hpp"
#include "config.hpp"

namespace m8 {
	//-----------------------------------------------------------------------
	// service_t
	class service_t {
	public:
		typedef std::map<basic_string, std::unique_ptr<msg_queue_t>> msg_queues_t;

		service_t(zmq::context_t& ctx, const basic_string& name, const stream_config_t& stream_info);

		const basic_string& get_name() const;
		msg_queues_t& get_queues();

	private:
		const basic_string m_name;
		msg_queues_t m_queues;
	};
}
