#pragma once
#include <ostream>

#define PREAMBLE "func=" << __FUNCTION__ << " file=" << __FILE__ << ":" << __LINE__ << " "

namespace m8 {
	//------------------------------------------------------------------------
	// m8::logstream
	class logstream : public std::ostream {
	public:
		typedef std::ostream inherited;

		logstream(std::ostream& os, bool enabled = true);

		bool enable(bool value);
		bool enabled() const;

		logstream& format(const char* fmt, ...);

		logstream& operator<<(short value);
		logstream& operator<<(unsigned short value);
		logstream& operator<<(int value);
		logstream& operator<<(unsigned int value);
		logstream& operator<<(long value);
		logstream& operator<<(unsigned long value);
		logstream& operator<<(long long value);
		logstream& operator<<(unsigned long long value);
		logstream& operator<<(float value);
		logstream& operator<<(double value);
		logstream& operator<<(long double value);
		logstream& operator<<(bool value);
		logstream& operator<<(const char* value);
		logstream& operator<<(const void* value);
/*
		logstream& operator<<(std::nullptr_t);
		logstream& operator<<(std::basic_streambuf<char, std::char_traits<char>>* sb);
		logstream& operator<<(std::ios_base& (*func)(std::ios_base&));
		logstream& operator<<(std::basic_ios<char, std::char_traits<char>>& (*func)(std::basic_ios<char, std::char_traits<char>>&));
		logstream& operator<<(std::basic_ostream<char, std::char_traits<char>>& (*func)(std::basic_ostream<char, std::char_traits<char>>&));
		logstream& operator<<(logstream& (*func)(logstream& os));
 */

	private:
		std::ostream& m_os;
		bool m_enabled;
	};

	extern logstream cout;
	extern logstream cerr;
	extern logstream clog;
}

#ifdef LOG_INLINE
#include "log.inl"
#endif
