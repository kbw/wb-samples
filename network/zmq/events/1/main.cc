#include <zmq.hpp>
#include <string>
#include <iostream>

class server_monitor_t : public zmq::monitor_t
{
public:
	typedef zmq::monitor_t inherited;
	using inherited::inherited;

	void on_monitor_started()									{ std::cout << "started" << std::endl; }
	void on_event_connected(const zmq_event_t&, const char*)	{ std::cout << "connected" << std::endl; }
	void on_event_listening(const zmq_event_t&, const char*)	{ std::cout << "listening" << std::endl; }
	void on_event_accepted(const zmq_event_t&, const char*)		{ std::cout << "accepting" << std::endl; }
	void on_event_closed(const zmq_event_t&, const char*)		{ std::cout << "closed" << std::endl; }
	void on_event_disconnected(const zmq_event_t&, const char*)	{ std::cout << "disconnected" << std::endl; }
};

int main()
try
{
	zmq::context_t ctx(1);

	zmq::socket_t srv(ctx, zmq::socket_type::req);
	std::string srv_addr("tcp://127.0.0.1:5501");

	std::string heartbeat_addr("tcp://127.0.0.1:5501");
	zmq::socket_t heartbeat(ctx, zmq::socket_type::pub);

	zmq_pollitem_t items[] = {
		{ static_cast<void*>(srv), 0, ZMQ_POLLIN, 0 },
		{ static_cast<void*>(heartbeat), 0, ZMQ_POLLOUT, 0 }
	};

	for (bool quit = false; !quit; ) {
		int n = zmq_poll(items, 1, 30);
		switch (n) {
		case 0:
			std::clog << "srv: readable" << std::endl;
			break;
		case 1:
			std::cout << "heartbeat: writable" << std::endl;
			break;
		case -1:
			std::cout << "timout" << std::endl;
			break;
		default:
			std::cout << "unknown: " << n << std::endl;
		}

		quit = true;
	}
}
catch (const std::exception& e)
{
	std::clog << "fatal: " << e.what() << std::endl;
}
