#pragma once

#include <sys/socket.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

#include <cstdint>
#include <cstring>
#include <string_view>
#include <stdexcept>
#include <memory>

namespace tinyplex {
using socket_t = int;
using port_t = std::uint16_t;

using domain_t = decltype(PF_INET); // PF_INET, PF_INET6, ...
using type_t = decltype(SOCK_STREAM); // SOCK_DGRAM, SOCK_STREAM, ...
using protocol_t = decltype(IPPROTO_TCP); // IPPROTO_UDP, IPPROTO_TCP, ...

class Socket {
public:
	struct Info {
		domain_t domain = static_cast<domain_t>(0);
		type_t type = static_cast<type_t>(0);
		protocol_t protocol = static_cast<protocol_t>(0);

		void clear();
	};

	Socket(Info info);
	Socket(socket_t s, Info info) :
		s_(s), info_(info) {
	}

	virtual ~Socket();

	Socket(const Socket& socket) = delete;
	Socket& operator=(const Socket& socket) = delete;

	Socket(Socket&& socket);
	Socket& operator=(Socket&& socket);

	virtual void close();

	virtual void bind(in_addr_t inaddr, port_t port);
	virtual void bind(std::string_view hostAndPort);
	virtual void listen(int backlog);
	virtual std::unique_ptr<Socket> accept(sockaddr* addr = nullptr, socklen_t* addrLen = nullptr);
	virtual void connect(std::string_view hostAndPort);
	virtual ssize_t send(std::string_view payload, int flags = 0);
	virtual ssize_t sendto(std::string_view payload, std::string_view hostAndPort, int flags = 0);
	virtual ssize_t recv(void* buffer, size_t size, int flags = 0);
	virtual ssize_t recvfrom(void* buffer, size_t size, int flags = 0, sockaddr* addr = nullptr, socklen_t* addrLen = nullptr);

	virtual socket_t socket() const { return s_; }
	virtual Info info() const { return info_; }

	static std::unique_ptr<Socket> create(Info info) {
		return std::make_unique<Socket>(info);
	}

	static std::unique_ptr<Socket> create(socket_t s, Info info) {
		return std::make_unique<Socket>(s, info);
	}

private:
	socket_t s_;
	Info info_;
}; // Socket
}  // namespace tinyplex

//----------------------------------------------------------------------------
//
struct Plugin {
	Plugin(tinyplex::Socket::Info info) :
		s(tinyplex::Socket::create({info.domain, info.type, info.protocol})) {
	}

	Plugin(tinyplex::domain_t domain,
		   tinyplex::type_t type,
			tinyplex::protocol_t protocol) :
		s(tinyplex::Socket::create({domain, type, protocol})) {
	}

	Plugin(const Plugin& ) = delete;
	Plugin& operator=(const Plugin& ) = delete;

	virtual ~Plugin() = default;
	virtual tinyplex::socket_t socket() { return s->socket(); }

protected:
	std::unique_ptr<tinyplex::Socket> s;
};

struct ServerPlugin : public Plugin {
	ServerPlugin(tinyplex::Socket::Info info,
				 tinyplex::port_t port) :
		Plugin(info.domain, info.type, info.protocol) {
		bind(INADDR_ANY, port);
	}

	ServerPlugin(tinyplex::domain_t domain,
				 tinyplex::type_t type,
				 tinyplex::protocol_t protocol,
				 tinyplex::port_t port) :
		Plugin(domain, type, protocol) {
		bind(INADDR_ANY, port);
	}

	virtual void bind(in_addr_t inaddr, tinyplex::port_t port) {
		s->bind(inaddr, port);
	}

	virtual void bind(std::string_view hostAndPort) {
		s->bind(hostAndPort);
	}
};

struct ClientPlugin : public Plugin {
	ClientPlugin(tinyplex::Socket::Info info,
				 std::string_view addressStr) :
		Plugin(info.domain, info.type, info.protocol) {
		connect(addressStr);
	}

	ClientPlugin(tinyplex::domain_t domain,
				 tinyplex::type_t type,
				 tinyplex::protocol_t protocol,
				 std::string_view addressStr) :
		Plugin(domain, type, protocol) {
		connect(addressStr);
	}

	virtual void connect(std::string_view hostAndPort) {
		s->connect(hostAndPort);
	}
};

struct TCPServerPlugin : public ServerPlugin {
	TCPServerPlugin(tinyplex::port_t port) :
		ServerPlugin(PF_INET, SOCK_STREAM, IPPROTO_TCP, port) {
		listen(5);
	}

	virtual void listen(int backlog) {
		s->listen(backlog);
	}

	virtual std::unique_ptr<tinyplex::Socket> accept(sockaddr* addr = nullptr, socklen_t* addrLen = nullptr) {
		return s->accept(addr, addrLen);
	}
};

struct UDPServerPlugin : public ServerPlugin {
	UDPServerPlugin(tinyplex::port_t port) :
		ServerPlugin(PF_INET, SOCK_DGRAM, IPPROTO_UDP, port) {
	}

	virtual ssize_t recvfrom(void* buffer, size_t size, int flags = 0, sockaddr* addr = nullptr, socklen_t* addrLen = nullptr) {
		return s->recvfrom(buffer, size, flags, addr, addrLen);
	}

	virtual ssize_t sendto(std::string_view payload, std::string_view hostAndPort, int flags = 0) {
		return s->sendto(payload, hostAndPort, flags);
	}
};

struct TCPClientPlugin : public ClientPlugin {
	TCPClientPlugin(std::string_view addressStr) :
		ClientPlugin(PF_INET, SOCK_STREAM, IPPROTO_TCP, addressStr) {
	}

    virtual ssize_t recv(void* buffer, size_t size, int flags) {
        return s->recv(buffer, size, flags);
    }

	virtual ssize_t send(std::string_view payload, int flags = 0) {
		return s->send(payload, flags);
	}
};

struct UDPClientPlugin : public ClientPlugin {
	UDPClientPlugin(std::string_view addressStr) :
		ClientPlugin(PF_INET, SOCK_DGRAM, IPPROTO_UDP, addressStr) {
	}

	virtual ssize_t recvfrom(void* buffer, size_t size, int flags = 0, sockaddr* addr = nullptr, socklen_t* addrLen = nullptr) {
		return s->recvfrom(buffer, size, flags, addr, addrLen);
	}

	virtual ssize_t sendto(std::string_view payload, std::string_view hostAndPort, int flags = 0) {
		return s->sendto(payload, hostAndPort, flags);
	}
};

namespace tinyplex {
inline
void Socket::Info::clear() {
    domain = static_cast<domain_t>(0);
    type = static_cast<type_t>(0);
    protocol = static_cast<protocol_t>(0);
}

inline
Socket::Socket(Info info) :
	s_(::socket(info.domain, info.type, info.protocol)),
	info_(info) {
}

inline
Socket::~Socket() {
	close();
}

inline
Socket::Socket(Socket&& socket) :
    s_(socket.s_), info_(socket.info_) {
    socket.s_ = -1;
    socket.info_.clear();
}

inline
Socket& Socket::operator=(Socket&& socket) {
    if (this != &socket) {
        close();
        s_ = socket.s_;
        info_ = socket.info_;
        socket.s_ = -1;
        socket.info_.clear();
    }
    return *this;
}

inline
void Socket::close() {
	if (s_ != -1) {
		::close(s_);
        s_ = -1;
		info_.clear();
	}
}

inline
void Socket::bind(in_addr_t inaddr, port_t port) {
	sockaddr_in addr;
	std::memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons( port );
	addr.sin_addr.s_addr = inaddr;

	::bind(socket(), (sockaddr*)&addr, sizeof(addr));
}

inline
void Socket::bind(std::string_view hostAndPort) {
	char host[128];
	std::memset(host, 0, sizeof(host));
	const char* separator = std::strchr(hostAndPort.data(), ':');
	if (!separator)
		throw std::runtime_error("eek!");
	std::strncpy(host, hostAndPort.data(), separator - hostAndPort.data());

	sockaddr_in addr;
	std::memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons( (port_t)std::atoi(separator + 1) );  // assumed to be null terminated
	addr.sin_addr.s_addr = inet_addr(host);

	::bind(socket(), (sockaddr*)&addr, sizeof(addr));
}

inline
void Socket::listen(int backlog) {
	::listen(socket(), backlog);
}

inline
std::unique_ptr<Socket> Socket::accept(sockaddr* addr, socklen_t* addrLen) {
	socket_t s = ::accept(socket(), addr, addrLen);
	if (s == -1)
		throw std::runtime_error("eek!");
	return create(s, info());
}

inline
void Socket::connect(std::string_view hostAndPort) {
	char host[128];
	std::memset(host, 0, sizeof(host));
	const char* separator = std::strchr(hostAndPort.data(), ':');
	if (!separator)
		throw std::runtime_error("eek!");
	std::strncpy(host, hostAndPort.data(), separator - hostAndPort.data());

	sockaddr_in addr;
	std::memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons( (port_t)std::atoi(separator + 1) );  // assumed to be null terminated
	addr.sin_addr.s_addr = inet_addr(host);

	::connect(socket(), (sockaddr*)&addr, sizeof(addr));
}

inline
ssize_t Socket::send(std::string_view payload, int flags) {
	return ::send(socket(), payload.data(), payload.size(), flags);
}

inline
ssize_t Socket::sendto(std::string_view payload, std::string_view hostAndPort, int flags) {
	char host[128];
	std::memset(host, 0, sizeof(host));
	const char* separator = std::strchr(hostAndPort.data(), ':');
	if (!separator)
		throw std::runtime_error("eek!");
	std::strncpy(host, hostAndPort.data(), separator - hostAndPort.data());

	sockaddr_in addr;
	std::memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons( (port_t)std::atoi(separator + 1) );  // assumed to be null terminated
	addr.sin_addr.s_addr = inet_addr(host);

	return ::sendto(socket(), payload.data(), payload.size(), 0, (sockaddr*)&addr, sizeof(addr));
}

inline
ssize_t Socket::recv(void* buffer, size_t size, int flags) {
	return ::recv(socket(), buffer, size, flags);
}

inline
ssize_t Socket::recvfrom(void* buffer, size_t size, int flags, sockaddr* addr, socklen_t* addrLen) {
	return ::recvfrom(socket(), buffer, size, flags, addr, addrLen);
}
}  // namespace tinyplex
