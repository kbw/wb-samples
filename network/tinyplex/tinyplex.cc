#include "tinyplex.hpp"

#include <iostream>
#include <vector>

#include <assert.h>

//  ---------------------         ----------------
//  | adminStreamServer |-accept->| adminClients |
//  ---------------------         ----------------
//            ^                           ^
//            |                           |
//         connect                    send/recv
//  ---------------------                 |
//  | adminStreamClient |<----------------*
//  ---------------------

tinyplex::port_t defaultPort = 2022;
std::string_view defaultAddr = "127.0.0.1:2022";

void onIdle(void* ctx = nullptr) {
	std::cout << "onIdle" << '\n';
}

int main(int argc, char* argv[]) {
	// servers
	auto adminMsgServer = std::make_unique<UDPServerPlugin>(defaultPort);
	auto adminStreamServer = std::make_unique<TCPServerPlugin>(defaultPort);

	// clients
	auto adminStreamClient = std::make_unique<TCPClientPlugin>(defaultAddr);
	std::cout << "init: adminStreamClient->connect" << '\n';

	auto adminMsgClient = std::make_unique<UDPClientPlugin>(defaultAddr);
	adminMsgClient->sendto("admin client: hello", defaultAddr);
	std::cout << "init: adminMsgClient->sendto: \"admin client: hello\"" << '\n';

	// run
	fd_set rdset, wrset, errset;
	char addrClient[2 * sizeof(sockaddr_in6)]; // large enough client address?
	constexpr std::size_t buffersz = 16*1024; // buffer must be large enough, udp drops excess
	auto buffer = std::make_unique<char[]>(buffersz);
	std::vector<std::unique_ptr<tinyplex::Socket>> adminClients;

	for (int selects = 0; selects < 4; ++selects) {
		FD_ZERO(&rdset);
		FD_ZERO(&wrset);
		FD_ZERO(&errset);

		FD_SET(adminMsgServer->socket(), &rdset);
		FD_SET(adminStreamServer->socket(), &rdset);
		tinyplex::socket_t mxSocket = std::max(adminMsgServer->socket(), adminStreamServer->socket());
		for (std::size_t i = 0; i != adminClients.size(); ++i) {
			FD_SET(adminClients[i]->socket(), &rdset);
			mxSocket = std::max(mxSocket, adminClients[i]->socket());
		}

		timeval timeout{0, 1000};
		auto selectRet = ::select(mxSocket + 1, &rdset, &wrset, &errset, &timeout);
		std::cout << "\nselect=" << selectRet << " adminClients.size=" << adminClients.size() << '\n';
		if (selectRet == 0) {
			onIdle();
		}

		int nAdminClient{};
		for (auto p = adminClients.begin(); p != adminClients.end(); ++nAdminClient) {
			auto& adminClient = *p;

			if (FD_ISSET(adminClient->socket(), &rdset)) {
		        std::cout << "handler: adminClient[" << nAdminClient << "]\n";
				ssize_t n = adminClient->recv(buffer.get(), buffersz, 0);
				if (n != -1)
					buffer.get()[n] = '\0';
				std::cout << "handler: adminClient: read: " << buffer.get() << '\n';

				if (std::strncmp(buffer.get(), "hello", 5) == 0) {
					adminClient->send({"bye", 3}, 0);
//					p = adminClients.erase(p);
					++p;
				} else
					++p;
			} else
				++p;
		}
		if (FD_ISSET(adminStreamClient->socket(), &rdset)) {
			std::cout << "handler: adminStreamClient\n";
			ssize_t n = adminStreamClient->recv(buffer.get(), buffersz, 0);
			if (n != -1)
				buffer.get()[n] = '\0';
			std::cout << "handler: adminStreamClient: read: " << buffer.get() << '\n';
		}
		if (FD_ISSET(adminMsgServer->socket(), &rdset)) {
			std::cout << "handler: adminMsgServer\n";
			std::memset(addrClient, 0, sizeof(addrClient));
			socklen_t addrLen = sizeof(addrClient);
			ssize_t n = adminMsgServer->recvfrom(buffer.get(), buffersz, 0, (sockaddr*)&addrClient, &addrLen);
			if (n != -1) {
//				std::cout << "n:" << n << " addrLen:" << addrLen << " sizeof(sockaddr_in):" << sizeof(sockaddr_in) << '\n';
				assert(addrLen == sizeof(sockaddr_in));
				buffer.get()[n] = '\0';
				std::cout << "handler: adminMsgServer: read: " << buffer.get() << '\n';
			}
		}
		if (FD_ISSET(adminStreamServer->socket(), &rdset)) {
			std::cout << "handler: adminStreamServer\n";
			// accept connection
			socklen_t addrLen = sizeof(addrClient);
			auto s = adminStreamServer->accept((sockaddr*)&addrClient, &addrLen);
			assert(addrLen == sizeof(sockaddr_in));
			std::cout << "handler: adminStreamServer->accept:" << '\n';

			adminClients.emplace_back( std::move(s) );
			adminStreamClient->send("adminStreamClient: hello");
			std::cout << "handler: adminStreamClient->send: adminStreamClient: hello" << '\n';
		}
	}
}
