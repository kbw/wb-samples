#include <unistd.h>
#include <strings.h>
#include <pthread.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct client_info {
	int s;
	struct sockaddr_in addr;
	socklen_t addrlen;
	int shutdown_delay;
};

void* client_handler(void* param);

int main(int argc, char* argv[]) {
	int rc;
	uint16_t port = (argc > 1) ? atoi(argv[1]) : 2345;
 
	int s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); 

	struct sockaddr_in addr;
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(port);

	rc = bind(s, (struct sockaddr*)&addr, sizeof(addr));
	if (rc == -1) {
		fprintf(stderr, "fatal: %s failed, fd=%d, errno=%d, err=%s\n", "bind", s, errno, strerror(errno));
		close(s); s = -1;
		exit(1);
	}

	rc = listen(s, 5);
	if (rc == -1) {
		fprintf(stderr, "fatal: %s failed, fd=%d, errno=%d, err=%s\n", "listen", s, errno, strerror(errno));
		close(s); s = -1;
		exit(1);
	}

	while (1) {
		struct client_info* info = malloc(sizeof(struct client_info));
		bzero(info, sizeof(struct client_info));
		info->shutdown_delay = 1;

		info->s = accept(s, (struct sockaddr*)&info->addr, &info->addrlen);
		fprintf(stderr, "client fd=%d\n", info->s);

		pthread_t tid;
		pthread_create(&tid, NULL, client_handler, info);
		pthread_detach(tid);
	}
	close(s); 
}

void* client_handler(void* param) {
	struct client_info* info = (struct client_info*)param;

	char msg[] = "HELLO: Welcome to the service";
	ssize_t nbytes = send(info->s, msg, strlen(msg), 0);
	if (nbytes == -1) {
		fprintf(stderr, "fatal: %s failed, fd=%d, errno=%d, err=%s\n", "listen", info->s, errno, strerror(errno));
		goto done;
	}
	sleep(info->shutdown_delay);

done:
	fprintf(stderr, "close fd=%d\n", info->s);
	close(info->s);
	free(info);
	return NULL;
}
