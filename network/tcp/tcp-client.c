#include <unistd.h>
#include <strings.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSZ (1024 * 4)

int main(int argc, char* argv[]) {
	int rc;
	const char* host = (argc > 1) ? argv[1] : "localhost";
	uint16_t port = (argc > 2) ? atoi(argv[2]) : 2345;
 
	int s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); 

	struct sockaddr_in addr;
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(host);
	addr.sin_port = htons(port);

	rc = connect(s, (struct sockaddr*)&addr, sizeof(addr));
	if (rc == -1) {
		fprintf(stderr, "fatal: %s failed, errno=%d, err=%s\n", "connect", errno, strerror(errno));
		close(s); s = -1;
		exit(1);
	}

	while (1) {
		char buf[BUFSZ];
		ssize_t nbytes = recv(s, buf, sizeof(buf), 0);
		if (nbytes == -1) {
			fprintf(stderr, "fatal: %s failed, errno=%d, err=%s\n", "send", errno, strerror(errno));
			break;
		}
		if (nbytes == 0) {
			break;
		}
		buf[nbytes] = '\0';

		printf("%s\n", buf);
		break;
	}
	shutdown(s, SHUT_WR);
	close(s); s = -1;
	exit(1);
}
