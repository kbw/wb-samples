#include "echo2.hpp"
#include "echo2-server.hpp"
#include <unistd.h>
#include <signal.h>
#include <stdio.h>

static volatile bool destroy_flag;

static void INT_HANDLER(int signo)
{
	destroy_flag = true;
}

int main()
{
	// register the signal SIGINT handler
	struct sigaction act;
	act.sa_handler = INT_HANDLER;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaction(SIGINT, &act, 0);

	// create libwebsocket context
	echo2_server::static_info rec;
	struct lws_context *context = lws_create_context(&rec.get_info());
	if (context == NULL)
	{
		printf("%s[server: Main] Websocket context create error.\n%s", KRED, RESET);
		return -1;
	}
	printf("%s[server: Main] Websocket context create success.\n%s", KGRN, RESET);

	// websocket service game loop
	while ( !destroy_flag )
		lws_service(context, 50);

	usleep(10);
	lws_context_destroy(context);

	return 0;
}
