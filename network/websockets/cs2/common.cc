#include "common.hpp"
#include <stdio.h>
#include <string.h>

// websocket_write_back:
//	write the string data to the destination wsi.
int write(struct lws *wsi, const char *str, int str_size)
{
	if (str == NULL || wsi == NULL)
		return -1;

	const int len = (str_size < 1) ? strlen(str) : str_size;

	if (unsigned char *out = new unsigned char[LWS_SEND_BUFFER_PRE_PADDING + len + LWS_SEND_BUFFER_POST_PADDING])
	{
		memcpy(&out[LWS_SEND_BUFFER_PRE_PADDING], str, len);
		int n = lws_write(
					wsi,
					&out[LWS_SEND_BUFFER_PRE_PADDING], len,
					LWS_WRITE_TEXT);
		printf("%s[server: websocket_write_back] %s\n%s",
			KBLU, str, RESET);

		delete [] out;
		return n;
	}

	return -1;
}
