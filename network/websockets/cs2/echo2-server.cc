#include "echo2-server.hpp"
#include "echo2.hpp"
#include <map>
#include <stdio.h>
#include <string.h>

namespace echo2_server
{
	class session
	{
	public:
		session(lws* wsi);
		virtual ~session() = default;
		virtual void OnCreate();
		virtual void OnDestroy();
		virtual void OnClientConnect();
		virtual void OnClientDisconnect();
		virtual void OnClientReceive(const char* str, int len);
	
	private:
		lws* m_wsi;
	};

	struct session_mgr
	{
	public:
		typedef std::map<lws*, session*> session_map;

		session_mgr() = default;
		~session_mgr() = default;
		session_mgr(const session_mgr& n) = delete;
		session_mgr& operator=(const session_mgr& n) = delete;

		session_map& getMapping() { return map; }

	private:
		session_map map;
	};

	session::session(lws* wsi) :
		m_wsi(wsi)
	{
	}

	void session::OnCreate()
	{
	}

	void session::OnDestroy()
	{
	}

	void session::OnClientConnect()
	{
	}

	void session::OnClientDisconnect()
	{
	}

	void session::OnClientReceive(const char* str, int len)
	{
		write(m_wsi, str, len);
	}

	session_mgr* mgr;

	int callback(
			 struct lws *wsi,
			 enum lws_callback_reasons reason,
			 void *user, void *in, size_t len)
	{
		switch (reason)
		{
		case LWS_CALLBACK_PROTOCOL_INIT:
			printf("%s[server: callback] PROTOCOL_INIT: user=%p in=%p len=%u\n%s", KYEL, user, in, len, RESET);
			mgr = new session_mgr;
			if (lws_protocol_vhost_options* p = (lws_protocol_vhost_options*)in)
			{ 
				printf("%svhost init: %s=%s\n%s",
					KYEL,
					(p->name  ? p->name  : ""),
					(p->value ? p->value : ""),
					RESET);
			}
			break;
		case LWS_CALLBACK_PROTOCOL_DESTROY:
			printf("%s[server: callback] PROTOCOL_DESTROY\n%s",
				KYEL, RESET);
			delete mgr;
			break;

		case LWS_CALLBACK_WSI_CREATE:
			printf("%s[server: callback] WSI_CREATE\n%s",
				KYEL, RESET);
			{
				auto ret = mgr->getMapping().insert(
								std::make_pair(
									wsi, new session(wsi)));
				ret.first->second->OnCreate();
				printf("%s[server: callback] session created\n%s",
					KYEL, RESET);
			}
			break;
		case LWS_CALLBACK_WSI_DESTROY:
			printf("%s[server: callback] WSI_DESTROY\n%s",
				KYEL, RESET);
			{
				auto p = mgr->getMapping().find(wsi);
				if (p != mgr->getMapping().end())
				{
					p->second->OnDestroy();
					delete p->second, p->second = nullptr;
					mgr->getMapping().erase(p);
					printf("%s[server: callback] session destroyed\n%s",
						KYEL, RESET);
				}
			}
			break;

		case LWS_CALLBACK_ESTABLISHED:
			printf("%s[server: callback] ESTABLISHED\n%s",
				KYEL, RESET);
			{
				auto p = mgr->getMapping().find(wsi);
				if (p != mgr->getMapping().end())
					p->second->OnClientConnect();
			}
			break;

		// If receive a data from client
		case LWS_CALLBACK_RECEIVE:
			printf("%s[server: callback] RECEIVE: %s\n%s",
				KYEL, (char *)in, RESET);
			{
				auto p = mgr->getMapping().find(wsi);
				if (p != mgr->getMapping().end())
					p->second->OnClientReceive((const char*)in, len);
			}
			break;

		case LWS_CALLBACK_CLOSED:
			printf("%s[server: callback] CLOSED\n%s", KYEL, RESET);
			{
				auto p = mgr->getMapping().find(wsi);
				if (p != mgr->getMapping().end())
					p->second->OnClientDisconnect();
			}
			break;

		default:
			;
		}

		return 0;
	}

	static_info::static_info(
			// args pack for websocket context info
			// server url will usd port 5000
			int port,
			const char *interface,
			// Not using ssl
			const char *cert_path,
			const char *key_path,
			// no special options
			int opts)
	{
		// setup websocket protocol
		memset(&protocol, 0, sizeof(protocol));
		protocol.name = "echo2";
		protocol.callback = callback;
		protocol.per_session_data_size = sizeof(session_data);

		// setup websocket context info
		memset(&info, 0, sizeof info);
		info.port = port;
		info.iface = interface;
		info.protocols = &protocol;
//		info.extensions = lws_get_internal_extensions();
		info.ssl_cert_filepath = cert_path;
		info.ssl_private_key_filepath = key_path;
		info.gid = -1;
		info.uid = -1;
		info.options = opts;
	}
}
