#include <unistd.h>
#include "common.hpp"
#include "echo2.hpp"
#include <signal.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static volatile bool destroy_flag;	// set in interrupt handler
static volatile bool connection_flag;// signals across threads
static bool writeable_flag;			// signals across states within same thread -- implements 2 msg only

static void INT_HANDLER(int signo)
{
	destroy_flag = true;
}

struct pthread_routine_tool
{
	struct lws_context *context;
	struct lws *wsi;
};

static int echo2_callback(
						 struct lws *wsi,
						 enum lws_callback_reasons reason, void *user,
						 void *in, size_t len)
{
	switch (reason)
	{
	case LWS_CALLBACK_CLIENT_ESTABLISHED:
		printf("%s[client: Main Service] CLIENT_ESTABLISHED\n%s", KYEL, RESET);
		connection_flag = true;
		break;

	case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
		printf("%s[client: Main Service] CLIENT_CONNECTION_ERROR\n%s", KRED, RESET);
		destroy_flag = true;
		connection_flag = false;
		break;

	case LWS_CALLBACK_CLOSED:
		printf("%s[client: Main Service] CALLBACK_CLOSED\n%s", KYEL, RESET);
		destroy_flag = true;
		connection_flag = false;
		break;

	case LWS_CALLBACK_CLIENT_RECEIVE:
		printf("%s[client: Main Service] CLIENT_RECEIVE: %s\n%s", KYEL, (char *)in, RESET);
		if (writeable_flag)
			destroy_flag = true;
		break;

	case LWS_CALLBACK_CLIENT_WRITEABLE:
		printf("%s[client: Main Service] CLIENT_WRITEABLE\n%s", KYEL, RESET);
		write(wsi, "Byebye! See you later", -1);
		writeable_flag = true;
		break;

	default:
		break;
	}

	return 0;
}

static void *pthread_routine(void *tool_in)
{
	// connection & vhost
	struct pthread_routine_tool *tool = (pthread_routine_tool*)tool_in;

	printf("%s[client: pthread_routine] Good day. This is pthread_routine.\n%s", KBRN, RESET);

	// waiting for connection with server done
	while (!connection_flag)
		usleep(1000*20);

	// Send greeting to server
	printf("%s[client: pthread_routine] Server is ready. send a greeting message to server.\n%s", KBRN, RESET); 
	write(tool->wsi, "Good day", -1);

	printf("%s[client: pthread_routine] sleep 2 seconds then call onWritable\n%s", KBRN, RESET);
	sleep(1);
	printf("%s-client: -----------------------------------------------------\n%s", KBRN, RESET);
	sleep(1);
	printf("%s[client: pthread_routine] sleep 2 seconds then call onWritable\n%s", KBRN, RESET);

	// involked writable
	// Request a callback when this socket becomes able to be written to without blocking
	// The callback just passed the connection
	printf("%s[client: pthread_routine] call on writable.\n%s", KBRN, RESET);   
	lws_callback_on_writable(tool->wsi);

	printf("%s[client: pthread_routine] Thread terminating\n%s", KBRN, RESET);   
	return NULL;
}

int main()
{
	// register the signal SIGINT handler
	struct sigaction act;
	act.sa_handler = INT_HANDLER;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaction(SIGINT, &act, 0);

	struct lws_protocols protocol;
	memset(&protocol, 0, sizeof(protocol));
	protocol.name = "echo2";
	protocol.callback = &echo2_callback;
	protocol.per_session_data_size = sizeof(struct session_data);

	struct lws_context_creation_info info;
	memset(&info, 0, sizeof info);
	info.port = CONTEXT_PORT_NO_LISTEN;
	info.iface = NULL;
	info.protocols = &protocol;
	info.ssl_cert_filepath = NULL;
	info.ssl_private_key_filepath = NULL;
//	info.extensions = lws_get_internal_extensions();
	info.gid = -1;
	info.uid = -1;
	info.options = 0;

	// create vhost
	struct lws_context *context = lws_create_context(&info);
	if (!context)
	{
		printf("%s[client: Main] context is NULL.\n%s", KRED, RESET);
		return -1;
	}
	printf("%s[client: Main] context created.\n%s", KGRN, RESET);

	// connect to server
	struct lws *wsi = lws_client_connect(
						context,
						"localhost", 5000, 0, "/",
						"localhost:5000", NULL, protocol.name, -1);
	if (!wsi)
	{
		printf("%s[client: Main] wsi create error.\n%s", KRED, RESET);
		return -1;
	}
	printf("%s[client: Main] wsi create success.\n%s", KGRN, RESET);

	struct pthread_routine_tool tool;
	tool.wsi = wsi;			// connection
	tool.context = context;	// vhost

	pthread_t pid;
	pthread_create(&pid, NULL, pthread_routine, &tool);
	pthread_detach(pid);

	// response service game loop
	while (!destroy_flag)
		lws_service(context, 50);

	lws_context_destroy(context);

	return 0;
}
