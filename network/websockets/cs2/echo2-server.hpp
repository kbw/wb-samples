#pragma once
#include "common.hpp"

namespace echo2_server
{
	struct static_info
	{
	public:
		static_info(
			// args pack for websocket context info
			// server url will usd port 5000
			int port = 5000,
			const char *interface = NULL,
			// Not using ssl
			const char *cert_path = NULL,
			const char *key_path = NULL,
			// no special options
			int opts = 0);

		lws_protocols& get_protocol() { return protocol; }
		lws_context_creation_info& get_info() { return info; }

	private:
		struct lws_protocols protocol;
		struct lws_context_creation_info info;
	};
}
