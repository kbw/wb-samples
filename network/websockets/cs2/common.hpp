#pragma once
#include <libwebsockets.h>

#define KGRN "\033[0;32;32m"
#define KRED "\033[0;32;31m"
#define KBLU "\033[0;32;34m"
#define KYEL "\033[1;33m"
#define KBRN "\033[0;33m"
#define KMAG "\033[0;35m"
#define KCYN "\033[0;36m"
#define KCYN_L "\033[1;36m"
#define RESET "\033[0m"

// websocket_write_back:
//	write the string data to the destination wsi.
int write(struct lws *wsi, const char *str, int str_size);
