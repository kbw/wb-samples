#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <libwebsockets.h>

#define KGRN "\033[0;32;32m"
#define KCYN "\033[0;36m"
#define KRED "\033[0;32;31m"
#define KYEL "\033[1;33m"
#define KMAG "\033[0;35m"
#define KBLU "\033[0;32;34m"
#define KCYN_L "\033[1;36m"
#define RESET "\033[0m"

// Server:	The main running object. It manages the "Game Loop" that
//			runs each VHost.
//
// VHost:	A server's virtual host. It is a WebSocket's endpoint, a
//			connection class. It implements a WebSockets Protocol.
//			When clients connect, they connect to a VHost.
//
// Session:	This is a running client. It's an instance of a VHost. It
//			is a client connection, a running proto instance.

namespace ka
{
	namespace websockets
	{
		class Server
		{
		public:
			Server();

			void Add(VHost* vhost);
			bool Remove(VHost* vhost);

			void Run();
		};

		class VHost
		{
		public:
			VHost();
			~VHost();

		private:
			static int callback(
						 struct lws *wsi,
						 enum lws_callback_reasons reason,
						 void *user, void *in, size_t len);

		private:
			lws_context* m_ctx;	// WebSockets Context
		};

		class Session
		{
		public:
			virtual ~Session();
	
			virtual	void OnClientConnect(lws* wsi);
			virtual	void OnClientDisconnect(lws* wsi);
			virtual	void OnReceive(lws* wsi, const char* str, int len);

			virtual int write(const char *str, int str_size_in);

			static Session* Create();

		private:
			Session();

		private:
			lws* m_wsi;		// WebSockets Instance
		};
	}
}


namespace ka
{
	namespace websockets
	{
		int VHost::callback(
				 struct lws *wsi,
				 enum lws_callback_reasons reason,
				 void *user, void *in, size_t len)
		{
			switch (reason)
			{
			case LWS_CREATE:
				// new wsi created
				break;

			case LWS_DESTROY:
				// destroy
				break;

			case LWS_CALLBACK_ESTABLISHED:
				OnClientConnect(wsi);
				break;

			case LWS_CALLBACK_CLOSED:
				OnClientDisconnect(wsi);
				break;

			case LWS_CALLBACK_RECEIVE:
				OnReceive(wsi, (const char*)in, len);
				break;
			}
		}

		int VHost::write(lws* wsi, const char *str, int str_size_in) 
		{
			if (str == NULL || wsi == NULL)
				return -1;

			int len = (str_size_in < 1) 
						? strlen(str)
						: str_size_in;

			char *out = (char *)malloc(
								sizeof(char) *
								(LWS_SEND_BUFFER_PRE_PADDING + 
								 len +
								 LWS_SEND_BUFFER_POST_PADDING));
			strncpy(out + LWS_SEND_BUFFER_PRE_PADDING, str, len);

			int n  = lws_write(
						wsi,
						out + LWS_SEND_BUFFER_PRE_PADDING,
						len,
						LWS_WRITE_TEXT);	// protocol

			free(out);
			return n;
		}

		VHost* VHost::Create(lws_context_creation_info& info)
		{
			// create vhost
			if (lws_context *ctx = lws_create_context(&info))
			{
				return new VHost(ctx);
			}

			return nullptr;
		}
	}
}


static volatile int destroy_flag = 0;

static void INT_HANDLER(int signo)
{
	destroy_flag = 1;
	lws_cancel_service(ctx);
}

/* *
 * websocket_write_back: write the string data to the destination wsi.
 */
int websocket_write_back(struct lws *wsi_in, char *str, int str_size_in) 
{
	if (str == NULL || wsi_in == NULL)
		return -1;

	int len;
	if (str_size_in < 1) 
		len = strlen(str);
	else
		len = str_size_in;

	char *out = NULL;
	out = (char *)malloc(sizeof(char)*(LWS_SEND_BUFFER_PRE_PADDING + len + LWS_SEND_BUFFER_POST_PADDING));

	//* setup the buffer */
	memcpy(out + LWS_SEND_BUFFER_PRE_PADDING, str, len);

	//* write out */
	int n;
	n = lws_write(wsi_in, out + LWS_SEND_BUFFER_PRE_PADDING, len, LWS_WRITE_TEXT);
	printf(KBLU"[server: websocket_write_back] %s\n"RESET, str);

	//* free the buffer */
	free(out);

	return n;
}

static int ws_service_callback(
						 struct lws *wsi,
						 enum lws_callback_reasons reason, void *user,
						 void *in, size_t len)
{
	switch (reason)
	{
	case LWS_CALLBACK_ESTABLISHED:
		printf(KYEL"[server: Main Service] LWS_ESTABLISHED\n"RESET);
		break;

	//* If receive a data from client */
	case LWS_CALLBACK_RECEIVE:
		printf(KCYN_L"[server: Main Service] LWS_RECEIVE: %s\n"RESET, (char *)in);
		//* echo back to client */
		websocket_write_back(wsi, (char *)in, -1);
		break;

	case LWS_CALLBACK_CLOSED:
		printf(KYEL"[server: Main Service] LWS_CLOSED\n"RESET);
		break;

	default:
//		printf(KYEL"[server: Main Service] reason=%d\n"RESET, reason);
		break;
	}

	return 0;
}

struct per_session_data
{
	int fd;
};

int main(void)
{
	//* args pack for websocket context info */
	// server url will usd port 5000
	int port = 5000;
	const char *interface = NULL;
	// Not using ssl
	const char *cert_path = NULL;
	const char *key_path = NULL;
	// no special options
	int opts = 0;


	//* register the signal SIGINT handler */
	struct sigaction act;
	act.sa_handler = INT_HANDLER;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaction(SIGINT, &act, 0);

	//* setup websocket protocol */
	struct lws_protocols protocol;
	protocol.name = "my-echo-protocol";
	protocol.callback = ws_service_callback;
	protocol.per_session_data_size = sizeof(struct per_session_data);
	protocol.rx_buffer_size = 0;

	//* setup websocket context info */
	struct lws_context_creation_info info;
	memset(&info, 0, sizeof info);
	info.port = port;
	info.iface = interface;
	info.protocols = &protocol;
	info.extensions = lws_get_internal_extensions();
	info.ssl_cert_filepath = cert_path;
	info.ssl_private_key_filepath = key_path;
	info.gid = -1;
	info.uid = -1;
	info.options = opts;

	//* create libwebsocket context. */
	struct lws_context *context;
	context = lws_create_context(&info);	// create vhost
	if (context == NULL)
	{
		printf(KRED"[server: Main] Websocket context create error.\n"RESET);
		return -1;
	}
	printf(KGRN"[server: Main] Websocket context create success.\n"RESET);

	//* websocket service */
	while ( !destroy_flag )
		lws_service(context, 50);

	usleep(10);
	lws_context_destroy(context);

	return 0;
}
