#pragma once

#include "defs.hh"

#include <unistd.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include <stdexcept>
#include <string>
#include <vector>

//----------------------------------------------------------------------------

struct errno_error : public std::runtime_error {
	errno_error(const char* op) : std::runtime_error(make_msg(op)) {}

private:
	static std::string make_msg(const char* op) {
		char buffer[96];
#if (defined(__USE_XOPEN2K) && !defined(__USE_GNU)) || \
    (defined(__ANDROID_API__) && defined(__USE_GNU) && __ANDROID_API__ < 23) || \
    (defined(__POSIX_VISIBLE) && __POSIX_VISIBLE >= 200112)
		int len = snprintf(buffer, sizeof(buffer), "%s:%d ", op, errno);
		strerror_r(errno, buffer + len, sizeof(buffer) - len);
		return buffer;
#else
		char* str = strerror_r(errno, buffer, sizeof(buffer));
		int len = snprintf(buffer, sizeof(buffer), "%s: %d %s", op, errno, str);
		if (unlikely(len == -1))
			return std::to_string(errno);
		return { buffer, static_cast<size_t>(len) };
#endif
	}
};

//----------------------------------------------------------------------------

struct ip4_address : public sockaddr_in {
	static constexpr int protocol = PF_INET;
	static constexpr int family = AF_INET;

	ip4_address() {
		sin_family = family;
		sin_addr.s_addr = INADDR_ANY;
		sin_port = 0;
	}
	ip4_address(uint16_t port) {
		sin_family = family;
		sin_addr.s_addr = INADDR_ANY;
		sin_port = htons(port);
	}
	ip4_address(const std::string& ipaddr, uint16_t port) {
		sin_family = family;
		sin_addr.s_addr = ::inet_addr(ipaddr.c_str());
		sin_port = htons(port);
	}
};

struct ip6_address : public sockaddr_in6 {
	static constexpr int protocol = PF_INET6;
	static constexpr int family = AF_INET6;

	ip6_address() {
		sin6_family = family;
		sin6_addr = in6addr_any;
		sin6_port = 0;
	}
	ip6_address(uint16_t port) {
		sin6_family = family;
		sin6_addr = in6addr_any;
		sin6_port = htons(port);
	}
	ip6_address(const std::string& ipaddr, uint16_t port) {
		sin6_family = family;
		inet_pton(family, ipaddr.c_str(), &sin6_addr);
		sin6_port = htons(port);
	}
};

//----------------------------------------------------------------------------

template <typename IMPL>
struct socket_base {
	void close()	{ IMPL::close(); }
};

template <typename IMPL, typename ADDR>
struct socket_server_base {
	void bind(const ADDR& addr)	{ IMPL::bind(addr); } 
	void listen(int n)			{ IMPL::listen(n); }
};

template <typename IMPL>
struct socket_client_base {
	void connect()	{ IMPL::connect(); }
};

template <typename IMPL>
struct socket_tcp_base {
	void send(const std::vector<char>& buf)	{ IMPL::send(buf); }
	void recv(std::vector<char>& buf)		{ IMPL::recv(buf); }
};

template <typename IMPL, typename ADDR>
struct socket_udp_base {
	void sendto(const std::vector<char>& buf, const ADDR& addr)	{ IMPL::send(buf, addr); }
	void recvfrom(std::vector<char>& buf, ADDR& addr)			{ IMPL::recv(buf, addr); }
};

//----------------------------------------------------------------------------

struct socket_server4 :	public socket_base<socket_server4>,
						public socket_server_base<socket_server4, ip4_address>,
						public socket_tcp_base<socket_server4> {
public:
	socket_server4(int s = -1) : s_(s) {}
	~socket_server4() { close(); }

	socket_server4(const ip4_address& addr) {
		s_ = ::socket(ip4_address::protocol, SOCK_STREAM, IPPROTO_TCP);
		if (s_ == -1)
			throw errno_error("socket");

		bind(addr);
	}

	socket_server4(const socket_server4 &) = delete;
	socket_server4& operator=(const socket_server4 &) = delete;

	socket_server4(socket_server4&& n) : s_(n.s_) { n.s_ = -1; }
	socket_server4& operator=(socket_server4&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}

	void close() {
		if (s_ != -1)
			::close(s_);
	}

	void bind(const ip4_address& addr) {
		int ret = ::bind(s_, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
		if (ret == -1)
			throw errno_error("bind");
	}

	void listen(int n) {
		int ret = ::listen(s_, n);
		if (ret == -1)
			throw errno_error("listen");
	}

	void send(const std::vector<char>& buf)	{
		int nsent = 0;
		while (nsent < buf.size()) {
			int nbytes = ::send(s_, &buf.front() + nsent, buf.size() - nsent, 0);
			if (nbytes == -1)
				throw errno_error("send");

			nsent += nbytes;
		}
	}

	void recv(std::vector<char>& buf) {
		int nreceived = 0;
		while (nreceived < buf.size()) {
			int nbytes = ::recv(s_, &buf.front() + nreceived, buf.size() - nreceived, 0);
			if (nbytes == -1)
				throw errno_error("recv");

			nreceived += nbytes;
		}
	}

private:
	int s_ = -1;
};

//----------------------------------------------------------------------------

struct socket_server6 :	public socket_base<socket_server6>,
						public socket_server_base<socket_server6, ip6_address>,
						public socket_tcp_base<socket_server6> {
public:
	socket_server6(int s = -1) : s_(s) {}
	~socket_server6() { close(); }

	socket_server6(const ip6_address& addr) {
		s_ = ::socket(ip6_address::protocol, SOCK_STREAM, IPPROTO_TCP);
		if (s_ == -1)
			throw errno_error("socket");

		bind(addr);
	}

	socket_server6(const socket_server6 &) = delete;
	socket_server6& operator=(const socket_server6 &) = delete;

	socket_server6(socket_server6&& n) : s_(n.s_) { n.s_ = -1; }
	socket_server6& operator=(socket_server6&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}

	void close() {
		if (s_ != -1)
			::close(s_);
	}

	void bind(const ip6_address& addr) {
		int ret = ::bind(s_, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
		if (ret == -1)
			throw errno_error("bind");
	}

	void listen(int n) {
		int ret = ::listen(s_, n);
		if (ret == -1)
			throw errno_error("listen");
	}

	void send(const std::vector<char>& buf)	{
		int nsent = 0;
		while (nsent < buf.size()) {
			int nbytes = ::send(s_, &buf.front() + nsent, buf.size() - nsent, 0);
			if (nbytes == -1)
				throw errno_error("send");

			nsent += nbytes;
		}
	}

	void recv(std::vector<char>& buf) {
		int nreceived = 0;
		while (nreceived < buf.size()) {
			int nbytes = ::recv(s_, &buf.front() + nreceived, buf.size() - nreceived, 0);
			if (nbytes == -1)
				throw errno_error("recv");

			nreceived += nbytes;
		}
	}

private:
	int s_ = -1;
};
