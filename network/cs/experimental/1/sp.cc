#include "sp.hh"

int main()
try {
	ip6_address addr;
	socket_server6 server(addr);
}
catch (const std::exception& e) {
	fprintf(stderr, "%s\n", e.what());
}
