#include "sp.hh"

int main()
try {
	{
		ip4_address addr;
		socket_tcp_server_ipv4 server(addr);
	}
	{
		ip6_address addr;
		socket_tcp_server_ipv6 server(addr);
	}
	{
		ip4_address addr;
		socket_udp_server_ipv4 server(addr);
	}
	{
		ip6_address addr;
		socket_udp_server_ipv6 server(addr);
	}
	{
		ip4_address addr;
		socket_udp_client_ipv4 server(addr);
	}
	{
		ip6_address addr;
		socket_udp_client_ipv6 server(addr);
	}
}
catch (const std::exception& e) {
	fprintf(stderr, "%s\n", e.what());
}
