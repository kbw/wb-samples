#pragma once

#include "defs.hh"

#include <unistd.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include <stdexcept>
#include <string>
#include <vector>

//----------------------------------------------------------------------------

struct errno_error : public std::runtime_error {
	errno_error(const char* op) : std::runtime_error(make_msg(op)) {}

private:
	static std::string make_msg(const char* op) {
		char buffer[96];
#if (defined(__USE_XOPEN2K) && !defined(__USE_GNU)) || \
    (defined(__ANDROID_API__) && defined(__USE_GNU) && __ANDROID_API__ < 23)
		int len = snprintf(buffer, sizeof(buffer), "%s:%d ", op, errno);
		strerror_r(errno, buffer + len, sizeof(buffer) - len);
		return buffer;
#else
		char* str = strerror_r(errno, buffer, sizeof(buffer));
		int len = snprintf(buffer, sizeof(buffer), "%s: %d %s", op, errno, str);
		if (unlikely(len == -1))
			return std::to_string(errno);
		return { buffer, static_cast<size_t>(len) };
#endif
	}
};

//----------------------------------------------------------------------------

struct ip4_address : public sockaddr_in {
	static constexpr int protocol = PF_INET;
	static constexpr int family = AF_INET;

	ip4_address() {
		sin_family = family;
		sin_addr.s_addr = INADDR_ANY;
		sin_port = 0;
	}
	ip4_address(uint16_t port) {
		sin_family = family;
		sin_addr.s_addr = INADDR_ANY;
		sin_port = htons(port);
	}
	ip4_address(const std::string& ipaddr, uint16_t port) {
		sin_family = family;
		sin_addr.s_addr = ::inet_addr(ipaddr.c_str());
		sin_port = htons(port);
	}
};

struct ip6_address : public sockaddr_in6 {
	static constexpr int protocol = PF_INET6;
	static constexpr int family = AF_INET6;

	ip6_address() {
		sin6_family = family;
		sin6_addr = in6addr_any;
		sin6_port = 0;
	}
	ip6_address(uint16_t port) {
		sin6_family = family;
		sin6_addr = in6addr_any;
		sin6_port = htons(port);
	}
	ip6_address(const std::string& ipaddr, uint16_t port) {
		sin6_family = family;
		inet_pton(family, ipaddr.c_str(), &sin6_addr);
		sin6_port = htons(port);
	}
};

//----------------------------------------------------------------------------

class socket_base
{
	int& s_;

public:
	socket_base(int& s) : s_(s) {
	}

	void close() {
		if (s_ != -1)
			::close(s_);
	}
};

template <typename ADDR>
class socket_tcp_server_base
{
	int& s_;

public:
	socket_tcp_server_base(int& s) : s_(s) {
	}

	socket_tcp_server_base(int& s, const ADDR& addr) : s_(s) {
		s_ = ::socket(ADDR::protocol, SOCK_STREAM, IPPROTO_TCP);
		if (s_ == -1)
			throw errno_error("socket");

		bind(addr);
	}

	void bind(const ADDR& addr) {
		int ret = ::bind(s_, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
		if (ret == -1)
			throw errno_error("bind");
	}

	void listen(int n) {
		int ret = ::listen(s_, n);
		if (ret == -1)
			throw errno_error("listen");
	}
};

template <typename ADDR>
class socket_tcp_client_base
{
	int& s_;

public:
	socket_tcp_client_base(int& s) : s_(s) {
	}

	socket_tcp_client_base(int& s, const ADDR& addr) : s_(s) {
		s_ = ::socket(ADDR::protocol, SOCK_STREAM, IPPROTO_TCP);
		if (s_ == -1)
			throw errno_error("socket");
	}

	void connect(const ADDR& addr) {
		if (s_ != -1)
			::connect(s_, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
	}
};

class socket_tcp_base
{
	int& s_;

public:
	socket_tcp_base(int& s) : s_(s) {
	}

	void send(const std::vector<char>& buf)	{
		int nsent = 0;
		while (nsent < buf.size()) {
			int nbytes = ::send(s_, &buf.front() + nsent, buf.size() - nsent, 0);
			if (nbytes == -1)
				throw errno_error("send");

			nsent += nbytes;
		}
	}

	void recv(std::vector<char>& buf) {
		int nreceived = 0;
		while (nreceived < buf.size()) {
			int nbytes = ::recv(s_, &buf.front() + nreceived, buf.size() - nreceived, 0);
			if (nbytes == -1)
				throw errno_error("recv");

			nreceived += nbytes;
		}
	}
};

template <typename ADDR>
class socket_udp_server_base
{
	int& s_;

public:
	socket_udp_server_base(int& s) : s_(s) {
	}

	socket_udp_server_base(int& s, const ADDR& addr) : s_(s) {
		s_ = ::socket(ADDR::protocol, SOCK_DGRAM, IPPROTO_UDP);
		if (s_ == -1)
			throw errno_error("socket");
	}

	void bind(const ADDR& addr) {
		int ret = ::bind(s_, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
		if (ret == -1)
			throw errno_error("bind");
	}
};

template <typename ADDR>
class socket_udp_client_base
{
	int& s_;

public:
	socket_udp_client_base(int& s) : s_(s) {
	}

	socket_udp_client_base(int& s, const ADDR& addr) : s_(s) {
		s_ = ::socket(ADDR::protocol, SOCK_DGRAM, IPPROTO_UDP);
		if (s_ == -1)
			throw errno_error("socket");
	}
};

template <typename ADDR>
class socket_udp_base
{
	int& s_;

public:
	socket_udp_base(int& s) : s_(s) {
	}

	void sendto(const std::vector<char>& buf, const ADDR& addr) {
		int nsent = 0;
		while (nsent < buf.size()) {
			int nbytes = ::sendto(s_, &buf.front() + nsent, buf.size() - nsent, 0, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
			if (nbytes == -1)
				throw errno_error("sendto");

			nsent += nbytes;
		}
	}

	void recvfrom(std::vector<char>& buf, ADDR& addr) {
		int nreceived = 0;
		while (nreceived < buf.size()) {
			int nbytes = ::recvfrom(s_, &buf.front() + nreceived, buf.size() - nreceived, 0, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
			if (nbytes == -1)
				throw errno_error("recv");

			nreceived += nbytes;
		}
	}
};

//----------------------------------------------------------------------------
// socket_tcp_server

struct socket_tcp_server_ipv4 :	public socket_base,
								public socket_tcp_server_base<ip4_address>,
								public socket_tcp_base
{
	int s_ = -1;

public:
	socket_tcp_server_ipv4(int s = -1) :
		socket_base(s_),
		socket_tcp_server_base<ip4_address>(s_),
		socket_tcp_base(s_),
		s_(s)
	{
	}
	~socket_tcp_server_ipv4() {
		close();
	}

	socket_tcp_server_ipv4(const ip4_address& addr) :
		socket_base(s_),
		socket_tcp_server_base<ip4_address>(s_, addr),
		socket_tcp_base(s_)
	{
	}

	socket_tcp_server_ipv4(const socket_tcp_server_ipv4 &) = delete;
	socket_tcp_server_ipv4& operator=(const socket_tcp_server_ipv4 &) = delete;

	socket_tcp_server_ipv4(socket_tcp_server_ipv4&& n) :
		socket_base(s_),
		socket_tcp_server_base<ip4_address>(s_),
		socket_tcp_base(s_),
		s_(n.s_)
	{
		n.s_ = -1;
	}
	socket_tcp_server_ipv4& operator=(socket_tcp_server_ipv4&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}
};

class socket_tcp_server_ipv6 :	public socket_base,
								public socket_tcp_server_base<ip6_address>,
								public socket_tcp_base
{
	int s_ = -1;

public:
	socket_tcp_server_ipv6(int s = -1) :
		socket_base(s_),
		socket_tcp_server_base<ip6_address>(s_),
		socket_tcp_base(s_),
		s_(s)
	{
	}
	~socket_tcp_server_ipv6() {
		close();
	}

	socket_tcp_server_ipv6(const ip6_address& addr) :
		socket_base(s_),
		socket_tcp_server_base<ip6_address>(s_, addr),
		socket_tcp_base(s_)
	{
	}

	socket_tcp_server_ipv6(const socket_tcp_server_ipv6 &) = delete;
	socket_tcp_server_ipv6& operator=(const socket_tcp_server_ipv6 &) = delete;

	socket_tcp_server_ipv6(socket_tcp_server_ipv6&& n) :
		socket_base(s_),
		socket_tcp_server_base<ip6_address>(s_),
		socket_tcp_base(s_),
		s_(n.s_)
	{
		n.s_ = -1;
	}
	socket_tcp_server_ipv6& operator=(socket_tcp_server_ipv6&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}
};

//----------------------------------------------------------------------------
// socket_tcp_client

class socket_client_ipv4 :	public socket_base,
							public socket_tcp_client_base<ip4_address>,
							public socket_tcp_base
{
	int s_ = -1;

public:
	socket_client_ipv4(int s = -1) :
		socket_base(s_),
		socket_tcp_client_base<ip4_address>(s_),
		socket_tcp_base(s_),
		s_(s)
	{
	}
	~socket_client_ipv4() {
		close();
	}

	socket_client_ipv4(const ip4_address& addr) :
		socket_base(s_),
		socket_tcp_client_base<ip4_address>(s_, addr),
		socket_tcp_base(s_)
	{
	}

	socket_client_ipv4(const socket_client_ipv4 &) = delete;
	socket_client_ipv4& operator=(const socket_client_ipv4 &) = delete;

	socket_client_ipv4(socket_client_ipv4&& n) :
		socket_base(s_),
		socket_tcp_client_base<ip4_address>(s_),
		socket_tcp_base(s_),
		s_(n.s_)
	{
		n.s_ = -1;
	}
	socket_client_ipv4& operator=(socket_client_ipv4&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}
};

class socket_client_ipv6 :	public socket_base,
							public socket_tcp_client_base<ip6_address>,
							public socket_tcp_base
{
	int s_ = -1;

public:
	socket_client_ipv6(int s = -1) :
		socket_base(s_),
		socket_tcp_client_base<ip6_address>(s_),
		socket_tcp_base(s_),
		s_(s)
	{
	}
	~socket_client_ipv6() {
		close();
	}

	socket_client_ipv6(const ip6_address& addr) :
		socket_base(s_),
		socket_tcp_client_base<ip6_address>(s_, addr),
		socket_tcp_base(s_)
	{
	}

	socket_client_ipv6(const socket_client_ipv6 &) = delete;
	socket_client_ipv6& operator=(const socket_client_ipv6 &) = delete;

	socket_client_ipv6(socket_client_ipv6&& n) :
		socket_base(s_),
		socket_tcp_client_base<ip6_address>(s_),
		socket_tcp_base(s_),
		s_(n.s_)
	{
		n.s_ = -1;
	}
	socket_client_ipv6& operator=(socket_client_ipv6&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}
};

//----------------------------------------------------------------------------
// socket_udp_server

struct socket_udp_server_ipv4 :	public socket_base,
								public socket_udp_server_base<ip4_address>,
								public socket_udp_base<ip4_address>
{
	int s_ = -1;

public:
	socket_udp_server_ipv4(int s = -1) :
		socket_base(s_),
		socket_udp_server_base<ip4_address>(s_),
		socket_udp_base(s_),
		s_(s)
	{
	}
	~socket_udp_server_ipv4() {
		close();
	}

	socket_udp_server_ipv4(const ip4_address& addr) :
		socket_base(s_),
		socket_udp_server_base<ip4_address>(s_, addr),
		socket_udp_base(s_)
	{
	}

	socket_udp_server_ipv4(const socket_udp_server_ipv4 &) = delete;
	socket_udp_server_ipv4& operator=(const socket_udp_server_ipv4 &) = delete;

	socket_udp_server_ipv4(socket_udp_server_ipv4&& n) :
		socket_base(s_),
		socket_udp_server_base<ip4_address>(s_),
		socket_udp_base(s_),
		s_(n.s_)
	{
		n.s_ = -1;
	}
	socket_udp_server_ipv4& operator=(socket_udp_server_ipv4&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}
};

struct socket_udp_server_ipv6 :	public socket_base,
								public socket_udp_server_base<ip6_address>,
								public socket_udp_base<ip6_address>
{
	int s_ = -1;

public:
	socket_udp_server_ipv6(int s = -1) :
		socket_base(s_),
		socket_udp_server_base<ip6_address>(s_),
		socket_udp_base(s_),
		s_(s)
	{
	}
	~socket_udp_server_ipv6() {
		close();
	}

	socket_udp_server_ipv6(const ip6_address& addr) :
		socket_base(s_),
		socket_udp_server_base<ip6_address>(s_, addr),
		socket_udp_base(s_)
	{
	}

	socket_udp_server_ipv6(const socket_udp_server_ipv6 &) = delete;
	socket_udp_server_ipv6& operator=(const socket_udp_server_ipv6 &) = delete;

	socket_udp_server_ipv6(socket_udp_server_ipv6&& n) :
		socket_base(s_),
		socket_udp_server_base<ip6_address>(s_),
		socket_udp_base(s_),
		s_(n.s_)
	{
		n.s_ = -1;
	}
	socket_udp_server_ipv6& operator=(socket_udp_server_ipv6&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}
};

//----------------------------------------------------------------------------
// socket_udp_client

struct socket_udp_client_ipv4 :	public socket_base,
								public socket_udp_client_base<ip4_address>,
								public socket_udp_base<ip4_address>
{
	int s_ = -1;

public:
	socket_udp_client_ipv4(int s = -1) :
		socket_base(s_),
		socket_udp_client_base<ip4_address>(s_),
		socket_udp_base(s_),
		s_(s)
	{
	}
	~socket_udp_client_ipv4() {
		close();
	}

	socket_udp_client_ipv4(const ip4_address& addr) :
		socket_base(s_),
		socket_udp_client_base<ip4_address>(s_, addr),
		socket_udp_base(s_)
	{
	}

	socket_udp_client_ipv4(const socket_udp_client_ipv4 &) = delete;
	socket_udp_client_ipv4& operator=(const socket_udp_client_ipv4 &) = delete;

	socket_udp_client_ipv4(socket_udp_client_ipv4&& n) :
		socket_base(s_),
		socket_udp_client_base<ip4_address>(s_),
		socket_udp_base(s_),
		s_(n.s_)
	{
		n.s_ = -1;
	}
	socket_udp_client_ipv4& operator=(socket_udp_client_ipv4&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}
};

struct socket_udp_client_ipv6 :	public socket_base,
								public socket_udp_client_base<ip6_address>,
								public socket_udp_base<ip6_address>
{
	int s_ = -1;

public:
	socket_udp_client_ipv6(int s = -1) :
		socket_base(s_),
		socket_udp_client_base<ip6_address>(s_),
		socket_udp_base(s_),
		s_(s)
	{
	}
	~socket_udp_client_ipv6() {
		close();
	}

	socket_udp_client_ipv6(const ip6_address& addr) :
		socket_base(s_),
		socket_udp_client_base<ip6_address>(s_, addr),
		socket_udp_base(s_)
	{
	}

	socket_udp_client_ipv6(const socket_udp_client_ipv6 &) = delete;
	socket_udp_client_ipv6& operator=(const socket_udp_client_ipv6 &) = delete;

	socket_udp_client_ipv6(socket_udp_client_ipv6&& n) :
		socket_base(s_),
		socket_udp_client_base<ip6_address>(s_),
		socket_udp_base(s_),
		s_(n.s_)
	{
		n.s_ = -1;
	}
	socket_udp_client_ipv6& operator=(socket_udp_client_ipv6&& n) {
		if (this != &n) {
			close();

			s_ = n.s_;
			n.s_ = -1;
		}
		return *this;
	}
};
