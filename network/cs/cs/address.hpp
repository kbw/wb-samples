#pragma once
	
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <string>
#include <stdexcept>

#include <string.h>

//----------------------------------------------------------------------------

namespace cs {
	struct ip4_address : public sockaddr_in {
		static constexpr int domain = PF_INET;
		static constexpr int family = AF_INET;
	
		ip4_address(uint16_t port = 0) {
			memset(this, 0, sizeof(*this));
			sin_family = family;
			sin_addr.s_addr = INADDR_ANY;
			sin_port = htons(port);
		}
		ip4_address(const std::string& ipaddr, uint16_t port = 0) {
			memset(this, 0, sizeof(*this));
			sin_family = family;
			int ret = inet_pton(family, ipaddr.c_str(), &sin_addr);
			if (ret == 0)
				throw std::runtime_error("bad ip4 address");
			sin_port = htons(port);
		}
	
		std::string address() const {
			char buf[INET_ADDRSTRLEN];
			return inet_ntop(AF_INET, &sin_addr, buf, sizeof(buf));
		}
	
		uint16_t port() const {
			return ntohs(sin_port);
		}
	};
	
	struct ip6_address : public sockaddr_in6 {
		static constexpr int domain = PF_INET6;
		static constexpr int family = AF_INET6;
	
		ip6_address(uint16_t port = 0) {
			memset(this, 0, sizeof(*this));
			sin6_family = family;
			sin6_addr = in6addr_any;
			sin6_port = htons(port);
		}
		ip6_address(const std::string& ipaddr, uint16_t port = 0) {
			memset(this, 0, sizeof(*this));
			sin6_family = family;
			int  ret = inet_pton(family, ipaddr.c_str(), &sin6_addr);
			if (ret == 0)
				throw std::runtime_error("bad ip6 address");
			sin6_port = htons(port);
		}
	
		std::string address() const {
			char buf[INET6_ADDRSTRLEN];
			return inet_ntop(AF_INET6, &sin6_addr, buf, sizeof(buf));
		}
	
		uint16_t port() const {
			return ntohs(sin6_port);
		}
	};
}
