#pragma once

#include "defs.hpp"

#include <unistd.h>
#include <sys/errno.h>
#include <sys/types.h>

#include <string.h>
#include <stdio.h>

#include <stdexcept>
#include <string>

//----------------------------------------------------------------------------

struct errno_error : public std::runtime_error {
	errno_error(const char* op) : std::runtime_error(make_msg(op)) {}

private:
	static std::string make_msg(const char* op) {
		char buffer[96];
#if (defined(__USE_XOPEN2K) && !defined(__USE_GNU)) || \
    (defined(__ANDROID_API__) && defined(__USE_GNU) && __ANDROID_API__ < 23) || \
    (defined(__POSIX_VISIBLE) && __POSIX_VISIBLE >= 200112)
		int len = snprintf(buffer, sizeof(buffer), "%s:%d ", op, errno);
		strerror_r(errno, buffer + len, sizeof(buffer) - len);
		return buffer;
#else
		char* str = strerror_r(errno, buffer, sizeof(buffer));
		int len = snprintf(buffer, sizeof(buffer), "%s: %d %s", op, errno, str);
		if (unlikely(len == -1))
			return std::to_string(errno);
		return { buffer, static_cast<size_t>(len) };
#endif
	}
};
