#pragma once

#include "defs.hpp"
#include "error.hpp"
#include "address.hpp"

#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <vector>

//----------------------------------------------------------------------------

namespace cs { namespace internal {
	template <typename SOCKET, typename ADDR>
	int socket() {
		int s = ::socket(ADDR::domain, SOCKET::type, SOCKET::protocol);
		if (s == -1)
			throw errno_error("socket");

		return s;
	}

	template <typename SOCKET>
	void close(SOCKET& s) {
		if (unlikely(s.fd() == -1))
			return;

		::close(s.fd());
	}

	template <typename SOCKET>
	void reuse_address(SOCKET& s, bool value = true) {
		if (unlikely(s.fd() == -1))
			return;

		int enable = value ? 1 : 0;
		int ret = ::setsockopt(s.fd(), SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));
		if (ret == -1)
			throw errno_error("setsockopt(SO_REUSEADDR)");
	}

	template <typename SOCKET>
	void reuse_port(SOCKET& s, bool value = true) {
		if (unlikely(s.fd() == -1))
			return;

		int enable = value ? 1 : 0;
#if defined(SO_REUSEPORT_LB) // BSD load balance
		::setsockopt(s.fd(), SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(int));
#elif defined(SO_REUSEPORT) // Linux load balance
		::setsockopt(s.fd(), SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(int));
#endif
	}

	template <typename SOCKET, typename ADDR>
	void bind(SOCKET& s, const ADDR& addr) {
		if (unlikely(s.fd() == -1))
			return;

		int ret = ::bind(s.fd(), reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
		if (ret == -1)
			throw errno_error("bind");
	}

	template <typename SOCKET>
	void listen(SOCKET& s, int n = 64) {
		if (unlikely(s.fd() == -1))
			return;

		int ret = ::listen(s.fd(), n);
		if (ret == -1)
			throw errno_error("listen");
	}

	template <typename SOCKET, typename ADDR>
	void connect(SOCKET& s, const ADDR& addr) {
		if (unlikely(s.fd() == -1))
			return;

		int ret = ::connect(s.fd(), reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
		if (ret == -1)
			throw errno_error("connect");
	}

	template <typename SOCKET, typename ADDR>
	int accept(SOCKET& s, ADDR& addr) {
		if (unlikely(s.fd() == -1))
			return -1;

		socklen_t len = 0;
		int client = ::accept(s.fd(), reinterpret_cast<sockaddr*>(&addr), &len);
		if (unlikely(client == -1 || len != sizeof(addr)))
			throw errno_error("accept");

		return client;
	}

	template <typename SOCKET>
	void send(SOCKET& s, const std::vector<char>& buf)	{
		if (unlikely(s.fd() == -1))
			return;

		size_t nsent = 0;
		while (nsent < buf.size()) {
			int nbytes = ::send(s.fd(), &buf.front() + nsent, buf.size() - nsent, 0);
			if (nbytes == -1)
				throw errno_error("send");

			nsent += nbytes;
		}
	}

	template <typename SOCKET>
	void recv(SOCKET& s, std::vector<char>& buf) {
		if (unlikely(s.fd() == -1))
			return;

		size_t nreceived = 0;
		while (nreceived < buf.size()) {
			int nbytes = ::recv(s.fd(), &buf.front() + nreceived, buf.size() - nreceived, 0);
			if (nbytes == -1)
				throw errno_error("recv");

			nreceived += nbytes;
		}
	}

	template <typename SOCKET, typename ADDR>
	void sendto(SOCKET& s, const std::vector<char>& buf, const ADDR& addr) {
		if (unlikely(s.fd() == -1))
			return;

		size_t nsent = 0;
		while (nsent < buf.size()) {
			int nbytes = ::sendto(s.fd(), &buf.front() + nsent, buf.size() - nsent, 0, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
			if (nbytes == -1)
				throw errno_error("sendto");

			nsent += nbytes;
		}
	}

	template <typename SOCKET, typename ADDR>
	void recvfrom(SOCKET& s, std::vector<char>& buf, ADDR& addr) {
		if (unlikely(s.fd() == -1))
			return;

		size_t nreceived = 0;
		while (nreceived < buf.size()) {
			int nbytes = ::recvfrom(s.fd(), &buf.front() + nreceived, buf.size() - nreceived, 0, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
			if (nbytes == -1)
				throw errno_error("recv");

			nreceived += nbytes;
		}
	}
}} // cs::internal

class socket_base
{
	int& s_;

public:
	socket_base(int& s) : s_(s) {
	}

	int fd() const {
		return s_;
	}

	void close() {
		if (s_ != -1)
			::close(s_);
	}
};

template <typename ADDR>
class socket_tcp_server_base
{
	int& s_;

public:
	socket_tcp_server_base(int& s) : s_(s) {
	}

	socket_tcp_server_base(int& s, const ADDR& addr) : s_(s) {
		s_ = ::socket(ADDR::protocol, SOCK_STREAM, IPPROTO_TCP);
		if (s_ == -1)
			throw errno_error("socket");

		reuse_address();
		reuse_port();
		bind(addr);
		listen();
	}

	void reuse_address(bool value = true) {
		int enable = value ? 1 : 0;
		int ret = setsockopt(s_, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));
		if (ret == -1)
			throw errno_error("setsockopt(SO_REUSEADDR)");
	}

	void reuse_port(bool value = true) {
		int enable = value ? 1 : 0;
		int ret = setsockopt(s_, SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(int));
		if (ret == -1)
			throw errno_error("setsockopt(SO_REUSEADDR)");
	}

	void bind(const ADDR& addr) {
		int ret = ::bind(s_, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
		if (ret == -1)
			throw errno_error("bind");
	}

	void listen(int n = 64) {
		int ret = ::listen(s_, n);
		if (ret == -1)
			throw errno_error("listen");
	}
};

template <typename ADDR>
class socket_tcp_client_base
{
	int& s_;

public:
	socket_tcp_client_base(int& s) : s_(s) {
	}

	socket_tcp_client_base(int& s, const ADDR& addr) : s_(s) {
		s_ = ::socket(ADDR::protocol, SOCK_STREAM, IPPROTO_TCP);
		if (s_ == -1)
			throw errno_error("socket");

		connect(addr);
	}

	void connect(const ADDR& addr) {
		if (s_ != -1)
			::connect(s_, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
	}
};

class socket_tcp_base
{
	int& s_;

public:
	socket_tcp_base(int& s) : s_(s) {
	}

	void send(const std::vector<char>& buf)	{
		size_t nsent = 0;
		while (nsent < buf.size()) {
			int nbytes = ::send(s_, &buf.front() + nsent, buf.size() - nsent, 0);
			if (nbytes == -1)
				throw errno_error("send");

			nsent += nbytes;
		}
	}

	void recv(std::vector<char>& buf) {
		size_t nreceived = 0;
		while (nreceived < buf.size()) {
			int nbytes = ::recv(s_, &buf.front() + nreceived, buf.size() - nreceived, 0);
			if (nbytes == -1)
				throw errno_error("recv");

			nreceived += nbytes;
		}
	}
};

template <typename ADDR>
class socket_udp_server_base
{
	int& s_;

public:
	socket_udp_server_base(int& s) : s_(s) {
	}

	socket_udp_server_base(int& s, const ADDR& addr) : s_(s) {
		s_ = ::socket(ADDR::protocol, SOCK_DGRAM, IPPROTO_UDP);
		if (s_ == -1)
			throw errno_error("socket");

		reuse_address();
		bind(addr);
	}

	void reuse_address(bool value = true) {
		int enable = value ? 1 : 0;
		int ret = setsockopt(s_, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));
		if (ret == -1)
			throw errno_error("setsockopt(SO_REUSEADDR)");
	}

	void bind(const ADDR& addr) {
		int ret = ::bind(s_, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
		if (ret == -1)
			throw errno_error("bind");
	}
};

template <typename ADDR>
class socket_udp_client_base
{
	int& s_;

public:
	socket_udp_client_base(int& s) : s_(s) {
	}

	socket_udp_client_base(int& s, const ADDR&) : s_(s) {
		s_ = ::socket(ADDR::protocol, SOCK_DGRAM, IPPROTO_UDP);
		if (s_ == -1)
			throw errno_error("socket");
	}
};

template <typename ADDR>
class socket_udp_base
{
	int& s_;

public:
	socket_udp_base(int& s) : s_(s) {
	}

	void sendto(const std::vector<char>& buf, const ADDR& addr) {
		size_t nsent = 0;
		while (nsent < buf.size()) {
			int nbytes = ::sendto(s_, &buf.front() + nsent, buf.size() - nsent, 0, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
			if (nbytes == -1)
				throw errno_error("sendto");

			nsent += nbytes;
		}
	}

	void recvfrom(std::vector<char>& buf, ADDR& addr) {
		size_t nreceived = 0;
		while (nreceived < buf.size()) {
			int nbytes = ::recvfrom(s_, &buf.front() + nreceived, buf.size() - nreceived, 0, reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
			if (nbytes == -1)
				throw errno_error("recv");

			nreceived += nbytes;
		}
	}
};
