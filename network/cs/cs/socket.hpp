#pragma once

#include "error.hpp"
#include "address.hpp"
#include "socket_internal.hpp"

#include <unistd.h>

namespace cs {
	//------------------------------------------------------------------------
	// socket_tcp_client
	template <typename ADDR>
	class socket_tcp_client
	{
		int s_;

	public:
		static constexpr int type = SOCK_STREAM;
		static constexpr int protocol = IPPROTO_TCP;

		int fd() const {
			return s_;
		}

		socket_tcp_client(int s = -1) : s_(s) {
		}

		socket_tcp_client(const ADDR& addr) : s_(-1) {
			try {
				s_ = internal::socket<socket_tcp_client, ADDR>();
				internal::connect<socket_tcp_client, ADDR>(*this, addr);
			}
			catch (const std::exception &) {
				internal::close(*this);
				throw;
			}
		}

		~socket_tcp_client() {
			internal::close(*this);
		}

		socket_tcp_client(const socket_tcp_client& n) = delete;
		socket_tcp_client& operator=(const socket_tcp_client& n) = delete;

		socket_tcp_client(socket_tcp_client&& n) : s_(n.s_) {
			n.s_ = -1;
		}

		socket_tcp_client& operator=(socket_tcp_client&& n) {
			if (this != &n) {
				close();
				s_ = n.s_;
				n.s_ = -1;
			}
			return *this;
		}

		void close() {
			internal::close<socket_tcp_client>(*this);
		}

		void connect(const ADDR& addr) {
			internal::connect<socket_tcp_client, ADDR>(*this, addr);
		}

		void send(const std::vector<char>& buffer) {
			internal::send<socket_tcp_client>(*this, buffer);
		}

		void recv(std::vector<char>& buffer) {
			internal::recv<socket_tcp_client>(*this, buffer);
		}
	};

	//------------------------------------------------------------------------
	// socket_tcp_server
	template <typename ADDR>
	class socket_tcp_server
	{
		int s_;

	public:
		static constexpr int type = SOCK_STREAM;
		static constexpr int protocol = IPPROTO_TCP;

		int fd() const {
			return s_;
		}

		socket_tcp_server(int s = -1) : s_(s) {
		}

		socket_tcp_server(const ADDR& addr) : s_(-1) {
			try {
				s_ = internal::socket<socket_tcp_server, ADDR>();
				internal::bind<socket_tcp_server>(*this, addr);
				internal::reuse_address<socket_tcp_server>(*this);
				internal::reuse_port<socket_tcp_server>(*this);
				internal::listen<socket_tcp_server>(*this, 64);
			}
			catch (const std::exception &) {
				internal::close(*this);
				throw;
			}
		}

		~socket_tcp_server() {
			internal::close<socket_tcp_server>(*this);
		}

		socket_tcp_server(const socket_tcp_server &) = delete;
		socket_tcp_server& operator=(const socket_tcp_server &) = delete;

		socket_tcp_server(socket_tcp_server&& n) : s_(n.s_) {
			n.s_ = -1;
		}

		socket_tcp_server& operator=(socket_tcp_server&& n) {
			if (this != &n) {
				close();
				s_ = n.s_;
				n.s_ = -1;
			}
			return *this;
		}

		void socket(const ADDR &) {
			if (unlikely(s_ != -1))
				throw std::runtime_error("socket already initialised");

			s_ = internal::socket<socket_tcp_server, ADDR>();
		}

		void bind(const ADDR& addr) {
			internal::bind<socket_tcp_server>(*this, addr);
		}

		void reuse_address(bool value = true) {
			internal::reuse_address<socket_tcp_server>(*this, value);
		}

		void reuse_port(bool value = true) {
			internal::reuse_port<socket_tcp_server>(*this, value);
		}

		void listen(int value = 64) {
			internal::listen<socket_tcp_server>(*this, value);
		}

		void close() {
			internal::close<socket_tcp_server>(*this);
		}

		void send(const std::vector<char>& buffer) {
			internal::send<socket_tcp_server>(*this, buffer);
		}

		void recv(std::vector<char>& buffer) {
			internal::recv<socket_tcp_server>(*this, buffer);
		}

		socket_tcp_client<ADDR> accept(ADDR& addr) {
			return internal::accept<socket_tcp_server>(*this, addr);
		}
	};

	//------------------------------------------------------------------------
	// socket_udp_client
	template <typename ADDR>
	class socket_udp_client
	{
		int s_;

	public:
		static constexpr int type = SOCK_DGRAM;
		static constexpr int protocol = IPPROTO_UDP;

		int fd() const {
			return s_;
		}

		socket_udp_client(int s = -1) : s_(s) {
		}

		~socket_udp_client() {
			internal::close(*this);
		}

		socket_udp_client(const socket_udp_client& n) = delete;
		socket_udp_client& operator=(const socket_udp_client& n) = delete;

		socket_udp_client(socket_udp_client&& n) : s_(n.s_) {
			n.s_ = -1;
		}

		socket_udp_client& operator=(socket_udp_client&& n) {
			if (this != &n) {
				close();
				s_ = n.s_;
				n.s_ = -1;
			}
			return *this;
		}

		void close() {
			internal::close<socket_udp_client>(*this);
		}

		void sendto(const std::vector<char>& buffer, const ADDR& addr) {
			internal::sendto<socket_udp_client, ADDR>(*this, buffer, addr);
		}

		void recvfrom(std::vector<char>& buffer, ADDR& addr) {
			internal::recvfrom<socket_udp_client, ADDR>(*this, buffer, addr);
		}
	};

	//------------------------------------------------------------------------
	// socket_udp_server
	template <typename ADDR>
	class socket_udp_server
	{
		int s_;

	public:
		static constexpr int type = SOCK_DGRAM;
		static constexpr int protocol = IPPROTO_UDP;

		int fd() const {
			return s_;
		}

		socket_udp_server(int s = -1) : s_(s) {
		}

		socket_udp_server(const ADDR& addr) : s_(-1) {
			try {
				s_ = internal::socket<socket_udp_server, ADDR>();
				internal::bind<socket_udp_server>(*this, addr);
				internal::reuse_address<socket_udp_server>(*this);
				internal::reuse_port<socket_udp_server>(*this);
			}
			catch (const std::exception &) {
				internal::close(*this);
				throw;
			}
		}

		~socket_udp_server() {
			internal::close<socket_udp_server>(*this);
		}

		socket_udp_server(const socket_udp_server &) = delete;
		socket_udp_server& operator=(const socket_udp_server &) = delete;

		socket_udp_server(socket_udp_server&& n) : s_(n.s_) {
			n.s_ = -1;
		}

		socket_udp_server& operator=(socket_udp_server&& n) {
			if (this != &n) {
				close();
				s_ = n.s_;
				n.s_ = -1;
			}
			return *this;
		}

		void socket(const ADDR &) {
			if (unlikely(s_ != -1))
				throw std::runtime_error("socket already initialised");

			s_ = internal::socket<socket_udp_server, ADDR>();
		}

		void bind(const ADDR& addr) {
			internal::bind<socket_udp_server>(*this, addr);
		}

		void reuse_address(bool value = true) {
			internal::reuse_address<socket_udp_server>(*this, value);
		}

		void reuse_port(bool value = true) {
			internal::reuse_port<socket_udp_server>(*this, value);
		}

		void close() {
			internal::close<socket_udp_server>(*this);
		}

		void sendto(const std::vector<char>& buffer, const ADDR& addr) {
			internal::sendto<socket_udp_server, ADDR>(*this, buffer, addr);
		}

		void recvfrom(std::vector<char>& buffer, ADDR& addr) {
			internal::recvfrom<socket_udp_server, ADDR>(*this, buffer, addr);
		}
	};

	typedef socket_tcp_server<ip4_address> socket_tcp_server_ipv4;
	typedef socket_tcp_server<ip6_address> socket_tcp_server_ipv6;

	typedef socket_tcp_client<ip4_address> socket_tcp_client_ipv4;
	typedef socket_tcp_client<ip6_address> socket_tcp_client_ipv6;

	typedef socket_udp_server<ip4_address> socket_udp_server_ipv4;
	typedef socket_udp_server<ip6_address> socket_udp_server_ipv6;

	typedef socket_udp_client<ip4_address> socket_udp_client_ipv4;
	typedef socket_udp_client<ip6_address> socket_udp_client_ipv6;
}
