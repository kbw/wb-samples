#pragma once

#include <string.h>

namespace cs {
	struct buffer_t : public std::pair<char*, size_t> {
		buffer_t(size_t sz) {
			first = (char*)malloc(sz);
			second = sz;
		}

		~buffer_t()	{ free(first); }

		buffer_t(const buffer_t &n) = delete;
		buffer_t& operator=(const buffer_t &n) = delete;

		buffer_t(buffer_t&& n) {
			free(first);
			first = n.first;
			second = n.second;

			n.first = nullptr;
			n.second = 0;
		}

		buffer_t& operator=(buffer_t&& n) {
			if (this != &n) {
				free(first);
				first = n.first;
				second = n.second;

				n.first = nullptr;
				n.second = 0;
			}
			return *this;
		}

		char* resize(size_t n) {
			if (n != 0) {
				first = (char*)realloc(first, n);
				if (!first) {
					second = 0;
					throw std::bad_alloc();
				}
				second = n;
			}
			return first;
		}

		char*	resize(int n)	{ return resize((size_t)n); }
		char*	buf()			{ return first; }
		char*	buf() const		{ return first; }
		size_t	size() const	{ return second; }
	};
}
