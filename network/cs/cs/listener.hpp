#pragma once

#include "error.hpp"
#include "socket.hpp"
#include "buffer.hpp"

#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>

#include <variant>
#include <vector>
#include <set>
#include <map>

namespace cs {
	using socket_t = std::variant<
						socket_tcp_server_ipv4,
						socket_tcp_server_ipv6,
						socket_tcp_client_ipv4,
						socket_tcp_client_ipv6,
						socket_udp_server_ipv4,
						socket_udp_server_ipv6,
						socket_udp_client_ipv4,
						socket_udp_client_ipv6>;
	using sockets_t = std::vector<socket_t>;

	class listener {
	private:	// members
		int maxfd_{ -1 };
		bool refresh_sockets_{ true };
		sockets_t sockets_;
		std::map<int, size_t> index_map_; // fd -> sockets_ index

		static constexpr size_t buffersz_{ 64*1024 };

	private:	// methods
		void onTimeout() {
		}

		void onSocket(fd_set& working_set, buffer_t& buffer) {
			for (int s = 0; s != maxfd_; ++s) {
				if (FD_ISSET(s, &working_set)) {
					auto p = index_map_.find(s);
					if (p != index_map_.end()) {
						size_t index = p->second;
						onSocket(*this, sockets_[index], buffer);
					}
				}
			}
		}

		void onSocket(listener& l, socket_t& socket, buffer_t& buffer) {
			class visitor_t {
			private:
				listener& listener_;
				buffer_t& buf_;

			public:
				visitor_t(listener& l, buffer_t& buf) : listener_(l), buf_(buf) {
				}

				void operator()(const socket_tcp_server_ipv4& s) {
					ip4_address addr;
					socklen_t len;
					listener_.sockets_.emplace_back( socket_tcp_server_ipv4{ ::accept(s.fd(), (sockaddr*)&addr, &len) } );
					listener_.refresh_sockets_ = true;
				}
				void operator()(const socket_tcp_server_ipv6& s) {
					ip6_address addr;
					socklen_t len;
					listener_.sockets_.emplace_back( socket_tcp_server_ipv6{ ::accept(s.fd(), (sockaddr*)&addr, &len) } );
					listener_.refresh_sockets_ = true;
				}
				void operator()(const socket_tcp_client_ipv4& s) {
					int nbytes = ::read(s.fd(), buf_.buf(), buf_.size());
					buf_.resize(nbytes);
					// do callback
				}
				void operator()(const socket_tcp_client_ipv6& s) {
					int nbytes = ::read(s.fd(), buf_.buf(), buf_.size());
					buf_.resize(nbytes);
					// do callback
				}
				void operator()(const socket_udp_server_ipv4& s) {
					ip4_address addr;
					socklen_t len;
					int nbytes = ::recvfrom(s.fd(), buf_.buf(), buf_.size(), 0, (sockaddr*)&addr, &len);
					buf_.resize(nbytes);
					// do callback
				}
				void operator()(const socket_udp_server_ipv6& s) {
					ip6_address addr;
					socklen_t len;
					int nbytes = ::recvfrom(s.fd(), buf_.buf(), buf_.size(), 0, (sockaddr*)&addr, &len);
					buf_.resize(nbytes);
					// do callback
				}
				void operator()(const socket_udp_client_ipv4& s) {
					ip4_address addr;
					socklen_t len;
					int nbytes = ::recvfrom(s.fd(), buf_.buf(), buf_.size(), 0, (sockaddr*)&addr, &len);
					buf_.resize(nbytes);
					// do callback
				}
				void operator()(const socket_udp_client_ipv6& s) {
					ip6_address addr;
					socklen_t len;
					int nbytes = ::recvfrom(s.fd(), buf_.buf(), buf_.size(), 0, (sockaddr*)&addr, &len);
					buf_.resize(nbytes);
					// do callback
				}
			};

			visitor_t visitor{l, buffer};
			std::visit(visitor, socket);
		}

		static int fd(const socket_t &socket) {
	 		int fd_ = -1;
			std::visit([&](auto&& s) { fd_ = s.fd(); }, socket);
			return fd_;
		}

	public:
		listener(sockets_t sockets) : sockets_(std::move(sockets)) {
		}

		void listen() {
			fd_set master_set;
			struct timeval timeout;
			buffer_t buffer{ buffersz_ };

			while (true) {
				if (refresh_sockets_) {
					maxfd_ = -1;
					for (size_t i = 0; i != sockets_.size(); ++i)
						maxfd_ = std::max(maxfd_, fd(sockets_[i]));

					FD_ZERO(&master_set);
					index_map_.clear();
					for (size_t i = 0; i != sockets_.size(); ++i) {
						const int s{ fd(sockets_[i]) };
						FD_SET(s, &master_set);
						index_map_[s] = i;
					}

					refresh_sockets_ = false;
				}

				fd_set working_set;
				memcpy(&working_set, &master_set, sizeof(fd_set));

				timeout.tv_sec = 30;
				timeout.tv_usec = 0;

#ifdef TEMP_FAILURE_RETRY
				int n = TEMP_FAILURE_RETRY(select(maxfd_ + 1, &working_set, nullptr, nullptr, &timeout));
#else
				int n = select(maxfd_ + 1, &working_set, nullptr, nullptr, &timeout);
#endif
				switch (n) {
				case -1:
					throw errno_error{"select"};
				case 0:
					onTimeout();
					break;
				default:
					onSocket(working_set, buffer);
					buffer.resize(buffersz_);
				}
			}
		}
	};
}
