#pragma once

#include <string>

#include <stdint.h>

//----------------------------------------------------------------------------

#pragma pack(push, 1)
struct msg_raw {
	uint16_t length;
	char type;
};
#pragma pack(pop)

//----------------------------------------------------------------------------

constexpr char MSG_BASE = ' ';
constexpr char MSG_HEARTBEAT = 'H';

struct msg_base {
	msg_base() = default;
	virtual ~msg_base() = default;
	virtual char type() const;
	virtual std::string serialize() const;

	static msg_base* factory(const char* buffer, size_t length);
};

struct msg_heartbeat : public msg_base {
	msg_heartbeat() = default;
	char type() const;
	virtual std::string serialize() const;
};

//----------------------------------------------------------------------------

inline char msg_base::type() const {
	return MSG_BASE;
}

inline std::string msg_base::serialize() const {
	msg_raw raw{ 0, MSG_BASE };
	return std::string{reinterpret_cast<const char*>(&raw), sizeof(raw)};
}

inline msg_base* msg_base::factory(const char* buffer, size_t length) {
	if (!buffer)
		return nullptr;

	const msg_raw* raw = reinterpret_cast<const msg_raw*>(buffer);
	if (length < sizeof(raw->length))
		return nullptr;

	if (length < (sizeof(msg_raw) + raw->length))
		return nullptr;

	switch (raw->type) {
	case 'H':	return new msg_heartbeat();
	default:	return nullptr;
	}
}

//----------------------------------------------------------------------------

inline char msg_heartbeat::type() const {
	return MSG_HEARTBEAT;
}

inline std::string msg_heartbeat::serialize() const {
	msg_raw raw{ 0, MSG_HEARTBEAT };
	return std::string{reinterpret_cast<const char*>(&raw), sizeof(raw)};
}
