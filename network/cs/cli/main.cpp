#include <unistd.h>
#include <sys/wait.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>

#include <memory>
#include <atomic>

#include <cs/msg.hpp>

std::atomic<bool> ok{true};

void handler(int sig) {
	switch (sig) {
	case SIGINT:
		ok.store(false, std::memory_order_relaxed);
		break;
	}
}

int main(int argc, char* argv[]) {
	signal(SIGINT, handler);

	uint16_t port = argc > 1 ? atoi(argv[argc - 1]) : 3636;

	int s, ret;
	if ((s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		perror("socket");
		exit(1);
	}

	struct sockaddr_in6 addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin6_family = AF_INET6;
	ret = inet_pton(AF_INET6, "::1", &addr.sin6_addr);
	if (ret == 0)
		throw std::runtime_error("bad ip6 address");
	addr.sin6_port = htons(port);

	if ((ret = connect(s, (struct sockaddr*)&addr, sizeof(addr))) == -1) {
		perror("connect");
		close(s);
		exit(1);
	}

	const std::string buffer{ msg_heartbeat().serialize() };

	struct timeval delay;
	delay.tv_sec = 10, delay.tv_usec = 0;

	while (ok.load(std::memory_relaxed)) {
		fd_set in;
		FD_ZERO(&in); FD_SET(s, &in);

		int nbytes;
		struct timeval timeout = delay;

#ifdef TEMP_FAILURE_RETRY
		int selection = TEMP_FAILURE_RETRY(select(s + 1, &in, NULL, NULL, &timeout));
#else
		int selection = select(s + 1, &in, NULL, NULL, &timeout);
#endif
//		char text[32];
//		snprintf(text, sizeof(text), "select returned %d\n", selection);
//		write(STDOUT_FILENO, text, strlen(text));

		if (selection == -1) {
			perror("select");
			ret = 1;
			break;
		}
		else if (selection == 0) { // timeout
			uint16_t size = buffer.size();
			if ((nbytes = write(s, &size, sizeof(size))) != sizeof(size)) {
				perror("write: length");
				ret = 1;
				break;
			}
			if ((nbytes = write(s, buffer.c_str(), buffer.size())) != static_cast<int>(buffer.size())) {
				perror("write: msg_heartbeat");
				ret = 1;
			}

			const char text[] = "send heartbeat\n";
			write(STDOUT_FILENO, text, strlen(text));
		}
		else if (FD_ISSET(s, &in)) {
			uint16_t size;
			if ((nbytes = read(s, &size, sizeof(size))) != sizeof(size)) {
				perror("read: bad length");
				ret = 1;
				break;
			}

			char buffer[12];
			if ((nbytes = read(s, buffer, size)) != size) {
				perror("read: bad payload");
				ret = 1;
				break;
			}

			std::unique_ptr<msg_base> msg{ msg_base::factory(buffer, size) };
			if (!msg.get()) {
				perror("read: null message");
				ret = 1;
				break;
			}
			if (msg->type() != MSG_HEARTBEAT) {
				perror("read: unrecognized message");
			}

			const char text[] = "recv heartbeat\n";
			write(STDOUT_FILENO, text, strlen(text));
		}
	}

	shutdown(s, SHUT_RD);
	close(s);
	return ret;
}
