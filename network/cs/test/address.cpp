#include <cs/address.hpp>

#include <string>
#include <vector>
#include <utility>

#include <gtest/gtest.h>

namespace {
	const std::vector<std::pair<std::string, uint16_t>> pairs4 = {
		{ "127.0.0.1", 32400 },
		{ "192.168.20.27", 40888 },
		{ "10.0.63.254", 80 }
	};
	const std::vector<std::pair<std::string, uint16_t>> pairs6 = {
		{ "::1", 32400},
		{ "fe80::4321:965d:c492:3e45", 80 },
		{ "fe80::2e8a:72ff:feb9:f481", 40808 }
	};
}

TEST(Address, ip4_construct) {
	for (const auto pair : pairs4) {
		const auto addr_str = pair.first;
		const auto port = pair.second;

		const cs::ip4_address addr(addr_str, port);

		EXPECT_EQ(ntohs(port), addr.sin_port);
		EXPECT_EQ(port, addr.port());
		EXPECT_EQ(addr_str, addr.address());
		EXPECT_EQ(AF_INET, addr.family);
		EXPECT_EQ(PF_INET, addr.domain);
	}
}

TEST(Address, ip6_construct) {
	for (const auto pair : pairs6) {
		const auto addr_str = pair.first;
		const auto port = pair.second;;

		const cs::ip6_address addr(addr_str, port);

		EXPECT_EQ(ntohs(port), addr.sin6_port);
		EXPECT_EQ(port, addr.port());
		EXPECT_EQ(addr_str, addr.address());
		EXPECT_EQ(AF_INET6, addr.family);
		EXPECT_EQ(PF_INET6, addr.domain);
	}
}
