#include <cs/msg.hpp>

#include <memory>

#include <gtest/gtest.h>

TEST(Msg, Heartbeat) {
	msg_heartbeat heartbeat;
	std::string stream{ heartbeat.serialize() };

	std::unique_ptr<msg_base> msg{ msg_base::factory(stream.c_str(), stream.size()) };
	EXPECT_TRUE(dynamic_cast<msg_heartbeat*>(msg.get()));
	EXPECT_EQ(MSG_HEARTBEAT, msg->type());
}
