#pragma once

#include "cs/address.hpp"
#include "cs/socket.hpp"

#include <atomic>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

// Signal handlers:
//	SIGCHLD:	collect and ignore child exit codes
//	SIGINT:		stop the server
extern std::atomic<bool> ok;

// configuration driven behaviour
namespace config {
	std::string get_mode(int, char* argv[]);
}

// 8nput:	reads srv.json
namespace JSON {
	int exec(int, char* argv[]);
}

// input:	argv is a list of IP addresses (mix of v4/v6)
//			IPv4 pors are defined with environment variable SRV4
//			IPv6 pors are defined with environment variable SRV6
// output:	pair of socket collections, first is IPv4 connections, second is IPv6 connections
namespace MultipleIPs {
	typedef std::vector<std::pair<std::unique_ptr<cs::socket_tcp_server_ipv4>, std::unique_ptr<cs::ip4_address>>> ipv4_sockets_t;
	typedef std::vector<std::pair<std::unique_ptr<cs::socket_tcp_server_ipv6>, std::unique_ptr<cs::ip6_address>>> ipv6_sockets_t;

	std::tuple<ipv4_sockets_t, ipv6_sockets_t> create_listeners(int argc, char* argv[]);

	int exec(int argc, char* argv[]);
}

// input:	address port
//			port, with default host ::1
//			no args, with default host ::1, default port 3636
namespace Default {
	int exec(int argc, char* argv[]);
}
