#include "srv.hpp"
#include "child.hpp"

#include "cs/address.hpp"
#include "cs/socket.hpp"

#include <unistd.h>

#include <iostream>

#include <stdint.h>

// input:	address port
//			port, with default host ::1
//			no args, with default host ::1, default port 3636
namespace Default {
	int exec(int argc, char* argv[]) {
		const char* host = (argc > 2) ? argv[1] : "::1";
		uint16_t	port = (argc > 1) ? atoi(argv[argc - 1]) : 3636;

		cs::ip6_address addr{ host, port };
		cs::socket_tcp_server_ipv6 s{ addr };

		int id{};
		while (ok) {
			cs::ip6_address client_addr;
			cs::socket_tcp_client_ipv6 client{ s.accept(client_addr) };

			switch (int pid = fork()) {
			case -1:
				return 1;
			case 0:
				s.close();
				child(id, std::move(client), std::move(client_addr));
				return {};
			default:
				std::clog << "child:" << pid << "\n";
				client.close();
			}
		}
		return {};
	}
}
