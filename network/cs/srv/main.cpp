#include "srv.hpp"

#include <rapidjson/document.h>

#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

#include <atomic>
#include <exception>
#include <string>

// Signal handlers:
//	SIGCHLD:	collect and ignore child exit codes
//	SIGINT:		stop the server
std::atomic<bool> ok{ true };

void handler(int sig) {
	switch (sig) {
	case SIGCHLD: {
			pid_t pid;
			int status;
			while (sig == SIGCHLD && (pid = waitpid(-1, &status, WNOHANG)) > 0)
				;
		}
		break;
	case SIGINT:
		ok.store(false, std::memory_order_relaxed);
		break;
	default:
		;
	}
}

int main(int argc, char* argv[])
try {
	signal(SIGCHLD, handler);
	signal(SIGINT, handler);

	const std::string mode = config::get_mode(argc, argv);
	if (mode == "multiple_ips")
		return MultipleIPs::exec(argc, argv);
	else if (mode == "json")
		return JSON::exec(argc, argv);
	else
		return Default::exec(argc, argv);
}
catch (const std::exception& e) {
	fprintf(stderr, "fatal: %s\n", e.what());
	return 1;
}
