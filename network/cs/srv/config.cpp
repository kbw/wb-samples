/*
#include "cs/socket.hpp"
#include "srv.hpp"
#include "child.hpp"

#include <rapidjson/document.h>

#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdlib.h>

#include <atomic>
#include <exception>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <vector>
 */
#include "srv.hpp"

#include <fstream>
#include <string>

#include <string.h>

// configuration driven behaviour
namespace config {
	std::string get_mode(int, char* argv[]) {
		std::ifstream is{ std::string{static_cast<const char*>(argv[0])} + ".conf" };

		std::string line;
		while (std::getline(is, line))
			if (strncmp("mode=", line.c_str(), 5) == 0)
				return line.substr(5);

		return {};
	}
}
