#include "srv.hpp"

#include "cs/address.hpp"
#include "cs/socket.hpp"

#include <exception>
#include <memory>
#include <vector>

#include <stdlib.h>

// input:	argv is a list of IP addresses (mix of v4/v6)
//			IPv4 pors are defined with environment variable SRV4
//			IPv6 pors are defined with environment variable SRV6
// output:	pair of socket collections, first is IPv4 connections, second is IPv6 connections
namespace MultipleIPs {
	typedef std::vector<std::pair<std::unique_ptr<cs::socket_tcp_server_ipv4>, std::unique_ptr<cs::ip4_address>>> ipv4_sockets_t;
	typedef std::vector<std::pair<std::unique_ptr<cs::socket_tcp_server_ipv6>, std::unique_ptr<cs::ip6_address>>> ipv6_sockets_t;

	std::tuple<ipv4_sockets_t, ipv6_sockets_t> create_listeners(int argc, char* argv[]) {
		uint16_t port4 = 3444;
		if (const char* port_str = getenv("SRV4"))
			port4 = atoi(port_str);
		uint16_t port6 = 3446;
		if (const char* port_str = getenv("SRV6"))
			port6 = atoi(port_str);

		std::vector<std::pair<std::unique_ptr<cs::socket_tcp_server_ipv4>, std::unique_ptr<cs::ip4_address>>> s4;
		std::vector<std::pair<std::unique_ptr<cs::socket_tcp_server_ipv6>, std::unique_ptr<cs::ip6_address>>> s6;
		for (int i = 1; i < argc; ++i) {
			try {
				std::unique_ptr<cs::ip4_address> addr{ new cs::ip4_address{argv[i], port4} };
				std::unique_ptr<cs::socket_tcp_server_ipv4> sock{ new cs::socket_tcp_server_ipv4{*addr.get()} };
				s4.emplace_back( std::make_pair(std::move(sock), std::move(addr)) );
			}
			catch (const std::exception &) {
			}
			try {
				std::unique_ptr<cs::ip6_address> addr{ new cs::ip6_address(argv[i], port6) };
				std::unique_ptr<cs::socket_tcp_server_ipv6> sock{ new cs::socket_tcp_server_ipv6{*addr.get()} };
				s6.emplace_back( std::make_pair(std::move(sock), std::move(addr)) );
			}
			catch (const std::exception &) {
			}
		}

		return { std::move(s4), std::move(s6) };
	}

	int exec(int argc, char* argv[]) {
		auto listeners{ create_listeners(argc, argv) };

		cs::ip4_address client_addr;
		cs::socket_tcp_client_ipv4 client{ std::get<0>(listeners).front().first->accept(client_addr) };
		return {};
	}
}
