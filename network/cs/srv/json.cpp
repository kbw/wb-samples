#include "srv.hpp"
#include "cs/address.hpp"
#include "cs/socket.hpp"
#include "cs/listener.hpp"

#include <rapidjson/document.h>

#include <sys/stat.h>

#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

// input:	reads srv.json
namespace JSON {
	namespace {
//		const char* kTypeNames[] = {
//			"Null", "False", "True", "Object", "Array", "String", "Number" };

		template <typename OBJ>
		std::ostream& show_type(std::ostream& os, OBJ& obj) {
			const std::string prefix{ " is " };

			os << std::boolalpha;
			if (obj.IsNull())	os << prefix << "Null";
			if (obj.IsFalse())	os << prefix << "False";
			if (obj.IsTrue())	os << prefix << "True";
			if (obj.IsBool())	os << prefix << "Bool";
			if (obj.IsObject())	os << prefix << "Object";
			if (obj.IsArray())	os << prefix << "Array";
			if (obj.IsNumber())	os << prefix << "Number";
			if (obj.IsInt())	os << prefix << "Int";
			if (obj.IsUint())	os << prefix << "Uint";
			if (obj.IsInt64())	os << prefix << "Int64";
			if (obj.IsUint64())	os << prefix << "Uint64";
			if (obj.IsDouble())	os << prefix << "Double";
			if (obj.IsString())	os << prefix << "String";
			if (obj.IsFloat())	os << prefix << "Float";
			if (obj.IsLosslessDouble())	os << prefix << "LosslessDouble";
			if (obj.IsLosslessFloat())	os << prefix << "LosslessFloat";

			return os;
		}
	}

	namespace {
		cs::socket_t create_socket(std::string type, std::string host, uint16_t port) {
			if (host == "srvtcp4") {
				cs::ip4_address addr{host, port};
				return cs::socket_tcp_server_ipv4{addr};
			}
			if (host == "srvtcp6") {
				cs::ip6_address addr{host, port};
				return cs::socket_tcp_server_ipv6{addr};
			}
			if (host == "srvdpp4") {
				cs::ip4_address addr{host, port};
				return cs::socket_udp_server_ipv4{addr};
			}
			if (host == "srvudp6") {
				cs::ip6_address addr{host, port};
				return cs::socket_udp_server_ipv6{addr};
			}
			if (host == "clitcp4") {
//				cs::ip4_address addr{host, port};
				return cs::socket_tcp_client_ipv4{};
			}
			if (host == "clitcp6") {
//				cs::ip6_address addr{host, port};
				return cs::socket_tcp_client_ipv6{};
			}
			if (host == "clidpp4") {
//				cs::ip4_address addr{host, port};
				return cs::socket_udp_client_ipv4{};
			}
			if (host == "cliudp6") {
//				cs::ip6_address addr{host, port};
				return cs::socket_udp_client_ipv6{};
			}
			throw std::runtime_error{"bad arg"};
		}
	}

	// symbol -> [object, type]
	namespace obj { struct base; }
	typedef std::map<std::string, std::pair<std::unique_ptr<obj::base>, std::string>> components_t;

	namespace obj {
		struct base {
			virtual ~base() = default;
		};

		struct tcp4_server : public base {
			cs::ip4_address addr_;
			cs::socket_tcp_server_ipv4 srv_;

			tcp4_server(cs::ip4_address addr) : addr_(addr), srv_(addr) {
			}
		};

		struct tcp6_server : public base {
			cs::ip6_address addr_;
			cs::socket_tcp_server_ipv6 srv_;

			tcp6_server(cs::ip6_address addr) : addr_(addr), srv_(addr) {
			}
		};

		struct udp4_server : public base {
			cs::ip4_address addr_;
			cs::socket_udp_server_ipv4 srv_;

			udp4_server(cs::ip4_address addr) : addr_(addr), srv_(addr) {
			}
		};

		struct udp6_server : public base {
			cs::ip6_address addr_;
			cs::socket_udp_server_ipv6 srv_;

			udp6_server(cs::ip6_address addr) : addr_(addr), srv_(addr) {
			}
		};

		struct listener : public base {
			cs::listener listener_;

			listener(std::vector<std::string> sockets) : listener_(cs::sockets_t()) {
			}
		};

		void parse_srv(rapidjson::Value::ConstMemberIterator p, std::string& type_str, std::string& host, uint16_t& port) {
			auto type = p->value.FindMember("type");
			if (type != p->value.MemberEnd() && type->value.IsString()) {
//				std::cout << type->name.GetString() << ':' << type->value.GetString() << '\n';
				type_str = type->value.GetString();
			}

			auto params = p->value.FindMember("params");
			if (params != p->value.MemberEnd()) {
//				std::cout << params->name.GetString() << '\n';
				if (params->value.IsObject()) {
					std::string str{ "port" };
					auto p = params->value.FindMember(str.c_str());
					if (p != params->value.MemberEnd()) {
//						std::cout << "create_tcp4:srv:" << str << ":"; show_type(std::cout, p->value) << '\n';
						if (p->value.IsUint() && strcmp(p->name.GetString(), str.c_str()) == 0) {
							port = p->value.GetUint();
//							std::cout << '\t' << str << ":" << port << '\n';
						}
					}

					str = "host";
					p = params->value.FindMember(str.c_str());
					if (p != params->value.MemberEnd()) {
//						std::cout << "create_tcp4:srv:" << str << ":"; show_type(std::cout, p->value) << '\n';
						if (p->value.IsString() && strcmp(p->name.GetString(), str.c_str()) == 0) {
							host = p->value.GetString();
//							std::cout << '\t' << str << ":" << host << '\n';
						}
					}
				}
			}
		}

		void parse_listen(rapidjson::Value::ConstMemberIterator p, std::string& type_str, std::vector<std::string> values) {
			auto type = p->value.FindMember("type");
			if (type != p->value.MemberEnd() && type->value.IsString()) {
//				std::cout << "listener:srv:" << type->name.GetString() << ':' << type->value.GetString() << '\n';
				type_str = type->value.GetString();
			}

			auto params = p->value.FindMember("params");
			if (params != p->value.MemberEnd() && params->value.IsObject()) {
				std::string str{ "sockets" };
				auto sockets = params->value.FindMember(str.c_str());
				if (sockets->value.IsArray()) {
					for (rapidjson::Value::ConstValueIterator p = sockets->value.Begin(); p != sockets->value.End(); ++p) {
						if (p->IsInt()) {
//							std::cout << '\t' << p->GetInt() << '\n';
							values.emplace_back(std::to_string(p->GetInt()));
						}
						if (p->IsString()) {
//							std::cout << '\t' << p->GetString() << '\n';
							values.emplace_back(p->GetString());
						}
					}
				}
			}
		}

		std::pair<std::unique_ptr<obj::base>, std::string> create_tcp4srv(rapidjson::Value::ConstMemberIterator p) {
			std::string type, host;
			uint16_t port;
			parse_srv(p, type, host, port);

			return std::make_pair(
					std::unique_ptr<obj::base>(new tcp4_server{ cs::ip4_address{host, port} }),
					std::move(type));
		}

		std::pair<std::unique_ptr<obj::base>, std::string> create_tcp6srv(rapidjson::Value::ConstMemberIterator p) {
			std::string type, host;
			uint16_t port;
			parse_srv(p, type, host, port);

			return std::make_pair(
					std::unique_ptr<obj::base>(new tcp6_server{ cs::ip6_address{host, port} }),
					std::move(type));
		}

		std::pair<std::unique_ptr<obj::base>, std::string> create_udp4srv(rapidjson::Value::ConstMemberIterator p) {
			std::string type, host;
			uint16_t port;
			parse_srv(p, type, host, port);

			return std::make_pair(
					std::unique_ptr<obj::base>(new udp4_server{ cs::ip4_address{host, port} }),
					std::move(type));
		}

		std::pair<std::unique_ptr<obj::base>, std::string> create_udp6srv(rapidjson::Value::ConstMemberIterator p) {
			std::string type, host;
			uint16_t port;
			parse_srv(p, type, host, port);

			return std::make_pair(
					std::unique_ptr<obj::base>(new udp6_server{ cs::ip6_address{host, port} }),
					std::move(type));
		}

		std::pair<std::unique_ptr<obj::base>, std::string> create_listener(rapidjson::Value::ConstMemberIterator p) {
			std::string type;
			std::vector<std::string> sockets;
			parse_listen(p, type, sockets);

			cs::sockets_t s;

			return std::make_pair(
					std::unique_ptr<obj::base>(new listener(sockets)),
					std::move(type));
		}

		std::pair<std::unique_ptr<obj::base>, std::string> create(rapidjson::Value::ConstMemberIterator p) {
			std::string type_str;

			auto type = p->value.FindMember("type");
			if (type != p->value.MemberEnd() && type->value.IsString()) {
				std::cout << "create: " << type->name.GetString() << ':' << type->value.GetString() << '\n';
				type_str = type->value.GetString();
			}

			if (type_str == "srvtcp4") {
				return create_tcp4srv(p);
			}
			else if (type_str == "srvtcp6") {
				return create_tcp6srv(p);
			}
			else if (type_str == "srvudp4") {
				return create_udp4srv(p);
			}
			else if (type_str == "srvudp6") {
				return create_udp6srv(p);
			}
			else if (type_str == "listener") {
				return create_listener(p);
			}
			return {};
		}
	}

	namespace {
		std::unique_ptr<char[]> jbuf;
		size_t jbufsz{};
	}

	int exec(int, char* argv[]) {
		struct stat info;
		std::string filename{ std::string(static_cast<const char*>(argv[0])) + ".json" };
		if (stat(filename.c_str(), &info) == 0) {
			jbufsz = info.st_size;
			jbuf.reset(new char[jbufsz + 1]);
			jbuf[jbufsz] = '\0';

			std::ifstream jdoc{ filename, std::ios::binary };
			jdoc.read(jbuf.get(), jbufsz);
			std::cout << jbuf.get() << "\n";

			rapidjson::Document doc;
			doc.ParseInsitu(jbuf.get());

			// symbol -> {object, type}
			components_t components;

			std::cout << "parsed doc"; show_type(std::cout, doc) << '\n';
			if (doc.IsObject()) {
				for (auto p = doc.MemberBegin(); p != doc.MemberEnd(); ++p) {
					std::cout
						<< "symbol:" << p->name.GetString()
						<< " type:" ;//<< kTypeNames[p->value.GetType()];
						show_type(std::cout, p->value) << '\n';
					if (!p->value.IsObject())
						break;

					auto component{ obj::create(p) };
					if (component.first.get() && !component.second.empty()) {
						std::cout << "adding label:" << p->name.GetString() << '\n';
						components.emplace(
							std::piecewise_construct,
							std::forward_as_tuple( p->name.GetString() ),
							std::forward_as_tuple( std::move(component) ));
					}
				}

				/*
				const std::string str{ "app" };
				auto p = doc.FindMember(str.c_str());
				if (p != doc.MemberEnd()) {
					std::cout << "p->value"; show_type(std::cout, p->value);
				}
				 */
			}
			std::clog << "jdoc ok\n";
		}

		return {};
	}
}
