#include "child.hpp"
#include "cs/msg.hpp"

#include <unistd.h>
#include <sys/select.h>
#include <netinet/in.h>

#include <errno.h>
#include <string.h>

#include <memory>

int child(int id, cs::socket_tcp_client_ipv6 s, cs::ip6_address /*addr*/) {
	/*
	 * send heartbeats
	 */
	const std::string buffer{ msg_heartbeat().serialize() };

	struct timeval delay;
	delay.tv_sec = 10, delay.tv_usec = 0;

	int ret = 0;
	int ok = 1;
	while (ok) {
		fd_set in;
		FD_ZERO(&in); FD_SET(s.fd(), &in);

		int nbytes;
		struct timeval timeout = delay;

#ifdef TEMP_FAILURE_RETRY
		int selection = TEMP_FAILURE_RETRY(select(s.fd() + 1, &in, NULL, NULL, &timeout));
#else
		int selection = select(s.fd() + 1, &in, NULL, NULL, &timeout);
#endif
		char text[32];
//		snprintf(text, sizeof(text), "id=%d: select returned %d\n", id, selection);
//		write(STDOUT_FILENO, text, strlen(text));

		if (selection == -1) {
			perror("select");
			ret = 1;
			break;
		}
		else if (selection == 0) { // timeout
			uint16_t size = buffer.size();
			if ((nbytes = write(s.fd(), &size, sizeof(size))) != sizeof(size)) {
				perror("write: length");
				ret = 1;
				break;
			}
			if ((nbytes = write(s.fd(), buffer.c_str(), buffer.size())) != static_cast<int>(buffer.size())) {
				perror("write: msg_heartbeat");
				ret = 1;
				break;
			}

			snprintf(text, sizeof(text), "id=%d: send heartbeat\n", id);
			write(STDOUT_FILENO, text, strlen(text));
		}
		else if (FD_ISSET(s.fd(), &in)) {
			uint16_t size;
			if ((nbytes = read(s.fd(), &size, sizeof(size))) != sizeof(size)) {
				perror("read: bad length");
				ret = 1;
				break;
			}

			char buffer[12];
			if ((nbytes = read(s.fd(), buffer, size)) != size) {
				perror("read: bad payload");
				ret = 1;
				break;
			}

			std::unique_ptr<msg_base> msg{ msg_base::factory(buffer, size) };
			if (!msg.get()) {
				perror("read: null message");
				ret = 1;
				break;
			}
			if (msg->type() != MSG_HEARTBEAT) {
				perror("read: unrecognized message");
			}

			snprintf(text, sizeof(text), "id=%d: recv heartbeat\n", id);
			write(STDOUT_FILENO, text, strlen(text));
		}
	}

	return ret;
}
