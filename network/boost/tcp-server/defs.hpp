#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/asio/ip/tcp.hpp>


typedef boost::shared_ptr<boost::asio::ip::tcp::socket> socket_ptr;
typedef void handler_t(socket_ptr s);
