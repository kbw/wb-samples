#include "daemon.hpp"
#include "serial_server.hpp"
#include "parallel_server.hpp"

#include <boost/thread.hpp>
#include <boost/bind.hpp>


//---------------------------------------------------------------------------

void handler(socket_ptr s)
{
	boost::scoped_array<char> data(new char[4096]);
	size_t len = s->receive(boost::asio::buffer(data.get(), 4090));
	if (len > 0)
	{
		strcat(data.get(), "ok\n");
		s->send(boost::asio::buffer(data.get(), len + 3));
	}
}

int main()
{
	init_daemon();

	boost::thread(boost::bind(serial_server,   "127.0.0.1", 2001, handler));
	boost::thread(boost::bind(parallel_server, "127.0.0.1", 2002, handler));
}
