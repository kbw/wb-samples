#include "parallel_server.hpp"

#include <boost/bind.hpp>
#include <boost/ref.hpp>

#include <boost/asio.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/io_service.hpp>


//---------------------------------------------------------------------------

void start_accept(
		boost::asio::io_service& service,
		boost::asio::ip::tcp::acceptor& acc,
		socket_ptr s, handler_t* handler);
void handle_accept(
		boost::asio::io_service& service,
		boost::asio::ip::tcp::acceptor& acc,
		socket_ptr s, handler_t* handler,
		const boost::system::error_code& err);

void parallel_server(const std::string& host, uint16_t port, handler_t* handler)
{
	boost::asio::io_service service;
	boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(host), port);
	boost::asio::ip::tcp::acceptor acc(service, endpoint);

	socket_ptr s(new boost::asio::ip::tcp::socket(service));
	start_accept(service, acc, s, handler);

	service.run();
}

void start_accept(
		boost::asio::io_service& service,
		boost::asio::ip::tcp::acceptor& acc,
		socket_ptr s, handler_t* handler)
{
	acc.async_accept(
		*s,
		boost::bind(
			handle_accept,
			boost::reference_wrapper<boost::asio::io_service>(service),
			boost::reference_wrapper<boost::asio::ip::tcp::acceptor>(acc),
			s, handler, _1));
}

void handle_accept(
		boost::asio::io_service& service,
		boost::asio::ip::tcp::acceptor& acc,
		socket_ptr s, handler_t* handler,
		const boost::system::error_code& err)
{
	if (err)
		return;

	handler(s);

	s.reset(new boost::asio::ip::tcp::socket(service));	// reuse client socket wrapper
	start_accept(service, acc, s, handler);
}
