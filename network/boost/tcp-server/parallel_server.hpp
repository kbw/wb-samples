#pragma once

#include "defs.hpp"
#include <string>
#include <stdint.h>


void parallel_server(const std::string& host, uint16_t port, handler_t* handler);
