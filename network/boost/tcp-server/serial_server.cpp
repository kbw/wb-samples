#include "serial_server.hpp"

#include <boost/asio.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/io_service.hpp>


//---------------------------------------------------------------------------

void serial_handler(socket_ptr s, handler_t* handler);

void serial_server(const std::string& host, uint16_t port, handler_t* handler)
{
	boost::asio::io_service service;
	boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(host), port);

	boost::asio::ip::tcp::acceptor acceptor(service, endpoint);
	while (true)
	{
		socket_ptr client_socket(new boost::asio::ip::tcp::socket(service));
		acceptor.accept(*client_socket);

		serial_handler(client_socket, handler);
//		boost::thread( boost::bind(serial_handler, client_socket, handler) );
	}
}

void serial_handler(socket_ptr s, handler_t* handler)
{
	while (true)
	{
		handler(s);
	}
}
