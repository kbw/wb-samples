#pragma once

#include "defs.hpp"
#include <string>
#include <stdint.h>


void serial_server(const std::string& host, uint16_t port, handler_t* handler);
