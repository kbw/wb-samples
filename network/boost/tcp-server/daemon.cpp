/*
**      daemon.c     
**
**      Copyright (c) 2002 Paul Cevoli and Butterworth Heinemann
**
**      a skeleton daemon process
*/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/resource.h>
#include <strings.h>
#include <stdlib.h>

/*
**      name:   handle_sigcld
**      effect: catches the SIGCLD thrown by the child on process
**              termination. This keeps the child process from becoming
**              a zombie processes
*/
extern "C"
void handle_sigcld(int)
{
   pid_t	pid;
   int 	status;
   
   while ((pid = wait3(&status, WNOHANG, (struct rusage *)NULL)) > 0)
       ;
}

/*
**      name:   init_daemon
**      effect: forces the process to process to run in the background
**              as a daemon process
*/
extern "C"
void init_daemon()
{
   pid_t  childpid  = 0;
   int    fd        = 0;
   struct rlimit    max_files;

   /*
   **   create a child process 
   */
   if ((childpid = fork()) < 0)
   {
      /* handle the error condition */
      exit(-1);
   }
   else if (childpid > 0)
   {
      /* this is the parent, we may successfully exit */
      exit(0);
   }

   /* now executing as the child process */
   
   /* become the session leader */
   setsid();
   
   /*
   **    close all open file descriptors, need to get the maximum
   **    number of open files from getrlimit.
   */
   bzero(&max_files, sizeof(struct rlimit));
   getrlimit(RLIMIT_NOFILE, &max_files);
   for (fd = 0; fd < (int)max_files.rlim_max; fd++)
   {
      close(fd);
   }
  
   /*
   **    the current working directory is held open by the kernel for the
   **    life of a process so the file system cannot be unmounted, reset
   **    the current working directory to root.
   */
   chdir("/");

   /*
   **    a process inherits file access permissions from the process
   **    which created it, reset the user access mask
   */
   umask(0);

   /*
   **    we don't care about the exit status but we need to cleanup after
   **    each process that handles a request so the system isn't flooded
   **    with zombie processes using resources
   */
   signal(SIGCHLD, handle_sigcld);
}
