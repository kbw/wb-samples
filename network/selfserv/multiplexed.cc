#include "multiplexed.hpp"
#include "msg.hpp"

#include <chrono>
#include <cstring>
#include <iostream>
#include <iomanip>

namespace {
inline struct timeval operator-(struct timeval a, struct timeval b) {
	struct timeval c{ .tv_sec = a.tv_sec - b.tv_sec, .tv_usec = a.tv_usec - b.tv_usec };
	if (c.tv_usec < 0) {
		constexpr long sec_as_usec = 1000*1000;
		--c.tv_sec;
		c.tv_usec += sec_as_usec;
	}
	return c;
}

inline std::ostream& operator<<(std::ostream& os, const struct timeval& obj) {
	os << obj.tv_sec << '.' << std::setfill('0') << std::setw(6) << obj.tv_usec;
	return os;
}
}  // namespace

Multiplexed::Multiplexed(std::size_t ntcpclients, port_t serv_port) :
	buffer(std::make_unique<char[]>(bufsz)),
	buf(buffer.get()),
	ntcpclients_(ntcpclients),
	serv_port(serv_port) {
	int rc;

	tcpserv = ::socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); assert(tcpserv >= 0);
	std::cout << "Multiplexed: tcpserv=" << tcpserv << '\n';

	const int enable = 1;
	rc = ::setsockopt(tcpserv, SOL_SOCKET, SO_REUSEADDR, (const char*)&enable, sizeof(int)); assert(rc == 0);

	const linger lin{.l_onoff = 1, .l_linger = 0};
	rc = ::setsockopt(tcpserv, SOL_SOCKET, SO_LINGER, (const char*)&lin, sizeof(lin)); assert(rc == 0);

	sockaddr_in addr = create_server_addr(serv_port);
	rc = ::bind(tcpserv, (sockaddr*)&addr, sizeof(addr)); assert(rc == 0);
	rc = ::listen(tcpserv, 64); assert(rc == 0);
	info.try_emplace(tcpserv, addr);
}

Multiplexed::~Multiplexed() {
	close(tcpserv);
	close_all();
	assert(info.empty());
}

void Multiplexed::run() {
	using namespace std::chrono_literals;
	auto stop = std::chrono::steady_clock::now() + 3s;
	for (int loop = 0; std::chrono::steady_clock::now() < stop; ++loop) {
		reactor();
	} // for loop
}

void Multiplexed::reactor() {
	fd_set rdset;
	FD_ZERO(&rdset);

	FD_SET(tcpserv, &rdset);
	auto mxfd = tcpserv;
	for (auto tcphandler : tcphandlers_) {
		FD_SET(tcphandler, &rdset);
		mxfd = std::max(mxfd, tcphandler);
	}
	for (auto tcpclient : tcpclients_) {
		FD_SET(tcpclient, &rdset);
		mxfd = std::max(mxfd, tcpclient);
	}

	constexpr timeval delay{.tv_sec = 0, .tv_usec = 1};
	timeval timeout = delay;
	int nfds = ::select(mxfd + 1, &rdset, nullptr, nullptr, &timeout);
//	std::cout << "waited=" << (delay - timeout) << " remaining=" << timeout << '\n';

	if (nfds == 0) {
		onTimeout();
		return;
	}
	if (FD_ISSET(tcpserv, &rdset)) {
		onTcpServerAccept(tcpserv);
	}
	for (auto tcphandler : tcphandlers_) {
		if (FD_ISSET(tcphandler, &rdset)) {
			onTcpHandlerRead(tcphandler);
		}
	}
	for (auto tcpclient : tcpclients_) {
		if (FD_ISSET(tcpclient, &rdset)) {
			onTcpClientRead(tcpclient);
		}
	}
}

void Multiplexed::onTimeout() {
	if (state_ == State::init) {
		state_ = State::create_clients;
		state_info_.create_clients.index = 0;
	}
	if (state_ == State::create_clients) {
		if (state_info_.create_clients.index >= ntcpclients_) {
			state_ = State::client_send_hello;
		}

		// init: create some clients
//		std::cout << "onTimeout: index=" << state_info_.create_clients.index << " create_tcpclients(port=" << serv_port << ")\n";
		create_tcpclients(serv_port);
	}
	if (state_ == State::client_send_hello) {
//		std::cout << "onTimeout: state=" << (int)state_ << " tcpclients=" << tcpclients_.size() << '\n';
		for (auto tcpclient : tcpclients_) {
			ClientHello msg("id=" + std::to_string(tcpclient));
			std::cout << "onTimeout: send: " << msg.to_string() << '\n';
			::send(tcpclient, msg.to_string().data(), msg.to_string().size(), 0);
		}
		state_ = State::run;
	}
//	std::cout << "onTimeout: state=" << (int)state_ << '\n';
}

void Multiplexed::onTcpServerAcceptAll(socket_t s) {
	do {
		onTcpServerAccept(s);
	}
	while (can_read(s));
}

void Multiplexed::onTcpServerAccept(socket_t server) {
	socklen_t len = bufsz;
	auto tcphandler = ::accept(server, (sockaddr*)buf, &len);
	std::cout << "onTcpServerAccept: accept(" << server << ") = " << tcphandler << '\n';
	assert(tcphandler != -1);
	assert(len == sizeof(sockaddr_in));

	tcphandlers_.push_back(tcphandler);
	info.try_emplace(tcphandler, *(sockaddr_in*)buf);
}

void Multiplexed::onTcpHandlerRead(socket_t tcphandler) {
	auto nbytes = ::recv(tcphandler, buf, bufsz, 0); assert(nbytes > 0);
//	std::cout << "onTcpHandlerRead: recv(" << std::string_view(buf, nbytes) << ") = " << nbytes << '\n';
	auto msg = Msg::factory({buf, bufsz});
	if (const auto* client_hello = dynamic_cast<ClientHello*>(msg.get())) {
		std::cout << "onTcpHandlerRead: recvived text=" << client_hello->text() << '\n';
	}
}

void Multiplexed::onTcpClientRead(socket_t tcpclient) {
	auto nbytes = ::recv(tcpclient, buf, bufsz, 0); assert(nbytes > 0);
	std::cout << "onTcpHandlerRead: recv(" << std::string_view(buf, nbytes) << ") = " << nbytes << '\n';
}

void Multiplexed::close_all() {
	for (auto tcpclient : tcpclients_) {
		close(tcpclient);
	}
	for (auto tcphandler : tcphandlers_) {
		close(tcphandler);
	}
}

void Multiplexed::close(socket_t s) {
#if defined(_WIN32) || defined(_WIN64)
	::closesocket(s);
#else
	::close(s);
#endif
	auto p = info.find(s); assert(p != info.end());
	info.erase(p);
	std::cout << "close: close(" << s << ")" << '\n';
}

bool Multiplexed::can_read(socket_t s) {
	fd_set rdset;
	FD_ZERO(&rdset);

	FD_SET(s, &rdset);
	timeval nodelay{0, 0};
	int nfds = ::select(s + 1, &rdset, nullptr, nullptr, &nodelay);

	return nfds == 1;
}

sockaddr_in Multiplexed::create_server_addr(port_t port) {
	sockaddr_in addr;
	std::memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;
	return addr;
}

sockaddr_in Multiplexed::create_client_addr(port_t port) {
	sockaddr_in addr;
	std::memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	return addr;
}

void Multiplexed::create_tcpclients(port_t port) {
	if (state_ != State::create_clients) {
		return;
	}

	auto& index = state_info_.create_clients.index;
	if (index >= ntcpclients_) {
		return;
	}

	auto addr = create_client_addr(port);
	auto tcpclient = ::socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); assert(tcpclient >= 0);
	std::cout << "create_tcpclients: tcpclient=" << tcpclient << '\n';
	auto rc = ::connect(tcpclient, (sockaddr*)&addr, sizeof(addr)); assert(rc == 0);
	tcpclients_.push_back(tcpclient);
	info.try_emplace(tcpclient, addr);
	++index;
//	std::clog << "create_tcpclient: s=" << tcpclient << '\n';
}
