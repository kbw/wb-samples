#pragma once
#include <memory>
#include <string>
#include <string_view>

struct Msg {
	virtual ~Msg() = default;
	static std::unique_ptr<Msg> factory(std::string_view in);
};

struct ClientHello : public Msg {
	ClientHello(std::string_view text) : msg_(construct(text)) {
	}
	std::string_view to_string() const {
		return {msg_.data(), msg_.size()};
	}
	std::string_view text() const {
		return {msg_.data() + token().size(), msg_.size() - token().size()};
	}
	static std::string_view token() {
		return token_;
	}

private:
	static std::string construct(std::string_view text) {
		std::string str;
		str.reserve(token_.size() + text.size());
		str.append(token_);
		str.append(text);
		return str;
	}

	static constexpr std::string_view token_{"HELLO: "};
	const std::string msg_;
};
