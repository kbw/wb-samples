#include "msg.hpp"

#include <cstring>

std::unique_ptr<Msg> Msg::factory(std::string_view in) {
	if (in.size() >= ClientHello::token().size() &&
	    memcmp(ClientHello::token().data(), in.data(), ClientHello::token().size()) == 0) {
		return std::make_unique<ClientHello>(in);
	}
	return {};
}
