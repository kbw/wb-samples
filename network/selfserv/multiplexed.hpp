#pragma once
#include "defs.hpp"

#include <cstring>
#include <map>
#include <memory>
#include <vector>

class Multiplexed {
public:
	Multiplexed(std::size_t ntcpclients = 3, port_t serv_port = 19123);

	virtual ~Multiplexed();
	virtual void run();
	virtual void reactor();
	virtual void onTimeout();
	virtual void onTcpServerAcceptAll(socket_t s);
	virtual void onTcpServerAccept(socket_t s);
	virtual void onTcpHandlerRead(socket_t s);
	virtual void onTcpClientRead(socket_t s);

private:
	void close(socket_t s);
	void close_all();
	bool can_read(socket_t s);
	sockaddr_in create_server_addr(port_t port);
	sockaddr_in create_client_addr(port_t port);
	void create_tcpclients(port_t port);

private:
	struct Info {
		Info(const sockaddr_in &in) {
			std::memcpy(&addr, &in, sizeof(addr));
		}
		sockaddr_in addr;
	};

	static constexpr std::size_t bufsz = 16*1024;
	std::unique_ptr<char[]> buffer; // = std::make_unique<char>(bufsz);
	char* const buf; // = buffer.get();

	enum class State { init, create_clients, client_send_hello, run };
	union StateInfo {
		StateInfo() {
			create_clients.index = std::numeric_limits<decltype(create_clients.index)>::max();
		}
		struct {
			std::size_t index;
		} create_clients;
	};
	State state_ = State::init;
	StateInfo state_info_;

	std::size_t ntcpclients_{};
	port_t serv_port = 19123;
    socket_t tcpserv = -1;

	std::map<socket_t, Info> info;
	std::vector<socket_t> tcphandlers_;
	std::vector<socket_t> tcpclients_;
};
