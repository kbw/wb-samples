#include "multiplexed.hpp"

int main(int argc, char* argv[]) {
	std::size_t ntcpclients = (argc > 1 ? std::atoi(argv[1]) : 3);
	port_t serv_port = (argc > 2 ? std::atoi(argv[2]) : 19123);

	Multiplexed mplx(ntcpclients, serv_port);
	mplx.run();
}
