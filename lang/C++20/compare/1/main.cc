#include <spdlog/spdlog.h>

#include <cmath>
#include <compare>
#include <ostream>

class real {
	std::optional<double> val_{};

public:
	real() = default;

	template <typename U>
	real(U val) : val_(static_cast<double>(val)) {
	}

	bool isnan() const {
		return !val_ || std::isnan(*val_);
	}

	bool isinf() const {
		return !isnan() && std::isinf(*val_);
	}

	bool ispos() const {
		return !isnan() && *val_ > 0.0;
	}

	template <typename U>
	U as() const;

	friend std::ostream& operator<<(std::ostream& os, const real& value);

	friend real operator/(const real& lhs, const real& rhs);
	friend std::partial_ordering operator<=>(const real& lhs, const real& rhs);
};

template <>
double real::as() const {
	return val_.value_or(0.0);
}

std::ostream& operator<<(std::ostream& os, const real& value) {
	if (value.isnan())
		os << "nan";
	else
		os << value.as<double>();
	return os;
}

real operator/(const real& lhs, const real& rhs) {
	if (lhs.isnan() || rhs.isnan())
		return {};
	return *lhs.val_ / *rhs.val_;
}

std::partial_ordering operator<=>(const real& lhs, const real& rhs) {
	if (lhs.isnan() || rhs.isnan())
		return std::partial_ordering::unordered;

	auto delta = *lhs.val_ - *rhs.val_;
	if (delta < 0)
		delta *= -1.0;
	if (delta > std::numeric_limits<double>::epsilon())
		return *lhs.val_ <=> *rhs.val_;
	else
		return std::partial_ordering::equivalent;
}

bool operator==(const real& lhs, const real& rhs) {
	return std::is_eq(lhs <=> rhs);
}

template<>
struct fmt::formatter<real> {
	template<typename ParseContext>
	constexpr auto parse(ParseContext& ctx) {
		return ctx.begin();
	}

	template <typename FormatContext>
	auto format(const real& number, FormatContext& ctx) {
		if (number.isnan())
			return fmt::format_to(ctx.out(), "nan");
		return fmt::format_to(ctx.out(), "{}", number.as<double>());
	}
};

int main() {
	real a, b{3}, c{4.0f};
	spdlog::info("isnan: a:{} b:{} c:{}", a.isnan(), b.isnan(), c.isnan());

	auto a_lt_c = a < c;
	auto b_lt_c = b < c;
	spdlog::info("a<b:{} b<c:{}", a_lt_c, b_lt_c);
	auto a_gt_c = a > c;
	auto b_gt_c = b > c;
	spdlog::info("a>b:{} b>c:{}", a_gt_c, b_gt_c);

	real num{6}, den{2}, res{3};
	auto ok = std::is_eq(num/den <=> res);
	auto eq = num/den == res; // added operator==
	spdlog::info("{}/{} <=> {}: {}, eq: {}",
			num, den, res, ok ? "equal" : "unequal", eq);
}
