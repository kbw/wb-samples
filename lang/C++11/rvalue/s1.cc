#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <algorithm>
#include <memory>

// ---------------------------------------------------------------------------

class SocketImp;

typedef int socket_t;

class Socket
{
	std::unique_ptr<SocketImp> m_pImp;

	Socket(const Socket &);
	Socket& operator=(const Socket &);

protected:
	void destroy();

public:
	Socket();
	Socket(socket_t s);
	Socket(int family, int type, int proto);
	Socket(Socket &&s);
	~Socket();

	Socket& operator=(Socket &&);

	void swap(Socket &s);

	operator socket_t () const;
};

class SocketImp
{
	socket_t m_s;

	SocketImp(const SocketImp &n);
	SocketImp& operator=(const SocketImp &);

protected:
	void destroy();

public:
	SocketImp();
	~SocketImp();
	SocketImp(socket_t s);

	socket_t get_socket() const;
};

// ---------------------------------------------------------------------------

Socket::Socket() :
	m_pImp(new SocketImp)
{
}

Socket::Socket(socket_t s) :
	m_pImp(new SocketImp(s))
{
}

Socket::Socket(int family, int type, int proto) :
	m_pImp(new SocketImp(socket(family, type, proto)))
{
}

Socket::Socket(Socket &&s) :
	m_pImp(std::move(s.m_pImp))
{
}

Socket::~Socket()
{
	destroy();
}

Socket& Socket::operator=(Socket &&n)
{
	if (this != &n)
	{
		m_pImp = std::move(n.m_pImp);
	}
	return *this;
}

void Socket::destroy()
{
//	delete m_pImp;
//	m_pImp = nullptr;
}

void Socket::swap(Socket &s)
{
	std::swap(this->m_pImp, s.m_pImp);
}

Socket::operator socket_t () const
{
	return m_pImp->get_socket();
}

// ---------------------------------------------------------------------------

SocketImp::SocketImp() :
	m_s(-1)
{
}

SocketImp::SocketImp(socket_t s) :
	m_s(s)
{
}

SocketImp::~SocketImp()
{
	destroy();
}

SocketImp& SocketImp::operator=(const SocketImp &n)
{
	if (this != &n)
	{
		destroy();
		m_s = n.m_s;
	}
	return *this;
}

void SocketImp::destroy()
{
	if (m_s != -1)
	{
		close(m_s);
		m_s = -1;
	}
}

socket_t SocketImp::get_socket() const
{
	return m_s;
}

// ---------------------------------------------------------------------------

int main()
{
	Socket s;
	Socket server(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP));;

	s.swap(server);

//	std::swap(s, server);

	return 0;
}
