#include <cxxabi.h>		// GNU C++ abi

#include <exception>
#include <stdexcept>
#include <string>
#include <iostream>

#include <stdlib.h>

namespace
{
	void terminate()
	{
		// catch recursion
		static bool terminating = false;
		if (terminating)
		{
			abort();
		}
		terminating = true;

		// Did we get an exception?
		if (std::type_info *t = abi::__cxa_current_exception_type())
		{
			// get its name
			char const *mangled_name = t->name();
			int status = -1;
			char* name = abi::__cxa_demangle(mangled_name, 0, 0, &status);
			std::cout << "terminate: on: " << name << std::endl;
			if (status == 0)
			{
				free(name), name = NULL;
			}

			// If the exception is derived from std::exception, we can give more information.
			__try
			{
				__throw_exception_again;
			}
#ifdef __EXCEPTIONS
			__catch (const std::exception& e)
			{
				std::cout << "terminate: description: " << e.what() << std::endl;
			}
#endif
			__catch (...)
			{
				std::cout << "terminate: no futher information available" << std::endl;
			}
		}

		exit(1);
	}
}

int main()
{
	std::terminate_handler hdlr = std::set_terminate(terminate);

	std::string s;
	s.at(0) = '3';
	return 0;
}
