#include <exception>
#include <stdexcept>
#include <string>
#include <iostream>

void handle_eptr(std::exception_ptr eptr) // passing by value is ok
{
	try
	{
		if (eptr != std::exception_ptr())
		{
			std::rethrow_exception(eptr);
		}
	}
	catch(const std::exception& e)
	{
		std::cout << "Caught exception \"" << e.what() << "\"\n";
	}
}

int main()
{
	std::exception_ptr eptr;

	try
	{
		std::string().at(1); // this generates an std::out_of_range
	}
	catch (...)
	{
		eptr = std::current_exception(); // capture
	}

	handle_eptr(eptr);
}
