#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <iomanip>

std::ostream& operator<<(std::ostream& os, const timeval &tv)
{
	using namespace std;

	os << tv.tv_sec << '.' << setw(6) << setfill('0') << tv.tv_usec;
	return os;
}

timeval operator-(const timeval &a, const timeval &b)
{
	timeval c = { a.tv_sec - b.tv_sec, a.tv_usec - b.tv_usec };
	if (c.tv_usec < 0)
	{
		// carry
		--c.tv_sec;
		c.tv_usec += 1000000;
	}

	return c;
}

timeval test()
{
	timeval tv_start, tv_stop;
	gettimeofday(&tv_start, NULL);

	static struct timespec ts_delay = { 0, 0 };
	nanosleep(&ts_delay, NULL);		// hopefully this'll force a context switch

	gettimeofday(&tv_stop, NULL);

	return tv_stop - tv_start;
}

int main()
{
	for (int i = 0; i != 20; ++i)
		std::cout << i << '\t' << test() << std::endl;
	
	return 0;
}
