#include "socket.hpp"
#include "socketimp.hpp"

#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>

#include <algorithm>
#include <memory>

// ---------------------------------------------------------------------------

Socket::Socket() :
	m_pImp(new SocketImp)
{
}

Socket::Socket(socket_t s) :
	m_pImp(new SocketImp(s))
{
}

Socket::Socket(int family, int type, int proto) :
	m_pImp(new SocketImp(socket(family, type, proto)))
{
}

Socket::Socket(Socket &&s) :
	m_pImp(std::move(s.m_pImp))
{
}

Socket::~Socket()
{
	destroy();
}

Socket& Socket::operator=(Socket &&n)
{
	if (this != &n)
	{
		m_pImp = std::move(n.m_pImp);
	}
	return *this;
}

void Socket::destroy()
{
	delete m_pImp;
	m_pImp = nullptr;
}

void Socket::swap(Socket &s)
{
	std::swap(this->m_pImp, s.m_pImp);
}

Socket::operator socket_t () const
{
	return m_pImp->get_socket();
}

// ---------------------------------------------------------------------------

UDPSocket::UDPSocket() :
	Socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)
{
}

void UDPSocket::Bind(const IP4Address &)
{
}

void UDPSocket::Send(const IP4Address &, const std::string &)
{
}

std::string UDPSocket::Recv(IP4Address &)
{
	return std::string();
}
