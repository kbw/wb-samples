#pragma once

#include "defs.h"

class SocketImp
{
	socket_t m_s;

	SocketImp(const SocketImp &n);
	SocketImp& operator=(const SocketImp &);

protected:
	void destroy();

public:
	SocketImp();
	~SocketImp();
	SocketImp(socket_t s);

	socket_t get_socket() const;
};
