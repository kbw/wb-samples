#include "socketimp.hpp"

SocketImp::SocketImp() :
	m_s(static_cast<socket_t>(-1))
{
}

SocketImp::~SocketImp()
{
	destroy();
}

void SocketImp::destroy()
{
	if (m_s != static_cast<socket_t>(-1))
	{
		close(m_s);
		m_s = -1;
	}
}

socket_t SocketImp::get_socket() const
{
	return m_s;
}
