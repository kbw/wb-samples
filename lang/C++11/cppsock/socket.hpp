#pragma once

#include "defs.h"
#include <memory>

class IP4Address;
class SocketImp;

class Socket
{
	std::unique_ptr<SocketImp> m_pImp;

	Socket(const Socket &) = delete;
	Socket& operator=(const Socket &) = delete;

protected:
	Socket(int family, int type, int proto);
	void destroy();

public:
	Socket();
	Socket(socket_t s);
	Socket(Socket &&s);
	~Socket();

	Socket& operator=(Socket &&);

	void swap(Socket &s);

	operator socket_t () const;
};

class UDPSocket : public Socket
{
public:
	UDPSocket();
	void Bind(const IP4Address &);
	void Send(const IP4Address &, const std::string &);
	std::string Recv(IP4Address &);
};

class TCPServer : public Socket
{
};

class TCPClient : public Socket
{
};
