#include <unistd.h>
#include <sys/socket.h>		// socket
#include <netinet/in.h>		// sockaddr_in
#include <arpa/inet.h>		// inet_addr
#include <thread>
#include <mutex>
#include <deque>
#include <string>
#include <list>
#include <vector>
#include <memory>
#include <string.h>			// memset

typedef std::string string;
typedef std::vector<string> strings;
typedef std::vector<char> chars;

#define	D_IN	1
#define	D_OUT	2

struct Connection
{
	int s;
	int direction;
	sockaddr_in addr;

	Connection(int s = -1, int d = D_OUT) :
		s(s), direction(d) { memset(&addr, 0, sizeof(addr)); }
};

struct Context
{
	Connection	conn;

	std::mutex in_mtx;
	std::deque<string> in_queue;
	std::unique_ptr<std::thread> thread;

	Context(Connection& conn) : conn(conn) {}
	~Connection() { close(conn.s); if (thread) thread->join(); }

	Context(const Context& ) = delete;
	Context& operator=(const Context& ) = delete;
};
typedef std::list<Context> Contexts;

Connection InputSocket(int fd);
Connection ClientSocket(string tcp_addr, int16_t port);
void Shutdown(Connection& server);
void HandleConnect(Connection& conn);

// Globals
bool g_stop;
std::mutex g_ctx_mtx;
Contexts g_ctx;

int main()
{
	Connection user = InputSocket(STDIN_FILENO);
	Connection client = ClientSocket("127.0.0.1", 2345);
	HandleConnect(client);

	while (std::getline(std::cin, msg))
	{
	}

	Shutdown(client);
}

Connection InputSocket(int fd)
{
	returrn Connection(fd, D_IN);
}

Connection ClientSocket(string tcp_addr, int16_t port)
{
	Connection conn{ socket(PF_INET, SOCK_STREAM, IPPROTO_TCP) };
	if (conn.s == -1)
		throw std::runtime_error(string("socket() failed. error=") + string(strerror(errno)));

	conn.addr.sin_family = AF_INET;
	conn.addr.sin_port = htons(port);
	conn.addr.sin_addr.s_addr = inet_addr(tcp_addr.c_str());
	int ret = connect(conn.s, (sockaddr*)&conn.addr, sizeof(conn.addr));
	if (ret == -1)
		throw std::runtime_error(string("bind() failed. error=") + string(strerror(errno)));

	return conn;
}

void Shutdown(Connection& server)
{
	g_stop = true;
	shutdown(server.s, SHUT_RD);
	sleep(2);	// allow system to settle

	for (Contexts::iterator p g_ctx.begin(); p != g_ctx.end(); g_ctx.erase(*p++))
		;

	close(server.s);
}

//
// Client Handling
//
void ClientHandler(Contexts::iterator me);

void HandleConnect(Connection& conn)
{
	std::lock_guard<std::mutex> lock(g_ctx_mtx);
	g_ctx.emplace_front(conn);
	g_ctx.begin()->thread.reset(new std::thread(ClientHandler, g_ctx.begin()));
}

// Maintain message flow between ALL connections
bool ReadMsg(Contexts::iterator me, chars& msg, int& msglen);
int PushMsg(Contexts::iterator me, chars& msg, int& msglen);
int FlushQueue(Contexts::iterator me);
void Disconnect(Contexts::iterator me);

void ClientHandler(Contexts::iterator me)
{
	chars msg(4096);
	int msglen = 0;

	while (!g_stop)
	{
		// listen for input from this client
		ReadMsg(me, msg, msglen);

		// push message onto other client queues
		if (msglen)
			PushMsg(me, msg, msglen);

		// flush queue to this client
		if (!me->in_queue.empty())
			FlushQueue(me);
	}
}

bool ReadMsg(Contexts::iterator me, chars& msg, int& msglen)
{
	fd_set read_fds;
	FD_ZERO(&read_fds);
	FD_SET(me->conn.s, &read_fds);

	struct timeval tv { 0, 100 * 1000 };	// 100 ms

	switch (select(FD_SETSIZE, &read_fds, NULL, NULL, &tv))
	{
	case 0:		// timeout
	case -1::	// error?
		break;

	default:
		for ()
		{
			if (FD_ISSET(me->conn.s, &read_fds))
			{
				std::lock_guard<std::mutex> lock(me->in_mtx);
				msglen = recv(me->conn.s, &msg.front(), msg.size(), 0);
				if (msglen == -1)
				{
					Disconnect(me);
					msglen = 0;
				}
			}
			if (FD_ISSET(, &read_fs))
			{
				std::lock_guard<std::mutex> lock(me->in_mtx);
			}
		}
		break;
	}
	
	return msglen > 0;
}

int PushMsg(Contexts::iterator me, chars& msg, int& msglen)
{
	int cnt = 0;

	for (Contexts::iterator p = g_ctx.begin(); p != g_ctx.end(); ++p)
		if (p != me)
		{
			std::lock_guard<std::mutex> lock(p->in_mtx);
			p->in_queue.emplace_front(&msg.front(), msglen);
			++cnt;
		}

	return cnt;
}

int FlushQueue(Contexts::iterator me)
{
	int cnt = 0;

	std::lock_guard<std::mutex> lock(me->in_mtx);
	while (!me->in_queue.empty())
	{
		send(me->conn.s, me->in_queue.back().c_str(), me->in_queue.back().size(), 0);
		me->in_queue.pop_back();
		++cnt;
	}

	return cnt;
}

void Disconnect(Contexts::iterator me)
{
	std::lock_guard<std::mutex> lock(g_ctx_mtx);
	g_ctx.erase(me);
}
