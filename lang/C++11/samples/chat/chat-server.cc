#include <unistd.h>
#include <sys/socket.h>		// socket
#include <netinet/in.h>		// sockaddr_in
#include <arpa/inet.h>		// inet_addr
#include <thread>
#include <mutex>
#include <deque>
#include <string>
#include <list>
#include <vector>
#include <memory>
#include <string.h>			// memset

typedef std::string string;
typedef std::vector<string> strings;
typedef std::vector<char> chars;

struct Connection
{
	int s;
	sockaddr_in addr;

	Connection() : s(-1) { memset(&addr, 0, sizeof(addr)); }
};

struct Context
{
	Connection	conn;

	std::mutex out_mtx;
	std::deque<string> out_queue;
	std::unique_ptr<std::thread> thread;

	Context(Connection& conn) : conn(conn) {}
	~Context();
	Context(const Context &) = delete;
	Context& operator=(const Context &) = delete;
};
typedef std::list<Context> Contexts;

// Globals
bool g_stop;
std::mutex g_ctx_mtx;
Contexts g_ctx;

Connection ServerSocket(string tcp_addr, int16_t port);
bool Accept(Connection& server, Connection& client);
void Shutdown(Connection& server);
void HandleConnect(Connection& conn);

int main()
{
	Connection server = ServerSocket("0.0.0.0", 2345);

	Connection client;
	while (Accept(server, client))
		HandleConnect(client);

	Shutdown(server);
}

Connection ServerSocket(string tcp_addr, int16_t port)
{
	Connection conn;
	conn.s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (conn.s == -1)
		throw std::runtime_error(string("socket() failed. error=") + string(strerror(errno)));

	conn.addr.sin_family = AF_INET;
	conn.addr.sin_port = htons(port);
	conn.addr.sin_addr.s_addr = inet_addr(tcp_addr.c_str());
	int ret = bind(conn.s, (sockaddr*)&conn.addr, sizeof(conn.addr));
	if (ret == -1)
		throw std::runtime_error(string("bind() failed. error=") + string(strerror(errno)));

	ret = listen(conn.s, 50);
	if (ret == -1)
		throw std::runtime_error(string("listen() failed. error=") + string(strerror(errno)));

	return conn;
}

bool Accept(Connection& server, Connection& client)
{
	Connection local;
	socklen_t len = sizeof(local.addr);
	local.s = accept(server.s, (sockaddr*)&local.addr, &len);
	if (local.s == -1)
		return false;

	client = local;
	return true;
}

void Shutdown(Connection& server)
{
	g_stop = true;
	shutdown(server.s, SHUT_RD);
	sleep(4);	// allow system to settle

	for (Contexts::iterator p = g_ctx.begin(); p != g_ctx.end(); g_ctx.erase(p++))
		;

	close(server.s);
}

Context::~Context()
{
	close(conn.s);

	if (thread)
		thread->join();
}

//
// Client Handling
//
void ClientHandler(Contexts::iterator me);

void HandleConnect(Connection& conn)
{
	std::lock_guard<std::mutex> lock(g_ctx_mtx);
	g_ctx.emplace_front(conn);
	g_ctx.begin()->thread.reset(new std::thread(ClientHandler, g_ctx.begin()));
}

// Maintain message flow between ALL connections
bool ReadMsg(Contexts::iterator me, chars& msg, int& msglen);
int PushMsg(Contexts::iterator me, const chars& msg, int msglen);
int FlushQueue(Contexts::iterator me);
void Disconnect(Contexts::iterator me);

void ClientHandler(Contexts::iterator me)
{
	chars msg(4096);
	int msglen = 0;

	while (!g_stop)
	{
		// listen for input from this client
		ReadMsg(me, msg, msglen);

		// push message onto other client queues
		if (msglen)
			PushMsg(me, msg, msglen);

		// flush queue to this client
		if (!me->out_queue.empty())
			FlushQueue(me);
	}
}

bool ReadMsg(Contexts::iterator me, chars& msg, int& msglen)
{
	fd_set read_fds;
	FD_ZERO(&read_fds);
	FD_SET(me->conn.s, &read_fds);

	struct timeval tv { 0, 100 * 1000 };	// 100 ms

	switch (select(FD_SETSIZE, &read_fds, NULL, NULL, &tv))
	{
	case 1:
		if (FD_ISSET(me->conn.s, &read_fds))
		{
			std::lock_guard<std::mutex> lock(me->out_mtx);
			msglen = recv(me->conn.s, &msg.front(), msg.size(), 0);
			if (msglen == -1)
			{
				msglen = 0;
				Disconnect(me);
			}
		}
		break;

	case 0:		// timeout
	default:	// error?
		msglen = 0;
	}
	
	return msglen > 0;
}

int PushMsg(Contexts::iterator me, const chars& msg, int msglen)
{
	int cnt = 0;

	for (Contexts::iterator p = g_ctx.begin(); p != g_ctx.end(); ++p)
		if (p != me)
		{
			std::lock_guard<std::mutex> lock(p->out_mtx);
			p->out_queue.emplace_front(&msg.front(), size_t(msglen));
			++cnt;
		}

	return cnt;
}

int FlushQueue(Contexts::iterator me)
{
	int cnt = 0;

	std::lock_guard<std::mutex> lock(me->out_mtx);
	while (!me->out_queue.empty())
	{
		send(me->conn.s, me->out_queue.back().c_str(), me->out_queue.back().size(), 0);
		me->out_queue.pop_back();
		++cnt;
	}

	return cnt;
}

void Disconnect(Contexts::iterator me)
{
	close(me->conn.s);

	std::lock_guard<std::mutex> lock(g_ctx_mtx);
	g_ctx.erase(me);
}
