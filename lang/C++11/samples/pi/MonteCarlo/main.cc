// https://webbusy.wordpress.com/page/2/

#include <pthread.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <stdint.h>

namespace MonteCarloPI
{
    struct Info
    {
        typedef uint32_t value_type;

        Info() : inPoints(0), totalPoints(0) {}

        value_type inPoints;
        value_type totalPoints;
    };

    void* func(void* param)
    {
        if (Info* info = reinterpret_cast<Info*>(param))
        {
            // do some work
            for (size_t i = 0; i != info->totalPoints; ++i)
            {
                std::pair<double, double> pt(rand()/(double)RAND_MAX, rand()/(double)RAND_MAX);
                double d = pt.first*pt.first + pt.second*pt.second;
                if (d <= 1.0)
                    ++(info->inPoints);
            }
        }

        return param;
    }

    double pi(size_t nthreads, size_t niters)
    {
        std::vector<pthread_t> handles(nthreads);
        std::vector<Info> info(nthreads);

        // start the threads
        for (size_t i = 0; i != nthreads; ++i)
        {
            info[i].totalPoints = niters;
            pthread_create(&handles[i], NULL, func, &info[i]);
        }

        // wait for the threads to complete
        for (size_t i = 0; i != nthreads; ++i)
            pthread_join(handles[i], NULL);

        // process the reply
        double inPoints = 0.0;
        double totalPoints = 0.0;
        for (size_t i = 0; i != nthreads; ++i)
        {
            inPoints += info[i].inPoints;
            totalPoints += info[i].totalPoints;
        }
        const double pi = (4.0 * inPoints / totalPoints);

        return pi;
    }
}

int main()
{
    srand(2012);

    const size_t NTHREADS = 4;
    const size_t NITERS = 100*1000*1000;

    double pi = MonteCarloPI::pi(NTHREADS, NITERS);
    std::cout << "pi = " << pi << std::endl;

    return 0;
}

