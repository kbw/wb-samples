// from https://webbusy.wordpress.com/page/2/

#include <iostream>
#include <stdlib.h>
#include <stdint.h>

namespace LiebnizPI
{
    typedef uint32_t value_type;

    double pi(value_type niters)
    {
        double pi = 0.0;
        for (size_t i = 0; i != niters; ++i)
        {
            if (i % 2)
                pi -= 1.0 / (2*i + 1);
            else
                pi += 1.0 / (2*i + 1);
        }

        return 4.0 * pi;
    }
}

int main()
{
    srand(2012);

    const LiebnizPI::value_type NITERS = 1*1000*1000;

    double pi = LiebnizPI::pi(NITERS);
    std::cout << "pi = " << pi << std::endl;

    return 0;
}

