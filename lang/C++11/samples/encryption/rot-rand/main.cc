/*
	Write a text‐based program that can encrypt or decrypt a text file
	using the “unbreakable” One‐Time Pad cryptosystem. The program should read
	a text file that contains only English characters (upper and lower case) and blank
	spaces, and generate an encrypted or decrypted file following these rules:
	(1) For encryption: For each character p in the unencrypted file, generate a
	random number r between 1 and 53. The encrypted character C will be
	computed by the formula C = p + r. To do this you will need to enccrypt
	characters and blank spaces following exactly this mapping:
	‘a’ ‘b’ …   ‘z’ ‘A’ ‘B’ …   ‘Z’ [blank space]
	1   2 … 26 27 28 … 52          53
	Example: if your random number r is equal to 7, the unencrypted
	character ‘z’ will be encrypted to character C = ‘z’ + 7 =  33 = ‘H’. Note
	that this formula should wrap around; for example, for r = 6 and p = ‘Y’, C
	= ‘Y’ + 6 = 51 + 6 = 4 = ‘d’.
	The generated sequence of random numbers r should be stored in a
	separate file which is the secret key to decrypt the encrypted file.
	Also, of course, you need to generate an encrypted file containing the
	encrypted characters.
	To run the program the user should type otp -e <unencrypted_file>
	<output_file> <secret_file>, where unencrypted_file is the name of the file
	you want to encrypt, output_file is the name of the file you want to give to
	the encrypted file, and secret_file is the name of the file that will contain the
	generated random numbers which are the key to decrypt output_file at a
	later time.
	(2) For decryption: For each character C in the encrypted file, read the
	corresponding number r from the secret key file generated during
	encryption, and compute the unencrypted character p using the formula
	p = C – r.
	Example: if your secret key number for a particular character C is r = 7,
	the unencrypted character ‘H’ will be computed using the formula p = 33
	– 7 = 26 = ‘z’. Note again that this formula should wrap around; for
	example, for r = 6 and C = ‘d’, p = ‘d’ ‐ 6 = 4 ‐ 6 = 51 = ‘Y’.
	To run the program the user should type otp -d <encrypted_file>
	<output_file> <secret_file>, where encrypted_file is the name of the file
	you want to decrypt, output_file is the name of the file you want to give to
	the unencrypted file, and secret_file is the name of the file that contains the
	generated random numbers which are the key to decrypt the output_file.
 */

#include <fstream>
#include <iostream>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>

static const char chmap[] =
	"abcdefghijklmnopqrstuvwxyz"
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	" ";

char encrypt(char c, int r)
{
	// find the position of the char in the char map
	const char* p = std::find(chmap, chmap + sizeof(chmap), c);
	if (p == chmap + sizeof(chmap))
		throw std::domain_error("Input character out of range");

	// find the index of the char
	ptrdiff_t idx = p - chmap;

	// add on the random offset
	idx += r;
	idx = idx % sizeof(chmap);

	return chmap[idx];
}

char decrypt(char c, int r)
{
	// find the position of the char in the char map
	const char* p = std::find(chmap, chmap + sizeof(chmap), c);
	if (p == chmap + sizeof(chmap))
		throw std::domain_error("Input character out of range");

	// find the index of the char
	ptrdiff_t idx = p - chmap;

	// subtract on the random offset
	idx -= r;
	idx += sizeof(chmap);
	idx = idx % sizeof(chmap);

	return chmap[idx];
}

int run(int argc, char* argv[])
{
	if (argc == 5  &&  strcmp(argv[1], "-e") == 0)
	{
		std::ifstream unencrypted(argv[2]);
		std::ofstream outputfile(argv[3]);
		std::ofstream secretfile(argv[4]);

		srand(0);
		std::string line;
		while (std::getline(unencrypted, line))
		{
			for (size_t i = 0, mx = line.size(); i != mx; ++i)
			{
				int r = 1 + rand() % sizeof(chmap);
				outputfile << encrypt(line[i], r);
				secretfile << r << ' ';
			}
			outputfile << '\n';
			secretfile << '\n';
		}
	}
	else if (argc == 5  &&  strcmp(argv[1], "-d") == 0)
	{
		std::ifstream encrypted(argv[2]);
		std::ofstream outputfile(argv[3]);
		std::ifstream secretfile(argv[4]);

		std::string line;
		while (std::getline(encrypted, line))
		{
			for (size_t i = 0, mx = line.size(); i != mx; ++i)
			{
				int r = {};
				secretfile >> r;
				outputfile << decrypt(line[i], r);
			}
			outputfile << '\n';
		}
	}
	else
	{
		const char	*text[] =
		{
			"usage:\t[-e, -d] InputFile OutputFile RandomFile",
			0
		};

		for (size_t i = 0; text[i]; ++i)
			std::clog << text[i] << "\n";

		return -1;
	}

	return 0;
}

int main(int argc, char* argv[])
{
	try
	{
		return run(argc, argv);
	}
	catch (const std::exception &e)
	{
		std::clog << "error: " << e.what() << '\n';
		return -1;
	}
}
