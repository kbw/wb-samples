#include <string>
#include <vector>
#include <set>
#include <iostream>

// Create a function that returns the first non duplicate character.
// string has lowercase letters
char dup1(const std::string& s)
{
	struct rec_t {
		rec_t() : count(0), pos(-1) {}
		int count, pos;
	};

	std::vector<rec_t> recs;
	for (size_t i = 0; i != s.size(); ++i) {
		size_t idx = s[i] - ' ';
		if (idx >= recs.size())
			recs.resize( idx + 1 );
		rec_t& rec = recs[idx];

		rec.count++;
		if (rec.pos == -1)
			rec.pos = i;
	}

	std::set<int> idx;
	for (size_t i = 0; i != recs.size(); ++i)
		if (recs[i].count == 1)
			idx.insert(recs[i].pos);

	if (!idx.empty())
		return s[*idx.begin()];

	throw "duplicates not found";
}

int main(int argc, char* argv[])
{
	for (int i = 1; i < argc; ++i)
		try {
			std::cout << argv[i] << ": " << dup1(argv[i]) << std::endl;
		}
		catch (const char* err) {
			std::cout << err << std::endl;
		}
}
