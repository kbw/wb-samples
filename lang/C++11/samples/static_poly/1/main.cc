#include <iostream>

template <typename T>
struct shape{
	std::ostream& print(std::ostream& os) { return static_cast<T*>(this)->print_impl(os); }

	std::ostream& print_impl(std::ostream& os) { return os << "shape\n"; }
};

struct square : shape<square> {
	std::ostream& print_impl(std::ostream& os) { return os << "square\n"; }
};

struct circle : shape<circle> {
	std::ostream& print_impl(std::ostream& os) { return os << "circle\n"; }
};

int main() {
	square().print(std::cout);
	circle().print(std::cout);
}
