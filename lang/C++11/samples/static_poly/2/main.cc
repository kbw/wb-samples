#include <iostream>

template <typename T>
struct shape {
	static std::ostream& print(std::ostream &os) { return T::print_impl(os); }
};

struct square: public shape<square> {
//	friend struct shape<square>;
	static std::ostream& print_impl(std::ostream &os) { return os << "square\n"; }
};

struct circle: public shape<circle> {
//	friend struct shape<circle>;
	static std::ostream& print_impl(std::ostream &os) { return os << "circle\n"; }
};

int main() {
	square().print(std::cout);
	circle().print(std::cout);
}
