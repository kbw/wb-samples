#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <tuple>
#include <memory>
#include <vector>
#include <limits>
#include <stdexcept>
#include <fstream>

class bitvec
{
public:
	class indexop;

	typedef	uint64_t	num_t;
	typedef	num_t		idx_t;

public:
	bitvec(num_t len, bool state = true, num_t ll=1024*16);
	bitvec(const bitvec& n) = default;
	bitvec& operator=(const bitvec& n) = delete;
	~bitvec() = default;

	bool set(idx_t i);
	bool clr(idx_t i);
	bool get(idx_t i) const;
	num_t size() const;
	bool operator[](num_t i) const;
	indexop operator[](num_t i);

	class indexop
	{
	public:
		indexop(bitvec& a, idx_t i) : a(a), i(i) {}
		void chk(idx_t i) const;
		operator bool() const;
		bool operator=(bool n);

	private:
		bitvec& a;
		const num_t i;
	};

private:
	typedef	uint8_t		offset_t;
	typedef	num_t		mask_t;

	class segment
	{
	public:
		segment(num_t len, bool state);
		segment(const segment&) = delete;
		~segment();

		size_t	size() const				{ return m_sz; }
		num_t&	operator[](size_t i)		{ return m_array[i]; }
		num_t	operator[](size_t i) const	{ return m_array[i]; }

	private:
		typedef	num_t*	array_t;

		const num_t	m_len;
		const int	m_sz;
		array_t		m_array;
	};

	struct index_info : public std::tuple<size_t, idx_t, mask_t>
	{
		typedef	std::tuple<size_t, idx_t, mask_t> inherited;

		index_info(size_t seg, idx_t idx, mask_t m) :
			tuple(seg, idx, m)
		{}
		size_t	segment() const	{ return std::get<0>(*this); }
		idx_t	idx() const		{ return std::get<1>(*this); }
		mask_t	mask() const	{ return std::get<2>(*this); }
	};

	static const uint16_t	wordlen;

	index_info	index(idx_t i) const;

private:
	typedef std::unique_ptr<segment> segment_t;
	typedef std::vector<segment_t> segments_t;

	segments_t	m_seg;
};

int wordsize()
{
	int nbits = 1;
	for (unsigned m = std::numeric_limits<unsigned>::max(); m >>= 1; )
		++nbits;
	return nbits;
}

const uint16_t bitvec::wordlen = wordsize();

bitvec::segment::segment(num_t len, bool state) :
	m_len(len),
	m_sz(sizeof(num_t)*(1 + m_len/wordlen)),
	m_array(new num_t[m_sz])
{
	memset(m_array, state ? -1 : 0, m_sz);
}

bitvec::segment::~segment()
{
	delete [] m_array;
}

bitvec::bitvec(num_t len, bool state, num_t lowerlimit)
{
	m_seg.reserve(8);
	size_t n = 1;
	while (m_seg.size() < n)
	{
		try
		{
			m_seg.push_back(
				std::unique_ptr<segment>(
					new segment(len, state)));
		}
		catch (const std::bad_alloc &)
		{
			if (len < lowerlimit)
				return;	// give up

			n <<= 1;
			len >>= 1;
		}
	}
}

inline
bitvec::index_info bitvec::index(bitvec::idx_t const i) const
{
	// find segment from relative index
	num_t	relidx = i / wordlen;
	size_t	seg;
	for (seg = 0; relidx > m_seg[seg]->size(); ++seg)
		relidx -= m_seg[seg]->size();

	num_t	idx    = i / wordlen;
	offset_t offset = i % wordlen;

	mask_t mask = 1;
	mask <<= offset;

	return index_info(seg, idx, mask);
}

inline
bool bitvec::set(idx_t i)
{
	index_info val = index(i);
	segment& a = *m_seg[val.segment()];
	idx_t idx = val.idx();
	mask_t mask = val.mask();

//	printf("\ta[%llu] |= 0x%016llx\n", idx, mask);
	a[idx] |= mask;
	return a[idx] & mask;
}

inline
bool bitvec::clr(idx_t i)
{
	index_info val = index(i);
	segment& a = *m_seg[val.segment()];
	idx_t idx = val.idx();
	mask_t mask = val.mask();

//	printf("\ta[%u][%llu] &= 0x%016llx\n", val.segment(), idx, ~mask);
	a[idx] &= ~mask;
	return a[idx] & mask;
}

inline
bool bitvec::get(idx_t i) const
{
	index_info val = index(i);
	const segment& a = *m_seg[val.segment()];
	idx_t idx = val.idx();
	mask_t mask = val.mask();

	return a[idx] & mask;
}

bitvec::num_t bitvec::size() const
{
	num_t sz = 0;
	for (size_t i = 0; i != m_seg.size(); ++i)
		sz += m_seg[i]->size();

	return sz * wordlen;
}

inline
bool bitvec::operator[](num_t i) const
{
	return get(i);
}

inline
bitvec::indexop bitvec::operator[](num_t i)
{
	return indexop(*this, i);
}

inline
void bitvec::indexop::chk(idx_t ) const
{
/*
#ifndef NDEBUG
	if (i >= a.m_len)
	{
		char buf[32];
		snprintf(buf, sizeof(buf), "%llu >= %llu", i, a.m_len);

		throw std::out_of_range(buf);
	}
#endif
 */
}

inline
bitvec::indexop::operator bool() const
{
	chk(i);
	return a.get(i);
}

inline
bool bitvec::indexop::operator=(bool val)
{
	chk(i);
	return val ? a.set(i) : a.clr(i);
}

void sieve(bitvec& a, bitvec::num_t sz)
{
	// filter even numbers
	for (bitvec::num_t j = 2*2; j < sz; j += 2)
		a[j] = false;

	// filter odd indexes of odd numbers not already cleared
	for (bitvec::num_t i = 3, mx = sz; i < mx; i += 2)
		if (a[i])
			for (bitvec::num_t j = 3*i; j < sz; j += 2*i)
				a[j] = false;
}

int main(int argc, char* argv[])
{
	if (argc == 1)
		return 0;
	const bitvec::num_t len = strtoll(argv[1], NULL, 0);

	bitvec a(len + 1);
	if (len != a.size())
		fprintf(stderr, "actual len=%llu\n", a.size());
	sieve(a, len + 1);

	int count = 0;
	for (bitvec::num_t i = 1; i <= len; ++i)
		if (a[i])
			++count;
	printf("%d\n", count);

	std::ofstream os("primes.txt");
	for (bitvec::num_t i = 1; i <= len; ++i)
		if (a[i])
			os << i << "\n";
}
