extern "C"
{
  #include "timer.h"
}
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>

int getsize(int argc, char* argv[]);
int intcmp(const void* arg1, const void* arg2);

int main(int argc, char* argv[])
{
	srand(2016);	// fix for repeatable sequence
	int sz = getsize(argc, argv);

	int* arr = (int*)calloc(sz, sizeof(int));
	std::vector<int> vec0(sz), vec1(sz);
	for (int i = 0; i != sz; ++i)
		arr[i] = vec0[i] = vec1[i] = rand();

	hrtime_t t[6];
	t[0] = get_hrtime();
	qsort(arr, sz, sizeof(int), intcmp);
	t[1] = get_hrtime();

	t[2] = get_hrtime();
	std::sort(vec0.data(), vec0.data() + sz);
	t[3] = get_hrtime();

	t[4] = get_hrtime();
	std::sort(vec1.begin(), vec1.end());
	t[5] = get_hrtime();

	hrtime_t d[3];
	d[0] = diff_hrtime(t[0], t[1]);
	d[1] = diff_hrtime(t[2], t[3]);
	d[2] = diff_hrtime(t[4], t[5]);
	printf("%d\t%ld.%09ld\t%ld.%09ld\t%ld.%09ld\n",
		sz,
		d[0].tv_sec, d[0].tv_nsec,
		d[1].tv_sec, d[1].tv_nsec,
		d[2].tv_sec, d[2].tv_nsec);

	free(arr);
}

int intcmp(const void* arg1, const void* arg2)
{
	int& a = *(int*)arg1;
	int& b = *(int*)arg2;

	if (a < b) return -1;
	if (a > b) return 1;
	return 0;
}

int getsize(int argc, char* argv[])
{
	long n = 16 * 1024 * 1024;

	if (argc == 2)
	{
		char* type = nullptr;
		n = strtol(argv[1], &type, 0);
		switch (*type)
		{
		case 'g':
		case 'G':
			n *= 1024 * 1024 * 1024;
			break;
		case 'm':
		case 'M':
			n *= 1024 * 1024;
			break;
		case 'k':
		case 'K':
			n *= 1024;
			break;
		}
	}

	return (int)n;
}
