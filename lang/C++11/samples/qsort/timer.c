#include "timer.h"

#if defined(MACOS)
#include <mach/mach.h>
#include <mach/mach_time.h>
#endif

#include <stdlib.h>
#include <string.h>		// memset

hrtime_t zero_hrtime(void)
{
#if defined(MACOS)
	return 0;
#elif defined(WINDOWS)
	LARGE_INTEGER val;
	memset(&val, 0, sizeof(val));
	return val;
#else
	hrtime_t tv;
	memset(&tv, 0, sizeof(tv));
	return tv;
#endif
}

hrtime_t get_hrtime(void)
{
#if defined(MACOS)
	return mach_absolute_time();
#elif defined(WINDOWS)
	LARGE_INTEGER val;
	QueryPerformanceCounter(&val);
	return val;
#else
	hrtime_t tv;
	clock_gettime(CLOCK_MONOTONIC, &tv);
	return tv;
#endif
}

hrtime_t diff_hrtime(hrtime_t a, hrtime_t b)
{
#if defined(MACOS)
	static mach_timebase_info_data_t ref;
	if (ref.denom == 0)
	{
		mach_absolute_info(&ref);

		while (((int)ref.denom & 1) && (ref.denom != 1) && !((int)ref.numer & 1))
		{
			ref.denom >>= 1;
			ref.numer >>= 1;
		}
	}

	return ref.numer * (b - a) / ref.denom;
#elif defined(WINDOWS)
	static hrtime_t ref = 0;
	if (ref == 0)
		QueryPerformanceFrequency(&ref);

	return (b - a) / ref;
#else
	hrtime_t c;

    c.tv_sec = b.tv_sec - a.tv_sec;
    c.tv_nsec = b.tv_nsec - a.tv_nsec;
    if (c.tv_nsec < 0)
    {
        c.tv_nsec += TIME_NSEC;
        --c.tv_sec;
    }

	return c;
#endif
}

hrtime_t sum_hrtime(hrtime_t a, hrtime_t b)
{
#if defined(MACOS)
	static mach_timebase_info_data_t ref;
	if (ref.denom == 0)
	{
		mach_absolute_info(&ref);

		while (((int)ref.denom & 1) && (ref.denom != 1) && !((int)ref.numer & 1))
		{
			ref.denom >>= 1;
			ref.numer >>= 1;
		}
	}

	return ref.numer * (b + a) / ref.denom;
#elif defined(WINDOWS)
	static hrtime_t ref = 0;
	if (ref == 0)
		QueryPerformanceFrequency(&ref);

	return (b + a) / ref;
#else
	hrtime_t c;

    c.tv_sec = b.tv_sec + a.tv_sec;
    c.tv_nsec = b.tv_nsec + a.tv_nsec;
    if (c.tv_nsec >= TIME_NSEC)
    {
        c.tv_nsec -= TIME_NSEC;
        ++c.tv_sec;
    }

	return c;
#endif
}
