#ifndef	KA_CORE_TIMER_H
#define	KA_CORE_TIMER_H

#define	TIME_NSEC	1000000000
#define	TIME_USEC	1000000
#define	TIME_MSEC	1000
#define	TIME_SEC	1

#ifdef __APPLE__
#define MACOS	1
#endif

#ifdef __FreeBSD__
#define FREEBSD	1
#endif

#if defined(MACOS)
#include <stdint.h>
typedef	uint64_t	hrtime_t;
#elif defined(WINDOWS)
#include <windows.h>
typedef LARGE_INTEGER	hrtime_t;
#else
#include <time.h>
typedef	struct timespec	hrtime_t;
#endif

hrtime_t	zero_hrtime(void);
hrtime_t	get_hrtime(void);
hrtime_t	diff_hrtime(hrtime_t a, hrtime_t b);
hrtime_t	sum_hrtime(hrtime_t a, hrtime_t b);

#endif
