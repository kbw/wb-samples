#include <algorithm>
#include <vector>
#include <iostream>

int main()
{
	std::vector<int> vi{ 1, 2, 4, 7, 14 };

	std::cout << "sum of vector { ";
	for (auto x : vi)
		std::cout << x << " ";

	int sum{};
	std::for_each(vi.begin(), vi.end(), [&](int i){ sum += i; });
	std::cout << "} = "
		<< sum << std::endl;
}
