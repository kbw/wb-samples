#include <algorithm>
#include <memory>
#include <vector>
#include <string>
#include <ostream>

struct Box
{
	const std::string name;

	Box(const std::string& n) : name(n) {}

	const std::string&	GetName() const { return name; }
};

struct Cabinet
{
	std::vector<std::unique_ptr<Box>> v;

	Cabinet(std::initializer_list<std::string> b)
	{
		v.reserve(b.size());

		std::for_each(b.begin(), b.end(),
			[this](const std::string& s)
			{
				v.emplace_back(std::unique_ptr<Box>(new Box(s)));
			});
	}

	void ShowAll(std::ostream& os, const std::string& delimiter = " ") const
	{
		std::for_each(v.begin(), v.end(),
			[&](const std::unique_ptr<Box>& e) mutable
			{
				os << e->GetName() << delimiter;
			});
	}
};

int main()
{
	Cabinet c{ "star trek", "outlander" };
}
