#include <cstdlib>
#include <new>
#include <algorithm>
#include <iostream>

template <typename T>
class vector
{
	T*	block_;
	size_t size_;
	size_t capacity_;

public:
	vector(size_t sz = 0);
	~vector();

	vector(const vector<T>& n);
	vector(vector<T>&& n);

	vector<T>& operator=(const vector<T>& n);
	vector<T>& operator=(vector<T>&& n);

	T& operator[](int idx)		{ return block_[idx]; }
	T operator[](int idx) const	{ return block_[idx]; }

	size_t	size() const		{ return size_; }
	size_t	capacity() const	{ return capacity_; }
};

template <typename T>
vector<T>::vector(size_t sz) :
	block_{nullptr},
	size_{sz},
	capacity_{sz}
{
	block_ = static_cast<T*>(malloc(capacity_ * sizeof(T)));
	for (size_t i = 0; i != size_; ++i)
		new (block_ + i) T();
}

template <typename T>
vector<T>::~vector()
{
	for (size_t i = size_; i != 0; --i)
		block_[i].~T();
	free(block_);
}

template <typename T>
vector<T>::vector(const vector<T>& n) :
	block_{nullptr},
	size_{n.size_},
	capacity_{n.capacity_}
{
	block_ = static_cast<T*>(malloc(capacity_ * sizeof(T)));
	for (size_t i = 0; i != size_; ++i)
		new (block_ + i) T(n.block_[i]);
}

template <typename T>
vector<T>::vector(vector<T>&& n) :
	block_{n.block_},
	size_{n.size_},
	capacity_{n.capacity_}
{
	n.block_ = nullptr;
	n.size_ = 0;
	n.capacity_ = 0;
}

template <typename T>
vector<T>& vector<T>::operator=(const vector<T>& n)
{
	if (this != &n)
	{
		~vector<T>();

		//new this vector<T>(n);
		block_ = static_cast<T*>(malloc(capacity_ * sizeof(T)));
		for (size_t i = 0; i != size_; ++i)
			new (block_ + i) T(n.block_[i]);
	}

	return *this;
}

template <typename T>
vector<T>& vector<T>::operator=(vector<T>&& n)
{
	if (this != &n)
	{
		~vector<T>();
	
		block_ = n.block_;
		size_ = n.size_;
		capacity_ = n.capacity_;

		n.block_ = nullptr;
		n.size_ = 0;
		n.capacity_ = 0;
	}

	return *this;
}

int main()
{
	constexpr size_t n { 100 * 1000 * 1000 };

	vector<double> d1 {n};
	std::cout << &d1 << '\t' << &d1[0] << '\t' << d1.size() << '\t' << d1.capacity() << std::endl;

	vector<double> d2 = std::move(d1);
	std::cout << &d1 << '\t' << &d1[0] << '\t' << d1.size() << '\t' << d1.capacity() << std::endl;
	std::cout << &d2 << '\t' << &d2[0] << '\t' << d2.size() << '\t' << d2.capacity() << std::endl;

	vector<double> d3 = std::move(d2);
	std::cout << &d1 << '\t' << &d1[0] << '\t' << d1.size() << '\t' << d1.capacity() << std::endl;
	std::cout << &d2 << '\t' << &d2[0] << '\t' << d2.size() << '\t' << d2.capacity() << std::endl;
	std::cout << &d3 << '\t' << &d3[0] << '\t' << d3.size() << '\t' << d3.capacity() << std::endl;
}

