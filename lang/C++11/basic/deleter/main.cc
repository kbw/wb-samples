#include <memory>
#include <functional>
#include <stdexcept>
#include <string>

#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

namespace ncc
{
//	template<typename T, typename Deleter>
//	using unique_ptr = std::unique_ptr<T, Deleter>;

//	template<typename T, typename Deleter = std::function<void(T*)>>
//	using unique_ptr = std::unique_ptr<T, Deleter>;

	template<typename T>
	using unique_ptr = std::unique_ptr<T, std::function<void(T*)>>;
}

int main()
{
//	ncc::unique_ptr<int> buf(new int);
	ncc::unique_ptr<FILE> file(fopen("prog", "r"), [](FILE* f) { fclose(f); });
	ncc::unique_ptr<char> str(strdup("prog"),      [](char* f) { free(f); });

	boost::scoped_ptr<int> b_buf(new int);
	boost::shared_ptr<FILE> b_file(fopen("prog", "r"), fclose);
	boost::shared_ptr<char> b_str(strdup("prog"),      free);
}
