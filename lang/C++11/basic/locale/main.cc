#include <sstream>
#include <iostream>
#include <locale>
#include <string.h>

// also see: http://en.cppreference.com/w/cpp/locale/ctype_char
struct custom_delimiter : std::ctype<char>
{
	custom_delimiter() : std::ctype<char>(get_table()) {}
	static const mask *get_table()
	{
		static mask rc[table_size];
		memcpy(rc, classic_table(), table_size);

		rc[',']  = std::ctype_base::space;
		rc['\n'] = std::ctype_base::space;
		return rc;
	}
};

int main()
{
	const std::string data { "33,33,33,1024,56,21,21" };
	std::istringstream is(data);
	is.imbue(std::locale(is.getloc(), new custom_delimiter));

	std::string line;
	while (is)
	{
		is >> line;
		std::cout << line << std::endl;
	}
}
