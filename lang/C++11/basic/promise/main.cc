#include <future>
#include <stdexcept>
#include <iostream>

int main()
{
	try
	{
		std::promise<int> p;
		std::future<int> h = std::async(
			std::launch::async,
			[](std::promise<int>& p)
			{
				try
				{
					throw std::runtime_error("Ugh");
//					p.set_value(1 + 2);
				}
				catch (const std::exception &e)
				{
					p.set_exception(std::current_exception());
				}

				p.set_value(-1);
				return 0;
			}, std::ref(p));

//		std::cout << "value = " << h.get() << "\n";
		std::cout << "value = " << p.get_future().get() << "\n";
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
	}
}
