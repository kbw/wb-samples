#include "fileinfo.hpp"
#include "digest.hpp"

#ifdef HAVE_OPENSSL_MD2
#include <openssl/md2.h>
#endif

#ifdef HAVE_OPENSSL_MD4
#include <openssl/md4.h>
#endif

#ifdef HAVE_OPENSSL_MD5
#include <openssl/md5.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>

static string to_md2(const string& filename);
static string to_md4(const string& filename);
static string to_md5(const string& filename);

bool get_info(const string& name, info_t& info)
{
	struct stat s;
	if (stat(name.c_str(), &s) == -1)
		return false;

	info_t::ids_t ids = { s.st_uid, s.st_gid };
	info_t::times_t times = { s.st_atime, s.st_mtime, s.st_ctime };
	strings stamps =
	{
#ifdef HAVE_OPENSSL_MD2
		to_md2(name),
#endif
#ifdef HAVE_OPENSSL_MD4
		to_md4(name),
#endif
#ifdef HAVE_OPENSSL_MD5
		to_md5(name)
#endif
	};

	info.ids = ids;
	info.times = times;
	info.stamps = stamps;
	return true;
}

#ifdef HAVE_OPENSSL_MD2
static string to_md2(const string& filename)
{
	MD2_CTX ctx;
	MD2_Init(&ctx);

	int fd = open(filename.c_str(), O_RDONLY);
	if (fd >= 0)
	{
		while (true)
		{
			char buf[4 * 1024];
			int n = read(fd, buf, sizeof(buf));
			if (n <= 0)
				break;

			MD2_Update(&ctx, buf, n);
		}
		close(fd);
		fd = -1;
	}

	digest_result digest = {};
	MD2_Final(digest.rec, &ctx);
	return string(reinterpret_cast<char*>(digest.rec), digest_result_size_t);
}
#endif

#ifdef HAVE_OPENSSL_MD4
static string to_md4(const string& filename)
{
	MD4_CTX ctx;
	MD4_Init(&ctx);

	int fd = open(filename.c_str(), O_RDONLY);
	if (fd >= 0)
	{
		while (true)
		{
			char buf[4 * 1024];
			int n = read(fd, buf, sizeof(buf));
			if (n <= 0)
				break;

			MD4_Update(&ctx, buf, n);
		}
		close(fd);
		fd = -1;
	}

	digest_result digest = {};
	MD4_Final(digest.rec, &ctx);
	return string(reinterpret_cast<char*>(digest.rec), digest_result_size_t);
}
#endif

#ifdef HAVE_OPENSSL_MD5
static string to_md5(const string& filename)
{
	MD5_CTX ctx;
	MD5_Init(&ctx);

	int fd = open(filename.c_str(), O_RDONLY);
	if (fd >= 0)
	{
		while (true)
		{
			char buf[4 * 1024];
			int n = read(fd, buf, sizeof(buf));
			if (n <= 0)
				break;

			MD5_Update(&ctx, buf, n);
		}
		close(fd);
		fd = -1;
	}

	digest_result digest = {};
	MD5_Final(digest.rec, &ctx);
	return string(reinterpret_cast<char*>(digest.rec), digest_result_size_t);
}
#endif
