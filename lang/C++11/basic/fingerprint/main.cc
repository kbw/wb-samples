#include "defs.hpp"
#include "fileinfo.hpp"

#include <sys/types.h>
#include <sys/dirent.h>
#include <dirent.h>

#include <iostream>

struct opts_t
{
};

void process(const opts_t& opts, const char* spec, size_t speclen = 0);

int main(int argc, char* argv[])
{
	opts_t opts;
	for (int i = 1; i < argc; ++i)
		if (argv[i][0] == '-')
		{
			if (argv[i][1] == '-')
			{
			}
		}
		else
		{
			process(opts, argv[i]);
		}
}

void process(const opts_t& opts, const char* spec, size_t speclen)
{
	auto mk_name = [](const char* spec, size_t speclen, bool need_slash, const struct dirent& e) -> string
	{
		string name;
		name.reserve(speclen + 1 + e.d_namlen);

		name.append(spec, speclen);
		if (need_slash)
			name += "/";
		name.append(e.d_name, e.d_namlen);

		return name;
	};


	if (speclen == 0)
		speclen = strlen(spec);
	bool need_slash = *spec && spec[speclen - 1] != '/';

	if (DIR* d = opendir(spec))
	{
		while (const struct dirent* e = readdir(d))
		{
			if ((e->d_type != DT_REG) && (e->d_type != DT_DIR) && (e->d_type != DT_LNK))
				continue;

			if ((e->d_name[0] == '.' && e->d_namlen == 1) ||
				(e->d_name[0] == '.' && e->d_name[1] == '.' && e->d_namlen == 2))
				continue;

			string name = mk_name(spec, speclen, need_slash, *e);

			info_t info;
			if (get_info(name, info))
			{
				if (e->d_type == DT_DIR)
					std::cout << '+';
				else if (e->d_type == DT_LNK)
					std::cout << '*';
				else if (e->d_type == DT_REG)
					std::cout << ' ';

				std::cout << '\"' << name << "\" " << info << "\n";
			}

			if (e->d_type == DT_DIR)
			{
				process(opts, name.c_str(), name.size());
			}
		}

		closedir(d);
	}
	else
		std::cerr <<  "opendir(" << spec << ") failed: " << strerror(errno) << "\n";
}
