#pragma once

#include <string>
#include <vector>

typedef std::string string;
typedef std::vector<string> strings;
