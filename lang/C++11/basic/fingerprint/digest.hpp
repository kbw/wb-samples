#pragma once

const size_t digest_result_size_t = 16;

struct digest_result
{
	unsigned char rec[digest_result_size_t];
};

inline bool operator==(const digest_result& a, const digest_result& b)
{
	return memcmp(&a.rec, &b.rec, sizeof(a.rec)) == 0;
}
