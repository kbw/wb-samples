#pragma once

#include "defs.hpp"

#include <sys/time.h>

#include <initializer_list>
#include <iterator>
#include <algorithm>
#include <iosfwd>

struct info_t
{
	struct ids_t
	{
		ids_t(std::initializer_list<unsigned> list) :
			uid(0), gid(0)
		{
			std::vector<unsigned> coll;
			std::copy(list.begin(), list.end(), std::back_insert_iterator<std::vector<unsigned>>(coll));

			auto p = coll.rbegin();
			switch (coll.size())
			{
			case 2:	gid = *p++;
			case 1:	uid = *p++;
			case 0:	break;
			default:
				throw std::runtime_error("bad args to info_t::ids_t");
			}
		};

		ids_t(int uid = 0, int gid = 0) :
			uid(uid), gid(gid)
		{
		}

		int uid, gid;
	};

	struct times_t
	{
		times_t(std::initializer_list<time_t> list) :
			atime(0), mtime(0), ctime(0)
		{
			std::vector<time_t> coll;
			std::copy(list.begin(), list.end(), std::back_insert_iterator<std::vector<time_t>>(coll));

			auto p = coll.rbegin();
			switch (coll.size())
			{
			case 3:	ctime = *p++;
			case 2:	mtime = *p++;
			case 1:	atime = *p++;
			case 0:	break;
			default:
				throw std::runtime_error("bad args to info_t::times_t");
			}
		}

		times_t(time_t atime = 0, time_t mtime = 0, time_t ctime = 0) :
			atime(atime), mtime(mtime), ctime(ctime)
		{
		}

		time_t atime, mtime, ctime;
	};

	ids_t	ids;
	times_t	times;
	strings	stamps;
};

std::ostream& operator<<(std::ostream& os, const info_t& info);
std::istream& operator>>(std::istream& os, info_t& info);
