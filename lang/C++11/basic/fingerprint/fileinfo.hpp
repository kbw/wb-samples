#pragma once

#include "defs.hpp"
#include "fingerprint.hpp"

bool get_info(const string& name, info_t& info);
