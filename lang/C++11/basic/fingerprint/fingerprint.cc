#include "fingerprint.hpp"
#include <ostream>
#include <istream>
#include <stdio.h>

std::ostream& operator<<(std::ostream& os, const info_t& info)
{
	os << info.ids.uid << " ";
	os << info.ids.gid << " ";
	os << info.times.atime << " ";
	os << info.times.mtime << " ";
	os << info.times.ctime << " ";

	for (const string& stamp : info.stamps)
	{
		using namespace std;

		string str(2*stamp.size() + 1, '\0');
		{
			char* s = &str.front();
			for (size_t i = 0; i < stamp.size(); ++i)
				snprintf(&s[2*i], 2, "%02x", stamp[i]);
		}

		os << str << " ";
	}

	return os;
}

std::istream& operator>>(std::istream& is, info_t& info)
{
	is >> info.ids.uid;
	is >> info.ids.gid;
	is >> info.times.atime;
	is >> info.times.mtime;
	is >> info.times.ctime;
	return is;
}
