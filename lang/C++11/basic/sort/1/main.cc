#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>

struct Fruit
{
	std::string	name;
	std::string	colour;
};
std::ostream& operator<<(std::ostream& os, const Fruit& fruit)
{
	return os << fruit.name << ":" << fruit.colour;
}

int main()
{
	std::vector<Fruit> fruits {
		{ "Apple", "Green" },
		{ "Apple", "Red" },
		{ "Apple", "Yellow" },
		{ "Banana", "Green" },
		{ "Banana", "Red" },
		{ "Banana", "Yellow" },
		{ "Mango", "Green" },
		{ "Mango", "Red" },
		{ "Mango", "Yellow" }
	};

	struct ByColor
	{
		bool operator()(const Fruit& a, const Fruit& b) const
		{
			return a.colour < b.colour;
		}
	} by_colour;
	std::cout << "by colour: ";
	std::sort(fruits.begin(), fruits.end(), by_colour);
	std::copy(fruits.begin(), fruits.end(), std::ostream_iterator<Fruit>(std::cout, ": "));
	std::cout << std::endl;

	struct ByName
	{
		bool operator()(const Fruit& a, const Fruit& b) const
		{
			return a.name < b.name;
		}
	} by_name;
	std::cout << "by name: ";
	std::sort(fruits.begin(), fruits.end(), by_name);
	std::copy(fruits.begin(), fruits.end(), std::ostream_iterator<Fruit>(std::cout, ": "));
	std::cout << std::endl;
}
