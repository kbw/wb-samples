/*
	def digits_of(number):
		return [int(i) for i in str(number)]

	def luhn_checksum(card_number):
		digits = digits_of(card_number)
		odd_digits = digits[-1::-2]
		even_digits = digits[-2::-2]
		total = sum(odd_digits)
		for digit in even_digits:
			total += sum(digits_of(2 * digit))
		return total % 10

	def is_luhn_valid(card_number):
		return luhn_checksum(card_number) == 0
 */

#include <string>
#include <vector>
#include <iostream>

#include <stdlib.h>

using namespace std;

typedef int digit_t;
typedef vector<digit_t> digits_t;

digits_t digits_of(const string& n)
{
	digits_t nums;
	for (size_t i = 0; i != n.size(); ++i)
	{
		char ch[2] = { n[i], 0 };
		nums.push_back(atoi(ch));

//		clog << "\t" << nums[ nums.size() - 1] << endl;
	}

	return nums;
}

digit_t luhn_checksum(const string& card)
{
	digits_t digits = digits_of(card);

	digit_t sum = 0;
	for (size_t i = 0; i != digits.size(); ++i)
	{
		if (i % 2 == 0)
		{
			digit_t digit = digits[i];

			clog << "\t" << sum << " += " << digit << endl;
			sum += digit;
		}
		else
		{
			digit_t n = digits[i];
			n *= 2;
			digit_t digit = n/10 + n%10;

			clog << "\t" << sum << " += " << n/10 << " + " << n%10 << " which is " << digit << endl;
			sum += digit;
		}
	}

	clog << "\tsum = " << sum << endl;
	clog << "\tchecksum = (" << sum << " * 9) % 10" << endl;
	clog << "\tchecksum = " << 9*sum << " % 10" << endl;

	digit_t checksum = (9*sum) % 10;
	clog << "\tchecksum = " << checksum << endl;

	clog << "\t(sum + checksum) % 10 = (" << sum << " + " << checksum << ") % 10" << endl;
	clog << "\t(sum + checksum) % 10 = (" << sum + checksum << ") % 10" << endl;
	clog << "\t(sum + checksum) % 10 = " << (sum + checksum) % 10 << endl;
	return (sum + checksum) % 10;
}

bool is_luhn_valid(const string& card)
{
	return luhn_checksum(card) == 0;
}

int main()
{
	const vector<string> cards =
	{
		"7992739871",
		"4686006570307405",
		"4686006570307407",
		"4093650457937474",
		"4340423439668810",
		"1234567812345670",
		"5509415774265347",
		"X234567812345670",
		"4539281167952835",
		"4532528637398511",
		"4653549906803760"
	};
	for (const string& card : cards)
		cout << "check on " << card << ": " << (is_luhn_valid(card) ? "true" : "false") << endl;
}
