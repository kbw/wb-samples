#include <iostream>

template <typename T>
void g(T n)
{
	std::cout << n << std::endl;
}

void f()
{
	std::cout << std::endl;
}

template <typename T, typename... Tail>
void f(T head, Tail... tail)
{
	g(head);
	f(tail...);
}

int main()
{
	f(1, 1.0/3.0, "hello");
	f(3ULL, 1.0/4.0, 0, "string");
}
