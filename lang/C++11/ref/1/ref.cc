#include <functional>
// using std::tr1::reference_wrapper;
using std::reference_wrapper;

// simple class containing reference
class ref
{
public:
	ref(int& i) : member(i) {}

private:
	int& member;
};

// simple class containing reference_wrapper
class refwrap
{
public:
	refwrap(int& i) : member(i) {}

private:
	reference_wrapper<int> member;
};

// demonstrate copying
int main()
{
	int i, j;

	ref r0(i);
	ref r1(j);
//	r1 = r0;        // error: ref can't be copied  refwrap rw0(i);

	refwrap rw0(i);
	refwrap rw1(j);
	rw1 = rw0;     // okay: refwrap can be copied

	return 0;
}
