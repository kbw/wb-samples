#include <condition_variable>
#include <mutex>
#include <future>
#include <iostream>

template <typename T>
struct sync_t
{
	template <typename U>
	sync_t(U u) : flag(u) {}

	T flag;	// protects against spurious wakes
	std::mutex mutex;
	std::condition_variable cond;
};

sync_t<bool> ready(false);

void thread1()
{
	// do something thread2 needs as preparation
	std::cout << "<return>" << std::endl;
	std::cin.get();

	// signal that thread1 has prepared a condition
	{
		std::lock_guard<std::mutex> lg(ready.mutex);
		ready.flag = true;		// wait predicate
	} // release lock
	ready.cond.notify_one();	// release wait()
}

void thread2()
{
	// wait until thread1 is ready (readyFlag is true)
	{
		std::unique_lock<std::mutex> ul(ready.mutex);
		ready.cond.wait(ul, []{ return ready.flag; });	// wait for notify
	} // release lock

	// do whatever shall happen after thread1 has prepared things
	std::cout << "done" << std::endl;
}

int main()
{
	auto f1 = std::async(std::launch::async, thread1);
	auto f2 = std::async(std::launch::async, thread2);
}
