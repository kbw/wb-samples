#include <thread>
#include <future>
#include <numeric>
#include <vector>
#include <iostream>

template <typename T>
typename T::value_type dot_product(const T& a, const T& b)
{
	typedef typename T::value_type	value_t;
	typedef std::promise<value_t>	promise_t;
	typedef std::vector<promise_t>	promises_t;

	size_t idx {};
	promises_t promises(std::min(a.size(), b.size()));
	for (typename T::const_iterator pa = a.begin(), pb = b.begin(); pa != a.end() && pb != b.end(); ++pa, ++pb)
	{
		std::thread(
				[](promise_t& p, value_t a, value_t b) { p.set_value(a * b); },
				std::ref(promises[idx++]), *pa, *pb).detach();
	}

	return std::accumulate(promises.begin(), promises.end(), value_t(0),
				[](value_t sum, promise_t& p) { return sum + p.get_future().get(); });
}

int main()
{
	{
		std::vector<int> a = { 1, 2, 3 }, b = { 4, 5, 6 };
		std::cout << dot_product(a, b) << std::endl;
	}
	{
		std::vector<double> a = { 1.1, 2.2, 3.3 }, b = { 4.4, 5.5, 6.6 };
		std::cout << dot_product(a, b) << std::endl;
	}
}
