#include <thread>
#include <future>
#include <vector>
#include <iostream>

template <typename T>
void product(std::promise<T>& p, T a, T b)
{
	p.set_value(a * b);
}

template <typename T>
typename T::value_type dot_product(const T& a, const T& b)
{
	typedef typename T::value_type	value_t;
	typedef std::promise<value_t>	promise_t;
	typedef std::vector<promise_t>	promises_t;

	size_t idx {};
	promises_t promises(std::min(a.size(), b.size()));
	for (typename T::const_iterator pa = a.begin(), pb = b.begin(); pa != a.end() && pb != b.end(); ++pa, ++pb)
		std::thread(product<value_t>, std::ref(promises[idx++]), *pa, *pb).detach();

	value_t sum {};
	for (auto& promise : promises)
		sum += promise.get_future().get();

	return sum;
}

int main()
{
	{
		std::vector<int> a = { 1, 2, 3 }, b = { 4, 5, 6 };
		std::cout << dot_product(a, b) << std::endl;
	}
	{
		std::vector<double> a = { 1.1, 2.2, 3.3 }, b = { 4.4, 5.5, 6.6 };
		std::cout << dot_product(a, b) << std::endl;
	}
}
