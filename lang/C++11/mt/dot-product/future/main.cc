#include <future>
#include <vector>
#include <iostream>

template <typename T>
T product(T a, T b)
{
	return a * b;
}

template <typename T>
typename T::value_type dot_product(const T& a, const T& b)
{
	typedef typename T::value_type	value_t;
	typedef std::future<value_t>	future_t;
	typedef std::vector<future_t>	futures_t;

	futures_t futures;
	for (typename T::const_iterator pa = a.begin(), pb = b.begin(); pa != a.end() && pb != b.end(); ++pa, ++pb)
		futures.push_back(std::async(std::launch::async, product<value_t>, *pa, *pb));

	value_t sum {};
	for (auto& future : futures)
		sum += future.get();

	return sum;
}

int main()
{
	{
		std::vector<int> a = { 1, 2, 3 }, b = { 4, 5, 6 };
		std::cout << dot_product(a, b) << std::endl;
	}
	{
		std::vector<double> a = { 1.1, 2.2, 3.3 }, b = { 4.4, 5.5, 6.6 };
		std::cout << dot_product(a, b) << std::endl;
	}
}
