#include <list>
#include <atomic>
#include <thread>
#include <future>
#include <iostream>

std::list<int> data;
std::atomic<bool> data_ready;

void reader_thread()
{
	for (int i = 0; i < 2; ++i) {
		while (!data_ready.load(std::memory_order_acquire))
			std::this_thread::sleep_for(std::chrono::duration<int, std::milli>(1));
		if (!data.empty()) {
			std::cout << "The answert=" << data.front() << "\n";
			data.pop_front();
		}
	}
}

void writer1_thread()
{
	data.push_back(42);
	data_ready.store(true, std::memory_order_release);;
}

void writer2_thread()
{
	data.push_back(24);
	data_ready.store(true, std::memory_order_release);;
}

int main()
{
	auto t1 = std::async(std::launch::async, reader_thread);
	auto t2 = std::async(std::launch::async, writer1_thread);
	auto t3 = std::async(std::launch::async, writer2_thread);
}
