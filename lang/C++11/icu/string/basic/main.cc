#include <unicode/unistr.h>
#include <unicode/ustream.h>
#include <iostream>

int main()
{
	icu::UnicodeString	a("Gren egss and yam");
	icu::UnicodeString	s(a);
	std::cout << s << "\n";

	std::cout << "toUpper: " << s.toUpper() << "\n";
	std::cout << "toLower: " << s.toLower() << "\n";
}
