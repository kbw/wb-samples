#include <atomic>
#include <iostream>

int main()
{
	std::cout << "\tbool is_lock_free " << std::atomic<bool>().is_lock_free() << std::endl;
	std::cout << "\tint  is_lock_free " << std::atomic<int >().is_lock_free() << std::endl;
	std::cout << "\tint* is_lock_free " << std::atomic<int*>().is_lock_free() << std::endl;
	std::cout << "\tlong is_lock_free " << std::atomic<long>().is_lock_free() << std::endl;
}
