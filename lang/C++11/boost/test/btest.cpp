#include "order.hpp"

#define BOOST_TEST_MODULE OrderTest
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(OrderTest)

BOOST_AUTO_TEST_CASE(TestNoOrders)
{
	Order order;
	OrderWriter writer;
	OrderStore store;
	SimpleOrderManager mgr;

	BOOST_REQUIRE_THROW(mgr.WriteAllOrders(store, writer), const char*);
}

BOOST_AUTO_TEST_SUITE_END()
