#include <vector>

class Order
{
	//represents an order
};

class OrderWriter
{
public:
	virtual void writeOrders(const std::vector<Order>& allOrders)
	{
		// ... writes the orders somewhere ...
	}
};

class OrderStore
{
public:
	virtual std::vector<Order> getOrders() const
	{
		// ... returns an array of orders ...
		return std::vector<Order>();	// kw: I added this, to get a minimal valid program
	}
};

class SimpleOrderManager
{
public:
	void WriteAllOrders(OrderStore& orderStore, OrderWriter& orderWriter)
	{
		std::vector<Order> allOrders = orderStore.getOrders();
		if (allOrders.size() == 0)
			throw "No orders in store";
		orderWriter.writeOrders(allOrders);
	}
};
