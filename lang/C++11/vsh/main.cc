// --------------------------------------------------------------------------
//	Copyright (c) 2013 Keith Williams.
//	All rights reserved.
//
//	Redistribution and use in source and binary forms are permitted
//	provided that the above copyright notice and this paragraph are
//	duplicated in all such forms and that any documentation,
//	advertising materials, and other materials related to such
//	distribution and use acknowledge that the software was developed
//	by Webbusy Ltd.
// --------------------------------------------------------------------------
//	Originally posted here as a sample.
//	http://www.cplusplus.com/forum/general/121052/#msg658880
// --------------------------------------------------------------------------

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/errno.h>
#include <string.h>
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>

struct cmd_context
{
	int status;
	std::string line;
};

typedef std::vector<char*> vargs_t;

vargs_t	getargs(const std::string& line);
int		execute(const cmd_context& ctx, vargs_t args);

const std::string prompt = "$ ";

int main()
{
	cmd_context ctx;

	for (;;)
	{
		// get a commmand from the user
		std::cout << prompt;
		if (!std::getline(std::cin, ctx.line) || ctx.line == "done")
			break;

		pid_t pid = fork();
		if (pid == -1)
		{
			std::clog << "fork() failed: " << strerror(errno) << std::endl;
			break;
		}
		else if (pid == 0)
		{
			// execute in child
			vargs_t args = getargs(ctx.line);
			int ret = execute(ctx, args);
			if (ret == -1)
			{
				std::clog << "execvp() failed: " << strerror(errno) << std::endl;
				exit(1);
			}
		}
		else
		{
			// parent waits for child to complete
			int status;
			waitpid(pid, &status, 0);
			ctx.status = WEXITSTATUS(status);
		}
	}

	return 0;
}

vargs_t getargs(const std::string& line)
{
	vargs_t args;

	char* p = strdup(line.c_str());
	while (char* q = strchr(p, ' '))
	{
		// save arg
		*q = 0;
		args.push_back(p);

		// step pass spaces
		for (p = q + 1; *p == ' '; ++p)
			;
	}
	// save final arg and terminator
	args.push_back(p);
	args.push_back(nullptr);	// force C++11 so args is moved rather than copied in C++98

	return args;
}

int execute(const cmd_context& ctx, vargs_t args)
{
	char c = 0;
	char* cmd = args[0] ? args[0] : &c;
	if (strcmp("?", cmd) == 0)
	{
		// show last command status
		std::cout << ctx.status << std::endl;
		exit(0);
	}

	return execvp(args[0], &args.front());
}
