Sample code: C++11
	cppsock		move semantics
	ctx-switch	show scheduler context switch time
	except		Exceptions
		1		std::current_exception ...
		2		determine exception from terminate handler
	ref			std::reference_wrapper
		{1,2,3}
	rvalue		same as ref/3 ???
