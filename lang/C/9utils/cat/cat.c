#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

void cat(int in, const char* name)
{
	char buf[4 * 1024];
	int n, m;

	while ((n = read(in, buf, sizeof buf)) > 0)
		if ((m = write(1, buf, n)) != n)
		{
			perror("<stdout>");
			exit(1);
		}

	if (n < 0)
	{
		perror(name);
		exit(1);
	}
}

int main(int argc, char* argv[])
{
	int i;

	if (argc == 1)
	{
		cat(0, "<stdin>");
		return 0;
	}

	for (i = 1; i < argc; ++i)
	{
		int in = open(argv[i], O_RDONLY);
		if (in == -1)
		{
			perror(argv[i]);
			continue;
		}

		cat(in, argv[i]);
	}
	return 0;
}
