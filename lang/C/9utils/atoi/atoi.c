#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

int digit(char c, int base) {
	if (c - '0' < base) return c - '0';
	if (base > 10 && c - 'A' < base - 10) return 10 + c - 'A';
	if (base > 10 && c - 'a' < base - 10) return 10 + c - 'a';

	errno = EINVAL;
	return -1;
}

int to_num(const char* s, int base)
{
	int acc = 0;
	for (; *s; ++s) {
		int d = digit(*s, base);
		if (d == -1)	// domain [0, base)
			return -1;	// errno already set

		int acc2 = acc;
		acc *= base;
		acc += d;

		if (acc < acc2) {	// check overflow
			errno = E2BIG;
			return -1;
		}
	}

	return acc;
}

int my_atoi(const char* s) {
	errno = 0;

	int sign = 1;
	if (*s == '-') {
		++s;
		sign = -1;
	}

	int val = (*s == '0')
		? ((s[1] == 'x') ? to_num(s + 2, 16) : to_num(s + 1, 8))
		: to_num(s, 10);
	if (val == -1)	// domain is [0, maxint)
		return -1;	// errno already set

	return sign * val;
}

int main(int argc, char* argv[])
{
	int i;

	for (i = 1; i != argc; ++i)
		printf("%d\n", my_atoi( argv[i] ));
}
