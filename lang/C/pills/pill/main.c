#include "load_rec.h"
#include <libpill/defs.h>
#include <libpill/string_list.h>
#include <libpill/slist.h>

string_list* load_path(int argc, char* argv[]) {
	int i;

	string_list* p = string_list_create(argc);
	if (p) {
		for (i = 0; i < argc; ++i)
			string_list_assign(p, i, argv[i]);
		return p;
	}

	return NULL;
}

void search_load_run(string_list* p) {
	int i;
	slist* modules;		// linked list of load_rec

	slist_create(&modules);
	for (i = 0; i < p->n_items; ++i) {
		load_rec_create_from_path(p->item[i], &modules);
	}

/*
	slist_iterator iter;
	slist_iter_init(&modeles, &iter);
	while (slist_iter_get(&iter)) {
		launch(slist_iter_get(&iter));
	}
 */

	slist_destroy(&modules, load_rec_destroy);
	string_list_destroy(p);
}

int main(int argc, char* argv[]) {
	// build path list
	string_list* path = load_path(argc, argv);

	// load modules and exec
	search_load_run(path);
}
