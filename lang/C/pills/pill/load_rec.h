#pragma once

#ifndef	NDEBUG
#define	LOADREC_SIG			0x1962
#define	LOADREC_ASSIGN(x)	x = LOADREC_SIG
#define	LOADREC_CHECK(x)	(x == LOADREC_SIG)
#define	LOADREC_DECL		int sig;
#else
#define	LOADREC_SIG
#define	LOADREC_ASSIGN(x)
#define	LOADREC_CHECK(x)	(1)
#define	LOADREC_DECL
#endif

struct slist;

typedef int factory_func(int argc, char* argv[]);

typedef struct load_rec {
	LOADREC_DECL
	char* name;
	void* module;
	factory_func* create;
} load_rec;

load_rec*	load_rec_create(const char* name, void* h, factory_func* create);
void		load_rec_destroy(void* p);
load_rec*	load_rec_create_from_file(const char* name);
void		load_rec_create_from_path(const char* path, struct slist** modules);
