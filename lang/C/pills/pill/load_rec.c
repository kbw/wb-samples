#include "load_rec.h"
#include <libpill/string_list.h>
#include <libpill/slist.h>
#include <libpill/defs.h>
#include <libpill/dir.h>
#include <sys/types.h>
#include <dirent.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

load_rec* load_rec_create(const char* name, void* h, factory_func* create) {
	load_rec* rec = malloc(sizeof(load_rec));
	if (rec) {
		LOADREC_ASSIGN(rec->sig);
		rec->name = strdup(name);
		rec->module = h;
		rec->create = create;
	}

	return rec;
}

void load_rec_destroy(void* p) {
	load_rec* rec = (load_rec*)p;
	if (rec) {
		VERIFY(LOADREC_CHECK(rec->sig), "load_rec: bad block");
		if (LOADREC_CHECK(rec->sig)) {
			free(rec->name);
			free(rec);
		}
	}
}

load_rec* load_rec_create_from_file(const char* name) {
	void* h = dlopen(name, RTLD_NOW);
	if (h) {
		factory_func* create = (factory_func*)dlsym(h, "pill");
		if (create) {
			return load_rec_create(name, h, create);
		}

		dlclose(h);
	}

	return NULL;
}

static void cb(const char* name, int len, void* ctx) {
	load_rec* rec = load_rec_create_from_file(name);

	if (rec) {
		slist** modules = (slist**)ctx;
		slist_prepend(modules, rec);

		printf("%s\n", name);
	}
}

void load_rec_create_from_path(const char* path, struct slist** modules) {
	traverse_dir(path, cb, (void*)modules);
}
