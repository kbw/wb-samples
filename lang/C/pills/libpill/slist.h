#pragma once

#ifndef	NDEBUG
#define	SLIST_SIG			0x789a
#define	SLIST_ASSIGN(x)		x = SLIST_SIG
#define	SLIST_CHECK(x)		(x == SLIST_SIG)
#define	SLIST_DECL			int sig;
#else
#define	SLIST_SIG
#define	SLIST_ASSIGN(x)
#define	SLIST_CHECK(x)		(1)
#define	SLIST_DECL
#endif

typedef struct slist {
	SLIST_DECL
	struct slist*	next;
	void*	data;
} slist;

typedef struct slist_iter {
	struct slist* head;
	struct slist* p;
} slist_iter;

void slist_create(slist** head);
void slist_destroy(slist** head, void (*destroy)(void*));
void slist_prepend(slist** head, void* data);
void slist_append(slist** head, void* data);
int  slist_find(
		slist_iter** iter, const void* data,
		int (*compare)(const void*, const void*));
slist_iter* slist_begin(slist* head, slist_iter** iter);
