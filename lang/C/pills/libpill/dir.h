#pragma once

typedef void dir_cb(const char* name, int len, void* ctx);
void traverse_dir(const char* path, dir_cb* cb, void* ctx);
