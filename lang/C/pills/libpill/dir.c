#define _GNU_SOURCE
#include "defs.h"
#include "dir.h"
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>

void traverse_dir(const char* path, dir_cb* cb, void* ctx) {
	DIR* dir = opendir(path);
	if (dir) {
		struct dirent* e;
		while ((e = readdir(dir))) {
			if (e->d_name[0] == '.')
				continue;	// skip hidden content

			char* name;
			int len;
			
			len  = asprintf(&name, "%s/%s", path, e->d_name);
			if (!name)
				continue;

			if (e->d_type & DT_DIR) {
				traverse_dir(name, cb, ctx);
			}

			if (e->d_type & DT_REG) {
				if (cb) {
					cb(name, len, ctx);
				}
			}

			free(name);
		}

		closedir(dir);
	}
}
