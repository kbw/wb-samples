#pragma once

typedef struct string_list {
	int n_items;
	char** item;
} string_list;

string_list* string_list_create(int sz);
void		string_list_destroy(string_list* p);
int			string_list_resize(string_list* p, int sz);
const char*	string_list_assign(string_list* p, int idx, const char* str);
const char*	string_list_move(string_list* p, int idx, char* str);
const char*	string_list_str(string_list* p, int idx);
