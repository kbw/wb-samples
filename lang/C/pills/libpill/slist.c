#include "slist.h"
#include "defs.h"
#include <stdlib.h>

void slist_create(slist** head) {
	*head = NULL;
}

void slist_destroy(slist** head, void (*destroy)(void*)) {
	slist* p;
	slist* next;

	for (p = *head; p; p = next) {
		VERIFY(SLIST_CHECK(p->sig), "bad slist block");
		if (SLIST_CHECK(p->sig)) {
			next = p->next;
	
			if (destroy)
				destroy(p->data);
			free(p);
		}
	}
}

void slist_prepend(slist** head, void* data) {
	slist* node = malloc(sizeof(slist));
	if (!node)
		return;

	SLIST_ASSIGN(node->sig);
	node->data = data;
	node->next = *head;

	*head = node;
}

void slist_append(slist** head, void* data) {
	slist* node = malloc(sizeof(slist));
	if (!node)
		return;

	SLIST_ASSIGN(node->sig);
	node->data = data;
	node->next = NULL;

	if (*head == NULL) {
		*head = node;
		return;
	}
	
	slist* p = *head;
	while (p->next) {
		VERIFY(SLIST_CHECK(p->sig), "bad slist block");
		p = p->next;
	}

	p->next = node;
}

int slist_find(
		slist_iter** iter, const void* data,
		int (*compare)(const void*, const void*)) {
	slist* p = (*iter)->p;
	VERIFY(SLIST_CHECK(p->sig), "bad slist block");

	while (p && p->next) {
		VERIFY(SLIST_CHECK(p->sig), "bad slist block");
		if (SLIST_CHECK(p->sig) && compare(p->data, data)) {
			(*iter)->p = p;

			return 1;
		}
	}

	(*iter)->p = p;
	return 0;
}

slist_iter* slist_begin(slist* head, slist_iter** iter) {
	if (!(*iter) || !(*iter)->head) {
		return NULL;
	}

	if (head) {
		VERIFY(SLIST_CHECK(head->sig), "bad slist block");
		(*iter)->head = (*iter)->p = head;
	}

	return *iter;
}
