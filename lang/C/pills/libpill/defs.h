#pragma once
#include <stdio.h>

#ifndef	NDEBUG
#define	VERIFY(exp, msg)	if (!exp) fprintf(stderr, "%s:%d \"%s\"\n", __FILE__, __LINE__, msg)
#else
#define	VERIFY(exp, msg)
#endif
