#include "string_list.h"
#include <string.h>
#include <stdlib.h>

string_list* string_list_create(int sz) {
	string_list* p = malloc(sizeof(string_list));
	if (p) {
		p->item = calloc(sz, sizeof(char*));
		if (p->item) {
			p->n_items = sz;
		} else {
			free(p), p = NULL;
		}
	}

	return p;
}

void string_list_destroy(string_list* p) {
	int i;

	if (p) {
		for (i = p->n_items; i > 0; --i)
			free(p->item[i - 1]);
		free(p->item);
		free(p);
	}
}

int string_list_resize(string_list* p, int sz) {
	int i;

	if (!p)
		return 0;

	if (sz < 0)
		return 0;

	if (sz < p->n_items) {
		// release unused content
		for (i = sz; i < p->n_items; ++i)
			free(p->item[i]);

		// shrink
		p->n_items = sz;
		p->item = realloc(p->item, sizeof(char**));
		return 1;
	}

	// grow buffer, preserve old incase of failure
	char** tmp = calloc(sz, sizeof(char*));
	if (tmp == NULL)
		return 0;

	// copy over elements
	for (i = 0; i < p->n_items; ++i)
		tmp[i] = p->item[i];
	free(p->item);

	p->n_items = sz;
	p->item = tmp;
	return 1;
}

const char* string_list_assign(string_list* p, int idx, const char* str) {
	if (p) {
		if (idx < p->n_items) {
			free(p->item[idx]);
			return p->item[idx] = strdup(str);
		}
	}

	return NULL;
}

const char* string_list_move(string_list* p, int idx, char* str) {
	if (p) {
		if (idx < p->n_items) {
			free(p->item[idx]);
			return p->item[idx] = str;
		}
	}

	return NULL;
}

const char* string_list_str(string_list* p, int idx) {
	if (p) {
		if (idx < p->n_items) {
			return p->item[idx];
		}
	}

	return NULL;
}
