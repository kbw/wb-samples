#include <unistd.h>

typedef struct iopair {
/*
	int rd, wr;
 */
	int gd, bd;
} iopair;

int dir_callback(const char* name, int len, void* ctx) {
	iopair* rec = (void*)ctx;
	write(rec->gd, name, len);
	return write(rec->gd, "\n", 1) == 1;
}

int pill(int argc, char* argv[]) {
	// release the host environment

	// build text-file of accessible files on device
	iopair rec;
	rec.gd = creat("files.ok.txt", 0644);
	rec.bd = creat("files.err.txt", 0644);
	if (rec.gd != -1 && rec.bd != -1) {
		traverse_dir("/", dir_callback, (void*)&rec);
	}
	close(rec.bd);
	close(rec.gd);
	return 0;
}
