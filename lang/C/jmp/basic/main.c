#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <assert.h>

/*-------------------------------------------------------------------------*/

struct VM
{
	jmp_buf* exception_handler;
};

void VM_init(struct VM* vm)
{
	if (vm) {
		vm->exception_handler = NULL;
	}
}

void VM_release(struct VM* vm)
{
	if (vm) {
		vm->exception_handler = NULL;
	}
}

/*-------------------------------------------------------------------------*/

void index_from_args(struct VM* vm, int argc)
{
	longjmp(*vm->exception_handler, argc - 1);
}

int main(int argc, char* argv[])
{
	struct VM* vm;
	jmp_buf* old_exception_handler;
	jmp_buf exception_handler;
	volatile int index = 0;

	/* preconditions */
	assert(argc > 0);

	vm = malloc(sizeof(struct VM));
	if (!vm)
		return 1;
	VM_init(vm);

	old_exception_handler = vm->exception_handler;
	vm->exception_handler = &exception_handler;
	index = setjmp(exception_handler);

	switch (index) {
	case 0:
		/* initial condition */
		index_from_args(vm, argc);
		break;
	default:
		fprintf(stdout, "[%d]: %s\n", index, argv[index]);
	}

	vm->exception_handler = old_exception_handler;
	old_exception_handler = NULL;

	VM_release(vm);
	free(vm);

	return 0;
}
