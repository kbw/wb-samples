#include "signal_handler.h"

#include <signal.h>		// signal
#include <stdlib.h>		// exit
#include <stdio.h>		// printf

const char* to_string(int sig)
{
	switch (sig)
	{
	case SIGHUP:	return "SIGHUP";
	case SIGINT:	return "SIGINT";
	case SIGQUIT:	return "SIGQUIT";
	case SIGILL:	return "SIGILL";
	case SIGTRAP:	return "SIGTRAP";
	case SIGABRT:	return "SIGABRT";
//	case SIGEMT:	return "SIGEMT";
	case SIGFPE:	return "SIGFPE";
	case SIGKILL:	return "SIGKILL";
	case SIGBUS:	return "SIGBUS";
	case SIGSEGV:	return "SIGSEGV";
	case SIGSYS:	return "SIGSYS";
	case SIGPIPE:	return "SIGPIPE";
	case SIGALRM:	return "SIGALRM";
	case SIGTERM:	return "SIGTERM";
	case SIGURG:	return "SIGURG";
	case SIGSTOP:	return "SIGSTOP";
	case SIGTSTP:	return "SIGTSTP";
	case SIGCONT:	return "SIGCONT";
	case SIGCHLD:	return "SIGCHLD";
	case SIGTTIN:	return "SIGTTIN";
	case SIGTTOU:	return "SIGTTOU";
	case SIGIO:		return "SIGIO";
	case SIGXCPU:	return "SIGXCPU";
	case SIGXFSZ:	return "SIGXFSZ";
	case SIGVTALRM:	return "SIGVTALRM";
	case SIGPROF:	return "SIGPROF";
	case SIGWINCH:	return "SIGWINCH";
//	case SIGINFO:	return "SIGINFO";
	case SIGUSR1:	return "SIGUSR1";
	case SIGUSR2:	return "SIGUSR2";
//	case SIGTHR:	return "SIGTHR";
//	case SIGLIBRT:	return "SIGLIBRT";
	default:		return "UNKNOWN";
	}
}

void set_signalhandler2(int child)
{
	struct sigaction act;
	struct sigaction oact;

	act.sa_handler = sighandler;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	if (child)
	{
		sigaddset(&act.sa_mask, SIGHUP);
		sigaddset(&act.sa_mask, SIGINT);
		sigaddset(&act.sa_mask, SIGQUIT);
		sigaddset(&act.sa_mask, SIGILL);
		sigaddset(&act.sa_mask, SIGTRAP);
		sigaddset(&act.sa_mask, SIGABRT);
//		sigaddset(&act.sa_mask, SIGEMT);
		sigaddset(&act.sa_mask, SIGFPE);
//		sigaddset(&act.sa_mask, SIGKILL);
		sigaddset(&act.sa_mask, SIGBUS);
		sigaddset(&act.sa_mask, SIGSEGV);
		sigaddset(&act.sa_mask, SIGSYS);
		sigaddset(&act.sa_mask, SIGPIPE);
		sigaddset(&act.sa_mask, SIGALRM);
		sigaddset(&act.sa_mask, SIGTERM);
		sigaddset(&act.sa_mask, SIGURG);
//		sigaddset(&act.sa_mask, SIGSTOP);
		sigaddset(&act.sa_mask, SIGTSTP);
		sigaddset(&act.sa_mask, SIGCONT);
		sigaddset(&act.sa_mask, SIGCHLD);
		sigaddset(&act.sa_mask, SIGTTIN);
		sigaddset(&act.sa_mask, SIGTTOU);
		sigaddset(&act.sa_mask, SIGIO);
		sigaddset(&act.sa_mask, SIGXCPU);
		sigaddset(&act.sa_mask, SIGXFSZ);
		sigaddset(&act.sa_mask, SIGVTALRM);
		sigaddset(&act.sa_mask, SIGPROF);
		sigaddset(&act.sa_mask, SIGWINCH);
//		sigaddset(&act.sa_mask, SIGINFO);
		sigaddset(&act.sa_mask, SIGUSR1);
		sigaddset(&act.sa_mask, SIGUSR2);
//		sigaddset(&act.sa_mask, SIGTHR);
//		sigaddset(&act.sa_mask, SIGLIBRT);
	}

	if (sigaction(SIGHUP, &act, &oact) == -1)	printf("can't catch SIGHUP\n");
	if (sigaction(SIGINT, &act, &oact) == -1)	printf("can't catch SIGINT\n");
	if (sigaction(SIGQUIT, &act, &oact) == -1)	printf("can't catch SIGQUIT\n");
	if (sigaction(SIGILL, &act, &oact) == -1)	printf("can't catch SIGILL\n");
	if (sigaction(SIGTRAP, &act, &oact) == -1)	printf("can't catch SIGTRAP\n");
	if (sigaction(SIGABRT, &act, &oact) == -1)	printf("can't catch SIGABRT\n");
//	if (sigaction(SIGEMT, &act, &oact) == -1)	printf("can't catch SIGEMT\n");
	if (sigaction(SIGFPE, &act, &oact) == -1)	printf("can't catch SIGFPE\n");
	if (sigaction(SIGKILL, &act, &oact) == -1)	printf("can't catch SIGKILL\n");
	if (sigaction(SIGBUS, &act, &oact) == -1)	printf("can't catch SIGBUS\n");
	if (sigaction(SIGSEGV, &act, &oact) == -1)	printf("can't catch SIGSEGV\n");
	if (sigaction(SIGSYS, &act, &oact) == -1)	printf("can't catch SIGSYS\n");
	if (sigaction(SIGPIPE, &act, &oact) == -1)	printf("can't catch SIGPIPE\n");
	if (sigaction(SIGALRM, &act, &oact) == -1)	printf("can't catch SIGALRM\n");
	if (sigaction(SIGTERM, &act, &oact) == -1)	printf("can't catch SIGTERM\n");
	if (sigaction(SIGURG, &act, &oact) == -1)	printf("can't catch SIGURG\n");
	if (sigaction(SIGSTOP, &act, &oact) == -1)	printf("can't catch SIGSTOP\n");
	if (sigaction(SIGTSTP, &act, &oact) == -1)	printf("can't catch SIGTSTP\n");
	if (sigaction(SIGCONT, &act, &oact) == -1)	printf("can't catch SIGCONT\n");
	if (sigaction(SIGCHLD, &act, &oact) == -1)	printf("can't catch SIGCHLD\n");
	if (sigaction(SIGTTIN, &act, &oact) == -1)	printf("can't catch SIGTTIN\n");
	if (sigaction(SIGTTOU, &act, &oact) == -1)	printf("can't catch SIGTTOU\n");
	if (sigaction(SIGIO, &act, &oact) == -1)	printf("can't catch SIGIO\n");
	if (sigaction(SIGXCPU, &act, &oact) == -1)	printf("can't catch SIGXCPU\n");
	if (sigaction(SIGXFSZ, &act, &oact) == -1)	printf("can't catch SIGXFSZ\n");
	if (sigaction(SIGVTALRM, &act, &oact) == -1)printf("can't catch SIGVTALRM\n");
	if (sigaction(SIGPROF, &act, &oact) == -1)	printf("can't catch SIGPROF\n");
	if (sigaction(SIGWINCH, &act, &oact) == -1)	printf("can't catch SIGWINCH\n");
//	if (sigaction(SIGINFO, &act, &oact) == -1)	printf("can't catch SIGINFO\n");
	if (sigaction(SIGUSR1, &act, &oact) == -1)	printf("can't catch SIGUSR1\n");
	if (sigaction(SIGUSR2, &act, &oact) == -1)	printf("can't catch SIGUSR2\n");
//	if (sigaction(SIGTHR, &act, &oact) == -1)	printf("can't catch SIGTHR\n");
//	if (sigaction(SIGLIBRT, &act, &oact) == -1) printf("can't catch SIGLIBRT\n");
}
