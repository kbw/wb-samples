#ifndef SIGNAL_HANDLER_H
#define	SIGNAL_HANDLER_H

#define MAX_WORKERS 10

const char* to_string(int sig);
void sighandler(int sig);
void set_signalhandler2(int child);

#endif
