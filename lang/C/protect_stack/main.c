#include "signal_handler.h"
#include <pthread.h>	// pthreads
#include <limits.h>		// PTHREAD_STACK_MIN
#include <string.h>		// memset
#include <stdlib.h>		// atoi, exit
#include <stdio.h>		// prinft

int worker_n;
pthread_t worker_threads[MAX_WORKERS];

void sighandler(int sig)
{
	printf("%s: pthread=%lx\n", to_string(sig), pthread_self());

	int i;
	for (i = 0; i < MAX_WORKERS; ++i)
		if (pthread_self() == worker_threads[i])
		{
			printf("%s: pthread=%lx, matched on worker thread\n", to_string(sig), pthread_self());
			exit(1);
		}
}

void* work(void* param)
{
	if (param)
	{
		printf("work: threadid=0x%lx\n", pthread_self());
		worker_threads[worker_n++] = pthread_self();

		set_signalhandler2(1);
		param = NULL;
	}

	static int count = 0;

	int bufsz = 1024;
	char buf[bufsz];
	memset(buf, 0, bufsz);
	snprintf(buf, bufsz, "work: %d", ++count);
	printf("%s\n", buf);

	return work(param);
}

int main(int argc, char* argv[])
{
	set_signalhandler2(0);

	int stacksz = argc > 1 ? atoi(argv[1])*1024 : 0;
	if (stacksz < PTHREAD_STACK_MIN)
		stacksz = PTHREAD_STACK_MIN;

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setstacksize(&attr, stacksz);
	printf("main: threadid=0x%lx, setting the stack size to: %dk\n", pthread_self(), stacksz / 1024);

	pthread_t work_id;
	pthread_create(&work_id, &attr, work, work);
	void* ret;
	pthread_join(work_id, &ret);

	printf("done\n");
	return 0;
}
