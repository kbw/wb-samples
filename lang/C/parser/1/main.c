#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/* EXPR.C: (C) Copyright 1985, Allen I. Holub. All rights reserved
 *
 *   Evaluate an expression pointed to by str. Expressions evaluate
 *   right to left unless parenthesis are present. Valid operators are
 *   * + - / for multiply add, subtract and divide. The expression must
 *   be formed from the character set { 0123456789+-*()/ }. White
 *   space is not allowed.
 *
 *   <expr>      ::=    <factor>
 *                    | <factor> * <expr>
 *                    | <factor> / <expr>
 *                    | <factor> + <expr>
 *                    | <factor> - <expr>
 *
 *   <factor>    ::=     ( <expr> )
 *                    | -( <expr> )
 *                    |  <constant>
 *                    | -<constant>
 *
 *   <constant>  ::=  A string of ASCII chars in the range '0'-'9'.
 *
 *---------------------------------------------------------------
 *  Global variables:
 */

static int factor();
static int constant();
static int expr();

static char *Str ;   /* Current position in string being parsed    */
static int  Error;   /* # of errors found so far                   */

/*--------------------------------------------------------------*/
#ifdef DEBUG

int     parse(char* expression, int* err);

int main(int argc, char** argv)
{
        /*     Routine to exercise the expression parser. If an
         *     expression is given on the command line it is
         *     evaluated and the result is printed, otherwise
         *     expressions are fetched from stdin (one per line)
         *     and evaluated. The program will return -1 to the
         *     shell on a syntax error, 0 if it's in interactive
         *     mode, otherwise it returns the result of  the
         *     evaluation.
         */

        char buf[133], *bp = buf;
        int err, rval;

        if( argc > 2 )
        {
                fprintf(stderr, "Usage: expr [<expression>]");
                exit( -1 );
        }

        if( argc > 1 )
        {
                rval = parse( argv[1], &err );
                printf(err ? "*** ERROR ***" : "%d", rval );
                exit( rval );
        }

        printf("Enter expression or <CR> to exit program\n");

        while( 1 )
        {
                printf("?");

                if(gets(buf) == NULL || !*buf )
                       exit(0);

                rval= parse(buf, &err);

                if( err )
                        printf("*** ERROR ***\n");
                else
                        printf("%s: %d\n", buf, rval);
        }
}

#endif
/*------------------------------------------------------------------*/

int     parse(char* expression, int* err)
{
        /* Return the value of "expression" or 0 if any errors were
         * found in the string. "*Err" is set to the number of errors.
         * "Parse" is the "access routine" for expr(). By using it you
         * need not know about any of the global vars used by expr().
         */

        register int        rval;

        Error = 0;
        Str = expression;
        rval = expr();
        return( (*err = Error) ? 0 : rval );
}

/*------------------------------------------------------------------*/

static int expr()
{
        int    lval;

        lval = factor();

        switch (*Str)
        {
        case '+':  Str++;  lval += expr();    break;
        case '-':  Str++;  lval -= expr();    break;
        case '*':  Str++;  lval *= expr();    break;
        case '/':  Str++;  lval /= expr();    break;
        default :                             break;
        }

        return( lval );
}

/*------------------------------------------------------------------*/

static int factor()
{
        int    rval = 0 , sign = 1 ;

        if ( *Str == '-' )
        {
                sign = -1 ;
                Str++;
        }

        if ( *Str != '(' )
                rval = constant();
        else
        {
                Str++;
                rval = expr();

                if ( *Str ==')')
                        Str++;
                else
                {
                        printf("Mis-matched parenthesis\n");
                        Error++ ;
                }
        }

        return (rval * sign);
}

/*------------------------------------------------------------------*/

static int constant()
{
        int    rval = 0;

        if( !isdigit( *Str ))
                Error++;

        while ( *Str && isdigit(*Str) )
        {
                 rval = (rval * 10) + (*Str - '0');
                 Str++;
        }

        return( rval );
}
