/* see: date --date='@2147483647' */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define	BASIC_LOG	1

int verbose;

int main(int argc, char* argv[])
{
	int i;

	for (i = 1; i < argc; ++i)
	{
		if (*argv[i] == '-')
		{
			if (strcmp(argv[i], "-v"))
				++verbose;

			continue;
		}

		time_t t = atoi(argv[i]);
		if (verbose >= BASIC_LOG)
			fprintf(stderr, "%ld = atoi(\"%s\"\n", t, argv[i]);

		struct tm* out = localtime(&t);
		fprintf(stdout, "%s\t%04d/%02d/%02d %02d:%02d:%02d\n",
			argv[i],
			out->tm_year + 1900,
			out->tm_mon + 1,
			out->tm_mday,
			out->tm_hour,
			out->tm_min,
			out->tm_sec);
	}

	return 0;
}
