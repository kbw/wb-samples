#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int64_t fib(int n)
{
	int i;
	int64_t f0 = 0, f1 = 1, f;
	int sign = (n >= 0 || n % 2) ? 1 : -1;
	n = abs(n);

	switch (n) {
	case 0: return sign*f0;
	case 1: return sign*f1;
	default:
		for (i = 2; i <= n; ++i) {
			f = f0 + f1;
			if (f0 < f1)
				f0 = f;
			else
				f1 = f;
		}
		return sign*f;
	}
}

int main(int argc, char* argv[])
{
	int i;
	int min = 0, max = 9;
	switch (argc) {
	case 1:
		break;
	case 2:
		max = atoi(argv[1]);
		break;
	default:
		min = atoi(argv[1]);
		max = atoi(argv[2]);
		break;
	}

	for (i = min; i <= max; ++i)
		printf("fib(%d) = %ld\n", i, fib(i));
	return 0;
}
