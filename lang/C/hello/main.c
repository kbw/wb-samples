#include <unistd.h>
#include <stdio.h>

int main()
{
	const char text[] = "Hello, World\n";
	const size_t textsz = sizeof(text) - 1;
	size_t nbytes;

	for (nbytes = 0; nbytes < textsz; ++nbytes) {
		ssize_t n = write(STDOUT_FILENO, text + nbytes, textsz - nbytes);
		if (n <= 0)
			break;

		nbytes += (size_t)n;
	}

	return 0;
}
