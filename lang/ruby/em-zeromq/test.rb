require 'em-zeromq'

EM.run do
	zmq = EM::ZeroMQ::Context.new(1)

	monitor_controller = zmq.socket(ZMQ::PULL)
	monitor_controller.bind("tcp://*:11008")

	monitor_controller.on(:message) { |msg|
		message.close
	}
end
