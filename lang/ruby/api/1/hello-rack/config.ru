require 'rack'

class HelloRack
	def call(env)
#		[200, {"Content-Type" => "text/html"}, [env.to_s]]

		req = Rack::Request.new(env)
		case req.path_info
		when /hello/
			[200, {"Content-Type" => "text/html"}, ['hello']]
		when /bye/
			[200, {"Content-Type" => "text/html"}, ['goodbye']]
		when /info/
			[200, {"Content-Type" => "text/html"}, [env.to_s]]
		else
			[404, {"Content-Type" => "text/html"}, ["lost"]]
		end
	end
end

run HelloRack.new
