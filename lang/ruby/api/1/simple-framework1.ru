# $:.unshift File.dirname(__FILE__)
#require 'simple_framework'
#require 'action'

class Action
	attr_reader :headers, :body, :request

	def initialize(&block)
		@block = block
		@status = 200
		@headers = {"Content-Type" => "text/html"}
		@body = ""
	end

	def status(value = nil)
		value ? @status = value : @status
	end

	def params
		request.params
	end

	def call(env)
		@request = Rack::Request.new(env)
		@body = self.instance_eval(&@block)
		[status, headers, [body]]
	end
end

#----------------------------------------------------------------------------

class SimpleFramework1
	def self.app
		@app ||= begin
			Rack::Builder.new do
				map "/" do
					run ->(env) {[404, {'Content-Type' => 'text/plain'}, ['Page Not Found!']] }
				end
			end
		end
	end
end

#----------------------------------------------------------------------------

def route(pattern, &block)
	SimpleFramework1.app.map(pattern) do
		run Action.new(&block)
	end
end

#----------------------------------------------------------------------------

route("/hello") do
	"Hello #{params['name'] || "World"}!"
end

route("/goodbye") do
	status 500
	"Goodbye Cruel World!"
end

run SimpleFramework1.app
