require 'rack'
require 'thin'

class HelloWorld
  def call(env)
#		puts "env=#{env}"
    [200, {"Content-Type" => "text/html"}, ["Hello World!"]]
  end
end

run HelloWorld.new
