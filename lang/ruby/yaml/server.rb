require 'psych'

#o = File.open("server.log")
#puts "File.open('server.log'): #{o}"

o = Psych::load_stream( File.open("server.log") )
#puts "Psych::load_stream(File.open('server.log')): #{o}"
puts "#{o}"

#o = File.open("server2.log")
#puts "File.open('server2.log'): #{o}"

o = Psych::load_stream( File.open("server2.log") )
#puts "Psych::load_stream(File.open('server2.log')): #{o}"
puts "#{o}"

=begin
o = Psych::load_file("env.yaml")
puts "#{o}"

begin
  o = Psych::parse("--- \"", "env1.yaml")
  puts "#{o}"
rescue Psych::SyntaxError => ex
	puts "#{ex.file}"
	puts "#{ex.message}"
end

o = Psych::parse("--- hello", "env.yaml")
puts "#{o}"
=end


