# usage: ruby prog.rb --config file1 file2 ... filen
require 'psych'
require 'yaml'

# return file name array or nil
def get_param name
	key = '--' + name
  #puts "key=#{key}"
	#puts "ARGV.length=#{ARGV.length}"

	if ARGV.include?(key) && (ARGV.index(key)+1 < ARGV.length) && (ARGV[ARGV.index(key)+1][0] != "-")
		# build new arg list containing just the files that match --name
		arg_start = ARGV.index(key) + 1
	  arg_end = arg_start
		#while (arg_end < ARGV.length) && (ARGV[arg_end][0] != "-") do
		while (arg_end < ARGV.length) && (ARGV[arg_end][0] != "-") do
			#puts "index=#{arg_end} ARGV[arg_end][0]=#{ARGV[arg_end][0]} ok=#{ARGV[arg_end][0] != "-"}"
			arg_end += 1
		end

		(arg_start == arg_end) ? ARGV[arg_start] : ARGV[arg_start .. arg_end-1]
	else
		nill
	end
end

# return yaml stream or nil
def get_yaml names
#	puts "names=#{names}"
	out = String.new

	names.each { |file|
#	names.reverse_each { |file|
		content = File.read(file)
		out += content + "\n"
#		out += "--- " + content + "\n"
  }

	out
end

def into pristine override
  override.select { |val|
		if pristine.include? val
			into val pristine[val]
		end
	}
end

#puts "#{get_param("config")}"
#puts "#{get_yaml get_param("config")}"
#puts "#{YAML.load get_yaml( get_param("config") )}"
#puts "#{Psych.parse get_yaml( get_param("config") )}"

#puts "#{get_yaml( get_param("config") )}"
#puts "#{get_yaml( get_param("config") ).class}"

puts "#{YAML.load_file( "env.yaml" )}"
puts "#{YAML.load_file( "env.yaml" ).class}"

puts "#{YAML.load( get_yaml( get_param("config") ) )}"
puts "#{YAML.load( get_yaml( get_param("config") ) ).class}"

puts "#{Psych.parse( get_yaml( get_param("config") ) )}"
puts "#{Psych.parse( get_yaml( get_param("config") ) ).class}"
#puts "#{YAML.load Psych.dump( get_yaml( get_param("config") ) )}"
