#include <yaml-cpp/yaml.h>
#include <string>
#include <list>
#include <algorithm>
#include <memory>
#include <list>
#include <exception>
#include <iostream>
#include <fstream>

typedef std::list<YAML::Node> nodes_t;
typedef std::list<std::string> strings_t;

inline std::string to_string(bool val) { return val ? "true" : "false"; }

std::unique_ptr<strings_t> get_opt(int argc, char** argv, const std::string& arg)
{
	const std::string tag = "--" + arg;

	char** begin = argv + 1;
	char** end = argv + argc;
	char** p = std::find_if(begin, end, [&](char* p) { return strcmp(p, tag.c_str()) == 0; });
	if (p != end) {
		std::unique_ptr<strings_t> out(new strings_t);

		for (++p; (p != end) && (**p != '-'); ++p)
			out->emplace_back(*p);

		return out;
	}

	return nullptr;
}

namespace
{
	strings_t mk_keys(const YAML::Node& node)
	{
		strings_t out;

		if (node.IsMap()) {
			for (YAML::const_iterator p = node.begin(); p != node.end(); ++p)
				if (p->first.IsScalar())
					out.push_back(p->first.as<std::string>());
		}

		return out;
	}

	bool has_key(const strings_t& keys, const std::string& key)
	{
		return std::find(keys.begin(), keys.end(), key) != keys.end();
	}
}

YAML::Node append(const YAML::Node& a, const YAML::Node& b)
{
	strings_t akeys = mk_keys(a);
	strings_t bkeys = mk_keys(b);
	strings_t keys = akeys;
	std::copy(bkeys.begin(), bkeys.end(), std::back_inserter(keys));
	keys.sort();
	keys.unique();

	if (!keys.empty()) {
		YAML::Node merged;

		for (strings_t::const_iterator p = keys.begin(); p != keys.end(); ++p) {
			const std::string& key = *p;

			if ((!a.IsNull() && has_key(akeys, key)) && (!b.IsNull() && has_key(bkeys, key)))
				merged[key] = append(a[key], b[key]);
			else if ((!a.IsNull() && has_key(akeys, key)) && (b.IsNull() || !has_key(bkeys, key))) 
				merged[key] = append(a[key], YAML::Node());
			else if (!b.IsNull() && has_key(bkeys, key))
				merged[key] = append(YAML::Node(), b[key]);
		}

		return merged;
	}
	else if (!a.IsNull())
		return a;
	else if (!b.IsNull())
		return b;
	else
		return YAML::Node();
}

YAML::Node get_config(int argc, char* argv[], const std::string& arg)
{
	YAML::Node config;

	std::unique_ptr<strings_t> args = get_opt(argc, argv, arg);
	if (args.get() && !args->empty()) {
		const strings_t& strs = *args.get();
		int count = 0;
		for (const std::string& str : strs)
			config = (count++ == 0) ? YAML::LoadFile(str) : append(config, YAML::LoadFile(str));
	}

	return config;
}

int main(int argc, char* argv[])
try
{
	std::cout << get_config(argc, argv, "config");
}
catch (const std::exception& e)
{
	std::cerr << "fatal: " << e.what() << std::endl;
}
