require 'yaml'
require 'yaml/store'

def get_opt arg
	arg = "--" + tag

	if ARGV.include? arg
		out = Array.new

		idx = ARGV.index(arg) + 1
		while (idx < ARGV.length) && (ARGV[idx][0] != "-")
			out.push(ARGV[idx])
			idx = idx + 1
		end

		out
	else
		nil
	end
end

def hash_append a, b
	keys = Array.new
	keys = keys + a.keys if a.is_a? Hash
	keys = keys + b.keys if b.is_a? Hash
	keys.uniq!

	if !keys.empty?
		merged = Hash.new
		keys.collect {|key|
			if (a.is_a?(Hash) && a.has_key?(key)) && (b.is_a?(Hash) && b.has_key?(key))
				merged[key] = hash_append a[key], b[key]
			elsif (a.is_a?(Hash) && a.has_key?(key)) && !(b.is_a?(Hash) && b.has_key?(key))
				merged[key] = hash_append a[key], nil
			elsif (b.is_a?(Hash) && b.has_key?(key))
				merged[key] = hash_append nil, b[key]
			end
		}
		merged
	elsif !a.nil?
		 a
	elsif !b.nil?
		 b
	else
		 nil
	end
end

def get_config arg
	args = get_opt(arg)
	if !args.nil? && args.length > 0
		config = YAML.load_file(args[0])
		for i in 1..args.length-1
			config = hash_append( config, YAML.load_file(args[i]) )
		end
	end

	config
end

def get_opt_names arg, default
	out = default

	args = get_opt arg
	if !args.nil?
		out = "--" + arg
		args.each {|val| out = out + " #{val}"}
	end

	out
end

def get_param name
    key = '--' + name
    ARGV.include?(key) ? ARGV[ARGV.index(key) + 1] : nil
end

config = get_config "config"
puts "config=#{config}"

if !config.nil? && !config.keys.empty?
	store = YAML::Store.new "out.yaml"
	store.transaction do
		config.keys.collect {|key| store[key] = config[key] }
	end
end
