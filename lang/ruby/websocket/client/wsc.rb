require 'faye/websocket'
require 'eventmachine'
require 'json'

EM.run {
# ws = Faye::WebSocket::Client.new('ws://www.example.com/')
  ws = Faye::WebSocket::Client.new('ws://api.hitbtc.com:80')

  ws.on :open do |event|
    p [:open]
  end

  ws.on :message do |event|
    p JSON.parse event.data
  end

  ws.on :close do |event|
    p [:close, event.code, event.reason]
    ws = nil
  end
}
