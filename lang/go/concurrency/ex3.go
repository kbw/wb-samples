package main

import (
	"os"
	"strconv"
	"fmt"
	"time"
	"math/rand"
)

func main() {
	max := 0

	nArgs := len(os.Args)
	for i := 1; i < nArgs; i++ {
		switch (os.Args[i]) {
		case "-n":
			if ((i + 1) < nArgs) {
				val, err := strconv.Atoi(os.Args[i + 1])
				if (err != nil) {
					fmt.Println("error while parsing -n: ", err)
					return
				}

				i++
				max = val
			} else {
				fmt.Println("error while parsing -n: ", "no value specified")
				return
			}

		default:
			fmt.Println("error while parsing command line args: ", os.Args[i])
			return
		}
	}
	
	rand.Seed(int64(time.Millisecond))
	for i := 0; i < max; i++ {
		go booring(i, "Steph")
	}

	time.Sleep(1 * time.Second)
}

func booring(id int, msg string) {
	for i := 0; ; i++ {
		fmt.Println(id, msg, i)
//		time.Sleep(10 * time.Millisecond)
		time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
	}
}
