package main

import (
	"os"
	"strconv"
	"fmt"
	"time"
	"math/rand"
)

func main() {
	max := 1
	runtime := 2
	wait := 1

	nArgs := len(os.Args)
	for i := 1; i < nArgs; i++ {
		switch (os.Args[i]) {
		case "-n":
			if ((i + 1) < nArgs) {
				val, err := strconv.Atoi(os.Args[i + 1])
				if (err != nil) {
					fmt.Println("error while parsing -n: ", err)
					return
				}

				i++
				max = val
			} else {
				fmt.Println("error while parsing -n: ", "no value specified")
				return
			}

		case "-t":
			if ((i + 1) < nArgs) {
				val, err := strconv.Atoi(os.Args[i + 1])
				if (err != nil) {
					fmt.Println("error while parsing -t: ", err)
					return
				}

				i++
				runtime = val
			} else {
				fmt.Println("error while parsing -t: ", "no value specified")
				return
			}

		case "-w":
			if ((i + 1) < nArgs) {
				val, err := strconv.Atoi(os.Args[i + 1])
				if (err != nil) {
					fmt.Println("error while parsing -w: ", err)
					return
				}

				i++
				wait = val
			} else {
				fmt.Println("error while parsing -w: ", "no value specified")
				return
			}

		default:
			fmt.Println("error while parsing command line args: ", os.Args[i])
			return
		}
	}
	
	rand.Seed(int64(time.Millisecond))
	for i := 0; i < max; i++ {
		go booring(i, wait, "Steph")
	}

	time.Sleep(time.Duration(runtime) * time.Second)
}

func booring(id int, wait int, msg string) {
	time.Sleep(time.Duration(wait) * time.Second)
	for i := 0; ; i++ {
		fmt.Println(id, msg, i)
//		time.Sleep(10 * time.Millisecond)
		time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
	}
}
