/*
// Server2 is a minimal "echo" and counter server.
package main

import (
    "fmt"
    "log"
    "net/http"
    "sync"
)

var mu sync.Mutex
var count int

func main() {
    http.HandleFunc("/", handler)
    http.HandleFunc("/count", counter)
    log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

// handler echoes the Path component of the requested URL.
func handler(w http.ResponseWriter, r *http.Request) {
    mu.Lock()
    count++
    mu.Unlock()
    fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)
}

// counter echoes the number of calls so far.
func counter(w http.ResponseWriter, r *http.Request) {
    mu.Lock()
    fmt.Fprintf(w, "Count %d\n", count)
    mu.Unlock()
}
 */
#include <crow.h>

std::mutex mtx;
int count{};

int main() {
	crow::SimpleApp app;

	CROW_ROUTE(app, "/")
	([](const crow::request& req) {
	 	{
	 		std::lock_guard<std::mutex> lock(mtx);
			++count;
		}
		return "URL.path = " + req.url;
	});

	CROW_ROUTE(app, "/count")
	([]() {
	 	return "Count " + std::to_string(count);
	});

	app.port(8000).run();
}
