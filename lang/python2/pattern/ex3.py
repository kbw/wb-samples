from pattern.vector import Document, Model

d1 = Document('I am fond of pigs. Pigs treat us as equals.', name='pig1')
d2 = Document('Never wrestle with pigs. Pigs like it.', name='pig2')
d3 = Document('A kid is a baby goat. It''s cute and has redeeming social value.', name='goat1')
d4 = Document('When a goat likes a book, the whole book is gone.', name='goat2')
 
m = Model([d1, d2, d3, d4])
m.reduce(2)
  
for d in m.documents:
	print
	print d.name
	for concept, w1 in m.lsa.vectors[d.id].items():
		for feature, w2 in m.lsa.concepts[concept].items():
			if w1 != 0 and w2 != 0:
				print (feature, w1 * w2) 
