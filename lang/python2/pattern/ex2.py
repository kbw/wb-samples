from pattern.vector import Document, Model, TFIDF
 
d1 = Document('A tiger is a big yellow cat with stripes.', type='tiger')
d2 = Document('A lion is a big yellow cat with manes.', type='lion',)
d3 = Document('An elephant is a big grey animal with a slurf.', type='elephant')
print d1.vector
print d2.vector
print d3.vector

m = Model(documents=[d1, d2, d3], weight=TFIDF)   
print d1.vector
print d2.vector
print d3.vector
print
print m.similarity(d1, d2) # tiger vs. lion
print m.similarity(d1, d3) # tiger vs. elephant
