// https://blog.aaronballman.com/2011/08/static-polymorphism-in-c/
//
#include <stdio.h>

template< class Derived >
class Base {
public:
    bool CanPerformAction() {
        return static_cast< Derived * >( this )->canPerformActionImpl();
    }
};
  
class Derived1: public Base< Derived1 > {
    friend class Base< Derived1 >;
private:
    bool canPerformActionImpl() {
        return true;
    }
};
  
class Derived2: public Base< Derived2 > {
    friend class Base< Derived2 >;
private:
    bool canPerformActionImpl() {
        return false;
    }
};
 
int main( void ) { 
  Base< Derived1 > *b1 = new Derived1();
  Base< Derived2 > *b2 = new Derived2();
 
  ::printf( "b1 %s perform the action.\nb2 %s perform the action\n", 
    b1->CanPerformAction() ? "can" : "cannot",
    b2->CanPerformAction() ? "can" : "cannot" );
 
  return 0;
}
