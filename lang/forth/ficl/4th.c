#include <ficl.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	FICL_SYSTEM* pSYS;
	FICL_VM* pVM;

	if ((pSYS = ficlInitSystem(10*1024)))
	{
		if ((pVM = ficlNewVM(pSYS)))
		{
			char buf[128] = { 0 };
			while (fgets(buf, sizeof(buf) - 1, stdin))
				if (ficlExec(pVM, buf) == VM_USEREXIT)
				{
					ficlFreeVM(pVM);
					break;
				}
		}

		ficlTermSystem(pSYS);
	}

	return 0;
}
