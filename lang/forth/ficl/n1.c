#include <ficl.h>

static ficlSystemInformation* buildSI(void* context, int dictionarySize, int stackSize)
{
	static ficlSystemInformation ficlSI;

	ficlSystemInformationInitialize(&ficlSI);
	ficlSI.context = context;
	ficlSI.dictionarySize = dictionarySize;
	ficlSI.stackSize = stackSize;

	if (ficlSI.dictionarySize <= 0)
		ficlSI.dictionarySize = FICL_DEFAULT_DICTIONARY_SIZE;

	if (ficlSI.stackSize <= 0)
		ficlSI.stackSize = FICL_DEFAULT_STACK_SIZE;

	ficlSI.environmentSize = FICL_DEFAULT_DICTIONARY_SIZE;

	return &ficlSI;
}

int main(int argc, char* argv[])
{
	ficlSystem* ficlSys;

	ficlSys = ficlSystemCreate(buildSI(NULL, 0, 0));
	ficlSystemDestroy(ficlSys);

	return 0;
}
