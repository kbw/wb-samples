#include <ficl.h>

int main(int argc, char* argv[])
{
	FICL_SYSTEM* pSYS;
	FICL_VM* pVM;
	int ret;

	if ((pSYS = ficlInitSystem(10*1024)))
	{
		if ((pVM = ficlNewVM(pSYS)))
		{
			ret = ficlExec(pVM, ".( hello) CR");
			ficlFreeVM(pVM);
		}

		ficlTermSystem(pSYS);
	}

	return 0;
}
