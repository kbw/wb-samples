fn by_ref(x: &i32) -> i32 {
    *x + 1
}

fn sqr(x: f64) -> f64 {
    return x * x;
}

fn main() {
    println!("Hello, world!");

    let i = 10;
    let res1 = by_ref(&i);
    let res2 = by_ref(&41);
    println!("{} {}", res1,res2);

    let n = 3;
    assert_eq!(n, 3);
    println!("n = {}", n);

    let mut sum = 0.0;
    for i in 0..4 {
        sum += i as f32;
    }
    println!("sqr(sum) = {}", sqr(sum as f64));
}
