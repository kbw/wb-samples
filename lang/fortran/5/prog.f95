MODULE MSR
   IMPLICIT NONE

CONTAINS
   subroutine amuxms (n, x, y, a,ja)
      real*8  x(*), y(*), a(*)
      integer n, ja(*)
      integer i, k

      do 10 i=1, n
         y(i) = a(i)*x(i)
10    continue
      do 100 i = 1, n
         do 99 k = ja(i), ja(i + 1) - 1
            y(i) = y(i) + a(k) * x(ja(k))
99       continue
100   continue
      return
   end
END MODULE

PROGRAM MSRtest
   USE MSR
   IMPLICIT NONE
   INTEGER :: i
   REAL(KIND(0.D0)), DIMENSION(4) :: y, x = (/ 0., 1.3, 4.2, 0.8 /)

   REAL(KIND(0.D0)), DIMENSION(9) :: AA = (/ 1.01, 4.07, 6.08, 9.9, 0., 2.34, 3.12, 1.06, 2.2 /)
   INTEGER , DIMENSION(9)         :: JA = (/ 6, 7, 7, 8, 10, 3, 1, 1, 3 /)
   CALL amuxms(4,x,y,aa,ja)

   WRITE(6,FMT='(4F8.3)') (y(I), I=1,4)
END PROGRAM
