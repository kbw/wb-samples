#include <variant>
#include <vector>
#include <string>
#include <iostream>

struct apple {
	void fall() {}
};

struct banana {
	int yellowness;
	banana(int yellowness) : yellowness(yellowness) {}
};

struct triangle {
	double h, w;
	triangle(double h, double w) : h(h), w(w) {}
	double area() const { return h*w; }
};

using thing = std::variant<apple, banana, triangle>;
using things = std::vector<thing>;

int main() {
	using v_sl_t = std::variant<std::string, long>;

	v_sl_t a;
	a = "hello";
	std::cout << std::get<0>(a) << '\n';

	try {
		std::cout << std::get<1>(a) << '\n';
	}
	catch (const std::bad_variant_access &e) {
		std::clog << "fatal: " << e.what() << '\n';
	}
	std::cout << "dome\n";
}
