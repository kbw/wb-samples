#include <variant>
#include <string>
#include <iostream>

int main() {
	using v_sl_t = std::variant<std::string, long>;

	v_sl_t a;
	a = "hello";
	std::cout << std::get<0>(a) << '\n';

	try {
		std::cout << std::get<1>(a) << '\n';
	}
	catch (const std::bad_variant_access &e) {
		std::clog << "fatal: " << e.what() << '\n';
	}
	std::cout << "dome\n";
}
