#include <cstdint>
#include <string>
#include <tuple>
#include <variant>
#include <vector>

struct DigitalPin {
	DigitalPin(int p, bool v) : mPin{p}, mValue{v} {}
	void set() const {}
	const uint8_t mPin;
	bool mValue;
};

auto digi_out = [](const DigitalPin* const pin) { pin->set(); };

struct AnalogPin {
	AnalogPin(uint8_t pin, int value) : mPin{pin}, mValue{value} {}
	void write() const {}
	const uint8_t mPin;
	int mValue;
};

struct SerialPort {
	SerialPort(int p) : mPortNum{p} {}
	void send() const {}
	const int mPortNum;
	std::string mMsg;
};

using var_def = std::variant<DigitalPin*, AnalogPin*, SerialPort*>;

DigitalPin digi{1, true};
AnalogPin anl{20, 3};
SerialPort serial{20000};
std::vector<var_def> outputs{&digi, &anl, &serial};

void out() {
	for (auto& o : outputs) {
		[](var_def const& value) {
			switch (value.index()) {
			case 0:	std::get<DigitalPin*>(value)->set(); break;
			case 1: std::get<AnalogPin*>(value)->write(); break;
			case 2: std::get<SerialPort*>(value)->send(); break;
			}

			if (auto res = std::get_if<DigitalPin*>(&value))
				(*res)->set();
			if (auto res = std::get_if<AnalogPin*>(&value))
				(*res)->write();
			if (auto res = std::get_if<SerialPort*>(&value))
				(*res)->send();
		}(o);  // call lambda in immediate mode

		std::visit(out_overload);
	}
}

int main() {
	out();
}
