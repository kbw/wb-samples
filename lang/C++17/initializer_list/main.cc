#include <initializer_list>
#include <iostream>

template <typename T>
struct VectorObj {
	VectorObj(std::initializer_list<T> args) {
		std::size_t i = 0;
		for (auto p = args.begin(); p != args.end(); ++p, ++i)
			std::cout << "arg[" << i << "]: " << *p << '\n';
	}
};

int main() {
	VectorObj v{1.2, 2.2, 3.3};
}
