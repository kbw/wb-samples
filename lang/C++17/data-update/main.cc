#include <avakar/destructure.h>

#include <cstring>
#include <fstream>
#include <iostream>
#include <optional>
#include <string>
#include <string_view>
#include <stdexcept>
#include <tuple>
#include <vector>
#include <utility>

//----------------------------------------------------------------------------

template <typename D, typename S>
inline D convert_cast(S src)
{
    D dst = static_cast<D>(src);
    if (static_cast<S>(dst) != src)
        throw std::runtime_error{"convert_cast<> failed"};
    return dst;
}

template <>
unsigned short convert_cast<unsigned short, std::string_view>(std::string_view src) {
    unsigned short n{};
    for (std::size_t i = 0; i != src.size(); ++i) {
        if (src[i] > '9' || src[i] < '0')
            throw std::runtime_error{"convert_cast<> failed"};
        n *= 10 * i;
        n += (src[i] - '0');
    }
    return n;
}

//----------------------------------------------------------------------------

struct IUpdater {
    virtual void dataUpdate(std::string_view name,
                            std::vector<std::pair<unsigned short, std::string_view>> snapshot) = 0;
};

//----------------------------------------------------------------------------

struct PersonSnapshot {
    std::string recName;
	std::optional<unsigned short> nchildren;
};

struct DataSource : public IUpdater {
    void dataUpdate(std::string_view name,
                    std::vector<std::pair<unsigned short, std::string_view>> snapshot) {
	using avakar::destructure;

	PersonSnapshot ps;
    auto&& pst = destructure(ps);
	ps.recName = name;
	std::cout << " name: " << std::get<0>(pst) << '\n';
#ifdef UNHIDE
        std::cout << "tag=" << name << " : ";
        for (const auto& [fid, value] : snapshot)
            std::cout << '\"' << fid << "=" << value << "\", ";
        std::cout << std::endl;
#endif // UNHIDE
    }
};

struct Reader {
    Reader(const std::string& filename) : istream_(filename) {
    }

    void Register(IUpdater* updater) {
        clients_.push_back(updater);
    }

    void Run() {
        std::string line;
        while (std::getline(istream_, line)) {
            auto data = Parse(line);
            for (auto client : clients_) {
                client->dataUpdate(std::get<0>(data), std::get<1>(data));
            }
        }
    }

    std::tuple<std::string_view, std::vector<std::pair<unsigned short, std::string_view>>> Parse(const std::string& line) const {
        const auto separator = ',';
        const auto kv_separator = '=';
        const char* begin = line.c_str();
        const char* end;

        // read name
        end = std::strchr(begin, separator);
        std::size_t length{end ? static_cast<std::size_t>(end - begin) : std::strlen(begin)};
        std::string_view name{begin, length};

        // read key/value pairs
        std::vector<std::pair<unsigned short, std::string_view>> pairs;
        for (begin += length + 1; begin < (line.c_str() + line.size()); begin += length + 1) {
            end = std::strchr(begin, separator);
            length = end ? static_cast<std::size_t>(end - begin) : std::strlen(begin);

            const char* kv_end = std::strchr(begin, kv_separator);
            if (!kv_end || kv_end > (line.c_str() + line.size()))
                throw std::runtime_error{"bad input: " + line};

            std::size_t k_length{static_cast<std::size_t>(kv_end - begin)};
            std::size_t v_length{length - k_length - 1};
            std::string_view key{begin, k_length};
            std::string_view val{kv_end + 1, v_length};
            pairs.push_back({convert_cast<unsigned short>(key), val});
        }

        return {name, pairs};
    }

    std::ifstream istream_;
    std::vector<IUpdater*> clients_;
};
 
//----------------------------------------------------------------------------

int main()
try {
    DataSource datasource;

    Reader reader("data.csv");
    reader.Register(&datasource);
    reader.Run();
}
catch (const std::exception& e) {
    std::clog << "fatal: " << e.what() << '\n';
}
