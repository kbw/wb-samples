// https://en.cppreference.com/w/cpp/utility/functional/boyer_moore_searcher
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <fstream>
#include <memory>
#include <string>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <iomanip>

std::string load_text()
{
	constexpr auto filename{ "bible.txt" };
	struct stat info;
	int rc = stat(filename, &info);
	if (rc == -1)
		throw std::runtime_error("cannot find: " + std::string{filename});

	auto buffer{ std::make_unique<char[]>(info.st_size) };
	std::ifstream file{ filename, std::ios::binary };
	file.read(buffer.get(), info.st_size);
	return {buffer.get(), static_cast<std::size_t>(info.st_size)};
}

auto default_search(const std::string& str, const std::string& substr)
{
	return std::search(str.begin(), str.end(),
                       std::default_searcher(substr.begin(), substr.end()));
}

auto boyer_moore_search(const std::string& str, const std::string& substr)
{
	return std::search(str.begin(), str.end(),
                       std::boyer_moore_searcher(substr.begin(), substr.end()));
}

auto boyer_moore_horspool_search(const std::string& str, const std::string& substr)
{
	return std::search(str.begin(), str.end(),
                       std::boyer_moore_horspool_searcher(substr.begin(), substr.end()));
}

int main()
{
	using namespace std::chrono_literals;

    std::string haystack{ load_text() };
    const std::string needle {"Jesus wept"};

	auto timer_start = std::chrono::high_resolution_clock::now();
    auto it = default_search(haystack, needle);
	auto timer_stop = std::chrono::high_resolution_clock::now();
    std::cout << "default_search " << " m=" << haystack.size() << " n=" << needle.size()
              << " : " << std::chrono::duration_cast<std::chrono::microseconds>(timer_stop - timer_start).count() << " usec"
              << '\n';

	timer_start = std::chrono::high_resolution_clock::now();
    it = boyer_moore_search(haystack, needle);
	timer_stop = std::chrono::high_resolution_clock::now();
    std::cout << "boyer_moore_search " << " m=" << haystack.size() << " n=" << needle.size()
              << " : " << std::chrono::duration_cast<std::chrono::microseconds>(timer_stop - timer_start).count() << " usec"
              << '\n';

	timer_start = std::chrono::high_resolution_clock::now();
    it = boyer_moore_horspool_search(haystack, needle);
	timer_stop = std::chrono::high_resolution_clock::now();
    std::cout << "boyer_moore_horspool_search " << " m=" << haystack.size() << " n=" << needle.size()
              << " : " << std::chrono::duration_cast<std::chrono::microseconds>(timer_stop - timer_start).count() << " usec"
              << '\n';

}
