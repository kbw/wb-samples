#pragma once
#include <type_traits>
#include <stdexcept>
#include <stdlib.h>

template <typename T>
class SimpleAllocator {
public:
    using value_type = T;
    using is_always_equal = std::true_type; // c++17

    SimpleAllocator() = default;
    
    template <typename U>
    SimpleAllocator(const SimpleAllocator<U>& other) {
        (void) other;
    }
    
    T* allocate(size_t n) {
        auto ptr = static_cast<T*>(malloc(sizeof(T) * n));
        if (ptr)
            return ptr;
            
        throw std::bad_alloc();
    }
    
    void deallocate(T* ptr, size_t n) {
        (void) n;
        free(ptr);
    }
};
