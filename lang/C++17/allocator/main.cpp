#include "SimpleAllocator.hpp"
#include "CacheAlignedAllocator.hpp"

#include <thread>
#include <vector>

using WorkerVector = std::vector<CacheAlignedAllocator<int>>;

int main() {
    constexpr size_t nThreads{4};
    constexpr size_t pageSize{4*1024};
    constexpr size_t vecSize{16*pageSize};

    std::vector<WorkerVector, SimpleAllocator<WorkerVector>> threads;
    threads.reserve(nThreads);
    for (size_t i = 0; i != nThreads; ++i) {
    }
}
