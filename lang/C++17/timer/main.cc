#include "timer.hpp"

inline void f() {
	Timer t("f");
}

int main() {
	Timer t("main");
	f();
}
