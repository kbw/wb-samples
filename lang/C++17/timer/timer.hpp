#pragma once
#include <chrono>
#include <iostream>

class Timer {
	class TimerImpl {
		using ClockType = std::chrono::steady_clock;

		ClockType::time_point start_;

	public:
		TimerImpl() : start_{ClockType::now()} {}

		void reset() { start_ = ClockType::now(); }

		template <typename T = std::chrono::microseconds>
		auto checkpoint() const {
			return std::chrono::duration_cast<T>(ClockType::now() - start_).count();
		}
	};

	const char* const id_;
	TimerImpl timer_;

public:
	Timer(const char* id) : id_(id) {}

	const char* id() const { return id_; }

	void reset() { return timer_.reset(); }

	template <typename T = std::chrono::nanoseconds>
	auto dt() const { return timer_.checkpoint<T>(); }

	~Timer() {
		auto t{ dt<std::chrono::nanoseconds>() };
		std::cout << id() << ": " << t << "ns\n";
	}
};

