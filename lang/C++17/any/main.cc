// https://www.oreilly.com/ideas/c++17-upgrades-you-should-be-using-in-your-code
// https://en.cppreference.com/w/cpp/types/type_index
//
#include <any>
#include <vector>
#include <typeinfo>
#include <typeindex>
#include <unordered_map>
#include <iostream>

int main()
{
	std::unordered_map<std::type_index, const char*> type_names {
		{ std::type_index(typeid(char)), "char" },
		{ std::type_index(typeid(unsigned char)), "unsigned char" },
		{ std::type_index(typeid(int)), "int" },
		{ std::type_index(typeid(unsigned)), "unsigned int" },
		{ std::type_index(typeid(short)), "short" },
		{ std::type_index(typeid(unsigned short)), "unsigned short" },
		{ std::type_index(typeid(long)), "long" },
		{ std::type_index(typeid(unsigned long)), "unsigned long" },
		{ std::type_index(typeid(float)), "float" },
		{ std::type_index(typeid(double)), "double" },
		{ std::type_index(typeid(bool)), "bool" },
		{ std::type_index(typeid(char*)), "char*" },
		{ std::type_index(typeid(const char*)), "const char*" }
	};

	std::vector<std::any> container { 1, 2.2, false, "hi!" };
	for (std::vector<std::any>::iterator p = std::begin(container); p != std::end(container); ++p) {
		std::unordered_map<std::type_index, const char*>::iterator q = type_names.find( std::type_index(p->type()) );
		if ( q != std::end(type_names) )
			std::cout << q->second << std::endl;
	}
}
