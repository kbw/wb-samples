#include <unistd.h>

#include <boost/asio/io_service.hpp>
#include <boost/thread.hpp>
#include <boost/core/ref.hpp>

#include <memory>
#include <iostream>

#include <stdlib.h>

void thread_func(boost::asio::io_service& io_service) {
	io_service.run();
	std::clog << "."; std::flush(std::clog);
}

int main(int argc, char* argv[]) {
	const int nthreads = (argc > 1) ? atoi(argv[1]) : 2;

	boost::asio::io_service io_service;
	std::shared_ptr<boost::asio::io_service::work> work{ std::make_shared<boost::asio::io_service::work>(io_service) };

	boost::thread_group thread_group;
	for (auto i = 0; i < nthreads; ++i)
		thread_group.create_thread( boost::bind(thread_func, boost::ref(io_service)) );

	struct timespec tm = {0, 10*1000};
	nanosleep(&tm, nullptr);

//	io_service.stop();
	work.reset();
	thread_group.join_all();

	std::clog << "done\n";
}
