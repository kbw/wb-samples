#include <array>
#include <algorithm>
#include <vector>
#include <utility>

constexpr std::size_t APPLE{0};
constexpr std::size_t PEAR{1};
constexpr std::size_t BLACK{2};
constexpr std::size_t WHITE{3};
constexpr std::size_t TRAIN{4};
constexpr std::size_t PLAIN{5};

constexpr std::size_t maxSize{48};

struct Entry {
	bool valid{};
	int payload{};

	bool operator<(const Entry &n) const {
		if (valid && n.valid)
			return payload < n.payload;
		return n.valid;
	}
};
using Entries = std::vector<Entry>;
struct Book : std::array<Entries, 6> {
	Book() {
		at(APPLE).resize(2*maxSize);
		at(PEAR).resize(2*maxSize);
		at(BLACK).resize(2);
		at(WHITE).resize(2);
		at(TRAIN).resize(1);
		at(PLAIN).resize(1);
	}
};

Book createBook() {
	return Book{};
}

int main() {
	Book book = createBook();
	std::sort(book[APPLE].begin(), book[PEAR].end());
	std::sort(book[PEAR].begin(), book[PEAR].end());
	std::sort(book[BLACK].begin(), book[BLACK].end());
	std::sort(book[WHITE].begin(), book[WHITE].end());
	std::sort(book[TRAIN].begin(), book[TRAIN].end());
	std::sort(book[PLAIN].begin(), book[PLAIN].end());
}
