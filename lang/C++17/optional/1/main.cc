// https://en.cppreference.com/w/cpp/utility/optional/value_or
// https://en.cppreference.com/w/cpp/utility/optional/value
//
#include <optional>
#include <iostream>
#include <cstdlib>

std::optional<const char*> maybe_getenv(const char* n)
{
	if (const char* x = std::getenv(n))
		return x;
	else
		return {};
}

int main()
{
	auto ret = maybe_getenv("USERNAME");
	std::cout << ret.value_or("kbw") << std::endl;

	try
	{
		std::cout << ret.value() << std::endl;
	}
	catch (const std::bad_optional_access& e)
	{
		std::clog << "fatal: " << e.what() << std::endl;
	}
}
