#include "optional.hpp"

#include <string>

#include <assert.h>

int main() {
	optional<std::string> a;
	assert(!a.has_value());
	assert(a.value().size() == 0);

	optional<std::string> b;
	b = {"hello world"};
	assert(b.has_value());
	assert(b.value() == "hello world");
}
