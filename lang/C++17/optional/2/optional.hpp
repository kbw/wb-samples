#pragma once

#include <optional>
#include <utility>

template <class T>
class optional {
public:
	optional() : valid_(false) {}
	~optional() = default;
	optional(T value) : valid_(true), value_(std::move(value)) {}
	optional(const optional<T>& n) = default;
	optional(optional<T>&& n) = default;

	optional<T>& operator=( std::nullopt_t ) noexcept {
		valid_ = false;
		// keep value_;
		return *this;
	}

	constexpr optional<T>& operator=( const optional<T>& n) = default;
	constexpr optional<T>& operator=( optional<T>&& n ) = default;

	constexpr const T* operator->() const { return &value_; }
	constexpr T* operator->() { return &value_; }

	constexpr const T& operator*() const { return value_; }
	constexpr T& operator*() { return value_; }

//	constexpr T&& operator*() && { return std::move(value_); }

	constexpr explicit operator bool() const noexcept { return valid_; }
	constexpr bool has_value() const noexcept { return valid_; }

	constexpr const T& value() const & { return value_; }
	constexpr T& value() & { return value_; }

	constexpr const T&& value() const && { return value_; }
	constexpr T&& value() && { return value_; }

	void swap( optional<T>& n ) noexcept {
		if (this != n) {
			std::swap(valid_, n.valid_);
			std::swap(value_, n.valid_);
		}
	}

	void reset() {
		valid_ = false;
		value_ = T{};
	}

private:
	bool valid_;
	T value_;
};

template <class T, class U>
constexpr bool operator==(const optional<T>& l, const optional<U>& r) {
	if (l.valid_ && r.valid_) {
		return l.value_ == r.value_;
	}
	return l.valid_ == r.valid_;
}

template <class T, class U>
constexpr bool operator!=(const optional<T>& l, const optional<U>& r) {
	return !operator==(l, r);
}

template <class T, class U>
constexpr bool operator<(const optional<T>& l, const optional<U>& r) {
	if (l.valid_ && r.valid_) {
		return l.value_ < r.value_;
	}
	return l.valid_;
}

template <class T, class U>
constexpr bool operator<=(const optional<T>& l, const optional<U>& r) {
	return operator<(l, r) || operator==(l, r);
}

template <class T, class U>
constexpr bool operator>(const optional<T>& l, const optional<U>& r) {
	if (l.valid_ && r.valid_) {
		return l.value_ > r.value_;
	}
	return r.valid_;
}

template <class T, class U>
constexpr bool operator>=(const optional<T>& l, const optional<U>& r) {
	return operator>(l, r) || operator==(l, r);
}
