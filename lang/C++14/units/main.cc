#include <ratio>
#include <math.h>
#include "RQuantity.hpp"


int main() {
	QLength x(2.3), y(3.2), z(8.2);

	// valid and compile-time dimension checked
	QLength diagonal = Qsqrt(x*x + y*y + z*z);

	// gives a compile-time error
//	QLength dimMismatch = Qsqrt(x + y + z);
}
