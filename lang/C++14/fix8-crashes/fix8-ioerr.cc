#include <unistd.h>
#include <stdlib.h>
#include <thread>
#include <iostream>

//----------------------------------------------------------------------------

struct Async {
	std::atomic<std::thread*> _thread{nullptr};

	virtual ~Async();
	virtual void join(std::thread* new_thread = nullptr);
};

Async::~Async() {
	join();
}

void Async::join(std::thread* new_thread) {
	if (std::thread* thread = _thread.exchange(new_thread)) {
		thread->join();
	}
}

//----------------------------------------------------------------------------

struct Reader : public Async {
	std::atomic<bool> _done{};

	void start();
	void stop();
	static void process(Reader& self);
};

void Reader::start() {
	join(new std::thread(process, std::ref(*this)));
}

void Reader::stop() {
	_done = true;
	join();
}

void Reader::process(Reader& self) {
	while (!self._done)
		sleep(1);
}

//----------------------------------------------------------------------------

struct Writer : public Async {
	void start();
	void stop();
	void process();
};

//----------------------------------------------------------------------------

struct Connection {
	Reader _reader;
//	Writer _writer;

	void start();
	void stop();
	void process();
};

void Connection::start() {
	_reader.start();
//	_writer.start();
}

void Connection::stop() {
	_reader.stop();
//	_writer.stop();
}

void Connection::process() {
	_reader.join();
}

//----------------------------------------------------------------------------

int main(int argc, char* argv[])
try {
	Connection conn;
	conn.start();
	sleep(5);
	conn.process();

	std::clog << "done\n";
/*
	int test = (argc > 1) ? atoi(argv[1]) : 0;
	std::unique_ptr<std::thread> writer, reader;

	switch (test) {
	case 1:
		writer.reset( new std::thread([&]{ sleep(1); writer->join(); }) );
		break;

	case 0:
	default:
		int delay = (argc > 2) ? atoi(argv[2]) : 4;
		writer.reset( new std::thread([&]{ sleep(1); reader->join(); }) );
		reader.reset( new std::thread([&]{ sleep(1); }) );
		sleep(delay);
		writer->join();
		reader->join();
	}
 */
}
catch (const std::exception &e) {
	std::clog << e.what() << "\n";
	return 1;
}
catch (...) {
	std::clog << "unknown stop\n";
	return 2;
}
