#include <unistd.h>
#include <stdlib.h>
#include <thread>
#include <iostream>

int main(int argc, char* argv[])
try {
	int test = (argc > 1) ? atoi(argv[1]) : 0;
	std::unique_ptr<std::thread> writer, reader;

	switch (test) {
	case 1:
		writer.reset( new std::thread([&]{ sleep(1); writer->join(); }) );
		break;

	case 0:
	default:
		int delay = (argc > 2) ? atoi(argv[2]) : 4;
		writer.reset( new std::thread([&]{ sleep(1); reader->join(); }) );
		reader.reset( new std::thread([&]{ sleep(1); }) );
		sleep(delay);
		writer->join();
		reader->join();
	}
}
catch (const std::exception &e) {
	std::clog << e.what() << "\n";
	return 1;
}
catch (...) {
	std::clog << "unknown stop\n";
	return 2;
}
