#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/WebSocket.h"
#include "Poco/Net/HTTPClientSession.h"
#include <memory>
#include <iostream>

using Poco::Net::HTTPClientSession;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPMessage;
using Poco::Net::WebSocket;

int main(int args,char **argv)
{
	try {
		HTTPClientSession cs("echo.websocket.org", 80);	

		HTTPRequest request(HTTPRequest::HTTP_GET, "/?encoding=text", HTTPMessage::HTTP_1_1);
		request.set("origin", "http://www.websocket.org");

		HTTPResponse response;

		std::unique_ptr<WebSocket> m_psock(new WebSocket(cs, request, response));

		std::string testStr("Hello echo websocket!");
		int slen = m_psock->sendFrame(testStr.c_str(), testStr.size(), WebSocket::FRAME_TEXT);
		std::cout << "Sent bytes " << slen << std::endl;

		int flags = 0;
		std::unique_ptr<char[]> receiveBuff(new char[slen + 1]);
		int rlen = m_psock->receiveFrame(receiveBuff.get(), slen, flags);
		receiveBuff[rlen] = '\0';
		std::cout << "Received bytes " << rlen << std::endl;
		std::cout << receiveBuff << std::endl;
	}
	catch (std::exception &e) {
		std::cout << "Exception " << e.what();
	}
}
