
# Build Agent 8.10.15
#
# $1 script number
# $2 xml
# $3 jackEnabled
# $5 shard

script="$1"
type=$2
jackEnabled="$3"
compilePath=$4
shard="_$5"
version=-+8.10.15
compiler=-gcc

if [ "${type}" = "xml" ]; then
  LIBVAR="-lsiteconfidenceinterfacex${version}${compiler} -lxerces-c"
else
  LIBVAR="-lsiteconfidenceinterface${version}${compiler}"
fi

CXX=g++
CPPFLAGS=-w -pthread -O3 -DNDEBUG -D_FORTIFY_SOURCE=2 -DSHARD_AWARE -DOPENSSL_NO_KRB5 -DSC_CRASH_HANDLER -DSC_WEBKIT_V89
INCLUDES=\
 -I${compilePath}/agent/src/include \
 -I/home/sc/agent/sclibs/include/mysql \
 -I/home/sc/agent/sclibs/include/xercesc \
 -I/home/sc/agent/sclibs/include/htmlcxx \
 -I/home/sc/agent/sclibs/include/openssl \
 -I/home/sc/agent/sclibs/include
STATIC_LIBS=
DYN_LIBS=\
 -L${compilerPath}/lib -lsiteconfidence${version}${compiler} ${LIBVAR} \
 -L${compilerPath}/lib -lscwebkit${version}${compiler} -lwebkitwrapper${version}${compiler} -lcurlwrapper${version}${compiler} \
 -L${compilerPath}/lib/gcc -lboost_regex -lboost_system -lmysqlclient \
 -L/home/sc/agent/sclibs/lib -lssl -lcrypto -ljson -lscparser -lntlm -lXt -lX11 \
 -licui18n -licuuc -licudata -lsqlite3 -lxslt -lz -lxml2 -ljpeg -lpng12 -ldl -lglib-2.0 -lpthread -lstdc++ -ljson \
 -lTSETestClass

echo ${CXX} ${CPPFLAGS} ${INCLUDE} \
 -o /home/sc/build/testScript_${scriptId}${shard} testScript_${scriptId}${shard}.cpp \
 -Wl,--rpath -Wl,${compilePath}/lib:${compilePath}/lib/gcc \
 -Wl,-Bstatic  ${STATIC_LIBS} \
 -Wl,-Bdynamic ${DYN_LIBS}
