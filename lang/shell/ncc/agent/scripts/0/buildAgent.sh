#!/bin/sh

# Arg:
#   ${1} is the build directory
#   ${2} is the step [1-5] or ALL

STEP=${2}

VERSION=081013
VERSION_STR=8.10.13

case ${STEP} in
	[1A]* )
		# create clean target directories
		(mkdir -p /home/sc/lib/${VERSION}/agent/src/include/ncc; \
		 mkdir -p /home/sc/lib/${VERSION}/agent/src/lib; \
		 mkdir -p /home/sc/lib/${VERSION}/bin; \
		 mkdir -p /home/sc/lib/${VERSION}/lib; \
		 mkdir -p /home/sc/agent/sclibs/lib; \
		 rm /home/sc/lib/${VERSION}/lib/*);;
esac

case ${STEP} in
	[2A]* )
		# copy header files and archived libs
		(cd ${1} && \
		 cp -dpv include/*.h /home/sc/lib/${VERSION}/agent/src/include/ && \
		 cp -dpv include/ncc/*.hpp /home/sc/lib/${VERSION}/agent/src/include/ncc/ && \
		 cp -dpRv buildbin/lib/* /home/sc/lib/${VERSION}/lib/;);;
esac

case ${STEP} in
	[3A]* )
		# build TSETest
		(cd ${1}/tse && \
		  ./make.sh && \
		  cp -dpv TSETestClass.h /home/sc/agent/sclibs/include && \
		  cp -dpv libTSETestClass.a /home/sc/agent/sclibs/lib);;
esac

case ${STEP} in
	[4A]* )
		# build all
		(cd ${1} && \
		 make STATIC_LINK=true NOSECURE_FLAGS=true -j -B release);;
esac

case ${STEP} in
	[5A]* )
		# fixup lib sym links
		(cd /home/sc/lib/${VERSION}/lib/ && \
		  ln -s libcurlwrapper-${VERSION_STR}-gcc.a  -s libcurlwrapper.a && \
		  ln -s libcurlwrapper-${VERSION_STR}-gcc.so -s libcurlwrapper.so && \
		  ln -s libsiteconfidence-${VERSION_STR}-gcc.a  -s libsiteconfidence.a && \
		  ln -s libsiteconfidence-${VERSION_STR}-gcc.so -s libsiteconfidence.so && \
		  ln -s libsiteconfidenceinterface-${VERSION_STR}-gcc.a  -s libsiteconfidenceinterface.a && \
		  ln -s libsiteconfidenceinterface-${VERSION_STR}-gcc.so -s libsiteconfidenceinterface.so && \
		  ln -s libsiteconfidenceinterfacex-${VERSION_STR}-gcc.a  -s libsiteconfidenceinterfacex.a && \
		  ln -s libsiteconfidenceinterfacex-${VERSION_STR}-gcc.so -s libsiteconfidenceinterfacex.so && \
		  ln -s libscwebkit-${VERSION_STR}-gcc.so -s libscwebkit.so && \
		  ln -s libwebkitwrapper-${VERSION_STR}-gcc.a  -s libwebkitwrapper.a && \
		  ln -s libwebkitwrapper-${VERSION_STR}-gcc.so -s libwebkitwrapper.so);;
esac
