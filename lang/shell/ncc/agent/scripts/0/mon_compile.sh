
# ${scriptId} script number
# $2 xml
# $3 jackEnabled
# $4 compile path
# $5 shard

cd /home/sc/agent/src/mon

scriptId=$1
if [ "$2" = "xml" ]; then
     Libvar="-lsiteconfidenceinterfacex -lxerces-c" 
else
     Libvar="-lsiteconfidenceinterface" 
fi
jackEnabled="$3"
compilePath=$4
shard="_$5"


if [ "$3" = "0" ]; then
  #v7
  g++ -g -DOPENSSL_NO_KRB5 -I${compilePath}/agent/src/include -I/home/sc/agent/sclibs/include/mysql -I/home/sc/agent/sclibs/include/xercesc -I/home/sc/agent/sclibs/include/htmlcxx -I/home/sc/agent/sclibs/include/openssl -I/home/sc/agent/sclibs/include -Os -o /home/sc/build/testScript_${scriptId}${shard} testScript_${scriptId}${shard}.cpp -L${compilePath}/lib -L/home/sc/agent/sclibs/lib -Wl,-Bstatic ${Libvar} -lscparser -lsiteconfidence -lntlm -lmysqlclient_r -lz -lssl -lcrypto -Wl,-Bdynamic -lpthread -lstdc++ -ljson -lboost_regex-gcc-d-1_43 -lboost_regex-gcc-1_43 -lTSETestClass

elif [ "$3" = "1" ]; then
  #v88
  g++ -O -DOPENSSL_NO_KRB5 -I${compilePath}/agent/src/include -I/home/sc/agent/sclibs/include/mysql -I/home/sc/agent/sclibs/include/xercesc -I/home/sc/agent/sclibs/include/htmlcxx -I/home/sc/agent/sclibs/include/openssl -I/home/sc/agent/sclibs/include -Os -o /home/sc/build/testScript_${scriptId}${shard} testScript_${scriptId}${shard}.cpp -L${compilePath}/lib -L/home/sc/agent/sclibs/lib -Wl,-Bstatic ${Libvar} -lscparser -L${compilePath}/lib -lgthread-2.0 -lrt -Wl,--rpath -Wl,${compilePath}/lib -Wl,--rpath,${compilePath}/lib -lsiteconfidence -lntlm -lmysqlclient_r -lz -lssl -lcrypto -Wl,-Bdynamic -lwebkitwrapper -lwebkit-1.0 -lsoupwrapper -lXt -lX11 -licui18n -licuuc -licudata -lsqlite3 -lxslt -lz -lxml2 -ljpeg -lpng12 -ldl -lglib-2.0 -lpthread -lstdc++ -ljson -lboost_regex-gcc-d-1_43 -lboost_regex-gcc-1_43 -lTSETestClass

else
  CXX=g++
  INCLUDES=\
   -I${compilePath}/agent/src/include \
   -I/home/sc/agent/sclibs/include/mysql \
   -I/home/sc/agent/sclibs/include/xercesc \
   -I/home/sc/agent/sclibs/include/htmlcxx \
   -I/home/sc/agent/sclibs/include/openssl \
   -I/home/sc/agent/sclibs/include

  if [ $compilePath = "/home/sc/lib/081015" ]; then
    # 8.10.15
    CPPFLAGS=-w -pthread -O3 -DNDEBUG -DSHARD_AWARE -DOPENSSL_NO_KRB5 -DSC_CRASH_HANDLER -DSC_WEBKIT_V89
    if [ "$2" = "xml" ]; then
      Libvar="-lsiteconfidenceinterfacex-8.10.15-gcc -lxerces-c"
    else
      Libvar="-lsiteconfidenceinterface-8.10.15-gcc"
    fi
    STATIC_LIBS=\
     -L${compilerPath}/lib -lsiteconfidence-8.10.15-gcc ${Libvar}
    DYN_LIBS=\
     -L${compilerPath}/lib -lscwebkit-8.10.15-gcc -lwebkitwrapper-8.10.15-gcc -lcurlwrapper-8.10.15-gcc \
     -L${compilerPath}/lib/gcc -lboost_regex -lboost_system -lmysqlclient \
     -L/home/sc/agent/sclibs/lib -lssl -lcrypto -ljson -lscparser -lntlm -lXt -lX11 \
     -licui18n -licuuc -licudata -lsqlite3 -lxslt -lz -lxml2 -ljpeg -lpng12 -ldl -lglib-2.0 -lpthread -lstdc++ -ljson \
     -lTSETestClass

  elif [ $compilePath = "/home/sc/lib/081014" ]; then
    # 8.10.14 -- no security flags
    CPPFLAGS=-w -pthread -O3 -DNDEBUG -DSHARD_AWARE -DOPENSSL_NO_KRB5 -DSC_CRASH_HANDLER -DSC_WEBKIT_V89
    if [ "$2" = "xml" ]; then
      Libvar="-lsiteconfidenceinterfacex-8.10.14-gcc -lxerces-c"
    else
      Libvar="-lsiteconfidenceinterface-8.10.14-gcc"
    fi
    STATIC_LIBS=\
     -L${compilerPath}/lib -lsiteconfidence-8.10.14-gcc ${Libvar}
    DYN_LIBS=\
     -L${compilerPath}/lib -lscwebkit-8.10.14-gcc -lwebkitwrapper-8.10.14-gcc -lcurlwrapper-8.10.14-gcc \
     -L${compilerPath}/lib/gcc -lboost_regex -lboost_system -lmysqlclient \
     -L/home/sc/agent/sclibs/lib -lssl -lcrypto -ljson -lscparser -lntlm -lXt -lX11 \
     -licui18n -licuuc -licudata -lsqlite3 -lxslt -lz -lxml2 -ljpeg -lpng12 -ldl -lglib-2.0 -lpthread -lstdc++ -ljson \
     -lTSETestClass

  elif [ $compilePath = "/home/sc/lib/081013" ]; then
    # 8.10.13 -- static link, no security flags
    CPPFLAGS=-w -pthread -O3 -DNDEBUG -DSHARD_AWARE -DOPENSSL_NO_KRB5 -DSC_CRASH_HANDLER -DSC_WEBKIT_V89
    if [ "$2" = "xml" ]; then
      Libvar="-lsiteconfidenceinterfacex-8.10.13-gcc -lxerces-c"
    else
      Libvar="-lsiteconfidenceinterface-8.10.13-gcc"
    fi
    STATIC_LIBS=\
     -L${compilerPath}/lib -lsiteconfidence-8.10.13-gcc ${Libvar}
    DYN_LIBS=\
     -L${compilerPath}/lib -lscwebkit-8.10.13-gcc -lwebkitwrapper-8.10.13-gcc -lcurlwrapper-8.10.13-gcc \
     -L${compilerPath}/lib/gcc -lboost_regex -lboost_system -lmysqlclient \
     -L/home/sc/agent/sclibs/lib -lssl -lcrypto -ljson -lscparser -lntlm -lXt -lX11 \
     -licui18n -licuuc -licudata -lsqlite3 -lxslt -lz -lxml2 -ljpeg -lpng12 -ldl -lglib-2.0 -lpthread -lstdc++ -ljson \
     -lTSETestClass
  fi

  echo ${CXX} ${CPPFLAGS} ${INCLUDE} \
   -o /home/sc/build/testScript_${scriptId}${shard} testScript_${scriptId}${shard}.cpp \
   -Wl,--rpath -Wl,${compilePath}/lib:${compilePath}/lib/gcc \
   -Wl,-Bstatic  ${STATIC_LIBS} \
   -Wl,-Bdynamic ${DYN_LIBS}

  ${CXX} ${CPPFLAGS} ${INCLUDE} \
   -o /home/sc/build/testScript_${scriptId}${shard} testScript_${scriptId}${shard}.cpp \
   -Wl,--rpath -Wl,${compilePath}/lib:${compilePath}/lib/gcc \
   -Wl,-Bstatic  ${STATIC_LIBS} \
   -Wl,-Bdynamic ${DYN_LIBS}

# else
    #default to v89/v810
#   g++ -O3 -w -DOPENSSL_NO_KRB5 -I${compilePath}/agent/src/include -I/home/sc/agent/sclibs/include/mysql -I/home/sc/agent/sclibs/include/xercesc -I/home/sc/agent/sclibs/include/htmlcxx -I/home/sc/agent/sclibs/include/openssl -I/home/sc/agent/sclibs/include -Os -o /home/sc/build/testScript_${scriptId}${shard} testScript_${scriptId}${shard}.cpp -L${compilePath}/lib -L/home/sc/agent/sclibs/lib -Wl,-Bstatic ${Libvar} -lscparser -L${compilePath}/lib -lgthread-2.0 -lrt -Wl,--rpath -Wl,${compilePath}/lib -Wl,--rpath,${compilePath}/lib -lsiteconfidence -lntlm -lmysqlclient_r -lz -lssl -lcrypto -Wl,-Bdynamic -lwebkitwrapper -lscwebkit -lcurlwrapper -lXt -lX11 -licui18n -licuuc -licudata -lsqlite3 -lxslt -lz -lxml2 -ljpeg -lpng12 -ldl -lglib-2.0 -lpthread -lstdc++ -ljson -lboost_regex-gcc-d-1_43 -lboost_regex-gcc-1_43 -lTSETestClass

  fi
fi

strip /home/sc/build/testScript_${scriptId}${shard}
gzip -9 /home/sc/build/testScript_${scriptId}${shard}
mv /home/sc/build/testScript_${scriptId}${shard}.gz /home/sc/build/testScript_${scriptId}${shard}
