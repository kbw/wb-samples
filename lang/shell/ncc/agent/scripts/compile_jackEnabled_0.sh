
# compile Agent 7
#
# $1 script number
# $2 xml
# $3 jackEnabled
# $4 compile path
# $5 shard

cd /home/sc/agent/src/mon

if [ "$2" = "xml" ]; then
     Libvar="-lsiteconfidenceinterfacex -lxerces-c" 
else
     Libvar="-lsiteconfidenceinterface" 
fi
script="$1"
jackEnabled="$3"
compilePath=$4
shard="_$5"

# g++ -g -DOPENSSL_NO_KRB5 -I$compilePath/agent/src/include -I/home/sc/agent/sclibs/include/mysql -I/home/sc/agent/sclibs/include/xercesc -I/home/sc/agent/sclibs/include/htmlcxx -I/home/sc/agent/sclibs/include/openssl -I/home/sc/agent/sclibs/include -Os -o /home/sc/build/testScript_$1$shard testScript_$1$shard.cpp -L$compilePath/lib -L/home/sc/agent/sclibs/lib -Wl,-Bstatic $Libvar -lscparser -lsiteconfidence -lntlm -lmysqlclient_r -lz -lssl -lcrypto -Wl,-Bdynamic -lpthread -lstdc++ -ljson -lboost_regex-gcc-d-1_43 -lboost_regex-gcc-1_43 -lTSETestClass
