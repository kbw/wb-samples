
# $1 script number
# $2 xml
# $3 jackEnabled
# $4 compile path
# $5 shard

case ${jackEnabled} in
0 )
	# compile Agent 7
	eval "./compile_jackEnabled_0.sh" ${1} ${2} ${3} ${4} ${5}
	;;

1 )
	# compile Agent 8.8
	eval "./compile_jackEnabled_1.sh" ${1} ${2} ${3} ${4} ${5}
	;;

* )
    compilePath=$4
    version=${compilePath:13:6}
	scriptName=compile_${version}.sh 

	if [ ${version} > 081012 ]
	then
	    # compile Agent 8.10.13 or later
		eval "./compile_${version}.sh" ${1} ${2} ${3} ${4} ${5}
	else
	    # compile Agent 8.9/8.10
		eval "./compile_jackEnabled_2.sh" ${1} ${2} ${3} ${4} ${5}
	fi
	;;
esac

# strip /home/sc/build/testScript_$1$shard
# gzip -9 /home/sc/build/testScript_$1$shard
# mv /home/sc/build/testScript_$1$shard.gz /home/sc/build/testScript_$1$shard
