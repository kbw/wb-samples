#!/bin/sh

# Bourne shell regular # expression handling
# call with:
#  rgx.sh /home/sc/lib/010813
#  rgx.sh /home/sc/lib/010814
#
# It'll vector off the call to another script or call a default script

version=${1}

home=/home/sc/
scver_prefix=${home}lib/

# check if we are in the correct directory to begin with
if expr "${version}" : ${scver_prefix} != 13
then
	echo 'incorrect lib spec'
fi

# expr "${version}" : '/home/sc/lib/0810*'
# expr "${version}" : '[a-zA-Z]*'
# expr "${version}" : '[^a-zA-Z]*'
# expr "${version}" : '[^a-zA-Z]*[a-zA-Z]'
# expr "${version}" : '[^a-zA-Z]*[a-zA-Z]*'
