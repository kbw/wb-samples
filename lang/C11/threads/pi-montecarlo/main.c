/*
 * https://webbusy.wordpress.com/2012/07/19/%cf%80-in-c/
 */

#include <math.h>
#include <threads.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

struct Info
{
	size_t inPoints;
	size_t totalPoints;
};

struct Point
{
	double x;
	double y;
};

int func(void* param)
{
	struct Info* info = (struct Info*)param;
	if (info)
	{
		// do some work
		for (size_t i = 0; i != info->totalPoints; ++i)
		{
			struct Point pt = { .x = rand()/(double)RAND_MAX, .y = rand()/(double)RAND_MAX };
			double h = pt.x*pt.x + pt.y*pt.y;
			if (h <= 1.0)
				++(info->inPoints);
		}
	}

	return 0;
}

double pi(size_t nthreads, size_t niters)
{
	struct Info* info = calloc(niters, sizeof(struct Info));
	memset(info, 0, niters*sizeof(struct Info));

	// start the threads
	thrd_t* handles = calloc(nthreads, sizeof(thrd_t));
	for (size_t i = 0; i != nthreads; ++i) {
		info[i].totalPoints = niters;
		thrd_create(&handles[i], func, &info[i]);
	}

	// wait for the threads to complete
	for (size_t i = 0; i != nthreads; ++i)
		thrd_join(handles[i], NULL);

	// process the reply
	size_t inPoints = 0;
	size_t totalPoints = 0;
	for (size_t i = 0; i != nthreads; ++i) {
		inPoints += info[i].inPoints;
		totalPoints += info[i].totalPoints;
	}
	const double pi = (4.0 * (double)inPoints / (double)totalPoints);

	return pi;
}

int main(int argc, char* argv[])
{
	srand(2012);

	const size_t NITERS   = (size_t)(argc > 1 ? atoi(argv[1]) : 100*1000*1000);
	const size_t NTHREADS = (size_t)(argc > 2 ? atoi(argv[2]) : 4);

	const double pi_value = pi(NTHREADS, NITERS);
	printf("pi: %f err: %f\n", pi_value, fabs(acos(-1.0) - pi_value));

	return 0;
}
