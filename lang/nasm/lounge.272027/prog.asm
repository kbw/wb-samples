section .data
	msg: db "hello ",0      
	enter: db "enter name",11,0

section .bss
	name: resb 20

section .text
	global _start

_start:
	mov rax,enter
	call _printFunc
	call _enterName
	mov rax,msg
	call _printFunc
	mov rax,name
	call _printFunc
	mov rax,60
	mov rdi,0
	syscall

_printFunc:
	push rax
	mov rbx,0
	
_printloop:
 	inc rax
	inc rbx
	mov cl,[rax]
	cmp cl,0
	jne _printloop

 	mov rax,1
	mov rdi,1
	pop rsi
	mov rdx,rbx
	syscall
	ret

_enterName:
	 mov rax,0
	 mov rdi,0
	 mov rsi,name
	 mov rdx,20
	 syscall
	 ret
