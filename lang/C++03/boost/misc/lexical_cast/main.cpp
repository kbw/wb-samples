#include <boost/lexical_cast.hpp>
#include <exception>
#include <string>
#include <sstream>
#include <iostream>

const char* g_argv[] = { "234", "2016", "+", "elephants", "9", nullptr };

int main()
{
	using boost::lexical_cast;
	using boost::bad_lexical_cast;

	for (const char** argv = g_argv; *argv; ++argv)
	{
		try
		{
			std::ostringstream os;
			os << "argv: " << lexical_cast<short>(*argv);
			std::cout << os.str() << std::endl;
		}
		catch(const bad_lexical_cast &e)
		{
			std::cerr << e.what() << ". value: " << (*argv) << std::endl;
		}
	}
}
