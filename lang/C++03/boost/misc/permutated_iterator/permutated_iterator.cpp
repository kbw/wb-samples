#include <boost/iterator/permutation_iterator.hpp>
#include <algorithm>
#include <iostream>

int main()
{
	int values[] = { 10, 12, 8, 6, 0, 4, 2, 14 };
	int order[] = { 0, 1, 2, 3, 7, 6, 5, 4 };

	std::sort(
		boost::make_permutation_iterator(values, order),
		boost::make_permutation_iterator(
			values + sizeof(values)/sizeof(values[0]),
			order + sizeof(order)/sizeof(order[0]) ));

	for (auto v: values)
		std::cout << v << ' ';
	std::cout << std::endl;

	for (auto v: order)
		std::cout << v << ' ';
	std::cout << std::endl;

	return 0;
}
