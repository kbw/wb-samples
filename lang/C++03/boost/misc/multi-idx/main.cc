#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/mem_fun.hpp>

#include <string>
#include <iostream>

//---------------------------------------------------------------------------

struct Employee
{
	int id;
	std::string	name;
	int ssn;

	Employee(int id, const std::string& name, int ssn) :
		id(id), name(name), ssn(ssn) {}

	uint64_t getTag() const	{ return (static_cast<uint64_t>(id) << 32) & static_cast<uint64_t>(ssn); }
};

bool operator<(const Employee& a, const Employee& b)
{
	return a.id < b.id;
}

std::ostream& operator<<(std::ostream& os, const Employee& e);

//---------------------------------------------------------------------------

using namespace boost::multi_index;

typedef boost::multi_index_container<
	Employee,
	indexed_by<
		// order by operator<
		ordered_unique<identity<Employee>>,

		// order by std::less<std::string>
		ordered_non_unique<member<Employee, std::string, &Employee::name>>,

		// order by hash on ssn
		hashed_unique<member<Employee, int, &Employee::ssn>>,

		// order by member function
		ordered_non_unique<const_mem_fun<Employee, uint64_t, &Employee::getTag>>
	>
> Employees;

std::ostream& operator<<(std::ostream& os, const Employee& e)
{
	os	<< "id=" << e.id
		<< " name=" << e.name
		<< " ssn=" << e.ssn;

	return os;
}

template <typename T>
std::ostream& write(std::ostream& os, T& iter)
{
	for (const Employee& entry : iter)
		os << entry << "\n";

	return os;
}

int main()
{
	Employees e;

	// insert into container
	e.emplace(Employee(0, "wally", 333001));
	e.emplace(Employee(1, "steph", 333002));

	// get iterators to different indexes
	Employees::nth_index<0>::type& idx1 = e.get<0>();
	Employees::nth_index<1>::type& idx2 = e.get<1>();
	Employees::nth_index<2>::type& idx3 = e.get<2>();
	Employees::nth_index<3>::type& idx4 = e.get<3>();

	// insert via indexes
	idx1.emplace(Employee(2, "manky", 333003));
	idx2.emplace(Employee(3, "bello", 333004));
	idx2.emplace(Employee(4, "keith", 333005));
	write(std::cout, idx3) << std::endl;

	// erase via an index
	Employees::nth_index<3>::type::iterator p = std::begin(idx4);
	std::advance(p, e.size() - 1);
	idx4.erase(p);
	write(std::cout, idx3) << std::endl;
}
