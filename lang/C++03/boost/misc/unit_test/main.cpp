#include <boost/config.hpp>

#define BOOST_TEST_MODULE UnitTest
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

void test();

boost::unit_test::test_suite* init_unit_test_suite(int argc, char* argv[])
{
	if (boost::unit_test_framework::test_suite *suite = BOOST_TEST_SUITE("Unit test"))
	{
		suite->add(BOOST_TEST_CASE(test));
		return suite;
	}

	return nullptr;
}

void test()
{
	BOOST_CHECK_EQUAL(1, sizeof(char));
}
