#pragma once

#if defined(__CYGWIN__) && __CYGWIN__ == 1 && defined(__GNUC__) && __GNUC__ == 4 && defined(__GNUC_MINOR__) && __GNUC_MINOR__ == 8 && defined(__GNUC_PATCHLEVEL__) && __GNUC_PATCHLEVEL__ < 4
#include <string>
#include <cstdio>

namespace std
{
  inline std::string to_string(long n)
  {
    char str[32];
    snprintf(str, sizeof(str), "%ld", n);
    return str;
  }

  inline std::string to_string(size_t n)
  {
    char str[32];
    snprintf(str, sizeof(str), "%lu", n);
    return str;
  }
}
#endif
