#include <stdint.h>
#include <stdlib.h>

class first_fit
{
private:
	typedef	unsigned char	byte_t;

	struct obj_header
	{
		obj_header();

		obj_header*	prev;
		obj_header*	next;
		size_t	size;
		byte_t*	base;
	};

	struct block_header
	{
		block_header();

		block_header*	prev;
		block_header*	next;
		size_t	size;
		byte_t*	base;
	};

public:
	first_fit();
	~first_fit();

	void* allocate(size_t n);
	void* reallocate(void* p, size_t n);
	void  deallocate(void* p, size_t n);

	first_fit(const first_fit& n) = delete;
	first_fit& operator=(const first_fit& n) = delete;

private:
	block_header* m_block_header;
};

//---------------------------------------------------------------------------
//
inline first_fit::obj_header::obj_header() :
	prev(nullptr),
	next(nullptr),
	size(0),
	base(nullptr)
{
}

inline first_fit::block_header::block_header() :
	prev(nullptr),
	next(nullptr),
	size(0),
	base(nullptr)
{
}

//---------------------------------------------------------------------------
//
first_fit::first_fit() :
	m_block_header(nullptr)
{
}

first_fit::~first_fit()
{
	block_header* p = m_block_header;
	while (p) {
		block_header* current = p;
		p = p->next;
		free(current);
	}
}

void* first_fit::allocate(size_t n)
{
	return nullptr;
}

void* first_fit::reallocate(void* p, size_t n)
{
	return nullptr;
}

void first_fit::deallocate(void* p, size_t n)
{
}

int main()
{
	first_fit heap;

	if (void* p = heap.allocate(4 * 1024)) {
		heap.deallocate(p, 4 * 1024);
	}
}
