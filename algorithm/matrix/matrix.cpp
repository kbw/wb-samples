// extracts from:
//	https://chryswoods.com/parallel_c++/map.html
#include <spdlog/spdlog.h>
#include <spdlog/fmt/fmt.h>

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef>
#include <functional>
#include <numeric>
#include <optional>
#include <ostream>
#include <stdexcept>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#ifdef HAVE_TBB
#include <execution>

#if HAVE_TBB == 2
#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>
#define HAVE_MAP_REDUCE 1
#endif  // HAVE_TBB == 2

#endif  // HAVE_TBB

namespace ka::util {
namespace detail {
template <typename CAR>
inline std::size_t get_min_container_size(const CAR& car) {
	return car.size();
}

template <typename CAR, typename... CDR>
inline std::size_t get_min_container_size(const CAR& car, const std::vector<CDR>&... cdr) {
	return std::min(car.size(), get_min_container_size(cdr...));
}
}  // namespace details

template <typename FUNC, typename... ARGS>
[[nodiscard]] auto map(FUNC func, const std::vector<ARGS>&... args) {
	using RETURN_TYPE = typename std::result_of<FUNC(ARGS...)>::type;

	std::size_t nargs = detail::get_min_container_size(args...);

	std::vector<RETURN_TYPE> result(nargs);
	for (std::size_t i = 0; i != nargs; ++i) {
		result[i] = func(args[i]...);
	}
	return result;
}

#ifdef HAVE_MAP_REDUCE
template <typename MAPFUNC, typename REDFUNC, typename... ARGS>
[[nodiscard]] auto mapReduce(MAPFUNC mapfunc, REDFUNC redfunc, const std::vector<ARGS>&... args) {
	using RETURN_TYPE = typename std::result_of<MAPFUNC(ARGS...)>::type;

	std::size_t nargs = detail::get_min_container_size(args...);

	return tbb::parallel_reduce(
			tbb::blocked_range<std::size_t>(0, nargs),
			RETURN_TYPE(0),
			[&](tbb::blocked_range<std::size_t> r, RETURN_TYPE task_result) {
				for (std::size_t i = r.begin(); i != r.end(); ++i)
					task_result = redfunc(task_result, mapfunc(args[i]...));

				return task_result;
			},
			redfunc);
}
#endif  // HAVE_MAP_REDUCE

template <typename T>
inline T sum(T x, T y) {
    return x + y;
}

template <typename T>
inline T difference(T x, T y) {
    return x - y;
}

template <typename T>
inline T multiply(T x, T y) {
    return x * y;
}
}  // namespace ka::util

namespace ka::math {
// Point
template <typename T, std::size_t D = 2>
class Point {
public:
	Point() = default;
	Point(std::array<T, D> n) : impl_(n) {
	}

#ifdef NDEBUG
	[[nodiscard]] T operator()(std::size_t index) const {
		return impl_[index];
	}

	[[nodiscard]] T& operator()(std::size_t index) {
		return impl_[index];
	}
#else
	[[nodiscard]] T operator()(std::size_t index) const {
		return impl_.at(index);
	}

	[[nodiscard]] T& operator()(std::size_t index) {
		return impl_.at(index);
	}
#endif // NDEBUG

private:
	std::array<T, D> impl_{};
};

template <typename T, std::size_t D>
[[nodiscard]] Point<T, D> operator+(const Point<T, D>& a, const Point<T, D>& b) {
	Point<T, D> c{a};
	for (std::size_t i = 0; i != D; ++i) {
		c(i) += b(i);
	}
	return c;
}

template <typename T, std::size_t D>
std::ostream& operator<<(std::ostream& os, const Point<T, D>& n) {
	os << "{";
	for (std::size_t i = 0; i != D; ++i) {
		if (i == 0) {
			os << n(i);
			continue;
		}
		os << ", " << n(i);
	}
	os << "}";
	return os;
}

template <typename T, std::size_t D>
std::string to_string(const Point<T, D>& n) {
	std::ostringstream os;
	os << n;
	return os.str();
}

template <typename T, std::size_t D>
std::string to_json(const Point<T, D>& n) {
	return to_string(n);
}

// Vector
template <typename T, std::size_t D = 2>
class Vector {
	template <typename U>
	struct Polar {
		double theta;
		U length;
	};

	Point<T, D> origin_;
	std::optional<Point<T, D>> length_;
	std::optional<std::array<Polar<T>, D>> polar_;
};

// Matrix
template <typename T>
class Matrix {
public:
	Matrix() = default;
	Matrix(std::size_t rows, std::size_t cols) :
		impl_(std::vector<std::vector<T>>(rows, std::vector<T>(cols))) {
	}
	Matrix(std::vector<std::vector<T>> n) :
		impl_(std::move(n)) {
	}

	[[nodiscard]] bool empty() const {
		return impl_.empty() || impl_[0].empty();
	}

	[[nodiscard]] std::size_t rows() const {
		return empty() ? 0 : impl_.size();
	}

	[[nodiscard]] std::size_t cols() const {
		return empty() ? 0 : impl_[0].size();
	}

	[[nodiscard]] std::tuple<std::size_t, std::size_t> size() const {
		if (empty())
			return {{}, {}};
		return {impl_.size(), impl_[0].size()};
	}

#ifdef NDEBUG
	[[nodiscard]] T operator()(std::size_t row, std::size_t col) const {
		return impl_[row][col];
	}

	[[nodiscard]] T& operator()(std::size_t row, std::size_t col) {
		return impl_[row][col];
	}
#else
	[[nodiscard]] T operator()(std::size_t row, std::size_t col) const {
		return impl_.at(row).at(col);
	}

	[[nodiscard]] T& operator()(std::size_t row, std::size_t col) {
		return impl_.at(row).at(col);
	}
#endif // NDEBUG

	[[nodiscard]] Matrix<T>& operator()(std::size_t row, std::vector<T> cols) {
#ifndef NDEBUG
		if (impl_.at(row).size() != cols.size()) {
			throw std::runtime_error{
				fmt::format(
					"size mismatch: expected {} received {}",
					impl_[row].size(),
					cols.size())};
		}
#endif
		impl_[row].swap(cols);
		return *this;
	}

	[[nodiscard]] std::vector<T> row(std::size_t index) const {
#ifdef NDEBUG
		return impl_[index];
#else
		return impl_.at(index);
#endif
	}

	std::vector<T> col(std::size_t index) const {
		std::vector<T> v;
		v.reserve(impl_.size());
		for (std::size_t row = 0; row != impl_.size(); ++row)
#ifdef NDEBUG
			v.push_back(impl_[index][index]);
#else
			v.push_back(impl_.at(index).at(index));
#endif
		return v;
	}

private:
	std::vector<std::vector<T>> impl_;
};

template <typename T>
inline T dot_product(const std::vector<T>& a, const std::vector<T>& b) {
#ifndef NDEBUG
	if (a.size() != b.size()) {
		throw std::runtime_error{
			fmt::format(
				"dot_product: a.size={} b.size()={}", a.size(), b.size())};
	}
#endif

#ifdef HAVE_MAP_REDUCE
	return ka::util::mapReduce(std::multiplies<T>(), std::plus<T>(), a, b);
#else
	auto products = ka::util::map(ka::util::multiply<T>, a, b);
	auto result = std::reduce(
#ifdef HAVE_TBB
		std::execution::par,
#endif  // HAVE_TBB
		std::begin(products), std::end(products), T{}, std::plus<>());
	return result;
#endif  // HAVE_MAP_REDUCE
}

template <typename T>
[[nodiscard]] Matrix<T> operator*(const Matrix<T>& a, const Matrix<T>& b) {
	const auto [a_rows, a_cols] = a.size();
	const auto [b_rows, b_cols] = b.size();
#ifndef NDEBUG
	if (a_cols != b_rows) {
		throw std::runtime_error{
			fmt::format(
				"multiply size error: a.cols={} b.size()={}", a_cols, b_rows)};
	}
	spdlog::debug("multiplying: {}x{} and {}x{} matrices", a_rows, a_cols, b_rows, b_cols);
#endif

	Matrix<T> out{a_rows, b_cols};
	for (std::size_t row = 0; row != out.rows(); ++row)
		for (std::size_t col = 0; col != out.cols(); ++col)
			out(row, col) = dot_product(a.row(row), b.col(col));
	return out;
}

template <typename T>
[[nodiscard]] Matrix<T> operator+(const Matrix<T>& a, const Matrix<T>& b) {
	const auto [a_rows, a_cols] = a.size();
	const auto [b_rows, b_cols] = b.size();
#ifndef NDEBUG
	if (a_cols != b_cols || a_rows != b_rows) {
		throw std::runtime_error{
			fmt::format(
				"add size error: a={}x{} b={}x{}", a_rows, a_cols, b_rows, b_cols)};
	}
	spdlog::debug("add: {}x{} and {}x{} matrices", a_rows, a_cols, b_rows, b_cols);
#endif

	Matrix<T> ret{a_rows, a_cols};
	for (std::size_t row = 0; row != a_rows; ++row)
		for (std::size_t col = 0; col != a_cols; ++col)
			ret(row, col) = a(row, col) + b(row, col);
	return ret;
}

template <typename T>
[[nodiscard]] Matrix<T> operator-(const Matrix<T>& a, const Matrix<T>& b) {
	const auto [a_rows, a_cols] = a.size();
	const auto [b_rows, b_cols] = b.size();
#ifndef NDEBUG
	if (a_cols != b_cols || a_rows != b_rows) {
		throw std::runtime_error{
			fmt::format(
				"subtract size error: a={}x{} b={}x{}", a_rows, a_cols, b_rows, b_cols)};
	}
	spdlog::debug("subtract: {}x{} and {}x{} matrices", a_rows, a_cols, b_rows, b_cols);
#endif

	Matrix<T> ret{a_rows, a_cols};
	for (std::size_t row = 0; row != a_rows; ++row)
		for (std::size_t col = 0; col != a_cols; ++col)
			ret(row, col) = a(row, col) - b(row, col);
	return ret;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T> &n) {
	os << "{";
	const auto [rows, cols] = n.size();
	for (std::size_t row = 0; row != rows; ++row) {
		if (row == 0)
			os << "{";
		else
			os << ", " << "{";

		for (std::size_t col = 0; col != cols; ++col) {
			if (col == 0)
				os << n(row, col);
			else
				os << ", " << n(row, col);
		}
		os << "}";
	}
	os << "}";
	return os;
}

template <typename T>
std::string to_string(const Matrix<T>& n) {
	std::ostringstream os;
	os << n;
	return os.str();
}

template <typename T>
inline std::string to_json(const Matrix<T>& n) {
	return to_string(n);
}
}  // namespace ka::math

//template <>
template <typename T, std::size_t D>
struct fmt::formatter<ka::math::Point<T, D>> {
	template <typename ParseContext>
	constexpr auto parse(ParseContext& ctx) {
		return ctx.begin();
	}

	template <typename FormatContext>
	auto format(const ka::math::Point<T, D>& n, FormatContext& ctx) {
		return fmt::format_to(ctx.out(), "{0}", to_json(n));
  	}
};

template <typename T>
struct fmt::formatter<ka::math::Matrix<T>> {
	template <typename ParseContext>
	constexpr auto parse(ParseContext& ctx) {
		return ctx.begin();
	}

	template <typename FormatContext>
	auto format(const ka::math::Matrix<T>& n, FormatContext& ctx) {
		return fmt::format_to(ctx.out(), "{0}", to_json(n));
  	}
};

/*
// Perceptron
template <typename T>
struct Layer {
	Layer() = default;
	Layer(std::size_t n) : values(n, {}) {
	}

	std::size_t size() const {
		return values.size();
	}

	std::vector<T> values;
};

template <typename T>
struct Connect {
	Connect(Layer<T>& a, Layer<T>&b) : a(a), b(b), weights(a.size(), b.size()) {
	}

	Layer<T>& a;
	Layer<T>& b;
	Matrix<T> weights;
};

template <typename T>
struct Perceptron {
	Perceptron() = default;
	Perceptron(std::size_t n_inputs, std::size_t n_outputs, std::vector<std::size_t> n_layers) {
		layers.emplace_back(n_inputs);

		for (std::size_t i = 0; i != n_layers.size(); ++i) {
			std::size_t n_layer = n_layers[i];
			layers.emplace_back(n_layer);
			connects.emplace_back(layers[layers.size() - 2], layers.back());
		}

		layers.emplace_back(n_outputs);
		connects.emplace_back(layers[layers.size() - 2], layers.back());
	}

	std::vector<Layer<T>> layers;
	std::vector<Connect<T>> connects;
};
 */

int main()
try {
	spdlog::default_logger()->set_level(spdlog::level::debug);

	using namespace ka::math;
	{
		Point<int, 2> pa;
		Point<int, 2> pb{{-1, 2}};
		Point<int, 2> pc = pa + pb;
		spdlog::info("{} + {} = {}", pa, pb, pc);
	}
	{
		Matrix<int> a{{{0, 1, 2, 3}, {4, 5, 6, 8}}};
		Matrix<int> b{{{8, 9, 10}, {11, 12, 13}, {14, 15, 16}, {17, 18, 19}}};
		auto c = a * b;
		auto [rows, cols] = c.size();
		spdlog::info("{} * {} = {}", a, b, c);
	}
	{
		Matrix<int> a{{{0, 1, 2, 3}, {4, 5, 6, 8}}};
		Matrix<int> b{{{8, 9, 10, 11}, {12, 13, 14, 15}}};
		auto c = a + b;
		auto [rows, cols] = c.size();
		spdlog::info("{} + {} = {}", a, b, c);
	}
	{
		Matrix<int> a{{{0, 1, 2, 3}, {4, 5, 6, 8}}};
		Matrix<int> b{{{8, 9, 10, 11}, {12, 13, 14, 15}}};
		auto c = a - b;
		auto [rows, cols] = c.size();
		spdlog::info("{} - {} = {}", a, b, c);
	}
/*
	{
		Layer<float> in{3};
		Layer<float> h1{5};
		Connect<float> layer1{in, h1};
	}
	{
		Perceptron<float> p{3, 2, {4, 5, 3}};
	}
 */
}
catch (const std::exception& e) {
	spdlog::error("fatal: {}", e.what());
}
