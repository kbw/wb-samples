PROG.matrix   = matrix
REPORT.matrix = matrix.report
SRCS.matrix   = matrix.cpp

PREFIX   ?= ${HOME}
CXXFLAGS += -g -std=c++20 -D_GLIBCXX_DEBUG -Wall -Wextra -Wno-unused-parameter -Wno-unused-function -Wno-unused-but-set-variable -Iinclude -I$(PREFIX)/include
LDFLAGS  += -L${PREFIX}/lib

INFO      ?= clang-tidy
INFOFLAGS ?= -checks='clang-analyzer-*,misc-*,modernize-*,-modernize-use-trailing-return-type,portability-*,performance-*,readability-*,-readability-identifier-length,-readability-braces-around-statements'

.PHONY: all report clean

all: $(PROG.matrix)

report: $(REPORT.matrix)

clean:
	- rm $(PROG.matrix) $(SRCS.matrix:.cpp=.o) $(SRCS.matrix:.cpp=.d)
	- rm $(REPORT.matrix) $(SRCS.matrix:.cpp=.info)

#-- specific rules ---
$(PROG.matrix): $(SRCS.matrix:.cpp=.o)
	$(LINK.cpp) -o $@ $^ $(LDFLAGS)

$(REPORT.matrix): $(SRCS.matrix:.cpp=.info)

#-- generic rules ---
%.d: %.cpp
	$(CXX) -MM $(CXXFLAGS) $< | sed 's,[^:]*:,$*.o $@ :,g' > $@

%.info: %.cpp
	$(INFO) $(INFOFLAGS) $< -- $(CXXFLAGS) > $@

#-- generated inculde file ---
include $(SRCS.matrix:.cpp=.d)
