#include <memory>
#include <limits>
#include <vector>
#include <utility>
#include <iostream>

/**
 *
 * @author Vijini
 */

class Population;
class Individual;
class SimpleDemoGA;

//---------------------------------------------------------------------------
//
class Individual {
	std::vector<int> m_genes;
	int m_fitness;

public:
	Individual(size_t gene_size) : m_genes(gene_size), m_fitness(0) {
		//Set genes randomly for each individual
		for (size_t i = 0; i != m_genes.size(); ++i)
			m_genes[i] = rand() % 2;
	}

	size_t	size() const			{ return m_genes.size(); }
	int		gene(size_t idx) const	{ return m_genes.at(idx); }
	int&	gene(size_t idx)		{ return m_genes.at(idx); }
	int		fitness() const			{ return m_fitness; }

	//Calculate fitness
	int calcFitness() {
		m_fitness = 0;

		for (size_t i = 0; i != m_genes.size(); ++i)
			if (m_genes[i])
				++m_fitness;

		return fitness();
	}
};

//---------------------------------------------------------------------------
//
class Population {
	std::vector<std::unique_ptr<Individual>> m_individuals;
	int m_fittest = 0;

public:
	//Initialize population
	Population(size_t size, size_t gene_size) : m_individuals(size), m_fittest(0) {
		for (auto& e : m_individuals)
			e.reset(new Individual(gene_size));
	}

	size_t				size() const				{ return m_individuals.size(); }
	const Individual&	individual(size_t idx) const{ return *m_individuals.at(idx); }
	Individual&			individual(size_t idx)		{ return *m_individuals.at(idx); }
	const Individual& 	front() const				{ return *m_individuals.front(); }
	Individual&			front()						{ return *m_individuals.front(); }
	const Individual& 	back() const				{ return *m_individuals.back(); }
	Individual&			back()						{ return *m_individuals.back(); }

	//Get the fittest individual
	size_t getFittestIndex() {
		int maxFit = std::numeric_limits<int>::min(); //Integer.MIN_VALUE;

		size_t maxFitIndex = 0;
		for (size_t i = 0; i != m_individuals.size(); ++i) {
			if (maxFit <= m_individuals[i]->fitness()) {
				maxFit = m_individuals[i]->fitness();
				maxFitIndex = i;
			}
		}

		m_fittest = m_individuals.at(maxFitIndex)->fitness();
		return maxFitIndex;
	}

	Individual& getFittest() {
		size_t maxFitIndex = getFittestIndex();
		return *m_individuals.at(maxFitIndex);
	}

	//Get the second most fittest individual
	size_t getSecondFittestIndex() {
		size_t maxFit1 = 0;
		size_t maxFit2 = 0;
		for (size_t i = 0; i != m_individuals.size(); ++i) {
			if (m_individuals[i]->fitness() > m_individuals[maxFit1]->fitness()) {
				maxFit2 = maxFit1;
				maxFit1 = i;
			} else if (m_individuals[i]->fitness() > m_individuals[maxFit2]->fitness()) {
				maxFit2 = i;
			}
		}
		return maxFit2;
	}

	Individual& getSecondFittest() {
		size_t maxFit2 = getSecondFittestIndex();
		return *m_individuals.at(maxFit2);
	}

	//Get index of least fittest individual
	size_t getLeastFittestIndex() {
		int minFitVal = std::numeric_limits<int>::max(); //Integer.MAX_VALUE;
		size_t minFitIndex = 0;
		for (size_t i = 0; i != m_individuals.size(); ++i) {
			if (minFitVal >= m_individuals[i]->fitness()) {
				minFitVal = m_individuals[i]->fitness();
				minFitIndex = i;
			}
		}
		return minFitIndex;
	}

	//Calculate fitness of each individual
	void calculateFitness() {
		for (size_t i = 0; i != m_individuals.size(); ++i) {
			m_individuals[i]->calcFitness();
		}
		getFittestIndex();
	}
};

//---------------------------------------------------------------------------
//
class SimpleDemoGA {
	Population m_population;
	Individual* m_fittest;
	Individual* m_secondFittest;
	int m_generationCount = 0;

public:
	SimpleDemoGA(size_t population_size = 10, size_t gene_size = 5) :
		m_population(population_size, gene_size),
		m_fittest(&m_population.front()),
		m_secondFittest(&m_population.back()),
		m_generationCount(0)
	{
	}

	const Population&	getPopulation() const		{ return m_population; }
	Population&			getPopulation()				{ return m_population; }

	int					getGenerationCount() const	{ return m_generationCount; }
	int					incGenerationCount()		{ return ++m_generationCount; }

	Individual&			fittest()					{ return *m_fittest; }
	Individual&			secondFittest()				{ return *m_secondFittest; }

	//Selection
	void selection() {
		//Select the most fittest individual
		m_fittest = &m_population.getFittest();

		//Select the second most fittest individual
		m_secondFittest = &m_population.getSecondFittest();
	}

	//Crossover
	void crossover() {
		//Select a random crossover point
		size_t crossOverPoint = rand() % m_fittest->size();

		//Swap values among parents
		for (size_t i = 0; i != crossOverPoint; ++i)
			std::swap(m_fittest->gene(i), m_secondFittest->gene(i));
	}

	//Mutation
	void mutation() {
		//Select a random mutation point
		int mutationPoint;

		//Flip values at the mutation point
		mutationPoint = rand() % m_fittest->size();
		if (m_fittest->gene(mutationPoint) == 0) {
			m_fittest->gene(mutationPoint) = 1;
		} else {
			m_fittest->gene(mutationPoint) = 0;
		}

		mutationPoint = rand() % m_fittest->size();
		if (m_secondFittest->gene(mutationPoint) == 0) {
			m_secondFittest->gene(mutationPoint) = 1;
		} else {
			m_secondFittest->gene(mutationPoint) = 0;
		}
	}

	//Get fittest offspring
	Individual& getFittestOffspring() {
		return (m_fittest->fitness() > m_secondFittest->fitness()) ? *m_fittest : *m_secondFittest;
	}

	//Replace least fittest individual from most fittest offspring
	void addFittestOffspring() {
		//Update fitness values of offspring
		m_fittest->calcFitness();
		m_secondFittest->calcFitness();

		//Get index of least fit individual
		int leastFittestIndex = m_population.getLeastFittestIndex();

		//Replace least fittest individual from most fittest offspring
		m_population.individual(leastFittestIndex) = getFittestOffspring();
	}
};

int main(int argc, char* argv[]) {
	int population_size = argc > 1 ? atoi(argv[1]) : 10000;
	int gene_size = argc > 2 ? atoi(argv[2]) : 5;
	int seed = argc > 3 ? atoi(argv[3]) : 2018;
	srand(seed);

	SimpleDemoGA demo(population_size, gene_size);

	//Calculate fitness of each individual
	demo.getPopulation().calculateFitness();
	std::cout << "Generation: " << demo.getGenerationCount() << " Fittest: " << demo.fittest().fitness() << std::endl;

	//While population gets an individual with maximum fitness
	while (demo.fittest().fitness() < gene_size) {
		demo.incGenerationCount();

		//Do selection
		demo.selection();

		//Do crossover
		demo.crossover();

		//Do mutation under a random probability
		if (rand()%7 < 5) {
			demo.mutation();
		}

		//Add fittest offspring to population
		demo.addFittestOffspring();

		//Calculate new fitness value
		demo.getPopulation().calculateFitness();
		std::cout << "Generation: " << demo.getGenerationCount() << " Fittest: " << demo.fittest().fitness() << std::endl;
	}

	std::cout << "\nSolution found in generation " << demo.getGenerationCount() << std::endl;
	std::cout << "Fitness: " << demo.fittest().fitness() << std::endl;

	std::cout << "Genes: ";
	for (size_t i = 0; i != demo.fittest().size(); ++i)
		std::cout << demo.fittest().gene(i);
	std::cout << std::endl;
}
