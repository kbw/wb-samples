#include <algorithm>
#include <array>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <iomanip>
#include <iostream>
#include <random>
#include <stdexcept>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

//----------------------------------------------------------------------------
// utils - general

template <typename T, typename U>
std::ostream& operator<<(std::ostream& os, const std::pair<T, U>& value) {
	os << "(" << value.first << ", " << value.second << ")";
	return os;
}

template <typename T>
T log(unsigned base, T x) {
	// log2 (x) = logy (x) / logy (2)
	return static_cast<T>(std::log(x)) / static_cast<T>(std::log(base));
}

//----------------------------------------------------------------------------
// utils - ga

template <typename T>
void randomize(T&& container) {
	for (auto&& value : container) {
		value.randomize();
	}
}

template <typename T>
void sort(T& container,
          std::function<bool(const typename T::value_type&, const typename T::value_type&)> pred) {
	std::sort(container.begin(), container.end(), pred);
}

template <typename T>
std::pair<T, T> mutate(std::pair<T, T> parents, typename T::real_type probability) {
	parents.first.mutate(probability);
	parents.second.mutate(probability);
	return parents;
}

template <typename T>
std::pair<T, T> crossover(std::pair<T, T> parents, typename T::real_type probability) {
	parents = mutate(parents, probability);
	T::crossover(parents);
	return parents;
}

//----------------------------------------------------------------------------

using real_type = long double;

class Genome {
public:
	using Gene = bool;
	using real_type = ::real_type;

	Genome(std::pair<real_type, real_type> domainValues, int precision)
		:	domainValues_(std::move(domainValues)),
			precision_(precision),
			nbits_(1 + static_cast<std::size_t>(log<real_type>(2, range()))),
			chromosome_(nbits_) {
	}

	[[nodiscard]] real_type fitness() const {
		return eval();
	}

	void randomize() {
		for (auto&& gene : chromosome_) {
			if (uniform_int_dist_0_1_(gen_)) {
				gene = !gene;
			}
		}
		mutate(1.0);
	}

	[[nodiscard]] std::size_t nbits() const {
		return nbits_;
	}

	void mutate(real_type probability) {
		if (nbits() == 0) {
			return;
		}
		do_mutate(probability);
	}

	static void crossover(std::pair<Genome, Genome>& pair) {
		if (pair.first.nbits() != pair.second.nbits()) {
			throw std::runtime_error("chromosome size mismatch in Genome::crossover");
		}
		for (std::size_t i = 0; i != pair.first.nbits(); ++i) {
			if (uniform_int_dist_0_1_(gen_)) {
				std::swap(pair.first.chromosome_[i], pair.second.chromosome_[i]);
			}
		}
	}

private:
	[[nodiscard]] real_type eval() const {
		static auto pNbits = static_cast<real_type>(std::pow<real_type>(2, nbits())); // power of nbits
		return domainValues_.first + (toBase10() * domain() / (pNbits - 1));
	}

	[[nodiscard]] real_type range() const {
		return domain() * precision_;
	}

	[[nodiscard]] real_type domain() const {
		return domainValues_.second - domainValues_.first;
	}

	[[nodiscard]] unsigned toBase10() const {
		unsigned sum = 0;
		unsigned p2 = 1; // power of 2
		for (auto gene : chromosome_) {
			sum += gene ? p2 : 0;
			p2 *= 2;
		}
		return sum;
	}

	void do_mutate(real_type probability) {
		auto generated = uniform_dist_0_1_(gen_);
		if (generated <= probability) {
			static std::uniform_int_distribution<> dist{0, static_cast<int>(nbits() - 1)};
			std::size_t idx = dist(gen_);
//			idx = nbits()/2 + idx/2;
			auto &&gene = chromosome_.at(idx);
			gene = !gene;
//		} else {
//			std::cout << "do_mutate: no mutation, probability=" << std::setprecision(8) << probability << " generated=" << std::setprecision(8) << generated << '\n';
		}
	}

	std::pair<real_type, real_type> domainValues_;
	int precision_;
	std::size_t nbits_;
	std::vector<Gene> chromosome_;

	inline static std::random_device rd_;
	inline static std::default_random_engine gen_{rd_()};
	inline static std::uniform_int_distribution<> uniform_int_dist_0_1_{0, 1};
	inline static std::uniform_real_distribution<> uniform_dist_0_1_{static_cast<real_type>(0), static_cast<real_type>(1)};
};
using Genomes = std::vector<Genome>;

int main(int argc, char* argv[])
try {
	real_type low = -1.0;
	real_type high = 2.0;
	int precision = 6;
	std::size_t populationSize = 50;
	std::size_t maxIterations = 26;
	real_type mutateProbability = 0.7;

	for (int i = 1; i < argc; ++i) {
		enum class TokenTag {
			low, high,
			precision,
			populationSize,
			mutateProbability,
			iterations
		};
		static const std::array<std::string_view, 6> tokens {
			"low=", "high=",
			"precision=",
			"populationSize=",
			"mutateProbability=",
			"iterations="
		};
		auto tokenMatch = [](std::string_view val, std::string_view token) -> bool {
			std::size_t i;
			for (i = 0; i != val.size(); ++i) {
				if (val[i] == '=') {
					++i;
					break;
				}
			}
			if (i == val.size()) {
				return false; // no = and value in val
			}

			return (i == token.size()) &&
			       (std::memcmp(val.data(), token.data(), i) == 0);
		};

		std::string_view arg{argv[i], std::strlen(argv[i])};
		bool match = false;
		for (std::size_t j = 0; !match && j != tokens.size(); ++j) {
			const auto token = tokens[j];

			if (tokenMatch(arg, token)) {
				switch (static_cast<TokenTag>(j)) {
				case TokenTag::low:
					low = std::atof(arg.data() + token.size());
					break;
				case TokenTag::high:
					high = std::atof(arg.data() + token.size());
					break;
				case TokenTag::precision:
					precision = std::atoi(arg.data() + token.size());
					break;
				case TokenTag::populationSize:
					populationSize = std::atoi(arg.data() + token.size());
					break;
				case TokenTag::mutateProbability:
					mutateProbability = std::atof(arg.data() + token.size());
					break;
				case TokenTag::iterations:
					maxIterations = std::atoi(arg.data() + token.size());
					break;
				}
				match = true; 
			}
		}
		if (!match) {
			throw std::runtime_error("bad arg: " + std::string{arg});
		}
	}
	if (low >= high) {
		throw std::runtime_error("domain low >= high");
	}
	std::pair<real_type, real_type> domain{low, high};
	precision = static_cast<int>(std::ceil( std::pow(10.0,  static_cast<real_type>(precision)) ));

	// create inital randomized population
	// sort by fitness
	// while (best no fit enough)
	//   for top 30%
	//     select fittest 2 from two parents and two children create thru crossover
	//   randomize the ramaining population
	//   sort by fitness
	auto fittest = [](const Genome& a, const Genome& b) -> bool {
		return a.fitness() > b.fitness();
	};

	Genomes genomes{populationSize, {domain, precision}};
	std::cout << "nbits=" << genomes.front().nbits() << '\n';
	randomize(genomes);
	sort(genomes, fittest);
	for (std::size_t iteration = 0; iteration != maxIterations; ++iteration) {
		std::cout << std::setw(2) << iteration << ": " << std::setprecision(18) << genomes[0].fitness() << '\n';

		std::size_t i = 0;
		for (i = 0; (i + 1) < 30*genomes.size()/100; i += 2) {
			auto children = crossover<Genome>({genomes[i], genomes[i + i]}, mutateProbability);
			Genomes tmp{
				std::move(genomes[i]),
				std::move(genomes[i + 1]),
				std::move(children.first),
				std::move(children.second)};
			sort(tmp, fittest);
			genomes[i] = std::move(tmp[0]);
			genomes[i + 1] = std::move(tmp[1]);
		}
		for (; i < genomes.size(); ++i) {
			genomes[i].randomize();
		}
		sort(genomes, fittest);
	}
}
catch (const std::exception& e) {
	std::cerr << "fatal: " << e.what() << '\n';
	return 1;
}
