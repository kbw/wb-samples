#!/bin/sh

# ./prog low=-1 high=2 precision=6 populationSize=8 iterations=1 mutateProbability=0.9
cgdb --args ./prog low=-1 high=2 precision=6 populationSize=8 iterations=1 mutateProbability=0.9
# ./prog low=-1 high=2 precision=8 populationSize=8 iterations=18 mutateProbability=0.9
