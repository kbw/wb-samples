#pragma once
#include "Individual.hpp"
#include <string>

namespace FitnessCalc {
	/* Public methods */
	// Set a candidate solution as a byte array
	void setSolution(const std::vector<Individual::byte>& newSolution);

	// To make it easier we can use this method to set our candidate solution
	// with string of 0s and 1s
	void setSolution(const std::string& newSolution);

	// Calculate inidividuals fittness by comparing it to our candidate solution
	int getFitness(const Individual& individual);
	
	// Get optimum fitness
	int getMaxFitness();
}
