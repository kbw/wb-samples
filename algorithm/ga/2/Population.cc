#include "Population.hpp"

/*
 * Constructors
 */
// Create a population
Population::Population(size_t populationSize, bool initialise) :
	individuals(populationSize, initialise)
{
}

/* Getters */
Individual& Population::getIndividual(size_t index) {
	return individuals.at(index);
}

const Individual& Population::getIndividual(size_t index) const {
	return individuals.at(index);
}

const Individual& Population::getFittest() const {
	const Individual* fittest = &individuals.front();

	// Loop through individuals to find fittest
	for (size_t i = 0; i != size(); ++i) {
		if (fittest->getFitness() <= getIndividual(i).getFitness()) {
			fittest = &getIndividual(i);
		}
	}
	return *fittest;
}

// Get population size
size_t Population::size() const {
	return individuals.size();
}

// Save individual
void Population::saveIndividual(size_t index, const Individual& indiv) {
	individuals[index] = indiv;
}
