#include "FitnessCalc.hpp"

namespace {
	std::vector<Individual::byte> solution(64);
}

namespace FitnessCalc {
	// Set a candidate solution as a byte array
	void setSolution(std::vector<Individual::byte>& newSolution) {
		solution = newSolution;
	}

	// To make it easier we can use this method to set our candidate solution
	// with string of 0s and 1s
	void setSolution(const std::string& newSolution) {
		solution.resize(newSolution.size());

		// Loop through each character of our string and save it in our byte
		// array
		for (size_t i = 0; i != newSolution.size(); ++i) {
//			String character = newSolution.substring(i, i + 1);
//			if (character.contains("0") || character.contains("1")) {
//				solution[i] = Byte.parseByte(character);
//			} else {
//				solution[i] = 0;
//			}
			solution[i] = newSolution[i] == '1' ? 1 : 0;
		}
	}

	// Calculate inidividuals fittness by comparing it to our candidate solution
	int getFitness(const Individual& individual) {
		int fitness = 0;

		// Loop through our individuals genes and compare them to our cadidates
		for (size_t i = 0; i != individual.size() && i != solution.size(); ++i)
			if (individual.getGene(i) == solution[i])
				++fitness;

		return fitness;
	}
	
	// Get optimum fitness
	int getMaxFitness() {
		return (int)solution.size();
	}
}
