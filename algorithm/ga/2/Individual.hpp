#pragma once
#include <vector>
#include <string>

class Individual {
public:
	typedef unsigned char byte;
	enum { defaultGeneLength = 64 };

	// Create a random individual
	Individual(bool randomize = false);

	/* Getters and setters */
	// Use this if you want to create individuals with different gene lengths
//	public static void setDefaultGeneLength(int length) {
//		defaultGeneLength = length;
//	}
//	
	byte getGene(size_t index) const;
	void setGene(size_t index, byte value);

	size_t size() const;
	int getFitness() const;
	std::string to_string() const;

private:
	std::vector<byte> genes;
	mutable int fitness = 0;
};
