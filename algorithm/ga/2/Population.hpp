#pragma once
#include "Individual.hpp"

class Population {
	std::vector<Individual> individuals;

public:
	/*
	 * Constructors
	 */
	// Create a population
	Population(size_t populationSize, bool initialise = false);

	/* Getters */
	Individual& getIndividual(size_t index);
	const Individual& getIndividual(size_t index) const;
	const Individual& getFittest() const;

	// Get population size
	size_t size() const;

	// Save individual
	void saveIndividual(size_t index, const Individual& indiv);
};
