#include "Individual.hpp"
#include "FitnessCalc.hpp"
#include "Random.hpp"

Individual::Individual(bool randomize) : genes(defaultGeneLength, 0) {
	if (randomize) {
		for (size_t i = 0; i != size(); i++) {
//			byte gene = (byte) Math.round(Math.random());
//			genes[i] = (double)rand()/std::numeric_limits<byte>::max();
			genes[i] = Random::random() % 2;
		}
	}
}

//	/* Getters and setters */
//	// Use this if you want to create individuals with different gene lengths
//	public static void setDefaultGeneLength(int length) {
//		defaultGeneLength = length;
//	}
	
Individual::byte Individual::getGene(size_t index) const {
	return genes.at(index);
}

void Individual::setGene(size_t index, byte value) {
	genes.at(index) = value;
	fitness = 0;
}

size_t Individual::size() const {
	return genes.size();
}

int Individual::getFitness() const {
	if (fitness == 0) {
		fitness = FitnessCalc::getFitness(*this);
	}
	return fitness;
}

std::string Individual::to_string() const {
	std::string geneString;
	for (size_t i = 0; i != size(); ++i) {
		geneString += '0' + getGene(i);
	}
	return geneString;
}
