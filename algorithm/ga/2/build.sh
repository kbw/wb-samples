#!/bin/sh

test -d simpleGa || mkdir simpleGa
javac GA.java FitnessCalc.java Individual.java Population.java Algorithm.java

cp *.class simpleGa && rm *.class
