#pragma once
#include "Population.hpp"

namespace Algorithm {
    // Evolve a population
	Population evolvePopulation(const Population& pop);
}
