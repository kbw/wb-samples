#include "Algorithm.hpp"
#include "Random.hpp"
#include <limits>

namespace {
	/* GA parameters */
	double uniformRate = 0.5;
	double mutationRate = 0.015;
	size_t tournamentSize = 5;
	bool elitism = true;

	// Crossover individuals
	Individual crossover(const Individual& indiv1, const Individual& indiv2) {
		size_t sz = std::min(indiv1.size(), indiv2.size());
		Individual newSol(sz);

		// Loop through genes
		for (size_t i = 0; i != sz; ++i) {
			// Crossover
			if (Random::random_d() <= uniformRate) {
				newSol.setGene(i, indiv1.getGene(i));
			} else {
				newSol.setGene(i, indiv2.getGene(i));
			}
		}

		return newSol;
	}

	// Mutate an individual
	void mutate(Individual& indiv) {
		// Loop through genes
		for (size_t i = 0; i != indiv.size(); i++) {
			if (Random::random_d() <= mutationRate) {
				// Create random gene
				indiv.setGene(i, Random::random()%2);
			}
		}
	}

	// Select individuals for crossover
	Individual tournamentSelection(const Population& pop) {
		// Create a tournament population
		Population tournament(tournamentSize);

		// For each place in the tournament get a random individual
		for (size_t i = 0; i != tournament.size(); ++i) {
			int randomId = Random::random() % pop.size();
			tournament.saveIndividual(i, pop.getIndividual(randomId));
		}

		// Get the fittest
		return tournament.getFittest();
	}
}

namespace Algorithm {
	// Evolve a population
	Population evolvePopulation(const Population& pop) {
		Population newPopulation(pop.size());

		// Keep our best individual
		if (elitism) {
			newPopulation.saveIndividual(0, pop.getFittest());
		}

		// Crossover population
		size_t elitismOffset = elitism ? 1 : 0;

		// Loop over the population size and create new individuals with
		// crossover
		for (size_t i = elitismOffset; i != pop.size(); ++i) {
			Individual indiv1 = tournamentSelection(pop);
			Individual indiv2 = tournamentSelection(pop);
			Individual newIndiv = crossover(indiv1, indiv2);
			newPopulation.saveIndividual(i, newIndiv);
		}

		// Mutate population
		for (size_t i = elitismOffset; i != newPopulation.size(); ++i) {
			mutate(newPopulation.getIndividual(i));
		}

		return newPopulation;
	}
}
