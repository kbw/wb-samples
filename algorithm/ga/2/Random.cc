#include "Random.hpp"
#include <random>
#include <memory>

namespace {
	std::random_device _r;
	std::default_random_engine _e1(_r());
	std::unique_ptr<std::uniform_int_distribution<int>> _uniform_dist;

	int _low, _hi;
}

namespace Random {
	void init(int low, int hi) {
		_uniform_dist.reset(new std::uniform_int_distribution<int>(_low = low, _hi = hi));
	}

	double random_d() {
		return (double)random() / (_hi - _low);
	}

	int random() {
		if (!_uniform_dist)
			throw "uninitialized Random::random()";

		std::uniform_int_distribution<int>& uniform_dist = *_uniform_dist.get();
		return uniform_dist(_e1);
	}
}
