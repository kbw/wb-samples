#include "Population.hpp"
#include "FitnessCalc.hpp"
#include "Algorithm.hpp"
#include "Random.hpp"
#include <iostream>
#include <stdlib.h>

int main() {
	// initialize PRNG
	Random::init();

	// Set a candidate solution
	FitnessCalc::setSolution("1111000000000000000000000000000000000000000000000000000000001111");

	// Create an initial population
	Population myPop(50, true);

	// Evolve our population until we reach an optimum solution
	int generationCount = 0;
	while (generationCount < 1 && myPop.getFittest().getFitness() < FitnessCalc::getMaxFitness()) {
		++generationCount;
		std::cout << "Generation: " << generationCount << " Fittest: " << myPop.getFittest().getFitness() << std::endl;
		myPop = Algorithm::evolvePopulation(myPop);
	}
	std::cout << "Solution found!" << std::endl;
	std::cout << "Generation: " << generationCount << std::endl;
	std::cout << "Genes:" << myPop.getFittest().to_string() << std::endl;
}
