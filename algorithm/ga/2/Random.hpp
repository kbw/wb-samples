#pragma once
#include <limits>

namespace Random {
	void init(int low = 0, int hi = std::numeric_limits<int>::max());
	double random_d();
	int random();
}
