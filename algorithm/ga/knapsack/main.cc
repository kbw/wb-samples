#include <string>
#include <vector>

#include <stdlib.h>

struct DataKnapSack {
	static constexpr size_t fields{6};

	int value;
	int weight;
	int length, width, height;
	int brittleness;

	int fitness() const {
		constexpr int value_c{10};
		constexpr int weight_c{10};
		constexpr int volume_c{10};
		constexpr int brittleness_c{10};

		int volume = length * width * height;
		int ret =
			value * value_c +
			weight * weight_c +
			volume * volume_c +
			brittleness * brittleness_c;
		return ret;
	}
};

struct DataStr {
	inline static const std::string target{"to be or not to be"};
	std::string value;

	static DataStr createRandom() {
		// valid chars: lower case + space
		DataStr datum;
		for (size_t i = 0; i != datum.value.size(); ++i) {
			auto range = 1 + 'z' - 'a' + 1;
			auto r = rand() % range;
			datum.value[i] = (r != (range - 2)) ? char('a' + i) : ' ';
		}
		return datum;
	}

	DataStr() : value(target.size(), ' ') {
	}

	int fitness() const {
		int score{};
		for (size_t i = 0; i != value.size(); ++i)
			value[i] == target[i] && ++score;
		return score;
	}

	void mutate(double mutation_rate) {
		// choose a random character
		auto i = rand() % value.size();

		if (double rand_as_float = 1.0 * rand() / RAND_MAX; rand_as_float < mutation_rate) {
			// choose a mutation
			auto range = 1 + 'z' - 'a' + 1;
			auto r = rand() % range;
		}
	}
};

template <typename T>
class GA {
	const size_t step_size_;
	double crossover_rate_;
	double mutation_rate_;
	std::vector<T> population_;

public:
	GA(size_t population_size = 512,
	  size_t step_size = 16,
	  double crossover_rate = 0.5,
	  double mutation_rate = 0.05) :
		step_size_(step_size),
		crossover_rate_(crossover_rate),
		mutation_rate_(mutation_rate),
		population_(RandPopulation(population_size)) {
	}

	std::vector<T> RandPopulation(size_t population_size) const {
		std::vector<T> ret;
		ret.reserve(population_size);
		for (size_t i = 0; i != population_size; ++i)
			ret.push_back(T::createRandom());
		return ret;
	}

	T selectTournament(size_t step_size, const std::vector<T>& population) const {
		std::vector<size_t> selected;
		selected.reserve(step_size);
		for (size_t i = 0; i != step_size; ++i)
			selected.push_back(rand() % population.size());

		size_t idx{};
		int max_fitness{};
		for (size_t i = 0; i != step_size; ++i) {
			int fitness = population[selected[i]].fitness();
			if (fitness > max_fitness) {
				max_fitness = fitness;
				idx = i;
			}
		}
		return population[selected[idx]];
	}

	std::vector<T> selectTournament(size_t step_size) const {
		std::vector<T> tmp;
		tmp.reserve(population_.size());
		for (size_t i = 0; i != population_.size(); ++i)
			tmp.push_back(selectTournament(step_size, population_));
		return tmp;
	}

	std::vector<T> crossover(double crossover_rate) {
		std::vector<T> tmp;
		tmp.reserve(population_.size());
		return tmp;
	}

	T mutate(double mutation_rate, const T& datum) {
		return datum.mutate();
	}

	std::vector<T> mutation(double mutation_rate) {
		std::vector<T> tmp;
		tmp.reserve(population_.size());
		return tmp;
	}

	void CreatePopulation() {
		std::vector<T> ret{selectTournament(step_size_)};
		population_.swap(ret);
	}
};

int main() {
	srand(0);
	GA<DataStr> ga;
	ga.CreatePopulation();
}
