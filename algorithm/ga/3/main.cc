#include <algorithm>
#include <functional>
#include <cmath>
#include <iostream>
#include <ostream>
#include <random>
#include <utility>
#include <vector>

#include <assert.h>

template <typename T, typename U>
std::ostream& operator<<(std::ostream& os, const std::pair<T, U>& v) {
	os << '{' << v.first << ", " << v.second << '}';
	return os;
}

template <typename T>
T log(unsigned base, T x) {
	// log2 (x) = logy (x) / logy (2)
	return static_cast<T>(std::log(x)) / static_cast<T>(std::log(base));
}

template <typename T = float>
struct Genome {
	using numeric_precision_type = T;
	using Gene = bool;
	using Chromosome = std::vector<Gene>; 

	Chromosome chromosome;

	Genome(std::pair<int, int> domain, int precision)
		:	rawDomain_(domain), precision_(precision),
			nbits_(1 + static_cast<std::size_t>(log<T>(2, range()))) {
		chromosome.resize(nbits());
		randomize();
	}

	std::size_t nbits() const {
		return nbits_;
	}

	T eval() const {
		static T pNbits = static_cast<T>(std::pow<T>(2, nbits())); // power of nbits
		return rawDomain_.first + (toBase10() * domain() / (pNbits - 1));
	}

	void randomize() {
		static std::uniform_int_distribution<> uniform_dist{0, 1};
		for (auto&& gene : chromosome)
			if (uniform_dist(gen))
				gene = !gene;
	}

	void mutate(double mutationRate, std::size_t mutations = 1) {
		static std::uniform_real_distribution<> uniform_dist{0.0, 1.0};
		if (uniform_dist(gen) <= mutationRate)
			mutate(mutations);
	}

	std::ostream& info(std::ostream& os) const {
		os	<< "nbits=" << nbits() << ' '
			<< "domain=" << rawDomain_ << ' '
			<< "eval=" << eval() << '\n';
		return os;
	}

private:
	int range() const { return domain() * precision_; }
	int domain() const { return rawDomain_.second - rawDomain_.first; }

	unsigned toBase10() const {
		unsigned sum = 0;
		unsigned p2 = 1; // power of 2
		for (auto gene : chromosome) {
			sum += gene ? p2 : 0;
			p2 *= 2;
		}
		return sum;
	}

	void mutate(std::size_t mutations = 1) {
		static std::uniform_int_distribution<> uniform_dist{0, static_cast<int>(nbits())};
		for (std::size_t i = 0; i != mutations; ++i) {
			int index = uniform_dist(gen);
			chromosome[index] = !chromosome[index];
		}
	}

	std::pair<int, int> rawDomain_; // domain low/high
	int precision_;
	std::size_t nbits_;

	inline static std::random_device rd;
	inline static std::default_random_engine gen{rd()};
};  // Genome

template <typename T>
Genome<T> mutate(const Genome<T>& a, double mutationRate) {
	Genome<T> out = a;
	out.mutate(mutationRate);
	return out;
}

template <typename T>
std::pair<Genome<T>, Genome<T>> crossover(std::pair<Genome<T>, Genome<T>> parents, std::size_t position) {
	if (parents.first.nbits() != parents.second.nbits())
		throw std::runtime_error("mismatched parent sizes in crossover()");
	for (std::size_t i = position; i < parents.first.nbits(); ++i)
		std::swap(parents.first.chromosome[i], parents.second.chromosome[i]);
	return parents;
}

template <typename T>
std::pair<Genome<T>, Genome<T>> fitness(const std::pair<Genome<T>, Genome<T>> parents,
                                        const std::pair<Genome<T>, Genome<T>> children) {
	// we want the max 2 genomes
	std::array<Genome<T>, 4> genomes{parents.first, parents.second, children.first, children.second}; // copy?
	std::sort(genomes.begin(), genomes.end(), [](const Genome<T>& a, const Genome<T>& b) { return a.eval() > b.eval(); });
	return {genomes[0], genomes[1]};
}

int main(int argc, char* argv[]) {
	int domainLow = -1;
	int domainHigh = 2;
	int precision = std::pow(10, 6);
	std::size_t populationSize = 20;
	double mutationRate = 0.001;
	double crossoverRate = 0.1;
	std::size_t nIterations = 10;

	if (argc > 2) {
		domainLow = std::atoi(argv[1]);
		domainHigh = std::atoi(argv[2]);
		assert(domainLow < domainHigh);
	}
	if (argc > 3)
		precision = std::atoi(argv[3]);
	if (argc > 4) {
		int val = std::abs(std::atoi(argv[4]));
		val = std::min(2*(val/2), val);
		populationSize = static_cast<std::size_t>(val);
	}
	if (argc > 5)
		mutationRate = std::atof(argv[5]);
	if (argc > 6)
		crossoverRate = std::max(0.0, std::min(std::atof(argv[6]), 1.0));
	if (argc > 7)
		nIterations = std::max(static_cast<std::size_t>(0), static_cast<std::size_t>(std::atol(argv[7])));
	std::cout	<< "using: domainLow=" << domainLow << " domainHight=" << domainHigh
				<< " precision=" << precision
				<< " populationSize=" << populationSize
				<< " mutationRate=" << mutationRate
				<< " crossoverRate=" << crossoverRate
				<< " iterations=" << nIterations << '\n';
#ifndef NDEBUG
	Genome g{{domainLow, domainHigh}, precision};
	for (auto&& bit : g.chromosome) bit = false;
	g.info(std::cout << "clr all: ");
	for (auto&& bit : g.chromosome) bit = true;
	g.info(std::cout << "set all: ");
#endif

	// Find the MAX value for each generation
	std::vector<Genome<float>> genes{populationSize, {{domainLow, domainHigh}, precision}};
	std::function<bool(const Genome<float>&, const Genome<float>&)> fitFunc =
		[](const Genome<float>& a, const Genome<float>& b) { return a.eval() > b.eval(); };
	std::sort(genes.begin(), genes.end(), fitFunc);

	const std::size_t crossoverPostion = static_cast<std::size_t>(crossoverRate * genes[0].nbits());

	for (std::size_t iterations = 0; iterations < nIterations; ++iterations) {
		// pick 20%, replace bottom %80
		std::size_t i = 0;
		for (; i < genes.size()/5; i += 2) {
			auto children = crossover<decltype(genes)::value_type::numeric_precision_type>({mutate(genes[i], mutationRate), mutate(genes[i + 1], mutationRate)}, crossoverPostion);
			auto fitest = fitness<decltype(genes)::value_type::numeric_precision_type>({genes[i], genes[i + 1]}, children);

			genes[i] = fitest.first;
			genes[i + 1] = fitest.second;
		}
		for (; i != genes.size(); ++i)
			genes[i].randomize();

		std::sort(genes.begin(), genes.end(), [](const Genome<float>& a, const Genome<float>& b) { return a.eval() > b.eval(); });

		// show top 10
		std::cout << "n=" << genes.size() << '\n';
		for (i = 0; i < std::min(static_cast<std::size_t>(10), genes.size()); ++i)
			genes[i].info(std::cout << i << ": ");
	}
}
