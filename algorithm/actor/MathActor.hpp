#pragma once
#include "Math.hpp"

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <optional>
#include <queue>
#include <thread>
#include <tuple>
#include <variant>

using IntOutType    = std::function<int(void)>;
using DoubleOutType = std::function<double(void)>;
using MathType   = std::variant<IntOutType, DoubleOutType>;
using ReturnType = std::variant<int, double>;

struct Wrapper {
	ReturnType operator()(IntOutType &func) {
		return func();
	}
	ReturnType operator()(DoubleOutType &func) {
		return func();
	}
};

// not sure how to specify Actor Messages as template params
// so no template yet
struct MathActor {
	MathActor(Math &m) : m_(m) {
	}
	~MathActor() {
		stopping_ = true;
		if (worker_.joinable())
			worker_.join();
	}
	MathActor(const MathActor &) = delete;
	MathActor &operator=(const MathActor &) = delete;

	int factorial(int term) {
		std::optional<ReturnType> ret;
		std::unique_lock<std::mutex> lock{mtx_};
		IntOutType func = std::bind(&Math::factorial, m_, term);
		queue_.push({std::ref(ret), func});
		++queue_len_;
		cv_.wait(lock, [&ret]{ return ret.has_value(); }); // spin lock
		return std::get<int>(*ret);
	}
	double sin(double theta) {
		std::optional<ReturnType> ret;
		std::unique_lock<std::mutex> lock{mtx_};
		DoubleOutType func = std::bind(&Math::sin, m_, theta);
		queue_.push({std::ref(ret), func});
		++queue_len_;
		cv_.wait(lock, [&ret]{ return ret.has_value(); }); // spin lock
		return std::get<double>(*ret);
	}
	int fibonacci(int term) {
		std::optional<ReturnType> ret;
		std::unique_lock<std::mutex> lock{mtx_};
		IntOutType func = std::bind(&Math::fibonacci, m_, term);
		queue_.push({std::ref(ret), func});
		++queue_len_;
		cv_.wait(lock, [&ret]{ return ret.has_value(); }); // spin lock
		return std::get<int>(*ret);
	}

	std::mutex mtx_;
	std::condition_variable cv_;
	std::atomic<std::size_t> queue_len_{};
	std::atomic<bool> stopping_{};

	Math &m_;
	std::queue<std::tuple<std::optional<ReturnType> &, MathType>> queue_; // {ret&, func}
	std::thread worker_{[this]() {
			while (!stopping_) {
				while (queue_len_) {
					std::lock_guard<std::mutex> lock{mtx_};
					auto&& arg = queue_.front();
					std::get<0>(arg) = std::visit(Wrapper{}, std::get<1>(arg));
					queue_.pop();
					--queue_len_;
					cv_.notify_all();
				}

				using namespace std::chrono_literals;
				std::this_thread::sleep_for(100ns);
			}
		}};
};
