#include "Math.hpp"
#include "MathActor.hpp"
#include "Timer.hpp"

#include <spdlog/spdlog.h>

#include <functional>
#include <queue>

int main() {
	Math m;
	std::function<int(int)> factorial = std::bind(&Math::factorial, m, std::placeholders::_1);
	std::function<int(int)> fibonacci = std::bind(&Math::fibonacci, m, std::placeholders::_1);
	std::function<double(double)> sin = std::bind(&Math::sin, m, std::placeholders::_1);

	constexpr int term{7};
//	constexpr auto pi = 3.141592653589793238462643383279502884e+00;
	constexpr auto pi = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651e+00;
//	constexpr auto third_pi = 1.047197551196597746154214461093167628e+00;
	constexpr auto third_pi = 1.04719755119659774615421446109316762806572313312503527365831486410260546876206966620934494178070568932738269550e+00;
	constexpr double theta{third_pi / 2};

	MathActor actor{m};
	factorial(term);
	sin(theta);
	fibonacci(term);

	Timer timer;
	timer.start();
	int fact_val  = factorial(term);
	double sin_val= sin(theta);
	int fib_val   = fibonacci(term);
	timer.stop();
	auto directCallTime = timer.deltaNS();
	spdlog::info("factorial({})={} sin({})={} fibonacci({})={}", term, fact_val, "π/6", sin_val, term, fib_val);

	timer.start();
	fact_val = actor.factorial(term);
	sin_val  = actor.sin(theta);
	fib_val  = actor.fibonacci(term);
	timer.stop();
	auto actorCallTime = timer.deltaUS();
	spdlog::info("factorial({})={} sin({})={} fibonacci({})={}", term, fact_val, "π/6", sin_val, term, fib_val);

	spdlog::info("direct call: {}ns", directCallTime);
	spdlog::info("actor  call: {}us", actorCallTime);
}
