#pragma once
#include <chrono>

struct Timer {
	std::chrono::time_point<std::chrono::high_resolution_clock> start_, stop_;
	void start() {
		start_ = std::chrono::high_resolution_clock::now();
	}
	void stop() {
		stop_ = std::chrono::high_resolution_clock::now();
	}
	double deltaMS() const {
		return std::chrono::duration_cast<std::chrono::milliseconds>(stop_ - start_).count();
	}
	double deltaUS() const {
		return std::chrono::duration_cast<std::chrono::microseconds>(stop_ - start_).count();
	}
	double deltaNS() const {
		return std::chrono::duration_cast<std::chrono::nanoseconds>(stop_ - start_).count();
	}
};
