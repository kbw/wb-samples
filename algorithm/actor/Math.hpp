#pragma once
#include <cmath>
#include <functional>
#include <optional>
#include <vector>

struct Math {
	int factorial(int term) {
		switch (term) {
		case 0:	return 0;
		default:
			while (term >= factorials_.size())
				factorials_.resize(2*factorials_.size());
			if (factorials_[term])
				return *factorials_[term];
			factorials_[term] = term * factorial(term - 1);
			return *factorials_[term];
		}
	}
	int triangular(int term) {
		switch (term) {
		case 0:	return 0;
		default:
			while (term >= triangulars_.size())
				triangulars_.resize(2*triangulars_.size());
			if (triangulars_[term])
				return *triangulars_[term];
			triangulars_[term] = term + triangular(term - 1);
			return *triangulars_[term];
		}
	}
	int fibonacci(int term) {
		switch (term) {
		case 0:	return 0;
		case 1:	return 1;
		default:
			while (term >= fibonaccis_.size())
				fibonaccis_.resize(2*fibonaccis_.size());
			if (fibonaccis_[term])
				return *fibonaccis_[term];
			fibonaccis_[term] = fibonacci(term - 2) + fibonacci(term - 1);
			return *fibonaccis_[term];
		}
	}
	double sin(double theta) {
		return ::sin(theta);
	}

	inline static std::vector<std::optional<int>> factorials_{8};
	inline static std::vector<std::optional<int>> triangulars_{8};
	inline static std::vector<std::optional<int>> fibonaccis_{8};
};
