#include <algorithm>
#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

template <typename T>
struct opt {
	bool valid_{};
	T value_{};

	opt() = default;
	~opt() = default;
	opt(const opt<T> &) = default;
	opt& operator=(const opt<T> &) = default;
	opt(opt<T> &&) = default;
	opt& operator=(opt<T> &&) = default;

	opt(const T &n) : valid_(true), value_(n) {}
	opt(T &&n) : valid_(true), value_(std::move(n)) {}
	opt& operator=(const T &n) { valid_ = true; value_ = n; return *this; }
	opt& operator=(T &&n) { valid_ = true; value_ = std::move(n); return *this; }

	bool has_value() const { return valid_; }
	const T& value() const { return value_; }
	void reset() { valid_ = false; } // keep value_
};

struct Entry {
	double price{};
	size_t size{};

	Entry() = default;
	Entry(double price, size_t size) : price(price), size(size) {}

	bool operator!=(const Entry &n) const {
		return price != n.price || size != n.size;
	}
	bool operator==(const Entry &n) const {
		return price == n.price && size == n.size;
	}
};

struct EntryPriceGreater : std::binary_function<const opt<Entry>&, const opt<Entry>&, bool> {
	bool operator()(const opt<Entry>& a, const opt<Entry>& b) const {
		if (a.has_value() && b.has_value())
			return a.value().price > b.value().price;
		return a.has_value() && !b.has_value();
	}
};

struct EntryPriceLess : std::binary_function<const opt<Entry>&, const opt<Entry>&, bool> {
	bool operator()(const opt<Entry>& b, const opt<Entry>& a) const {
		if (a.has_value() && b.has_value())
			return a.value().price > b.value().price;
		return !a.has_value() && b.has_value();
	}
};

std::string to_string(const std::vector<opt<Entry>>& n) {
	std::ostringstream os;

	os << "{ ";
	for (size_t i = 0; i != n.size(); ++i) {
		if (i > 0)
			os << ", ";
		if (n[i].has_value())
			os << "{" << n[i].value().price << "," << n[i].value().size << "}";
		else
			os << "{}";
	}
	os << " }";

	return os.str();
}

const std::vector<opt<Entry>> values{ opt<Entry>(), opt<Entry>(), Entry(1,1), Entry(2,1) };

int main() {
	auto gt{ values };
	auto lt{ values };

	std::sort(gt.begin(), gt.end(), EntryPriceGreater());
	std::sort(lt.begin(), lt.end(), EntryPriceLess());

	std::cout << "gt: " << to_string(gt) << "\n";
	std::cout << "lt: " << to_string(lt) << "\n";
}
