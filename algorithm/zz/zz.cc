#include <spdlog/spdlog.h>
#include <spdlog/fmt/bundled/format.h>

#include <algorithm>
#include <ostream>
#include <sstream>
#include <string>

#include <set>
#include <vector>

namespace zz {
template <typename T>
inline std::string to_string(const T& val) {
	std::ostringstream oss;
	oss << val;
	return oss.str();
}

struct CompoundName {
	CompoundName(std::string root, std::string aux) : root(std::move(root)), aux(std::move(aux)) {}
	std::string root, aux;
};
inline bool operator==(const CompoundName &a, const CompoundName &b) {
	return a.root == b.root && a.aux == b.aux;
}
inline bool operator<(const CompoundName &a, const CompoundName &b) {
	if (a.root < b.root) {
		return true;
	}
	if (a.root == b.root) {
		return a.aux < b.aux;
	}
	return false;
}
inline std::ostream& operator<<(std::ostream& os, const CompoundName& name) {
	os << "{" << name.root << "/" << name.aux << "}";
	return os;
}

// Rank is an ordered set of cells
struct Cell;
struct Rank {
	using cells_t = std::vector<std::shared_ptr<Cell>>;

	Rank(CompoundName name) : name(std::move(name)) {}
	const CompoundName name;
	mutable bool circular{};
	mutable cells_t cells;	// Ranks may intersect at Cells
};
inline bool operator==(const Rank &a, const Rank &b) {
	return a.name == b.name;
}
inline bool operator<(const Rank &a, const Rank &b) {
	return a.name < b.name;
}
inline std::ostream& operator<<(std::ostream& os, const Rank& rank) {
	os << std::boolalpha;
	os << "{" << rank.name << ", " << rank.circular << ", " << rank.cells.size() << "}";
	return os;
}

// Cell is (so far) comprised of the set of intersecting Ranks
struct Cell {
	struct property {
		property(const Rank* rank, std::string mimeType = "", std::string payload = "") :
			rank(rank), mimeType(std::move(mimeType)), payload(std::move(payload)) {
		}
		const Rank* rank;
		std::string mimeType;
		std::string payload;

		inline bool operator<(const property& val) const {
			return rank < val.rank;
		}
		inline bool operator==(const property& val) const {
			return rank == val.rank;
		}
	};
	using ranks_t = std::set<property>;
	ranks_t properties; // unordered set of intersecting Ranks
};

std::vector<Rank> ranks;

std::shared_ptr<Cell> findIterectingCell(const std::vector<const Rank*>& targetRanks) {
	if (targetRanks.empty()) {
		return {};
	}

	// search one dimension
	for (auto cell : targetRanks.front()->cells) {
		// look for matches in all other dimensions in a single cell
		std::vector<bool> flags(targetRanks.size());
		for (size_t i = 0; i != targetRanks.size(); ++i) {
			flags[i] = std::find(cell->properties.begin(), cell->properties.end(), targetRanks[i]) != cell->properties.end();
		}
		if (std::all_of(flags.begin(), flags.end(), [](bool flag) { return flag; })) {
			return cell;
		}
	}

	return {};
}

std::shared_ptr<Cell> addRankToCell(CompoundName src, CompoundName dst) {
	spdlog::info("addRankToCell({}, {})", to_string(src), to_string(dst));

	bool srcCreated{};
	auto srcIter = std::find(ranks.begin(), ranks.end(), Rank{src});
	if (srcIter == ranks.end()) {
		ranks.emplace_back(src);
		srcCreated = true;
		srcIter = std::find(ranks.begin(), ranks.end(), Rank{src});
	}

	auto dstIter = std::find(ranks.begin(), ranks.end(), Rank{dst});
	if (dstIter == ranks.end()) {
		ranks.emplace_back(dst);
		srcIter = std::find(ranks.begin(), ranks.end(), Rank{src});
		dstIter = std::find(ranks.begin(), ranks.end(), Rank{dst});
	}
	auto& srcRank = *srcIter;
	auto& dstRank = *dstIter;

	// we have a rank, but do we have a cell that intersects with src? let's check.
	if (!srcCreated && findIterectingCell({&srcRank, &dstRank})) {
		spdlog::info("2: srcRank={}", to_string(srcRank));
		return {};
	}

	// we have src and dst Ranks, but no common cell, lets create one
	auto cell = std::make_shared<Cell>();

	srcRank.cells.push_back(cell);
	cell->properties.emplace(&srcRank, "test/plain", std::move(src.aux));

	dstRank.cells.push_back(cell);
	cell->properties.emplace(&dstRank, "test/plain", std::move(dst.aux));

	return cell;
}
} //namespace zz

int main(int argc, char* argv[]) {
	std::shared_ptr<zz::Cell> cell;

	cell = zz::addRankToCell({"actor", "Steve McQueen"}, {"movie", "The Getaway"});
	if (cell) {
		for (const auto& property : cell->properties) {
			spdlog::info("{}", property.payload);
		}
	}

	cell = zz::addRankToCell({"actor", "Ali MacGraw"}, {"movie", "The Getaway"});
	if (cell) {
		for (const auto& property : cell->properties) {
			spdlog::info("{}", property.payload);
		}
	}
}
