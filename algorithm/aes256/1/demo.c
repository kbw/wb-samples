/*  
 *  Byte-oriented AES-256 implementation.
 *  All lookup tables replaced with 'on the fly' calculations. 
 *
 *  Copyright (c) 2007 Ilya O. Levin, http://www.literatecode.com
 *
 *  Permission to use, copy, modify, and distribute this software for any
 *  purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "aes256.h"
#include <stdio.h>

#define DUMP(s, buf, sz) \
{ size_t i; \
  printf(s); \
  for (i = 0; i < (sz); ++i) \
    printf("%02x ", buf[i]); \
  printf("\n"); \
}

void encrypt_ecb(uint8_t* key, size_t keysz, uint8_t* buf, size_t bufsz)
{
	aes256_context ctx;

	aes256_init(&ctx, key);
	aes256_encrypt_ecb(&ctx, buf);
	DUMP("enc_ecb: ", buf, bufsz);
	aes256_done(&ctx);
}

void decrypt_ecb(uint8_t* key, size_t keysz, uint8_t* buf, size_t bufsz)
{
	aes256_context ctx;

	aes256_init(&ctx, key);
	aes256_decrypt_ecb(&ctx, buf);
	DUMP("dec_ecb: ", buf, bufsz);
	aes256_done(&ctx);
}

void encrypt_cbc(uint8_t* key, size_t keysz, uint8_t* buf, size_t bufsz, uint8_t* iv, size_t ivsz)
{
	aes256_context ctx;

	aes256_init(&ctx, key);
	aes256_encrypt_cbc(&ctx, buf, iv);
	DUMP("enc_cbc: ", buf, bufsz);
	aes256_done(&ctx);
}

void decrypt_cbc(uint8_t* key, size_t keysz, uint8_t* buf, size_t bufsz, uint8_t* iv, size_t ivsz)
{
	aes256_context ctx;

	aes256_init(&ctx, key);
	aes256_decrypt_cbc(&ctx, buf, iv);
	DUMP("dec_cbc: ", buf, bufsz);
	aes256_done(&ctx);
}

int main (int argc, char *argv[])
{
    uint8_t key[32];
    uint8_t buf[16], iv[sizeof(buf)];
	size_t i;

    /* put a test vector */
    for (i = 0; i < sizeof(buf); ++i) buf[i] = i * 16 + i;
    for (i = 0; i < sizeof(key); ++i) key[i] = i, iv[i] = i << ((i + 1)%8);

    DUMP("txt: ", buf, sizeof(buf));
    DUMP("key: ", key, sizeof(key));
    printf("---\n");

	encrypt_ecb(key, sizeof(key), buf, sizeof(buf));
	decrypt_ecb(key, sizeof(key), buf, sizeof(buf));

	encrypt_cbc(key, sizeof(key), buf, sizeof(buf), iv, sizeof(iv));
	decrypt_cbc(key, sizeof(key), buf, sizeof(buf), iv, sizeof(iv));

    return 0;
} /* main */
