Parallel Experiments

1. core-affinity
	args: cpuids

	Run a simple integer sum in parallel.  There's no memory references in the calculation.

	The idea is to run each of these on a named core and time the whole run.

	There is some variance in the output, so multiple runs are required.
