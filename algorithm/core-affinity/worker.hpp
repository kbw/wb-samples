#pragma once

#include <chrono>

#ifndef likely
  #ifdef NDEBUG
    #define likely(x)   __builtin_expect(!!(x), 1)
    #define unlikely(x) __builtin_expect(!!(x), 0)
  #else
    #define likely(x)   (x)
    #define unlikely(x) (x)
  #endif
#endif

using number_t = int64_t;

struct Result {
	number_t value;
	std::chrono::high_resolution_clock::time_point start;
	std::chrono::high_resolution_clock::time_point stop;
};

typedef void Worker(Result& result, int cpuid, number_t start, number_t stop);

//----------------------------------------------------------------------------

void pin_thread_to_cpu(int cpuid);

void add_independent(Result& result, int cpuid, number_t start, number_t stop);
void add_relaxed(Result& result, int cpuid, number_t start, number_t stop);
void add_acquire_release(Result& result, int cpuid, number_t start, number_t stop);
