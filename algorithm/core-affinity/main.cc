#include "worker.hpp"

#include <iostream>
#include <thread>
#include <utility>
#include <vector>

#include <stdint.h>
#include <stdlib.h>

//----------------------------------------------------------------------------

namespace {
	struct App {
		const char* name;
		Worker* worker;
	};

	const std::vector<App> apps{
		{ "independent", add_independent },
		{ "relaxed", add_relaxed },
		{ "acquire_release", add_acquire_release }
	};

	size_t match(const char* name) {
		for (size_t i = 0; i != apps.size(); ++i)
			if (name[0] == apps[i].name[0])
				return i;
		return 0;
	}
}

int main(int argc, char* argv[]) {
	using namespace std::chrono;

	if (argc < 2)
		return 0;

	size_t app_idx = 0;
	size_t param_start = 1;
	if (*argv[1] != '0' && atoi(argv[1]) == 0) {
		app_idx = match(argv[1]);
		param_start = 2;
	}

	std::vector<Result> sums(static_cast<size_t>(argc) - param_start);
	std::vector<std::thread> threads(sums.size());
	auto start = high_resolution_clock::now();
	{
		for (size_t i = 0; i != threads.size(); ++i)
			threads[i] = std::thread(
							apps[app_idx].worker,
							std::ref(sums[i]),
							atoi(argv[param_start + i]),
							0, 1<<24);
		for (size_t i = 0; i != threads.size(); ++i)
			threads[i].join();
	}
	auto stop = high_resolution_clock::now();

	std::cout.imbue(std::locale(""));
	for (size_t i = 0; i != sums.size(); ++i)
		std::cout
			<< "runtime: " << duration_cast<microseconds>(sums[i].stop - sums[i].start).count() << "us" << '\t'
			<< "sum[" << i << "]=" << sums[i].value << '\n';
	std::cout << "runtime: " << duration_cast<microseconds>(stop - start).count() << "us" << std::endl;
}
