#include "worker.hpp"

extern "C" {
#if defined(__linux)
  #include <sched.h>
#elif defined(__FreeBSD__)
  #include <sys/types.h>
  #include <sys/_cpuset.h>
  #include <sys/cpuset.h>
  #include <sys/thr.h>
#endif
}

#include <iostream>

//----------------------------------------------------------------------------

void pin_thread_to_cpu(int cpuid) {
#if defined(__linux)
	cpu_set_t cpuset;
#elif defined(__FreeBSD__)
	cpuset_t cpuset;
#endif
	CPU_ZERO(&cpuset);
	CPU_SET(cpuid, &cpuset);

#if defined(__linux)
	int rc = sched_setaffinity(0, sizeof(cpuset), &cpuset);
#elif defined(__FreeBSD__)
	long tid;
	thr_self(&tid);
	int rc = cpuset_setaffinity(CPU_LEVEL_WHICH, CPU_WHICH_TID, tid, sizeof(cpuset), &cpuset);
#endif
	if (rc != 0) {
		std::cerr << "Error setting thread affinity: " << rc << std::endl;
	}
}
