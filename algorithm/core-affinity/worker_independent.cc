#include "worker.hpp"

void add_independent(Result& result, int cpuid, number_t start, number_t stop) {
	pin_thread_to_cpu(cpuid);
	result.start = std::chrono::high_resolution_clock::now();

	number_t sum{};
	for (; likely(start != stop); ++start)
		++sum;
	result.value = sum;

	result.stop = std::chrono::high_resolution_clock::now();
}
