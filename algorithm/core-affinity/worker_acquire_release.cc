#include "worker.hpp"

#include <atomic>

namespace {
	std::atomic<number_t> sum{};
	number_t g1{}, g2{}, g3{}, g4{};
}

void add_acquire_release(Result& result, int cpuid, number_t start, number_t stop) {
	pin_thread_to_cpu(cpuid);
	result.start = std::chrono::high_resolution_clock::now();

	for (; likely(start != stop); ++start) {
		++g1, ++g2, ++g3, ++g4;
		sum.fetch_add(1, std::memory_order_acq_rel);
	}
	result.value = sum.load(std::memory_order_acquire);

	result.stop = std::chrono::high_resolution_clock::now();
}
