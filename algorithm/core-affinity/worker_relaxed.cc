#include "worker.hpp"

#include <atomic>

namespace {
	std::atomic<number_t> sum{};
}

void add_relaxed(Result& result, int cpuid, number_t start, number_t stop) {
	pin_thread_to_cpu(cpuid);
	result.start = std::chrono::high_resolution_clock::now();

	for (; likely(start != stop); ++start)
		sum.fetch_add(1, std::memory_order_relaxed);
	result.value = sum.load(std::memory_order_relaxed);

	result.stop = std::chrono::high_resolution_clock::now();
}
