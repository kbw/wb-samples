PROG.mlp   = mlp
REPORT.mlp = mlp.report
SRCS.mlp   = mlp.cpp

PREFIX   ?= ${HOME}
CXXFLAGS += -g -std=c++20 -D_GLIBCXX_DEBUG -Wall -Wextra -Wno-unused-parameter -Wno-unused-function -Iinclude -I$(PREFIX)/include
LDFLAGS  += -L${PREFIX}/lib

INFO      ?= clang-tidy
INFOFLAGS ?= -checks='clang-analyzer-*,misc-*,modernize-*,-modernize-use-trailing-return-type,portability-*,performance-*,readability-*,-readability-identifier-length,-readability-braces-around-statements'

.PHONY: all report clean

all: $(PROG.mlp)

report: $(REPORT.mlp)

clean:
	- rm $(PROG.mlp) $(SRCS.mlp:.cpp=.o) $(SRCS.mlp:.cpp=.d)
	- rm $(REPORT.mlp) $(SRCS.mlp:.cpp=.info)

#-- specific rules ---
$(PROG.mlp): $(SRCS.mlp:.cpp=.o)
	$(LINK.cpp) -o $@ $^ $(LDFLAGS)

$(REPORT.mlp): $(SRCS.mlp:.cpp=.info)

#-- generic rules ---
%.d: %.cpp
	$(CXX) -MM $(CXXFLAGS) $< | sed 's,[^:]*:,$*.o $@ :,g' > $@

%.info: %.cpp
	$(INFO) $(INFOFLAGS) $< -- $(CXXFLAGS) > $@

#-- generated inculde file ---
include $(SRCS.mlp:.cpp=.d)
