#include <vector>
#include <cmath>
#include <cassert>
#include <iostream>
#include <tuple>
#include <random>
#include <functional>

template<typename Type>
class Matrix {
	std::size_t cols_{};
	std::size_t rows_{};
	std::vector<std::vector<Type>> data_;

	std::tuple<std::size_t, std::size_t> shape_{{}, {}};
	std::size_t elementCount_{};

public:
	Matrix() = default;
	Matrix(std::size_t rows, std::size_t cols) :
		cols_(cols), rows_(rows),
		data_(std::vector<std::vector<Type>>(rows, std::vector<Type>(cols))),
		shape_(std::tuple<std::size_t, std::size_t>(rows, cols)),
		elementCount_(rows * cols) {
	}

	std::size_t rows() const { return rows_; }
	std::size_t cols() const { return cols_; }
	std::tuple<std::size_t, std::size_t> shape() const { return shape_; }
	std::size_t elementCount() const { return elementCount_; }

#ifndef NDEBUG
	Type operator()(std::size_t row, std::size_t col) const {
		return data_.at(row).at(col);
	}

	Type& operator()(std::size_t row, std::size_t col) {
		return data_.at(row).at(col);
	}
#else
	Type operator()(std::size_t row, std::size_t col) const {
		return data_[row][col];
	}

	Type& operator()(std::size_t row, std::size_t col) {
		return data_[row][col];
	}
#endif

	void print(std::ostream& os) const;
//	Matrix<Type> matmul(Matrix<Type>& m);
	Matrix<Type> multiply_elementwise(const Matrix<Type>& m) const;
//	Matrix<Type> multiply_scalar(Type scalar);
	Matrix<Type> square() const;

//	Matrix<Type> add(Matrix<Type>& m);
//	Matrix<Type> sub(Matrix& target);
	Matrix<Type> T() const;

	Matrix<Type> apply_function(Type(*func)(Type));

	Matrix<Type>& operator+=(const Matrix<Type>& target) {
		*this = *this + target;
		return *this;
	}

	Matrix operator-() {
		Matrix output(rows(), cols());
		for (std::size_t r = 0; r < rows(); ++r) {
			for (std::size_t c = 0; c < cols(); ++c) {
				output(r, c) = -(*this)(r, c);
			}
		}
		return output;
	}

	Matrix<Type> operator-(const Matrix<Type>& target) const {
		return target * static_cast<Type>(-1);
	}
};

// methods
template<typename Type>
void Matrix<Type>::print(std::ostream& os) const {
	for (std::size_t i = 0; i < rows(); i++) {
		for (std::size_t j = 0; j < cols(); j++) {
			os << data_[i][j] << " ";
		}
		os << '\n';
	}
}

template <typename Type>
Matrix<Type> operator*(const Matrix<Type>& a, const Matrix<Type>& b) {
	assert(a.cols() == b.rows());

	Matrix<Type> output{a.rows(), b.cols()};
	for (std::size_t x = 0; x != output.rows(); ++x) {
		for (std::size_t y = 0; y != output.cols(); ++y) {
			for (std::size_t k = 0; k < b.rows(); ++k)
				output(x, y) += a(x, k) * b(k, y);
		}
	}
	return output;
};

template<typename Type>
Matrix<Type> operator*(const Matrix<Type>& a, Type scalar) {
	Matrix output{a};
	for (std::size_t r = 0; r < output.rows(); ++r) {
		for (std::size_t c = 0; c < output.cols(); ++c) {
			output(r, c) = scalar * a(r, c);
		}
	}
	return output;
}

template <typename Type>
Matrix<Type> Matrix<Type>::multiply_elementwise(const Matrix<Type>& target) const {
	assert(shape() == target.shape());

	Matrix output{*this};
	for (std::size_t r = 0; r < output.rows(); ++r) {
		for (std::size_t c = 0; c < output.cols(); ++c) {
			output(r, c) = target(r, c) * (*this)(r, c);
		}
	}
	return output;
}

template<typename Type>
Matrix<Type> Matrix<Type>::square() const {
	return multiply_elementwise(*this);
}

template<typename Type>
Matrix<Type> operator+(const Matrix<Type>& a, const Matrix<Type>& b) {
	assert(a.shape() == b.shape());

	Matrix<Type> output{a.rows(), b.cols()};
	for (std::size_t r = 0; r < output.rows(); ++r) {
		for (std::size_t c = 0; c < output.cols(); ++c) {
			output(r, c) = a(r, c) + b(r, c);
		}
	}
	return output;
}

template<typename Type>
Matrix<Type> operator-(const Matrix<Type>& a, const Matrix<Type>& b) {
	Matrix<Type> neg_b = -b;
	return a + neg_b;
}

template<typename Type>
Matrix<Type> Matrix<Type>::T() const {
	std::size_t new_rows{ cols() }, new_cols { rows() };
	Matrix transposed(new_rows, new_cols);

	for (std::size_t r = 0; r < new_rows; ++r) {
		for (std::size_t c = 0; c < new_cols; ++c) {
			transposed(r, c) = (*this)(c, r);  // swap row and col
		}
	}
	return transposed;
}

template<typename Type>
Matrix<Type> Matrix<Type>::apply_function(Type(*func)(Type)) {
	Matrix output((*this));
	for (std::size_t r = 0; r < rows(); ++r) {
		for (std::size_t c = 0; c < cols(); ++c) {
			output(r, c) = func((*this)(r, c));
		}
	}
	return output;
}

template <typename T>
struct mtx {
	static Matrix<T> randn(std::size_t rows, std::size_t cols) {
		Matrix<T> M(rows, cols);

		std::random_device rd {};
		std::mt19937 gen { rd() };

		// init Gaussian distr. w/ N(mean=0, stdev=1/sqrt(numel))
		T n(M.elementCount());
		T stdev{ 1 / sqrt(n) };
		std::normal_distribution<T> d { 0, stdev };

		// fill each element w/ draw from distribution
		for (std::size_t r = 0; r < rows; ++r) {
			for (std::size_t c = 0; c < cols; ++c) {
				M(r, c) = d(gen);
			}
		}
		return M;
	}
};

//MLP.h
//#pragma once
//#include "Matrix.h"

template<typename T>
class MLP {
 public:
  std::vector<std::size_t> units_per_layer;
  std::vector<Matrix<T>> bias_vectors;
  std::vector<Matrix<T>> weight_matrices;
  std::vector<Matrix<T>> activations;
  std::vector<Matrix<T>> zs;

	inline static constexpr T lr{0.001};

	MLP(std::vector<std::size_t> units_per_layer) : units_per_layer(std::move(units_per_layer)) {
		for (std::size_t i = 0; i != units_per_layer.size() - 1; ++i) {
			std::size_t in_channels{units_per_layer[i]};
			std::size_t out_channels{units_per_layer[i + 1]};

			// initialize to random Gaussian
			auto weight = mtx<T>::randn(out_channels, in_channels);
			weight_matrices.push_back(weight);

			auto bias = mtx<T>::randn(out_channels, 1);
			bias_vectors.push_back(bias);

			auto z = mtx<T>::randn(out_channels,1);
			zs.push_back(z);

			activations.resize(units_per_layer.size());
		}
	}

	auto forward(const Matrix<T>& x) {
		assert(x.rows() == units_per_layer[0] && x.cols());

		activations[0] = x;
		Matrix<T> prev(x);
		for (std::size_t i = 0; i < units_per_layer.size() - 1; ++i) {
			Matrix<T> y = weight_matrices[i] * prev;
			y = y + bias_vectors[i];
			y = y.apply_function(sigmoid);
			activations[i + 1] = y;
			prev = y;
		}
		return prev;
	}

	void backprop(Matrix<T> &target) {
		assert(target.rows() == units_per_layer.back());

		// determine the simple error
		// error = target - output
		Matrix<T> y = target;
		Matrix<T> y_hat = activations.back();
		Matrix<T> error = (target - y_hat);
		Matrix<T> last_z = zs[zs.size()-1];
		Matrix<T> delta_L = error.multiply_elementwise(last_z.apply_function(d_sigmoid));

		// backprop the error from output to input and step the weights
		for (int i = static_cast<int>(weight_matrices.size()) - 2; i >= 0; --i) {
			// calculating error for previous layer
			Matrix<T> delta_l = (weight_matrices[i].T() * delta_L).multiply_elementwise(
				zs[i].apply_function(d_sigmoid));

			// calculating the change of weights and biases
			Matrix<T> nabla_w = delta_l * activations[i].T();
			Matrix<T> nabla_b = delta_l;

			// updating weights and biases
			weight_matrices[i] = weight_matrices[i] - (nabla_w * lr);
			bias_vectors[i] = bias_vectors[i] - (delta_l * lr);

			// updating the error term delta
			delta_L = delta_l;
		}
	}

	static T sigmoid(T x) {
		return 1.0 / (1 + exp(-x));
	}

	static T d_sigmoid(T x) {
		return (x * (1 - x));
	}
};

//Main.cpp
//#pragma once
//#include "Matrix.h"
//#include "MLP.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>

int main() {
	// init model
	std::vector<std::size_t> layers = {1,8,8,8,1};

	// open file to save loss, x, y, and model(x)
	std::ofstream my_file{"data.txt"};

	int max_iter{10000};
	const double PI {3.14159};
	MLP<double> model(layers);

	for (int i = 0; i < max_iter; i++) {
		auto x = mtx<double>::randn(1, 1) * (PI);
		auto y = x.apply_function([](double v) -> double { return sin(v) * sin(v); });

		// forward and backward
		auto y_hat = model.forward(x);
		model.backprop(y); // loss and grads computed in here

		// function that logs (loss, x, y, y_hat)
//		log(file, x, y, y_hat);
	}
}
