#include "digest.hpp"

#include <cryptopp/sha.h>
#include <cryptopp/hex.h>

#include <stdio.h>
#include <string.h>

namespace Digest
{
	typedef unsigned char byte_t;
	typedef std::string digest_t;

	namespace SHA256
	{
		digest_t digest(const std::string& data)
		{
			byte_t digest[CryptoPP::SHA256::DIGESTSIZE];

			CryptoPP::SHA256().CalculateDigest(
				digest,
				reinterpret_cast<const byte_t*>(data.c_str()), data.size());

			return digest_t(reinterpret_cast<const char*>(digest), sizeof(digest));
		}

		digest_t digest(const char* data)
		{
			return digest(std::string(data));
		}

		digest_t digest(const char* data, size_t len)
		{
			return digest(std::string(data, len));
		}
	}

	std::string to_hex(const unsigned char* data, size_t len)
	{
		std::string encoded;
		encoded.reserve(len*2);

//		CryptoPP::StringSource(
//			reinterpret_cast<const byte_t*>(data), len,
//			new CryptoPP::HexEncoder(new CryptoPP::StringSink(encoded)));
//
		for (size_t i = 0; i != len; ++i) {
			unsigned byte = data[i];

			char buf[3];
			snprintf(buf, sizeof(buf), "%02x", byte);
			encoded.append(buf);
		}

		return encoded;
	}

	std::string to_hex(const char* digest)
	{
		return to_hex(reinterpret_cast<const unsigned char*>(digest), strlen(digest));
	}

	std::string to_hex(const digest_t& digest)
	{
		return to_hex(reinterpret_cast<const unsigned char*>(digest.c_str()), digest.size());
	}
}
