#pragma once

#include <string>

namespace Digest
{
	typedef unsigned char byte_t;
	typedef std::string digest_t;

	namespace SHA256
	{
		digest_t digest(const std::string& data);
		digest_t digest(const char* data);
		digest_t digest(const char* data, size_t len);
	}

	std::string to_hex(const unsigned char* data, size_t len);
	std::string to_hex(const char* digest);
	std::string to_hex(const digest_t& digest);
}
