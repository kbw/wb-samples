#include "digest.hpp"
#include "block.hpp"

#include <algorithm>
#include <iostream>

#include <string.h>

int main(int argc, char* argv[])
{
	std::string difficulty;
	bool expect_difficulty = false;

	std::for_each(argv + 1, argv + argc, [&](const char* arg) {
		if (expect_difficulty) {
			expect_difficulty = false;
			difficulty = arg;
			return;
		}
		if (!strcmp(arg, "-d") || !strcmp(arg, "--difficulty")) {
			expect_difficulty = true;
			return;
		}

		using namespace Digest;
		std::cout << to_hex(SHA256::digest(arg)) << "\t";
		std::cout << to_hex(Block(arg, difficulty).digest()) << "\t";
		std::cout << arg << "\n";
	});
}
