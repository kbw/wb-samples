#pragma once

#include "digest.hpp"

#include <algorithm>
#include <iostream>

#include <string.h>

class Block
{
public:
	Block(const std::string& data, const std::string& difficulty = std::string()) :
		m_data(data)
	{
		proof(difficulty, data);
	}

	std::string			data() const	{ return m_data; }
	Digest::digest_t	digest() const	{ return m_digest; }
	int					nonce() const	{ return m_nonce; }

private:
	void proof(const std::string& difficulty, const std::string& data)
	{
		int nonce = -1;
		Digest::digest_t digest;

		do
		{
			std::string str = std::to_string(++nonce) + data;
			digest = Digest::SHA256::digest(str);
		}
		while (strncmp(digest.c_str(), difficulty.c_str(), difficulty.size()));

		m_digest = std::move(digest);
		m_nonce = nonce;
	}

	std::string m_data;
	Digest::digest_t m_digest;
	int m_nonce;
};
