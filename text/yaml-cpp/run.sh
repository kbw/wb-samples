#!/bin/sh

rm build-node-?.yaml 2>/dev/null
rm copy-node-?.yaml 2>/dev/null

time make && \
 ./build-node && \
 echo "----build-node-0.yaml----" && cat build-node-0.yaml && echo "" && \
 echo "----build-node-1.yaml----" && cat build-node-1.yaml && echo "" && \
 echo "----build-node-2.yaml----" && cat build-node-2.yaml && echo "" && \
 ./copy-node && \
 echo "----copy-node-0.yaml-----" && cat copy-node-0.yaml && echo "" && \
 echo "----copy-node-1.yaml-----" && cat copy-node-1.yaml && echo ""
