#include <yaml-cpp/yaml.h>

#include <fstream>
#include <string>

#include <cxxabi.h>
#include <exception>
#include <typeinfo>
#include <iostream>

// https://github.com/jbeder/yaml-cpp/wiki/Tutorial

YAML::Node create_tree();
YAML::Node create_subtree();
std::string type(const YAML::Node& in);

YAML::Node create_tree() {
	YAML::Node n0;
	n0["tree"] = create_subtree();

	return n0;
}

YAML::Node create_subtree() {
	YAML::Node n0, n1;
	for (int i = 0; i < 12; i += 4)
		n1["seq"].push_back(i);
	n0["subtree"] = n1;

	return n0;
}

std::string type(const YAML::Node& in) {
	if (in.IsScalar())
		return "scalar";
	else if (in.IsSequence())
		return "sequence";
	else if (in.IsMap())
		return "map";

	return "dunno";
}

YAML::Node copy(const YAML::Node& in) {
	YAML::Node node;

	if (in.IsScalar()) {
	}
	else if (in.IsSequence())
		for (YAML::const_iterator p = in.begin(); p != in.end(); ++p)
			node.push_back(p->as<std::string>());
	else if (in.IsMap())
		for (YAML::const_iterator p = in.begin(); p != in.end(); ++p)
			node[p->first.as<std::string>()] = copy(p->second);
	else
		throw std::runtime_error("unknown node");

	return node;
}

int main()
try
{
	YAML::Node node;

	// create hierarchy
	{
		node = create_tree();

		std::ofstream out("copy-node-0.yaml", std::ios::trunc);
		out << node;
	}

	// copy out node->tree->subtree and paste into node->new
	{
		node["dup"] = copy(node);
		node["dup-tree"] = copy(node["tree"]);
		node["dup-tree-subtree"] = copy(node["tree"]["subtree"]);
		node["dup-tree-subtree-seq"] = copy(node["tree"]["subtree"]["seq"]);

		std::ofstream out("copy-node-1.yaml", std::ios::trunc);
		out << node;
	}
}
catch (const std::exception& e)
{
	int status = -1;
	const char* name = typeid(e).name();

	if (const std::type_info *t = abi::__cxa_current_exception_type())
	{
		// get exception type name (mangled)
		const char* mangled_name = t->name();

		// get exception type name plain text
		name = abi::__cxa_demangle(mangled_name, 0, 0, &status);
		if (status != 0)
		{
			// fall back to the mangled name
			name = mangled_name;
		}
	}

	std::clog << name << ": " << e.what() << std::endl;

	if (status == 0)
	{
		free(const_cast<char*>(name));
	}
}
