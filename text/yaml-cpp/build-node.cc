#include <yaml-cpp/yaml.h>
#include <exception>
#include <typeinfo>
#include <iostream>
#include <fstream>

// https://github.com/jbeder/yaml-cpp/wiki/Tutorial

int main()
try
{
	YAML::Node node;	// starts out as null
	{
		std::ofstream out("build-node-0.yaml", std::ios::trunc);
		out << node;
	}

	node["key"] = "value";	// it now is a map node
	{
		std::ofstream out("build-node-1.yaml", std::ios::trunc);
		out << node;
	}

	node["seq"].push_back("first element");  // node["seq"] automatically becomes a sequence
	node["seq"].push_back("second element");
	{
		std::ofstream out("build-node-2.yaml", std::ios::trunc);
		out << node;
	}

	node["mirror"] = node["seq"][0];	// this creates an alias
	node["seq"][0] = "1st element";	// this also changes node["mirror"]
	node["mirror"] = "element #1";	// and this changes node["seq"][0] - they're really the "same" node
	{
		std::ofstream out("build-node-3.yaml", std::ios::trunc);
		out << node;
	}

	node["self"] = node;	// you can even create self-aliases
	node[node["mirror"]] = node["seq"];	// and strange loops
	{
		std::ofstream out("build-node-4.yaml", std::ios::trunc);
		out << node;
	}
}
catch (const std::exception& e)
{
	std::clog << typeid(e).name() << ": " << e.what() << std::endl;
}
