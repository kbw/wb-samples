#include <yaml-cpp/yaml.h>
#include <exception>
#include <typeinfo>
#include <iostream>

int main()
try
{
	YAML::Node config = YAML::LoadFile("missing.yaml");
}
catch (const std::exception& e)
{
	using namespace std;
	clog << typeid(e).name() << ": " << e.what() << endl;
}
