#include <rapidjson/document.h>
#include <rapidjson/pointer.h>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>

typedef	std::string string_t;
typedef	std::vector<string_t> strings_t;
typedef	std::unique_ptr<rapidjson::Document> jsondoc_t;
typedef	std::vector<jsondoc_t> jsondocs_t;

inline const char* to_cstr( const string_t& s ) {
	return s.c_str();
}

const strings_t check_payload = {
	"{\"component_n\":14,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":15,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":16,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":32,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":34,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":59,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":85,\"errors\":[\"DNSConnectThreshold\"]}",
	"{\"component_n\":1,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":2,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":15,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":16,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":34,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":40,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":57,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":59,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":64,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":69,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":74,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":78,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":82,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":83,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":84,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":85,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":90,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":91,\"errors\":[\"ConnectThreshold\"]}"
};

const strings_t result_code_json = {
	"{\"uid\":\"300000012345600\",\"step_n\":1,\"checks\":{\"components\":[{\"component_n\":14,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":15,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":16,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":32,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":34,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":59,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":85,\"errors\":[\"DNSConnectThreshold\"]}]},\"result\":{\"code\":\"42\",\"color\":\"Red\"}}",
	"{\"uid\":\"300000012345600\",\"step_n\":1,\"checks\":{\"components\":[{\"component_n\":1,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":2,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":15,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":16,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":34,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":40,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":57,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":59,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":64,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":69,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":74,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":78,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":82,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":83,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":84,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":85,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":90,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":91,\"errors\":[\"ConnectThreshold\"]}]},\"result\":{\"code\":42,\"color\":\"Red\"}}"
};

std::ostream& print_type(std::ostream& os, rapidjson::Document& doc);
std::ostream& print_type(std::ostream& os, rapidjson::Value& value);
std::ostream& print_value(std::ostream& os, rapidjson::Document& doc);
std::ostream& print_value(std::ostream& os, rapidjson::Value& doc);
std::ostream& print_object_value(std::ostream& os, rapidjson::Document& doc);
std::ostream& print_array_value(std::ostream& os, rapidjson::Document& doc);
std::ostream& print_object_value(std::ostream& os, rapidjson::Value& doc);
std::ostream& print_array_value(std::ostream& os, rapidjson::Value& doc);
string_t serialize(rapidjson::Document& doc);
void process(rapidjson::Document& doc);

int main() {
	jsondocs_t docs;
	for (const string_t& str : result_code_json) {
		docs.emplace_back(new rapidjson::Document());
		docs.back()->Parse( to_cstr(str) );
	}

	std::cout << (result_code_json.back() == serialize(*docs.back()) ? "same" : "different") << std::endl;
	process(*docs.back());
}

std::ostream& print_type(std::ostream& os, rapidjson::Document& doc) {
	os << (
		doc.IsNull()	? "null" :
		doc.IsBool()	? "bool" :
		doc.IsNumber()	? "number" :
		doc.IsInt()		? "int" :
		doc.IsDouble()	? "double" :
		doc.IsString()	? "string" :
		doc.IsObject()	? "object" :
		doc.IsArray()	? "array" : "dunno");

	return os;
}

std::ostream& print_type(std::ostream& os, rapidjson::Value& doc) {
	os << (
		doc.IsNull()	? "null" :
		doc.IsBool()	? "bool" :
		doc.IsNumber()	? "number" :
		doc.IsInt()		? "int" :
		doc.IsDouble()	? "double" :
		doc.IsString()	? "string" :
		doc.IsObject()	? "object" :
		doc.IsArray()	? "array" : "dunno");

	return os;
}

std::ostream& print_value(std::ostream& os, rapidjson::Document& doc) {
	(
		doc.IsNull()	? (os << "(null)") :
		doc.IsBool()	? (os << doc.GetBool()) :
//		doc.IsNumber()	? (os << doc.GetNumber()) :
		doc.IsInt()		? (os << doc.GetInt()) :
		doc.IsDouble()	? (os << doc.GetDouble()) :
		doc.IsString()	? (os << doc.GetString()) :
		doc.IsObject()	? print_object_value(os, doc) :
		doc.IsArray()	? print_array_value(os, doc) : (os << "dunno"));

	return os;
}

std::ostream& print_value(std::ostream& os, rapidjson::Value& doc) {
	(
		doc.IsNull()	? (os << "(null)") :
		doc.IsBool()	? (os << doc.GetBool()) :
//		doc.IsNumber()	? (os << doc.GetNumber()) :
		doc.IsInt()		? (os << doc.GetInt()) :
		doc.IsDouble()	? (os << doc.GetDouble()) :
		doc.IsString()	? (os << '\"' << doc.GetString() << '\"') :
		doc.IsObject()	? print_object_value(os, doc) :
		doc.IsArray()	? print_array_value(os, doc) : (os << "dunno"));

	return os;
}

std::ostream& print_object_value(std::ostream& os, rapidjson::Document& doc) {
	os << "{";
	bool first = true;
	for (rapidjson::Value::MemberIterator p = doc.MemberBegin(); p != doc.MemberEnd(); ++p) {
		if (first)
			first = false;
		else
			os << ',';
		print_value(os, p->name);
		os << ':';
		print_value(os, p->value);
	}
	os << "}";

	return os;
}

std::ostream& print_array_value(std::ostream& os, rapidjson::Document& doc) {
	os << "[";
	for (size_t i = 0; i != doc.Size(); ++i) {
		if (i)
			os << ',';
		print_value(os, doc[i]);
	}
	os << "]";

	return os;
}

std::ostream& print_object_value(std::ostream& os, rapidjson::Value& doc) {
	os << "{";
	bool first = true;
	for (rapidjson::Value::MemberIterator p = doc.MemberBegin(); p != doc.MemberEnd(); ++p) {
		if (first)
			first = false;
		else
			os << ',';
		print_value(os, p->name);
		os << ':';
		print_value(os, p->value);
	}
	os << "}";

	return os;
}

std::ostream& print_array_value(std::ostream& os, rapidjson::Value& doc) {
	os << "[";
	for (size_t i = 0; i != doc.Size(); ++i) {
		if (i)
			os << ',';
		print_value(os, doc[i]);
	}
	os << "]";

	return os;
}

string_t serialize(rapidjson::Document& doc) {
	std::ostringstream oss;
	print_value(oss, doc);
	return oss.str();
}

void process(rapidjson::Document& doc) {
	using namespace std;

	print_type(cout, doc) << ":";
	print_value(cout, doc) << endl;

	if (rapidjson::Value* value = rapidjson::Pointer("/checks/components").Get(doc)) {
		print_type(cout, *value) << ":";
		print_value(cout, *value) << endl;
	}

	if (rapidjson::Value* value = rapidjson::Pointer("/result/code").Get(doc)) {
		print_type(cout, *value) << ":";
		print_value(cout, *value) << endl;
	}

	if (rapidjson::Value* value = rapidjson::Pointer("/result/color").Get(doc)) {
		print_type(cout, *value) << ":";
		print_value(cout, *value) << endl;
	}
}
