#include "defs.hpp"
#include "rj_tools.hpp"
#include <ostream>
#include <sstream>

std::ostream& print_type(std::ostream& os, const rapidjson::Document& doc) {
	os << (
		doc.IsNull()	? "null" :
		doc.IsBool()	? "bool" :
		doc.IsNumber()	? "number" :
		doc.IsInt()		? "int" :
		doc.IsDouble()	? "double" :
		doc.IsString()	? "string" :
		doc.IsObject()	? "object" :
		doc.IsArray()	? "array" : "dunno");

	return os;
}

std::ostream& print_type(std::ostream& os, const rapidjson::Value& doc) {
	os << (
		doc.IsNull()	? "null" :
		doc.IsBool()	? "bool" :
		doc.IsNumber()	? "number" :
		doc.IsInt()		? "int" :
		doc.IsDouble()	? "double" :
		doc.IsString()	? "string" :
		doc.IsObject()	? "object" :
		doc.IsArray()	? "array" : "dunno");

	return os;
}

std::ostream& print_value(std::ostream& os, const rapidjson::Document& doc) {
	(
		doc.IsNull()	? (os << "(null)") :
		doc.IsBool()	? (os << doc.GetBool()) :
//		doc.IsNumber()	? (os << doc.GetNumber()) :
		doc.IsInt()		? (os << doc.GetInt()) :
		doc.IsDouble()	? (os << doc.GetDouble()) :
		doc.IsString()	? (os << doc.GetString()) :
		doc.IsObject()	? print_object_value(os, doc) :
		doc.IsArray()	? print_array_value(os, doc) : (os << "dunno"));

	return os;
}

std::ostream& print_value(std::ostream& os, const rapidjson::Value& doc) {
	(
		doc.IsNull()	? (os << "(null)") :
		doc.IsBool()	? (os << doc.GetBool()) :
//		doc.IsNumber()	? (os << doc.GetNumber()) :
		doc.IsInt()		? (os << doc.GetInt()) :
		doc.IsDouble()	? (os << doc.GetDouble()) :
		doc.IsString()	? (os << '\"' << doc.GetString() << '\"') :
		doc.IsObject()	? print_object_value(os, doc) :
		doc.IsArray()	? print_array_value(os, doc) : (os << "dunno"));

	return os;
}

std::ostream& print_object_value(std::ostream& os, const rapidjson::Document& doc) {
	os << "{";
	bool first = true;
	for (rapidjson::Value::ConstMemberIterator p = doc.MemberBegin(); p != doc.MemberEnd(); ++p) {
		if (first)
			first = false;
		else
			os << ',';

		print_value(os, p->name);
		os << ':';
		print_value(os, p->value);
	}
	os << "}";

	return os;
}

std::ostream& print_object_value(std::ostream& os, const rapidjson::Value& doc) {
	os << "{";
	bool first = true;
	for (rapidjson::Value::ConstMemberIterator p = doc.MemberBegin(); p != doc.MemberEnd(); ++p) {
		if (first)
			first = false;
		else
			os << ',';

		print_value(os, p->name);
		os << ':';
		print_value(os, p->value);
	}
	os << "}";

	return os;
}

std::ostream& print_array_value(std::ostream& os, const rapidjson::Document& doc) {
	os << "[";
	for (size_t i = 0; i != doc.Size(); ++i) {
		if (i)
			os << ',';

		print_value(os, doc[i]);
	}
	os << "]";

	return os;
}

std::ostream& print_array_value(std::ostream& os, const rapidjson::Value& doc) {
	os << "[";
	for (size_t i = 0; i != doc.Size(); ++i) {
		if (i)
			os << ',';

		print_value(os, doc[i]);
	}
	os << "]";

	return os;
}

string_t to_string(const rapidjson::Document& doc) {
	std::ostringstream oss;
	print_value(oss, doc);
	return oss.str();
}

rapidjson::Document to_json(const string_t& json) {
	rapidjson::Document doc;
	doc.Parse(json.c_str());
	return doc;
}
