#pragma once

#include <string>
#include <vector>

typedef	std::string string_t;
typedef	std::vector<string_t> strings_t;

inline const char* to_cstr( const string_t& s ) {
	return s.c_str();
}
