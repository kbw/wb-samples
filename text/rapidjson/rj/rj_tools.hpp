#pragma once

#include "defs.hpp"
#include <rapidjson/document.h>
#include <rapidjson/pointer.h>
#include <iosfwd>

typedef	std::unique_ptr<rapidjson::Document> jsondoc_t;
typedef	std::vector<jsondoc_t> jsondocs_t;

std::ostream& print_type(std::ostream& os, const rapidjson::Document& doc);
std::ostream& print_type(std::ostream& os, const rapidjson::Value& value);

std::ostream& print_value(std::ostream& os, const rapidjson::Document& doc);
std::ostream& print_value(std::ostream& os, const rapidjson::Value& doc);

std::ostream& print_object_value(std::ostream& os, const rapidjson::Document& doc);
std::ostream& print_object_value(std::ostream& os, const rapidjson::Value& doc);

std::ostream& print_array_value(std::ostream& os, const rapidjson::Document& doc);
std::ostream& print_array_value(std::ostream& os, const rapidjson::Value& doc);

string_t to_string(const rapidjson::Document& doc);
rapidjson::Document to_json(const string_t& json);
