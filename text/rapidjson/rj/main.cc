#include "defs.hpp"
#include "rj_tools.hpp"
#include <iostream>

const strings_t check_payload_json = {
	"{\"component_n\":14,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":15,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":16,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":32,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":34,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":59,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":85,\"errors\":[\"DNSConnectThreshold\"]}",
	"{\"component_n\":1,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":2,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":15,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":16,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":34,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":40,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":57,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":59,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":64,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":69,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":74,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":78,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":82,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":83,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":84,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":85,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":90,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":91,\"errors\":[\"ConnectThreshold\"]}"
};

const strings_t result_code_json = {
	"{\"uid\":\"300000012345600\",\"step_n\":1,\"checks\":{\"components\":[{\"component_n\":14,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":15,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":16,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":32,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":34,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":59,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":85,\"errors\":[\"DNSConnectThreshold\"]}]},\"result\":{\"code\":\"42\",\"color\":\"Red\"}}",
	"{\"uid\":\"300000012345600\",\"step_n\":1,\"checks\":{\"components\":[{\"component_n\":1,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":2,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":15,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":16,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":34,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":40,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":57,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":59,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":64,\"errors\":[\"ConnectThreshold\",\"DNSConnectThreshold\"]},{\"component_n\":69,\"errors\":[\"DNSConnectThreshold\",\"ConnectThreshold\"]},{\"component_n\":74,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":78,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":82,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":83,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":84,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":85,\"errors\":[\"DNSConnectThreshold\"]},{\"component_n\":90,\"errors\":[\"ConnectThreshold\"]},{\"component_n\":91,\"errors\":[\"ConnectThreshold\"]}]},\"result\":{\"code\":42,\"color\":\"Red\"}}"
};

void process(rapidjson::Document& doc);

int main() {
	jsondocs_t docs;
	for (string_t str : check_payload_json) {
		str = "[" + str + "]";
		docs.emplace_back(new rapidjson::Document());
		docs.back()->Parse( to_cstr(str) );

		std::cout
			<< "parse test:"     << (str == to_string(*docs.back()) ? "same" : "different") << " "
			<< "serialize test:" << (str == to_string(to_json(str)) ? "same" : "different") << std::endl;
	}
	for (const string_t& str : result_code_json) {
		docs.emplace_back(new rapidjson::Document());
		docs.back()->Parse( to_cstr(str) );

		std::cout
			<< "parse test:"     << (str == to_string(*docs.back()) ? "same" : "different") << " "
			<< "serialize test:" << (str == to_string(to_json(str)) ? "same" : "different") << std::endl;
	}

	process(*docs.back());
}

void process(rapidjson::Document& doc) {
	using namespace std;

	rapidjson::Document newdoc;
	string_t name;

	name = "/checks";
	if (rapidjson::Value* value = rapidjson::Pointer(name.c_str()).Get(doc)) {
		rapidjson::Pointer(name.c_str()).GetWithDefault(newdoc, *value);
	}

	cout << "doc:";
	print_type(cout, doc) << endl;
	print_value(cout, doc) << endl;
	cout << "newdoc:";
	print_type(cout, newdoc) << endl;
	print_value(cout, newdoc) << endl;
}

/*
void process(rapidjson::Document& doc) {
	using namespace std;

	print_type(cout, doc) << ":";
	print_value(cout, doc) << endl;

	if (rapidjson::Value* value = rapidjson::Pointer("/checks/components").Get(doc)) {
		print_type(cout, *value) << ":";
		print_value(cout, *value) << endl;
	}

	if (rapidjson::Value* value = rapidjson::Pointer("/result/code").Get(doc)) {
		print_type(cout, *value) << ":";
		print_value(cout, *value) << endl;
	}

	if (rapidjson::Value* value = rapidjson::Pointer("/result/color").Get(doc)) {
		print_type(cout, *value) << ":";
		print_value(cout, *value) << endl;
	}
}
 */
