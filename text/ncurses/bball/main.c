#include <ncurses.h>
#include <unistd.h>

#define DELAY (30*1000)

void update_pos(int* y, int* x, int* dy, int* dx);

int main()
{
	int y, x, dy, dx;

	initscr();
	clear();
	noecho();

	for (y = x = 0, dy = dx = 1; ; update_pos(&y, &x, &dy, &dx))
	{
		mvprintw(y += dy, x += dx, "+");

		refresh();
		usleep(DELAY);
	}

	endwin();
	return 0;
}

void update_pos(int* y, int* x, int* dy, int* dx)
{
	int maxy, maxx;
	getmaxyx(stdscr, maxy, maxx);

	if (*y <= 0) {
		*y = 0;
		*dy = 1;
	}
	else if (*y >= maxy) {
		*y = maxy;
		*dy = -1;
	}
	else if (*dy != -1 && *dy != 1)
		*dy = 1;

	if (*x <= 0) {
		*x = 0;
		*dx = 1;
	}
	else if (*x >= maxx) {
		*x = maxx;
		*dx = -1;
	}
	else if (*dx != -1 && *dx != 1)
		*dx = 1;
}
