#include "ncurses.hpp"
#include <ncurses.h>
#include <unistd.h>
#include <stdlib.h>

#include <algorithm>
#include <list>

namespace NCurses
{
	class AppImpl
	{
	public:
		AppImpl(App* parent);
		~AppImpl();
		void run();
		void stop();

		void add(NCurses::Window* w);
		void remove(NCurses::Window* w);

	private:
		AppImpl(const AppImpl& n) = delete;
		AppImpl& operator=(const AppImpl& n) = delete;

		typedef std::list<Window*> windows_t;

		App* const parent_;
		windows_t windows_;
	};

	class WindowImpl
	{
	public:
		WindowImpl(NCurses::App* app, NCurses::Window* parent, int y, int x, int dy, int dx, Border border);
		~WindowImpl();
		void setBorder(Border b);

	private:
		WindowImpl(const WindowImpl& n) = delete;
		WindowImpl& operator=(const WindowImpl& n) = delete;

		App* const app_;
		Window* const parent_;
		int y_, x_, dy_, dx_;
		Border b_;
		WINDOW* const w_;
	};
}

//---------------------------------------------------------------------------
// App
//
inline NCurses::AppImpl::AppImpl(NCurses::App* parent) :
	parent_(parent)
{
	initscr();
	clear();
	noecho();
}

inline NCurses::AppImpl::~AppImpl()
{
	endwin();
}

inline void NCurses::AppImpl::run()
{
}

inline void NCurses::AppImpl::stop()
{
	if (parent_->OnStop()) {
		;
	}
}

inline void NCurses::AppImpl::add(NCurses::Window* w)
{
	windows_.push_back(w);
}

inline void NCurses::AppImpl::remove(NCurses::Window* w)
{
	auto p = std::find(windows_.begin(), windows_.end(), w);
	if (p !=  windows_.end())
		windows_.erase(p);
}

//---------------------------------------------------------------------------

NCurses::App::App() :
	pImpl(new NCurses::AppImpl(this))
{
}

NCurses::App::~App()
{
	delete pImpl;
}

void NCurses::App::run()
{
	bool first = true;
	for (int ch = 0; ch != 'q'; ch = getch()) {
		if (first) {
			first = false;
			OnInit();
		}

		OnKey(ch);
	}
}

void NCurses::App::stop()
{
	pImpl->stop();
}

void NCurses::App::add(NCurses::Window* w)
{
	pImpl->add(w);
	w->OnCreate();
}

void NCurses::App::remove(NCurses::Window* w)
{
	w->OnDestroy();
	pImpl->remove(w);
}

//---------------------------------------------------------------------------
// Window
//
inline NCurses::WindowImpl::WindowImpl(NCurses::App* app, NCurses::Window* parent, int y, int x, int dy, int dx, Border border) :
	app_(app),
	parent_(parent),
	y_(y), x_(x), dy_(dy), dx_(dx),
	b_(std::move(border)),
	w_(newwin(dy_, dx_, y_, x_))
{
	box(w_, 0, 0);
	wborder(w_, b_.l, b_.r, b_.t, b_.b, b_.tl, b_.tr, b_.bl, b_.br);

	app_->add(parent_);
	wrefresh(w_);
	parent_->OnCreate();
}

inline NCurses::WindowImpl::~WindowImpl()
{
	parent_->OnDestroy();
	app_->remove(parent_);

	wborder(w_, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	/* The parameters taken are
	 * 1. win: the window on which to operate
	 * 2. ls: character to be used for the left side of the window
	 * 3. rs: character to be used for the right side of the window
	 * 4. ts: character to be used for the top side of the window
	 * 5. bs: character to be used for the bottom side of the window
	 * 6. tl: character to be used for the top left corner of the window
	 * 7. tr: character to be used for the top right corner of the window
	 * 8. bl: character to be used for the bottom left corner of the window 
	 * 9. br: character to be used for the bottom right corner of the window 
	 */
	wrefresh(w_);
	delwin(w_);
}

void NCurses::WindowImpl::setBorder(Border b) {
	b_ = std::move(b);
}

//---------------------------------------------------------------------------
//
NCurses::Window::Window(NCurses::App* app, int y, int x, int dy, int dx, Border border) :
	pImpl(new WindowImpl(app, this, y, x, dy, dx, std::move(border)))
{
}

NCurses::Window::~Window()
{
	delete pImpl;
}

void NCurses::Window::setBorder(Border b) {
	pImpl->setBorder(std::move(b));
}

void NCurses::Window::OnCreate()
{
}

void NCurses::Window::OnDestroy()
{
}
