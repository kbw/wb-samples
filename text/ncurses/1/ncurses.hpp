#pragma once

namespace NCurses
{
	class Window;
	class WindowImpl;
	class AppImpl;

	class App
	{
	public:
		App();
		virtual ~App();
		virtual void run();
		virtual void stop();

		virtual void OnInit() {}
		virtual bool OnStop() { return true; }
		virtual void OnKey(int /*ch*/) {}

		virtual void add(NCurses::Window* w);
		virtual void remove(NCurses::Window* w);

	private:
		App(const App& n) = delete;
		App& operator=(const App& n) = delete;

		NCurses::AppImpl* pImpl;
	};

	struct Border
	{
		static constexpr char defSides{'|'};
		static constexpr char defTopBottom{'-'};
		static constexpr char defCorners{'+'};

		Border(char sides = defSides, char topBottom = defTopBottom, char corners = defCorners) {
			setSides(sides);
			setTopBottom(topBottom);
			setCorners(corners);
		}

		void setSides(char ch = defCorners) { l = r = ch; }
		void setTopBottom(char ch = defCorners) { t = b = ch; }
		void setCorners(char ch = defCorners) { tl = tr = bl = br = ch; }

		char l = defSides, r = defSides;
		char t = defTopBottom, b = defTopBottom;
		char tl = defCorners, tr = defCorners, bl = defCorners, br = defCorners;
	};

	class Window
	{
	public:
		Window(App* app, int y, int x, int dy = 5, int dx = 20, Border border = Border{});
		virtual ~Window();
		virtual void setBorder(Border b);

		virtual void OnCreate();
		virtual void OnDestroy();

	private:
		Window(const Window& n) = delete;
		Window& operator=(const Window& n) = delete;

		NCurses::WindowImpl* pImpl;
	};
}
