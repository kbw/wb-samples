#include "ncurses.hpp"
#include <unistd.h>
#include <memory>
#include <vector>
#include <time.h>
#include <stdlib.h>

#define DELAY (30*1000)

class App : public NCurses::App
{
public:
	App(time_t stop);
	void run();

	void OnCreate();

private:
	time_t stop_;
	int nwindows_;
};

//---------------------------------------------------------------------------
//
App::App(time_t stop) :
	stop_(stop),
	nwindows_(4)
{
}

void App::run()
{
	std::vector<std::unique_ptr<NCurses::Window>> w;
	for (int i = 0; i != nwindows_; ++i) {
//		w.emplace_back(new NCurses::Window(this, 2*i, 2*i + 8));
		w.emplace_back(new NCurses::Window(this, 2*i, 2*i + 8, 5, 20, {'*', '*', '*'}));
		sleep(1);
	}

	while (time(0) < stop_) {
		//refresh();
		//wrefresh(*main);
		//wrefresh(*w2);
		//wrefresh(*w3);
		//wrefresh(*w4);
		usleep(DELAY);
	}
}

//---------------------------------------------------------------------------
//
int main(int argc, char* argv[])
{
	time_t stop = time(0) + (argc > 1 ? atoi(argv[1]) : 10);

	std::unique_ptr<NCurses::App> app(new App(stop));
	app->run();
}
