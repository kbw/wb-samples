//----------------------------------------------------------------------------
// A single-line ACS character box is drawn around the standard screen
//----------------------------------------------------------------------------
#include <ncurses.h>

int main() {
	initscr();

	box(stdscr, 0, 0);
	refresh();
	getch();

	endwin();
}
