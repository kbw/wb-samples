//----------------------------------------------------------------------------
// toggle between the two windows created in 09-02_switch
//----------------------------------------------------------------------------
#include <ncurses.h>

int main() {
	initscr();
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLUE);
	init_pair(2, COLOR_WHITE, COLOR_RED);
	init_pair(3, COLOR_WHITE, COLOR_GREEN);
	init_pair(4, COLOR_WHITE, COLOR_CYAN);
	init_pair(5, COLOR_WHITE, COLOR_MAGENTA);

	// create 4 windows
	WINDOW* topleft = newwin(LINES/2, COLS/2, 0, 0);
	if (!topleft) {
		addstr("Unable to create window");
		refresh();
		getch();
		endwin();
		return 1;
	}

	WINDOW* topright = newwin(LINES/2, COLS/2, LINES/2, 0);
	if (!topright) {
		addstr("Unable to create window");
		refresh();
		getch();
		delwin(topleft);
		endwin();
		return 1;
	}

	WINDOW* bottomleft = newwin(LINES/2, COLS/2, 0, COLS/2);
	if (!bottomleft) {
		addstr("Unable to create window");
		refresh();
		getch();
		delwin(topright);
		delwin(topleft);
		endwin();
		return 1;
	}

	WINDOW* bottomright = newwin(LINES/2, COLS/2, LINES/2, COLS/2);
	if (!bottomright) {
		addstr("Unable to create window");
		refresh();
		getch();
		delwin(bottomleft);
		delwin(topright);
		delwin(topleft);
		endwin();
		return 1;
	}

	wbkgd(topleft, COLOR_PAIR(5));
	box(topleft, 0, 0);
	waddstr(topleft, "This is top left window\n");
	waddstr(topleft, "Press Enter to switch, any other key to quit");

	wbkgd(topright, COLOR_PAIR(4));
	box(topright, 0, 0);
	waddstr(topright, "This is top right window\n");
	waddstr(topright, "Press Enter to switch, any other key to quit");

	wbkgd(bottomleft, COLOR_PAIR(3));
	box(bottomleft, 0, 0);
	waddstr(bottomleft, "This is bottom left window\n");
	waddstr(bottomleft, "Press Enter to switch, any other key to quit");

	wbkgd(bottomright, COLOR_PAIR(2));
	box(bottomright, 0, 0);
	waddstr(bottomright, "This is bottom right window\n");
	waddstr(bottomright, "Press Enter to switch, any other key to quit");

	bkgd(COLOR_PAIR(1));
	box(stdscr, 0, 0);
	addstr("This is the standard screen\n");
	addstr("Press Enter to switch, any other key to quit");

	int ch;
	do {
		touchwin(stdscr); // invalidate rect
		refresh();
		ch = getch();
		if (ch != 10) break;

		touchwin(topleft); // invalidate rect
		wrefresh(topleft);
		ch = wgetch(topleft);
		if (ch != 10) break;

		touchwin(topright); // invalidate rect
		wrefresh(topright);
		ch = wgetch(topright);
		if (ch != 10) break;

		touchwin(bottomleft); // invalidate rect
		wrefresh(bottomleft);
		ch = wgetch(bottomleft);
		if (ch != 10) break;

		touchwin(bottomright); // invalidate rect
		wrefresh(bottomright);
		ch = wgetch(bottomright);
		if (ch != 10) break;
	}
	while (true);

	delwin(bottomright);
	delwin(bottomleft);
	delwin(topright);
	delwin(topleft);
	endwin();
}
