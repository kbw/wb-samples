//----------------------------------------------------------------------------
// create two windows with color to distiguish them
//----------------------------------------------------------------------------
#include <ncurses.h>

int main() {
	initscr();
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLUE);
	init_pair(2, COLOR_WHITE, COLOR_RED);

	// create second window
	WINDOW* second = newwin(0, 0, 0, 0);
	if (!second) {
		addstr("Unable to create window");
		refresh();
		getch();
		endwin();
		return 1;
	}

	wbkgd(second, COLOR_PAIR(2));
	waddstr(second, "This is the second window");

	bkgd(COLOR_PAIR(1));
	addstr("This is the standard screen\n");
	addstr("Press Enter");
	refresh();
	getch();

	wrefresh(second);
	getch();

	delwin(second);
	endwin();
}
