#pragma once

#include <ncurses.h>

#include <stdexcept>
#include <vector>
#include <iostream>

class Palette {
public:
	struct value_t { int fg{}, bg{}; };

	Palette(const std::vector<value_t>& values) {
		start_color();
		for (size_t i = 0; i != values.size(); ++i)
			init_pair(i + 1, values[i].fg, values[i].bg);
	}
};

class Window {
public:
	Window() {
		if (nDefaultInitialisation)
			throw std::runtime_error("stdscr Window already creates");
		nDefaultInitialisation = true;
		::initscr();
		handle_ = stdscr;
	}
	Window(WINDOW* handle) : handle_(handle) {
		if (!handle_)
			throw std::runtime_error("cannot initialize with null WINDOW");
	}
	Window(int dx, int dy, int x, int y) : handle_(::newwin(dx, dy, x, y)) {
		if (!handle_)
			throw std::runtime_error("cannot create window");
	}
	Window(Window& parent, int dx, int dy, int x, int y) : handle_(::derwin(parent.handle_, dx, dy, x, y)) {
		if (!handle_)
			throw std::runtime_error("cannot create window");
	}
	virtual ~Window() {
		if (handle_ == stdscr && nDefaultInitialisation) {
			nDefaultInitialisation = false;
			::endwin();
			return;
		}
		close();
	}
	Window(const Window& ) = delete;
	Window& operator=(const Window& ) = delete;
	Window(Window&& n) : handle_(n.handle_) {
		n.handle_ = nullptr;
	}
	Window& operator=(Window&& n) {
		if (this != &n) {
			close();
			handle_ = n.handle_;
			n.handle_ = nullptr;
		}
		return *this;
	}

	void GetMaxYX(int& x, int &y) {
		getmaxyx(handle_, x, y);
	}

	void Box(int x, int y) {
		box(handle_, x, y);
	}

	WINDOW* handle() {
		return handle_;
	}

private:
	void close() {
		if (handle_) {
			::delwin(handle_);
			handle_ = nullptr;
		}
	}

	WINDOW* handle_{nullptr};
	static bool nDefaultInitialisation;
};
