//----------------------------------------------------------------------------
// subwindow within subwindow
//----------------------------------------------------------------------------
#include "ncurses.hpp"

#include <iostream>

int main() 
try {
	Window defwin;
	Palette{{{COLOR_WHITE, COLOR_BLUE}, {COLOR_CYAN, COLOR_YELLOW}, {COLOR_BLACK, COLOR_GREEN}, {COLOR_MAGENTA, COLOR_WHITE}}};

	// create windows
	Window grandpa(LINES-4, COLS-4, 2, 2); // real window
	int gl, gc;
	grandpa.GetMaxYX(gl, gc);

	Window father(grandpa, gl-4, gc-4, 2, 2); // sub-window
	father.GetMaxYX(gl, gc);

	Window son(father, gl-4, gc-4, 2, 2); // sub-window

	// draw box around stdscr
	defwin.Box(0, 0);
	grandpa.Box(0, 0);
	father.Box(0, 0);
	son.Box(0, 0);

	// color windows + some text, order matters, platform dependent
	wbkgd(defwin.handle(), COLOR_PAIR(4));
	wbkgd(grandpa.handle(), COLOR_PAIR(1));
	wbkgd(father.handle(), COLOR_PAIR(2));
	wbkgd(son.handle(), COLOR_PAIR(3));

	waddstr(defwin.handle(), "I am the standard screen");
	waddstr(grandpa.handle(), "I am Grandpa");
	waddstr(father.handle(), "I am Dad");
	waddstr(son.handle(), "I am the boy");

	// put text to the subwindow
	wrefresh(defwin.handle());
	wrefresh(grandpa.handle());
	wrefresh(father.handle());
	wrefresh(son.handle());
	getch();
}
catch (const std::exception& e) {
	std::cerr << "fatal: " << e.what() << std::endl;
}
