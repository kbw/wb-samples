//----------------------------------------------------------------------------
// subwindow shares data with parent
//----------------------------------------------------------------------------
#include <ncurses.h>

int main() {
	initscr();

	// create second window
	WINDOW* sub = derwin(stdscr, LINES-2, COLS - 2, 1, 1);
	if (!sub) {
		addstr("Unable to create window");
		refresh();
		getch();
		endwin();
		return 1;
	}

	// draw box around stdscr
	box(stdscr, 0, 0);

	// put text to the subwindow
	addstr("I'm writing text\n");
	addstr("to the standard screen");
	refresh();
	getch();

	wclear(sub);
	wrefresh(sub);
	getch();

	delwin(sub);
	endwin();
}
