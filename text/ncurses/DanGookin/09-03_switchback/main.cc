//----------------------------------------------------------------------------
// toggle between the two windows created in 09-02_switch
//----------------------------------------------------------------------------
#include <ncurses.h>

int main() {
	initscr();
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLUE);
	init_pair(2, COLOR_WHITE, COLOR_RED);

	// create second window
	WINDOW* second = newwin(0, 0, 0, 0);
	if (!second) {
		addstr("Unable to create window");
		refresh();
		getch();
		endwin();
		return 1;
	}

	wbkgd(second, COLOR_PAIR(2));
	waddstr(second, "This is the second window\n");
	waddstr(second, "Press Enter to switch, any other key to quit");

	bkgd(COLOR_PAIR(1));
	addstr("This is the standard screen\n");
	addstr("Press Enter to switch, any other key to quit");

	int ch;
	do {
		touchwin(stdscr); // invalidate rect
		refresh();
		ch = getch();
		if (ch != 10) break;

		touchwin(second); // invalidate rect
		wrefresh(second);
		ch = wgetch(second);
		if (ch != 10) break;
	}
	while (true);

	delwin(second);
	endwin();
}
