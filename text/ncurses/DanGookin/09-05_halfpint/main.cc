//----------------------------------------------------------------------------
// toggle between the two windows created in 09-02_switch
//----------------------------------------------------------------------------
#include <ncurses.h>

int main() {
	initscr();
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLUE);
	init_pair(2, COLOR_WHITE, COLOR_RED);

	// create nre tiny window
	WINDOW* tiny = newwin(LINES/2, COLS/2, LINES/4, COLS/4);
	if (!tiny) {
		addstr("Unable to create window");
		refresh();
		getch();
		endwin();
		return 1;
	}

	wbkgd(tiny, COLOR_PAIR(2));
	waddstr(tiny, "This is tiny window\n");
	waddstr(tiny, "Press Enter to switch, any other key to quit");

	bkgd(COLOR_PAIR(1));
	addstr("This is the standard screen\n");
	addstr("Press Enter to switch, any other key to quit");

	int ch;
	do {
		touchwin(stdscr); // invalidate rect
		refresh();
		ch = getch();
		if (ch != 10) break;

		touchwin(tiny); // invalidate rect
		wrefresh(tiny);
		ch = wgetch(tiny);
		if (ch != 10) break;
	}
	while (true);

	delwin(tiny);
	endwin();
}
