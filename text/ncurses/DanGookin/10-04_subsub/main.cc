//----------------------------------------------------------------------------
// subwindow within subwindow
//----------------------------------------------------------------------------
#include <ncurses.h>

int main() {
	int gl, gc;

	initscr();
	refresh(); // update stdscr

	// colours
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLUE);
	init_pair(2, COLOR_RED, COLOR_YELLOW);
	init_pair(3, COLOR_BLACK, COLOR_GREEN);

	// create windows
	WINDOW* grandpa = newwin(LINES-4, COLS-4, 2, 2);
	if (!grandpa) {
		addstr("Unable to create grandpa window");
		refresh();
		getch();
		endwin();
		return 1;
	}
	getmaxyx(grandpa, gl, gc);

	WINDOW* father = derwin(grandpa, gl-4, gc-4, 2, 2);
	if (!father) {
		addstr("Unable to create father window");
		refresh();
		getch();
		delwin(grandpa);
		endwin();
		return 1;
	}
	getmaxyx(father, gl, gc);

	WINDOW* son = derwin(father, gl-4, gc-4, 2, 2);
	if (!son) {
		addstr("Unable to create son window");
		refresh();
		getch();
		delwin(father);
		delwin(grandpa);
		endwin();
		return 1;
	}

	// draw box around stdscr
	box(stdscr, 0, 0);
	box(grandpa, 0, 0);
	box(father, 0, 0);
	box(son, 0, 0);

	// color windows + some text
	addstr("I am the standard screen");

	wbkgd(grandpa, COLOR_PAIR(1));
	waddstr(grandpa, "I am Grandpa");

	wbkgd(father, COLOR_PAIR(2));
	waddstr(father, "I am Father");

	wbkgd(son, COLOR_PAIR(3));
	waddstr(son, "I am the boy");

	// put text to the subwindow
	refresh();
	wrefresh(grandpa);
	wrefresh(father);
	wrefresh(son);
	getch();

	delwin(son);
	delwin(father);
	delwin(grandpa);
	endwin();
}
