//----------------------------------------------------------------------------
// subwindow
//----------------------------------------------------------------------------
#include <ncurses.h>

int main() {
	initscr();

	// draw box around stdscr
	box(stdscr, 0, 0);

	// create second window
	WINDOW* sub = subwin(stdscr, LINES-2, COLS - 2, 1, 1);
	if (!sub) {
		addstr("Unable to create window");
		refresh();
		getch();
		endwin();
		return 1;
	}

	// put text to the subwindow
	waddstr(sub, "I'm in a subwindow.\n");
	refresh();
	getch();

	delwin(sub);
	endwin();
}
