//----------------------------------------------------------------------------
// create a second window
//----------------------------------------------------------------------------
#include <ncurses.h>

int main() {
	initscr();
	addstr("This is the standard screen\n");
	refresh();
	getch();

	// create another window
	if (WINDOW* another = newwin(0, 0, 0, 0)) {
		waddstr(another, "This is another window");
		wrefresh(another);
		wgetch(another);
		delwin(another);
	}
	else {
		addstr("Unable to create window");
		refresh();
		endwin();
		getch();
	}

	endwin();
}
