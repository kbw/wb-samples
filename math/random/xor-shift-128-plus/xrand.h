#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void     xorshift128plus_seed(uint64_t a, uint64_t b);
uint64_t xorshift128plus();

#ifdef __cplusplus
}
#endif
