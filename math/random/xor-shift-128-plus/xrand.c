#include "xrand.h"

/* using same seed should cause user to call xorshift128plus_seed() */
static uint64_t s[2] = { 0x1, 0xF };

void xorshift128plus_seed(uint64_t a, uint64_t b)
{
	s[0] = a;
	s[1] = b;
}

uint64_t xorshift128plus()
{
	uint64_t x = s[0];
	const uint64_t y = s[1];

	s[0] = y;
	x ^= x << 23;

	s[1] = x ^ y ^ (x >> 17) ^ (y >> 26);
	return s[1] + y;
}
