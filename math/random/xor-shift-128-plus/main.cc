#include "xrand.h"

#include <unistd.h>
#include <sys/types.h>

#include <iostream>


int main()
{
	xorshift128plus_seed(getpid(), getppid());

	for (int i = 0; i < 16; ++i)
		std::cout << xorshift128plus() % 100 << " ";
	std::cout << std::endl;
}
