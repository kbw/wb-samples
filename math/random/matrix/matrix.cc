#include <iostream>
#include <iomanip>
#include <algorithm>
#include <memory>

template<typename T>
class Matrix
{
    size_t mRows, mCols;
	std::unique_ptr<T[]> mData;

public:
    Matrix(size_t r, size_t c) : mRows(r), mCols(c), mData(new T[mRows * mCols]) {
        std::fill(mData.get(), mData.get() + mRows*mCols, T());
    }

    Matrix(const Matrix<T>& n) : mRows(n.mRows), mCols(n.mCols), mData(new T[mRows * mCols]) {
		std::copy(n.mData.get(), n.mData.get() + mRows*mCols, mData.get());
    }

	Matrix<T> operator=(const Matrix<T>& n) {
		if (this != &n) {
			mRows = n.mRows;
			mCols = n.mCols;
			mData.reset(new T[mRows * mCols]);
			std::copy(n.mData.get(), n.mData.get() + mRows*mCols, mData.get());
		}
		return *this;
	}

          T* operator[](size_t r)       { return &mData.get()[r * mCols]; }
    const T* operator[](size_t r) const { return &mData.get()[r * mCols]; }

    size_t rows() const { return mRows; }
    size_t cols() const { return mCols; }
};

template<typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& m) {
    auto width = os.width(); // save current width to apply to all elements
    for (size_t r = 0; r < m.rows(); ++r)
    {
        for (size_t c = 0; c < m.cols(); ++c)
            os << std::setw(width) << m[r][c] << ' ';
        os << '\n';
    }
    return os;
}

int main()
{
    Matrix<int> m(5, 10), n(1, 1);

    for (size_t r = 0; r != m.rows(); ++r) {
        for (size_t c = 0; c != m.cols(); ++c)
            m[r][c] = int(c+1) * int(r+1);
	}

	n = m;
	Matrix<int> o(n);
    std::cout << std::setw(3) << o;
}
