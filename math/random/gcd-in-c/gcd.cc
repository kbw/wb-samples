#include <algorithm>

#include <stdio.h>
#include <stdlib.h>

int gcd(int a, int b)
{
	div_t question { std::max(a, b), std::min(a, b) };

	for (;;) {
		div_t answer = div(question.quot, question.rem);
		if (answer.rem == 0)
			return question.rem;

		question.quot = question.rem;
		question.rem = answer.rem;
	}
}

int main(int argc, char* argv[])
{
	if (argc < 3)
		return 1;

	int a = atoi( argv[1] );
	int b = atoi( argv[2] );

	printf("%d\n", gcd(a, b));

	return 0;
}
