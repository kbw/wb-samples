#include <stdio.h>
#include <stdlib.h>

int gcd(int a, int b)
{
	div_t question;
	if (a > b) {
		question.quot = a;
		question.rem  = b;
	}
	else {
		question.rem  = a;
		question.quot = b;
	}

	for (;;) {
		div_t answer = div(question.quot, question.rem);
		if (answer.rem == 0)
			return question.rem;

		question.quot = question.rem;
		question.rem = answer.rem;
	}
}

int main(int argc, char* argv[])
{
	int a, b;

	if (argc < 3)
		return 1;

	a = atoi( argv[1] );
	b = atoi( argv[2] );

	printf("%d\n", gcd(a, b));

	return 0;
}
