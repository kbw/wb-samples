#include "processor.hpp"
#include "process_base.hpp"

#include <iostream>

namespace ACT
{
	//---------------------------------------------------------------------------

	Actor::Actor() :
		m_workQ_done(false),
		m_workQ_runner(&Actor::run, this)
	{

	}

	Actor::~Actor()
	{
		m_workQ_done = true;

		while (!m_workQ.empty())
		{
			Message* msg = nullptr;
			m_workQ.wait_and_pop(msg);
			delete msg;
		}

		m_workQ_runner.join();
	}

	Ack* Actor::post_message(Message* msg)
	{
		if (m_workQ_done)
			return nullptr;

		if (!msg)
			return nullptr;

		m_workQ.push(msg);
		return msg->ack();
	}

	Reply* Actor::lookup_reply(sessionid_t sessionid)
	{
		Reply* reply = nullptr;

		// Fetch the reply
		resultQ_t::const_iterator p = m_resultQ.end();
		{
			std::lock_guard<std::mutex>	lock(m_resultQ_mtx);
			p = m_resultQ.find(sessionid);
		}

		// Do we have a reply?
		if (p != m_resultQ.end())
		{
			// get the reply object and poll for complete
			reply = p->second;

			// is task complete?
			if (reply->getState() == Reply::State::eDone)
			{
				// remove from container
				std::lock_guard<std::mutex>	lock(m_resultQ_mtx);
				m_resultQ.erase(p);
			}
			else
			{
				// leave task on the queue, return null
				reply = nullptr;
			}
		}

		return reply;
	}

	void Actor::run()
	{
		while (!m_workQ_done)
		{
			Message* msg = nullptr;
			m_workQ.wait_and_pop(msg);

			Reply* reply = msg->process();

			// insert into container
			{
				std::lock_guard<std::mutex>	lock(m_resultQ_mtx);
				m_resultQ.emplace(msg->getSessionId(), reply);
			}
			delete msg;
		}
	}

	//---------------------------------------------------------------------------

	// synchronous initiate request
	Ack* Processor::Call(Request* pRequest)
	{
		// push onto work Q
		return engine.post_message(pRequest);
	}

	// check for done
	Reply* Processor::Poll(sessionid_t sessionid)
	{
		// check result Q
		return engine.lookup_reply(sessionid);
	}
}
