#ifndef ACT_PROCESS_BASE_HPP
#define ACT_PROCESS_BASE_HPP

#include <cstdint>
#include <mutex>
#include <map>

//---------------------------------------------------------------------------

namespace ACT
{
	class Ack;
	class Request;
	class Reply;

	typedef std::uint64_t sessionid_t;
	typedef std::map<sessionid_t, Reply*> ReplyQ;

	//-----------------------------------------------------------------------

	class Ack
	{
	public:
		enum State { eOk, eFail, eUnknown };

		Ack(sessionid_t sessionid, State state);
		virtual ~Ack();

		virtual sessionid_t	getSessionId() const;
		virtual State	getState();

	private:
		sessionid_t	m_sessionid;
		State		m_state;
	};

	class Reply
	{
	public:
		enum State { eDone, eWait, eUnknown };

		Reply(sessionid_t sessionid);
		virtual ~Reply();

		virtual	sessionid_t	getSessionId() const;
		virtual State 	getState();
		virtual void	setState(State state = eDone);

	private:
		sessionid_t	m_sessionid;
		State		m_state;
	};

	class Request
	{
	public:
		Request();
		virtual ~Request();

		virtual Ack*	ack();
		virtual Reply*	process();
		virtual	ReplyQ*	getReplyQueue();

		virtual	sessionid_t	getSessionId() const;

	private:
		class SessionIdMgr
		{
		public:
			SessionIdMgr();

			std::mutex&		getMutex();
			sessionid_t&	getSessionid();

		private:
			std::mutex	m_mtx;
			sessionid_t	m_sessionid;
		};

	private:
		sessionid_t	getNextSessionId(SessionIdMgr& mgr);

	private:
		sessionid_t	m_sessionid;

		static SessionIdMgr	m_sessionmgr;
	};
}

//---------------------------------------------------------------------------

#endif	// ACT_PROCESS_BASE_HPP
