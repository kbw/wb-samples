#include "process_base.hpp"

//---------------------------------------------------------------------------

namespace ACT
{
	Request::SessionIdMgr Request::m_sessionmgr;

	//-----------------------------------------------------------------------

	Ack::Ack(sessionid_t sessionid, State state) :
		m_sessionid(sessionid),
		m_state(state)
	{
	}

	Ack::~Ack()
	{
	}

	sessionid_t	Ack::getSessionId() const
	{
		return m_sessionid;
	}

	Ack::State Ack::getState()
	{
		return m_state;
	}

	//-----------------------------------------------------------------------

	Reply::Reply(sessionid_t sessionid) :
		m_sessionid(sessionid),
		m_state(eDone)
	{
	}

	Reply::~Reply()
	{
	}

	sessionid_t	Reply::getSessionId() const
	{
		return m_sessionid;
	}

	Reply::State Reply::getState()
	{
		return m_state;
	}

	void Reply::setState(State state)
	{
		m_state = state;
	}

	//-----------------------------------------------------------------------

	Request::Request() :
		m_sessionid(getNextSessionId(m_sessionmgr))
	{
	}

	Request::~Request()
	{
	}

	Ack* Request::ack()
	{
		return nullptr;
	}

	Reply* Request::process()
	{
		return nullptr;
	}

	ReplyQ*	Request::getReplyQueue()
	{
		return nullptr;
	}

	sessionid_t	Request::getSessionId() const
	{
		return m_sessionid;
	}

	sessionid_t	Request::getNextSessionId(SessionIdMgr& mgr)
	{
		std::lock_guard<std::mutex> lock(mgr.getMutex());
		return ++mgr.getSessionid();
	}

	//-----------------------------------------------------------------------

	Request::SessionIdMgr::SessionIdMgr() :
		m_sessionid(0)
	{
	}

	std::mutex& Request::SessionIdMgr::getMutex()
	{
		return m_mtx;
	}

	sessionid_t& Request::SessionIdMgr::getSessionid()
	{
		return m_sessionid;
	}
}

