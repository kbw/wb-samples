#ifndef ACT_PROCESSOR_HPP
#define ACT_PROCESSOR_HPP

#include "process_base.hpp"
#include "actor/cc_queue.hpp"
#include <Wt/Http/Request>

#include <mutex>
#include <thread>
#include <map>

namespace ACT
{
	//---------------------------------------------------------------------------

	struct Context
	{
		Context() :
			sessionid(0)
		{
		}

		ACT::sessionid_t sessionid;
	};

	inline bool getParam(const Wt::Http::ParameterMap &params, const std::string& key, std::string& value)
	{
		Wt::Http::ParameterMap::const_iterator p = params.find(key);
		if (p != params.end())
		{
			for (const std::string &s : p->second)
			{
				value = s;
				return true;
			}
		}

		return false;
	}

	inline bool getParam(const Wt::Http::ParameterMap &params, const std::string& key, std::vector<std::string>& values)
	{
		Wt::Http::ParameterMap::const_iterator p = params.find(key);
		if (p != params.end())
		{
			if (!p->second.empty())
			{
				std::copy(p->second.begin(), p->second.end(), std::back_inserter(values));
				return true;
			}
		}

		return false;
	}

	inline bool getContext(const Wt::Http::Request &request, Context &ctx)
	{
		std::string str_sessionid;
		if (getParam(request.getParameterMap(), "sessionid", str_sessionid))
		{
			ctx.sessionid = std::stoull(str_sessionid);
			if (ctx.sessionid != 0)
			{
				return true;
			}
		}

		return false;
	}

	//---------------------------------------------------------------------------

	//	This is the unit of work processing.  It contains:
	//	-	work queue
	//	-	result queue
	//	-	asynchronous sequential work runner
	class Actor
	{
	public:
		typedef Request	Message;

		Actor();
		virtual	~Actor();
		virtual	Ack*	post_message(Message* msg);
		virtual	Reply*	lookup_reply(sessionid_t sessionid);

	private:
		typedef	std::map<sessionid_t, Reply*> resultQ_t;
		void	run();

		bool		m_workQ_done;
		std::thread	m_workQ_runner;
		concurrent_queue<Message*>	m_workQ;

		resultQ_t	m_resultQ;
		std::mutex	m_resultQ_mtx;
	};

	//---------------------------------------------------------------------------

	class Processor
	{
	public:
		Processor() = default;
		~Processor() = default;
		Processor(const Processor &) = delete;
		Processor& operator=(const Processor &) = delete;

		Ack*	Call(Request* pRequest);	// synchronous initiate request
		Reply*	Poll(sessionid_t id);		// check for done

	private:
		Actor	engine;
	};
}

#endif	// ACT_PROCESSOR_HPP
