#ifndef CC_QUEUE_HPP
#define CC_QUEUE_HPP

#include <mutex>
#include <condition_variable>
#include <thread>

#include <queue>

template<typename Data>
class concurrent_queue
{
    private:
        std::queue<Data> 	m_queue;
        mutable std::mutex 	m_mutex;
        std::condition_variable m_condition_variable;
        typedef std::unique_lock<std::mutex> scoped_mutex_t;

    public:
        void push(Data const& data)
        {
            scoped_mutex_t lock(m_mutex);
            m_queue.push(data);
            lock.unlock();
            m_condition_variable.notify_one();
        }

        bool empty() const
        {
            scoped_mutex_t lock(m_mutex);
            return m_queue.empty();
        }

        bool try_pop(Data& popped_value)
        {
            scoped_mutex_t lock(m_mutex);
            if(m_queue.empty())
            {
                return false;
            }

            popped_value=m_queue.front();
            m_queue.pop();
            return true;
        }

        void wait_and_pop(Data& popped_value)
        {
            scoped_mutex_t lock(m_mutex);
            while(m_queue.empty())
            {
                m_condition_variable.wait(lock);
            }

            popped_value=m_queue.front();
            m_queue.pop();
        }
};
#endif /* CC_QUEUE_HPP */
