#include "execute/request.hpp"
#include "execute/reply.hpp"
#include "config/request.hpp"
#include "config/reply.hpp"

#include <Wt/WApplication>
#include <Wt/WEnvironment>
#include <Wt/WResource>
#include <Wt/WServer>

#include <iostream>

//---------------------------------------------------------------------------

int main(int argc, char **argv)
{
	try
	{
		Wt::WServer server(argv[0]);
		server.setServerConfiguration(argc, argv, WTHTTP_CONFIGURATION);

		execute::Request execute;
		execute::Reply	execute_reply;
		server.addResource(&execute,		"/execute/");
		server.addResource(&execute_reply, 	"/execute_reply/");

		config::Request	config;
		config::Reply	config_reply;
		server.addResource(&config,			"/config/");
		server.addResource(&config_reply,	"/config_reply/");

		if (server.start())
		{
			Wt::WServer::waitForShutdown();
			server.stop();
		}
	}
	catch (const Wt::WServer::Exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	catch (const std::exception &e)
	{
		std::cerr << "exception: " << e.what() << std::endl;
	}
}
