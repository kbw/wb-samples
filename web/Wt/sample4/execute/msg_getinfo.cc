#include "msg_getinfo.hpp"
#include "config/config.hpp"
#include "config/msg_time.hpp"
#include "../utils.hpp"

#include <strings.h>
#include <sys/time.h>
#include <time.h>

namespace execute
{
	//-----------------------------------------------------------------------

	Ack::Ack(ACT::sessionid_t sessionid, ACT::Ack::State state) :
		ACT::Ack(sessionid, state)
	{
	}

	GetInfoReply::GetInfoReply(
			ACT::sessionid_t sessionid,
			URL::Runner* runner,
			const Payload &payload) :
		ACT::Reply(sessionid),
		timeconfig_sessionid(0),
		runner(runner),
		payload(payload)
	{
	}

	GetInfoReply::GetInfoReply(
			ACT::sessionid_t sessionid,
			ACT::sessionid_t timeconfig_sessionid,
			const Payload &payload) :
		ACT::Reply(sessionid),
		timeconfig_sessionid(timeconfig_sessionid),
		runner(nullptr),
		payload(payload)
	{
	}

	ACT::Reply::State GetInfoReply::getState()
	{
		if (runner)
		{
			if (runner->context().ready)
			{
				payload.hostname = "shoud be set in GetInfoReply::getState()";
				payload.config_time = "shoud be set in GetInfoReply::getState()";
				return eDone;
			}
		}
		else
		{
			// check if done
			if (config::TimeReply* reply = static_cast<config::TimeReply*>(config::processor.Poll(timeconfig_sessionid)))
			{
				payload.hostname = reply->payload.hostname;
				payload.config_time = reply->payload.current_time;
				delete reply;

				return eDone;
			}
		}

		return eWait;
	}

	GetInfoRequest::GetInfoRequest(int scriptid, Wt::WObject* wtobj) :
		scriptid(scriptid),
		wtobj(wtobj)
	{
		char n[64];

		// get time
		struct timeval tv;
		bzero(&tv, sizeof(tv));
		gettimeofday(&tv, nullptr);		// get time
		struct tm trec;
		localtime_r(&tv.tv_sec, &trec);	// convert to text
		std::snprintf(n, sizeof(n), "%02d:%02d:%02d.%06ld", trec.tm_hour, trec.tm_min, trec.tm_sec, tv.tv_usec);
		payload.request_time = n;
	}

	ACT::Ack* GetInfoRequest::ack()
	{
		return new Ack(getSessionId(), ACT::Ack::eOk);
	}

	ACT::Reply* GetInfoRequest::process()
	{
		char n[64];

		// get time
		struct timeval tv;
		bzero(&tv, sizeof(tv));
		gettimeofday(&tv, nullptr);		// get time
		struct tm trec;
		localtime_r(&tv.tv_sec, &trec);	// convert to text
		std::snprintf(n, sizeof(n), "%02d:%02d:%02d.%06ld", trec.tm_hour, trec.tm_min, trec.tm_sec, tv.tv_usec);
		payload.current_time = n;

		if (wtobj)
		{
			URL::Runner* runner = new URL::Runner;
			runner->run(wtobj, "http://localhost:8082/config/");
			return new GetInfoReply(getSessionId(), runner, payload);
		}
		else
		{
			// Push work (TimeRequest) onto Config queue
			std::unique_ptr<ACT::Ack> ack(config::processor.Call(new config::TimeRequest));
			return new GetInfoReply(getSessionId(), ack->getSessionId(), payload);
		}
	}
}
