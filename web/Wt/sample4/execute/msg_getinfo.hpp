#ifndef EXECUTE_MSG_GETINFO_HPP
#define EXECUTE_MSG_GETINFO_HPP

#include "utils.hpp"
#include "ACT/process_base.hpp"
#include <Wt/Http/Client>

//---------------------------------------------------------------------------

namespace execute
{
	//-----------------------------------------------------------------------
	struct Payload
	{
		std::string	hostname;
		std::string	config_time;
		std::string	current_time;
		std::string	request_time;
	};

	//-----------------------------------------------------------------------

	class Ack : public ACT::Ack
	{
	public:
		Ack(ACT::sessionid_t sessionid, ACT::Ack::State state);
	};

	class GetInfoReply : public ACT::Reply
	{
	public:
		GetInfoReply(						// handle REST replies
				ACT::sessionid_t sessionid,
				URL::Runner* runner,
				const Payload &payload);
		GetInfoReply(						// handle internal replies
				ACT::sessionid_t sessionid,
				ACT::sessionid_t timeconfig_sessionid,
				const Payload &payload);

		virtual	ACT::Reply::State getState();

		ACT::sessionid_t timeconfig_sessionid;
		URL::Runner* runner;
		Payload	payload;
	};

	class GetInfoRequest : public ACT::Request
	{
	public:
		GetInfoRequest(int scriptid, Wt::WObject* wtobj = nullptr);

		virtual ACT::Ack*	ack();
		virtual ACT::Reply*	process();

		int	scriptid;
		Wt::WObject* wtobj;
		Payload payload;
	};
}

//---------------------------------------------------------------------------

#endif	// EXECUTE_MSG_GETINFO_HPP
