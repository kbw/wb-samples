#ifndef EXECUTE_REPLY_HPP
#define EXECUTE_REPLY_HPP

#include <Wt/WResource>
#include <boost/system/error_code.hpp>

namespace Wt {
namespace Http {
	class Request;
	class Response;
} }

//---------------------------------------------------------------------------

namespace execute
{
	class Reply : public Wt::WResource
	{
	public:
		Reply();

		int getStatus() const;
		void setStatus(int status);

	protected:
		virtual void handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response);

	private:
		int m_status;
	};
}

#endif	// EXECUTE_REPLY_HPP
