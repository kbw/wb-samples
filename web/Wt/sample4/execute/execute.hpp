#ifndef EXECUTE_HPP
#define EXECUTE_HPP

#include "ACT/processor.hpp"

//---------------------------------------------------------------------------

namespace execute
{
	extern	ACT::Processor	processor;	
};

#endif	// EXECUTE_HPP
