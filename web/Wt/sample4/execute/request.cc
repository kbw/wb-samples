#include "request.hpp"
#include "execute.hpp"
#include "msg_getinfo.hpp"

#include <Wt/Http/Request>
#include <Wt/Http/Response>
#include <Wt/Http/Message>
#include <Wt/Http/Client>

//---------------------------------------------------------------------------

namespace execute
{
	Request::Request() :
		m_status(500)
	{
	}

	int Request::getStatus() const
	{
		return m_status;
	}

	void Request::setStatus(int value)
	{
		m_status = value;
	}

	void Request::handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response)
	{
		std::cout << "execute::Request::handleRequest" << std::endl;

		// Initially, we do nothing.
		setStatus(204);

		try
		{
			std::unique_ptr<ACT::Ack> ack(execute::processor.Call(new execute::GetInfoRequest(2, this)));
			if (ack)
				response.out() << "http://localhost:8082/execute_reply/?sessionid=" << ack->getSessionId();
			setStatus(200);
		}
		catch (const std::exception &e)
		{
			setStatus(500);
			response.out() << "Unhandled exception in server: " << e.what();
		}
	}
}
