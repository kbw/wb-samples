#include "utils.hpp"

#include <Wt/Http/Client>

namespace URL
{
	void Runner::runhelper(boost::system::error_code err, const Wt::Http::Message& response, Result &result)
	{
		std::cout << "Runner::runhelper : err=" << err << " status=" << response.status() << std::endl;

		result.err = err;
		result.status = response.status();
		result.content = response.body();

		std::cout << "Runner::runhelper : response.body=" << result.content << std::endl;

		{
			std::lock_guard<std::mutex> lock(ctx.m);
			ctx.ready = true;
		}
		ctx.cv.notify_one();
	}

	Result Runner::run(Wt::WObject *obj, const std::string &url)
	{
		Result result;

		std::cout << "Runner::run : url=" << url << std::endl;

		std::unique_ptr<Wt::Http::Client> client(new Wt::Http::Client(obj));
		client->setTimeout(5);
		client->setMaximumResponseSize(10 * 1024);
		client->done().connect(boost::bind(&Runner::runhelper, this, _1, _2, boost::ref(result)));

		if (client->get(url))
		{
			class Local
			{
			public:
				Local(Context& ctx) : m_ctx(ctx) {}
				bool operator()() { return m_ctx.ready; }
			private:
				Context& m_ctx;
			};
			Local local(ctx);

			std::unique_lock<std::mutex> lock(ctx.m);
			ctx.cv.wait(lock, local);

			std::cout << "Runner::run : status=" << result.status << " content=" << result.content << std::endl;
		}
		else
		{
			// in case of an error in the %URL
			result.status = 500;
			result.content = "content=URL error";
			std::cout << "Runner::run : status=" << result.status << " content=" << result.content << std::endl;
		}

		return result;
	}
}
