#ifndef CONFIG_REQUEST_HPP
#define CONFIG_REQUEST_HPP

#include <Wt/WResource>
#include <boost/system/error_code.hpp>

namespace Wt {
namespace Http {
	class Request;
	class Response;
} }

//---------------------------------------------------------------------------

namespace config
{
	class Request : public Wt::WResource
	{
	public:
		Request();

		int getStatus() const;
		void setStatus(int status);

	protected:
		virtual void handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response);

	private:
		int m_status;
	};
}

#endif	// CONFIG_REQUEST_HPP
