#include "msg_time.hpp"

#include <unistd.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <cstdio>

namespace config
{
	//-----------------------------------------------------------------------

	Ack::Ack(ACT::sessionid_t sessionid, ACT::Ack::State state) :
		ACT::Ack(sessionid, state)
	{
	}

	TimeReply::TimeReply(ACT::sessionid_t sessionid, const Payload &payload) :
		ACT::Reply(sessionid),
		payload(payload)
	{
	}

	TimeRequest::TimeRequest()
	{
	}

	ACT::Ack* TimeRequest::ack()
	{
		return new Ack(getSessionId(), ACT::Ack::eOk);
	}

	ACT::Reply* TimeRequest::process()
	{
		config::Payload payload;

		char n[64];

		// get hostname
		bzero(n, sizeof(n));
		gethostname(n, sizeof(n));
		payload.hostname = n;

		// get time
		struct timeval tv;
		bzero(&tv, sizeof(tv));
		gettimeofday(&tv, nullptr);		// get time
		struct tm trec;
		localtime_r(&tv.tv_sec, &trec);	// convert to text
		std::snprintf(n, sizeof(n), "%02d:%02d:%02d.%06ld", trec.tm_hour, trec.tm_min, trec.tm_sec, tv.tv_usec);
		payload.current_time = n;

		return new TimeReply(getSessionId(), payload);
	}
}
