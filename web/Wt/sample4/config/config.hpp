#ifndef CONFIG_HPP
#define CONFIG_HPP

#include "ACT/processor.hpp"

//---------------------------------------------------------------------------

namespace config
{
	extern	ACT::Processor	processor;	
};

#endif	// CONFIG_HPP
