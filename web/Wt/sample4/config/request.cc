#include "request.hpp"
#include "config.hpp"
#include "msg_time.hpp"

#include <Wt/Http/Request>
#include <Wt/Http/Response>
#include <Wt/Http/Message>
#include <Wt/Http/Client>

//---------------------------------------------------------------------------

namespace config
{
	Request::Request() :
		m_status(500)
	{
	}

	int Request::getStatus() const
	{
		return m_status;
	}

	void Request::setStatus(int value)
	{
		m_status = value;
	}

	void Request::handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response)
	{
		std::cout << "config::Request::handleRequest" << std::endl;

		// Initially, we do nothing.
		setStatus(204);

		try
		{
			std::unique_ptr<ACT::Ack> ack(config::processor.Call(new config::TimeRequest));
			if (ack)
				response.out() << "http://localhost:8082/config_reply/?sessionid=" << ack->getSessionId();
			setStatus(200);
		}
		catch (const std::exception &e)
		{
			setStatus(500);
			response.out() << "Unhandled exception in server: " << e.what();
		}
	}
}
