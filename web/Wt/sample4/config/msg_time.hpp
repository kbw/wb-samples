#ifndef CONFIG_MSG_TIME_HPP
#define CONFIG_MSG_TIME_HPP

#include "ACT/process_base.hpp"

//---------------------------------------------------------------------------

namespace config
{
	//-----------------------------------------------------------------------
	struct Payload
	{
		std::string	hostname;
		std::string	current_time;
	};

	//-----------------------------------------------------------------------

	class Ack : public ACT::Ack
	{
	public:
		Ack(ACT::sessionid_t sessionid, ACT::Ack::State state);
	};

	class TimeReply : public ACT::Reply
	{
	public:
		TimeReply(ACT::sessionid_t sessionid, const Payload &payload);

		Payload	payload;
	};

	class TimeRequest : public ACT::Request
	{
	public:
		TimeRequest();

		virtual ACT::Ack*	ack();
		virtual ACT::Reply*	process();
	};
}

//---------------------------------------------------------------------------

#endif	// CONFIG_MSG_TIME_HPP
