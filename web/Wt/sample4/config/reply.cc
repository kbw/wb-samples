#include "reply.hpp"
#include "config.hpp"
#include "msg_time.hpp"

#include <Wt/Http/Request>
#include <Wt/Http/Response>
#include <Wt/Http/Message>
#include <Wt/Http/Client>

//---------------------------------------------------------------------------

namespace config
{
	Reply::Reply() :
		m_status(500)
	{
	}

	int Reply::getStatus() const
	{
		return m_status;
	}

	void Reply::setStatus(int value)
	{
		m_status = value;
	}

	void Reply::handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response)
	{
		std::cout << "config::Reply::handleReply" << std::endl;

		// Initially, we do nothing.
		setStatus(204);

		try
		{
			ACT::Context ctx;
			if (!getContext(request, ctx))
			{
				setStatus(200);
				response.out() << "URL does not have sessionid";
				return;
			}

			if (config::TimeReply* reply = static_cast<config::TimeReply*>(processor.Poll(ctx.sessionid)))
			{
				setStatus(200);
				response.out() << "config"
					<< "\n\tcurrent_time: " << reply->payload.current_time
					<< "\n\thostname: " << reply->payload.hostname;

				delete reply;
				return;
			}

			setStatus(200);
			response.out() << "still processing sessionid=" << ctx.sessionid;
		}
		catch (const std::exception &e)
		{
			setStatus(500);
			response.out() << "Unhandled exception in server: " << e.what();
		}
	}
}
