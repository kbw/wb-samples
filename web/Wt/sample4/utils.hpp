#ifndef	UTILS_HPP
#define	UTILS_HPP

#include <boost/system/error_code.hpp>
#include <Wt/Http/Message>
#include <Wt/WObject>

#include <string>
#include <mutex>
#include <condition_variable>

namespace URL
{
	struct Context
	{
		Context() : ready(false) {}
		Context(const Context &) = delete;
		Context& operator=(const Context &) = delete;

		std::mutex m;
		std::condition_variable cv;
		bool ready;
	};

	struct Result
	{
		Result() : status(0) {}

		boost::system::error_code err;
		int status;
		std::string content;
	};

	class Runner
	{
		Context ctx;
		void	runhelper(boost::system::error_code err, const Wt::Http::Message& response, Result &result);

	public:
		Result	run(Wt::WObject *obj, const std::string &url);
		const Context& context() const { return ctx; }
	};
}

#endif	// UTILS_HPP
