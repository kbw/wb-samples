/*
 * Copyright (C) 2008 Emweb bvba, Heverlee, Belgium.
 *
 * See the LICENSE file for terms of use.
 */
#include <Wt/WApplication>
#include <Wt/WEnvironment>
#include <Wt/WResource>
#include <Wt/WServer>
#include <iostream>

//---------------------------------------------------------------------------

/*
 * A simple hello world application class which demonstrates how to react
 * to events, read input, and give feed-back.
 */
class HelloApplication : public Wt::WApplication
{
public:
	HelloApplication(const Wt::WEnvironment& env);
};

/*
 * The env argument contains information about the new session, and
 * the initial request. It must be passed to the WApplication
 * constructor so it is typically also an argument for your custom
 * application constructor.
 */
HelloApplication::HelloApplication(const Wt::WEnvironment& env) :
	Wt::WApplication(env)
{
}

Wt::WApplication *createApplication(const Wt::WEnvironment& env)
{
	/*
	 * You could read information from the environment to decide whether
	 * the user has permission to start a new application
	 */
	return new HelloApplication(env);
}

//---------------------------------------------------------------------------

struct Car : public Wt::WResource
{
protected:
	virtual void handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response);
};

void Car::handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response)
{
	std::cout << "Car::handleRequest"<< std::endl;

	std::cout << "\t" << request.method() << " from " << request.clientAddress() << std::endl;
	std::cout << "\tUser Agent = \"" << request.userAgent() << "\"" << std::endl;

	const std::string &cookies = request.headerValue("Cookie");
	if (!cookies.empty())
	{
		std::cout << "\tCookies = " << cookies << std::endl;
	}

	if (!request.cookies().empty())
	{
		std::cout << "\tn cookies = " << request.cookies().size() << std::endl;
		for (const auto &cookie : request.cookies())
			std::cout << "\t\t\"" << cookie.first << "\" = \"" << cookie.second << "\"" << std::endl;
	}

	if (!request.queryString().empty())
	{
		std::cout << "\tQuery String = \"" << request.queryString() << "\"" << std::endl;
		for (const auto &entry : request.getParameterMap())
		{
			std::cout << "\t\tkey = \"" << entry.first << "\" value =";
			for (const auto &value : entry.second)
				std::cout << " \"" << value << "\"";
			std::cout << std::endl;
		}
	}
}

//---------------------------------------------------------------------------

int main(int argc, char **argv)
{
	try
	{
		Car car;

		Wt::WServer server(argv[0]);
		server.setServerConfiguration(argc, argv, WTHTTP_CONFIGURATION);
		server.addResource(&car, "/car/");

		if (server.start())
		{
			Wt::WServer::waitForShutdown();
			server.stop();
		}

		/*
		 * Your main method may set up some shared resources, but should then
		 * start the server application (FastCGI or httpd) that starts listening
		 * for requests, and handles all of the application life cycles.
		 *
		 * The last argument to WRun specifies the function that will instantiate
		 * new application objects. That function is executed when a new user surfs
		 * to the Wt application, and after the library has negotiated browser
		 * support. The function should return a newly instantiated application
		 * object.
		return WRun(argc, argv, &createApplication);
		 */
	}
	catch (const Wt::WServer::Exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	catch (const std::exception &e)
	{
		std::cerr << "exception: " << e.what() << std::endl;
	}
}
