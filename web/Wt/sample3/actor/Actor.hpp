#ifndef ACTOR_HPP
#define ACTOR_HPP

#include "cc_queue.hpp"
#include <functional>

class Actor
{
	typedef std::function<void()>  Message;

	bool		m_done;
	std::thread	m_thread;
	concurrent_queue<Message>	m_messages;

	void run()
	{
		while( !m_done )
		{
			Message msg;
			m_messages.wait_and_pop(msg);
			msg();
		}
	}

public:
	Actor():
		m_done(false),
		m_thread(&Actor::run,this)
	{
	
	}

	void post_message( Message m )
	{
		m_messages.push(m);
	}

	virtual ~Actor()
	{
		post_message([&]{ 
			m_done = true; 
		});
		m_thread.join();
	}
};

#endif /* ACTOR_HPP */
