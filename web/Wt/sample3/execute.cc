#include "execute.hpp"
#include "common.hpp"

#include <Wt/Http/Request>
#include <Wt/Http/Response>
#include <Wt/Http/Message>
#include <Wt/Http/Client>

#include <cstdint>
#include <string>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <iostream>

//---------------------------------------------------------------------------

ACT::Actor* Execute::sm_worker = new ACT::Actor();

ACT::Actor* Execute::getWorker()
{
	return sm_worker;
}

//---------------------------------------------------------------------------

Execute::Execute() :
	m_status(500),
	m_sessionid(0)
{
}

int Execute::getStatus() const
{
	return m_status;
}

void Execute::setStatus(int value)
{
	m_status = value;
}

void Execute::handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response)
{
	std::cout << "Execute::handleRequest" << std::endl;

	setStatus(500);

	try
	{
		ACT::sessionid_t sessionid = getSessionId(request.getParameterMap());
		if (sessionid != 0)
		{
			const ACT::ReplyQ& replies = ACT::ExecuteRequest::replyQueue();
			ACT::ReplyQ::const_iterator p = replies.find(sessionid);
			if (p != replies.end())
			{
				if (ACT::ExecuteReply* reply = static_cast<ACT::ExecuteReply*>(p->second))
				{
					response.out() << "Processed, sessionid=" << reply->getSessionId();
					delete p->second;
				}
			}

			std::cout << "Execute::handleRequest : process reply done" << std::endl << std::endl;
			return;
		}

		const std::string& scriptid = getValue(request.getParameterMap(), "scriptid");

 		if (ACT::Ack* ack = Execute::getWorker()->post_message(new ACT::ExecuteRequest(std::stoi(scriptid))))
		{
			switch (ack->getState())
			{
			case ACT::Ack::eOk:
				setStatus(200);
				response.out() << "Submitted job start request, sessionid=" << ack->getSessionId();
				break;

			default:
				response.out() << "Job request failed";
			}
		}
		else
			response.out() << "Job control system is down";
	}
	catch (const std::exception &e)
	{
		response.out() << "execute failed";
	}

	// Initially, we do nothing.
	std::cout << "Execute::handleRequest : done" << std::endl << std::endl;
}
