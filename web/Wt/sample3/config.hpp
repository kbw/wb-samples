#ifndef CONFIG_HPP
#define CONFIG_HPP

#include "worker.hpp"

#include <Wt/WResource>
#include <boost/system/error_code.hpp>

namespace Wt {
namespace Http {
	class Request;
	class Response;
} }

//---------------------------------------------------------------------------

class Config : public Wt::WResource
{
public:
	Config();

	int getStatus() const;
	void setStatus(int status);

	static	ACT::Actor* getWorker();

protected:
	virtual void handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response);

private:
	int m_status;
	int m_sessionid;

	static	ACT::Actor* sm_worker;
};

#endif	// CONFIG_HPP
