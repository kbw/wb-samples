#ifndef COMMON_HPP
#define COMMON_HPP

#include "worker.hpp"
#include <Wt/Http/Request>
#include <map>

inline ACT::sessionid_t getSessionId(const Wt::Http::ParameterMap &params)
{
	auto p = params.find("sessionid");
	if (p != params.end())
	{
		for (const auto &s : p->second)
		{
			return atoi(s.c_str());
		}
	}

	return 0;
}

inline const std::string& getValue(const Wt::Http::ParameterMap &params, const std::string& key)
{
	auto p = params.find(key);
	if (p != params.end())
	{
		for (const auto &s : p->second)
		{
			return s;
		}
	}

	static const std::string empty_s;
	return empty_s;;
}

inline bool getReply(ACT::sessionid_t sessionid, ACT::ConfigReply* reply)
{
	reply = nullptr;

	const ACT::ReplyQ& replies = ACT::ConfigRequest::replyQueue();
	ACT::ReplyQ::const_iterator p = replies.find(sessionid);
	if (p != replies.end())
		reply = static_cast<ACT::ConfigReply*>(p->second);

	return reply != nullptr;
}

inline bool getReply(ACT::sessionid_t sessionid, ACT::ExecuteReply* reply)
{
	reply = nullptr;

	const ACT::ReplyQ& replies = ACT::ExecuteRequest::replyQueue();
	ACT::ReplyQ::const_iterator p = replies.find(sessionid);
	if (p != replies.end())
		reply = static_cast<ACT::ExecuteReply*>(p->second);

	return reply != nullptr;
}

#endif	// COMMON_HPP
