#ifndef WORKER_HPP
#define WORKER_HPP

#include "actor/Actor.hpp"
#include <map>

//---------------------------------------------------------------------------

namespace ACT
{
	class Ack;
	class Request;
	class Reply;

	typedef std::uint64_t sessionid_t;
	typedef std::map<sessionid_t, Reply*> ReplyQ;

	class SessionIdMgr
	{
	public:
		SessionIdMgr();

		std::mutex&		getMutex();
		sessionid_t&	getSessionid();

	private:
		std::mutex	m_mtx;
		sessionid_t	m_sessionid;
	};

	//-----------------------------------------------------------------------

	class Ack
	{
	public:
		enum State { eOk, eFail, eUnknown };

		Ack(sessionid_t sessionid, State state);
		virtual ~Ack();

		virtual sessionid_t	getSessionId() const;
		virtual State getState() const;

	private:
		sessionid_t	m_sessionid;
		State		m_state;
	};

	class Reply
	{
	public:
		Reply(sessionid_t sessionid);
		virtual ~Reply();

		virtual	sessionid_t	getSessionId() const;

	private:
		sessionid_t	m_sessionid;
	};

	class Request
	{
	public:
		Request();
		virtual ~Request();

		virtual Ack*	ack();
		virtual Reply*	process();
		virtual	ReplyQ*	getReplyQueue();

		virtual	sessionid_t	getSessionId() const;

	private:
		sessionid_t	getNextSessionId(SessionIdMgr& mgr);

	private:
		sessionid_t	m_sessionid;
		static SessionIdMgr	m_sessionmgr;
	};

	//-----------------------------------------------------------------------

	class ExectueAck : public Ack
	{
	public:
		ExectueAck(sessionid_t sessionid, State state);
	};

	class ExecuteReply : public Reply
	{
	public:
		ExecuteReply(sessionid_t sessionid);
	};

	class ExecuteRequest : public Request
	{
	public:
		ExecuteRequest(int scriptid);

		virtual Ack*	ack();
		virtual Reply*	process();
		virtual	ReplyQ*	getReplyQueue();

		int				getScriptid() const;

		static const ReplyQ& replyQueue();

	private:
		int	m_scriptid;
		static ReplyQ sm_replyqueue;
	};

	//-----------------------------------------------------------------------

	class ConfigAck : public Ack
	{
	public:
		ConfigAck(sessionid_t sessionid, State state);
	};

	class ConfigReply : public Reply
	{
	public:
		ConfigReply(sessionid_t sessionid, const std::string& host);
		const std::string& getHostname() const;

	private:
		std::string	m_host;
	};

	class ConfigRequest : public Request
	{
	public:
		ConfigRequest(const std::string &host);

		virtual Ack*	ack();
		virtual Reply*	process();
		virtual	ReplyQ*	getReplyQueue();

		const std::string& getHostname() const;

		static const ReplyQ& replyQueue();

	private:
		std::string	m_host;
		static ReplyQ sm_replyqueue;
	};

	//-----------------------------------------------------------------------

	class Actor
	{
		typedef Request	Message;

		bool		m_done;
		std::thread	m_thread;
		concurrent_queue<Message*>	m_messages;

		void run();

	public:
		Actor();

		Ack* post_message(Message* msg);

		virtual ~Actor();
	};
}

//---------------------------------------------------------------------------

#endif	// WORKER_HPP
