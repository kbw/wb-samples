#include "config.hpp"
#include "common.hpp"

#include <Wt/Http/Request>
#include <Wt/Http/Response>
#include <Wt/Http/Message>
#include <Wt/Http/Client>

#include <cstdint>
#include <string>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <iostream>

//---------------------------------------------------------------------------

ACT::Actor* Config::sm_worker = new ACT::Actor();

ACT::Actor* Config::getWorker()
{
	return sm_worker;
}

//---------------------------------------------------------------------------

Config::Config() :
	m_status(500),
	m_sessionid(0)
{
}

int Config::getStatus() const
{
	return m_status;
}

void Config::setStatus(int value)
{
	m_status = value;
}

void Config::handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response)
{
	std::cout << "Config::handleRequest" << std::endl;

	setStatus(204);

	try
	{
		ACT::sessionid_t sessionid = getSessionId(request.getParameterMap());
		if (sessionid != 0)
		{
			const ACT::ReplyQ& replies = ACT::ConfigRequest::replyQueue();
			ACT::ReplyQ::const_iterator p = replies.find(sessionid);
			if (p != replies.end())
			{
				if (ACT::ConfigReply* reply = static_cast<ACT::ConfigReply*>(p->second))
				{
					response.out() << "Processed, sessionid=" << reply->getSessionId() << ". hostname=" << reply->getHostname();
					delete p->second;
				}
			}

			std::cout << "Config::handleRequest : process reply done" << std::endl << std::endl;
			return;
		}

		char hostname[512];
		gethostname(hostname, sizeof(hostname));

 		if (ACT::Ack* ack = Config::getWorker()->post_message(new ACT::ConfigRequest(hostname)))
		{
			switch (ack->getState())
			{
			case ACT::Ack::eOk:
				setStatus(200);
				response.out() << "Submitted job start request, sessionid=" << ack->getSessionId();
				break;

			default:
				response.out() << "Job request failed";
			}
		}
		else
			response.out() << "Job control system is down";
	}
	catch (const std::exception &e)
	{
		response.out() << "confgi failed";
	}

	// Initially, we do nothing.
	std::cout << "Config::handleRequest : done" << std::endl << std::endl;
}
