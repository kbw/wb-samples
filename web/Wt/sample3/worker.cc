#include "worker.hpp"

//---------------------------------------------------------------------------

namespace ACT
{
	SessionIdMgr Request::m_sessionmgr;
	ReplyQ ExecuteRequest::sm_replyqueue;
	ReplyQ ConfigRequest::sm_replyqueue;

	//-----------------------------------------------------------------------

	SessionIdMgr::SessionIdMgr() :
		m_sessionid(0)
	{
	}

	std::mutex& SessionIdMgr::getMutex()
	{
		return m_mtx;
	}

	sessionid_t& SessionIdMgr::getSessionid()
	{
		return m_sessionid;
	}

	//-----------------------------------------------------------------------

	Ack::Ack(sessionid_t sessionid, State state) :
		m_sessionid(sessionid), m_state(state)
	{
	}

	Ack::~Ack()
	{
	}

	sessionid_t	Ack::getSessionId() const
	{
		return m_sessionid;
	}

	Ack::State Ack::getState() const
	{
		return m_state;
	}

	//-----------------------------------------------------------------------

	Reply::Reply(sessionid_t sessionid) :
		m_sessionid(sessionid)
	{
	}

	Reply::~Reply()
	{
	}

	sessionid_t	Reply::getSessionId() const
	{
		return m_sessionid;
	}

	//-----------------------------------------------------------------------

	Request::Request() :
		m_sessionid(getNextSessionId(m_sessionmgr))
	{
	}

	Request::~Request()
	{
	}

	Ack* Request::ack()
	{
		return nullptr;
	}

	Reply* Request::process()
	{
		return nullptr;
	}

	ReplyQ*	Request::getReplyQueue()
	{
		return nullptr;
	}

	sessionid_t	Request::getSessionId() const
	{
		return m_sessionid;
	}

	sessionid_t	Request::getNextSessionId(SessionIdMgr& mgr)
	{
		std::lock_guard<std::mutex> lock(mgr.getMutex());
		return ++mgr.getSessionid();
	}

	//-----------------------------------------------------------------------

	ExectueAck::ExectueAck(sessionid_t sessionid, State state) :
		Ack(sessionid, state)
	{
	}

	ExecuteReply::ExecuteReply(sessionid_t sessionid) :
		Reply(sessionid)
	{		
	}

	ExecuteRequest::ExecuteRequest(int scriptid) :
		m_scriptid(scriptid)
	{
	}

	Ack* ExecuteRequest::ack()
	{
		return new ExectueAck(getSessionId(), Ack::eOk);
	}

	Reply* ExecuteRequest::process()
	{
		return new ExecuteReply(getSessionId());
	}

	ReplyQ* ExecuteRequest::getReplyQueue()
	{
		return &sm_replyqueue;
	}

	int ExecuteRequest::getScriptid() const
	{
		return m_scriptid;
	}

	const ReplyQ& ExecuteRequest::replyQueue()
	{
		return sm_replyqueue;
	}

	//-----------------------------------------------------------------------

	ConfigAck::ConfigAck(sessionid_t sessionid, State state) :
		Ack(sessionid, state)
	{
	}

	ConfigReply::ConfigReply(sessionid_t sessionid, const std::string &host) :
		Reply(sessionid),
		m_host(host)
	{
	}

	const std::string& ConfigReply::getHostname() const
	{
		return m_host;
	}

	ConfigRequest::ConfigRequest(const std::string &host) :
		m_host(host)
	{
	}

	Ack* ConfigRequest::ack()
	{
		return new ConfigAck(getSessionId(), Ack::eOk);
	}

	Reply* ConfigRequest::process()
	{
		return new ConfigReply(getSessionId(), m_host);
	}

	ReplyQ* ConfigRequest::getReplyQueue()
	{
		return &sm_replyqueue;
	}

	const std::string& ConfigRequest::getHostname() const
	{
		return m_host;
	}

	const ReplyQ& ConfigRequest::replyQueue()
	{
		return sm_replyqueue;
	}

	//-----------------------------------------------------------------------

	void Actor::run()
	{
		while( !m_done )
		{
			Message* msg = nullptr;
			m_messages.wait_and_pop(msg);

			Reply* reply = msg->process();
			if (ReplyQ* rq = msg->getReplyQueue())
				rq->emplace(msg->getSessionId(), reply);
			delete msg;
		}
	}

	Actor::Actor() :
		m_done(false),
		m_thread(&Actor::run,this)
	{
	
	}

	Ack* Actor::post_message(Message* msg)
	{
		if (m_done)
			return nullptr;

		if (!msg)
			return nullptr;

		m_messages.push(msg);
		return msg->ack();
	}

	Actor::~Actor()
	{
		m_done = true;
		while (!m_messages.empty())
		{
			Message* msg = nullptr;
			m_messages.wait_and_pop(msg);
			delete msg;
		}

		m_thread.join();
	}
}
