#include "execute.hpp"
#include "runUrl.hpp"

#include <Wt/Http/Request>
#include <Wt/Http/Response>
#include <Wt/Http/Message>
#include <Wt/Http/Client>

#include <memory>
#include <mutex>
#include <condition_variable>

//---------------------------------------------------------------------------

namespace
{
	struct Context
	{
		Context() : ready(false) {}
		Context(const Context &) = delete;
		Context& operator=(const Context &) = delete;

		std::mutex m;
		std::condition_variable cv;
		bool ready;
	};

	struct Result
	{
		Result() : status(0) {}

		boost::system::error_code err;
		int status;
		std::string content;
	};

	Context ctx;

	void runhelper(boost::system::error_code err, const Wt::Http::Message& response, Result &result)
	{
		std::cout << "{}::runhelper : err=" << err << " status=" << response.status() << std::endl;

		result.err = err;
		result.status = response.status();
		result.content = response.body();

		std::cout << "{}::runhelper : response.body=" << result.content << std::endl;

		{
			std::lock_guard<std::mutex> lock(ctx.m);
			ctx.ready = true;
		}
		ctx.cv.notify_one();
	}

	Result run(Wt::WObject *obj, const std::string &url)
	{
		Result result;

		std::unique_ptr<Wt::Http::Client> client(new Wt::Http::Client(obj));
		client->setTimeout(5);
		client->setMaximumResponseSize(10 * 1024);
		client->done().connect(boost::bind(runhelper, _1, _2, boost::ref(result)));
		if (client->get(url))
		{
			std::unique_lock<std::mutex> lock(ctx.m);
			ctx.cv.wait(lock, []{return ctx.ready;});

			std::cout << "{}::run : status=" << result.status << " content=" << result.content << std::endl;
		}
		else
		{
			// in case of an error in the %URL
			result.status = 500;
			result.content = "content=URL error";
			std::cout << "{}::run : status=" << result.status << " content=" << result.content << std::endl;
		}

		return result;
	}
}

//---------------------------------------------------------------------------

Execute::Execute() :
	m_status(500)
{
}

int Execute::getStatus() const
{
	return m_status;
}

void Execute::setStatus(int value)
{
	m_status = value;
}

void Execute::handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response)
{
	std::cout << "Execute::handleRequest" << std::endl;

	// Initially, we do nothing.
	setStatus(204);

	try
	{
		Result result = run(this, "http://localhost:8082/proxy/?action=allocate");

		response.setStatus(result.status);
		response.out() << result.content;
	}
	catch (const std::exception &e)
	{
		response.out() << "failed";
	}

	// Initially, we do nothing.
	std::cout << "Execute::handleRequest : done" << std::endl << std::endl;
}
