#include "proxy.hpp"

#include <Wt/Http/Response>
#include <boost/algorithm/string/predicate.hpp>
#include <cstring>

//---------------------------------------------------------------------------

Proxy::Proxy() :
	m_status(500)
{
}

int Proxy::getStatus() const
{
	return m_status;
}

void Proxy::setStatus(int value)
{
	m_status = value;
}

void Proxy::handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response)
{
	std::cout << "Proxy::handleRequest" << std::endl;

	try
	{
		switch (getAction(request.getParameterMap()))
		{
		case EAllocate:
			actionAllocate(request, response);
			break;

		case ERelease:
			actionRelease(request, response);
			break;

		default:
			setStatus(405);	// not supported
			response.out() << "code " << getStatus() << ": unrecognised command: " << getActionText(request.getParameterMap());
		}
	}
	catch (const std::exception &e)
	{
		response.setStatus(500);		// internal server error
		response.out() << "internal server error";
	}

	response.setStatus(getStatus());		// internal server error
}

void Proxy::actionAllocate(const Wt::Http::Request &request, Wt::Http::Response &response)
{
	setStatus(200);
	response.out() << "code " << getStatus() << ": " << getActionText(request.getParameterMap());
}

void Proxy::actionRelease(const Wt::Http::Request &request, Wt::Http::Response &response)
{
	setStatus(200);
	response.out() << "code " << getStatus() << ": " << getActionText(request.getParameterMap());
}

std::string Proxy::getActionText(const Wt::Http::ParameterMap &params) const
{
	auto p = params.find("action");
	if (p != params.end())
	{
		for (const auto &s : p->second)
		{
			return s;
		}
	}

	return std::string();
}

Proxy::eAction Proxy::getAction(const Wt::Http::ParameterMap &params) const
{
	std::string s = getActionText(params);
	if (boost::iequals(s, "allocate"))
	{
		return EAllocate;
	}
	if (boost::iequals(s, "release"))
	{
		return ERelease;
	}

	return EUnknown;
}
