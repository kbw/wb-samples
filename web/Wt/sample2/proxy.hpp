#ifndef PROXY_HPP
#define PROXY_HPP

#include <Wt/WResource>
#include <Wt/Http/Request>

//---------------------------------------------------------------------------

class Proxy : public Wt::WResource
{
public:
	enum eAction { EUnknown, EAllocate, ERelease };

	Proxy();

	int getStatus() const;
	void setStatus(int status);

protected:
	virtual void handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response);
	virtual void actionAllocate(const Wt::Http::Request &request, Wt::Http::Response &response);
	virtual void actionRelease(const Wt::Http::Request &request, Wt::Http::Response &response);

private:
	virtual std::string getActionText(const Wt::Http::ParameterMap &params) const;
	virtual eAction getAction(const Wt::Http::ParameterMap &params) const;

private:
	int m_status;
};

#endif	// PROXY_HPP
