#include "runUrl.hpp"

#include <boost/bind.hpp>
#include <boost/functional.hpp>

#include <Wt/Http/Request>
#include <Wt/Http/Response>
#include <Wt/Http/Message>
#include <Wt/Http/Client>

#include <memory>
#include <condition_variable>

//---------------------------------------------------------------------------

/*
namespace
{
	struct Context
	{
		Context() : ready(false) {}
		Context(const Context &) = delete;
		Context& operator=(const Context &) = delete;

		std::mutex m;
		std::condition_variable cv;
		bool ready;
	};

	struct Result
	{
		Result() : status(0) {}

		boost::system::error_code err;
		int status;
		std::string content;
	};

	void helper(boost::system::error_code err, const Wt::Http::Message& response, Result &result)
	{
		std::cout << "{}::handleHttpResponse : err=" << err << " status=" << response.status() << std::endl;

		result.err = err;
		result.status = response.status();

		{
			std::lock_guard<std::mutex> lock(ctx.m);
			ctx.ready = true;
		}
		ctx.cv.notify_one();
	}
}

Result runUrl(Wt::WObject *obj, const std::string &url)
{
	Result result;

	std::unique_ptr<Wt::Http::Client> client(new Wt::Http::Client(obj));
	client->setTimeout(5);
	client->setMaximumResponseSize(10 * 1024);
	client->done().connect(boost::bind(runhelper, _1, _2, result));
	if (client->get(url))
	{
		std::unique_lock<std::mutex> lock(ctx.m);
		ctx.cv.wait(lock, []{return ctx.ready;});
	}
	else
	{
		// in case of an error in the %URL
		std::cout << "{}::allocate_slot : 500 : URL error" << std::endl;
		result.status = 500;
	}

	return result;
}
 */

namespace
{
	void local_helper(boost::system::error_code err, const Wt::Http::Message& response, RunUrl::Result &result)
	{
	}
}

RunUrl::RunUrl(Wt::WObject *obj) :
	m_obj(obj),
	m_ctx(new Context)
{
}

RunUrl::Result RunUrl::execute(const std::string &url)
{
	Result result;

	std::unique_ptr<Wt::Http::Client> client(new Wt::Http::Client(m_obj));
	client->setTimeout(5);
	client->setMaximumResponseSize(10 * 1024);
	client->done().connect(&RunUrl::helper, *this, _1, _2, boost::ref(result));
	if (client->get(url))
	{
		std::unique_lock<std::mutex> lock(m_ctx->m);

		// replace with lamda
		class Ready
		{
			bool& m_ready;
		public:
			Ready(bool& ready) : m_ready(ready) {}
			bool operator()() const { return m_ready; }
		};
		Ready ready(m_ctx->ready);
		m_ctx->cv.wait(lock, ready);
	}
	else
	{
		// in case of an error in the %URL
		std::cout << "{}::allocate_slot : 500 : URL error" << std::endl;
		result.status = 500;
	}

	return result;
}

void RunUrl::helper(boost::system::error_code err, const Wt::Http::Message& response, Result &result)
{
	std::cout << "{}::handleHttpResponse : err=" << err << " status=" << response.status() << std::endl;

	result.err = err;
	result.status = response.status();

	{
		std::lock_guard<std::mutex> lock(m_ctx->m);
		m_ctx->ready = true;
	}
	m_ctx->cv.notify_one();
}
