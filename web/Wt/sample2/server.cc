#include "execute.hpp"
#include "proxy.hpp"

#include <Wt/WApplication>
#include <Wt/WEnvironment>
#include <Wt/WResource>
#include <Wt/WServer>

#include <iostream>

//---------------------------------------------------------------------------

class ServerApplication : public Wt::WApplication
{
public:
	ServerApplication(const Wt::WEnvironment& env);
};

ServerApplication::ServerApplication(const Wt::WEnvironment& env) :
	Wt::WApplication(env)
{
}

Wt::WApplication *createApplication(const Wt::WEnvironment& env)
{
	return new ServerApplication(env);
}

//---------------------------------------------------------------------------

int main(int argc, char **argv)
{
	try
	{
		Execute exec;
		Proxy proxy;

		Wt::WServer server(argv[0]);
		server.setServerConfiguration(argc, argv, WTHTTP_CONFIGURATION);
		server.addResource(&exec, "/execute/");
		server.addResource(&proxy, "/proxy/");

		if (server.start())
		{
			Wt::WServer::waitForShutdown();
			server.stop();
		}
	}
	catch (const Wt::WServer::Exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	catch (const std::exception &e)
	{
		std::cerr << "exception: " << e.what() << std::endl;
	}
}
