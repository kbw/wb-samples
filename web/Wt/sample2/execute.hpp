#ifndef EXECUTE_HPP
#define EXECUTE_HPP

#include <Wt/WResource>
#include <boost/system/error_code.hpp>

namespace Wt {
namespace Http {
	class Request;
	class Response;
} }

//---------------------------------------------------------------------------

class Execute : public Wt::WResource
{
public:
	Execute();

	int getStatus() const;
	void setStatus(int status);

protected:
	virtual void handleRequest(const Wt::Http::Request &request, Wt::Http::Response &response);

private:
	int m_status;
};

#endif	// EXECUTE_HPP
