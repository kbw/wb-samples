#ifndef RUNURL_HPP
#define RUNURL_HPP

#include <boost/system/error_code.hpp>
#include <string>
#include <memory>
#include <mutex>
#include <condition_variable>

namespace Wt
{
	class WObject;

	namespace Http
	{
		class Message;
	}
}
//---------------------------------------------------------------------------

class RunUrl
{
public:
	struct Result
	{
		Result() : status(0) {}

		boost::system::error_code err;
		int status;
		std::string content;
	};

public:
	RunUrl(Wt::WObject *obj);

	Result execute(const std::string &url);

private:
	struct Context
	{
		Context() : ready(false) {}

		Context(const Context &) = delete;
		Context& operator=(const Context &) = delete;

		std::mutex m;
		std::condition_variable cv;
		bool ready;
	};

	void helper(boost::system::error_code err, const Wt::Http::Message& response, Result &result);
	bool ready() const { return m_ctx->ready; }

private:
	Wt::WObject *m_obj;
	std::unique_ptr<Context> m_ctx;
};

#endif	// RUNURL_HPP
