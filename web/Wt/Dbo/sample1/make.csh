#!/bin/csh

setenv CXX c++
setenv CXXFLAGS -I/usr/local/include
setenv LDFLAGS -L/usr/local/lib
setenv HTTP_ADDR 0.0.0.0
setenv LD_LIBRARY_PATH /usr/local/lib 

gmake $1
