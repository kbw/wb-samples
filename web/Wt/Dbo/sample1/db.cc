#include <Wt/Dbo/Dbo>
#include <Wt/Dbo/backend/Sqlite3>
#include <Wt/Json/Object>
#include <Wt/Json/Parser>

#include <ostream>
#include <fstream>
#include <vector>

/*
mysql> desc Script;
+-----------------------+--------------+------+-----+------------+----------------+
| Field                 | Type         | Null | Key | Default    | Extra          |
+-----------------------+--------------+------+-----+------------+----------------+
| ScriptID              | int(4)       | NO   | PRI | NULL       | auto_increment |
| CustomerNo            | int(4)       | NO   |     | 0          |                |
| ScriptName            | varchar(255) | NO   |     |            |                |
| TestingFreq           | int(2)       | NO   |     | 60         |                |
| Enabled               | int(1)       | NO   |     | 0          |                |
| Code                  | mediumtext   | NO   |     | NULL       |                |
| Status                | int(2)       | NO   |     | 0          |                |
| Version               | float        | NO   |     | 0          |                |
| NoOfBatches           | int(4)       | NO   |     | 0          |                |
| VersionDate           | date         | NO   |     | 0000-00-00 |                |
| TestPerBatch          | int(2)       | NO   |     | 10         |                |
| FreqOfTests           | int(2)       | NO   |     | 10         |                |
| DisplayOrder          | int(2)       | NO   |     | 1          |                |
| KPIDownloadTime       | int(4)       | NO   |     | 0          |                |
| KPIPageSize           | int(4)       | NO   |     | 0          |                |
| StoreInfo             | int(1)       | NO   |     | 0          |                |
| SCM                   | tinyint(4)   | NO   |     | 0          |                |
| CustomNumber          | int(4)       | NO   |     | 0          |                |
| DoSampling            | tinyint(4)   | NO   |     | 0          |                |
| SamplingDate          | date         | NO   |     | 0000-00-00 |                |
| SamplingResultCode    | int(2)       | NO   |     | 0          |                |
| ScriptType            | int(2)       | NO   |     | 0          |                |
| ScriptLanguage        | int(2)       | NO   |     | 0          |                |
| AgentVersion          | varchar(30)  | YES  |     | NULL       |                |
| IMEnabled             | int(1)       | NO   |     | 0          |                |
| IMFreq                | int(2)       | NO   |     | 0          |                |
| DisplayDiag           | tinyint(4)   | NO   |     | 0          |                |
| Comment               | varchar(255) | NO   |     |            |                |
| CustomerNote          | mediumtext   | NO   |     | NULL       |                |
| CustomString          | mediumtext   | NO   |     | NULL       |                |
| JackEnabled           | tinyint(4)   | NO   |     | 0          |                |
| CarrierToExclude      | int(11)      | NO   |     | 0          |                |
| Concurrent            | tinyint(4)   | YES  |     | 0          |                |
| CollectDiagsAtRunTime | int(1)       | NO   |     | 0          |                |
+-----------------------+--------------+------+-----+------------+----------------+

mysql> desc WebPage;
+--------------------------+--------------+------+-----+---------+----------------+
| Field                    | Type         | Null | Key | Default | Extra          |
+--------------------------+--------------+------+-----+---------+----------------+
| PageID                   | int(4)       | NO   | PRI | NULL    | auto_increment | 
| CustomerNo               | int(4)       | NO   | MUL | 0       |                | 
| PageType                 | int(2)       | NO   |     | 0       |                | 
| PageNumber               | int(2)       | NO   |     | 0       |                | 
| Status                   | int(2)       | NO   |     | 0       |                | 
| URL                      | mediumtext   | NO   |     | NULL    |                | 
| ResetCookies             | int(1)       | NO   |     | 0       |                | 
| AuthUser                 | varchar(255) | NO   |     |         |                | 
| AuthPass                 | varchar(255) | NO   |     |         |                | 
| DownloadThreshold        | int(2)       | NO   |     | 0       |                | 
| HackingString            | varchar(255) | NO   |     |         |                | 
| ResetCache               | int(1)       | NO   |     | 0       |                | 
| IgnoreRudeWords          | int(1)       | YES  |     | 1       |                | 
| HTTPMethod               | int(1)       | YES  |     | 0       |                | 
| IgnoreContentErrors      | int(1)       | YES  |     | 1       |                | 
| StepName                 | varchar(255) | NO   |     |         |                | 
| ScriptID                 | int(4)       | NO   | MUL | 0       |                | 
| DownloadSpeed            | int(4)       | NO   |     | 58254   |                | 
| Referer                  | mediumtext   | NO   |     | NULL    |                | 
| StoreResultDetail        | int(1)       | NO   |     | 1       |                | 
| KeepResultDetailFailures | int(1)       | NO   |     | 1       |                | 
| SendCSV                  | int(1)       | NO   |     | 0       |                | 
| MaxSize                  | int(4)       | NO   |     | 0       |                | 
| MinSize                  | int(4)       | NO   |     | 0       |                | 
| CertID                   | int(4)       | NO   |     | 0       |                | 
| SleepTime                | int(2)       | NO   |     | 0       |                | 
| AgentString              | varchar(255) | NO   |     |         |                | 
| ProcessScript            | int(1)       | NO   |     | 1       |                | 
| Comment                  | mediumtext   | NO   |     | NULL    |                | 
| UptimeFreq               | int(2)       | NO   |     | 0       |                | 
| StoreRequest             | int(1)       | NO   |     | 0       |                | 
| StoreHeaders             | int(1)       | NO   |     | 0       |                | 
| StoreHTML                | int(1)       | NO   |     | 0       |                | 
| StoreDiag                | int(1)       | NO   |     | 0       |                | 
| ContentType              | varchar(255) | NO   |     |         |                | 
| Header1                  | varchar(255) | NO   |     |         |                | 
| Header2                  | varchar(255) | NO   |     |         |                | 
| GzipEncoding             | int(1)       | NO   |     | 1       |                | 
| Host                     | varchar(255) | NO   |     |         |                | 
| ExtraInfo                | int(4)       | NO   |     | 0       |                | 
| KPIDownloadTime          | int(4)       | NO   |     | 0       |                | 
| KPIPageSize              | int(4)       | NO   |     | 0       |                | 
| StoreInfo                | int(1)       | NO   |     | 0       |                | 
| ProcessJavaScriptStatic  | int(1)       | NO   |     | 0       |                | 
| ProcessMMPreload         | int(1)       | NO   |     | 1       |                | 
| ProcessCSSImages         | int(1)       | YES  |     | 0       |                | 
| DiagNote                 | varchar(255) | NO   |     |         |                | 
| ConnectThreshold         | int(2)       | NO   |     | 30      |                | 
| DataStartThreshold       | int(2)       | NO   |     | 0       |                | 
| DoSampling               | tinyint(4)   | NO   |     | 0       |                | 
| SSLFix                   | int(4)       | NO   |     | 0       |                | 
| AgentVersion             | int(2)       | NO   |     | 0       |                | 
| ProcessOtherContent      | int(2)       | NO   |     | 0       |                | 
| ScriptType               | int(2)       | NO   |     | 0       |                | 
| DoCssParsing             | int(1)       | NO   |     | 1       |                | 
| XmlValidationLevel       | tinyint(1)   | NO   |     | 0       |                | 
| DoSslValidation          | tinyint(1)   | NO   |     | 0       |                | 
| IMEnabled                | int(1)       | NO   |     | 0       |                | 
| IMFreq                   | int(2)       | NO   |     | 0       |                | 
| WebCacheControl          | tinyint(1)   | NO   |     | 1       |                | 
| IMDownloadThreshold      | int(2)       | NO   |     | 120     |                | 
| NtlmUser                 | varchar(255) | NO   |     |         |                | 
| NtlmPassword             | varchar(255) | NO   |     |         |                | 
| NtlmDomain               | varchar(255) | NO   |     |         |                | 
| HackingStringPresence    | int(1)       | NO   |     | 1       |                | 
| HackingStringLogic       | int(1)       | NO   |     | 0       |                | 
| UploadSpeed              | int(4)       | NO   |     | 0       |                | 
| SiteConResponseTag       | varchar(255) | NO   |     |         |                | 
| UseLocalWebPageText      | int(1)       | NO   |     | 0       |                | 
| ActivateGetHeaders       | int(1)       | NO   |     | 0       |                | 
| FormDataBoundary         | varchar(255) | NO   |     |         |                | 
| MaximumMissingComponents | int(1)       | NO   |     | -1      |                | 
| WebkitTimeout            | smallint(6)  | NO   |     | 0       |                | 
| JavascriptEnabled        | tinyint(4)   | NO   |     | 1       |                | 
| JackEnabled              | tinyint(4)   | NO   |     | 0       |                | 
| CarrierToExclude         | int(11)      | NO   |     | 0       |                | 
| MaxThreadsPerHost        | int(11)      | NO   |     | 0       |                | 
| GlobalMaxThreads         | int(11)      | NO   |     | 0       |                | 
| NetworkLatency           | int(11)      | NO   |     | 0       |                | 
| ScreenXDimension         | int(11)      | NO   |     | 0       |                | 
| ScreenYDimension         | int(11)      | NO   |     | 0       |                | 
| JsonValidation           | tinyint(4)   | NO   |     | 0       |                | 
| MaxTotalThreads          | tinyint(11)  | NO   |     | 0       |                | 
| AcceptHeaderOverride     | varchar(255) | YES  |     | NULL    |                | 
| TextMimeTypes            | mediumtext   | YES  |     | NULL    |                | 
+--------------------------+--------------+------+-----+---------+----------------+

mysql> desc RunID;
+--------------------+---------------------+------+-----+---------------------+----------------+
| Field              | Type                | Null | Key | Default             | Extra          |
+--------------------+---------------------+------+-----+---------------------+----------------+
| RunID              | int(4)              | NO   | PRI | NULL                | auto_increment | 
| CustomerNo         | int(4)              | NO   |     | 0                   |                | 
| FirstResultID      | bigint(20) unsigned | NO   |     | 0                   |                | 
| StartDateTime      | datetime            | NO   | PRI | 0000-00-00 00:00:00 |                | 
| ScriptID           | int(4)              | NO   | MUL | 0                   |                | 
| EndDateTime        | datetime            | NO   |     | 0000-00-00 00:00:00 |                | 
| BatchID            | int(4)              | NO   |     | 0                   |                | 
| OverallResultCode  | int(2)              | NO   |     | 0                   |                | 
| TotalTime          | int(4)              | NO   |     | 0                   |                | 
| FinalStep          | int(2)              | NO   |     | 0                   |                | 
| TestType           | int(2)              | NO   |     | 0                   |                | 
| StartDateTimeLocal | datetime            | NO   |     | 0000-00-00 00:00:00 |                | 
+--------------------+---------------------+------+-----+---------------------+----------------+

mysql> desc ResultHeader;
+--------------------+---------------------+------+-----+---------------------+----------------+
| Field              | Type                | Null | Key | Default             | Extra          |
+--------------------+---------------------+------+-----+---------------------+----------------+
| ResultID           | bigint(20) unsigned | NO   | PRI | NULL                | auto_increment | 
| PageID             | int(4)              | NO   | MUL | 0                   |                | 
| TestServer         | int(2)              | NO   |     | 0                   |                | 
| StartDateTime      | datetime            | NO   | PRI | 0000-00-00 00:00:00 |                | 
| DNSTime            | int(4)              | NO   |     | 0                   |                | 
| FirstByteTime      | int(4)              | NO   |     | 0                   |                | 
| FirstDataTime      | int(4)              | NO   |     | 0                   |                | 
| CompleteTime       | int(4)              | NO   |     | 0                   |                | 
| ResultCode         | int(2)              | NO   |     | 0                   |                | 
| TotalBytes         | int(4)              | NO   |     | 0                   |                | 
| GzipTotal          | int(4)              | NO   |     | 0                   |                | 
| RunID              | int(4)              | NO   | MUL | 0                   |                | 
| BatchID            | int(4)              | NO   | MUL | 0                   |                | 
| TestType           | int(2)              | NO   |     | 0                   |                | 
| StartDateTimeLocal | datetime            | NO   |     | 0000-00-00 00:00:00 |                | 
| AgentVersion       | int(8)              | NO   |     | 0                   |                | 
| DiagStored         | int(1)              | NO   |     | 0                   |                | 
| SslConnectTime     | int(4)              | NO   |     | 0                   |                | 
| RequestSentTime    | int(4)              | NO   |     | 0                   |                | 
| RequestHeaderSize  | int(4)              | NO   |     | 0                   |                | 
| RequestContentSize | int(4)              | NO   |     | 0                   |                | 
| ResponseHeaderSize | int(4)              | NO   |     | 0                   |                | 
+--------------------+---------------------+------+-----+---------------------+----------------+

mysql> desc ResultDetail;
+--------------------+---------------------+------+-----+---------------------+-------+
| Field              | Type                | Null | Key | Default             | Extra |
+--------------------+---------------------+------+-----+---------------------+-------+
| ResultID           | bigint(20) unsigned | NO   | PRI | 0                   |       | 
| StartDateTime      | datetime            | NO   | PRI | 0000-00-00 00:00:00 |       | 
| StartOffset        | int(4)              | NO   |     | 0                   |       | 
| DNSTime            | int(4)              | NO   |     | 0                   |       | 
| FirstByteTime      | int(4)              | NO   |     | 0                   |       | 
| FirstDataTime      | int(4)              | NO   |     | 0                   |       | 
| CompleteTime       | int(4)              | NO   |     | 0                   |       | 
| ResultCode         | int(2)              | NO   |     | 0                   |       | 
| TotalBytes         | int(4)              | NO   |     | 0                   |       | 
| GzipTotal          | int(4)              | NO   |     | 0                   |       | 
| FileName           | mediumtext          | NO   |     | NULL                |       | 
| ComponentNo        | int(2)              | NO   | PRI | 0                   |       | 
| SslConnectTime     | int(4)              | NO   |     | 0                   |       | 
| RequestSentTime    | int(4)              | NO   |     | 0                   |       | 
| RequestHeaderSize  | int(4)              | NO   |     | 0                   |       | 
| RequestContentSize | int(4)              | NO   |     | 0                   |       | 
| ResponseHeaderSize | int(4)              | NO   |     | 0                   |       | 
+--------------------+---------------------+------+-----+---------------------+-------+

 */
namespace table
{
	const size_t	ScriptName_sz = 255;
	const size_t	AgentVersion_sz = 30;
	const size_t	Comment_sz = 255;

	struct Script
	{
		typedef int64_t		ScriptID_t;
		typedef int64_t		CustomerNo_t;
//		typedef char		ScriptName_t[ScriptName_sz];
		typedef std::string	ScriptName_t;
		typedef int16_t		TestingFreq_t;
		typedef	int			boolean_t;
		typedef int16_t		Status_t;
		typedef	double		Version_t;
		typedef	std::string	date_t;
//		typedef	char		AgentVersion_t[AgentVersion_sz];
		typedef std::string	AgentVersion_t;
//		typedef	char		Comment_t[Comment_sz];
		typedef std::string	Comment_t;

		ScriptID_t		ScriptID;
		CustomerNo_t	CustomerNo;
		ScriptName_t	ScriptName;
		TestingFreq_t	TestingFreq;
		boolean_t		Enabled;
		std::string		Code;
		Status_t		Status;
		Version_t		Version;
		int				NoOfBatches;
		date_t			VersionDate;
		int				TestPerBatch;
		int				FreqOfTests;
		int				DisplayOrder;
		int				KPIDownloadTime;
		int				KPIPageSize;
		int				StoreInfo;
		int				SCM;
		int				CustomNumber;
		int				DoSampling;
		date_t			SamplingDate;
		int				SamplingResultCode;
		int				ScriptType;
		int				ScriptLanguage;
		AgentVersion_t	AgentVersion;
		boolean_t		IMEnabled;
		int				IMFreq;
		int				DisplayDiag;
		Comment_t		Comment;
		std::string		CustomerNote;
		std::string		CustomString;
		int				JackEnabled;
		int				CarrierToExclude;
		int				Concurrent;
		int				CollectDiagsAtRunTime;

		Script()
		{
			NoOfBatches = 
			TestPerBatch = 
			FreqOfTests = 
			DisplayOrder = 
			KPIDownloadTime = 
			KPIPageSize = 
			StoreInfo = 
			SCM = 
			CustomNumber = 
			DoSampling = 
			SamplingResultCode = 
			ScriptType = 
			ScriptLanguage = 
			IMFreq = 
			DisplayDiag = 
			JackEnabled = 
			CarrierToExclude = 
			Concurrent = 
			CollectDiagsAtRunTime = 0;
		}

		template <class Action>
		void persist(Action& a)
		{
			Wt::Dbo::field(a, ScriptID,			"ScriptID");
			Wt::Dbo::field(a, CustomerNo,		"CustomerNo");
			Wt::Dbo::field(a, ScriptName,		"ScriptName");
			Wt::Dbo::field(a, TestingFreq,		"TestingFreq");
			Wt::Dbo::field(a, Enabled,			"Enabled");
			Wt::Dbo::field(a, Code,				"Code");
			Wt::Dbo::field(a, Status,			"Status");
/*
			Wt::Dbo::field(a, Version,			"Version");
			Wt::Dbo::field(a, NoOfBatches,		"NoOfBatches");
			Wt::Dbo::field(a, VersionDate,		"VersionDate");
			Wt::Dbo::field(a, FreqOfTests,		"FreqOfTests");
			Wt::Dbo::field(a, DisplayOrder,		"DisplayOrder");
			Wt::Dbo::field(a, KPIDownloadTime,	"KPIDownloadTime");
			Wt::Dbo::field(a, KPIPageSize,		"KPIPageSize");
			Wt::Dbo::field(a, StoreInfo,		"StoreInfo");
			Wt::Dbo::field(a, SCM,				"SCM");
			Wt::Dbo::field(a, CustomNumber,		"CustomNumber");
			Wt::Dbo::field(a, DoSampling,		"DoSampling");
			Wt::Dbo::field(a, SamplingDate,		"SamplingDate");
			Wt::Dbo::field(a, SamplingResultCode,"SamplingResultCode");
			Wt::Dbo::field(a, ScriptType,		"ScriptType");
			Wt::Dbo::field(a, ScriptLanguage,	"ScriptLanguage");
			Wt::Dbo::field(a, AgentVersion,		"AgentVersion");
			Wt::Dbo::field(a, IMEnabled,		"IMEnabled");
			Wt::Dbo::field(a, DisplayDiag,		"DisplayDiag");
			Wt::Dbo::field(a, Comment,			"Comment");
			Wt::Dbo::field(a, CustomerNote,		"CustomerNote");
			Wt::Dbo::field(a, CustomString,		"CustomString");
			Wt::Dbo::field(a, CarrierToExclude,	"CarrierToExclude");
			Wt::Dbo::field(a, CarrierToExclude,	"CarrierToExclude");
			Wt::Dbo::field(a, Concurrent,		"Concurrent");
			Wt::Dbo::field(a, CollectDiagsAtRunTime,"CollectDiagsAtRunTime");
 */
		}
	};
}
