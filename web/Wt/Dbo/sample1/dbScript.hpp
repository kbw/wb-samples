#ifndef DBSCRIPT_HPP
#define DBSCRIPT_HPP

/*
mysql> desc Script;
+-----------------------+--------------+------+-----+------------+----------------+
| Field                 | Type         | Null | Key | Default    | Extra          |
+-----------------------+--------------+------+-----+------------+----------------+
| ScriptID              | int(4)       | NO   | PRI | NULL       | auto_increment |
| CustomerNo            | int(4)       | NO   |     | 0          |                |
| ScriptName            | varchar(255) | NO   |     |            |                |
| TestingFreq           | int(2)       | NO   |     | 60         |                |
| Enabled               | int(1)       | NO   |     | 0          |                |
| Code                  | mediumtext   | NO   |     | NULL       |                |
| Status                | int(2)       | NO   |     | 0          |                |
| Version               | float        | NO   |     | 0          |                |
| NoOfBatches           | int(4)       | NO   |     | 0          |                |
| VersionDate           | date         | NO   |     | 0000-00-00 |                |
| TestPerBatch          | int(2)       | NO   |     | 10         |                |
| FreqOfTests           | int(2)       | NO   |     | 10         |                |
| DisplayOrder          | int(2)       | NO   |     | 1          |                |
| KPIDownloadTime       | int(4)       | NO   |     | 0          |                |
| KPIPageSize           | int(4)       | NO   |     | 0          |                |
| StoreInfo             | int(1)       | NO   |     | 0          |                |
| SCM                   | tinyint(4)   | NO   |     | 0          |                |
| CustomNumber          | int(4)       | NO   |     | 0          |                |
| DoSampling            | tinyint(4)   | NO   |     | 0          |                |
| SamplingDate          | date         | NO   |     | 0000-00-00 |                |
| SamplingResultCode    | int(2)       | NO   |     | 0          |                |
| ScriptType            | int(2)       | NO   |     | 0          |                |
| ScriptLanguage        | int(2)       | NO   |     | 0          |                |
| AgentVersion          | varchar(30)  | YES  |     | NULL       |                |
| IMEnabled             | int(1)       | NO   |     | 0          |                |
| IMFreq                | int(2)       | NO   |     | 0          |                |
| DisplayDiag           | tinyint(4)   | NO   |     | 0          |                |
| Comment               | varchar(255) | NO   |     |            |                |
| CustomerNote          | mediumtext   | NO   |     | NULL       |                |
| CustomString          | mediumtext   | NO   |     | NULL       |                |
| JackEnabled           | tinyint(4)   | NO   |     | 0          |                |
| CarrierToExclude      | int(11)      | NO   |     | 0          |                |
| Concurrent            | tinyint(4)   | YES  |     | 0          |                |
| CollectDiagsAtRunTime | int(1)       | NO   |     | 0          |                |
+-----------------------+--------------+------+-----+------------+----------------+
 */
namespace table
{
	const size_t	ScriptName_sz = 255;
	const size_t	AgentVersion_sz = 30;
	const size_t	Comment_sz = 255;

	struct Script
	{
		typedef int64_t		ScriptID_t;
		typedef int64_t		CustomerNo_t;
//		typedef char		ScriptName_t[ScriptName_sz];
		typedef std::string	ScriptName_t;
		typedef int16_t		TestingFreq_t;
		typedef	int			boolean_t;
		typedef int16_t		Status_t;
		typedef	double		Version_t;
		typedef	std::string	date_t;
//		typedef	char		AgentVersion_t[AgentVersion_sz];
		typedef std::string	AgentVersion_t;
//		typedef	char		Comment_t[Comment_sz];
		typedef std::string	Comment_t;

		ScriptID_t		ScriptID;
		CustomerNo_t	CustomerNo;
		ScriptName_t	ScriptName;
		TestingFreq_t	TestingFreq;
		boolean_t		Enabled;
		std::string		Code;
		Status_t		Status;
		Version_t		Version;
		int				NoOfBatches;
		date_t			VersionDate;
		int				TestPerBatch;
		int				FreqOfTests;
		int				DisplayOrder;
		int				KPIDownloadTime;
		int				KPIPageSize;
		int				StoreInfo;
		int				SCM;
		int				CustomNumber;
		int				DoSampling;
		date_t			SamplingDate;
		int				SamplingResultCode;
		int				ScriptType;
		int				ScriptLanguage;
		AgentVersion_t	AgentVersion;
		boolean_t		IMEnabled;
		int				IMFreq;
		int				DisplayDiag;
		Comment_t		Comment;
		std::string		CustomerNote;
		std::string		CustomString;
		int				JackEnabled;
		int				CarrierToExclude;
		int				Concurrent;
		int				CollectDiagsAtRunTime;

		Script() :
			ScriptID(0),
			CustomerNo(0),
			TestingFreq(0),
			Enabled(0),
			Status(0),
			Version(0)
		{
			NoOfBatches = 
			TestPerBatch = 
			FreqOfTests = 
			DisplayOrder = 
			KPIDownloadTime = 
			KPIPageSize = 
			StoreInfo = 
			SCM = 
			CustomNumber = 
			DoSampling = 
			SamplingResultCode = 
			ScriptType = 
			ScriptLanguage = 
			IMFreq = 
			DisplayDiag = 
			JackEnabled = 
			CarrierToExclude = 
			Concurrent = 
			CollectDiagsAtRunTime = 0;
		}

		template <class Action>
		void persist(Action& a)
		{
			Wt::Dbo::field(a, ScriptID,			"ScriptID");
			Wt::Dbo::field(a, CustomerNo,		"CustomerNo");
			Wt::Dbo::field(a, ScriptName,		"ScriptName");
			Wt::Dbo::field(a, TestingFreq,		"TestingFreq");
			Wt::Dbo::field(a, Enabled,			"Enabled");
			Wt::Dbo::field(a, Code,				"Code");
			Wt::Dbo::field(a, Status,			"Status");
/*
			Wt::Dbo::field(a, Version,			"Version");
			Wt::Dbo::field(a, NoOfBatches,		"NoOfBatches");
			Wt::Dbo::field(a, VersionDate,		"VersionDate");
			Wt::Dbo::field(a, FreqOfTests,		"FreqOfTests");
			Wt::Dbo::field(a, DisplayOrder,		"DisplayOrder");
			Wt::Dbo::field(a, KPIDownloadTime,	"KPIDownloadTime");
			Wt::Dbo::field(a, KPIPageSize,		"KPIPageSize");
			Wt::Dbo::field(a, StoreInfo,		"StoreInfo");
			Wt::Dbo::field(a, SCM,				"SCM");
			Wt::Dbo::field(a, CustomNumber,		"CustomNumber");
			Wt::Dbo::field(a, DoSampling,		"DoSampling");
			Wt::Dbo::field(a, SamplingDate,		"SamplingDate");
			Wt::Dbo::field(a, SamplingResultCode,"SamplingResultCode");
			Wt::Dbo::field(a, ScriptType,		"ScriptType");
			Wt::Dbo::field(a, ScriptLanguage,	"ScriptLanguage");
			Wt::Dbo::field(a, AgentVersion,		"AgentVersion");
			Wt::Dbo::field(a, IMEnabled,		"IMEnabled");
			Wt::Dbo::field(a, DisplayDiag,		"DisplayDiag");
			Wt::Dbo::field(a, Comment,			"Comment");
			Wt::Dbo::field(a, CustomerNote,		"CustomerNote");
			Wt::Dbo::field(a, CustomString,		"CustomString");
			Wt::Dbo::field(a, CarrierToExclude,	"CarrierToExclude");
			Wt::Dbo::field(a, CarrierToExclude,	"CarrierToExclude");
			Wt::Dbo::field(a, Concurrent,		"Concurrent");
			Wt::Dbo::field(a, CollectDiagsAtRunTime,"CollectDiagsAtRunTime");
 */
		}
	};
}

#endif
