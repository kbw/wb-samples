//
// http://www.webtoolkit.eu/wt/doc/tutorial/Wt::Dbo/tutorial.html
//
#ifndef SCDB_SCHEMA_HPP
#define SCDB_SCHEMA_HPP

#ifdef _MSC_VER
#pragma warning( disable : 4251 4512 4275 )
#endif

#include <Wt/Dbo/Dbo>
#include <Wt/WDateTime>
#include <Wt/WString>
#include <Wt/Dbo/WtSqlTraits>
#include <boost/optional/optional.hpp>

#include <string>
#include <cstdint>

//---------------------------------------------------------------------------

class Script;
class WebPage;

class RunID;
class ResultHeader;
class ResultDetail;

class Request;
class Header;

typedef int64_t	resultid_t;

struct Component
{
	resultid_t resultid;
	int componentno;

	Component(resultid_t resultid = -1, int componentno = -1) : resultid(resultid), componentno(componentno) {}
};

inline bool operator<(const Component& a, const Component& b)
{
	if (a.resultid < b.resultid)
		return true;
	else if (a.resultid > b.resultid)
		return false;

	return a.componentno < b.componentno;
}

inline std::ostream& operator<<(std::ostream& os, const Component& c)
{
	os << c.resultid << ' ' << c.componentno;
	return os;
}

namespace Wt {
	namespace Dbo {
		template<>
		struct dbo_traits<Script> : public dbo_default_traits {
			static const char* surrogateIdField()	{ return "ScriptID"; }
			static const char *versionField()		{ return NULL; }
		};

		template<>
		struct dbo_traits<WebPage> : public dbo_default_traits {
			static const char* surrogateIdField()	{ return "PageID"; }
			static const char *versionField()		{ return NULL; }
		};

		template<>
		struct dbo_traits<RunID> : public dbo_default_traits {
			static const char*	surrogateIdField()	{ return "RunID"; }
			static const char*	versionField()		{ return NULL; }
		};

		template<>
		struct dbo_traits<ResultHeader> : public dbo_default_traits {
			static const char*	surrogateIdField()	{ return "ResultID"; }
			static const char*	versionField()		{ return NULL; }
		};

		template <class Action>
		void field(Action& action, Component& c, const std::string&, int) {
			field(action, c.resultid, "ResultID");
			field(action, c.componentno, "ComponentNo");
		}

		template<>
		struct dbo_traits<ResultDetail> : public dbo_default_traits
		{
			typedef Component IdType;

			static IdType		invalidId()			{ return Component(); }
			static const char*	surrogateIdField()	{ return NULL; }
			static const char*	versionField()		{ return NULL; }
		};

		template<>
		struct dbo_traits<Request> : public dbo_default_traits
		{
			typedef Component IdType;

			static IdType		invalidId()			{ return Component(); }
			static const char*	surrogateIdField()	{ return NULL; }
			static const char*	versionField()		{ return NULL; }
		};

		template<>
		struct dbo_traits<Header> : public dbo_default_traits
		{
			typedef Component IdType;

			static IdType		invalidId()			{ return Component(); }
			static const char*	surrogateIdField()	{ return NULL; }
			static const char*	versionField()		{ return NULL; }
		};
	}
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class Script
{
public:
	Script(const std::string& ScriptName = std::string(), const std::string& Code = std::string()) : ScriptName(ScriptName), Code(Code) {}

	int ScriptID;
	std::string ScriptName;
	boost::optional<std::string> Code;
	Wt::WDateTime VersionDate;
//	Wt::WString VersionDate;

	template <class Action>
	void persist(Action& a)
	{
		Wt::Dbo::field(a, ScriptName, "ScriptName");
		Wt::Dbo::field(a, Code, "Code");
		Wt::Dbo::field(a, VersionDate, "VersionDate");
	}
};

class WebPage
{
public:
	WebPage(int ScriptID = -1, const std::string& URL = std::string()) : ScriptID(ScriptID), URL(URL) {}

	int	PageID;
	int	ScriptID;
	boost::optional<std::string> URL;

	template<class Action>
	void persist(Action& a)
	{
		Wt::Dbo::field(a, ScriptID, "ScriptID");
		Wt::Dbo::field(a, URL, "URL");
	}
};

class RunID
{
public:
	RunID(int mRunID = -1, int ScriptID = -1, resultid_t FirstResultID = -1) : mRunID(mRunID), ScriptID(ScriptID), FirstResultID(FirstResultID) {}

	int mRunID;
	int	ScriptID;
	resultid_t FirstResultID;

	template <class Action>
	void persist(Action& a)
	{
		Wt::Dbo::field(a, ScriptID, "ScriptID");
		Wt::Dbo::field(a, FirstResultID, "FirstResultID");
	}
};

class ResultHeader
{
public:
	ResultHeader(resultid_t ResultID = -1, int mRunID = -1, int ResultCode = -1) : ResultID(ResultID), mRunID(mRunID), ResultCode(ResultCode) {}

	resultid_t ResultID;
	int mRunID;
	int ResultCode;

	template <class Action>
	void persist(Action& a)
	{
		Wt::Dbo::field(a, ResultCode, "ResultCode");
		Wt::Dbo::field(a, mRunID, "RunID");
	}
};

class ResultDetail
{
public:
	ResultDetail(const Component& component = Component(), int ResultCode = -1) : component(component), ResultCode(ResultCode) {}

	Component component;
	int ResultCode;

	template <class Action>
	void persist(Action& a)
	{
		Wt::Dbo::id(a, component, "component");
		Wt::Dbo::field(a, ResultCode, "ResultCode");
	}
};

class Request
{
public:
	Request(const Component& component = Component(), const std::string& mRequest = std::string()) : component(component), mRequest(mRequest) {}

	Component component;
	std::string mRequest;

	template <class Action>
	void persist(Action& a)
	{
		Wt::Dbo::id(a, component, "component");
		Wt::Dbo::field(a, mRequest, "Request");
	}
};

class Header
{
public:
	Header(const Component& component = Component(), const std::string& mHeader = std::string()) : component(component), mHeader(mHeader) {}

	Component component;
	std::string mHeader;

	template <class Action>
	void persist(Action& a)
	{
		Wt::Dbo::id(a, component, "component");
		Wt::Dbo::field(a, mHeader, "Header");
	}
};

//---------------------------------------------------------------------------

#endif
