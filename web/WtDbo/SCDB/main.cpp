//
// http://www.webtoolkit.eu/wt/doc/tutorial/Wt::Dbo/tutorial.html
//
#ifdef _MSC_VER
#pragma warning( disable : 4251 4512 4275 )
#endif

#include "schema.hpp"

#include <Wt/Dbo/Session>
#include <Wt/Dbo/backend/MySQL>

#include <stdexcept>
#include <iostream>
#include <string>

//---------------------------------------------------------------------------

void createTables(Wt::Dbo::Session& session)
{
	try
	{
		session.createTables();
	}
	catch (const std::exception& e)
	{
		std::clog << e.what() << std::endl;
	}
}

void createRecord(Wt::Dbo::Session& session)
{
/*
	// A unit of work happens always within a transaction.
	Wt::Dbo::Transaction transaction(session);

	User user("Joe", "Secret", User::Visitor, 13);
	{
		Wt::Dbo::ptr<User> user1 = session.add(new User(user));

		Wt::Dbo::ptr<Post> post1 = session.add(new Post("Post 1"));
		post1.modify()->user = user1; // or joe.modify()->posts.insert(post);

		Wt::Dbo::ptr<Post> post2 = session.add(new Post("Post 2"));
		post2.modify()->user = user1; // or joe.modify()->posts.insert(post);
	}
	{

		user.role = User::Alien;
		user.password = "Marvin";
		user.karma = 54;
		Wt::Dbo::ptr<User> user2 = session.add(new User(user));
		Wt::Dbo::ptr<Post> post3 = session.add(new Post("Post 3"));
		post3.modify()->user = user2; // or joe.modify()->posts.insert(post);
	}
*/
}

void amend(Wt::Dbo::Session& session)
{
/*
	// A unit of work happens always within a transaction.
	Wt::Dbo::Transaction transaction(session);

	Wt::Dbo::collection<Wt::Dbo::ptr<User>> users = session.find<User>().where("name = ?").bind("Joe");
	if (users.size() > 0)
	{
		for (Wt::Dbo::collection<Wt::Dbo::ptr<User>>::iterator p = users.begin(); p != users.end(); ++p)
		{
			const User& user = **p;

			std::cout << p->id() << std::endl;
//			p->remove();
		}
	}
*/
}

void query(Wt::Dbo::Session& session)
{
	Wt::Dbo::Transaction transaction(session);
	Wt::Dbo::collection<Wt::Dbo::ptr<Script>> scripts = session.find<Script>();

	for (Wt::Dbo::collection<Wt::Dbo::ptr<Script>>::iterator p = scripts.begin(); p != scripts.end(); ++p)
	{
		const Script& script = **p;

		std::cout << "ScriptID = " << p->id() << std::endl;
		std::cout << "ScriptName = " << script.ScriptName << std::endl;
//      std::cout << "VersionDate = " << script.VersionDate.narrow() << std::endl;

		Wt::Dbo::collection<Wt::Dbo::ptr<WebPage>> webpages = session.find<WebPage>().where("ScriptID = ?").bind(p->id());
		for (Wt::Dbo::collection<Wt::Dbo::ptr<WebPage>>::iterator q = webpages.begin(); q != webpages.end(); ++q)
		{
			const WebPage& webpage = **q;

			std::cout << "\tPageID = " << q->id() << std::endl;
			std::cout << "\tURL = " << webpage.URL.get() << std::endl;
		}
	}
}

int main()
{
	try
	{
		// Open DB
		Wt::Dbo::Session session;
		Wt::Dbo::backend::MySQL mysql("SiteConfidence1");
		session.setConnection(mysql);

		// Set ORM mappings
		session.mapClass<Script>("Script");
		session.mapClass<WebPage>("WebPage");
		session.mapClass<RunID>("RunID");
		session.mapClass<ResultHeader>("ResultHeader");
		session.mapClass<ResultDetail>("ResultDetail");
		session.mapClass<ResultDetail>("Request");
		session.mapClass<ResultDetail>("Header");

//		createTables(session);

//		write(session);
//      amend(session);
		query(session);
	}
	catch (const std::exception& e)
	{
		std::clog << e.what() << std::endl;
		return 1;
	}

	return 0;
}
