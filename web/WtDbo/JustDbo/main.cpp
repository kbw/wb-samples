//
// http://www.webtoolkit.eu/wt/doc/tutorial/Wt::Dbo/tutorial.html
//
#ifdef _MSC_VER
#pragma warning( disable : 4251 4512 4275 )
#endif

#include <Wt/Dbo/Dbo>
#include <Wt/Dbo/Session>
#include <Wt/Dbo/backend/MySQL>

#include <boost/optional/optional.hpp>

#include <string>
#include <stdexcept>
#include <iostream>

//---------------------------------------------------------------------------

class User;
class Post;

class User
{
public:
	enum Role { Visitor = 0, Admin = 1, Alien = 42 };

	User(const std::string& name = std::string(), const std::string& password = std::string(), Role role = Visitor, int karma = 0) : name(name), password(password), role(role), karma(karma) {}

	std::string	name;
	std::string	password;
	Role		role;
	int			karma;
	Wt::Dbo::collection<Wt::Dbo::ptr<Post>> posts;

	template <class Action>
	void persist(Action& a)
	{
		Wt::Dbo::field(a, name, "name");
		Wt::Dbo::field(a, password, "password");
		Wt::Dbo::field(a, role, "role");
		Wt::Dbo::field(a, karma, "karma");
		Wt::Dbo::hasMany(a, posts, Wt::Dbo::ManyToOne, "user");
	}
};

class Post
{
public:
	Post(const std::string& s = std::string()) : entry(s) {}

	std::string	entry;
	Wt::Dbo::ptr<User> user;

	template<class Action>
	void persist(Action& a)
	{
		Wt::Dbo::field(a, entry, "entry");
		Wt::Dbo::belongsTo(a, user, "user");
	}
};

//---------------------------------------------------------------------------

void createTables(Wt::Dbo::Session& session)
{
	try
	{
		session.createTables();
	}
	catch (const std::exception& e)
	{
		std::clog << e.what() << std::endl;
	}
}

void write(Wt::Dbo::Session& session)
{
	// A unit of work happens always within a transaction.
	Wt::Dbo::Transaction transaction(session);

	User user("Joe", "Secret", User::Visitor, 13);
	{
		Wt::Dbo::ptr<User> user1 = session.add(new User(user));

		Wt::Dbo::ptr<Post> post1 = session.add(new Post("Post 1"));
		post1.modify()->user = user1; // or joe.modify()->posts.insert(post);

		Wt::Dbo::ptr<Post> post2 = session.add(new Post("Post 2"));
		post2.modify()->user = user1; // or joe.modify()->posts.insert(post);
	}
	{

		user.role = User::Alien;
		user.password = "Marvin";
		user.karma = 54;
		Wt::Dbo::ptr<User> user2 = session.add(new User(user));
		Wt::Dbo::ptr<Post> post3 = session.add(new Post("Post 3"));
		post3.modify()->user = user2; // or joe.modify()->posts.insert(post);
	}
}

void amend(Wt::Dbo::Session& session)
{
	// A unit of work happens always within a transaction.
	Wt::Dbo::Transaction transaction(session);

	Wt::Dbo::collection<Wt::Dbo::ptr<User>> users = session.find<User>().where("name = ?").bind("Joe");
	if (users.size() > 0)
	{
		for (Wt::Dbo::collection<Wt::Dbo::ptr<User>>::iterator p = users.begin(); p != users.end(); ++p)
		{
			const User& user = **p;

			std::cout << p->id() << std::endl;
//			p->remove();
		}
	}
}

int main()
{
	try
	{
		// Open DB
		Wt::Dbo::Session session;
		Wt::Dbo::backend::MySQL mysql("wbtest2");
		session.setConnection(mysql);

		// Set ORM mappings
		session.mapClass<User>("user");
		session.mapClass<Post>("post");

		createTables(session);

		write(session);
		amend(session);
	}
	catch (const std::exception& e)
	{
		std::clog << e.what() << std::endl;
		return 1;
	}

	return 0;
}
