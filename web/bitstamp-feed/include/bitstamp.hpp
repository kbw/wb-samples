#pragma once

#include <simple-websocket-server/client_wss.hpp>

#include <atomic>
#include <memory>
#include <string>
#include <thread>

// BitStamp ----------------------------------------------------------

class BitStamp {
	inline static const std::string host{"ws.bitstamp.net"};

	using WssClient = SimpleWeb::SocketClient<SimpleWeb::WSS>;
	WssClient client_;
	std::shared_ptr<WssClient::Connection> connection_;
	std::atomic<bool> connection_ready_;
	std::thread worker_;

public:
	BitStamp();
	~BitStamp();

	void subscribe(const std::string& symbol);
	void unsubscribe(const std::string& symbol);
};
