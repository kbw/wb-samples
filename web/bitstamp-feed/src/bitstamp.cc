#include "bitstamp.hpp"

#include <algorithm>
#include <optional>
#include <thread>

// BitStamp ----------------------------------------------------------

BitStamp::BitStamp() : client_{host, false} {
	worker_ = std::thread{[&]() {
		client_.on_message = [&](std::shared_ptr<WssClient::Connection> connection,
								 std::shared_ptr<WssClient::InMessage> in_message) {
			const auto& payload = in_message->string();
			std::cout << payload << '\n';
		};

		client_.on_open = [&](std::shared_ptr<WssClient::Connection> connection) {
			std::cout << "BitStamp: Opened connection\n";
			connection_ = connection;
			connection_ready_ = true;
		};

		client_.on_close = [&](std::shared_ptr<WssClient::Connection> connection,
							   int status,
							   const std::string & reason) {
			std::cout << "BitStamp: Closed connection with status code " << reason << '\n';
			connection_ready_ = false;
			connection_.reset();;
		};

		// See http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference.html, Error Codes for error code meanings
		client_.on_error = [&](std::shared_ptr<WssClient::Connection> connection,
							   const SimpleWeb::error_code &ec) {
			std::cerr << "BitStamp: Error: " << ec.value() << ", error message: " << ec.message() << '\n';
		};

		std::cout << "BitStamp: wss client start" << '\n';
		client_.start();
	}};
}

BitStamp::~BitStamp() {
	client_.stop();
	if (worker_.joinable())
		worker_.join();
}

void BitStamp::subscribe(const std::string& symbol) {
	if (connection_ready_ && connection_) {
		const std::array<std::string, 3> subscriptions{
			R"({"event":"bts:subscribe","data":{"channel":"order_book_)" + symbol + R"("}})",
			R"({"event":"bts:subscribe","data":{"channel":"live_orders_)" + symbol + R"("}})",
			R"({"event":"bts:subscribe","data":{"channel":"live_trades_)" + symbol + R"("}})"
		};
		for (const auto& subscription : subscriptions) {
			std::cout << subscription << '\n';
			connection_->send(subscription);
		}
	}
}

void BitStamp::unsubscribe(const std::string& symbol) {
	if (connection_ready_ && connection_) {
		const std::array<std::string, 3> subscriptions{
			R"({"event":"bts:unsubscribe","data":{"channel":"order_book_)" + symbol + R"("}})",
			R"({"event":"bts:unsubscribe","data":{"channel":"live_orders_)" + symbol + R"("}})",
			R"({"event":"bts:unsubscribe","data":{"channel":"live_trades_)" + symbol + R"("}})"
		};
		for (const auto& subscription : subscriptions) {
			std::cout << subscription << '\n';
			connection_->send(subscription);
		}
	}
}
