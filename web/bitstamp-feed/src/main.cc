#include "bitstamp.hpp"

int main() {
	BitStamp feed;
	std::this_thread::sleep_for(std::chrono::seconds(1)); // allow feed to intialize

	feed.subscribe("btcusd");
	std::this_thread::sleep_for(std::chrono::milliseconds(1000)); // run for 1 second

	feed.unsubscribe("btcusd");
	std::this_thread::sleep_for(std::chrono::milliseconds(100)); // allow cleanup and stop
}
