#pragma once

#include <variant>

namespace ka::net {

struct Connection {
	using socket_t = int;
	using Type = std::variant<socket_t>;

	Type type;

	static Connection create() {
		return Connection{-1};
	}
};

}  // namespace ka::net
