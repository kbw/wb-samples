#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Shared_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Box.H>

int main() {
    fl_register_images();                       // initialize image lib
    Fl_Window     win(720,486);                 // make a window
    Fl_Box        box(10,10,720-20,486-20);     // widget that will contain image
    Fl_JPEG_Image jpg("Ken-Thompson-Land-Dennis-RitchieR-creators-of-UNIX.jpg");      // load jpeg image into ram
    box.image(jpg);                             // attach jpg image to box
    win.show();
    return(Fl::run());
}
