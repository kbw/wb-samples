#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Multiline_Output.H>

int main(int argc, char **argv) {
	Fl_Window* w = new Fl_Window(0, 0, 560, 560);
		Fl_Multiline_Output* output = new Fl_Multiline_Output(10,10,500,500);

		// CLEAR OUTPUT
		output->value("");

		// SET OUTPUT
		output->value("one\ntwo");          // fltk does strdup() internally       

		// APPENDING OUTPUT
		//     Would be nice if this were an add() method in fltk ;)
		//
		const char *news = "Add this text";
		char *cat = (char*)malloc(strlen(output->value()) + strlen(news) + 1);
		strcpy(cat, output->value());
		strcat(cat, news);
		output->value(cat);
//		free(cat);
	w->end();
	w->show(argc, argv);
	return Fl::run();
}
