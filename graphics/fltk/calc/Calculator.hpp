#pragma once

class CalcModel;
class CalcWindow;

class Calculator
{
	CalcModel*	model;
	CalcWindow*	view;

	Calculator(const Calculator &);
	Calculator& operator=(const Calculator &);

public:
	Calculator(int argc, char** argv);
};
