#include "Calculator.hpp"
#include "CalcModel.hpp"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Output.H>
#include "FL/Fl_Matte_Button.H"


namespace
{
	const int BUTTON_X_WIDTH = 40;
	const int X_SEPERATION = 5;
	const int X_WIDTH = BUTTON_X_WIDTH + X_SEPERATION;
	const int X_OFFSET = 35;
	const int X0	= X_OFFSET;
	const int X1	= X0 + X_WIDTH;
	const int X2	= X1 + X_WIDTH;
	const int X3	= X2 + X_WIDTH;
	const int X4	= X3 + X_WIDTH;
	const int X5	= X4 + X_WIDTH;
	const int X6	= X5 + X_WIDTH;
	const int X7	= X6 + X_WIDTH;
	const int X8	= X7 + X_WIDTH;
	const int X9	= X8 + X_WIDTH;
	const int X10	= X9 + X_WIDTH;

	const int Y_HEIGHT = 25;
	const int Y_OFFSET = 25;
	const int Y0	= Y_OFFSET;
	const int Y1	= Y0 + Y_HEIGHT;
	const int Y2	= Y1 + Y_HEIGHT;
	const int Y3	= Y2 + Y_HEIGHT;
	const int Y4	= Y3 + Y_HEIGHT;


	class CalcDisplay : public Fl_Output
	{
	public:
		typedef Fl_Output inherited;

		CalcDisplay(int x, int y) :
			Fl_Output(x, y, 300, 25)
		{
//			textfont(FL_COURIER_BOLD);	//fixed width font
//			textsize(24);
			value("Keith, Carl, Stephanie & Yuri");
		}

		int handle(int event)
		{
			int ret = inherited::handle(event);
			switch (event)
			{
			case FL_KEYDOWN:
				if (ret == 1)
					ret = 0;
				break;
			}

			return ret;
		}
	};

	class CalcButton : public Fl_Matte_Button
	{
	public:
		typedef Fl_Button inherited;

		CalcButton(int x, int y, int dx, int dy, const char* label) :
			Fl_Matte_Button(x, y, dx, dy, label)
		{
//			box(FL_PLASTIC_THIN_UP_BOX);
		}
	};

/*
	class CalcButton : public Fl_Button
	{
		int m_colour;

	public:
		typedef Fl_Button inherited;

		CalcButton(int x, int y, int dx, int dy, const char* label) :
			Fl_Button(x, y, dx, dy, label),
			m_colour(FL_BACKGROUND_COLOR)
		{
			box(FL_PLASTIC_THIN_UP_BOX);
		}
	};
 */

	class StdButton : public CalcButton
	{
	public:
		typedef StdButton inherited;

		StdButton(int x, int y, const char* label) :
			CalcButton(x, y, BUTTON_X_WIDTH, 20, label)
		{
		}
	};
}

	struct Info
	{
		CalcDisplay*	display;

		CalcButton*	f;
		CalcButton*	g;

		CalcButton*	plus;
		CalcButton*	minus;
		CalcButton*	multi;
		CalcButton*	div;
		CalcButton*	enter;

		CalcButton*	key0;
		CalcButton*	key1;
		CalcButton*	key2;
		CalcButton*	key3;
		CalcButton*	key4;
		CalcButton*	key5;
		CalcButton*	key6;
		CalcButton*	key7;
		CalcButton*	key8;
		CalcButton*	key9;
	};

class CalcWindow : public Fl_Window
{
	Info*	pInfo;

public:
	typedef Fl_Window inherited;

	CalcWindow() :
		Fl_Window(0, 60, 500, 200),
		pInfo(new Info())
	{
		begin();
			pInfo->display = new CalcDisplay(180, 10);

			pInfo->f = new StdButton(X0, 100, "F");
			pInfo->f->color(FL_YELLOW);
			pInfo->g = new StdButton(X1, 100, "G");
			pInfo->g->color(FL_CYAN);

			pInfo->plus = new StdButton(440, 50, "+");
			pInfo->minus = new StdButton(440, 75, "-");
			pInfo->multi = new StdButton(440, 100, "x");
			pInfo->div = new StdButton(440, 125, "/");

			pInfo->key9 = new StdButton(395, 50, "9");
			pInfo->key8 = new StdButton(350, 50, "8");
			pInfo->key7 = new StdButton(305, 50, "7");
			pInfo->key6 = new StdButton(395, 75, "6");
			pInfo->key5 = new StdButton(350, 75, "5");
			pInfo->key4 = new StdButton(305, 75, "4");
			pInfo->key3 = new StdButton(395, 100, "3");
			pInfo->key2 = new StdButton(350, 100, "2");
			pInfo->key1 = new StdButton(305, 100, "1");
			pInfo->key0 = new StdButton(395, 125, "0");
			pInfo->enter = new CalcButton(305, 125, 85, 0, "Enter");
		end();
	}
};

Calculator::Calculator(int argc, char** argv) :
	model(new CalcModel()),
	view(new CalcWindow())
{
	view->show(argc, argv);
}
