#include "CalcModel.hpp"
#include <vector>


//----------------------------------------------------------------------------

class CalcModelImp
{
    enum EConstants { EStackSize = 4 };

    std::vector<CalcModel::number_t> m_stack;

    CalcModelImp(const CalcModelImp &);
    CalcModelImp& operator=(const CalcModelImp &);

public:
    CalcModelImp();
    ~CalcModelImp();

    void clear();
    CalcModel::number_t peek() const;
    void push(CalcModel::number_t n);
    void pop();

    void plus();
    void minus();

    CalcModel::number_t add(CalcModel::number_t n);
    CalcModel::number_t subtract(CalcModel::number_t n);
};

CalcModelImp::CalcModelImp() :
	m_stack(EStackSize)
{
	clear();
}

CalcModelImp::~CalcModelImp()
{
}

void CalcModelImp::clear()
{
	for (std::size_t i = 0; i != EStackSize; ++i)
		m_stack[i] = 0.0;
}

CalcModel::number_t CalcModelImp::peek() const
{
	return m_stack[0];
}

void CalcModelImp::push(CalcModel::number_t n)
{
	for (std::size_t i = 1; i != EStackSize; ++i)
		m_stack[i] = m_stack[i - 1];
	m_stack[0] = n;
}

void CalcModelImp::pop()
{
	for (std::size_t i = 1; i != EStackSize; ++i)
		m_stack[i - 1] = m_stack[i];
}

void CalcModelImp::plus()
{
	CalcModel::number_t op2 = peek();
	pop();
	CalcModel::number_t op1 = peek();
	pop();
	push(op1 + op2);
}

void CalcModelImp::minus()
{
	CalcModel::number_t op2 = peek();
	pop();
	CalcModel::number_t op1 = peek();
	pop();
	push(op1 + op2);
}

CalcModel::number_t CalcModelImp::add(CalcModel::number_t n)
{
	push(n);
	plus();
	return peek();
}

CalcModel::number_t CalcModelImp::subtract(CalcModel::number_t n)
{
	push(n);
	minus();
	return peek();
}


//----------------------------------------------------------------------------

CalcModel::CalcModel() :
	pImp(new CalcModelImp())
{
}

CalcModel::~CalcModel()
{
	delete pImp;
}

void CalcModel::clear()
{
	pImp->clear();
}

CalcModel::number_t CalcModel::peek() const
{
	return pImp->peek();
}

void CalcModel::push(CalcModel::number_t n)
{
	pImp->push(n);
}

void CalcModel::pop()
{
	pImp->pop();
}

void CalcModel::plus()
{
	pImp->plus();
}

void CalcModel::minus()
{
	pImp->minus();
}

CalcModel::number_t CalcModel::add(CalcModel::number_t n)
{
	pImp->add(n);
}

CalcModel::number_t CalcModel::subtract(CalcModel::number_t n)
{
	return pImp->subtract(n);
}
