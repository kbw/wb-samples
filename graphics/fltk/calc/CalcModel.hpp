#pragma once

class CalcModelImp;

class CalcModel
{
	CalcModelImp*	pImp;

	CalcModel(const CalcModel &);
	CalcModel& operator=(const CalcModel &);

public:
	typedef double number_t;

	CalcModel();
	virtual	~CalcModel();

	virtual	void clear();
	virtual	number_t peek() const;
	virtual	void push(number_t n);
	virtual	void pop();

	virtual	void plus();
	virtual	void minus();

	virtual	number_t add(number_t n);
	virtual	number_t subtract(number_t n);
};
