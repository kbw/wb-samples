#include "Calculator.hpp"
#include <FL/Fl.H>

int main(int argc, char* argv[])
{
	Calculator calc(argc, argv);
	return Fl::run();
}
