/***************************************************************************
 *   Copyright (C) 2013 by Keith Williams <keith.dev.uk@gmail.com>                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef HELLOKDE_H
#define HELLOKDE_H


#include <KXmlGuiWindow>

#include "ui_prefs_base.h"

class HelloKDEView;
class QPrinter;
class KToggleAction;
class KUrl;

/**
 * This class serves as the main window for HelloKDE.  It handles the
 * menus, toolbars and status bars.
 *
 * @short Main window class
 * @author Keith Williams <keith.dev.uk@gmail.com>
 * @version 0.1
 */
class HelloKDE : public KXmlGuiWindow
{
    Q_OBJECT
public:
    /**
     * Default Constructor
     */
    HelloKDE();

    /**
     * Default Destructor
     */
    virtual ~HelloKDE();

private slots:
    void fileNew();
    void optionsPreferences();

private:
    void setupActions();

private:
    Ui::prefs_base ui_prefs_base ;
    HelloKDEView *m_view;

    QPrinter   *m_printer;
    KToggleAction *m_toolbarAction;
    KToggleAction *m_statusbarAction;
};

#endif // _HELLOKDE_H_
