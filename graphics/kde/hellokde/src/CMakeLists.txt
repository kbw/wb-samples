set(hellokde_SRCS
   hellokde.cpp
   main.cpp
   hellokdeview.cpp
 )

kde4_add_ui_files(hellokde_SRCS hellokdeview_base.ui prefs_base.ui)

kde4_add_kcfg_files(hellokde_SRCS settings.kcfgc )

kde4_add_executable(hellokde ${hellokde_SRCS})

target_link_libraries(hellokde ${KDE4_KDEUI_LIBS} )

install(TARGETS hellokde ${INSTALL_TARGETS_DEFAULT_ARGS} )


########### install files ###############

install( PROGRAMS hellokde.desktop  DESTINATION ${XDG_APPS_INSTALL_DIR} )
install( FILES hellokde.kcfg  DESTINATION  ${KCFG_INSTALL_DIR} )
install( FILES hellokdeui.rc  DESTINATION  ${DATA_INSTALL_DIR}/hellokde )
