#include <SFML/Graphics.hpp>
#include <memory>
#include <iostream>

void Dispatch(sf::Window* window);

void OnClose(sf::Window& window, sf::Event& event);
void OnResized(sf::Window& window, sf::Event& event);
void OnLostFocus(sf::Window& window, sf::Event& event);
void OnGainedFocus(sf::Window& window, sf::Event& event);

void OnKeyPressed(sf::Window& window, sf::Event& event);
void OnKeyReleased(sf::Window& window, sf::Event& event);
void OnTextEntered(sf::Window& window, sf::Event& event);

void OnMouseWheelScrolled(sf::Window& window, sf::Event& event);
void OnMouseButtonPressed(sf::Window& window, sf::Event& event);
void OnMouseMoved(sf::Window& window, sf::Event& event);
void OnMouseEntered(sf::Window& window, sf::Event& event);
void OnMouseLeft(sf::Window& window, sf::Event& event);

int main()
{
	std::unique_ptr<sf::RenderWindow> w(new sf::RenderWindow(sf::VideoMode(800, 600), "My window", sf::Style::Titlebar | sf::Style::Close | sf::Style::Resize));
	while (w->isOpen())
	{
		Dispatch(w.get());

		// clear the window with black color
		w->clear(sf::Color::Black);

		// draw everything here...
		// window->draw(...);

		// end the current frame
		w->display();
	}

	return 0;
}

void Dispatch(sf::Window* w)
{
	sf::Event e;
	while (w->pollEvent(e))
	{
		switch (e.type)
		{
		case sf::Event::Closed:			OnClose(*w, e);			break;
		case sf::Event::Resized:		OnResized(*w, e);		break;
		case sf::Event::LostFocus:		OnLostFocus(*w, e);		break;
		case sf::Event::GainedFocus:	OnGainedFocus(*w, e);	break;

		case sf::Event::KeyPressed:		OnKeyPressed(*w, e);	break;
		case sf::Event::KeyReleased:	OnKeyPressed(*w, e);	break;
		case sf::Event::TextEntered:	OnTextEntered(*w, e);	break;

		case sf::Event::MouseWheelScrolled:	OnMouseWheelScrolled(*w, e);break;
		case sf::Event::MouseButtonPressed:	OnMouseButtonPressed(*w, e);break;
		case sf::Event::MouseMoved:			OnMouseMoved(*w, e);		break;
		case sf::Event::MouseEntered:		OnMouseEntered(*w, e);		break;
		case sf::Event::MouseLeft:			OnMouseLeft(*w, e);			break;

		default:
			;
		}
	}
}

void OnClose(sf::Window& window, sf::Event& event)
{
	 window.close();
}

void OnResized(sf::Window& window, sf::Event& event)
{
	std::clog << "new width: " << event.size.width << std::endl;
	std::clog << "new height: " << event.size.height << std::endl;
}

void OnLostFocus(sf::Window& window, sf::Event& event)
{
	std::clog << "lost focus" << std::endl;
}

void OnGainedFocus(sf::Window& window, sf::Event& event)
{
	std::clog << "have focus" << std::endl;
}

void OnKeyPressed(sf::Window& window, sf::Event& event)
{
	if (event.key.code == sf::Keyboard::Escape)
	{
		std::clog << "the escape key was pressed" << std::endl;
		std::clog << "control:" << event.key.control << std::endl;
		std::clog << "alt:" << event.key.alt << std::endl;
		std::clog << "shift:" << event.key.shift << std::endl;
		std::clog << "system:" << event.key.system << std::endl;
	}
}

void OnKeyReleased(sf::Window& window, sf::Event& event)
{
	if (event.key.code == sf::Keyboard::Escape)
	{
		std::clog << "the escape key was pressed" << std::endl;
		std::clog << "control:" << event.key.control << std::endl;
		std::clog << "alt:" << event.key.alt << std::endl;
		std::clog << "shift:" << event.key.shift << std::endl;
		std::clog << "system:" << event.key.system << std::endl;
	}
}

void OnTextEntered(sf::Window& window, sf::Event& event)
{
	if (event.text.unicode < 128)
	{
		char ch =  static_cast<char>(event.text.unicode);
		std::clog << "ASCII character typed: " << ch << std::endl;
	}
}

void OnMouseWheelScrolled(sf::Window& window, sf::Event& event)
{
	if (event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel)
	{
		std::clog << "wheel type: vertical" << std::endl;
	}
	else if (event.mouseWheelScroll.wheel == sf::Mouse::HorizontalWheel)
	{
		std::clog << "wheel type: horizontal" << std::endl;
	}
	else
	{
		std::clog << "wheel type: unknown" << std::endl;
	}

	std::clog << "wheel movement: " << event.mouseWheelScroll.delta << std::endl;
	std::clog << "mouse x: " << event.mouseWheelScroll.x << std::endl;
	std::clog << "mouse y: " << event.mouseWheelScroll.y << std::endl;
}

void OnMouseButtonPressed(sf::Window& window, sf::Event& event)
{
	if (event.mouseButton.button == sf::Mouse::Right)
	{
		std::clog << "the right button was pressed" << std::endl;
	}
}

void OnMouseMoved(sf::Window& window, sf::Event& event)
{
	std::clog << "new mouse x: " << event.mouseMove.x << std::endl;
	std::clog << "new mouse y: " << event.mouseMove.y << std::endl;
}

void OnMouseEntered(sf::Window& window, sf::Event& event)
{
	std::clog << "the mouse cursor has entered the window" << std::endl;
}

void OnMouseLeft(sf::Window& window, sf::Event& event)
{
	std::clog << "the mouse cursor has left the window" << std::endl;
}
