#pragma once

#include "GLWindow.hpp"

class myGLWindow : public GLWindow
{
public:
	myGLWindow(sf::VideoMode mode, const sf::String& title, sf::Uint32 style = sf::Style::Default, const sf::ContextSettings& settings = sf::ContextSettings());
	virtual ~myGLWindow();

	virtual	void OnResized(sf::Event& event);
	virtual	void OnLostFocus(sf::Event& event);
	virtual	void OnGainedFocus(sf::Event& event);

	virtual	void OnKeyPressed(sf::Event& event);
	virtual	void OnKeyReleased(sf::Event& event);
	virtual	void OnTextEntered(sf::Event& event);

	virtual	void OnMouseWheelScrolled(sf::Event& event);
	virtual	void OnMouseButtonPressed(sf::Event& event);
	virtual	void OnMouseMoved(sf::Event& event);
	virtual	void OnMouseEntered(sf::Event& event);
	virtual	void OnMouseLeft(sf::Event& event);
};
