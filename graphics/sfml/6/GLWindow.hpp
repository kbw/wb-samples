#pragma once
#include "Window.hpp"
#include <SFML/OpenGL.hpp>

class GLWindow : public Window
{
public:
	typedef Window inherited;

	GLWindow(sf::VideoMode mode, const sf::String& title, sf::Uint32 style = sf::Style::Default, const sf::ContextSettings& settings = sf::ContextSettings());

	bool isOpen();
	void Clear(const sf::Color& color = sf::Color(0, 0, 0, 255));
	void OnClose(sf::Event& event);
	void OnResized(sf::Event& event);

private:
	bool m_running;
};
