#include "myGLWindow.hpp"
#include <stdexcept>
#include <iostream>

//---------------------------------------------------------------------------
//
myGLWindow::myGLWindow(sf::VideoMode mode, const sf::String& title, sf::Uint32 style, const sf::ContextSettings& settings) :
	GLWindow(mode, title, style, settings)
{
	sf::Texture texture;
	if (!texture.loadFromFile("image.png"))
	{
		throw std::runtime_error("cannot load texture");
	}
}

myGLWindow::~myGLWindow()
{
}

void myGLWindow::OnResized(sf::Event& event)
{
	std::clog << "new width: " << event.size.width << std::endl;
	std::clog << "new height: " << event.size.height << std::endl;
}

void myGLWindow::OnLostFocus(sf::Event& event)
{
	std::clog << "lost focus" << std::endl;
}

void myGLWindow::OnGainedFocus(sf::Event& event)
{
	std::clog << "have focus" << std::endl;
}

void myGLWindow::OnKeyPressed(sf::Event& event)
{
	if (event.key.code == sf::Keyboard::Escape)
	{
		std::clog << "the escape key was pressed" << std::endl;
		std::clog << "control:" << event.key.control << std::endl;
		std::clog << "alt:" << event.key.alt << std::endl;
		std::clog << "shift:" << event.key.shift << std::endl;
		std::clog << "system:" << event.key.system << std::endl;
	}
}

void myGLWindow::OnKeyReleased(sf::Event& event)
{
	if (event.key.code == sf::Keyboard::Escape)
	{
		std::clog << "the escape key was pressed" << std::endl;
		std::clog << "control:" << event.key.control << std::endl;
		std::clog << "alt:" << event.key.alt << std::endl;
		std::clog << "shift:" << event.key.shift << std::endl;
		std::clog << "system:" << event.key.system << std::endl;
	}
}

void myGLWindow::OnTextEntered(sf::Event& event)
{
	if (event.text.unicode < 128)
	{
		char ch =  static_cast<char>(event.text.unicode);
		std::clog << "ASCII character typed: " << ch << std::endl;
	}
}

void myGLWindow::OnMouseWheelScrolled(sf::Event& event)
{
	if (event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel)
	{
		std::clog << "wheel type: vertical" << std::endl;
	}
	else if (event.mouseWheelScroll.wheel == sf::Mouse::HorizontalWheel)
	{
		std::clog << "wheel type: horizontal" << std::endl;
	}
	else
	{
		std::clog << "wheel type: unknown" << std::endl;
	}

	std::clog << "wheel movement: " << event.mouseWheelScroll.delta << std::endl;
	std::clog << "mouse x: " << event.mouseWheelScroll.x << std::endl;
	std::clog << "mouse y: " << event.mouseWheelScroll.y << std::endl;
}

void myGLWindow::OnMouseButtonPressed(sf::Event& event)
{
	if (event.mouseButton.button == sf::Mouse::Right)
	{
		std::clog << "the right button was pressed" << std::endl;
	}
}

void myGLWindow::OnMouseMoved(sf::Event& event)
{
	std::clog << "new mouse x: " << event.mouseMove.x << std::endl;
	std::clog << "new mouse y: " << event.mouseMove.y << std::endl;
}

void myGLWindow::OnMouseEntered(sf::Event& event)
{
	std::clog << "the mouse cursor has entered the window" << std::endl;
}

void myGLWindow::OnMouseLeft(sf::Event& event)
{
	std::clog << "the mouse cursor has left the window" << std::endl;
}
