#pragma once

#include <SFML/Graphics.hpp>
#include <memory>

class Window
{
public:
	Window(sf::VideoMode mode, const sf::String& title, sf::Uint32 style = sf::Style::Default, const sf::ContextSettings& settings = sf::ContextSettings());
	virtual ~Window();

	sf::RenderWindow& GetWindow()	{ return *m_win; }
	operator sf::RenderWindow& ()	{ return GetWindow(); }

	virtual	bool isOpen();

	virtual	void Dispatch();
	virtual	void Draw();
	virtual	void Display();
	virtual	void Clear(const sf::Color& color = sf::Color(0, 0, 0, 255));

	virtual	void OnClose(sf::Event& event);
	virtual	void OnResized(sf::Event& event);
	virtual	void OnLostFocus(sf::Event& event);
	virtual	void OnGainedFocus(sf::Event& event);

	virtual	void OnKeyPressed(sf::Event& event);
	virtual	void OnKeyReleased(sf::Event& event);
	virtual	void OnTextEntered(sf::Event& event);

	virtual	void OnMouseWheelScrolled(sf::Event& event);
	virtual	void OnMouseButtonPressed(sf::Event& event);
	virtual	void OnMouseMoved(sf::Event& event);
	virtual	void OnMouseEntered(sf::Event& event);
	virtual	void OnMouseLeft(sf::Event& event);

private:
	std::unique_ptr<sf::RenderWindow> m_win;
};
