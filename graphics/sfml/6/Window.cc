#include "Window.hpp"

//---------------------------------------------------------------------------
//
Window::Window(sf::VideoMode mode, const sf::String& title, sf::Uint32 style, const sf::ContextSettings& settings) :
	m_win(new sf::RenderWindow(mode, title, style, settings))
{
}

Window::~Window()
{
}

bool Window::isOpen()
{
	return GetWindow().isOpen();
}

void Window::Draw()
{
}

void Window::Display()
{
	return GetWindow().display();
}

void Window::Clear(const sf::Color& color)
{
	GetWindow().clear(color);
}

void Window::Dispatch()
{
	sf::Event event;
	while (GetWindow().pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:			OnClose(event);			break;
		case sf::Event::Resized:		OnResized(event);		break;
		case sf::Event::LostFocus:		OnLostFocus(event);		break;
		case sf::Event::GainedFocus:	OnGainedFocus(event);	break;

		case sf::Event::KeyPressed:		OnKeyPressed(event);	break;
		case sf::Event::KeyReleased:	OnKeyPressed(event);	break;
		case sf::Event::TextEntered:	OnTextEntered(event);	break;

		case sf::Event::MouseWheelScrolled:	OnMouseWheelScrolled(event);	break;
		case sf::Event::MouseButtonPressed:	OnMouseButtonPressed(event);	break;
		case sf::Event::MouseMoved:			OnMouseMoved(event);			break;
		case sf::Event::MouseEntered:		OnMouseEntered(event);			break;
		case sf::Event::MouseLeft:			OnMouseLeft(event);				break;

		default:
			;
		}
	}
}

void Window::OnClose(sf::Event& event)
{
	 GetWindow().close();
}

void Window::OnResized(sf::Event& event)
{
}

void Window::OnLostFocus(sf::Event& event)
{
}

void Window::OnGainedFocus(sf::Event& event)
{
}

void Window::OnKeyPressed(sf::Event& event)
{
}

void Window::OnKeyReleased(sf::Event& event)
{
}

void Window::OnTextEntered(sf::Event& event)
{
}

void Window::OnMouseWheelScrolled(sf::Event& event)
{
}

void Window::OnMouseButtonPressed(sf::Event& event)
{
}

void Window::OnMouseMoved(sf::Event& event)
{
}

void Window::OnMouseEntered(sf::Event& event)
{
}

void Window::OnMouseLeft(sf::Event& event)
{
}
