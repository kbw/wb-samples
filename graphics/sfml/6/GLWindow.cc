#include "GLWindow.hpp"

//---------------------------------------------------------------------------
//
GLWindow::GLWindow(sf::VideoMode mode, const sf::String& title, sf::Uint32 style, const sf::ContextSettings& settings) :
	Window(mode, title, style, settings)
{
	glEnable(GL_TEXTURE_2D);
	m_running = inherited::isOpen();
}

bool GLWindow::isOpen()
{
	return m_running;
}

void GLWindow::Clear(const sf::Color& color)
{
	// clear the buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GLWindow::OnClose(sf::Event& event)
{
	m_running = false;
}

void GLWindow::OnResized(sf::Event& event)
{
	// adjust the viewport when the window is resized
	glViewport(0, 0, event.size.width, event.size.height);
}
