#include "Window.hpp"
#include <iostream>

//---------------------------------------------------------------------------
//
Window::Window(sf::VideoMode mode, const sf::String& title, sf::Uint32 style, const sf::ContextSettings& settings) :
	m_win(new sf::RenderWindow(mode, title, style, settings))
{
}

Window::~Window()
{
}

bool Window::isOpen()
{
	return GetWindow().isOpen();
}

void Window::Draw()
{
}

void Window::Display()
{
	return GetWindow().display();
}

void Window::Clear(const sf::Color& color)
{
	GetWindow().clear(color);
}

void Window::Dispatch()
{
	sf::Event event;
	while (GetWindow().pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:			OnClose(event);			break;
		case sf::Event::Resized:		OnResized(event);		break;
		case sf::Event::LostFocus:		OnLostFocus(event);		break;
		case sf::Event::GainedFocus:	OnGainedFocus(event);	break;

		case sf::Event::KeyPressed:		OnKeyPressed(event);	break;
		case sf::Event::KeyReleased:	OnKeyPressed(event);	break;
		case sf::Event::TextEntered:	OnTextEntered(event);	break;

		case sf::Event::MouseWheelScrolled:	OnMouseWheelScrolled(event);	break;
		case sf::Event::MouseButtonPressed:	OnMouseButtonPressed(event);	break;
		case sf::Event::MouseMoved:			OnMouseMoved(event);			break;
		case sf::Event::MouseEntered:		OnMouseEntered(event);			break;
		case sf::Event::MouseLeft:			OnMouseLeft(event);				break;

		default:
			;
		}
	}
}

void Window::OnClose(sf::Event& event)
{
	 GetWindow().close();
}

void Window::OnResized(sf::Event& event)
{
	std::clog << "new width: " << event.size.width << std::endl;
	std::clog << "new height: " << event.size.height << std::endl;
}

void Window::OnLostFocus(sf::Event& event)
{
	std::clog << "lost focus" << std::endl;
}

void Window::OnGainedFocus(sf::Event& event)
{
	std::clog << "have focus" << std::endl;
}

void Window::OnKeyPressed(sf::Event& event)
{
	if (event.key.code == sf::Keyboard::Escape)
	{
		std::clog << "the escape key was pressed" << std::endl;
		std::clog << "control:" << event.key.control << std::endl;
		std::clog << "alt:" << event.key.alt << std::endl;
		std::clog << "shift:" << event.key.shift << std::endl;
		std::clog << "system:" << event.key.system << std::endl;
	}
}

void Window::OnKeyReleased(sf::Event& event)
{
	if (event.key.code == sf::Keyboard::Escape)
	{
		std::clog << "the escape key was pressed" << std::endl;
		std::clog << "control:" << event.key.control << std::endl;
		std::clog << "alt:" << event.key.alt << std::endl;
		std::clog << "shift:" << event.key.shift << std::endl;
		std::clog << "system:" << event.key.system << std::endl;
	}
}

void Window::OnTextEntered(sf::Event& event)
{
	if (event.text.unicode < 128)
	{
		char ch =  static_cast<char>(event.text.unicode);
		std::clog << "ASCII character typed: " << ch << std::endl;
	}
}

void Window::OnMouseWheelScrolled(sf::Event& event)
{
	if (event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel)
	{
		std::clog << "wheel type: vertical" << std::endl;
	}
	else if (event.mouseWheelScroll.wheel == sf::Mouse::HorizontalWheel)
	{
		std::clog << "wheel type: horizontal" << std::endl;
	}
	else
	{
		std::clog << "wheel type: unknown" << std::endl;
	}

	std::clog << "wheel movement: " << event.mouseWheelScroll.delta << std::endl;
	std::clog << "mouse x: " << event.mouseWheelScroll.x << std::endl;
	std::clog << "mouse y: " << event.mouseWheelScroll.y << std::endl;
}

void Window::OnMouseButtonPressed(sf::Event& event)
{
	if (event.mouseButton.button == sf::Mouse::Right)
	{
		std::clog << "the right button was pressed" << std::endl;
	}
}

void Window::OnMouseMoved(sf::Event& event)
{
	std::clog << "new mouse x: " << event.mouseMove.x << std::endl;
	std::clog << "new mouse y: " << event.mouseMove.y << std::endl;
}

void Window::OnMouseEntered(sf::Event& event)
{
	std::clog << "the mouse cursor has entered the window" << std::endl;
}

void Window::OnMouseLeft(sf::Event& event)
{
	std::clog << "the mouse cursor has left the window" << std::endl;
}
