#include "GLWindow.hpp"

int main()
{
	std::unique_ptr<Window> w(new GLWindow(sf::VideoMode(800, 600), "OpenGL", sf::Style::Titlebar | sf::Style::Close | sf::Style::Resize));
	while (w->isOpen())
	{
		w->Dispatch();

		// clear the window with black color
		w->Clear(sf::Color::Black);

		// draw everything here...
		// window.draw(...);

		// end the current frame
		w->Display();
	}

	return 0;
}
