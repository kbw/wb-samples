#import <Foundation/Foundation.h>

int main (int argc, const char * argv[]) {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    NSLog(@"PathSample: start");

    // insert code here...

    NSLog(@"PathSample: stop");
    [pool drain];
    return 0;
}
