#!/bin/bash
TARGET=kam

HOME=`pwd`
REPOS=$HOME/src
BUILD=$HOME/build
PREFIX=/opt/$TARGET

rm -rf $REPOS
mkdir -p $REPOS

rm -rf $BUILD
mkdir -p $BUILD

sudo ln -s $BUILD $PREFIX
mkdir -p $PREFIX/etc/$TARGET/

rm -rf $BUILD/logs
mkdir -p $BUILD/logs

cd $REPOS
git clone https://github.com/ruby/ruby.git

cd $REPOS/ruby
git checkout ruby_2_6
git clean -fdx
autoconf
./configure --prefix=$PREFIX --exec-prefix=$PREFIX --disable-install-doc --disable-install-rdoc
make install


export PATH=$PREFIX/bin:$PATH
export LD_LIBRARY_PATH=$PREFIX/lib

GEM=$PREFIX/bin/gem

$GEM source -r https://rubygems.org/
$GEM source -a https://rubygems.org/

$GEM install bundler
$GEM install rake
$GEM install rack
$GEM install thin
$GEM install redis
$GEM install selenium-webdriver
$GEM install faye
$GEM install eventmachine
$GEM install em-zeromq
$GEM install eventmachine-tail
$GEM install selenium-webdriver
$GEM install yajl-ruby
$GEM install sinatra
$GEM install sinatra-websocket
$GEM install mini_magick

# $PREFIX/bin/gem uninstall -aIx bundler
# $PREFIX/bin/gem install bundler