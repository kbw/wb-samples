#!/bin/sh
# https://github.com/antonello/kam/base/build.sh

scriptname=$(realpath $0)
scriptdir=$(dirname $scriptname)
cd $scriptdir

REPO=$(echo "$scriptdir" | rev | cut -d "/" -f 1 | rev | cut -d - -f 1)
TAG=$(echo "$scriptdir" | rev | cut -d "/" -f 1 | rev | cut -d - -f 2-)

USER=$(echo "$HOME" | rev | cut -d "/" -f 1 | rev)
UID=$(grep kbw /etc/passwd | cut -d ":" -f 3)
GID=$(grep kbw /etc/passwd | cut -d ":" -f 4)

ID_RSA="$(cat /home/kbw/.ssh/id_rsa)"
ID_RSA_PUB="$(cat /home/kbw/.ssh/id_rsa.pub)"

docker build --no-cache --rm \
  --build-arg REPO="$REPO" \
  --build-arg TAG="$TAG" \
  --build-arg USER="$USER" \
  --build-arg UID="$UID" \
  --build-arg ID_RSA="$ID_RSA" \
  --build-arg ID_RSA_PUB="$ID_RSA_PUB" \
  -t "${REPO}":"${TAG}" \
  .
