#!/bin/sh
#  https://github.com/antonello/kam/base/build.sh

scriptname=$(realpath $0)
scriptdir=$(dirname $scriptname)
cd $scriptdir

docker build --no-cache --rm \
  --build-arg USER=kbw \
  --build-arg UID=501 \
  --build-arg ID_RSA="$(cat /home/kbw/.ssh/id_rsa)" \
  --build-arg ID_RSA_PUB="$(cat /home/kbw/.ssh/id_rsa.pub)" \
  -t dev:base \
  .
