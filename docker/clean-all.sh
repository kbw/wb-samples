#!/bin/sh

for id in `docker ps       | grep -v IMAGE | awk '{print $2}'`; do (docker rmi -f $id) done
for id in `docker images   | grep -v IMAGE | awk '{print $3}'`; do (docker rmi -f $id) done
for id in `docker image ls | grep -v IMAGE | awk '{print $3}'`; do (docker rm  -f $id) done
