#!/bin/sh
# https://phoenixnap.com/kb/create-docker-images-with-dockerfile

scriptname=$(realpath $0)
scriptdir=$(dirname $scriptname)
cd $scriptdir

docker build -t hello .
